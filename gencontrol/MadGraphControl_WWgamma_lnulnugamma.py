from MadGraphControl.MadGraphUtils import *

fcard = open('proc_card_mg5.dat','w')
rcard='run_card.wwa.dat'
	
if not hasattr(runArgs,'runNumber'):
  raise RunTimeError("No run number found.")

#--------------------------------------------------------------
# MG5 Proc card
#--------------------------------------------------------------
if (runArgs.runNumber==185962):
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    generate p p > e- ve~ e+ ve a @1
    add process p p > mu- vm~ mu+ vm a @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WWgamma_lnulnuGamma"

elif (runArgs.runNumber==185963):
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~ 
    generate p p > e- ve~ mu+ vm a  @1
    add process p p > mu- vm~ e+ ve a @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WWgamma_enumunuGamma"

elif (runArgs.runNumber==185964):
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    generate p p > ta- vt~ e+ ve a @1
    add process p p > ta- vt~ mu+ vm a @2
    add process p p > ta- vt~ ta+ vt a @3
    add process p p > e- ve~ ta+ vt a @4
    add process p p > mu- vm~ ta+ vt a @5
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WWgamma_taunulnuGamma"

else:
    raise RunTimeError("No data set ID found")

beamEnergy=4000
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy=runArgs.ecmEnergy/2.0

else:
    raise RunTimeError("No center of mass energy found.")

# getting run cards
from PyJobTransformsCore.trfutil import get_files
get_files( rcard, keepDir=False, errorIfNotFound=True )

# generating events in MG
process_dir = new_process()

generate(run_card_loc=rcard,param_card_loc=None,mode=0,njobs=1,run_name='WWA',proc_dir=process_dir)

stringy = 'madgraph.' + str(runArgs.runNumber)+'.MadGraph_'+str(name)

skip_events = 0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name = 'WWA', proc_dir = process_dir, outputDS = stringy + '._00001.events.tar.gz', skip_events = skip_events)


#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include ( "MC12JobOptions/Pythia_Perugia2012_Common.py" )
topAlg.Pythia.PythiaCommand = ["pyinit user madgraph"]

#--------------------------------------------------------------
topAlg.Pythia.PythiaCommand +=  [
                "pystat 1 3 4 5",
                "pyinit dumpr 1 5",
                "pyinit pylistf 1",
                ]

evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.description = "MadGraph+Pythia6 production for " + str(name) + " with the Perugia2012 CTEQ6L1 tune"
evgenConfig.keywords = ["triboson", "electroweak", "SM"]
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = 5000
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
