# Set up for generic alpgen generation
#  This is the main job option to call

import sys
NJetInc=-1
Shower='Pythia' 
rand_seed=1
ecm = 4000.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenGenericGeneration')

if not hasattr(runArgs,'runNumber'):
    log.fatal('No run number found.  Generation will fail.  No idea how to decode this.')
    raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
    log.error('Random seed not set.  Will use 1.')
else:
    rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
    log.info('No skip events argument found.  Good!')
else:
    log.fatal('Skip events argument found.  No idea how to deal with that yet.  Bailing out.')
    raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
    log.warning('No center of mass energy found.  Will use the default of 7 TeV.')
else:
    ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
special1=None
special2=None
special3=None
nwarm = [ 4 , 1000000 ]

# design:
# 10 base samples: 0lp,1lp,2lp,3lp,4inc,4lp,5inc,ccbar,bbbar
nbase=10 #for better splitting
# 20 variations so far herwig, herwigpp as well as pythia perugia 2012, ktfac 0.5 perugia 2012, ktfac 2.0 perugia 2012,  15 spares   
nvar=20
# 3 channels: ll, ljets, allhadronic 
nchannel=3

# use dsids from 201000 to 201599
subrunfirst=201000
subrunlast=201000+nbase*nchannel*nvar-1 #(up to 201999 for Top WG reserved)
if run_num>=subrunfirst and run_num<=subrunlast:
    log.info('Recognized ttbar run number.  Will generate for run '+str(run_num))

    ### general settings 
    special1 = """ihvy 6
    itdec     1
    """

    ### specify base process
    jets = [(run_num-subrunfirst)%nbase,1]  #Exclusive matching for Np0-Np4
    ### Inclusive matching for Np5 or Np4
    if (run_num-subrunfirst)%nbase==4 :
        jets=[4,0]  #inclusive 4 jet bin 
    if (run_num-subrunfirst)%nbase==5 :
        jets=[4,1]  #exclusive 4 jet bin 
    if (run_num-subrunfirst)%nbase==6 :
        jets=[5,0]  #inclusive 5 jet bin 
    if NJetInc >= 0 :
        jets[NJetInc,0] #needed for testing 
    ### partonic cuts and process setting     
    if (run_num-subrunfirst)%nbase==7 :
        process = '4Q'
        jets = [0, 0] 
        special1 += """ihvy2 4
        ptcmin 0.0
        drcmin 0.0
        ptjmin 15.0
        ptbmin 0.0
        """
    elif (run_num-subrunfirst)%nbase==8 :
        process = '4Q'
        jets = [0, 0] 
        special1 += """ihvy2 5
        ptbmin 0.0
        drbmin 0.0
        ptjmin 15.0
        ptcmin 0.0
        """
    else :
        process = '2Q'
        special1 += """ptj1min -1
        ptj1max -1
        etajmax 6.0
        ptjmin 15
        drjmin 0.7
        """
    if (run_num-subrunfirst)%nbase==9 :     
        raise RunTimeError('Base sample not yet defined!')

    ### specify variation
    if int((run_num-subrunfirst)/(nbase))%nvar==0:
        Shower='Herwig'
    elif int((run_num-subrunfirst)/(nbase))%nvar==1:
        Shower='Herwigpp'
    else : 
        Shower='Pythia'
        
    if int((run_num-subrunfirst)/(nbase))%nvar==2:
        special1 += """ktfac 1.0
        """
    if int((run_num-subrunfirst)/(nbase))%nvar==3:
        special1 += """ktfac 0.5
        """
    if int((run_num-subrunfirst)/(nbase))%nvar==4:
        special1 += """ktfac 2.0
        """
    if int((run_num-subrunfirst)/(nbase))%nvar>4:
        raise RunTimeError('Systematic variation not yet defined!')


    ### Pythia settings  
    if Shower == 'Pythia' :
        special1 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
        lpclu 1         ! nloop for alphas in CKKW scale evaluation
        """

    ### specify channel
    if int((run_num-subrunfirst)/(nbase*nvar))%nchannel==0: # Use fully leptonic top decays
       special1 += """itdecmode 5
       """
    elif int((run_num-subrunfirst)/(nbase*nvar))%nchannel==1: # Use singleletponic top decays
        special1 += """itdecmode 4
       """
    elif int((run_num-subrunfirst)/(nbase*nvar))%nchannel==2: # all hadronic top decays 
        special1 += """itdecmode 6
       """
    else: 
        raise RunTimeError('Final state undefined!')
    
    special2=special1
    
    #    Pythia terminating early due to VERY low efficiency
    #                 16 min   32 min   ~20hrs    60 hrs    XXXX    XXXX 
    #                 30 min   2 hrs    92 hrs  XXXX     XXXX    XXXX  
    warmup_iters   = [3,4,5,6,6,6,6,3,3]
    warmup_numbers = [ 1000000,1000000, 25000000,25000000,10000000,10000000,10000000,1000000,1000000]
    #agrohsje off production 
    event_numbers  = [ 1000000,6000000,150000000,60000000,30000000,40000000,10000000,1000000,1000000]
    #agrohsje for grid production: 
    #event_numbers  = [ 1000000,6000000,150000000,5000,5000,5000,50,1000000,1000000]
    nwarm = [ warmup_iters[ (run_num-subrunfirst)%nbase ], warmup_numbers[ (run_num-subrunfirst)%nbase ] ]
    events_alpgen = event_numbers[ (run_num-subrunfirst)%nbase ]
    events_athena = 5000 #0,1 as well as b,c 
    if jets[0]==2: events_athena=2000
    if jets[0]==3: events_athena=2000
    if jets[0]==4: events_athena=500
    if jets[0]==5: events_athena=50
    
if process=='':
    log.fatal('Unknown run number!  Bailing out!')
    raise RunTimeError('Unknown run number.')

if hasattr(runArgs,'cutAthenaEvents'):
    events_athena = events_athena/runArgs.cutAthenaEvents
    log.warning('Reducing number of Athena events by '+str(runArgs.cutAthenaEvents)+' to '+str(events_athena)+' .  Later you should fix this.')
if hasattr(runArgs,'raiseAlpgenEvents'):
    events_alpgen = events_alpgen*runArgs.raiseAlpgenEvents
    log.warning('Increasing number of AlpGen events by '+str(runArgs.raiseAlpgenEvents)+' to '+str(events_alpgen)+' .  Later you should fix this.')

from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
    log.info('Special settings for mode 1:')
    log.info(special1)
if special2 is not None:
    log.info('Special settings for mode 2:')
    log.info(special2)
if special3 is not None:
    log.info('Special settings for pythia:')
    log.info(special3)

inputgridfile = None
if hasattr(runArgs,'inputGenConfFile'):
    log.info('Passing input grid file to AlpGenUtils ' +str(runArgs.inputGenConfFile))
    inputgridfile=runArgs.inputGenConfFile


SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2 , special3=special3 , inputgridfile=inputgridfile)

print special1

if hasattr(runArgs,'inputGeneratorFile'):
    log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'

if Shower == 'Herwig' :
    log.info('Passing events on to Herwig for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenHerwig.py') 
elif Shower == 'Herwigpp' :
    log.info('Passing events on to Herwigpp for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenHerwigpp.py') 
elif Shower == 'Pythia' :
    log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')
    #agrohsje add mc12joboptions for new perugia family and variation flag
    #www.hepforge.org/archive/pythia6/update_notes-6.4.27.txt
    #370 Perugia 2012 (P12) : Retune of Perugia 2011 w CTEQ6L1     (2012)
    #371 P12-radHi   : Variation with alphaS(pT/2)
    #372 P12-radLo   : Variation with alphaS(2pT)
    #373 P12-mpiHi   : Variation with more semi-hard MPI -> more UE
    #374 P12-loCR    : Variation using lower CR strength -> more Nch
    #375 P12-noCR    : Variation without any color reconnections
    #376 P12-FL      : Variation with more longitudinal fragmentation
    #377 P12-FT      : Variation with more transverse fragmentation
    #378 P12-M8LO    : Variation using MSTW 2008 LO PDFs
    #379 P12-LO**    : Variation using MRST LO** PDFs
    if int((run_num-subrunfirst)/(nbase))%nvar==3:
        include ("MC12JobOptions/AlpgenPythia_Perugia2012_radHi.py")
    elif int((run_num-subrunfirst)/(nbase))%nvar==4:
        include ("MC12JobOptions/AlpgenPythia_Perugia2012_radLo.py")
    else:
        include ("MC12JobOptions/AlpgenPythia_Perugia2012.py")
        
    # Additional bit for ME/PS matching
    phojf=open('./pythia_card.dat', 'w')
    phojinp = """
      !...Matching parameters...
      IEXCFILE=%i
      showerkt=T
      qcut=%i
      imss(21)=24
      imss(22)=24  
    """%(jets[1],15)
    # For the moment, hard-coded jet cut (in AlpGenUtils.py)

    phojf.write(phojinp)
    phojf.close()

# Fill up the evgenConfig
evgenConfig.description = 'alpgen_autoconfig'
evgenConfig.contact = ['zach.marshall@cern.ch']
evgenConfig.process = 'auto_alpgen_'+process+'_'+`jets[0]`+'p'
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
#evgenConfig.efficiency = 0.90000
evgenConfig.minevents = events_athena
