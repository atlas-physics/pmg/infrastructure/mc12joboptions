from PowhegControl.PowhegUtils import PowhegConfig_base

###############################################################################
#
#  ZZ
#
###############################################################################
class AtlasPowhegConfig_ZZ(PowhegConfig_base) :
  # These are user configurable - put generic properties in PowhegConfig_base
  mllmin = 4

  # integration parameters
  ncall1  = 300000
  itmx1   = 3
  ncall2  = 1000000
  itmx2   = 5
  foldcsi = 1
  foldy   = 1
  foldphi = 1
  nubound = 500000

  # Set process-dependent paths in the constructor
  def __init__(self,runArgs=None) :
    PowhegConfig_base.__init__(self,runArgs)
    self._powheg_executable += '/ZZ/pwhg_main'

  def generateRunCard(self) :
    self.generateRunCardSharedOptions()

    with open( str(self.TestArea)+'/powheg.input','a') as f :
      f.write( 'vdecaymodeZ1 '+str(self.vdecaymodeZ1)+' ! PDG code for charged decay product of the vector boson (11:e-; -11:e+; ...)\n' )
      f.write( 'vdecaymodeZ2 '+str(self.vdecaymodeZ2)+' ! PDG code for charged decay product of the vector boson (11:e-; -11:e+; ...)\n' )

      if self.mllmin:
          f.write( 'mllmin  '+str(self.mllmin)+'            ! Minimum invariant mass of lepton pairs from Z decay\n' )
