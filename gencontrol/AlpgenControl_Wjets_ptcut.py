# Set up for generic alpgen generation
#  This is the main job option to call

import sys

UseHerwig=True
rand_seed=1
ecm = 3500.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenGenericGeneration')

if not hasattr(runArgs,'runNumber'):
    log.fatal('No run number found.  Generation will fail.  No idea how to decode this.')
    raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
    log.error('Random seed not set.  Will use 1.')
else:
    rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
    log.info('No skip events argument found.  Good!')
else:
    log.fatal('Skip events argument found.  No idea how to deal with that yet.  Bailing out.')
    raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
    log.warning('No center of mass energy found.  Will use the default of 7 TeV.')
else:
    ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
ptcut=''
special1=None
special2=None
special3=None
nwarm = [ 4 , 1000000 ]

# W+jets samples with cut on W pt obo Sanya Solodkov
# Run numbers currently reserved:
# 147572-147643 and 185000-185141 - 72+142 = 214 runs in total
# first 72+72=144 runs subdivided like this
#   6 (number of jets) * 4 (pt range) * 3 (dR values) * 2 (e/mu flavour)
# then - 24 runs for Wcc only with decays into all lepton flavours
#   2 (number of jets) * 4 (pt range) * 3 (dR values)
# then - 18+18=36 runs for W,Wcc to e/mu without cut on W pt
#   6 (number of jets) * 3 (dR values) * 2 (e/mu flavour)
# then - 6 runs for Wcc only with decays into all lepton flavours without cut on Wpt
#   2 (number of jets) * 3 (dR values)
# 4 last run numbers are not used currently
#
# additional run numbers for qfac 0.5 and 2.0 
# 185508-185539 = 36 runs in total, only mu and no Wcc
#   4 (number of jets) * 4 (pt range) * 2 (qfac values) * 1 (mu flavour)

W_mode = 0
drmax  = 0
qfmax  = 0
if (147572<=run_num and run_num<=147643):
    run_off=run_num-147572
    W_mode = 1  # 1 - el
    jccoff = 0
    jccmax = 6  # 4 W+jets and 2 Wcc+jets
    wptoff = 1
    wptmax = 4  # 4 Wpt cuts
    droff  = 0
    drmax  = 3  # 3 dr values
elif (185000<=run_num and run_num<=185071):
    run_off=run_num-185000
    W_mode = 2  # 2 - mu
    jccoff = 0
    jccmax = 6  # 4 W+jets and 2 Wcc+jets
    wptoff = 1
    wptmax = 4  # 4 Wpt cuts
    droff  = 0
    drmax  = 3  # 3 dr values
elif (185072<=run_num and run_num<=185095):
    run_off=run_num-185072
    W_mode = 4  # 4 - e,mu,tau
    jccoff = 4  # only Wcc
    jccmax = 2  # 2 Wcc+jets
    wptoff = 1
    wptmax = 4  # 4 Wpt cuts
    droff  = 0
    drmax  = 3  # 3 dr values
elif (185096<=run_num and run_num<=185113):
    run_off=run_num-185096
    W_mode = 1  # 1 - el
    jccoff = 0
    jccmax = 6  # 4 W+jets and 2 Wcc+jets
    wptoff = 0  # no Wpt cut
    wptmax = 1  # one Wpt bin
    droff  = 0
    drmax  = 3  # 3 dr values
elif (185114<=run_num and run_num<=185131):
    run_off=run_num-185114
    W_mode = 2  # 2 - mu
    jccoff = 0
    jccmax = 6  # 4 W+jets and 2 Wcc+jets
    wptoff = 0  # no Wpt cut
    wptmax = 1  # one Wpt bin
    droff  = 0
    drmax  = 3  # 3 dr values
elif (185132<=run_num and run_num<=185137):
    run_off=run_num-185132
    W_mode = 4  # 4 - e,mu,tau
    jccoff = 4  # only Wcc
    jccmax = 2  # 2 Wcc+jets
    wptoff = 0  # no Wpt cut
    wptmax = 1  # one Wpt bin
    droff  = 0
    drmax  = 3  # 3 dr values

elif (185508<=run_num and run_num<=185539):
    run_off=run_num-185508
    W_mode = 2  # 2 - mu
    jccoff = 0
    jccmax = 4  # 4 W+jets only
    wptoff = 1
    wptmax = 4  # 4 Wpt cuts
    qfoff  = 1
    qfmax  = 2  # 2 qfac values


if W_mode>0:

    UseHerwig=True
    log.info('Recognized W+jets wuth pt cut run number.  Will generate for run '+str(run_num))

    jcc = run_off%jccmax+jccoff # type of W production:  Np1..Np4 or  Wcc Np1 or Wcc Np2
    jets = [1+(jcc%4),1] # Exclusive matching to the last number of partons Np1..Np4, Wcc Np1, Wcc Np2
    Wcc_mode = int(jcc/4) # 1 - Wcc

    # W decay mode
    special1 = 'iwdecmode %i          ! W decay mode (1: el, 2: mu, 3: tau, 4: all leptons)\n'%(W_mode)

    # Wcc mode
    if Wcc_mode>0:
        process = 'wqq'
        special1 += """ihvy 4        ! Select W+cc
ptcmin 0.            ! ptcmin
drcmin 0.            ! drcmin
"""
        if 3499.<ecm and ecm<3501.: # adjust Alpgen settings for 7 TeV MC11-like production 
            special1 += """mt 174.3             ! mt
mc 1.4               ! mc
"""
    else:
        process = 'wjet'
        if 3499.<ecm and ecm<3501.: # adjust Alpgen settings for 7 TeV MC11-like production 
            special1 += """mt 174.3             ! mt
"""

    # number of events to generate in singel job for Np1-Np4, Wcc Np1, Wcc Np2
    events_athena_def  = [ 5000,2000,500,100, 2000,100 ] 

    # scale factor for number of events in alpgen for different number of jets (Np1..Np4, Wcc Np1 Wcc Np2)
    # first 6 number in array - for dr=0.7 or dr=1.0, another 6 - for dr=0.4
    events_000_inf = [ 2000, 20000, 30000, 80000, 10000, 50000,   2000, 30000, 50000,100000, 15000, 80000 ]

    events_200_300 = [ 2000, 10000, 30000, 80000, 10000, 50000,   2000, 15000, 30000,100000, 10000, 50000 ]
    events_300_400 = [ 2000, 20000, 30000,150000, 25000,100000,   2000, 25000, 50000,260000, 25000,120000 ]
    events_400_500 = [ 2000, 20000, 60000,200000, 25000,200000,   2000, 25000,125000,260000, 30000,200000 ]
    events_500_inf = [ 2000, 25000,120000,260000, 25000,250000,   2000, 40000,175000,260000, 30000,250000 ]

    # index to be used for these arrays which might be corrected later
    evtInd = jcc

    if runArgs.maxEvents>0:
        events_athena = runArgs.maxEvents
    else:
        events_athena = events_athena_def[evtInd]

    # dR matching cut
    if drmax>0:
        dr_cut = int(run_off/(jccmax*wptmax))%drmax+droff # 0 - 0.7(default), 1- 0.4  2 - 1.0
        if dr_cut == 1:
            evtInd += 6
            special3 = "502    0.4        ! min RCLUS value for parton-jet matching\n"
            special1 += """drjmin  0.4          ! deltar jets
"""
        elif dr_cut == 2:
            special3 = "502    1.0        ! min RCLUS value for parton-jet matching\n"
            special1 += """drjmin  1.0          ! deltar jets
"""
        else:
            special3 = "502    0.7        ! min RCLUS value for parton-jet matching\n"
            special1 += """drjmin  0.7          ! deltar jets
"""

    # qfac scale - only for default dR matching cut
    elif qfmax>0:

        special3 = "502    0.7        ! min RCLUS value for parton-jet matching\n"
        special1 += """drjmin  0.7          ! deltar jets
"""
        qf_cut = int(run_off/(jccmax*wptmax))%qfmax+qfoff # 0 - 1.0(default), 1- 0.5  2 - 2.0
        if qf_cut == 1:
            special1 += """qfac  0.5            !  Factorization Scale Factor
"""
        elif qf_cut == 2:
            special1 += """qfac  2.0            !  Factorization Scale Factor
"""
        else:
            special1 += """qfac  1.0            !  Factorization Scale Factor
"""


    # pT slice index, index==0 - no silices
    pTsliceIndex = int(run_off/jccmax)%wptmax+wptoff # 1 - [200,300], 2 - [300,400], 3 - [400,500], 4 - [500,inf]
    ptcut=''
    pthrmin = -1.0
    pthrmax = -1.0
    events_alpgen = events_athena * events_000_inf[evtInd] 
    if pTsliceIndex==1:
        ptcut='_Wpt200to300'
        pthrmin = 200.
        pthrmax = 300.
        events_alpgen = events_athena * events_200_300[evtInd] 
    elif pTsliceIndex==2:
        ptcut='_Wpt300to400'
        pthrmin = 300.
        pthrmax = 400.
        events_alpgen = events_athena * events_300_400[evtInd] 
    elif pTsliceIndex==3:
        ptcut='_Wpt400to500'
        pthrmin = 400.
        pthrmax = 500.
        events_alpgen = events_athena * events_400_500[evtInd] 
    elif pTsliceIndex==4:
        ptcut='_Wpt500'
        pthrmin = 500.
        events_alpgen = events_athena * events_500_inf[evtInd] 
    elif pTsliceIndex==5: # not used currently
        ptcut='_Wpt200'
        pthrmin = 200.
        events_alpgen = events_athena * events_000_inf[evtInd] 

    if events_athena<50:
        events_alpgen *= 2
    if events_athena<20:
        events_alpgen *= 2

    if not (pthrmin<0.) or not (pthrmax<0.):
        special1 += """pthrmin %f          ! lower boson pT threshold
pthrmax %f        ! upper boson pT threshold
"""%(pthrmin, pthrmax)

    ncwarm = [2,2,3,3,4]
    
    nwarm = [ ncwarm[jets[0]] , events_alpgen ]

    special2 = special1

#########################################################

if process=='':
    log.fatal('Unknown run number!  Bailing out!')
    raise RunTimeError('Unknown run number.')

if not UseHerwig:  
        special1 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 
        special2 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 

if hasattr(runArgs,'cutAthenaEvents'):
    events_athena = events_athena/runArgs.cutAthenaEvents
    log.warning('Reducing number of Athena events by '+str(runArgs.cutAthenaEvents)+' to '+str(events_athena)+' .  Later you should fix this.')
if hasattr(runArgs,'raiseAlpgenEvents'):
    events_alpgen = events_alpgen*runArgs.raiseAlpgenEvents
    log.warning('Increasing number of AlpGen events by '+str(runArgs.raiseAlpgenEvents)+' to '+str(events_alpgen)+' .  Later you should fix this.')

from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
    log.info('Special settings for mode 1:')
    log.info(special1)
if special2 is not None:
    log.info('Special settings for mode 2:')
    log.info(special2)
if special3 is not None:
    if UseHerwig:  
        log.info('Special settings for Herwig:')
    else:
        log.info('Special settings for Pythia:')
    log.info(special3)

SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2 , special3=special3 )


if hasattr(runArgs,'inputGeneratorFile'):
    log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'

if UseHerwig:
    log.info('Passing events on to Herwig for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenHerwig.py') 

    if 3499.<ecm and ecm<3501.: # adjust Herwig settings for 7 TeV MC11-like production 
        print topAlg.Herwig.HerwigCommand 
        for i in xrange (0,len(topAlg.Herwig.HerwigCommand)):
            if topAlg.Herwig.HerwigCommand[i][0:5] == "swein":    # removing sin(theta) parameter completely 
                topAlg.Herwig.HerwigCommand[i] = "ptjim 4.412"    # this value was used in MC11
                print "Herwig swein parameter removed"
            elif topAlg.Herwig.HerwigCommand[i][0:5] == "ptjim":  # put old value of ptjim 
                topAlg.Herwig.HerwigCommand[i] = "ptjim 4.412"    # this value was used in MC11
                print "Herwig using ptjim 4.412"
        print topAlg.Herwig.HerwigCommand 
else:
    log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')
    include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
    include ( "MC12JobOptions/Pythia_Tauola.py" )
    include ( "MC12JobOptions/Pythia_Photos.py" )

    if 3499.<ecm and ecm<3501.: # adjust Pythia settings for 7 TeV MC11-like production (not yet done)
        print topAlg.Pythia.PythiaCommand

    # Additional bit for ME/PS matching
    phojf=open('./pythia_card.dat', 'w')
    phojinp = """
      !...Matching parameters...
      IEXCFILE=%i
      showerkt=T
      qcut=%i
      imss(21)=24
      imss(22)=24  
    """%(jets[1],15)
    # For the moment, hard-coded jet cut (in AlpGenUtils.py)

    phojf.write(phojinp)
    phojf.close()

# Fill up the evgenConfig
evgenConfig.description = 'alpgen_autoconfig'
evgenConfig.contact = ['sanya.solodkov@cern.ch' ]
evgenConfig.process = 'auto_alpgen_'+process+'_'+`jets[0]`+'p'+ptcut
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
#evgenConfig.efficiency = 0.90000
evgenConfig.minevents = events_athena
