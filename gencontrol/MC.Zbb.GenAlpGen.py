# Set up for generic alpgen generation
#  This is the main job option to call

import sys

UseHerwig=True
rand_seed=1
ecm = 3500.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenGenericGeneration')

if not hasattr(runArgs,'runNumber'):
    log.fatal('No run number found.  Generation will fail.  No idea how to decode this.')
    raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
    log.error('Random seed not set.  Will use 1.')
else:
    rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
    log.info('No skip events argument found.  Good!')
else:
    log.fatal('Skip events argument found.  No idea how to deal with that yet.  Bailing out.')
    raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
    log.warning('No center of mass energy found.  Will use the default of 7 TeV.')
else:
    ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
special1=None
special2=None
special3=None
nwarm = [ 4 , 1000000 ]
jets = 0

############################################################################

# Zbb 4LepM - Salvucci
UseM4lFilter = False
UseM4lVeto = False
Use3lFilter = False

if run_num>=146980 and run_num<=167110:
    
    log.info('Recognized Zbb+jets Filtered run number.  Will generate for run '+str(run_num))
    process = 'zqq'

    event_numbers=[250000000,120000000,100000000,131000000]
    events_athena = 5000
            
    #Zbb no filter
    if run_num>=167100 and run_num<=167110:
        run_min = 167100
        jets = [(run_num-run_min)%5,1] 
        if jets[0]==0: events_athena=500
        if jets[0]==1: events_athena=200
        if jets[0]==2: events_athena=2000
        if jets[0]==3: events_athena=1000
        if jets[0]==4: events_athena=500
        if jets[0]==5: events_athena=100

    #Zbb 4LepM
    if run_num>=146980 and run_num<=146989:
        UseM4lFilter = True
        run_min = 146980
        jets = [(run_num-run_min)%5,1] 
        if jets[0]==0: events_athena=500
        if jets[0]==1: events_athena=200
        if jets[0]==2: events_athena=2000
        if jets[0]==3: events_athena=1000
        if jets[0]==4: events_athena=500
        if jets[0]==5: events_athena=100
        
    #Zbb Veto4LepM+Pass3Lep
    if run_num>=146990 and run_num<=147700:
        UseM4lVeto = True
        Use3lFilter = True
        run_min = 146990
        jets = [(run_num-run_min)%5,1] 
        if jets[0]==0: events_athena=2000
        if jets[0]==1: events_athena=2000 
        if jets[0]==2: events_athena=200
        if jets[0]==3: events_athena=100
        if jets[0]==4: events_athena=50
        if jets[0]==5: events_athena=10
        
    nwarm = [ jets[0]+2 , 1000000 ]
    events_alpgen = event_numbers[jets[0]]
    # Ok, just kidding, we aren't doing 7 partons, we're doing 5 inclusive
    if jets[0]>3: jets[0]=3
    if jets[0]==3: jets[1]=0
    
    # Z_mode = 1(ee) 2(mumu) 3(tautau)
    Z_mode = int((run_num-run_min)/5)%2+1
    
    special1 = """mllmin  30           ! Minimum M_ll
mllmax  2000         ! Maximum M_ll
ihvy    5            ! Select Z+bb
mc      0            ! quark b mass
ptbmin  0            ! quark b pt min
ptcmin  0            ! quark c pt min
drjmin  0.4          ! deltar jets
drbmin  0            ! deltar b
"""

    if Z_mode<4: special1 += """ilep 0        ! Use leptons in the final state (not neutrinos)
izdecmode %i          ! Z decay mode (1: ee, 2: mumu, 3: tautau)\n """%(Z_mode)
    else: special1 += """ilep 1       ! Use neutrinos in the final state (not leptons) \n """
    special2 = special1

    special3 = "502    0.4        ! min RCLUS value for parton-jet matching\n"

if process=='':
    log.fatal('Unknown run number!  Bailing out!')
    raise RunTimeError('Unknown run number.')

if hasattr(runArgs,'cutAthenaEvents'):
    events_athena = events_athena/runArgs.cutAthenaEvents
    log.warning('Reducing number of Athena events by '+str(runArgs.cutAthenaEvents)+' to '+str(events_athena)+' .  Later you should fix this.')
if hasattr(runArgs,'raiseAlpgenEvents'):
    events_alpgen = events_alpgen*runArgs.raiseAlpgenEvents
    log.warning('Increasing number of AlpGen events by '+str(runArgs.raiseAlpgenEvents)+' to '+str(events_alpgen)+' .  Later you should fix this.')

from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
    log.info('Special settings for mode 1:')
    log.info(special1)
if special2 is not None:
    log.info('Special settings for mode 2:')
    log.info(special2)

SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2, special3=special3 )

if hasattr(runArgs,'inputGeneratorFile'):
    log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'

if UseHerwig:
    log.info('Passing events on to Herwig for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenHerwig.py') 
else:
    log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenPythia.py')

if Use3lFilter: include("MC12JobOptions/MC.GenAlpGen3lFilter.py")

if UseM4lFilter: include("MC12JobOptions/MC.GenAlpGenM4lFilter.py")

if UseM4lVeto: include("MC12JobOptions/MC.GenAlpGenM4lVeto.py")

# Fill up the evgenConfig
evgenConfig.description = 'alpgen_autoconfig'
evgenConfig.contact = ['zach.marshall@cern.ch','antonio.salvucci@cern.ch']
evgenConfig.process = 'auto_alpgen_'+process+'_'+`jets[0]`+'p'
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
#evgenConfig.efficiency = 0.90000
evgenConfig.minevents = events_athena

