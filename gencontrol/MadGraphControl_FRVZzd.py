from MadGraphControl.MadGraphUtils import *

# MC12.188987.MadGraphPythia8_AU2CTEQ6L1_FRVZ2zd_mH300.py
# MC12.188988.MadGraphPythia8_AU2CTEQ6L1_FRVZ2zd_mH600.py
# MC12.188989.MadGraphPythia8_AU2CTEQ6L1_FRVZ2zd_mH900.py
# MC12.188990.MadGraphPythia8_AU2CTEQ6L1_FRVZ4zd_mH300.py
# MC12.188991.MadGraphPythia8_AU2CTEQ6L1_FRVZ4zd_mH600.py
# MC12.188992.MadGraphPythia8_AU2CTEQ6L1_FRVZ4zd_mH900.py

# Variables that depend on run number: which process and which Higgs mass
process = { 
    2:  'generate g g > h > nd2 nd2, (nd2 > nd1 zd, zd > f- f+), (nd2 > nd1 zd, zd > f- f+)',
    4:  'generate g g > h > nd2 nd2, (nd2 > nd1 hd1, (hd1 > zd zd, zd > mmu- mmu+)), (nd2 > nd1 hd1, (hd1 > zd zd, zd > mmu- mmu+))' 
    }

if runArgs.runNumber==188987:
    mH=300
    nGamma=2
elif runArgs.runNumber==188988:
    mH=600
    nGamma=2
elif runArgs.runNumber==188989:
    mH=900
    nGamma=2
elif runArgs.runNumber==188990:
    mH=300
    nGamma=4
elif runArgs.runNumber==188991:
    mH=600
    nGamma=4
elif runArgs.runNumber==188992:
    mH=900
    nGamma=4
else:
    raise

if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 4000.

# initialise random number generator/sequence
import random
random.seed(runArgs.randomSeed)
# lifetime function
def lifetime(decaywidth = 4.2e-15):
    import math
    tau = 1.973269718e-13/decaywidth
    t = random.random()
    return -1.0 * tau * math.log(t)

# basename for madgraph LHEF file
rname = 'run_lj'+str(runArgs.runNumber)

# do not run MadGraph if config only is requested
if not opts.config_only:

    # writing proc card for MG
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
import model_v4 usrmodv4_lj
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define f- = me- mmu- pi-
define f+ = me+ mmu+ pi+
%s
output -f
""" % process[nGamma])
    fcard.close()


    # getting run cards
    from PyJobTransformsCore.trfutil import get_files
    get_files( 'MadGraph_DisplacedLJ_FRVZ_runcard.dat', keepDir=False, errorIfNotFound=True )
    get_files( 'MadGraph_DisplacedLJ_FRVZ_paramcard_mH%s.dat' % mH, keepDir=False, errorIfNotFound=True )

    # generating events in MG
    process_dir = new_process()

    generate(run_card_loc='MadGraph_DisplacedLJ_FRVZ_runcard.dat',param_card_loc='MadGraph_DisplacedLJ_FRVZ_paramcard_mH%s.dat' % mH,mode=0,njobs=1,run_name=rname,proc_dir=process_dir)

    # replacing lifetime of dark photon, manually
    unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz'])
    unzip1.wait()

    decaywidth = 4.2e-15
    oldlhe = open(process_dir+'/Events/'+rname+'/unweighted_events.lhe','r')
    newlhe = open(process_dir+'/Events/'+rname+'/unweighted_events2.lhe','w')
    init = True
    for line in oldlhe:
        if init==True:
            newlhe.write(line)
            if '</init>' in line:
                init = False
        else:
            if '3000001' in line:
                part1 = line[:-7]
                part2 = "%.11E" % (lifetime(decaywidth))
                part3 = line[-5:]
                newlhe.write(part1+part2+part3)
            elif '-3000011' in line:
                part1 = '      -11'
                part2 = line[10:]
                newlhe.write(part1+part2)
            elif '3000011' in line:
                part1 = '       11'
                part2 = line[10:]
                newlhe.write(part1+part2)
            elif '-3000013' in line:
                part1 = '      -13'
                part2 = line[10:]
                newlhe.write(part1+part2)
            elif '3000013' in line:
                part1 = '       13'
                part2 = line[10:]
                newlhe.write(part1+part2)
            else:
                newlhe.write(line)

    oldlhe.close()
    newlhe.close()

    zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+rname+'/unweighted_events2.lhe'])
    zip1.wait()
    shutil.move(process_dir+'/Events/'+rname+'/unweighted_events2.lhe.gz',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz')
    os.remove(process_dir+'/Events/'+rname+'/unweighted_events.lhe')

    arrange_output(run_name=rname,proc_dir=process_dir,outputDS=rname+'._00001.events.tar.gz',skip_events=False)

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
include ( "MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py" )
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
include("MC12JobOptions/Pythia8_MadGraph.py")
include ( "MC12JobOptions/Pythia8_Photos.py" )
topAlg.Pythia8.Commands += ["Main:timesAllowErrors = 5000"]

from TruthExamples.TruthExamplesConf import TestHepMC
topAlg += TestHepMC()
topAlg.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
topAlg.TestHepMC.MaxVtxDisp = 100000000 #in mm
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["MadGraph"]
evgenConfig.description = 'FRVZ process Higgs -> %sgamma_d + X with mH=%sGeV' % (nGamma,mH)
evgenConfig.keywords = ["exotics", "longlived", "nonSMhiggs"]
evgenConfig.inputfilecheck = rname
runArgs.inputGeneratorFile=rname+'._00001.events.tar.gz'

