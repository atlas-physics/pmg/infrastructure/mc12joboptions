include("MC12JobOptions/VBFForwardJetsFilter.py")

VBFForwardJetsFilter = topAlg.VBFForwardJetsFilter
VBFForwardJetsFilter.JetMinPt=15.*GeV
VBFForwardJetsFilter.JetMaxEta=3.0
VBFForwardJetsFilter.NJets=1
VBFForwardJetsFilter.Jet1MinPt=15.*GeV
VBFForwardJetsFilter.Jet1MaxEta=3.0
VBFForwardJetsFilter.Jet2MinPt=-1.*GeV
VBFForwardJetsFilter.Jet2MaxEta=100.0
VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
VBFForwardJetsFilter.MassJJ=-1.*GeV
VBFForwardJetsFilter.DeltaEtaJJ=-1.0
#VBFForwardJetsFilter.TruthJetContainer="Cone4TruthJets"
VBFForwardJetsFilter.TruthJetContainer="AntiKt4TruthJets"
VBFForwardJetsFilter.LGMinPt=15.*GeV
VBFForwardJetsFilter.LGMaxEta=2.5
VBFForwardJetsFilter.DeltaRJLG=0.05
VBFForwardJetsFilter.RatioPtJLG=0.3

