include("MC12JobOptions/ATauFilter.py")

ATauFilter = topAlg.ATauFilter
ATauFilter.Etacut      = 3.0
ATauFilter.llPtcute    = 5000.
ATauFilter.llPtcutmu   = 5000.
ATauFilter.lhPtcute    = 10000.
ATauFilter.lhPtcutmu   = 10000.
ATauFilter.lhPtcuth    = 10000.
ATauFilter.hhPtcut     = 12000.0
ATauFilter.maxdphi     = 10.0

