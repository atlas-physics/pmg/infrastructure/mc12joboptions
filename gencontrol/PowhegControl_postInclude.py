# perform some checks

if evgenConfig.minevents > 0 :
    powheg_nEvents = evgenConfig.minevents * evt_multiplier
else:
    powheg_nEvents = 5000 * evt_multiplier
if powheg_nEvents> 5000*1000:
    # 5M powheg events ~= file size of 7GB 
    evgenLog.error('The event multiplier times minevents needs to be less than 5M. Otherwise the LHEF file will be too big for grid jobs!')
    raise

process, decay = process.split('_')

if process == 'Z':
    # this would get the defaults from the PowhegControl package
    # include('PowhegControl/PowhegControl_Z_Common.py')

    # this gives ATLAS defaults
    include('MC12JobOptions/PowhegControl_Z.py')
    PowhegConfig = AtlasPowhegConfig_Z(runArgs)

    # handle decays: ee, mumu, tautau
    if decay == 'ee':
        PowhegConfig.vdecaymode=1
    elif decay == 'mumu':
        PowhegConfig.vdecaymode=2
    elif decay == 'tautau':
        PowhegConfig.vdecaymode=3
    else:
        evgenLog.error('Decay %s for powheg process %s not supported: exit' % (decay,process))

#elif process == 'Zj':
#    include('PowhegControl/PowhegControl_Zj_Common.py')

elif process == 'Wp' or process == 'Wm':
    # this would get the defaults from the PowhegControl package
    # include('PowhegControl/PowhegControl_W_Common.py')

    # this gives ATLAS defaults
    include('MC12JobOptions/PowhegControl_W.py')
    PowhegConfig = AtlasPowhegConfig_W(runArgs)

    # handle W charge
    if process == 'Wp':
        PowhegConfig.idvecbos=24
    elif process == 'Wm':
        PowhegConfig.idvecbos=-24
    else:
        evgenLog.error('Powheg process %s not supported: exit' % process)

    # handle decays: enu, munu, taunu
    if decay == 'enu':
        PowhegConfig.vdecaymode=1
    elif decay == 'munu':
        PowhegConfig.vdecaymode=2
    elif decay == 'taunu':
        PowhegConfig.vdecaymode=3
    else:
        evgenLog.error('Decay %s for powheg process %s not supported: exit' % (decay,process))

#elif process == 'Wplusj' or process == 'Wminusj':
#    include('PowhegControl/PowhegControl_Wj_Common.py')

elif process == 'WpWm':
    # this would get the defaults from the PowhegControl package
    # include('PowhegControl/PowhegControl_WW_Common.py')

    # this gives ATLAS defaults
    include('MC12JobOptions/PowhegControl_WW.py')
    PowhegConfig = AtlasPowhegConfig_WW(runArgs)

    if decay == 'ee':
        PowhegConfig.vdecaymodeWp = -11
        PowhegConfig.vdecaymodeWm = 11
    elif decay == 'mue':
        PowhegConfig.vdecaymodeWp = -13
        PowhegConfig.vdecaymodeWm = 11
    elif decay == 'taue':
        PowhegConfig.vdecaymodeWp = -15
        PowhegConfig.vdecaymodeWm = 11
    elif decay == 'emu':
        PowhegConfig.vdecaymodeWp = -11
        PowhegConfig.vdecaymodeWm = 13
    elif decay == 'mumu':
        PowhegConfig.vdecaymodeWp = -13
        PowhegConfig.vdecaymodeWm = 13
    elif decay == 'taumu':
        PowhegConfig.vdecaymodeWp = -15
        PowhegConfig.vdecaymodeWm = 13
    elif decay == 'etau':
        PowhegConfig.vdecaymodeWp = -11
        PowhegConfig.vdecaymodeWm = 15
    elif decay == 'mutau':
        PowhegConfig.vdecaymodeWp = -13
        PowhegConfig.vdecaymodeWm = 15
    elif decay == 'tautau':
        PowhegConfig.vdecaymodeWp = -15
        PowhegConfig.vdecaymodeWm = 15
    else:
        evgenLog.error('Decay %s for powheg process %s not supported: exit' % (decay,process))
    
elif process == 'WpZ' or process == 'WmZ':
    # this would get the defaults from the PowhegControl package
    # include('PowhegControl/PowhegControl_WZ_Common.py')

    # this gives ATLAS defaults
    include('MC12JobOptions/PowhegControl_WZ.py')
    PowhegConfig = AtlasPowhegConfig_WZ(runArgs)

    # handle W charge
    if process == 'WpZ':
        if decay == 'enuee':
            PowhegConfig.vdecaymodeW = -11
            PowhegConfig.vdecaymodeZ = 11
        elif decay == 'enumumu':
            PowhegConfig.vdecaymodeW = -11
            PowhegConfig.vdecaymodeZ = 13
        elif decay == 'enutautau':
            PowhegConfig.vdecaymodeW = -11
            PowhegConfig.vdecaymodeZ = 15
        elif decay == 'munuee':
            PowhegConfig.vdecaymodeW = -13
            PowhegConfig.vdecaymodeZ = 11
        elif decay == 'munumumu':
            PowhegConfig.vdecaymodeW = -13
            PowhegConfig.vdecaymodeZ = 13
        elif decay == 'munutautau':
            PowhegConfig.vdecaymodeW = -13
            PowhegConfig.vdecaymodeZ = 15
        elif decay == 'taunuee':
            PowhegConfig.vdecaymodeW = -15
            PowhegConfig.vdecaymodeZ = 11
        elif decay == 'taunumumu':
            PowhegConfig.vdecaymodeW = -15
            PowhegConfig.vdecaymodeZ = 13
        elif decay == 'taunutautau':
            PowhegConfig.vdecaymodeW = -15
            PowhegConfig.vdecaymodeZ = 15
        else:
            evgenLog.error('Decay %s for powheg process %s not supported: exit' % (decay,process))
    elif process == 'WmZ':
        if decay == 'enuee':
            PowhegConfig.vdecaymodeW = 11
            PowhegConfig.vdecaymodeZ = 11
        elif decay == 'enumumu':
            PowhegConfig.vdecaymodeW = 11
            PowhegConfig.vdecaymodeZ = 13
        elif decay == 'enutautau':
            PowhegConfig.vdecaymodeW = 11
            PowhegConfig.vdecaymodeZ = 15
        elif decay == 'munuee':
            PowhegConfig.vdecaymodeW = 13
            PowhegConfig.vdecaymodeZ = 11
        elif decay == 'munumumu':
            PowhegConfig.vdecaymodeW = 13
            PowhegConfig.vdecaymodeZ = 13
        elif decay == 'munutautau':
            PowhegConfig.vdecaymodeW = 13
            PowhegConfig.vdecaymodeZ = 15
        elif decay == 'taunuee':
            PowhegConfig.vdecaymodeW = 15
            PowhegConfig.vdecaymodeZ = 11
        elif decay == 'taunumumu':
            PowhegConfig.vdecaymodeW = 15
            PowhegConfig.vdecaymodeZ = 13
        elif decay == 'taunutautau':
            PowhegConfig.vdecaymodeW = 15
            PowhegConfig.vdecaymodeZ = 15
        else:
            evgenLog.error('Decay %s for powheg process %s not supported: exit' % (decay,process))
    else:
        evgenLog.error('Powheg process %s not supported: exit' % process)

elif process == 'ZZ':
    # this would get the defaults from the PowhegControl package
    # include('PowhegControl/PowhegControl_ZZ_Common.py')

    # this gives ATLAS defaults
    include('MC12JobOptions/PowhegControl_ZZ.py')
    PowhegConfig = AtlasPowhegConfig_ZZ(runArgs)

    if decay == 'eeee':
        PowhegConfig.vdecaymodeZ1 = 11
        PowhegConfig.vdecaymodeZ2 = 11
    elif decay == 'eemumu':
        PowhegConfig.vdecaymodeZ1 = 11
        PowhegConfig.vdecaymodeZ2 = 13
    elif decay == 'eetautau':
        PowhegConfig.vdecaymodeZ1 = 11
        PowhegConfig.vdecaymodeZ2 = 15
    elif decay == 'mumumumu':
        PowhegConfig.vdecaymodeZ1 = 13
        PowhegConfig.vdecaymodeZ2 = 13
    elif decay == 'mumutautau':
        PowhegConfig.vdecaymodeZ1 = 13
        PowhegConfig.vdecaymodeZ2 = 15
    elif decay == 'tautautautau':
        PowhegConfig.vdecaymodeZ1 = 15
        PowhegConfig.vdecaymodeZ2 = 15
    elif decay == 'eenunu':
        PowhegConfig.vdecaymodeZ1 = 11
        PowhegConfig.vdecaymodeZ2 = 12
    elif decay == 'mumununu':
        PowhegConfig.vdecaymodeZ1 = 13
        PowhegConfig.vdecaymodeZ2 = 14
    elif decay == 'tautaununu':
        PowhegConfig.vdecaymodeZ1 = 15
        PowhegConfig.vdecaymodeZ2 = 16
    else:
        evgenLog.error('Decay %s for powheg process %s not supported: exit' % (decay,process))
        
elif process == 'tt':
    # this would get the defaults from the PowhegControl package
    # include('PowhegControl/PowhegControl_tt_Common.py')

    # this gives ATLAS defaults
    include('MC12JobOptions/PowhegControl_ttbar.py')
    PowhegConfig = AtlasPowhegConfig_tt(runArgs)

    if decay == 'all':
        PowhegConfig.topdecaymode=22222
    elif decay == 'tt':
        PowhegConfig.topdecaymode=00000
    else:
        evgenLog.error('Decay %s for powheg process %s not supported: exit' % (decay,process))


elif process == 'ggH':
    # this would get the defaults from the PowhegControl package
    # include('PowhegControl/PowhegControl_ggH_Common.py')

    # this gives ATLAS defaults
    include('MC12JobOptions/PowhegControl_ggH.py')
    PowhegConfig = AtlasPowhegConfig_ggH(runArgs)

    if decay == 'all':
        PowhegConfig.hdecaymode=0
    else:
        evgenLog.error('Decay %s for powheg process %s not supported: exit' % (decay,process))

elif process == 'VBF':
    # this would get the defaults from the PowhegControl package
    # include('PowhegControl/PowhegControl_VBF_Common.py')

    # this gives ATLAS defaults
    include('MC12JobOptions/PowhegControl_VBF.py')
    PowhegConfig = AtlasPowhegConfig_VBF(runArgs)

    if decay == 'all':
        PowhegConfig.hdecaymode=0
    else:
        evgenLog.error('Decay %s for powheg process %s not supported: exit' % (decay,process))


else:
    evgenLog.error('Powheg process %s not supported: exit' % process)
    raise


if 'Pythia6' in postGenerator:
    postGenerator = postGenerator.replace('Pythia6','')

    if not postGeneratorTune: 
        postGeneratorTune='Perugia2011C'
    evgenLog.info('Will use Pythia6 for hadronisation using %s PDF' % postGeneratorTune)
    include('MC12JobOptions/Pythia_%s_Common.py' % postGeneratorTune)
    evgenConfig.generators += ["Powheg"]

    ## Read external Les Houches event file
    topAlg.Pythia.PythiaCommand += ["pyinit user lhef"]

    if 'Tauola' in postGenerator:
        include("MC12JobOptions/Pythia_Tauola.py")
        postGenerator = postGenerator.replace('Tauola','')
    if 'Photos' in postGenerator:
        include("MC12JobOptions/Pythia_Photos.py")
        postGenerator = postGenerator.replace('Photos','')

elif 'Pythia8' in postGenerator:
    postGenerator = postGenerator.replace('Pythia8','')

    # Pythia8 showering with new, main31-style shower
    if not postGeneratorTune: 
        postGeneratorTune='AU2_CT10'
    evgenLog.info('Will use Pythia8 for hadronisation using %s PDf' % postGeneratorTune)
    include('MC12JobOptions/Pythia8_%s_Common.py' % postGeneratorTune)
    include('MC12JobOptions/Pythia8_Powheg.py')
    # xxx or 
    # include('MC12JobOptions/Pythia8_Powheg_Main31.py')

    if 'Tauola' in postGenerator:
        include("MC12JobOptions/Pythia8_Tauola.py")
        postGenerator = postGenerator.replace('Tauola','')
    if 'Photos' in postGenerator:
        include("MC12JobOptions/Pythia8_Photos.py")
        postGenerator = postGenerator.replace('Photos','')

elif 'Jimmy' in postGenerator:
    postGenerator = postGenerator.replace('Jimmy','')

    if not postGeneratorTune: 
        postGeneratorTune='AUET2_CT10'

    evgenLog.info('Will use Jimmy for hadronisation using %s PDf' % postGeneratorTune)
    include('MC12JobOptions/Jimmy_%s_Common.py' % postGeneratorTune)

    ## Read external Les Houches event file
    topAlg.Herwig.HerwigCommand += ["iproc lhef"]
    evgenConfig.generators += ["Powheg"]

    if 'Tauola' in postGenerator:
        include('MC12JobOptions/Jimmy_Tauola.py')
        postGenerator = postGenerator.replace('Tauola','')
    if 'Photos' in postGenerator:
        include('MC12JobOptions/Jimmy_Photos.py')
        postGenerator = postGenerator.replace('Photos','')

#elif 'Herwigpp' in postGenerator:
#    postGenerator = postGenerator.replace('Herwigpp','')
#
#    if not postGeneratorTune: 
#        postGeneratorTune='UEEE3_CTEQ6L1'
#    evgenLog.info('Will use Herwig++ for hadronisation using %s PDf' % postGeneratorTune)
#    include ('MC12JobOptions/Herwigpp_%s_LHEF_Common.py' % postGeneratorTune)

# check if something is left to do
if postGenerator != '':
    evgenLog.error('No post generator for hadronisation specified: exit')
    raise
    

# change the default powheg PDF if requested
if pdf:
    import os

    def findline(filename, tag):
        """ find first occurens if tag in file and return line """
        for l in open(filename):
            if l.find(tag) > -1:
                return l
        return None

    if isinstance(pdf, str):
        # if pdf variable is of type string, interprete as PDF name (needs PDF index look up)

        # check if PDF is available
        if not os.path.exists(os.path.join(os.getenv('LHAPATH'), pdf+'.LHgrid')):
            evgenLog.error('PDF name %s not supported by LHAPDF installation: exit!' % pdf)
            raise

        if pdf == 'CT10':
            PowhegConfig.PDF = 10800
        else:
            evgenLog.error('PDF %s for powheg not supported: exit' % pdf)
            raise

    elif isinstance(pdf, int):
        # if pdf variable is of type int, interprete as PDF id

        # check if PDF is available
        l = findline(os.getenv('LHAPATH')+'/../PDFsets.index', str(pdf))
        if not l:
            evgenLog.error('PDF number %s not supported by LHAPDF version: exit!' % pdf)
            raise

        if not os.path.exists(os.path.join(os.getenv('LHAPATH'), l.split()[4])):
            evgenLog.error('PDF number/name %s/%s not supported by LHAPDF installation: exit!' % (pdf,l.split()[4]))
            raise

        # set pdf
        PowhegConfig.PDF = pdf

    else:
        evgenLog.error('PDF attribute type %s for powheg not supported: exit' % type(pdf))
        raise


# --- any overrides for powheg config
if 'powheg_override' in dir():
    print 'Powheg control before override:', PowhegConfig
    powheg_override()
    print 'Powheg control after override:', PowhegConfig


# --- now run the whole thing

# 
old_nEvents = PowhegConfig.nEvents
PowhegConfig.nEvents=powheg_nEvents
evgenLog.info('Change number of powheg events from %s to %s' % (old_nEvents, PowhegConfig.nEvents))

# generate Powheg card 
PowhegConfig.generateRunCard()

# run Powheg
if not opts.config_only:
    PowhegConfig._PowhegConfig_base__output_events_file_name = 'powheg_on_the_fly.lhe.events'
    PowhegConfig.generateEvents()
else:
    # python touch
    open('powheg_on_the_fly.lhe.events','a').close()
runArgs.inputGeneratorFile    = 'powheg_on_the_fly'

# block PowhegConfig attributes now
def blocksetattr(object, name, value):
    """ 
    the object attributes are blocked, through exception 

    this does work only on class level, but not on object level, as
    __setattr__() needs to be defined in the class ++
    """
    raise AttributeError('Attributes of class %s are blocked! powheg already configured!' % object.__class__.__name__)

setattr(PowhegConfig.__class__, '__setattr__', blocksetattr)

# and back to athena for hadronisation
