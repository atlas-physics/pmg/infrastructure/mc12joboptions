# Set up for generic alpgen generation
#	This is the main job option to call

import sys

rand_seed=1
ecm = 3500.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenGenericGeneration')

if not hasattr(runArgs,'runNumber'):
	log.fatal('No run number found.	Generation will fail.	No idea how to decode this.')
	raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
	log.error('Random seed not set.	Will use 1.')
else:
		rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
	log.info('No skip events argument found.	Good!')
else:
	log.fatal('Skip events argument found.	No idea how to deal with that yet.	Bailing out.')
	raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
	log.warning('No center of mass energy found.	Will use the default of 7 TeV.')
else:
	ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
special1=None
special2=None
special3=None
nwarm = [ 4 , 1000000 ]

# Z+jets samples with 2lepton filter, pT > 7 GeV
if run_num>=178354 and run_num<=178368:
	log.info('Recognized Z+jets run number.	Will generate for run '+str(run_num))
	process = 'zjet'
	ZJET=True
	run_min=178354
	jets = [(run_num-run_min)%5,1] # Exclusive matching to the last number of partons

	Z_mode = int((run_num-run_min)/5) + 1
	if Z_mode==3: #tautau
		event_numbers=[5200000,1000000,5500000,7500000,20000000]

		events_athena = 1000
		if jets[0]==1: events_athena=1000
		if jets[0]==2: events_athena=100
		if jets[0]==3: events_athena=10
		if jets[0]==4: events_athena=10
		
	else: #ee and mumu
		event_numbers=[1100000,900000,5000000,6500000,4500000]

		events_athena = 5000
		if jets[0]==1: events_athena=5000
		if jets[0]==2: events_athena=500
		if jets[0]==3: events_athena=50
		if jets[0]==4: events_athena=10
	
	ncwarm = [2,2,3,3,4]
	nwarm = [ ncwarm[jets[0]] , event_numbers[jets[0]] ]
	events_alpgen = event_numbers[jets[0]]

	special1 = """mllmin	10					 ! Minimum M_ll
mllmax	40					! Maximum M_ll
"""
	if jets[0]==4: jets[1]=0

	special1 += """ilep 0				! Use leptons in the final state (not neutrinos)
izdecmode %i					! Z decay mode (1: ee, 2: mumu, 3: tautau)\n """%(Z_mode)
	#special1 += """ilep 1			 ! Use neutrinos in the final state (not leptons) \n """

	special2 = special1
		
# Z+jets samples with 2lepton filter, pT > 7 GeV
if run_num>=178369 and run_num<=178383:
	log.info('Recognized Z+jets run number.	Will generate for run '+str(run_num))
	process = 'zjet'
	ZJET=True
	run_min=178369
	jets = [(run_num-run_min)%5,1] # Exclusive matching to the last number of partons

	Z_mode = int((run_num-run_min)/5) + 1
	if Z_mode==3: #tautau
		event_numbers=[700000,1100000,7000000,4000000,8000000]

		events_athena = 5000
		if jets[0]==1: events_athena=5000
		if jets[0]==2: events_athena=1000
		if jets[0]==3: events_athena=200
		if jets[0]==4: events_athena=50
		
	else: #ee and mumu
		event_numbers=[700000,400000,9000000,10000000,4000000]
		
		events_athena = 5000
		if jets[0]==1: events_athena=5000
		if jets[0]==2: events_athena=5000
		if jets[0]==3: events_athena=1000
		if jets[0]==4: events_athena=50
	
	ncwarm = [2,2,3,3,4]
	nwarm = [ ncwarm[jets[0]] , event_numbers[jets[0]] ]
	events_alpgen = event_numbers[jets[0]]

	special1 = """mllmin	40					 ! Minimum M_ll
mllmax	60					! Maximum M_ll
"""
	if jets[0]==4: jets[1]=0

	special1 += """ilep 0				! Use leptons in the final state (not neutrinos)
izdecmode %i					! Z decay mode (1: ee, 2: mumu, 3: tautau)\n """%(Z_mode)
	#special1 += """ilep 1			 ! Use neutrinos in the final state (not leptons) \n """

	special2 = special1

# Zcc+jets samples with 2lepton filter, pT > 7 GeV
if run_num>=178384 and run_num<=178395:
	log.info('Recognized Zcc+jets run number.	Will generate for run '+str(run_num))
	process = 'zqq'
	run_min=178384
	jets = [(run_num-run_min)%4,1] # Exclusive matching to the last number of partons
	if jets[0]==3: jets[1]=0

	Z_mode = int((run_num-run_min)/4) + 1
	if Z_mode==3: #tautau
		event_numbers=[8500000,9500000,16500000,32000000]

		events_athena = 100
		if jets[0]==1: events_athena=100
		if jets[0]==2: events_athena=10
		if jets[0]==3: events_athena=5
		
	else: #ee and mumu
		event_numbers=[5000000,7500000,15000000,40000000]

		events_athena = 1000
		if jets[0]==1: events_athena=500
		if jets[0]==2: events_athena=50
		if jets[0]==3: events_athena=10
	
	ncwarm = [2,2,3,3]
	nwarm = [ ncwarm[jets[0]] , event_numbers[jets[0]] ]
	events_alpgen = event_numbers[jets[0]]
		
	special1 = """mllmin	10					 ! Minimum M_ll
mllmax	60					! Maximum M_ll 
ihvy		4						 ! Select Z+cc
mc      1.4           ! quark c mass
mb      4.7           ! quark b mass
ptcmin  0.            ! quark c pt min
drcmin  0.            ! deltar c
"""
	special1 += """ilep 0				! Use leptons in the final state (not neutrinos)
izdecmode %i					! Z decay mode (1: ee, 2: mumu, 3: tautau)\n """%(Z_mode)
	#special1 += """ilep 1			 ! Use neutrinos in the final state (not leptons) \n """

	special2 = special1
		
# Zbb+jets samples with 2lepton filter, pT > 7 GeV
if run_num>=178396 and run_num<=178407:
	log.info('Recognized Zbb+jets run number. Will generate for run '+str(run_num))
	process = 'zqq'
	run_min=178396
	jets = [(run_num-run_min)%4,1] # Exclusive matching to the last number of partons
	if jets[0]==3: jets[1]=0
	
	Z_mode = int((run_num-run_min)/4) + 1
	if Z_mode==3: #tautau
		event_numbers=[10000000,9500000,5500000,30000000] 

		events_athena = 500
		if jets[0]==1: events_athena=100
		if jets[0]==2: events_athena=10
		if jets[0]==3: events_athena=10
		
	else: #ee and mumu
		event_numbers=[9500000,12000000,16000000,18000000]

		events_athena = 5000
		if jets[0]==1: events_athena=1000
		if jets[0]==2: events_athena=50
		if jets[0]==3: events_athena=10
	
	ncwarm = [2,2,3,3]
	nwarm = [ ncwarm[jets[0]] , event_numbers[jets[0]] ]
	events_alpgen = event_numbers[jets[0]]

	special1 = """mllmin	10					 ! Minimum M_ll
mllmax	60					! Maximum M_ll 
ihvy		5						 ! Select Z+bb
mc      1.4           ! quark c mass
mb      4.7           ! quark b mass
ptbmin  0.            ! quark b pt min
drbmin  0.            ! deltar b
"""
	special1 += """ilep 0				! Use leptons in the final state (not neutrinos)
izdecmode %i					! Z decay mode (1: ee, 2: mumu, 3: tautau)\n """%(Z_mode)
	#special1 += """ilep 1			 ! Use neutrinos in the final state (not leptons) \n """

	special2 = special1

if process=='':
	log.fatal('Unknown run number!	Bailing out!')
	raise RunTimeError('Unknown run number.')


special1 += """xlclu	 0.26					! lambda for alpha_s in CKKW scale evaluation
lpclu 1				 ! nloop for alphas in CKKW scale evaluation
""" 
special2 += """xlclu	 0.26					! lambda for alpha_s in CKKW scale evaluation
lpclu 1				 ! nloop for alphas in CKKW scale evaluation
""" 

if hasattr(runArgs,'cutAthenaEvents'):
	events_athena = events_athena/runArgs.cutAthenaEvents
	log.warning('Reducing number of Athena events by '+str(runArgs.cutAthenaEvents)+' to '+str(events_athena)+' .	Later you should fix this.')
if hasattr(runArgs,'raiseAlpgenEvents'):
	events_alpgen = events_alpgen*runArgs.raiseAlpgenEvents
	log.warning('Increasing number of AlpGen events by '+str(runArgs.raiseAlpgenEvents)+' to '+str(events_alpgen)+' .	Later you should fix this.')

from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
	log.info('Special settings for mode 1:')
	log.info(special1)
if special2 is not None:
	log.info('Special settings for mode 2:')
	log.info(special2)
if special3 is not None:
	log.info('Special settings for pythia:')
	log.info(special3)

inputgridfile = None
if hasattr(runArgs,'inputGenConfFile'):
	log.info('Passing input grid file to AlpGenUtils ' +str(runArgs.inputGenConfFile))
	inputgridfile=runArgs.inputGenConfFile
						

SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2 , special3=special3,inputgridfile=inputgridfile)

if hasattr(runArgs,'inputGeneratorFile'):
	log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'


log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')
include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )


# Additional bit for ME/PS matching
phojf=open('./pythia_card.dat', 'w')
phojinp = """
	!...Matching parameters...
	IEXCFILE=%i
	showerkt=T
	qcut=%i
	imss(21)=24
	imss(22)=24	
"""%(jets[1],15)
# For the moment, hard-coded jet cut (in AlpGenUtils.py)

phojf.write(phojinp)
phojf.close()

# Fill up the evgenConfig
evgenConfig.description = 'alpgen_autoconfig'
evgenConfig.contact = ['zach.marshall@cern.ch','thorsten.kuhl@cern.ch' ]
evgenConfig.keywords = ["SUSY","DY"]
evgenConfig.process = 'auto_alpgen_'+process+'_'+`jets[0]`+'p'
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
#evgenConfig.efficiency = 0.90000
evgenConfig.minevents = events_athena
