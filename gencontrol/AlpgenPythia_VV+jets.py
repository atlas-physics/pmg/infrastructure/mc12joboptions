# Set up for generic alpgen generation
#  This is the main job option to call

import sys

UseHerwig=True
rand_seed=1
ecm = 3500.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenGenericGeneration')

if not hasattr(runArgs,'runNumber'):
    log.fatal('No run number found.  Generation will fail.  No idea how to decode this.')
    raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
    log.error('Random seed not set.  Will use 1.')
else:
    rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
    log.info('No skip events argument found.  Good!')
else:
    log.fatal('Skip events argument found.  No idea how to deal with that yet.  Bailing out.')
    raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
    log.warning('No center of mass energy found.  Will use the default of 7 TeV.')
else:
    ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
special1=None
special2=None
special3=None
nwarm = [ 10 , 10000000 ]

#Optimization results
#===Auto generated code begin===
auto_events_alpgen = {}
auto_events_alpgen[187210] = 140000
auto_events_alpgen[187211] = 610000
auto_events_alpgen[187212] = 32000000
auto_events_alpgen[187213] = 99000000
auto_events_alpgen[187220] = 140000
auto_events_alpgen[187221] = 610000
auto_events_alpgen[187222] = 39000000
auto_events_alpgen[187223] = 98000000
auto_events_alpgen[187230] = 140000
auto_events_alpgen[187231] = 600000
auto_events_alpgen[187232] = 26000000
auto_events_alpgen[187233] = 110000000
auto_events_alpgen[187240] = 140000
auto_events_alpgen[187241] = 620000
auto_events_alpgen[187242] = 21000000
auto_events_alpgen[187243] = 96000000
auto_events_alpgen[187250] = 140000
auto_events_alpgen[187251] = 610000
auto_events_alpgen[187252] = 32000000
auto_events_alpgen[187253] = 86000000
auto_events_alpgen[187260] = 140000
auto_events_alpgen[187261] = 600000
auto_events_alpgen[187262] = 20000000
auto_events_alpgen[187263] = 87000000
auto_events_alpgen[187270] = 300000
auto_events_alpgen[187271] = 1700000
auto_events_alpgen[187272] = 61000000
auto_events_alpgen[187273] = 82000000
auto_events_alpgen[187280] = 290000
auto_events_alpgen[187281] = 1600000
auto_events_alpgen[187282] = 86000000
auto_events_alpgen[187283] = 81000000
auto_events_alpgen[187290] = 290000
auto_events_alpgen[187291] = 1600000
auto_events_alpgen[187292] = 51000000
auto_events_alpgen[187293] = 90000000
auto_events_alpgen[187300] = 300000
auto_events_alpgen[187301] = 1600000
auto_events_alpgen[187302] = 60000000
auto_events_alpgen[187303] = 85000000
auto_events_alpgen[187310] = 380000
auto_events_alpgen[187311] = 2000000
auto_events_alpgen[187312] = 120000000
auto_events_alpgen[187313] = 89000000
auto_events_alpgen[187320] = 390000
auto_events_alpgen[187321] = 1900000
auto_events_alpgen[187322] = 140000000
auto_events_alpgen[187323] = 90000000
auto_events_athena = {}
auto_events_athena[187210] = 5000
auto_events_athena[187211] = 5000
auto_events_athena[187212] = 1000
auto_events_athena[187213] = 500
auto_events_athena[187220] = 5000
auto_events_athena[187221] = 5000
auto_events_athena[187222] = 1000
auto_events_athena[187223] = 200
auto_events_athena[187230] = 5000
auto_events_athena[187231] = 5000
auto_events_athena[187232] = 1000
auto_events_athena[187233] = 500
auto_events_athena[187240] = 5000
auto_events_athena[187241] = 5000
auto_events_athena[187242] = 1000
auto_events_athena[187243] = 500
auto_events_athena[187250] = 5000
auto_events_athena[187251] = 5000
auto_events_athena[187252] = 1000
auto_events_athena[187253] = 200
auto_events_athena[187260] = 5000
auto_events_athena[187261] = 5000
auto_events_athena[187262] = 1000
auto_events_athena[187263] = 1000
auto_events_athena[187270] = 5000
auto_events_athena[187271] = 5000
auto_events_athena[187272] = 1000
auto_events_athena[187273] = 500
auto_events_athena[187280] = 5000
auto_events_athena[187281] = 5000
auto_events_athena[187282] = 1000
auto_events_athena[187283] = 1000
auto_events_athena[187290] = 5000
auto_events_athena[187291] = 5000
auto_events_athena[187292] = 1000
auto_events_athena[187293] = 500
auto_events_athena[187300] = 5000
auto_events_athena[187301] = 5000
auto_events_athena[187302] = 1000
auto_events_athena[187303] = 500
auto_events_athena[187310] = 5000
auto_events_athena[187311] = 5000
auto_events_athena[187312] = 500
auto_events_athena[187313] = 1000
auto_events_athena[187320] = 5000
auto_events_athena[187321] = 5000
auto_events_athena[187322] = 1000
auto_events_athena[187323] = 1000
#===Auto generated code end===



# Run numbers currently reserved: 187201 - 187400
# VV+jets samples P. Tepel
if run_num >= 187210 and run_num <= 187219:
    run_min = 187210
    log.info( 'Recognized WZ incl run number.  Will generate for run ' + str( run_num ) )
    process = 'vbjet'
    jets = [( run_num - run_min ) % 10, 1]  # Exclusive matching to the last number of partons
    if ( run_num - run_min ) <= 5: UseHerwig = False  # Move to Pythia for half the samples

    ncwarm = [2, 2, 3, 3, 4, 4, 4, 4]
    nwarm = [ ncwarm[jets[0]] , auto_events_alpgen[run_num] ]
    events_alpgen = auto_events_alpgen[run_num]
    events_athena = auto_events_athena[run_num]

    if jets[0] == 3: jets[1] = 0  # 3jet incl

    W_mode = 6
    n_Z = 1
    n_W = 1
    ZF_state = 5
    special1 = 'iwdecmode %i          ! W decay mode (1: el, 2: mu, 3: tau, 4:all_lep, 5:had, 6:incl)\n' % ( W_mode )
    special1 += 'nz %i        ! Number of Z Bosons\n' % ( n_Z )
    special1 += 'nw %i        ! Number of W Bosons\n' % ( n_W )
    special1 += 'zfstate %i ! Z final state (1:nu nubar, 2:ll, 3:qqbar(not t), 4:bbar, 5: ffbar)\n' % ( ZF_state )
    special2 = special1

if run_num >= 187220 and run_num <= 187269:
    run_min = 187220
    run_local = run_num - run_min
    ilep = 0
    if run_local <= 9:
        log.info( 'Recognized WZ->lnu+qq run number.  Will generate for run ' + str( run_num ) )
        W_mode = 4
        ZF_state = 3
    if run_local <= 19 and run_local > 9:
        log.info( 'Recognized WZ->qq+ll run number.  Will generate for run ' + str( run_num ) )
        W_mode = 5
        ZF_state = 2
    if run_local <= 29 and run_local > 19:
        log.info( 'Recognized WZ->qq+nunu run number.  Will generate for run ' + str( run_num ) )
        W_mode = 5
        ZF_state = 1
        ilep = 1
    if run_local <= 39 and run_local > 29:
        log.info( 'Recognized WZ->lnu+ll run number.  Will generate for run ' + str( run_num ) )
        W_mode = 4
        ZF_state = 2
    if run_local <= 49 and run_local > 39:
        log.info( 'Recognized WZ->lnu+nunu run number.  Will generate for run ' + str( run_num ) )
        W_mode = 4
        ZF_state = 1
        ilep = 1
    process = 'vbjet'
    jets = [( run_num - run_min ) % 10, 1]  # Exclusive matching to the last number of partons

    if run_local % 10 <= 5: UseHerwig = False  # Move to Pythia for half the samples
    ncwarm = [2, 2, 3, 3, 4, 4, 4, 4]
    nwarm = [ ncwarm[jets[0]] , auto_events_alpgen[run_num] ]
    events_alpgen = auto_events_alpgen[run_num]
    events_athena = auto_events_athena[run_num]

    if jets[0] == 3: jets[1] = 0  # 3jet incl
    # Z_mode = 1(ee) 2(mumu) 3(tautau) 4 nunu
    n_Z = 1
    n_W = 1
    special1 = 'iwdecmode %i          ! W decay mode (1: el, 2: mu, 3: tau, 4:all_lep, 5:had, 6:incl)\n' % ( W_mode )
    special1 += 'nz %i        ! Number of Z Bosons\n' % ( n_Z )
    special1 += 'nw %i        ! Number of W Bosons\n' % ( n_W )
    special1 += 'zfstate %i ! Z final state (1:nu nubar, 2:ll, 3:qqbar(not t), 4:bbar, 5: ffbar)\n' % ( ZF_state )
    special2 = special1

if run_num >= 187270 and run_num <= 187309:
    run_min = 187270
    run_local = run_num - run_min
    ilep = 0
    W_mode = 1
    if run_local <= 9:
        log.info( 'Recognized ZZ->ll+qq run number.  Will generate for run ' + str( run_num ) )
        ZF_state = 23
    if run_local <= 19 and run_local > 9:
        log.info( 'Recognized ZZ->nunu+qq run number.  Will generate for run ' + str( run_num ) )
        ZF_state = 13
        ilep = 1
    if run_local <= 29 and run_local > 19:
        log.info( 'Recognized ZZ->llnunu run number.  Will generate for run ' + str( run_num ) )
        ZF_state = 21
        ilep = 1
    if run_local <= 39 and run_local > 29:
        log.info( 'Recognized ZZ->llll run number.  Will generate for run ' + str( run_num ) )
        ZF_state = 22

    process = 'vbjet'
    jets = [( run_num - run_min ) % 10, 1]  # Exclusive matching to the last number of partons

    if run_local % 10 <= 5: UseHerwig = False  # Move to Pythia for half the samples
    ncwarm = [2, 2, 3, 3, 4, 4, 4, 4]
    nwarm = [ ncwarm[jets[0]] , auto_events_alpgen[run_num] ]
    events_alpgen = auto_events_alpgen[run_num]
    events_athena = auto_events_athena[run_num]

    if jets[0] == 3: jets[1] = 0  # 3jet incl
    # Z_mode = 1(ee) 2(mumu) 3(tautau) 4 nunu
    n_Z = 2
    n_W = 0
    special1 = 'iwdecmode %i          ! W decay mode (1: el, 2: mu, 3: tau, 4:all_lep, 5:had, 6:incl)\n' % ( W_mode )
    special1 += 'nz %i        ! Number of Z Bosons\n' % ( n_Z )
    special1 += 'nw %i        ! Number of W Bosons\n' % ( n_W )
    special1 += 'zfstate %i ! Z final state (1:nu nubar, 2:ll, 3:qqbar(not t), 4:bbar, 5: ffbar)\n' % ( ZF_state )
    special1 += 'cluopt=1\n'
    special1 += 'drphmin=0.6999999\n'
    special1 += 'drphjmin=0.6999999\n'
    special1 += 'etagap=2.5\n'
    special1 += 'irapgap=0\n'
    special1 += 'ptcen=20\n'
    special2 = special1

if run_num >= 187310 and run_num <= 187329:
    run_min = 187310
    run_local = run_num - run_min
    ilep = 0
    W_mode = 11
    ZF_state = 11
    if run_local <= 9:
        log.info( 'Recognized WW->lnu+qq run number.  Will generate for run ' + str( run_num ) )
        W_mode = 45
    if run_local <= 19 and run_local > 9:
        log.info( 'Recognized WW->lnu+lnu run number.  Will generate for run ' + str( run_num ) )
        W_mode = 44
        ilep = 1

    process = 'vbjet'
    jets = [( run_num - run_min ) % 10, 1]  # Exclusive matching to the last number of partons

    if run_local % 10 <= 5: UseHerwig = False  # Move to Pythia for half the samples
    ncwarm = [2, 2, 3, 3, 4, 4, 4, 4]
    nwarm = [ ncwarm[jets[0]] , auto_events_alpgen[run_num] ]
    events_alpgen = auto_events_alpgen[run_num]
    events_athena = auto_events_athena[run_num]

    if jets[0] == 3: jets[1] = 0  # 3jet incl
    # Z_mode = 1(ee) 2(mumu) 3(tautau) 4 nunu
    n_Z = 0
    n_W = 2
    special1 = 'iwdecmode %i          ! W decay mode (1: el, 2: mu, 3: tau, 4:all_lep, 5:had, 6:incl)\n' % ( W_mode )
    special1 += 'nz %i        ! Number of Z Bosons\n' % ( n_Z )
    special1 += 'nw %i        ! Number of W Bosons\n' % ( n_W )
    special1 += 'cluopt=1\n'
    special1 += 'drphmin=0.6999999\n'
    special1 += 'drphjmin=0.6999999\n'
    special1 += 'etagap=2.5\n'
    special1 += 'irapgap=0\n'
    special1 += 'ptcen=20\n'
    special2 = special1

if process=='':
    log.fatal('Unknown run number!  Bailing out!')
    raise RunTimeError('Unknown run number.')

if not UseHerwig:  
        special1 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 
        special2 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 

if hasattr(runArgs,'cutAthenaEvents'):
    events_athena = events_athena/runArgs.cutAthenaEvents
    log.warning('Reducing number of Athena events by '+str(runArgs.cutAthenaEvents)+' to '+str(events_athena)+' .  Later you should fix this.')
if hasattr(runArgs,'raiseAlpgenEvents'):
    events_alpgen = events_alpgen*runArgs.raiseAlpgenEvents
    log.warning('Increasing number of AlpGen events by '+str(runArgs.raiseAlpgenEvents)+' to '+str(events_alpgen)+' .  Later you should fix this.')

from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
    log.info('Special settings for mode 1:')
    log.info(special1)
if special2 is not None:
    log.info('Special settings for mode 2:')
    log.info(special2)
if special3 is not None:
    log.info('Special settings for pythia:')
    log.info(special3)

inputgridfile = None
if hasattr(runArgs,'inputGenConfFile'):
        log.info('Passing input grid file to AlpGenUtils ' +str(runArgs.inputGenConfFile))
        inputgridfile=runArgs.inputGenConfFile
            

SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2 , special3=special3,inputgridfile=inputgridfile)

if hasattr(runArgs,'inputGeneratorFile'):
    log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'

if UseHerwig:
    log.info('Passing events on to Herwig for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenHerwig.py') 
else:
    log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')
    include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
    include ( "MC12JobOptions/Pythia_Tauola.py" )
    include ( "MC12JobOptions/Pythia_Photos.py" )


    # Additional bit for ME/PS matching
    phojf=open('./pythia_card.dat', 'w')
    phojinp = """
      !...Matching parameters...
      IEXCFILE=%i
      showerkt=T
      qcut=%i
      imss(21)=24
      imss(22)=24  
    """%(jets[1],15)
    # For the moment, hard-coded jet cut (in AlpGenUtils.py)

    phojf.write(phojinp)
    phojf.close()

# Fill up the evgenConfig
evgenConfig.description = 'alpgen_autoconfig'
evgenConfig.contact = ['zach.marshall@cern.ch','thorsten.kuhl@cern.ch' ]
evgenConfig.process = 'auto_alpgen_'+process+'_'+`jets[0]`+'p'
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
#evgenConfig.efficiency = 0.90000
evgenConfig.minevents = events_athena
