from MadGraphControl.MadGraphUtils import *
     #from MadGraphControl.SetupMadGraphEventsForSM import setupMadGraphEventsForSM

     #mglog = Logging.logging.getLogger('MadGraphUtils')

fcard = open('proc_card_mg5.dat','w')

doPSVars=False

	#Default matching parameters
ickkw=1
xqcut = 20
qcut = 30

maxjetflavor=4

mllcut=-999.9
dRllcut=0.4
dRjlcut=0.4
pTlcut=-999.9
etalcut=2.5

cut_decays='T'

minevents=5000
nevents=40000

gridpackMode=False
nJobs=1
cluster_type=None
cluster_queue=None

	#DSID lists (extensions include systematics samples)

tZ_stchan_MGDecay=[110491]
tZ_Wtchan_MGDecay=[110492]


	##Scale variation lists
systDict={}

	#systDict['scalefactUP']   = []
	#systDict['scalefactDOWN'] = []
	#systDict['alpsfactUP']    = []
	#systDict['alpsfactDOWN']  = []
	#systDict['MoreFSR']       = []
	#systDict['LessFSR']       = []
	#systDict['xqcutUP']       = []
	#systDict['xqcutDOWN']     = []

if runArgs.runNumber in tZ_stchan_MGDecay:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define l = l+ l-
    define vlall = vl vl~
    define all = j l vlall
    define bbbar = b b~
    define ttbar = t t~
    define w = w- w+
    generate p p > ttbar z j bbbar, ttbar > l vlall bbbar, z > l l @0
    output -f
    """)
    fcard.close()

    gridpackMode=True
    nJobs=20
    cluster_type='pbs'
    cluster_queue='shatlas'

    iexcfile=-1
    ickkw=1
    maxjetflavor=5
    cut_decays='F'

    mllcut=5.0
    dRllcut=0.0
    pTlcut=1.0
    etalcut=5.0
    dRjlcut=0.0


    #name="tZ_Zll_5flav_tchan_Np0Np1Np2_nodecay"
    #name="tZ_tchan_Zll_PythiaDecays"
    name="tZ_stchan_Zll_MadgraphDecays"


elif runArgs.runNumber in tZ_Wtchan_MGDecay:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define l = l+ l-
    define vlall = vl vl~
    define all = j l vlall
    define bbbar = b b~
    define ttbar = t t~
    define w = w- w+
    generate p p > ttbar z w bbbar, ttbar > l vlall bbbar, z > l l,w > l vlall @0
    output -f
    """)
    fcard.close()

    gridpackMode=True
    nJobs=20
    cluster_type='pbs'
    cluster_queue='shatlas'

    iexcfile=-1
    ickkw=1
    maxjetflavor=5
    cut_decays='F'

    mllcut=5.0
    dRllcut=0.0
    pTlcut=1.0
    etalcut=5.0
    dRjlcut=0.0


    #name="tZ_Zll_5flav_Wtchan_Np0Np1Np2_nodecay"
    #name="tZ_Wtchan_Zll_PythiaDecays"
    name="tZ_Wtchan_Zll_MadgraphDecays"




else:
    print "ERROR: No undefined RunNumber was provided."


print "ickkw = %i"%ickkw


beamEnergy=4000
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#Default Scales
isFixedRenScale="F"
isFixedFacScale="F"

#Scale = m(Z)
#scale=91.1880
#Scale = m(top)
#scale= 172.5
#Scale = ( m(top)+m(top)+m(Z) ) / 2
scale=218.594

renScale =scale
facScale1=scale
facScale2=scale

renUD=1.0
facUD=1.0

systName=None

for k,v in systDict.iteritems():
    if runArgs.runNumber in v:
        systName=k
        name=name+"_"+k


if systName:
    print "\nSYSTEMATIC VARIATION:",systName,"\n"

    scaleUP=2.0
    scaleDOWN=0.5

    alpsUP=2.0
    alpsDOWN=0.5

    if systName=='xqcutUP':
        xqcut=25
    if systName=='xqcutDOWN':
        xqcut=15


    if systName=='MoreFSR':
        doPSVars=True
        PSVar="MoreFSR"
    if systName=='LessFSR':
        doPSVars=True
        PSVar="LessFSR"





#lhapdfid=10042
lhapdfid=-1
pdflabel='cteq6l1'

# Grab the run card and move it into place
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' = ickkw' in line:
            newcard.write('%i        = ickkw            ! 0 no matching, 1 MLM, 2 CKKW matching \n'%(ickkw))
        elif ickkw==1 and ' xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif ickkw==1 and ' xptj ' in line:
            newcard.write('   %f      = xptj ! minimum pt for at least one jet \n'%(xqcut))
        elif ickkw==1 and ' = maxjetflavor' in line:
           newcard.write(' %i = maxjetflavor \n'%(maxjetflavor))
        elif ' = nevents' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(nevents))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif ' = cut_decays' in line:
            newcard.write(' %s = cut_decays \n'%(cut_decays))
        elif ' = mmll ' in line and mllcut>0:
            newcard.write(' %f = mmll    ! min invariant mass of l+l- (same flavour) lepton pair \n'%(mllcut))
        elif ' = drll ' in line and dRllcut!=0.4:
            newcard.write(' %f = drll    ! min distance between leptons \n'%(dRllcut))
        elif ' = ptl ' in line and pTlcut>0:
            newcard.write(' %f = ptl       ! minimum pt for the charged leptons \n'%(pTlcut))
        elif ' = etal ' in line and etalcut!=2.5:
            newcard.write(' %f = etal    ! max rap for the charged leptons \n'%(etalcut))
        elif ' = drjl ' in line and dRjlcut!=0.4:
            newcard.write(' %f = drjl    ! min distance between jet and lepton \n'%(dRjlcut))
        elif lhapdfid<0 and '= pdlabel     ! PDF set' in line:
            newcard.write('   \'%s\'    = pdlabel     ! PDF set\n'%(pdflabel))
        elif lhapdfid>0 and '= pdlabel     ! PDF set' in line:
            newcard.write('   \'lhapdf\'    = pdlabel     ! PDF set\n')
            newcard.write('   %i       = lhaid       ! PDF number used ONLY for LHAPDF\n'%(lhapdfid))
        elif systName=='scalefactUP' and ' scalefact ' in line:
            newcard.write(' %f        = scalefact        ! scale factor for event-by-event scales \n'%(scaleUP))
        elif systName=='scalefactDOWN' and ' scalefact ' in line:
            newcard.write(' %f        = scalefact        ! scale factor for event-by-event scales \n'%(scaleDOWN))
        elif systName=='alpsfactUP' and ' alpsfact ' in line:
            newcard.write(' %f        = alpsfact         ! scale factor for QCD emission vx \n'%(alpsUP))
        elif systName=='alpsfactDOWN' and ' alpsfact ' in line:
            newcard.write(' %f        = alpsfact         ! scale factor for QCD emission vx \n'%(alpsDOWN))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()
    


stringy=''

print "proc_card_mg5.dat:"
procCard = subprocess.Popen(['cat','proc_card_mg5.dat'])
procCard.wait()

print "run_card.dat:"
runCard = subprocess.Popen(['cat','run_card.dat'])
runCard.wait()


runName='Test'




if not hasattr(runArgs, "inputGenConfFile"):
    #### Generate process ####
    if gridpackMode:
        print 'Generating process for gridpack'
    else:
        print 'Generating process and events'

    process_dir = new_process()
    generate(run_card_loc='run_card.dat',param_card_loc=None,mode=1,njobs=nJobs,proc_dir=process_dir,run_name=runName,grid_pack=gridpackMode,cluster_type=cluster_type,cluster_queue=cluster_queue)

else:
    seed=runArgs.randomSeed

    if gridpackMode and hasattr(runArgs, "inputGenConfFile"):
        #### Generate events from gridpack ####
        print 'Generating events using gripack mode'

        gridpack_dir='madevent/'
        generate_from_gridpack(run_name=runName,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=seed)
        process_dir=gridpack_dir



if not gridpackMode or (gridpackMode and hasattr(runArgs, "inputGenConfFile")):

    #### Pass to Pythia ####
    print 'Pass generated events to Pythia'
    stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)
    stringyConf = 'madgraph.'+str(runArgs.runNumber)+'.'+str(name)

    skip_events=0
    if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
    arrange_output(run_name=runName,proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)


    #--------------------------------------------------------------
    # General MC12 configuration
    #--------------------------------------------------------------
    include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
    topAlg.Pythia.PythiaCommand = ["pyinit user madgraph"]

    #--------------------------------------------------------------
    topAlg.Pythia.PythiaCommand +=  [
                    "pystat 1 3 4 5",
                    "pyinit dumpr 1 5",
                    "pyinit pylistf 1",
                    ]




    # Change Pythia parameters coherently with alpsfact in MG5
    if systName=='alpsfactUP' or systName=='alpsfactDOWN':

        print "\nChanging Pythia parameters to reflect change in alpsfact in MadGraph"
        if systName=='alpsfactUP':
            #PythiaParameterList = [ "PARP(64)=%f"%(1.0*(alpsUP*alpsUP)), "PARP(72)=%f"%(0.25*(1.0/alpsUP))]
            PythiaParameterList = [ "PARP(64)=%f"%(1.0*(alpsUP*alpsUP))]
        if systName=='alpsfactDOWN':
            #PythiaParameterList = [ "PARP(64)=%f"%(1.0*(alpsDOWN*alpsDOWN)), "PARP(72)=%f"%(0.25*(1.0/alpsDOWN))]
            PythiaParameterList = [ "PARP(64)=%f"%(1.0*(alpsDOWN*alpsDOWN))]

        print "New parameter values: ",PythiaParameterList
        topAlg.Pythia.PygiveCommand += PythiaParameterList




    # Parton shower variations
    if doPSVars:
        print "\nIncluding additional ISR/FSR/PS variation:",PSVar
        #include ( str("MC12JobOptions/Pythia_CTEQ6L1_"+PSVar+"_Common.py") )
        if PSVar=='MoreFSR':
            PythiaParameterList = [ "PARP(72)=0.7905", "PARJ(82)=0.5" ]
        elif PSVar=='LessFSR':
            PythiaParameterList = [ "PARP(72)=0.2635", "PARJ(82)=1.66" ]
        print "New parameter values: ",PythiaParameterList
        topAlg.Pythia.PygiveCommand += PythiaParameterList






    include ( "MC12JobOptions/Tauola_Fragment.py" )
    include ( "MC12JobOptions/Photos_Fragment.py" )


    evgenConfig.generators += ["MadGraph", "Pythia"]
    evgenConfig.description = 'MadGraph_'+str(name)
    evgenConfig.keywords += ["SM", "singletop", "Z"]
    evgenConfig.contact = ["Josh McFayden", "j.mcfayden@sheffield.ac.uk"]
    evgenConfig.inputfilecheck = stringy
    #evgenConfig.inputconfcheck = stringyConf
    evgenConfig.minevents = 5000
    runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
    #runArgs.inputGenConfFile=stringy+'.TXT.mc12_v1._00001.tar.gz'



    # Additional bit for ME/PS matching
    if ickkw==1:
        if iexcfile>=0:
            #Generating separate jet multiplicity samples
            phojf=open('./pythia_card.dat', 'w')
            phojinp = """
            !...Matching parameters...
            IEXCFILE=%i
            showerkt=T
            qcut=%i
            imss(21)=24
            imss(22)=24
            """%(iexcfile,qcut)
        else:
            #Not separating jet multiplicity samples
            phojf=open('./pythia_card.dat', 'w')
            phojinp = """
            !...Matching parameters...
            showerkt=T
            qcut=%i
            imss(21)=24
            imss(22)=24
            """%(qcut)

        phojf.write(phojinp)
        phojf.close()
