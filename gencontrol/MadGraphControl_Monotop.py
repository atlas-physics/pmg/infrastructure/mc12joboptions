# -*- coding: utf-8 -*-
############################################################
# Generation of Monotop samples with MadGraph5+Pythia8
# Timothee Theveneaux-Pelzer tpelzer@cern.ch
# Monotop models are described in Phys.Rev. D84 (2011) 074025 - arXiv:1106.6199 [hep-ph]
############################################################

### Runs
#    * 110148-110153: Monotop S1_right - right-handed couplings - missing ET particle mass: 0,20,40,60,80,100 GeV - resonance mass: 500 GeV - leptonic top decay
#    * 110154-110159: Monotop S1_right - right-handed couplings - missing ET particle mass: 0,20,40,60,80,100 GeV - resonance mass: 500 GeV - hadronic top decay
#    * 110160-110169: Monotop S4_right - right-handed couplings - missing ET particle mass: 0,25,50,75,100,125,150,200,250,300 GeV - leptonic top decay
#    * 110170-110179: Monotop S4_right - right-handed couplings - missing ET particle mass: 0,25,50,75,100,125,150,200,250,300 GeV - hadronic top decay
###  Extended production:
#    * 110180-110186: Monotop S4_right - right-handed couplings - missing ET particle mass: 400,500,600,700,800,900,1000 GeV - leptonic top decay
#    * 110187-110193: Monotop S4_right - right-handed couplings - missing ET particle mass: 400,500,600,700,800,900,1000 GeV - hadronic top decay
#    * 110194: Monotop S1_right - right-handed couplings - missing ET particle mass: 100 GeV - resonance mass: 500 GeV - effective coupling 0.5 - leptonic top decay
#    * 110195: Monotop S1_right - right-handed couplings - missing ET particle mass: 100 GeV - resonance mass: 500 GeV - effective coupling 1.0 - leptonic top decay
#    * 110196: Monotop S1_right - right-handed couplings - missing ET particle mass: 100 GeV - resonance mass: 500 GeV - effective coupling 0.5 - hadronic top decay
#    * 110197: Monotop S1_right - right-handed couplings - missing ET particle mass: 100 GeV - resonance mass: 500 GeV - effective coupling 1.0 - hadronic top decay
#    * 110867: MadgrapPythia,Monotop,resonance model, 1TeV for DC14
#    * 110868: MadgrapPythia,Monotop,resonance model, 1.5TeV for DC14
#    * 110869: MadgrapPythia,Monotop,non-resonance model, 1.5TeV for DC14

from MadGraphControl.MadGraphUtils import *
from MadGraphControl.SetupMadGraphEventsForSM import setupMadGraphEventsForSM

### Set generation card
if runArgs.runNumber>=110148 and runArgs.runNumber<=110153: # S1_right leptonic
    procname='Monotop'
    modelname='S1_right'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model MonoTops_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t fmet MT1=0 MT2=0 MT3=2 MT4=0, (t > j W+, W+ > l+ vl) @1
    add process p p > t~ fmet MT1=0 MT2=0 MT3=2 MT4=0, (t~ > j W-, W- > l- vl~) @2
    output -f
    """)
    fcard.close()
elif runArgs.runNumber>=110867 and runArgs.runNumber<=110868: # S1_right leptonic
    procname='Monotop'
    modelname='S1_right'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model MonoTops_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t fmet MT1=0 MT2=0 MT3=2 MT4=0, (t > j W+, W+ > l+ vl) @1
    add process p p > t~ fmet MT1=0 MT2=0 MT3=2 MT4=0, (t~ > j W-, W- > l- vl~) @2
    output -f
    """)
    fcard.close()
elif runArgs.runNumber>=110154 and runArgs.runNumber<=110159: # S1_right hadronic
    procname='Monotop'
    modelname='S1_right'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model MonoTops_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t fmet MT1=0 MT2=0 MT3=2 MT4=0, (t > j W+, W+ > j j) @1
    add process p p > t~ fmet MT1=0 MT2=0 MT3=2 MT4=0, (t~ > j W-, W- > j j) @2
    output -f
    """)
    fcard.close()
elif runArgs.runNumber>=110160 and runArgs.runNumber<=110169: # S4_right leptonic
    procname='Monotop'
    modelname='S4_right'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model MonoTops_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t vmet MT1=0 MT2=2 MT3=0 MT4=0, (t > j W+, W+ > l+ vl) @1
    add process p p > t~ vmet MT1=0 MT2=2 MT3=0 MT4=0, (t~ > j W-, W- > l- vl~) @2
    output -f
    """)
    fcard.close()
elif runArgs.runNumber>=110170 and runArgs.runNumber<=110179: # S4_right hadronic
    procname='Monotop'
    modelname='S4_right'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model MonoTops_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t vmet MT1=0 MT2=2 MT3=0 MT4=0, (t > j W+, W+ > j j) @1
    add process p p > t~ vmet MT1=0 MT2=2 MT3=0 MT4=0, (t~ > j W-, W- > j j) @2
    output -f
    """)
    fcard.close()
elif runArgs.runNumber>=110180 and runArgs.runNumber<=110186: # S4_right leptonic
    procname='Monotop'
    modelname='S4_right'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model MonoTops_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t vmet MT1=0 MT2=2 MT3=0 MT4=0, (t > j W+, W+ > l+ vl) @1
    add process p p > t~ vmet MT1=0 MT2=2 MT3=0 MT4=0, (t~ > j W-, W- > l- vl~) @2
    output -f
    """)
    fcard.close()
elif runArgs.runNumber==110869: # S4_right leptonic
    procname='Monotop'
    modelname='S4_right'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model MonoTops_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t vmet MT1=0 MT2=2 MT3=0 MT4=0, (t > j W+, W+ > l+ vl) @1
    add process p p > t~ vmet MT1=0 MT2=2 MT3=0 MT4=0, (t~ > j W-, W- > l- vl~) @2
    output -f
    """)
    fcard.close()
elif runArgs.runNumber>=110187 and runArgs.runNumber<=110193: # S4_right hadronic
    procname='Monotop'
    modelname='S4_right'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model MonoTops_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t vmet MT1=0 MT2=2 MT3=0 MT4=0, (t > j W+, W+ > j j) @1
    add process p p > t~ vmet MT1=0 MT2=2 MT3=0 MT4=0, (t~ > j W-, W- > j j) @2
    output -f
    """)
    fcard.close()
elif runArgs.runNumber>=110194 and runArgs.runNumber<=110195: # S1_right leptonic
    procname='Monotop'
    modelname='S1_right'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model MonoTops_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t fmet MT1=0 MT2=0 MT3=2 MT4=0, (t > j W+, W+ > l+ vl) @1
    add process p p > t~ fmet MT1=0 MT2=0 MT3=2 MT4=0, (t~ > j W-, W- > l- vl~) @2
    output -f
    """)
    fcard.close()
elif runArgs.runNumber>=110196 and runArgs.runNumber<=110197: # S1_right hadronic
    procname='Monotop'
    modelname='S1_right'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model MonoTops_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t fmet MT1=0 MT2=0 MT3=2 MT4=0, (t > j W+, W+ > j j) @1
    add process p p > t~ fmet MT1=0 MT2=0 MT3=2 MT4=0, (t~ > j W-, W- > j j) @2
    output -f
    """)
    fcard.close()
else:
    print 'ERROR: run number not in run range'
    
process_dir = new_process()

print 'INFO: Events will be generated for model '+modelname

### Modified parameters w.r.t default run_card.SM.dat
nevents = 6000
ptl = 0
xqcut = 0
bwcutoff = 25

### Choice of PDF set 
doMSTW08PDFSet = True 
#doMSTW08PDFSet = False
pdfSet='lhapdf' #default set to 'cteq6l1'
pdfNumber=21000 # for lhaid 

###################
# Masses and width
###################

# default masses
###################
# invisible particles - default is 100 GeV (arbitrary value)
MSM = 100
MVM = 100
MFM = 100
# resonances - default is 1 TeV (arbitrary value)
MSC = 1000
MVC = 1000

# default widths
###################
# invisible particles are considered as stable in the choosen mass range, so their width should remain 0 GeV
WSM = 0
WVM = 0
WFM = 0
# the widths for the resonances should be changed accordingly to their appropriate value - the 10 GeV default is a dummy value
WSC = 10
WVC = 10

# default coupling value
###################
coupl = 1.000000e-01

# in the following, only the relevant masses and widths are changed, the other ones remain at their default values
  
if runArgs.runNumber>=110148 and runArgs.runNumber<=110153: # S1_right leptonic - resonance of 500 GeV
  MFM = 0+20*(runArgs.runNumber%110148) # the 6 samples are by steps of 20 GeV
  MSC = 500 # scalar resonance of 500 GeV
  # setting the width of the resonance
  if   runArgs.runNumber==110148:
     WSC = 3.492 # invisible particle mass = 0 GeV
  elif runArgs.runNumber==110149:
     WSC = 3.491 # invisible particle mass = 20 GeV
  elif runArgs.runNumber==110150:
     WSC = 3.487 # invisible particle mass = 40 GeV
  elif runArgs.runNumber==110151:
     WSC = 3.481 # invisible particle mass = 60 GeV
  elif runArgs.runNumber==110152:
     WSC = 3.472 # invisible particle mass = 80 GeV
  elif runArgs.runNumber==110153:
     WSC = 3.461 # invisible particle mass = 100 GeV
  else:
     print 'ERROR: run number not in run range'
  print 'INFO: Using value MFM='+str(MFM)
  print 'INFO: Using value MSC='+str(MSC)
  print 'INFO: Using value WSC='+str(WSC)
  
elif runArgs.runNumber>=110154 and runArgs.runNumber<=110159: # S1_right hadronic - resonance of 500 GeV
  MFM = 0+20*(runArgs.runNumber%110154) # the 6 samples are by steps of 20 GeV
  MSC = 500 # scalar resonance of 500 GeV
  # setting the width of the resonance
  if   runArgs.runNumber==110154:
     WSC = 3.492 # invisible particle mass = 0 GeV
  elif runArgs.runNumber==110155:
     WSC = 3.491 # invisible particle mass = 20 GeV
  elif runArgs.runNumber==110156:
     WSC = 3.487 # invisible particle mass = 40 GeV
  elif runArgs.runNumber==110157:
     WSC = 3.481 # invisible particle mass = 60 GeV
  elif runArgs.runNumber==110158:
     WSC = 3.472 # invisible particle mass = 80 GeV
  elif runArgs.runNumber==110159:
     WSC = 3.461 # invisible particle mass = 100 GeV
  else:
    print 'ERROR: run number not in run range'
  print 'INFO: Using value MFM='+str(MFM)
  print 'INFO: Using value MSC='+str(MSC)
  print 'INFO: Using value WSC='+str(WSC)
  
elif runArgs.runNumber>=110160 and runArgs.runNumber<=110169: # S4_right leptonic
  if runArgs.runNumber<110166:
    MVM = 0+25*(runArgs.runNumber%110160) # the first 7 samples are by steps of 25 GeV
  else:
    MVM = 150+50*(runArgs.runNumber%110166) # the remaining samples are by steps of 50 GeV
  print 'INFO: Using value MVM='+str(MVM)
  
elif runArgs.runNumber>=110170 and runArgs.runNumber<=110179: # S4_right hadronic
  if runArgs.runNumber<110176:
    MVM = 0+25*(runArgs.runNumber%110170) # the first 7 samples are by steps of 25 GeV
  else:
    MVM = 150+50*(runArgs.runNumber%110176) # the remaining samples are by steps of 50 GeV
  print 'INFO: Using value MVM='+str(MVM)
  
elif runArgs.runNumber>=110180 and runArgs.runNumber<=110186: # S4_right leptonic - extended production
  MVM = 400+100*(runArgs.runNumber%110180) # the samples are by steps of 100 GeV
  print 'INFO: Using value MVM='+str(MVM)
  
elif runArgs.runNumber>=110187 and runArgs.runNumber<=110193: # S4_right hadronic - extended production
  MVM = 400+100*(runArgs.runNumber%110187) # the samples are by steps of 100 GeV
  print 'INFO: Using value MVM='+str(MVM)
  
elif runArgs.runNumber==110194 or runArgs.runNumber==110196: # S1_right - resonance of 500 GeV - extended production
  MFM = 100 # only 100 GeV mass point for the invisible particle
  MSC = 500 # scalar resonance of 500 GeV
  WSC = 21.63 # total width for effective coupling = 0.5
  coupl = 2.500000e-01 # effective coupling = 0.5
  print 'INFO: Using value MFM='+str(MFM)
  print 'INFO: Using value MSC='+str(MSC)
  print 'INFO: Using value WSC='+str(WSC)
  print 'INFO: Using coupling values a=b='+str(coupl)

elif runArgs.runNumber==110195 or runArgs.runNumber==110197: # S1_right leptonic - resonance of 500 GeV - extended production
  MFM = 100 # only 100 GeV mass point for the invisible particle
  MSC = 500 # scalar resonance of 500 GeV
  WSC = 86.52 # total width for effective coupling = 1.
  coupl = 5.000000e-01 # effective coupling = 1.
  print 'INFO: Using value MFM='+str(MFM)
  print 'INFO: Using value MSC='+str(MSC)
  print 'INFO: Using value WSC='+str(WSC)
  print 'INFO: Using coupling values a=b='+str(coupl)
  

elif runArgs.runNumber>= 110867 and runArgs.runNumber<=110868: # S1_right leptonic - resonance of 1000 and 1500 GeV - DC 14 studies
  MFM = 100 # invisible particle mass = 100 GeV
  if   runArgs.runNumber==110867:
     WSC = 7.099 # setting the width of the resonance
     MSC = 1000 # scalar resonance of 1000 GeV
  elif runArgs.runNumber==110868:
     WSC = 10.7 # setting the width of the resonance
     MSC = 1500 # scalar resonance of 1500 GeV
  else:
     print 'ERROR: run number not in run range'
  print 'INFO: Using value MFM='+str(MFM)
  print 'INFO: Using value MSC='+str(MSC)
  print 'INFO: Using value WSC='+str(WSC)
  
elif runArgs.runNumber==110869: # S4_right leptonic - extended production - DC14 studies
  MVM = 1500 # mass 1.5 TeV
  print 'INFO: Using value MVM='+str(MVM)

else:
  print 'ERROR: run number not in run range'

# Grab the run card and move it into place
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run_card.SM.dat'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' = ickkw ' in line:
            newcard.write('%i        = ickkw            ! 0 no matching, 1 MLM, 2 CKKW matching \n'%(0))
        elif ' xqcut  ' in line:
            newcard.write('%i   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(nevents))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' ptl ' in line:
            newcard.write('   %i      = ptl ! minimum pt for the charged leptons \n'%(ptl))
        elif ' etaj ' in line:
            newcard.write(' 1d2  = etaj    ! max rap for the jets \n')
        elif ' etab ' in line:
            newcard.write(' 1d2  = etab    ! max rap for the b \n')
        elif ' etal ' in line:
            newcard.write(' 1d2  = etal    ! max rap for the charged leptons \n')
        elif ' drll ' in line:
            newcard.write(' 0.  = drll    ! min distance between leptons \n')
        elif ' drjl ' in line:
            newcard.write(' 0.  = drjl    ! min distance between jet and lepton \n')
        elif ' ebeam1 ' in line:
            newcard.write('     %i     = ebeam1  ! beam 1 energy in GeV \n'%(runArgs.ecmEnergy/2))
        elif ' ebeam2 ' in line:
            newcard.write('     %i     = ebeam2  ! beam 2 energy in GeV \n'%(runArgs.ecmEnergy/2))
        elif ' bwcutoff' in line:
            newcard.write('     %i     = bwcutoff \n'%(bwcutoff))
        elif ' = pdlabel ' in line and doMSTW08PDFSet:
            newcard.write('   \'%s\'      = pdlabel ! PDF set\n   %d      = lhaid       ! PDF number used ONLY for LHAPDF\n' % (str(pdfSet), int(pdfNumber)))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()
            
# Grab parameter card and move it into place
paramcard = subprocess.Popen(['get_files','-data','param_card.Monotop.dat'])
paramcard.wait()
if not os.access('param_card.Monotop.dat',os.R_OK):
    print 'ERROR: Could not get param_card.Monotop.dat'
else:
    oldcard = open('param_card.Monotop.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if ' MSM' in line:
            newcard.write('9000001 %f # MSM \n'%(MSM))
        elif ' MVM' in line:
            newcard.write('9000002 %f # MVM \n'%(MVM))
        elif ' MFM' in line:
            newcard.write('9000003 %f # MFM \n'%(MFM))
        elif ' MSC' in line:
            newcard.write('9000004 %f # MSC \n'%(MSC))
        elif ' MVC' in line:
            newcard.write('9000005 %f # MVC \n'%(MVC))
        elif ' WSM' in line:
            newcard.write('DECAY 9000001 %f # WSM \n'%(WSM))
        elif ' WVM' in line:
            newcard.write('DECAY 9000002 %f # WVM \n'%(WVM))
        elif ' WFM' in line:
            newcard.write('DECAY 9000003 %f # WFM \n'%(WFM))
        elif ' WSC' in line:
            newcard.write('DECAY 9000004 %f # WSC \n'%(WSC))
        elif ' WVC' in line:
            newcard.write('DECAY 9000005 %f # WVC \n'%(WVC))
        else:#modified couplings for S1R model extended production
            if runArgs.runNumber>=110194 or runArgs.runNumber<=110197:
                if ' A12S3' in line:
                    newcard.write('    3 %s # A12S3 \n'%(str(coupl)))
                elif 'AQS1x2' in line:
                    newcard.write('    1   2 %s # AQS1x2 \n'%(str(coupl)))
                elif 'AQS2x1' in line:
                    newcard.write('    2   1 -%s # AQS2x1 \n'%(str(coupl)))
                elif 'B12S3' in line:
                    newcard.write('    3 %s # B12S3 \n'%(str(coupl)))
                elif 'BQS1x2' in line:
                    newcard.write('    1   2 %s # BQS1x2 \n'%(str(coupl)))
                elif 'BQS2x1' in line:
                    newcard.write('    2   1 -%s # BQS2x1 \n'%(str(coupl)))
                else:
                    newcard.write(line)
            else:
                newcard.write(line)
    oldcard.close()
    newcard.close()

generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

if  modelname=="S1_right":
  if runArgs.runNumber==110194 or runArgs.runNumber==110196:
    stringy = 'group.phys-gener.MadGraph.'+str(runArgs.runNumber)+'.'+procname+'_'+modelname+'_aR0.5_Mres'+str(MSC)+'_Mmet'+str(MFM)+'.TXT.mc12_v1'
  elif runArgs.runNumber==110195 or runArgs.runNumber==110197:
    stringy = 'group.phys-gener.MadGraph.'+str(runArgs.runNumber)+'.'+procname+'_'+modelname+'_aR1.0_Mres'+str(MSC)+'_Mmet'+str(MFM)+'.TXT.mc12_v1'
  else:
    stringy = 'group.phys-gener.MadGraph.'+str(runArgs.runNumber)+'.'+procname+'_'+modelname+'_Mres'+str(MSC)+'_Mmet'+str(MFM)+'.TXT.mc12_v1'
elif modelname=="S4_right":
  stringy = 'group.phys-gener.MadGraph.'+str(runArgs.runNumber)+'.'+procname+'_'+modelname+'_Mmet'+str(MVM)+'.TXT.mc12_v1'
else:
  print 'ERROR: model '+modelname+' is unknown'

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)



### MC 12 configuration with PYTHIA 8 

runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

evgenConfig.description = "MadGraph5+Pythia8 for monotop"

evgenConfig.keywords = ["monotop", "top", "single-top", "exotic"]

evgenConfig.inputfilecheck = "Monotop"

evgenConfig.generators = ["MadGraph", "Pythia8"]

if doMSTW08PDFSet:
    include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
else:
    include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

include("MC12JobOptions/Pythia8_LHEF.py")

