from MadGraphControl.MadGraphUtils import *

fcard = open('proc_card_mg5.dat','w')

#include fixed version
ttbargamma_noAllHad = [164439, 177998, 202332, 202333, 202334, 202335, 202336, 202337]
ttbargamma_AllHad = [174382]

# add all numbers (including systematics samples) that are of the "fixed" style
ttbargamma_fixed = [177998, 202332, 202333, 202334, 202335, 202336, 202337]

#Scale variation lists
systDict={}

systDict['scalefactUP']   = [202332]
systDict['scalefactDOWN'] = [202333]
systDict['alpsfactUP']    = [202334]
systDict['alpsfactDOWN']  = [202335]
systDict['MoreFSR']       = [202336]
systDict['LessFSR']       = [202337]

if runArgs.runNumber in ttbargamma_noAllHad:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t t~ > l+ vl b b~ l- vl~ a @1
    add process p p > t t~ > l+ vl b b~ j j a @2
    add process p p > t t~ > l- vl~ b b~ j j a @3
    output -f
    """)
    fcard.close()
    name="ttbargamma_Lep_Pt80"
elif runArgs.runNumber in ttbargamma_AllHad:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t t~ > b b~ a u d~ u~ d @1
    add process p p > t t~ > b b~ a u d~ c~ s @2
    add process p p > t t~ > b b~ a c s~ u~ d @3
    add process p p > t t~ > b b~ a c s~ c~ s @4
    output -f
    """)
    fcard.close()
    name="ttbargamma_allHad_Pt80"
else: 
    raise RunTimeError("No data set ID found.")  

beamEnergy=4000
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RunTimeError("No center of mass energy found.")

systName = None
for k,v in systDict.iteritems():
    if runArgs.runNumber in v:
        systName = k
        name = name + "_" + k
print "\nSYSTEMATIC VARIATION:", systName, "\n"

scaleUP = 2.0
scaleDOWN = 0.5

alpsUP = 2.0
alpsDOWN  =0.5

doPSVars = False

if systName == 'MoreFSR':
    doPSVars = True
    PSVar = "MoreFSR"
if systName == 'LessFSR':
    doPSVars = True
    PSVar = "LessFSR"

#cut values
draa = 0.1
draj = 0.1
drab = 0
dral = 0.1

drll = 0
drjl = 0
drbl = 0

if runArgs.runNumber in ttbargamma_fixed:
    pta = 15
else:
    pta = 80
ptl = 0

etaa = 5
etal = 5

# Grab the run card and move it into place
#runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
#runcard.wait()
if os.access(os.environ['MADPATH']+'/Template/Cards/run_card.dat',os.R_OK):
    shutil.copy(os.environ['MADPATH']+'/Template/Cards/run_card.dat','run_card.SM.dat')
elif os.access(os.environ['MADPATH']+'/Template/LO/Cards/run_card.dat',os.R_OK):
    shutil.copy(os.environ['MADPATH']+'/Template/LO/Cards/run_card.dat','run_card.SM.dat')
else:
    raise RuntimeError('Cannot find Template run_card.dat!')    

if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' = xqcut ' in line:
            newcard.write('   %i      = xqcut   ! minimum kt jet measure between partons \n'%(0))
        elif ' = ickkw ' in line:
            newcard.write('   %i      = ickkw   ! 0 no matching, 1 MLM, 2 CKKW matching\n'%(0))
        elif ' = nevents ' in line:
            if runArgs.runNumber in ttbargamma_fixed:
                numEvents = 150000
            else:
                numEvents = 20000
            newcard.write('   %i      = nevents ! Number of unweighted events requested \n'%(numEvents))
        elif 'pdlabel' in line:
            newcard.write(' ''cteq6l1''    = pdlabel     ! PDF set')
        elif ' = iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' = ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' = ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif ' = pta ' in line:
            newcard.write('   %s      = pta     ! minimum pt for the photons\n'%(pta))
        elif ' = ptl ' in line:
            newcard.write('   %s      = ptl     ! minimum pt for the charged leptons\n'%(ptl))
        elif ' = etaa ' in line:
            newcard.write('   %s      = etaa    ! max rap for the photons\n'%(etaa))
        elif ' = etal ' in line:
            newcard.write('   %s      = etal    ! max rap for the charged leptons\n'%(etal))
        elif ' = draa ' in line:
            newcard.write('   %s      = draa    ! min distance between gammas\n'%(draa))
        elif ' = draj ' in line:
            newcard.write('   %s      = draj    ! min distance between gamma and jet\n'%(draj))
        elif ' = drab ' in line:
            newcard.write('   %s      = drab    ! min distance between gamma and b\n'%(drab))
        elif ' = dral ' in line:
            newcard.write('   %s      = dral    ! min distance between gamma and lepton\n'%(dral))
        elif ' = drll ' in line:
            newcard.write('   %s      = drll    ! min distance between leptons\n'%(drll))
        elif ' = drjl ' in line:
            newcard.write('   %s      = drjl    ! min distance between jet and lepton\n'%(drjl))
        elif ' = drbl ' in line:
            newcard.write('   %s      = drbl    ! min distance between b and lepton\n'%(drbl))
        elif systName == 'scalefactUP' and ' scalefact ' in line:
            newcard.write(' %f        = scalefact        ! scale factor for event-by-event scales \n' % (scaleUP))
        elif systName == 'scalefactDOWN' and ' scalefact ' in line:
            newcard.write(' %f        = scalefact        ! scale factor for event-by-event scales \n' % (scaleDOWN))
        elif systName == 'alpsfactUP' and ' alpsfact ' in line:
            newcard.write(' %f        = alpsfact         ! scale factor for QCD emission vx \n' % (alpsUP))
        elif systName == 'alpsfactDOWN' and ' alpsfact ' in line:
            newcard.write(' %f        = alpsfact         ! scale factor for QCD emission vx \n' % (alpsDOWN))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

#print "proc card:"
#print fcard
print "Modified run_card.dat:"
rcard = open('run_card.dat','r')
print rcard.read()
rcard.close()


process_dir = new_process()

if runArgs.runNumber in ttbargamma_noAllHad:
    generate(run_card_loc = 'run_card.dat', param_card_loc = None, mode = 0, njobs = 1, run_name = 'Test', proc_dir = process_dir)
elif runArgs.runNumber in ttbargamma_AllHad:
    generate(run_card_loc = 'run_card.dat', param_card_loc = None, mode = 0, njobs = 1, run_name = 'Test', proc_dir = process_dir)


stringy = 'madgraph.' + str(runArgs.runNumber)+'.MadGraph_'+str(name)

skip_events = 0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name = 'Test', proc_dir = process_dir, outputDS = stringy + '._00001.events.tar.gz', skip_events = skip_events)


#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
topAlg.Pythia.PythiaCommand = ["pyinit user madgraph"]

#--------------------------------------------------------------
topAlg.Pythia.PythiaCommand +=  [
                "pystat 1 3 4 5",
                "pyinit dumpr 1 5",
                "pyinit pylistf 1",
                ]

if systName == 'alpsfactUP' or systName == 'alpsfactDOWN':

    print "\nChanging Pythia parameters to reflect change in alpsfact in MadGraph"
    if systName == 'alpsfactUP':
        #PythiaParameterList = [ "PARP(64)=%f" % (1.0 * (alpsUP * alpsUP)), "PARP(72)=%f" % (0.25 * (1.0 / alpsUP))]
        PythiaParameterList = [ "PARP(64)=%f" % (1.0 * (alpsUP * alpsUP))]
    if systName == 'alpsfactDOWN':
        #PythiaParameterList = [ "PARP(64)=%f" % (1.0 * (alpsDOWN * alpsDOWN)), "PARP(72)=%f" % (0.25 * (1.0 / alpsDOWN))]
        PythiaParameterList = [ "PARP(64)=%f" % (1.0 * (alpsDOWN * alpsDOWN))]

    print "New parameter values: ", PythiaParameterList
    topAlg.Pythia.PygiveCommand += PythiaParameterList

# Parton shower variations

if doPSVars:
    print "\nIncluding additional ISR/FSR/PS variation:", PSVar
    #include ( str("MC12JobOptions/Pythia_CTEQ6L1_"+PSVar+"_Common.py") )
    if PSVar == 'MoreFSR':
        PythiaParameterList = [ "PARP(72)=0.7905", "PARJ(82)=0.5" ]
    elif PSVar == 'LessFSR':
        PythiaParameterList = [ "PARP(72)=0.2635", "PARJ(82)=1.66" ]
    print "New parameter values: ", PythiaParameterList
    topAlg.Pythia.PygiveCommand += PythiaParameterList


include ( "MC12JobOptions/Tauola_Fragment.py" )
include ( "MC12JobOptions/Photos_Fragment.py" )


evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.description = 'MadGraph_' + str(name)
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = 5000
runArgs.inputGeneratorFile = stringy + '._00001.events.tar.gz'
