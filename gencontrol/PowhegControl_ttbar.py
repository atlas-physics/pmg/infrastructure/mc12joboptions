from PowhegControl.PowhegUtils import PowhegConfig_base

###############################################################################
#  tt
#
##############################################################################

class AtlasPowhegConfig_tt(PowhegConfig_base) :
  # These are user configurable - put generic properties in PowhegConfig_base
  quarkMass        = 172.5
  topdecaymode     = 22222
  tdec_wmass       = 80.399
  tdec_wwidth      = 2.085
  tdec_bmass       = 4.95
  tdec_twidth      = 1.3200
  tdec_elbranching = 0.108
  tdec_emass       = 0.00051
  tdec_mumass      = 0.1057
  tdec_taumass     = 1.777
  tdec_dmass       = 0.320
  tdec_umass       = 0.320
  tdec_smass       = 0.5
  tdec_cmass       = 1.55
  tdec_sin2cabibbo = 0.051
  # now in PowhegConfig_base 
  # matrix element parameters 
  #mu_F             = 1.0
  #mu_R             = 1.0
  #hdamp            =-1.0    
  
  # Integration parameters
  ncall1, ncall2 = 10000, 100000 # defaults are: 50000, 50000
  nubound, xupbound = 100000, 2  # defaults are: 50000, 2
  itmx1, itmx2 = 5, 5
  ixmax, iymax = 1, 1
  foldx, foldy, foldphi = 1, 1, 1

  # Set process-dependent paths in the constructor
  def __init__(self,runArgs=None) :
    PowhegConfig_base.__init__(self,runArgs)
    self._powheg_executable += '/hvq/pwhg_main'

  def generateRunCard(self) :
    self.generateRunCardSharedOptions()

    with open( str(self.TestArea)+'/powheg.input','a') as f :
      f.write( 'qmass  '+str(self.quarkMass)+'                  ! mass of heavy quark in GeV\n' )
      f.write( 'topdecaymode  '+str(self.topdecaymode)+'        ! an integer of 5 digits that are either 0, or 2, representing in\n' )
      f.write( '                                                !   the order the maximum number of the following particles(antiparticles)\n' )
      f.write( '                                                !   in the final state: e  mu tau up charm\n' )
      f.write( '                                                !   22222    All decays (up to 2 units of everything)\n' )
      f.write( '                                                !   20000    both top go into b l nu (with the appropriate signs)\n' )
      f.write( '                                                !   10011    one top goes into electron (or positron), the other into (any) hadron\n' )
      f.write( '                                                !            or one top goes into charm, the other into up\n' )
      f.write( '                                                !   00022    Fully hadronic\n' )
      f.write( '                                                !   00002    Fully hadronic with two charms\n' )
      f.write( '                                                !   00011    Fully hadronic with a single charm\n' )
      f.write( '                                                !   00012    Fully hadronic with at least one charm\n' )
      f.write( 'tdec/wmass '+str(self.tdec_wmass)+'             ! W mass for top decay\n' )
      f.write( 'tdec/wwidth '+str(self.tdec_wwidth)+'           ! W width\n' )
      f.write( 'tdec/bmass '+str(self.tdec_bmass)+'             ! b quark mass in t decay\n' )
      f.write( 'tdec/twidth '+str(self.tdec_twidth)+'           ! top width\n' )
      f.write( 'tdec/elbranching '+str(self.tdec_elbranching)+' ! W electronic branching fraction\n' )
      f.write( 'tdec/emass '+str(self.tdec_emass)+'             ! electron mass\n' )
      f.write( 'tdec/mumass '+str(self.tdec_mumass)+'           ! mu mass\n' )
      f.write( 'tdec/taumass '+str(self.tdec_taumass)+'         ! tau mass\n' )
      f.write( 'tdec/dmass '+str(self.tdec_dmass)+'             ! d mass\n' )
      f.write( 'tdec/umass '+str(self.tdec_umass)+'             ! u mass\n' )
      f.write( 'tdec/smass '+str(self.tdec_smass)+'             ! s mass\n' )
      f.write( 'tdec/cmass '+str(self.tdec_cmass)+'             ! c mass\n' )
      f.write( 'tdec/sin2cabibbo '+str(self.tdec_sin2cabibbo)+' ! sine of Cabibbo angle squared\n' )
      # now in PowhegConfig_base    
      #   f.write( 'hdamp '+str(self.hdamp)+'                       ! Born-zero damping factor \n' )



