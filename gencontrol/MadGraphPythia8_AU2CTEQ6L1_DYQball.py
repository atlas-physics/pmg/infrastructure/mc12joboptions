###############################################################
#
# Job options file for Evgen MadGraph Pythia8 Qball Generation
# W. Taylor, 2014-01-23
#==============================================================

#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include( "MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py" )
include( "MC12JobOptions/Pythia8_MadGraph.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.description = "Drell-Yan Qball generation for Mass=%s, Charge=%s with MadGraph+Pythia8 and the AU2 CTEQ6L1 tune in MC12" % (mass,charge)
evgenConfig.keywords = ["exotics", "Qball", "Drell-Yan"]
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.contact = ["wtaylor@cern.ch"]

evgenConfig.specialConfig = 'MASS=%s;CHARGE=%s;preInclude=SimulationJobOptions/preInclude.Qball.py' % (mass,charge)

PDG = 10000000 + int(float(charge)*100.0)

#--------------------------------------------------------------
# Edit PDGTABLE.MeV with Qball PDGid and mass
#--------------------------------------------------------------
ALINE1="M %s                         %s.E+03      +0.0E+00 -0.0E+00 QBall           +" % (PDG,mass)
ALINE2="W %s                         0.E+00        +0.0E+00 -0.0E+00 QBall           +" % (PDG)

import os
import sys

pdgmod = os.path.isfile('PDGTABLE.MeV')
if pdgmod is True:
    os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
f=open('PDGTABLE.MeV','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2
