# Set up for generic alpgen generation
#  This is the main job option to call

import sys

rand_seed=1
ecm = 4000.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenGenericGeneration')

if not hasattr(runArgs,'runNumber'):
    log.fatal('No run number found.  Generation will fail.  No idea how to decode this.')
    raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
    log.error('Random seed not set.  Will use 1.')
else:
    rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
    log.info('No skip events argument found.  Good!')
else:
    log.fatal('Skip events argument found.  No idea how to deal with that yet.  Bailing out.')
    raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
    log.warning('No center of mass energy found.  Will use the default of 7 TeV.')
else:
    ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
special1=None
special2=None
special3=None

njets = run_num%10
jets = [njets,1] # slicing index
event_numbers=[20000000,20000000,50000000,80000000,50000000,40000000]
event_numbers_warmup=25000000
ncwarm = [2,2,3,3,4,4]

option=''
W_mode = 4

# W+jets LF with akt10 250GeV filter 
if run_num>=190000 and run_num<=190025:
    log.info('Recognized W+jets LF run number.  Will generate for run '+str(run_num))
    process = 'wjet'
    W_mode = int((run_num-190000)/10)+1
    
# Wc+jets with akt10 250GeV filter 
if run_num>=190030 and run_num<=190034:
    log.info('Recognized Wc+jets run number.  Will generate for run '+str(run_num))
    process = 'wcjet'
    njets+=1
    option="""
    mc      0           ! quark c mass
    ptcmin  10.            ! quark c pt min
    """

# Wbb/cc+jets with akt10 250GeV filter 
if run_num>=190040 and run_num<=190054:
    log.info('Recognized Wc+jets run number.  Will generate for run '+str(run_num))
    process = 'wqq'
    njets+=2
    if run_num>=190050:
        option="""
        ihvy 5        ! Select W+bb
        mc      1.4           ! quark c mass
        mb      4.7           ! quark b mass
        ptbmin  0.            ! quark b pt min
        drbmin  0.            ! deltar b
        """
    else :
        option="""
        ihvy 4        ! Select W+cc
        mc      1.4           ! quark c mass
        mb      4.7           ! quark b mass
        ptcmin  0.            ! quark c pt min
        drcmin  0.            ! deltar c
        """


# inclusive production for Np5
if njets==5: jets[1]=0
if njets>5: log.error('wrong slicing %i %i' %(run_num, njets))

special1 = 'iwdecmode %i          ! W decay mode (1: el, 2: mu, 3: tau, 4: all)\n '%(W_mode)
special1 += option
special1 +='ptj1min 150           ! leading parton pt \n'
special1 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 
special2 = special1

nwarm = [ ncwarm[njets] , event_numbers_warmup ]
events_alpgen = event_numbers[njets]

if process=='':
    log.fatal('Unknown run number!  Bailing out!')
    raise RunTimeError('Unknown run number.')

from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
    log.info('Special settings for mode 1:')
    log.info(special1)
if special2 is not None:
    log.info('Special settings for mode 2:')
    log.info(special2)
if special3 is not None:
    log.info('Special settings for pythia:')
    log.info(special3)

inputgridfile = None
if hasattr(runArgs,'inputGenConfFile'):
        log.info('Passing input grid file to AlpGenUtils ' +str(runArgs.inputGenConfFile))
        inputgridfile=runArgs.inputGenConfFile
            

SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2 , special3=special3,inputgridfile=inputgridfile)

if hasattr(runArgs,'inputGeneratorFile'):
    log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'

log.info('Passing events on to Pythia for generation')
include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

# Additional bit for ME/PS matching
phojf=open('./pythia_card.dat', 'w')
phojinp = """
!...Matching parameters...
IEXCFILE=%i
showerkt=T
qcut=%i
imss(21)=24
imss(22)=24  
"""%(jets[1],15)
# For the moment, hard-coded jet cut (in AlpGenUtils.py)

phojf.write(phojinp)
phojf.close()

# Fill up the evgenConfig
evgenConfig.description = 'alpgen_autoconfig'
evgenConfig.contact = ['jiahang.zhong@cern.ch', 'zach.marshall@cern.ch','thorsten.kuhl@cern.ch' ]
evgenConfig.process = 'auto_alpgen_'+process+'_'+`jets[0]`+'p'
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
#evgenConfig.efficiency = 0.90000

# Akt10 jet filter

from JetRec.JetGetters import *
a10alg = make_StandardJetGetter('AntiKt', 1.0, 'Truth').jetAlgorithmHandle()

include("MC12JobOptions/JetFilter_Fragment.py")
topAlg.QCDTruthJetFilter.TruthJetContainer = "AntiKt10TruthJets"
topAlg.QCDTruthJetFilter.MinPt = 250.*GeV
topAlg.QCDTruthJetFilter.MaxEta = 2.2

