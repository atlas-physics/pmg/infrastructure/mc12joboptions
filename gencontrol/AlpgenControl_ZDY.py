# Set up for generic alpgen generation
#  This is the main job option to call

import sys

UseHerwig=True
rand_seed=1
ecm = 3500.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenGenericGeneration')

if not hasattr(runArgs,'runNumber'):
    log.fatal('No run number found.  Generation will fail.  No idea how to decode this.')
    raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
    log.error('Random seed not set.  Will use 1.')
else:
    rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
    log.info('No skip events argument found.  Good!')
else:
    log.fatal('Skip events argument found.  No idea how to deal with that yet.  Bailing out.')
    raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
    log.warning('No center of mass energy found.  Will use the default of 7 TeV.')
else:
    ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
special1=None
special2=None
special3=None
nwarm = [ 4 , 1000000 ]
UseDilFilter=False
UseLooseDilFilter=False
UseMediumDilFilter=False
ForceLeptonicTau=False
VBFFilter=False
TVBFFilter=False
VBFFilter_mjj350=False
ZJET=False
DoGrid=False

# Z->tautau filtered sample - B. Di Micco
# Alpgen+Pythia Perugia tune
if run_num>=181960 and run_num<=181965:
    UseHerwig=False
    run_num = run_num-181960+146930
    
if run_num>=146930 and run_num<=146935:
    UseLooseDilFilter=True
    run_min=146930
    event_numbers=[1500000,24000000,200000000,140000000,35000000,15000000,10000000,10000000,10000000]
    jets = [(run_num-run_min)%10,1]
    events_athena=5000
  
    if jets[0]==2: events_athena=500 
    if jets[0]==3: events_athena=200
    if jets[0]==4: events_athena=100
    if jets[0]==5: events_athena=50
    Z_mode=3
    special1 = """mllmin  60           ! Minimum M_ll
mllmax  2000          ! Maximum M_ll
"""
    ZJET=True

# Z->tautau dilepton plus VBF filtered sample - B. Di Micco
if run_num>=169450 and run_num<=169465:
    UseLooseDilFilter=True
    ForceLeptonicTau=True
    VBFFilter=True
    
    run_min=169450
    event_numbers=[15000000,48000000,290000000,160000000,70000000,30000000,10000000,10000000,10000000]
    jets = [(run_num-run_min)%10,1]

    if (run_num-169450)>9:
        UseHerwig=False # Move to Pythia for half the samples

    events_athena=5000
    if jets[0]==0: events_athena=2000
    if jets[0]==2: events_athena=1000
    if jets[0]==3: events_athena=1000
    if jets[0]==4: events_athena=500
    if jets[0]==5: events_athena=100
    Z_mode=3
    special1 = """mllmin  60           ! Minimum M_ll
mllmax  2000          ! Maximum M_ll
"""
    ZJET=True

 
# DY Filtered sample - thanks to B. Di Micco

if run_num>=146860 and run_num<=146886:
    UseDilFilter=True
    run_min=146860
    event_numbers=[11000000,2000000,250000000,131000000,65000000,30000000,10000000,10000000,10000000]
 
    jets = [(run_num-run_min)%10,1]
    events_athena = 5000
 
    if jets[0]==2: events_athena=1000
    if jets[0]==3: events_athena=200
    if jets[0]==4: events_athena=10
    if jets[0]==5: events_athena=50
 
    if (run_num>=146880 and run_num<=146886):
       event_numbers=[22000000,60000000,430000000,210000000,70000000,30000000,10000000,10000000,10000000]
       if jets[0]==0: events_athena=10
       if jets[0]==1: events_athena=1000
       if jets[0]==2: events_athena=10
       if jets[0]==3: events_athena=20
 
    nwarm = [ jets[0]+2 , 1000000 ]
    run_num=(run_num-run_min)+146830

# DY plus Dilepton + VBF filtered samples - thanks to B. Di Micco

if run_num>=181310 and run_num<=181325:
    UseMediumDilFilter=True
    VBFFilter_mjj350=True
    run_min=181310
    event_numbers=[11000000,60000000,250000000,131000000,65000000,30000000,10000000,10000000,10000000]
    jets = [(run_num-run_min)%10,1]
    events_athena = 5000
    Z_mode = int((run_num-run_min)/10)%4+1

    if jets[0]==0 and Z_mode==1: events_athena=100
    if jets[0]==0 and Z_mode==2: events_athena=20
    if jets[0]==1 and Z_mode==2: events_athena=2000
    if jets[0]==2: events_athena=50
    if jets[0]==3: events_athena=100
    if jets[0]==4 and Z_mode==1 : events_athena=50
    if jets[0]==4 and Z_mode==2 : events_athena=20
    if jets[0]==5: events_athena=5


    run_num=(run_num-run_min)+146830



# DY Sample - thanks to B. Di Micco
if run_num>=146830 and run_num<=146856:
    ZJET=True
    if not UseDilFilter and not UseMediumDilFilter:
        event_numbers=[120000,400000,20000000,90000000,70000000,20000000,10000000,10000000,10000000]

    run_min=146830
    jets = [(run_num-run_min)%10,1] # Exclusive matching to the last number of partons   
    events_athena = 5000
    if jets[0]==3: events_athena=500
    if jets[0]==4: events_athena=100
    if jets[0]==5: events_athena=20
    Z_mode = int((run_num-run_min)/10)%4+1
    special1 = """mllmin  10           ! Minimum M_ll
mllmax  60          ! Maximum M_ll
"""

# VBF filtered + Atau filtered samples Z B. Di Micco, K. Nakamura

ATauFilter=False

if run_num>=167320 and run_num<=(167320+29):
    ZJET=True
    VBFFilter=True
    run_min=167320
    Z_mode = int((run_num-run_min)/10)%4+1

    if Z_mode == 1 :
        event_numbers=[2000000,8500000,30000000,70000000,50000000,15000000,10000000,10000000,10000000]
    elif Z_mode == 2 :
        event_numbers=[4000000,18800000,73000000,90000000,50000000,15000000,10000000,10000000,10000000]
    else:
        event_numbers=[2600000,11000000,34000000,73000000,50000000,15000000,10000000,10000000,10000000]
        ATauFilter=True

    jets = [(run_num-run_min)%10,1] # Exclusive matching to the last number of partons

#    events_athena = 100
    events_athena = 5000
    if jets[0]==0 and Z_mode>1: events_athena=2000
    if jets[0]==1 and Z_mode==3: events_athena=2000
    if jets[0]==2: events_athena=1000
    if jets[0]==2 and Z_mode==3: events_athena=200
    if jets[0]==3: events_athena=2000
    if jets[0]==3 and Z_mode==3: events_athena=1000
    if jets[0]==4: events_athena=1000
    if jets[0]==4 and Z_mode==3: events_athena=500
    if jets[0]==5: events_athena=100

    special1 = """mllmin  60           ! Minimum M_ll
mllmax  2000          ! Maximum M_ll
"""



# Alpgen + Pythia VBF filter Z+jets   K. Nakamura (Dec 2013)
if run_num>=169640 and run_num<=(169640+29):
    UseHerwig=False
    ZJET=True

    VBFFilter=True
    run_min=169640
    Z_mode = int((run_num-run_min)/10)%4+1
    if Z_mode == 5 :
        VBFFilter=False

    if Z_mode == 1 :
        event_numbers=[2000000,8500000,30000000,70000000,50000000,15000000,10000000,10000000,10000000]
    elif Z_mode == 2 :
        event_numbers=[4000000,18800000,73000000,90000000,50000000,15000000,10000000,10000000,10000000]
    else:
        event_numbers=[2600000,11000000,34000000,73000000,50000000,15000000,10000000,10000000,10000000]
        ATauFilter=True

    jets = [(run_num-run_min)%10,1] # Exclusive matching to the last number of partons

    events_athena = 5000
    if Z_mode==1:
        if jets[0]==2: events_athena=1000
        if jets[0]==3: events_athena=1000
        if jets[0]==4: events_athena=500
        if jets[0]==5: events_athena=50

    if Z_mode==2:
        if jets[0]==2: events_athena=500
        if jets[0]==3: events_athena=2000
        if jets[0]==4: events_athena=500
        if jets[0]==5: events_athena=10

    if Z_mode==3:
        if jets[0]==2: events_athena=200
        if jets[0]==3: events_athena=500
        if jets[0]==4: events_athena=100
        if jets[0]==5: events_athena=10


    special1 = """mllmin  60           ! Minimum M_ll
mllmax  2000          ! Maximum M_ll
"""



# Tight VBF filtered + Atau filtered samples Z  K. Nakamura

if  run_num>=169500 and run_num<=(169500+29) :
    ZJET=True
    TVBFFilter=True
    run_min=169500

    Z_mode = int((run_num-run_min)/10)%4+1

    if Z_mode == 1 :
        event_numbers=[2000000,8500000,30000000,70000000,50000000,15000000,10000000,10000000,10000000]
    elif Z_mode == 2 :
        event_numbers=[4000000,18800000,73000000,90000000,50000000,15000000,10000000,10000000,10000000]
    else:
        event_numbers=[2600000,11000000,34000000,73000000,50000000,15000000,10000000,10000000,10000000]
        ATauFilter=True

    jets = [(run_num-run_min)%10,1] # Exclusive matching to the last number of partons

#    events_athena = 100
    events_athena = 1000
    if jets[0]==0 and Z_mode>1: events_athena=500
    if jets[0]==1 and Z_mode==3: events_athena=500
    if jets[0]==2: events_athena=200
    if jets[0]==2 and Z_mode==3: events_athena=50
    if jets[0]==3: events_athena=500
    if jets[0]==3 and Z_mode==3: events_athena=200
    if jets[0]==4: events_athena=200
    if jets[0]==4 and Z_mode==3: events_athena=100
    if jets[0]==5: events_athena=20

    special1 = """mllmin  60           ! Minimum M_ll
mllmax  2000          ! Maximum M_ll
"""


# Alpgen+Pythia Tight VBF filtered + Atau filtered samples Z  K. Nakamura (Dec 2013)
if  run_num>=169610 and run_num<=(169610+29) :
    UseHerwig=False
    ZJET=True
    TVBFFilter=True
    run_min=169610

    Z_mode = int((run_num-run_min)/10)%4+1

    if Z_mode == 1 :
        event_numbers=[2000000,8500000,30000000,70000000,50000000,15000000,10000000,10000000,10000000]
    elif Z_mode == 2 :
        event_numbers=[4000000,18800000,73000000,90000000,50000000,15000000,10000000,10000000,10000000]
    else:
        event_numbers=[2600000,11000000,34000000,73000000,50000000,15000000,10000000,10000000,10000000]
        ATauFilter=True

    jets = [(run_num-run_min)%10,1] # Exclusive matching to the last number of partons

    events_athena = 1000
    if Z_mode==1:
        if jets[0]==2: events_athena=500
        if jets[0]==3: events_athena=500
        if jets[0]==4: events_athena=200
        if jets[0]==5: events_athena=25

    if Z_mode==2:
        if jets[0]==2: events_athena=200
        if jets[0]==3: events_athena=100
        if jets[0]==4: events_athena=100
        if jets[0]==5: events_athena=10

    if Z_mode==3:
        if jets[0]==2: events_athena=25
        if jets[0]==3: events_athena=100
        if jets[0]==4: events_athena=25
        if jets[0]==5: events_athena=5


    special1 = """mllmin  60           ! Minimum M_ll
mllmax  2000          ! Maximum M_ll
"""




# Z+jets samples - thanks to O. Rosenthal; modified by B. Di Micco
if run_num>=147073 and run_num<=147136:
    ZJET=True
    run_min=147073

    event_numbers=[80000,2000000,4500000,70000000,50000000,15000000,10000000,10000000,10000000]
    jets = [(run_num-run_min)%8,1] # Exclusive matching to the last number of partons
    Z_mode = int((run_num-147073)/8)%4+1
    events_athena = 5000
    if jets[0]==0 and Z_mode==2: events_athena=2000
    if jets[0]==4: events_athena=2000
    if jets[0]==5: events_athena=100
    if (run_num-run_min)>=32: UseHerwig=False # Move to Pythia for half the samples

    special1 = """mllmin  60           ! Minimum M_ll
mllmax  2000          ! Maximum M_ll
"""

if ZJET:
    log.info('Recognized Z+jets run number.  Will generate for run '+str(run_num))
    process = 'zjet'
    if jets[0]==5: jets[1]=0

    events_alpgen = event_numbers[jets[0]]

    # Handle special cases for more than five partons.  Run # 6, Np5 excl.  Run # 7, Np6 incl.
    if jets[0]==6: 
        jets=[5,1]
    elif jets[0]==7:
        jets=[6,1]
    elif jets[0]>7:
        jets=[5,1]

    nwarm = [ jets[0]+2 , 1000000 ]
    if DoGrid:
       nwarm = [ jets[0]+8 , 60000000 ]
    if Z_mode<4: special1 += """ilep 0        ! Use leptons in the final state (not neutrinos)
izdecmode %i          ! Z decay mode (1: ee, 2: mumu, 3: tautau)\n """%(Z_mode)
    else: special1 += """ilep 1       ! Use neutrinos in the final state (not leptons) \n """

if DoGrid:
   events_alpgen=0

if process=='':
    log.fatal('Unknown run number!  Bailing out!')
    raise RunTimeError('Unknown run number.')

#if not UseHerwig:  
#        special1 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
#lpclu 1         ! nloop for alphas in CKKW scale evaluation"""
#
#        special2 = """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
#lpclu 1         ! nloop for alphas in CKKW scale evaluation"""

if hasattr(runArgs,'cutAthenaEvents'):
    events_athena = events_athena/runArgs.cutAthenaEvents
    log.warning('Reducing number of Athena events by '+str(runArgs.cutAthenaEvents)+' to '+str(events_athena)+' .  Later you should fix this.')
if hasattr(runArgs,'raiseAlpgenEvents'):
    events_alpgen = events_alpgen*runArgs.raiseAlpgenEvents
    log.warning('Increasing number of AlpGen events by '+str(runArgs.raiseAlpgenEvents)+' to '+str(events_alpgen)+' .  Later you should fix this.')

inputgridfile = None
if hasattr(runArgs,'inputGenConfFile'):
    log.info('Passing input grid file to AlpGenUtils ' +str(runArgs.inputGenConfFile))
    inputgridfile=runArgs.inputGenConfFile


from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
    log.info('Special settings for mode 1:')
    log.info(special1)
if special2 is not None:
    log.info('Special settings for mode 2:')
    log.info(special2)
if special3 is not None:
    log.info('Special settings for pythia:')
    log.info(special3)

SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2 , special3=special3 , inputgridfile=inputgridfile )

if hasattr(runArgs,'inputGeneratorFile'):
    log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'



if UseHerwig:
    log.info('Passing events on to Herwig for generation of '+str(events_athena)+' events in athena')
    include('MC12JobOptions/AlpgenControl_Herwig.py')
else:
    log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')
    include('MC12JobOptions/AlpgenControl_Pythia.py')

    # Additional bit for ME/PS matching
    phojf=open('./pythia_card.dat', 'w')
    phojinp = """
      !...Matching parameters...
      IEXCFILE=%i
      showerkt=T
      qcut=%i
      imss(21)=24
      imss(22)=24  
    """%(jets[1],15)
    # For the moment, hard-coded jet cut (in AlpGenUtils.py)

    phojf.write(phojinp)
    phojf.close()


if ForceLeptonicTau:
   include("MC12JobOptions/Tauola_LeptonicDecay_Fragment.py")
   # tauola multiplicative factor for AMI MetaData
   topAlg.Herwig.CrossSectionScaleFactor=0.1393
else:
   include("MC12JobOptions/Tauola_Fragment.py")
include ( "MC12JobOptions/Photos_Fragment.py" )

if UseDilFilter: include("MC12JobOptions/AlpgenControl_DilFilter.py")
if VBFFilter: include("MC12JobOptions/AlpgenControl_VBFFilter.py")
if ATauFilter: include("MC12JobOptions/AlpgenControl_ATauFilter.py")
if UseLooseDilFilter: include("MC12JobOptions/AlpgenControl_LooseDilFilter.py")
if UseMediumDilFilter: include("MC12JobOptions/AlpgenControl_MediumDilFilter.py")
if TVBFFilter: include("MC12JobOptions/AlpgenControl_TVBFFilter.py")
if VBFFilter_mjj350: include("MC12JobOptions/AlpgenControl_VBFFilter_mjj350.py")

# Fill up the evgenConfig
evgenConfig.description = 'alpgen_autoconfig'
evgenConfig.process = 'auto_alpgen_'+process+'_'+`jets[0]`+'p'
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
evgenConfig.contact = ['biagio.di.micco@cern.ch','zach.marshall@cern.ch','Koji.Nakamura@cern.ch']
evgenConfig.keywords = ['Z','leptonic']
evgenConfig.minevents = events_athena
