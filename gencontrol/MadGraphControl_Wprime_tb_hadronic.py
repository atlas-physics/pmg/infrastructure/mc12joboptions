### Generation of W'->tb samples with MadGraph+Pythia ###

### Runs
#    * 110721-110731: W'R, nominal, for each mass point 
#    * 110732-110742: W'R, less PS, for each mass point 
#    * 110743-110753: W'R, more PS, for each mass point 
#    * 110754-110764: W'L, nominal, for each mass point 
#    * 110765-110775: W'L, less PS, for each mass point 
#    * 110776-110786: W'L, more PS, for each mass point 
#    * 110787-110797: W'L + single-top s-channel, nominal, for each mass point

### Special runs with varied g'/g:
# 110855: Wprime_g3_SM_right_tb_hadronic_M500
# 110856: Wprime_g3_SM_right_tb_hadronic_M1500
# 110857: Wprime_g3_SM_right_tb_hadronic_M2500
# 110858: Wprime_g5_SM_right_tb_hadronic_M500
# 110859: Wprime_g5_SM_right_tb_hadronic_M1500
# 110860: Wprime_g5_SM_right_tb_hadronic_M2500
# 110861: Wprime_g3_SM_left_tb_hadronic_M500
# 110862: Wprime_g3_SM_left_tb_hadronic_M1500
# 110863: Wprime_g3_SM_left_tb_hadronic_M2500
# 110864: Wprime_g5_SM_left_tb_hadronic_M500
# 110865: Wprime_g5_SM_left_tb_hadronic_M1500
# 110866: Wprime_g5_SM_left_tb_hadronic_M2500


from MadGraphControl.MadGraphUtils import *

### Set generation card
if (runArgs.runNumber>=110721 and runArgs.runNumber<=110786) or (runArgs.runNumber>=110855 and runArgs.runNumber<=110866): # W' only
    procname='Wprime'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model WEff_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t b~ QED=0 NP=2, (t > b W+, W+ > j j)
    add process p p > t~ b QED=0 NP=2, (t~ > b~ W-, W- > j j)
    output -f
    """)
    fcard.close()
elif runArgs.runNumber>=110787 and runArgs.runNumber<=110797: # W'L+ST s-chan
    procname='WprimeSTschan'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model WEff_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t b~ QED=2 NP=2, (t > b W+, W+ > j j)
    add process p p > t~ b QED=2 NP=2, (t~ > b~ W-, W- > j j)
    output -f
    """)
    fcard.close()
else:
    print 'ERROR: run number not in run range'
    
process_dir = new_process()

### Modified parameters w.r.t default run_card.SM.dat
ptl = 0
xqcut = 0
nevents = 10000
bwcutoff = 1000

### Modified generation parameter w.r.t default param_card.Wprime_tb.dat
#    34 1.000000e+03 # MWp 
# DECAY  34 3.400000e+01 # WWp
# 
#     1 6.483972e-01 # gL 
#     2 0.000000e+00 # gR

# W masses and width (NLO - 7 and 8 TeV)
if runArgs.runNumber in [ 110721+(i*11) for i in range(7) ]:
    MWp = 1000
    WWpL = 34
    WWpR = 26
    gSF = 1
elif runArgs.runNumber in [ 110722+(i*11) for i in range(7) ]:
    MWp = 1250
    WWpL = 43
    WWpR = 32
    gSF = 1
elif runArgs.runNumber in [ 110723+(i*11) for i in range(7) ]:
    MWp = 1500
    WWpL = 52
    WWpR = 39
    gSF = 1
elif runArgs.runNumber in [ 110724+(i*11) for i in range(7) ]:
    MWp = 1750
    WWpL = 60
    WWpR = 46
    gSF = 1
elif runArgs.runNumber in [ 110725+(i*11) for i in range(7) ]:
    MWp = 2000
    WWpL = 69
    WWpR = 52
    gSF = 1
elif runArgs.runNumber in [ 110726+(i*11) for i in range(7) ]:
    MWp = 2250
    WWpL = 78
    WWpR = 59
    gSF = 1
elif runArgs.runNumber in [ 110727+(i*11) for i in range(7) ]:
    MWp = 2500
    WWpL = 86
    WWpR = 65
    gSF = 1
elif runArgs.runNumber in [ 110728+(i*11) for i in range(7) ]:
    MWp = 2750
    WWpL = 95
    WWpR = 72
    gSF = 1
elif runArgs.runNumber in [ 110729+(i*11) for i in range(7) ]:
    MWp = 3000
    WWpL = 104
    WWpR = 78
    gSF = 1
elif runArgs.runNumber in [ 110730+(i*11) for i in range(7) ]:
    MWp = 3250
    WWpL = 112
    WWpR = 85
    gSF = 1
elif runArgs.runNumber in [ 110731+(i*11) for i in range(7) ]:
    MWp = 3500
    WWpL = 121
    WWpR = 91         
    gSF = 1

# Special runs with g' different than 1
if runArgs.runNumber==110855 or runArgs.runNumber==110861:
    MWp = 500
    WWpL = 17
    WWpR = 12
    gSF = 3
elif runArgs.runNumber==110856 or runArgs.runNumber==110862:
    MWp = 1500
    WWpL = 52
    WWpR = 39
    gSF = 3
elif runArgs.runNumber==110857 or runArgs.runNumber==110863:
    MWp = 2500
    WWpL = 86
    WWpR = 65
    gSF = 3
elif runArgs.runNumber==110858 or runArgs.runNumber==110864:
    MWp = 500
    WWpL = 17
    WWpR = 12
    gSF = 5
elif runArgs.runNumber==110859 or runArgs.runNumber==110865:
    MWp = 1500
    WWpL = 52
    WWpR = 39
    gSF = 5
elif runArgs.runNumber==110860 or runArgs.runNumber==110866:
    MWp = 2500
    WWpL = 86
    WWpR = 65
    gSF = 5

# L/R couplings
gSM = (6.483972e-01)

if (runArgs.runNumber>=110721 and runArgs.runNumber<=110753) or (runArgs.runNumber>=110855 and runArgs.runNumber<=110860):
    chirname='right'
    gL = 0
    gR = gSM*gSF
    WWp = WWpR*(gR/gSM)**2
elif (runArgs.runNumber>=110754 and runArgs.runNumber<=110797) or (runArgs.runNumber>=110861 and runArgs.runNumber<=110866):
    chirname='left'
    gL = gSM*gSF
    gR = 0
    WWp = WWpL*(gL/gSM)**2

# Grab the run card and move it into place
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' xqcut  ' in line:
            newcard.write('%i   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(nevents))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' ptl ' in line:
            newcard.write('   %i      = ptl ! minimum pt for the charged leptons \n'%(ptl))
        elif ' etal ' in line:
            newcard.write(' 1d2  = etal    ! max rap for the charged leptons \n')
        elif ' ebeam1 ' in line:
            newcard.write('     %i     = ebeam1  ! beam 1 energy in GeV \n'%(runArgs.ecmEnergy/2))
        elif ' ebeam2 ' in line:
            newcard.write('     %i     = ebeam2  ! beam 2 energy in GeV \n'%(runArgs.ecmEnergy/2))
        elif ' bwcutoff' in line:
            newcard.write('     %i     = bwcutoff \n'%(bwcutoff))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

# Grab parameter card and move it into place
paramcard = subprocess.Popen(['get_files','-data','param_card.Wprime_tb.dat'])
paramcard.wait()
if not os.access('param_card.Wprime_tb.dat',os.R_OK):
    print 'ERROR: Could not get run card'
else:
    oldcard = open('param_card.Wprime_tb.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if ' MWp' in line:
            newcard.write('    34 %f # MWp \n'%(MWp))
        elif ' WWp' in line:
            newcard.write('DECAY  34 %f # WWp \n'%(WWp))
        elif ' gL' in line:
            newcard.write('    1 %f # gL \n'%(gL))
        elif ' gR' in line:
            newcard.write('    2 %f # gR \n'%(gR))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

### TODO: set better name

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+procname+'_'+chirname+'_'+str(MWp)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)


### PYTHIA 8 

runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

evgenConfig.description = "MadGraph5+Pythia8 for Wprime"

evgenConfig.keywords = ["Wprime", "top", "schan"]

evgenConfig.inputfilecheck = "Wprime"

evgenConfig.generators = ["MadGraph", "Pythia8"]

# CTEQ6l1
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

# MRST2008LO (TO DO)
# include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

include("MC12JobOptions/Pythia8_LHEF.py")

