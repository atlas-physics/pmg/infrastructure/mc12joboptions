import os
from MadGraphControl.MadGraphUtils import *

ptj = 20
ptb = 20
pta = 20
etaa = 2.7
etal = 2.7
drjj = 0.2
drll = 0.2
draa = 0.2
draj = 0.2
drjl = 0.2
dral = 0.2
#mmaa = 50
mmaa = 100
mmjj = 25.
mmbb = 80.
xqcut = 0

processPythia = ""
fcard = open('proc_card_mg5.dat','w')
if runArgs.runNumber==181782:
    processPythia = "Merging:Process = pp>aajj"
    proccard = """
set stdout_level DEBUG
import model sm
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u d s u~ d~ s~
define x = c c~
define y = b b~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > j j a a @0
add process p p > j x a a @1
add process p p > j y a a @2
add process p p > j j j a a @3
add process p p > j j x a a @4
add process p p > j j y a a @5
output -f
"""
elif runArgs.runNumber==181783:
    processPythia = "Merging:Process = pp>ccaa"
    proccard = """
import model sm
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > c c~ a a @0
add process p p > c c~ a a j @1
output -f
"""
elif runArgs.runNumber==181784:
    processPythia = "Merging:Process = pp>bbaa"
    proccard = """
import model sm
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > b b~ a a @0
add process p p > b b~ a a j @1
output -f
"""
elif runArgs.runNumber==189839:
    processPythia = "Merging:Process = pp>ccja"
    proccard = """
import model sm

define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define cjet = c c~
generate p p > cjet cjet j a @0
add process p p > cjet cjet j a j @1
output -f
"""
fcard.write(proccard)
fcard.close()

if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 4000.

# Grab the run card and move it into place
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(30000))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(rand_seed))
        elif ' ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif ' ptj ' in line:
            newcard.write('%f   = ptj   ! minimum pt for the jets \n'%(ptj))
        elif ' ptb ' in line:
            newcard.write('%f   = ptb   ! minimum pt for the b \n'%(ptb))
        elif ' pta ' in line:
            newcard.write('%f   = pta   ! minimum pt for the photons \n'%(pta))
        elif ' etaa ' in line:
            newcard.write('%f   = etaa   ! max rap for the photons \n'%(etaa))
        elif ' etal ' in line:
            newcard.write('%f   = etal   ! max rap for the charged leptons \n'%(etal))
        elif ' drjj ' in line:
            newcard.write('%f   = drjj   ! min distance between jets \n'%(drjj))
        elif ' drll ' in line:
            newcard.write('%f   = drll   ! min distance between leptons \n'%(drll))
        elif ' draa ' in line:
            newcard.write('%f   = draa   ! min distance between gammas \n'%(draa))
        elif ' draj ' in line:
            newcard.write('%f   = draj   ! min distance between gamma and jet \n'%(draj))
        elif ' drjl ' in line:
            newcard.write('%f   = drjl   ! min distance between jet and lepton \n'%(drjl))
        elif ' dral ' in line:
            newcard.write('%f   = dral   ! min distance between gamma and lepton \n'%(dral))
        elif ' mmjj ' in line:
            newcard.write('%f   = mmjj   ! min invariant mass of jet jet pair \n'%(mmjj))
        elif ' mmbb ' in line:
            newcard.write('%f   = mmbb   ! min invariant mass of bjet bjet pair \n'%(mmbb))
        elif ' mmaa ' in line:
            newcard.write('%f   = mmaa   ! min invariant mass of gamma gamma pair \n'%(mmaa))
        elif 'ktdurham' in line:
            newcard.write('-1   = ktdurham \n')
        elif ' xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif 'ickkw' in line:
            newcard.write('0   = ickkw    ! 0 no matching, 1 MLM, 2 CKKW matching \n')
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

process_dir = new_process()

generate(run_card_loc='run_card.dat',param_card_loc=None,mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_GammaGammaJetJet'

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)

#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
#include("MC12JobOptions/Pythia8_Photos.py")

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
Pythia8 = topAlg.Pythia8

topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Beams:frameType = 4",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            "Merging:doCutBasedMerging = on",
#                            "Merging:enforceCutOnLHE = off",
                            "Merging:QijMS = "+str(mmjj),
                            "Merging:pTiMS = "+str(ptj),
                            "Merging:dRijMS = "+str(drjj),
                            "Merging:nJetMax = 1",
                            processPythia
                            ]

evgenConfig.description = "MadGraph+Pythia8 production JO with the AU2 CTEQ6L1 tune for continua with jets and photons"
evgenConfig.keywords = ["diphoton", "dijet", "aabb", "aacc", "aajj","bbaj","bbjj","ccja"]
evgenConfig.inputfilecheck = stringy

evgenConfig.minevents = 5000
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
