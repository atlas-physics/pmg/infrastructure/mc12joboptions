## SUSY Herwig++ jobOptions for single squark d_Right production

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.DR_LSP')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_DR_LSP_mc12points.py' )

try:
    (mDR,mLSP) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_SD_Dr%s_L%s.slha' % (mDR, mLSP)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['sdownR','sstrangeR'], slha_file, 'TwoParticleInclusive')
cmds += 'insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/H0'

# define metadata
evgenConfig.description = 'Single_squark_dR grid generation with m_DR = %s, m_LSP = %s GeV' % (mDR,mLSP)
evgenConfig.keywords = ['SUSY','single_squark']
evgenConfig.contact  = ['william.kalderon@cern.ch','alexandru.dafinca@cern.ch','Sascha.Caron@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
