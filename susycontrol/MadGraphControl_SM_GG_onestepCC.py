# One-step gluino decay to LSP, x=1/2, WW (0-2-lepton, grid 4 last year)
# Also One-step gluino decay to LSP, grid in X, WW (0-2-lepton)
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

# m_heavy / m_LSP planes, grids 4,5,8-15
points2b1 = []
sumList   = [250,310,370,430,490,550,610,770,930,1090,1250,1410,1570,1730,1890,2050,2210,2370,2530,2690,2850]
for sum in ([0,50,100,150,200]+sumList):
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*80 ]
    for diff in diffL:
        msq = (sum+diff)/2.
        if msq>1200 or msq-diff<0 or msq<200: continue
        point = [msq,msq-diff]
        points2b1 += [ point ]

# Grids 6 and 7
def wantedPoint(dM1,dM2):
    dMStepsBoundaries=[10,25,50,100]
    dMStepBulk=100
    for s in dMStepsBoundaries:
        if dM1==s or dM2==s:
            return True
        pass
    if dM2 > dMStepsBoundaries[len(dMStepsBoundaries)-1]:
        if dM2%dMStepBulk==0:
            return True
        pass
    return False

mLSP=60
points2x = []
points2xextra = []
points2xextraBench = []
for mGluino in [200,300,400,500,600,700,800,900,1000,1100,1200]:
    for m in xrange(mLSP,mGluino):
        dM1=mGluino-m
        dM2=m-mLSP
        if not wantedPoint(dM1,dM2): continue
        point = [mGluino,float(dM2)/float(mGluino-mLSP)]
        points2x += [ point ]
for mGluino in [1300,1400,1500]:
    for m in xrange(mLSP,mGluino):
        dM1=mGluino-m
        dM2=m-mLSP
        if not wantedPoint(dM1,dM2): continue
        point = [mGluino,float(dM2)/float(mGluino-mLSP)]
        points2xextra += [ point ]
#other extra points
benchmarkPointsNew = {"1200_972":204892,"1300_1052":204893,"1400_1132":204894,"1500_1212":204895,"1600_1292":204896,"1700_1372":204897}
for bpoint in sorted(benchmarkPointsNew.keys()):
    m_gl = bpoint.split("_")[0]
    m_char = bpoint.split("_")[1]
    x_ratio=round((float(m_char)-float(mLSP))/(float(m_gl)-float(mLSP)),10)
    point = [int(m_gl),x_ratio]
    points2xextraBench += [ point ]


# Original grid points
therun = runArgs.runNumber-148001-len(points2b1)
if therun>=0 and therun<len(points2b1):
    evgenLog.info('Registered generation of one-step GG point '+str(therun))
    masses['1000021'] = points2b1[therun][0]
    masses['1000022'] = points2b1[therun][1]
    masses['1000024'] = 0.5*(points2b1[therun][0]+points2b1[therun][1])
    stringy = str(int(points2b1[therun][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(points2b1[therun][1]))
    gentype='GG'
    decaytype='onestepCC'
    njets=1
    use_decays=False
    use_Tauola=False
therun = therun-len(points2b1)-len(points2x)
if therun>=0 and therun<len(points2x):
    evgenLog.info('Registered generation of one-step GG in X point '+str(therun))
    masses['1000021'] = points2x[therun][0]
    masses['1000022'] = mLSP
    masses['1000024'] = points2x[therun][1] * (points2x[therun][0]-mLSP) + mLSP
    stringy = str(int(points2x[therun][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(mLSP))
    gentype='GG'
    decaytype='onestepCC'
    njets=1
    use_decays=False
    use_Tauola=False


# grid extension up to 1500 GeV in gluino mass
points2b21s = []
toremove = [210,240,270,300,330,410,490,570,650,730,810,890,970,1050,1130,1197.5]
counter=0
for sum in ([0,50,100,150,200]+sumList): # Need to loop over the alternating ones...
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*80 ]
    for diff in diffL:
        counter += 1
        msq = (sum+diff)/2.
        #remove regions already excluded or unreachable
        if msq>1500 or msq-diff<0 or msq-diff>800 or msq in toremove: continue
        if msq-diff>600 and (counter%2==0 or msq>1300): continue
        point = [msq,msq-diff]
        if msq>1200: points2b21s += [ point ]

# Extension
therun = runArgs.runNumber - 174391
if therun>=0 and therun<len(points2b21s):
    evgenLog.info('Registered generation of grid five, one-step GG point '+str(therun))
    masses['1000021'] = points2b21s[therun][0]
    masses['1000022'] = points2b21s[therun][1]
    masses['1000024'] = 0.5*(points2b21s[therun][0]+points2b21s[therun][1])
    stringy = str(int(points2b21s[therun][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(points2b21s[therun][1]))
    gentype='GG'
    decaytype='onestepCC'
    njets=1
    use_decays=False
    use_Tauola=False


therun = therun - len(points2b21s)

if therun>=0 and therun<len(points2xextra):
    evgenLog.info('Registered generation of grid seven, one-step GG in X point '+str(therun))
    masses['1000021'] = points2xextra[therun][0]
    masses['1000022'] = mLSP
    masses['1000024'] = points2xextra[therun][1] * (points2xextra[therun][0]-mLSP) + mLSP
    roundmass=int(masses['1000024'])
    if roundmass%5!=0: roundmass=roundmass+1
    stringy = str(int(points2xextra[therun][0]))+'_'+str(roundmass)+'_'+str(int(mLSP))
    gentype='GG'
    decaytype='onestepCC'
    njets=1
    use_decays=False
    use_Tauola=False
    
# gluino x=1/2 grid extension for run-II
pointsggx12run2 = []
toremove = [210,240,270,300,330,410,490,570,650,730,810,890,970,1050,1130,1197.5]
counter=0
for sum in ([0,50,100,150,200]+sumList): # Need to loop over the alternating ones...
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*80 ]
    for diff in diffL:
        counter += 1
        msq = (sum+diff)/2.
        #remove regions already excluded or unreachable
        if msq>2100 or msq-diff<0 or msq-diff>800 or msq < 1500 or msq in toremove: continue
        if msq-diff>600 and (counter%2==0 or msq>1300) or not msq-diff==25: continue
        point = [msq,msq-diff]
        if msq>1200: 
            pointsggx12run2 += [ point ]

therun = runArgs.runNumber - 204639
if therun>=0 and therun<len(pointsggx12run2):
    evgenLog.info('Registered generation of grid five, one-step GG point '+str(therun))
    masses['1000021'] = pointsggx12run2[therun][0]
    masses['1000022'] = pointsggx12run2[therun][1]
    masses['1000024'] = 0.5*(pointsggx12run2[therun][0]+pointsggx12run2[therun][1])
    stringy = str(int(pointsggx12run2[therun][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(pointsggx12run2[therun][1]))
    gentype='GG'
    decaytype='onestepCC'
    njets=1
    use_decays=False
    use_Tauola=False




therun = runArgs.runNumber - 204892
if therun>=0 and therun<len(points2xextraBench):
    evgenLog.info('Registered generation of grid seven, one-step GG in X point '+str(therun))
    masses['1000021'] = points2xextraBench[therun][0]
    masses['1000022'] = mLSP
    masses['1000024'] = points2xextraBench[therun][1] * (points2xextraBench[therun][0]-mLSP) + mLSP
    roundmass=int(masses['1000024'])
    #if roundmass%5!=0: roundmass=roundmass+1
    stringy = str(int(points2xextraBench[therun][0]))+'_'+str(roundmass)+'_'+str(int(mLSP))
    gentype='GG'
    decaytype='onestepCC'
    njets=1
    use_decays=False
    use_Tauola=False

# Systematics
therun = runArgs.runNumber-152752
if therun>=0 and therun<5*5:
    evgenLog.info('Registered generation of grid five systematics, one-step GG point '+str(therun))
    systpoints = [ [325,165], [317,292], [435,115], [205,45], [235,75] ]
    masses['1000021'] = systpoints[therun%5][0]
    masses['1000022'] = systpoints[therun%5][1]
    masses['1000024'] = 0.5*(systpoints[therun%5][0]+systpoints[therun%5][1])
    syst_mod='qup'
    if therun>=5: syst_mod='qdown'
    if therun>=10: syst_mod='radhi'
    if therun>=15: syst_mod='radlo'
    if therun>=20: syst_mod='rad0'
    stringy = str(int(systpoints[therun%5][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(systpoints[therun%5][1]))+'_'+syst_mod
    gentype='GG'
    decaytype='onestepCC'
    njets=1
    use_decays=False

# x-grid systematics
therun = runArgs.runNumber-152817
if therun>=0 and therun<5*8:
    evgenLog.info('Registered generation of grid seven systematics, x-grid one-step GG point '+str(therun))
    systpoints = [ [400,390], [400,375], [400,350], [400,110], [400,70], [200,100], [200,110], [200,190] ]
    masses['1000021'] = systpoints[therun%8][0]
    masses['1000024'] = systpoints[therun%8][1]
    masses['1000022'] = 60
    syst_mod='qup'
    if therun>=8: syst_mod='qdown'
    if therun>=16: syst_mod='radhi'
    if therun>=24: syst_mod='radlo'
    if therun>=32: syst_mod='rad0'
    stringy = str(int(systpoints[therun%8][0]))+'_'+str(int(systpoints[therun%8][1]))+'_60_'+syst_mod
    gentype='GG'
    decaytype='onestepCC'
    njets=1
    use_decays=False


if runArgs.runNumber in [152757, \
                 152759,152760,152761,
                 152825,152826,152827,152828,152830, \
                 152831,152832]:
    evt_multiplier = 4.0

evgenConfig.contact  = [ "genest@lpsc.in2p3.fr" ]
evgenConfig.keywords += ['gluino','one_step']
evgenConfig.description = 'gluino production one step without slepton in simplified model'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

