# Stop pair production, direct three-body decay stop to b W LSP
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

points_TT_directBWN = []
therun = -1
if runArgs.runNumber in range(178414,178473+1):
    therun = runArgs.runNumber-178414
    points_TT_directBWN = [
        [110,30],[110,55],[110,80],[110,100],
        [150,70],[150,95],[150,120],[150,140],
        [200,120],[200,145],[200,170],[200,190],
        [110,30],[110,55],[110,80],[110,100],
        [150,70],[150,95],[150,120],[150,140],
        [200,120],[200,145],[200,170],[200,190],
        [110,30],[110,55],[110,80],[110,100],
        [150,70],[150,95],[150,120],[150,140],
        [200,120],[200,145],[200,170],[200,190],
        [250,170],[250,195],[250,220],[250,240],
        [300,220],[300,245],[300,270],[300,290],
        [350,270],[350,295],[350,320],[350,340],
        [400,320],[400,345],[400,370],[400,390],
        [450,370],[450,395],[450,420],[450,440],
        [500,420],[500,445],[500,470],[500,490],
        ]
elif runArgs.runNumber in range(204674,204685+1):  
    therun = runArgs.runNumber-204674
    points_TT_directBWN = [
        [110,30],[110,55],[110,80],
        [150,70],[150,95],[150,120],
        [200,120],[200,145],[200,170],
        [250,170],[250,195],[250,220],
        ]
elif runArgs.runNumber in range(204941,204941+1):
    therun = runArgs.runNumber-204941
    points_TT_directBWN = [
        [300,220],
        ]
if therun>=0 and therun<len(points_TT_directBWN):
    masses['1000006'] = points_TT_directBWN[therun][0]
    masses['1000022'] = points_TT_directBWN[therun][1]
    stringy = str(int(points_TT_directBWN[therun][0]))+'_'+str(int(points_TT_directBWN[therun][1]))
    gentype = 'TT'
    decaytype = 'directBWN'
    njets=1
    evgenLog.info('Registered generation of stop pair production, stop to b+W+neutralino1: grid point '+str(points_TT_directBWN[therun])+', with '+str(njets)+' jets')
    use_decays = False
    use4bodydecay = True

evt_multiplier = 20.

evgenConfig.contact  = [ "takashi.yamanaka@cern.ch" ]
evgenConfig.keywords += ['stop']
evgenConfig.description = 'stop direct pair production with simplified model'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

