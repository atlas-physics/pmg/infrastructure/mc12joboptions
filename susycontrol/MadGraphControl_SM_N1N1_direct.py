# chi10 pair-production 

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' ) 

points = { 204586 : "65", 204587 : "100", 204588 : "300", 204589 : "500", 204619 : "1" }

therun = runArgs.runNumber
if therun>=0:
    log.info('Registered generation direct N1N1 point '+str(therun))
    masses['1000022'] = int(points[therun])
    stringy = points[therun] 
    gentype='N1N1'
    decaytype='direct'
    njets=1
    use_decays=False

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' ) 
