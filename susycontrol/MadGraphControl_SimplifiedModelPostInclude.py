# This comes after all Simplified Model setup files
from MadGraphControl.MadGraphUtils import *

# Set maximum number of events if the event multiplier has been modified
if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevts=runArgs.maxEvents*evt_multiplier
    else:
        nevts=5000*evt_multiplier

if njets<0:
    evgenLog.fatal('Run number unknown - wrong grid file included?  Will fail...')
    raise RuntimeError('Unknown run number')

# Set up for grid pack running
gridpackDirName=None
if hasattr(runArgs, "inputGenConfFile"):
    gridpackDirName='madevent/'

if 'writeGridpack' not in dir():
    writeGridpack=False

if not SLHAonly:
    if decaytype=='SGGrid':
        [qcut,outputDS] = SUSY_StrongSM_SG_Generation(run_number=runArgs.runNumber,gentype=gentype,masses=masses,stringy=stringy,nevts=nevts,rand_seed=rand_seed,njets=njets,skip_events=skip_events, syst_mod=syst_mod, beamEnergy=beamEnergy, keepOutput=keepOutput)
    elif decaytype == 'SG2NCGrid':
        [qcut,outputDS] = SUSY_StrongSM_SG2NC_Generation(run_number=runArgs.runNumber,gentype=gentype,masses=masses,stringy=stringy,nevts=nevts,rand_seed=rand_seed,njets=njets,skip_events=skip_events, syst_mod=syst_mod, beamEnergy=beamEnergy, keepOutput=keepOutput)
    else:
        # Hook to protect against old version of MadGraphControl, in case the grid pack options do not yet exist
        if SUSY_StrongSM_Generation.func_code.co_argcount>16:
            if usePythia6:
                [qcut,outputDS] = SUSY_StrongSM_Generation(run_number=runArgs.runNumber,gentype=gentype,decaytype=decaytype,masses=masses,stringy=stringy,nevts=nevts,rand_seed=rand_seed, njets=njets, use_decays=use_decays, skip_events=skip_events, syst_mod=syst_mod, beamEnergy=beamEnergy, writeGridpack=writeGridpack, gridpackDirName=gridpackDirName, keepOutput=keepOutput)
            else:
                [qcut,outputDS] = SUSY_StrongSM_Generation(run_number=runArgs.runNumber,gentype=gentype,decaytype=decaytype,masses=masses,stringy=stringy,nevts=nevts,rand_seed=rand_seed, njets=njets, use_decays=use_decays, skip_events=skip_events, syst_mod=syst_mod, beamEnergy=beamEnergy, writeGridpack=writeGridpack, gridpackDirName=gridpackDirName, keepOutput=keepOutput, getnewruncard=True)
        else:
            [qcut,outputDS] = SUSY_StrongSM_Generation(run_number=runArgs.runNumber,gentype=gentype,decaytype=decaytype,masses=masses,stringy=stringy,nevts=nevts,rand_seed=rand_seed, njets=njets, use_decays=use_decays, skip_events=skip_events, syst_mod=syst_mod, beamEnergy=beamEnergy, keepOutput=keepOutput)
else:
    if decaytype=='SGGrid':
        [qcut,outputDS] = SUSY_StrongSM_SG_Generation(run_number=runNumber,gentype=gentype,masses=masses,stringy=stringy,nevts=10000,rand_seed=1234,njets=1,skip_events=1, syst_mod=None, SLHAonly=True, keepOutput=keepOutput)
    elif decaytype=='SG2NCGrid':
        [qcut,outputDS] = SUSY_StrongSM_SG2NC_Generation(run_number=runNumber,gentype=gentype,masses=masses,stringy=stringy,nevts=10000,rand_seed=1234,njets=1,skip_events=1, syst_mod=None, SLHAonly=True, keepOutput=keepOutput)
    else:
        [qcut,outputDS] = SUSY_StrongSM_Generation(run_number=runNumber,gentype=gentype,decaytype=decaytype,masses=masses,stringy=stringy,nevts=10000,rand_seed=1234, njets=1, use_decays=False, skip_events=1, syst_mod=None, SLHAonly=True, keepOutput=keepOutput)
    from exception import Exception
    raise Exception('All done')

if qcut<0 or outputDS is None or ''==outputDS:
    evgenLog.warning('Looks like something went wrong with the MadGraph generation - bailing out!')
    raise RuntimeError('Error in MadGraph generation')

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

runArgs.qcut = qcut
runArgs.inputGeneratorFile = outputDS
if 'syst_mod' in dir():
    runArgs.syst_mod = syst_mod
runArgs.decaytype = decaytype
runArgs.gentype = gentype
runArgs.use_Tauola = use_Tauola
runArgs.use_Photos = use_Photos
runArgs.use_METfilter = use_METfilter
runArgs.use_MultiElecMuTauFilter = use_MultiElecMuTauFilter
runArgs.use_MultiLeptonFilter = use_MultiLeptonFilter
runArgs.pythia_tune = pythia_tune
runArgs.pythia8_tune = pythia8_tune

if usePythia6:
    evgenLog.info('Will use Pythia6...')
    include( 'MC12JobOptions/MadGraphControl_SimplifiedModelPythia6.py' )
else:
    evgenLog.info('Will use Pythia8...')
    if 'mmjj' in dir(): runArgs.mmjj=mmjj
    if 'drjj' in dir(): runArgs.drjj=drjj
    if 'ptj'  in dir(): runArgs.ptj =ptj
    include( 'MC12JobOptions/MadGraphControl_SimplifiedModelPythia8.py' )

