# Direct squark decay to LSP (0-lepton, grid 1 last year)
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

points2a = []
sumList   = [0,50,100,150,200,250,300,350,400,450,500,550,600,750,900,1050,1200,1350,1500,1650,1800,3950,2100,2250]
for sum in sumList:
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*75 ]
    for diff in diffL:
        msq = (sum+diff)/2.
        if msq>1200 or msq-diff<0: continue
        point = [msq,msq-diff]
        points2a += [ point ]

# Standard grid points
therun = runArgs.runNumber-144253
if therun>=0 and therun<len(points2a):
    evgenLog.info('Registered generation of grid one, direct SS point '+str(therun))
    for q in squarks: masses[q] = points2a[therun][0]
    masses['1000022'] = points2a[therun][1]
    stringy = str(int(points2a[therun][0]))+'_'+str(int(points2a[therun][1]))
    gentype='SS'
    decaytype='direct'
    njets=1
    use_decays=False

# 2012 Extension
sumList_SM_SS   = [0,150,300,450,600, 650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1350,1500,1650,1800,1950,2100,2250,2400,2550,2700,2850,3000,3150,3300,3450,3600,3750]
points_SM_SS = []
for sum in sumList_SM_SS:
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*75 ]
    for diff in diffL:
        msq = (sum+diff)/2.
        point = [msq,msq-diff]
        if msq>1600 or msq-diff<0 or msq-diff>1200: continue
        points_SM_SS += [ point ]
therun = runArgs.runNumber-162000
if therun>=0 and therun<len(points_SM_SS):
    evgenLog.info('Registered generation of grid one, direct SS point '+str(therun))
    for q in squarks: masses[q] = points_SM_SS[therun][0]
    masses['1000022'] = points_SM_SS[therun][1]
    stringy = str(int(points_SM_SS[therun][0]))+'_'+str(int(points_SM_SS[therun][1]))
    gentype='SS'
    decaytype='direct'
    njets=1
    use_decays=False

# 2012 Second extention with 
#  - low squark mass points for non-degenerate squarks studies
sumList_SM_SS_extension = [100, 200, 250, 350, 400, 500, 550]
points_SM_SS_extension = []
for sum in sumList_SM_SS_extension:
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*75 ]
    for diff in diffL:
        msq = (sum+diff)/2.
        point = [msq,msq-diff]
        if msq>1600 or msq-diff<0 or msq-diff>1200: continue
        points_SM_SS_extension += [ point ]
#  - iso massless lsp line
for msq in range(100,1210,50):
    if [msq, 0.] in points_SM_SS or [msq, 0.] in points_SM_SS_extension:
        continue
    points_SM_SS_extension.append([msq, 0.])
#  - iso squark mass line (msquark=450 GeV)
for mlsp in range(0,450,50):
    if [450., mlsp] in points_SM_SS or [450., mlsp] in points_SM_SS_extension:
        continue
    points_SM_SS_extension.append([450., mlsp])
therun = runArgs.runNumber-183657
if therun>=0 and therun<len(points_SM_SS_extension):
    evgenLog.info('Registered generation of grid one, direct SS point '+str(therun))
    for q in squarks: masses[q] = points_SM_SS_extension[therun][0]
    masses['1000022'] = points_SM_SS_extension[therun][1]
    stringy = str(int(points_SM_SS_extension[therun][0]))+'_'+str(int(points_SM_SS_extension[therun][1]))
    gentype='SS'
    decaytype='direct'
    njets=1
    use_decays=False

# 2012 Third extention with very compressed spectra
mList=[5,15]#DeltaM
MList=range(50, 1201, 50)#SumM
points_SM_SS_compressed = []
for m in mList:
    for M in MList:
        msq   =int((m+M)/2)
        mneu1 =int((M-m)/2)
        if msq < 70: continue
        if mneu1 < 0: continue
        point = [msq, mneu1]
        points_SM_SS_compressed += [ point ]
therun = runArgs.runNumber - 186854
if therun >= 0 and therun < len(points_SM_SS_compressed):
    evgenLog.info('Registered generation of grid one, direct SS point '+
                  str(therun))
    for q in squarks: masses[q] = points_SM_SS_compressed[therun][0]
    masses['1000022'] = points_SM_SS_compressed[therun][1]
    stringy = str(int(points_SM_SS_compressed[therun][0])) + '_' + \
              str(int(points_SM_SS_compressed[therun][1]))
    gentype='SS'
    decaytype='direct'
    njets=1
    use_decays=False
    msq = points_SM_SS_compressed[therun][0]
    if msq < 150: # Generate 500 events
        evt_multiplier = 15.0
    elif msq < 300: # Generate 1000 events
        evt_multiplier = 6.0
    else: # Generate 5000 events
        evt_multiplier = 12.0

# 2012 4th extention with very compressed spectra
points_SM_SS_compressed_ext = []
#deltaM=5GeV
for msq in range(625,750,25):
    mlsp =int(msq-5)
    point = [msq, mlsp]
    points_SM_SS_compressed_ext += [ point ]
#deltaM=15GeV
point = [725, 710]
points_SM_SS_compressed_ext += [ point ]
therun = runArgs.runNumber - 204749
if therun >= 0 and therun < len(points_SM_SS_compressed_ext):
    evgenLog.info('Registered generation of grid one, direct SS point '+
                  str(therun))
    for q in squarks: masses[q] = points_SM_SS_compressed_ext[therun][0]
    masses['1000022'] = points_SM_SS_compressed_ext[therun][1]
    stringy = str(int(points_SM_SS_compressed_ext[therun][0])) + '_' + \
              str(int(points_SM_SS_compressed_ext[therun][1]))
    gentype='SS'
    decaytype='direct'
    njets=1
    use_decays=False
    msq = points_SM_SS_compressed_ext[therun][0]
    if msq < 150: # Generate 500 events
        evt_multiplier = 15.0
    elif msq < 300: # Generate 1000 events
        evt_multiplier = 6.0
    else: # Generate 5000 events
        evt_multiplier = 12.0


# mc12_14TeV samples for upgrade studies
points_SM_SS_14TeV = {186348 : "1050_1", 186349 : "1350_1", 186350 : "1650_1", 186351 : "1950_1", 186352 : "2250_1", 186353 : "2550_1", 186354 : "2850_1", 186355 : "3150_1", 186356 : "1200_300", 186357 : "1500_300", 186358 : "1800_300", 186359 : "2100_300", 186360 : "2400_300", 186361 : "2700_300", 186362 : "3000_300", 186363 : "750_600", 186364 : "1050_600", 186365 : "1350_600", 186366 : "1650_600", 186367 : "1950_600", 186368 : "2250_600", 186369 : "2550_600", 186370 : "2850_600", 186371 : "1200_900", 186372 : "1050_900", 186373 : "1500_900", 186374 : "1800_900", 186375 : "2100_900", 186376 : "2400_900", 186377 : "2700_900", 186378 : "3000_900", 186379 : "1350_1200", 186380 : "1650_1200", 186381 : "1950_1200", 186382 : "2250_1200", 186383 : "2550_1200", 186384 : "2850_1200", 186385 : "1800_1500", 186386 : "1650_1500", 186387 : "2100_1500", 186388 : "2400_1500", 186389 : "2700_1500", 186390 : "3000_1500", 186391 : "1950_1800", 186392 : "2250_1800", 186393 : "2550_1800", 186394 : "2850_1800", 186395 : "2400_2100", 186396 : "2250_2100", 186397 : "2700_2100", 186398 : "3000_2100", 186399 : "2550_2400", 186400 : "2850_2400"}

therun = runArgs.runNumber
if therun>=0 and therun in points_SM_SS_14TeV:
    evgenLog.info('Registered generation of grid one, direct SS point '+str(therun))
    p = points_SM_SS_14TeV[therun].split("_")
    for q in squarks: masses[q] = p[0]
    masses['1000022'] = p[1]
    stringy = points_SM_SS_14TeV[therun]
    gentype='SS'
    decaytype='direct'
    njets=1
    use_decays=False


points_SM_SS_13TeV_benchmark = {204917:"900_0", 204918:"1050_0", 204919:"1350_0", 204920:"1650_0", 204921:"700_250", 204922: "700_400", 204923: "700_650", 204924:"537_512"}

therun = runArgs.runNumber
if therun in points_SM_SS_13TeV_benchmark.keys():
    evgenLog.info('Registered generation of grid two, direct SS point '+str(therun))
    p = points_SM_SS_13TeV_benchmark[therun].split("_")
    for q in squarks: masses[q] = p[0]
    masses['1000022'] = p[1] # lsp
    masses['1000021'] = 450000.0 # gluino    
    stringy = points_SM_SS_13TeV_benchmark[therun]
    gentype='SS'
    decaytype='direct'
    njets=1
    use_decays=False

    
# Systematics
therun = runArgs.runNumber-152647
if therun>=0 and therun<8*5:
    systpoints = [ [137,62], [137,112], [325,25], [325,175], [325,275], [600,0], [600,300], [625,575] ]
    evgenLog.info('Registered generation of squark grid one systematic uncertainty point '+str(therun))
    for q in squarks: masses[q] = systpoints[therun%8][0]
    masses['1000022'] = systpoints[therun%8][1]
    syst_mod='qup'
    if therun>=8: syst_mod='qdown'
    if therun>=16: syst_mod='radhi'
    if therun>=24: syst_mod='radlo'
    if therun>=32: syst_mod='rad0'
    stringy = str(int(systpoints[therun%8][0]))+'_'+str(int(systpoints[therun%8][1]))+'_'+syst_mod
    gentype='SS'
    decaytype='direct'
    njets=1
    use_decays=False

if runArgs.runNumber in [152655,152656]:
    evt_multiplier = 4.0

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

