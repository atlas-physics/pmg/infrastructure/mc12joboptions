from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.Gluino_Stop_charm')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_pMSSM_qL_to_h_mc12points.py' )
try:
    (M1, M2, mqL1) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

slha_file = "susy_pMSSM_qL-H-N2-%d_%d_%d.slha" % (M1, M2, mqL1)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['squarks_L'], slha_file, 'TwoParticleInclusive')
cmds += "insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/H0\n"
cmds += "insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/H+\n"
cmds += "insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/H-\n"
cmds += "insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/A0\n"

# define metadata
evgenConfig.description = 'pMSSM qL->chi20->h grid generation with M1 = %d, M2 = %d GeV, mqL1 = mqL2 = %d GeV' % (M1, M2, mqL1)
evgenConfig.keywords = ['SUSY','pMSSM', 'squark', 'higgs']
evgenConfig.contact  = ['scaron@nikhef.nl','geert-jan.besjes@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
