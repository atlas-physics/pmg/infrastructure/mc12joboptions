def slhaFileFromRunNumber(runNumber, offset):
  include('MC12JobOptions/SUSY_RPVUDD_StopGluino_points.py')
  p = rpvUDDStopGluinoPoints
  redoList = [
      p[13], p[14], p[15], p[16], p[17]
      ]

  index = int(runNumber) - int(offset)
  return redoList[index]

# Start of the run number range
firstRunNumber = 177261

slhaFile = slhaFileFromRunNumber(runArgs.runNumber, firstRunNumber)
print 'Using slha:',slhaFile

evgenConfig.description = 'RPV UDD Stop-Gluino Model, slha file: {0}'.format(slhaFile)
evgenConfig.keywords = ['SUSY','RPV']
evgenConfig.contact  = ['thomas.gillam@cern.ch']

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")
include("MC12JobOptions/Herwigpp_SUSYConfig.py")

## Add Herwig++ parameters for this process
cmds = buildHerwigppCommands(['gluino'], slhaFile, 'TwoParticleInclusive')

print '*** Begin Herwig++ SUSY commands ***'
print cmds
print '*** End Herwig++ SUSY commands ***'

## Append our commands
topAlg.Herwigpp.Commands += cmds.splitlines()

## Clean up
del cmds
