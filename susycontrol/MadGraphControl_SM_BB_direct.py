# Sbottom pair-production with direct decays to bottom-LSP
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )


#Assigned run numbers for direct sbottom->b+LSP @ 14 TeV, DS 202248-202300 (53). 
l_points_BB_directBB_14TeV = [[200, 190], [300, 290], [400, 300], [400, 350], [400, 390], [500, 300], [500, 400], [500, 450], [500, 490], [600, 1], [600, 200], [600, 400], [700, 1], [700, 200], [700, 400], [700, 600], [700, 650], [700, 690], [800, 1], [800, 200], [800, 400], [800, 600], [900, 1], [900, 200], [900, 400], [900, 600], [900, 800], [900, 850], [900, 890], [1000, 1], [1000, 400], [1000, 800], [1200, 1], [1200, 400], [1200, 800], [1400, 1], [1400, 400], [1400, 800], [1400, 1200], [1600, 1], [1600, 400], [1600, 800], [1600, 1200], [1800, 1], [1800, 400], [1800, 800], [1800, 1200], [1800, 1600], [2000, 1], [2000, 400], [2000, 800], [2000, 1200], [2000, 1600]]

extra_points_DC14 = [ [250,245], [275,270], [300,295], [400,395], [500,495] ]

njets=1

therun = runArgs.runNumber - 205080


if therun>=0 and therun<5:
    masses['1000005'] = extra_points_DC14[therun][0]
    masses['1000022'] = extra_points_DC14[therun][1]
    m_sbottom = extra_points_DC14[therun][0]
    m_LSP = extra_points_DC14[therun][1]
    
    njets=2
    
    stringy = str(int(extra_points_DC14[therun][0]))+'_'+str(int(extra_points_DC14[therun][1]))
    
    use_METfilter=True
    runArgs.METfilterCut = 80.0

    stringy += '_METfilter80GeV'

    evgenLog.info('Registered generation of grid thirteen at 13/14 TeV, direct (to bbbar) BB point '+str(therun) + ', masses ' + str(extra_points_DC14[therun]) + ', nr jets: ' + str(njets))


therun = runArgs.runNumber - 202248

if therun>=0 and therun<53:
    masses['1000005'] = l_points_BB_directBB_14TeV[therun][0]
    masses['1000022'] = l_points_BB_directBB_14TeV[therun][1]
    m_sbottom = l_points_BB_directBB_14TeV[therun][0]
    m_LSP = l_points_BB_directBB_14TeV[therun][1]
    
    njets=2
    
    stringy = str(int(l_points_BB_directBB_14TeV[therun][0]))+'_'+str(int(l_points_BB_directBB_14TeV[therun][1]))
    
    use_METfilter=True
    runArgs.METfilterCut = 50.0

    stringy += '_METfilter50GeV'

    evgenLog.info('Registered generation of grid thirteen at 14 TeV, direct (to bbbar) BB point '+str(therun) + ', masses ' + str(l_points_BB_directBB_14TeV[therun]) + ', nr jets: ' + str(njets))


#Assigned run numbers for direct sbottom->b+LSP, DS 164656 to 164890 (235). 
therun = runArgs.runNumber - 164656
import SUSY_BB_mc12points
l_points_BB_directBB = SUSY_BB_mc12points.l_points_BB_directBB
if therun>=0 and therun<235:
    masses['1000005'] = l_points_BB_directBB[therun][0]
    masses['1000022'] = l_points_BB_directBB[therun][1]
    m_sbottom = l_points_BB_directBB[therun][0]
    m_LSP = l_points_BB_directBB[therun][1]
    stringy = str(int(l_points_BB_directBB[therun][0]))+'_'+str(int(l_points_BB_directBB[therun][1]))
    if therun in range(0,235)[-4:]:
        stringy += '_noMETfilter'
        use_METfilter = False
    else:
        stringy += '_METfilter50GeV'
        use_METfilter=True
        runArgs.METfilterCut = 50.0
    evgenLog.info('Registered generation of grid thirteen, direct (to bbbar) BB point '+str(therun) + ', masses ' + str(l_points_BB_directBB[therun]) + ', nr jets: ' + str(njets))

points_BB_directBB = []
for m_sb in [250,350]:
    for deltaM in [10,30,50,100]:
        points_BB_directBB.append((m_sb,m_sb-deltaM))

# Assigned run numbers the sbottom->b+LSP points for systematic uncertainties: 177175 to 177238
# these cover scalefact, alpsfact, qscale variations and the old recommendations for radiation (FSR)
therun = runArgs.runNumber-177175
if therun>=0 and therun<(len(points_BB_directBB)*8):
    m_sb = points_BB_directBB[therun/8][0]
    m_LSP = points_BB_directBB[therun/8][1]
    masses['1000005'] = m_sb
    masses['1000022'] = m_LSP
    runArgs.syst_mod=dict_index_syst[therun%8]
    str_info_METfilter = "80 GeV"
    str_stringy_METfilter = "MET80"
    runArgs.METfilterCut = 80.0
    use_METfilter=True
    stringy = str(int(points_BB_directBB[therun/2][0]))+'_'+str(int(points_BB_directBB[therun/2][1]))+'_'+str_stringy_METfilter+'_'+runArgs.syst_mod
    evgenLog.info('Registered generation of sbottom pair production, sbottom to b+LSP; grid point '+str(therun)+' decoded into mass point ' + str(points_BB_directBB[therun/2]) + ', with ' + str(njets) + ' jets. Using MET filter ' +  str_info_METfilter + ' and systematic variation of type '+runArgs.syst_mod)

# Assigned run numbers the sbottom->b+LSP points for systematic uncertainties: 1775681-177601
# These cover the FSR up/down new recommendations
therun = runArgs.runNumber-177586
if therun>=0 and therun<(len(points_BB_directBB)*2):
    dict_index_syst = {0:'moreFSR',
                       1:'lessFSR'}
    m_sb = points_BB_directBB[therun/2][0]
    m_LSP = points_BB_directBB[therun/2][1]
    masses['1000005'] = m_sb
    masses['1000022'] = m_LSP
    runArgs.syst_mod=dict_index_syst[therun%2]
    str_info_METfilter = "80 GeV"
    str_stringy_METfilter = "MET80"
    runArgs.METfilterCut = 80.0
    use_METfilter=True
    stringy = str(int(points_BB_directBB[therun/2][0]))+'_'+str(int(points_BB_directBB[therun/2][1]))+'_'+str_stringy_METfilter+'_'+runArgs.syst_mod
    evgenLog.info('Registered generation of sbottom pair production, sbottom to b+LSP; grid point '+str(therun)+' decoded into mass point ' + str(points_BB_directBB[therun/2]) + ', with ' + str(njets) + ' jets. Using MET filter ' +  str_info_METfilter + ' and systematic variation of type '+runArgs.syst_mod)


# If we hit, then set a bunch of common things
if len(masses.keys())>0:
    gentype='BB'
    decaytype='direct'
    use_decays=False
    use_Tauola=False
    deltaM = m_sb - m_LSP
    if (deltaM<20.1):
        evt_multiplier = 20.0
    elif (deltaM<50.1):
        evt_multiplier = 15.0
    else:
        evt_multiplier = 10.0

#####################################################################
#                  MadGraph+Pythia8 Example                         # 
#####################################################################
points_BB_directBB = [
    [300,290]
    ]
therun = runArgs.runNumber-1000000 # larger than any valid DSID, just for illustration
if therun>=0 and therun<len(points_BB_directBB):
    m_sb = points_BB_directBB[therun][0]
    m_LSP = points_BB_directBB[therun][1]
    masses['1000005'] = m_sb
    masses['1000022'] = m_LSP
    stringy = str(m_sb)+'_'+str(m_LSP)
    gentype = 'BB'
    decaytype = 'direct'
    njets = 1 
    use_Photos = False
    use_Tauola = False # Should not be used with Pythia8
    use_decays = False
    usePythia6 = False
    pythia8_tune = 'A14_CTEQ6L1'
    doKtMerge = True
    evgenLog.info('Registered generation of sbottom pair production, sbottom to b+LSP; grid point '+str(therun)+' decoded into mass point ' + str(m_sb) + ', with ' + str(njets) + ' jets.')
######################################################################

evgenConfig.contact  = [ "kerim.suruliz@cern.ch" ]
evgenConfig.keywords += ['sbottom', 'signal_uncertainties']
evgenConfig.description = 'sbottom direct pair production, sb->b+LSP in simplified model, signal uncertainties'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )
