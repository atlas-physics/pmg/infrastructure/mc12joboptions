templatefile = 'susy_RPVMSSM_SM_DirectStop_template.slha'

evgenConfig.description = 'RPV direct stop generation - simplified model stop->bs '
evgenConfig.generators  = ['Herwigpp']
evgenConfig.keywords    = ['SUSY','RPV','stop']
evgenConfig.contact     = ['David.W.Miller@cern.ch']
evgenConfig.auxfiles   += ['susy_RPVMSSM_SM_DirectStop_template.slha', 'MSSM.model'] 

import os

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")
include("MC12JobOptions/Herwigpp_SUSYConfig.py")

## Add Herwig++ parameters for this process
sparticle_list = []
sparticle_list.append('stop1')
slha_file = templatefile
cmds = buildHerwigppCommands(sparticle_list, slha_file,'TwoParticleInclusive', earlyCopy=True)

cmds += """
# Protect against A non-integrable singularity or other bad integrand behavior was found in the integration interval for offshell particles
set /Herwig/NewPhysics/NewModel:WhichOffshell Selected
"""

print '*** Begin Herwig++ SUSY commands ***'
print cmds
print '*** End Herwig++ SUSY commands ***'

# define spectrum file name
include( 'MC12JobOptions/SUSY_RPV_UDD_DirectStop_mc12points.py' )
try:
    Mstop = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

if not os.access(templatefile,os.R_OK):
    print 'ERROR: Could not get slha file'
else:
    oldcard = open(templatefile,'r')
    newcard = open('spectrum.slha','w')
    for line in oldcard:
        if '   1000006     100              # ~t_1' in line:
            newcard.write('   1000006     %s              # ~t_1\n'%(Mstop))
            print '*** WRITING Mstop = %s' % (Mstop)
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()
    os.rename('spectrum.slha',templatefile)

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Generator Filter
#include("MC12JobOptions/ParentChildFilter.py")
#topAlg.ParentChildFilter.PDGParent  = [1000006]
#topAlg.ParentChildFilter.PtMinParent =  0.0 # MeV
#topAlg.ParentChildFilter.EtaRangeParent = 1000.
#topAlg.ParentChildFilter.PDGChild = [5]
#topAlg.ParentChildFilter.PtMinChild = 1000. # MeV
#topAlg.ParentChildFilter.EtaRangeChild = 1000.

del cmds





