## Herwig++ job option file for Susy 2-parton -> 2-sparticle processes

## Get a handle on the top level algorithms' sequence
from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.SMModeCDG')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

## Setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
if   ((runArgs.runNumber >= 204000 and runArgs.runNumber <= 204025) or
      (runArgs.runNumber >= 204643 and runArgs.runNumber <= 204647)):
    slha_file = 'susy_simplifiedModel_wC_sl_noWcascade_%s.slha' % (runArgs.runNumber)
elif (runArgs.runNumber >= 204056 and runArgs.runNumber <= 204075):
    slha_file = 'susy_simplifiedModel_wC_stau_noWcascade_%s.slha' % (runArgs.runNumber)
else:
    raise RuntimeError('Common file not defined for DSID %s. Aborting!' % runArgs.runNumber)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )

# Add Herwig++ parameters for this process
cmds = buildHerwigppCommands(['~chi_1+','~chi_1-'],slha_file,'Exclusive')

## Define metadata
evgenConfig.description = 'Simplified Model Mode C grid generation for direct gaugino search...'
evgenConfig.keywords    = ['SUSY','Direct Gaugino','Simplified Models','chargino pair production']
evgenConfig.contact     = ['alaettin.serhan.mete@cern.ch']

## Print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Generator Filter if used
if (runArgs.runNumber >= 204056 and runArgs.runNumber <= 204075):

    include ( 'MC12JobOptions/MultiElecMuTauFilter.py' )

    topAlg += MultiElecMuTauFilter(
        name = 'DileptonFilter',
        NLeptons  = 2,
        MinPt = 5000,
        MaxEta = 2.7,
        MinVisPtHadTau = 15000,
        IncludeHadTaus = 1
    )
    topAlg += MultiElecMuTauFilter(
        name = 'TauFilter',
        NLeptons  = 1,
        MinPt = 1e10,
        MaxEta = 2.7,
        MinVisPtHadTau = 15000,
        IncludeHadTaus = 1
    )

    StreamEVGEN.RequireAlgs = [ "DileptonFilter","TauFilter" ] 

if (runArgs.runNumber >= 204643 and runArgs.runNumber <= 204647):

    include ( 'MC12JobOptions/MultiElecMuTauFilter.py' )

    topAlg += MultiElecMuTauFilter(
        name = 'DileptonFilter',
        NLeptons  = 2,
        MinPt = 5000,
        MaxEta = 2.7,
        MinVisPtHadTau = 1e10,
        IncludeHadTaus = 0
    )

    StreamEVGEN.RequireAlgs = [ "DileptonFilter" ] 

## Clean up
del cmds
