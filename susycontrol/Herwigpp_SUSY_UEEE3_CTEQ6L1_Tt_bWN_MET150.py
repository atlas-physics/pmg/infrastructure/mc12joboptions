# SUSY Herwig++ jobOptions for direct stop pair production grid
# Use SUSYHIT and bottom-up schema for SLHA files

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.Stop')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

include ( 'MC12JobOptions/SUSY_Tt_bWN_MET150_mc12points.py' )
try:
  (mstop, mlsp) = pointdict[runArgs.runNumber]
except:
  raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
# define spectrum file name
slha_file = 'susy_Tt_bWN_t%03d_n%03d.slha' % (mstop, mlsp)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['stop1'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'stop grid generation'
evgenConfig.keywords = ['SUSY','stop']
evgenConfig.contact  = ['Takashi.Yamanaka@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds

#============================================================
#
# Filter events
#
#############################################################

## Include METFilter
include ( 'MC12JobOptions/METFilter.py' )
topAlg.METFilter.MissingEtCut = 150*GeV

#==============================================================
#
# End of job options file
#
###############################################################

