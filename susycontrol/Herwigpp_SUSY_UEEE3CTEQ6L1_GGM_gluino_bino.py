# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

masses = runArgs.jobConfig[0].split('.py')[0].split('_')[-2:]

# define spectrum file name
slha_file = 'susy_gl_bino_%s_%s.slha'%(masses[0],masses[1])

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GGM wino-bino grid generation with mgl=%s mbino=%s'%(masses[0],masses[1])
evgenConfig.keywords = ['SUSY', 'GGM', 'gluino', 'bino']
evgenConfig.contact = [ 'susan.fowler@cern.ch']

# Particle Filters

# Bino Filter - filtering for events with two binos only

from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
topAlg += ParticleFilter()
ParticleFilter = topAlg.ParticleFilter
ParticleFilter.Ptcut = 0.0
ParticleFilter.Etacut = 10.0
ParticleFilter.PDG = 1000022
ParticleFilter.MinParts = 2
ParticleFilter.StatusReq = 11
StreamEVGEN.RequireAlgs += [ "ParticleFilter" ]

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds
