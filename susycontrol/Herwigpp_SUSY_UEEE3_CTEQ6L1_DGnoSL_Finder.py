# SUSY Herwig++ jobOptions for pMSSM
# Use bottom-up schema for SLHA files

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.pMSSM')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

include ( 'MC12JobOptions/SUSY_pMSSM_DGnoSL_mc12points.py' ) 
try:
    (TB,M1,M2,mu) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)            

# define spectrum file name
slha_file = 'susy_DGnoSL_TB%s_M1M2MU_%s_%s_%s.slha' %(TB,M1,M2,mu)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
InitialSparticles = ['neutralinos','charginos'] 
OneOrTwoParticleInclusive = 'TwoParticleInclusive'
cmds = buildHerwigppCommands(InitialSparticles, slha_file, OneOrTwoParticleInclusive)

# define metadata
evgenConfig.description = 'EWK SUSY no-slepton pMSSM grid, 2-lepton filter; slha file: %s' %(slha_file)
evgenConfig.keywords = ['SUSY','MSSM','pMSSM','EWK','Direct neutralino/chargino']
evgenConfig.contact  = ['borge.gjelsten@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Generator Filter
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
from GeneratorFilters.GeneratorFiltersConf import SusySubprocessFinder
from AthenaCommon.AlgSequence import AthSequencer

topAlg += AthSequencer('FilterSeq')

before = SusySubprocessFinder('BeforeFilter')
before.BeforeFilter = True
after = SusySubprocessFinder('AfterFilter')
after.BeforeFilter = False

topAlg.FilterSeq += before
topAlg.FilterSeq += MultiElecMuTauFilter()
topAlg.FilterSeq += after

MultiElecMuTauFilter = topAlg.FilterSeq.MultiElecMuTauFilter
if runArgs.runNumber<186100:
    MultiElecMuTauFilter.NLeptons = 2
else:
    MultiElecMuTauFilter.NLeptons = 3
MultiElecMuTauFilter.MinPt = 5000.
MultiElecMuTauFilter.MaxEta = 2.7
MultiElecMuTauFilter.MinVisPtHadTau = 15000.  # pt-cut on the visible hadronic tau
MultiElecMuTauFilter.IncludeHadTaus = 1      # include hadronic taus

StreamEVGEN.RequireAlgs = [ 'MultiElecMuTauFilter' ]

# clean up
del cmds
