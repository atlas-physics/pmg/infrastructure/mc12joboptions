# Direct squark production with photon filter 
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

pointsSSphot = []
massS=[100,150,200,250,300]
deltaM=[1,5,10]
for m in massS:
    for dm in deltaM:
        point = [m,m-dm]
        pointsSSphot += [ point ]
massS=[87.5,162.5,237.5]
deltaM=[25]
for m in massS:
    for dm in deltaM:
        point = [m,m-dm]
        pointsSSphot += [ point ]
massS=[100,175,250]
deltaM=[50]
for m in massS:
    for dm in deltaM:
        point = [m,m-dm]
        pointsSSphot += [ point ]

squarks = []
for anum in [1,2,3,4]:
    squarks += [str(1000000+anum),str(-1000000-anum),str(2000000+anum),str(-2000000-anum)]

# Assigning run numbers
therun = runArgs.runNumber-179549
if therun>=0 and therun<len(pointsSSphot):
    evgenLog.info('Registered generation of grid with direct SS with photon point '+str(therun))
    for q in squarks: masses[q] = pointsSSphot[therun][0]
    masses['1000022'] = pointsSSphot[therun][1]
    stringy = str(int(pointsSSphot[therun][0]))+'_'+str(int(pointsSSphot[therun][1]))
    gentype='SSphot'
    decaytype='direct'
    njets=1
    use_decays=False
    use_Tauola=False
    evt_multiplier=4

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

