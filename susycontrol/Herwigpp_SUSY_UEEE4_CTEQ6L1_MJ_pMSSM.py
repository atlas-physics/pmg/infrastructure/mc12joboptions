# SUSY Herwig++ jobOptions for pMSSM with gluino, higgsinos and bino
# Use SUSYHIT and bottom-up schema for SLHA files

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.pMSSM')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_pMSSM_MJ_gl1200_ch200.slha'

# only 4 points in MC12 so don't need to use points file
if runArgs.runNumber == 205151:
    slha_file = 'susy_pMSSM_MJ_gl1200_ch500.slha'
if runArgs.runNumber == 205152:
    slha_file = 'susy_pMSSM_MJ_gl1400_ch200.slha'
if runArgs.runNumber == 205153:
    slha_file = 'susy_pMSSM_MJ_gl1400_ch500.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'Natural pMSSM grid generation with gluino, higgsinos and bino'
evgenConfig.keywords = ['SUSY','gluino','pMSSM']
evgenConfig.contact  = ['christopher.young@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds

#==============================================================
#
# End of job options file
#
###############################################################

