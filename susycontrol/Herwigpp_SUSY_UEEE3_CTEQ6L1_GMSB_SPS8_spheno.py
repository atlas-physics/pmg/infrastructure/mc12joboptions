
# Lambda, neutralino life time
pointdict = {
    # prompt photon
    178030: (100, 0),
    178031: (150, 0), 178032: (160, 0), 178033: (170, 0), 178034: (180, 0), 178035: (190, 0),
    178036: (200, 0), 178037: (210, 0), 178038: (220, 0), 178039: (230, 0), 178040: (240, 0),
    178041: (250, 0), 178042: (260, 0), 178043: (270, 0), 178044: (280, 0), 178045: (290, 0),
    178046: (300, 0), 178047: (310, 0), 178048: (320, 0), 178049: (330, 0), 178050: (340, 0),
    178051: (350, 0), 178052: (360, 0), 178053: (380, 0), 178054: (400, 0),
    # scan
    178055: (220, 0.01), 178056: (220, 0.02), 178057: (220, 0.05),
    178058: (220, 0.1),  178059: (220, 0.2),  178408: (220, 0.5),
    178409: (220, 1),                         178410: (220, 5),
    # non-pointing photon - 6ns
    178000: ( 70, 6), 178001: ( 80, 6), 178002: ( 90, 6),
    178003: (100, 6), 178004: (110, 6), 178005: (120, 6), 178006: (130, 6), 178007: (140, 6),
    178008: (150, 6), 178009: (160, 6),
    # non-pointing photon - 2ns
    178010: (150, 2), 178011: (160, 2), 178012: (170, 2), 178013: (180, 2), 178014: (190, 2),
    178015: (200, 2), 178016: (210, 2), 178017: (220, 2), 178018: (230, 2), 178019: (240, 2),
    178020: (250, 2), 178021: (260, 2), 178022: (270, 2), 178023: (280, 2), 178024: (290, 2),
    178025: (300, 2), 178026: (310, 2), 178027: (320, 2), 178028: (350, 2), 178029: (400, 2),
    }

if 'runArgs' not in dir():
    print 'Total number of defined points:', len(pointdict)
    print pointdict
    import sys
    sys.exit(1)

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.GMSB SPS8')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

try:
    Lambda, lifetime = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

# define spectrum file name
slha_file = 'susy_gmsb_%s_%s_1_15_1_1_spheno.slha'  % (Lambda, 2*Lambda)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['all'], slha_file, earlyCopy=(lifetime!=0))

if lifetime != 0:
    # specialConfig params for simulation: neutralino1 = 1000022, assumption that smuonR is mass degenerated with selectronR
    include ( 'MC12JobOptions/SUSYMetadata.py' )
    (m_neutralino, m_gravitino) = mass_extract(slha_file, ['1000022', '1000039'])

    # remove neutralino decays from slha file
    remove_decay(slha_file, '1000022')

# define metadata
evgenConfig.description = 'SUSY GMSB SPS8 grid: Lambda=%s, life time=%s ns' % (Lambda, lifetime)
evgenConfig.keywords = ['SUSY', 'GMSB', 'SPS8']
evgenConfig.contact = [ 'wolfgang.ehrenfeld@cern.ch']
if lifetime != 0:
    evgenConfig.specialConfig = 'GMSBIndex=1;GMSBNeutralino=%s*GeV;GMSBGravitino=%s*GeV;GMSBLifeTime=%s*ns;preInclude=SimulationJobOptions/preInclude.GMSB.py' % (m_neutralino,m_gravitino, lifetime)

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()
topAlg.Herwigpp.InFileDump = 'herwigpp.config.sps8.%s_%s.new' % (Lambda, lifetime)


# Clean up
if lifetime !=0:
    del m_neutralino, m_gravitino
del cmds, Lambda, lifetime, slha_file
