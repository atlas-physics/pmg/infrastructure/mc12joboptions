## SUSY Herwig++ jobOptions for simplified model Gtt
## ==> A copy of Gtt for OnShell Gtt !!!
##

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.GttOnShell')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include( 'MC12JobOptions/SUSY_GttOnShell_mc12points.py' )
try:
    mgl, mstop = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_GttOnShell_G%s_T%s_N60.slha' % (mgl, mstop)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GttOnShell grid generation with m_gluino = %s GeV, m_LSP = 60 GeV, m_stop = %s GeV' % (mgl,mstop)
evgenConfig.keywords = ['SUSY','Gtt','stop','gluino']
evgenConfig.contact  = ['anna.sfyrla@cern.ch, antoine.marzin@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
