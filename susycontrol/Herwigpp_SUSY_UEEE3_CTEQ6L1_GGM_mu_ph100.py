## SUSY Herwig++ jobOptions for GGM mu grid
## Author: Martin Tripiana (tripiana@cern.ch)

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.GGM_mu')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# Set up Herwig++
include('MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )
include('MC12JobOptions/Herwigpp_SUSYConfig.py')

# define spectrum file name
include ( 'MC12JobOptions/SUSY_GGM_mu_mc12points.py' )
try:
    mu = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_GGM_mu_%s.slha' % (mu)


## Add Herwig++ parameters for this process
cmds = buildHerwigppCommands(['gauginos'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GGM EWK grid generation'
evgenConfig.generators = ['Herwigpp']
evgenConfig.keywords = ['SUSY','GGM','EWK','neutralino','SP']
evgenConfig.contact  = ['tripiana@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds

#--------------------------------------------------------------------
#Filter (high pt photon)
#--------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import LeadingPhotonFilter
topAlg += LeadingPhotonFilter()

LeadingPhotonFilter = topAlg.LeadingPhotonFilter
LeadingPhotonFilter.PtMin = 100000.
LeadingPhotonFilter.EtaCut = 2.5

StreamEVGEN.RequireAlgs = [ "LeadingPhotonFilter" ]
