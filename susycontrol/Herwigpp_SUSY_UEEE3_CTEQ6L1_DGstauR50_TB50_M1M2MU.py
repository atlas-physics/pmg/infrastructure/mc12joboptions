# SUSY Herwig++ jobOptions for EW pMSSM
# Use SUSYHIT and bottom-up schema for SLHA files 

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.pMSSM')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_DGstauR50_TB50_M1M2MU_mc12points.py' )
try:
    (m1, m2, mu) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_DGstauR50_TB50_M1M2MU_%s_%s_%s.slha' % (m1, m2, mu)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['neutralinos','charginos','stau1'], slha_file, 'TwoParticleInclusive') 

# define metadata
evgenConfig.description = "ElWeak production, slha file: susy_DGstauR50_TB50_M1M2MU_75_100_100.slha"
evgenConfig.keywords = ['SUSY','MSSM','pMSSM','DirectGaugino','DGstauR50','chargino','neutralino','stau']
evgenConfig.contact  = ['borge.gjelsten@cern.ch']
evgenConfig.auxfiles += [ slha_file, 'MSSM.model' ]

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()



# Generator Filter
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
from GeneratorFilters.GeneratorFiltersConf import SusySubprocessFinder
from AthenaCommon.AlgSequence import AthSequencer

topAlg += AthSequencer("FilterSeq")

before = SusySubprocessFinder("BeforeFilter")
before.BeforeFilter = True
after = SusySubprocessFinder("AfterFilter")
after.BeforeFilter = False

topAlg.FilterSeq += before
topAlg.FilterSeq += MultiElecMuTauFilter()
topAlg.FilterSeq += after


#topAlg += MultiElecMuTauFilter()
#MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter

MultiElecMuTauFilter = topAlg.FilterSeq.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 2
MultiElecMuTauFilter.MinPt = 5000.
MultiElecMuTauFilter.MaxEta = 2.7
MultiElecMuTauFilter.MinVisPtHadTau = 15000.  # pt-cut on the visible hadronic tau
MultiElecMuTauFilter.IncludeHadTaus = 1      # include hadronic taus 

StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]


# clean up
del cmds
