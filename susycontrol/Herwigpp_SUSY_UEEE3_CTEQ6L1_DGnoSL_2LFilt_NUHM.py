# SUSY Herwig++ jobOptions for NUHM

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.NUHM')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
pointdict = {202642: (100, 100),
             202643: (100, 150),
             202644: (100, 200),
             202645: (100, 250),
             202646: (100, 300),
             202647: (100, 350),
             202648: (100, 400),
             202649: (100, 450),
             202650: (100, 500),
             202651: (150, 100),
             202652: (150, 150),
             202653: (150, 200),
             202654: (150, 250),
             202655: (150, 300),
             202656: (150, 350),
             202657: (150, 400),
             202658: (150, 450),
             202659: (150, 500),
             202660: (200, 100),
             202661: (200, 150),
             202662: (200, 200),
             202663: (200, 250),
             202664: (200, 300),
             202665: (200, 350),
             202666: (200, 400),
             202667: (200, 450),
             202668: (200, 500),
             202669: (250, 100),
             202670: (250, 150),
             202671: (250, 200),
             202672: (250, 250),
             202673: (250, 300),
             202674: (250, 350),
             202675: (250, 400),
             202676: (250, 450),
             202677: (250, 500),
             202678: (300, 100),
             202679: (300, 150),
             202680: (300, 200),
             202681: (300, 250),
             202682: (300, 300),
             202683: (300, 350),
             202684: (300, 400),
             202685: (300, 450),
             202686: (300, 500),
             202687: (350, 100),
             202688: (350, 150),
             202689: (350, 200),
             202690: (350, 250),
             202691: (350, 300),
             202692: (350, 350),
             202693: (350, 400),
             202694: (350, 450),
             202695: (350, 500),
             202696: (400, 100),
             202697: (400, 150),
             202698: (400, 200),
             202699: (400, 250),
             202700: (400, 300),
             202701: (400, 350),
             202702: (400, 400),
             202703: (400, 450),
             202704: (400, 500),
             202705: (450, 100),
             202706: (450, 150),
             202707: (450, 200),
             202708: (450, 250),
             202709: (450, 300),
             202710: (450, 350),
             202711: (450, 400),
             202712: (450, 450),
             202713: (450, 500),
             202714: (500, 100),
             202715: (500, 150),
             202716: (500, 200),
             202717: (500, 250),
             202718: (500, 300),
             202719: (500, 350),
             202720: (500, 400),
             202721: (500, 450),
             202722: (500, 500),
             }

try:
    (Mu, M12) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

slha_file = "susy_NUHM_%d_%d_15_5000_8000.slha" % (Mu, M12)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
InitialSparticles = ['neutralinos','charginos'] 
OneOrTwoParticleInclusive = 'TwoParticleInclusive'
cmds = buildHerwigppCommands(InitialSparticles, slha_file, OneOrTwoParticleInclusive)

# define metadata
evgenConfig.description = 'EWK SUSY NUHM grid, Radiatively driven arXiv:1310.4858 [hep-ph], 2-lepton filter; slha file: susy_NUHM_%d_%d_15_5000_8000.slha'% (Mu, M12)
evgenConfig.keywords = ['SUSY','MSSM','NUHM','EWK','Direct neutralino/chargino']
evgenConfig.contact  = ['judita.mamuzic@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Generator Filter
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
from GeneratorFilters.GeneratorFiltersConf import SusySubprocessFinder
from AthenaCommon.AlgSequence import AthSequencer

topAlg += AthSequencer('FilterSeq')

before = SusySubprocessFinder('BeforeFilter')
before.BeforeFilter = True
after = SusySubprocessFinder('AfterFilter')
after.BeforeFilter = False

topAlg.FilterSeq += before
topAlg.FilterSeq += MultiElecMuTauFilter()
topAlg.FilterSeq += after

MultiElecMuTauFilter = topAlg.FilterSeq.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons = 2
MultiElecMuTauFilter.MinPt = 5000.
MultiElecMuTauFilter.MaxEta = 2.7
MultiElecMuTauFilter.MinVisPtHadTau = 15000.  # pt-cut on the visible hadronic tau
MultiElecMuTauFilter.IncludeHadTaus = 1      # include hadronic taus

StreamEVGEN.RequireAlgs = [ 'MultiElecMuTauFilter' ]

# clean up
del cmds
