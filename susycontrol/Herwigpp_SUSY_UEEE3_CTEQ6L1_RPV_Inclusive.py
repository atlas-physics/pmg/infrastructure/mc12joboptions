## SUSY Herwig++ jobOptions example
## Author: Dominik Krauss (dkrauss@cern.ch)
## Contact: Mike Flowerdew (flowerde@cern.ch)

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.RPVInclusive')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# Set up Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# Connect dataset ID numbers with the corresponding mass points
lle_points = {202826: ( 800,  80), 202827: (1000,  100), 202828: (1200,  120), 202829: (1400,  140), 202830: (1600, 160),
              202831: (1000, 500), 202832: (1200,  600), 202833: (1400,  700), 202834: (1600,  800),
	      202835: (1000, 900), 202836: (1200, 1080), 202837: (1400, 1260), 202838: (1600, 1440)}
lqd_points = {202839: ( 600,  60), 202840: ( 800,   80), 202841: (1000,   10), 202842: (1200,  120), 202843: (1400, 140),
              202844: ( 800, 400), 202845: (1000,  500), 202846: (1200,  600), 202847: (1400,  700),
	      202848: ( 800, 720), 202849: (1000,  900), 202850: (1200, 1080), 202851: (1400, 1260),
              204419: (1000, 100)}
int_points = {202852: (1200, 600)}

# Get the particle masses and determine the model
try:
    mGluino, mBino = lle_points[runArgs.runNumber]
    slha_file = 'susy_RPV_Gluino_Inclusive_LLE_template.slha'
except KeyError:
    try:
        mGluino, mBino = lqd_points[runArgs.runNumber]
        slha_file = 'susy_RPV_Gluino_Inclusive_LQD_template.slha'
    except KeyError:
        try:
            mGluino, mBino = int_points[runArgs.runNumber]
            slha_file = 'susy_RPV_Gluino_Inclusive_template.slha'
        except KeyError:
            raise RuntimeError('DSID %s not found in grid point dictionaries. Aborting!' % runArgs.runNumber)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive', earlyCopy=True)

# Get spectrum file
fOut = open('spectrum.slha.tmp', 'w')
found_gluino = False
found_bino = False

for line in open(slha_file, 'r'):
  if not found_gluino and line.find('MGLUINO') != -1:
    line = line.replace('MGLUINO', str(mGluino))
    found_gluino = True
  elif not found_bino and line.find('MCHI10') != -1:
    line = line.replace('MCHI10', str(mBino))
    found_bino = True

  fOut.write(line)

fOut.close()

import os
os.rename('spectrum.slha.tmp', slha_file)

if not found_gluino or not found_bino:
  raise RuntimeError("Couldn't replace the particle masses in the slha template file %s. Aborting!" % (slha_template))

# Define metadata
evgenConfig.description = 'Model with gluino pair production and inclusive RPV decays'
evgenConfig.keywords = ['SUSY', 'RPV', 'Inclusive', 'Gluino']
evgenConfig.contact  = ['flowerde@cern.ch']

# Print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds
