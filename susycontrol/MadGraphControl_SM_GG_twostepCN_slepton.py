# Squark-squark production, two step decay through a chargino and slepton
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

points2b2q = []
toremove = [210,240,270,300,330,410,490,570,650,730,810,890,970,1050,1130,1197.5]
counter=0
counter2=0
sumList   = [250,310,370,430,490,550,610,770,930,1090,1250,1410,1570,1730,1890,2050,2210,2370,2530,2690,2850]
for sum in ([0,50,100,150,200]+sumList):
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*80 ]
    for diff in diffL:
        counter2 += 1
        msq = (sum+diff)/2.
        #remove regions already excluded or unreachable
        if msq>1200 or msq-diff<0 or msq-diff>800 or msq<250: continue
        if msq in toremove: continue
        if msq<=575 and msq-diff<200: continue
        if msq-diff>600 and counter2%2==0: continue
        point = [msq,msq-diff]
        points2b2q += [ point ]

points2b2 = []
toremove = [210,240,270,300,330,410,490,570,650,730,810,890,970,1050,1130,1197.5]
counter=0
counter2=0
sumList   = [250,310,370,430,490,550,610,770,930,1090,1250,1410,1570,1730,1890,2050,2210,2370,2530,2690,2850]
for sum in ([0,50,100,150,200]+sumList):
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*80 ]
    for diff in diffL:
        counter += 1
        msq = (sum+diff)/2.
        #remove regions already excluded or unreachable
        if msq>1500 or msq-diff<0 or msq-diff>800 or msq<250: continue
        if msq in toremove: continue
        if (((counter%2==0 or sum<=550) and runArgs.runNumber>=173677 and runArgs.runNumber<=173864) or runArgs.runNumber>173864) and msq<=850 and msq-diff<250: continue
        if msq<910 and msq-diff<100: continue
        if msq-diff>600 and counter%2==0: continue
        if msq-diff>600 and msq>1300: continue
        point = [msq,msq-diff]
        points2b2 += [ point ]

# Standard grid points
therun = runArgs.runNumber-173677-len(points2b2q)
if therun>=0 and therun<len(points2b2):
    evgenLog.info('Registered generation of grid twenty-one, intermediate slepton GG point '+str(therun))
    masses['1000021'] = points2b2[therun][0]
    masses['1000022'] = points2b2[therun][1]
    masses['1000023'] = 0.5*(points2b2[therun][0]+points2b2[therun][1])
    masses['1000024'] = 0.5*(points2b2[therun][0]+points2b2[therun][1])
    masses['1000011'] = 0.25*(3.*points2b2[therun][1]+points2b2[therun][0])
    masses['1000012'] = 0.25*(3.*points2b2[therun][1]+points2b2[therun][0])
    masses['1000013'] = 0.25*(3.*points2b2[therun][1]+points2b2[therun][0])
    masses['1000014'] = 0.25*(3.*points2b2[therun][1]+points2b2[therun][0])
    masses['1000015'] = 0.25*(3.*points2b2[therun][1]+points2b2[therun][0])
    masses['1000016'] = 0.25*(3.*points2b2[therun][1]+points2b2[therun][0])
    stringy = str(int(points2b2[therun][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(masses['1000016']))+'_'+str(int(points2b2[therun][1]))
    gentype='GG'
    decaytype='twostepCN_slepton'
    njets=1
    use_decays=False
    use_Tauola=False

# Grid extension
pointsGG2step = [[905,825],[945,625],[1025,865],[185,105],[265,185],[345,105]]
therun = runArgs.runNumber - 179846
if therun>=0 and therun<len(pointsGG2step):
    evgenLog.info('Registered generation of grid extension: GG two step with slepton '+str(therun))
    masses['1000021'] = pointsGG2step[therun][0]
    masses['1000022'] = pointsGG2step[therun][1]
    masses['1000023'] = 0.5*(pointsGG2step[therun][0]+pointsGG2step[therun][1])
    masses['1000024'] = 0.5*(pointsGG2step[therun][0]+pointsGG2step[therun][1])
    masses['1000011'] = 0.25*(3.*pointsGG2step[therun][1]+pointsGG2step[therun][0])
    masses['1000012'] = 0.25*(3.*pointsGG2step[therun][1]+pointsGG2step[therun][0])
    masses['1000013'] = 0.25*(3.*pointsGG2step[therun][1]+pointsGG2step[therun][0])
    masses['1000014'] = 0.25*(3.*pointsGG2step[therun][1]+pointsGG2step[therun][0])
    masses['1000015'] = 0.25*(3.*pointsGG2step[therun][1]+pointsGG2step[therun][0])
    masses['1000016'] = 0.25*(3.*pointsGG2step[therun][1]+pointsGG2step[therun][0])
    stringy = str(int(pointsGG2step[therun][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(masses['1000016']))+'_'+str(int(pointsGG2step[therun][1]))
    gentype='GG'
    decaytype='twostepCN_slepton'
    njets=1
    use_decays=False
    use_Tauola=False

evgenConfig.contact  = [ "genest@lpsc.in2p3.fr" ]
evgenConfig.keywords += ['gluino','two_step','slepton']
evgenConfig.description = 'gluino production two step with slepton in simplified model'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

