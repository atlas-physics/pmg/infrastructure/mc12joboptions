##################################
# Stop pair-production with stop > b l
include('MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

# interpret the run number
therun = runArgs.runNumber-202632
if runArgs.runNumber == 205069:
    therun = 10 # different run number scheme for the last point

if therun >= 0 and therun <= 10:
    masses['1000006'] = 100*therun+100 # 100 GeV scan over stop masses
    stringy = str(masses['1000006']) # Goes into the file name
    gentype = 'TT' # stop-stop
    decaytype = 'directBL' # magic word
    njets = 1 # Number of jets in MadGraph
    evgenLog.info('Registered generation of stop pair production, stop to bl; grid point ' +
                  str(therun) + ' decoded into mass ' + str(masses['1000006']) +
                  ', with ' + str(njets) + ' jets.')
    use_decays = False # To turn on decays in MadGraph
    evt_multiplier = 20.0

evgenConfig.contact  = [ "bjackson@cern.ch" ]
evgenConfig.keywords += ['stop']
evgenConfig.description = 'stop direct pair production with simplified model'
evgenConfig.minevents=50000

# Set number of events and event multiplier for each data set based on stop mass
if masses['1000006'] == 100:
    evt_multiplier = 40.0
    evgenConfig.minevents = 100000
if masses['1000006'] == 200:
    evt_multiplier = 40.0
    evgenConfig.minevents = 100000
if masses['1000006'] == 300:
    evt_multiplier = 40.0
    evgenConfig.minevents = 80000
if masses['1000006'] == 400:
    evt_multiplier = 40.0
    evgenConfig.minevents = 70000
if masses['1000006'] == 500:
    evt_multiplier = 20.0
    evgenConfig.minevents = 60000
if masses['1000006'] == 600:
    evt_multiplier = 20.0
    evgenConfig.minevents = 50000
if masses['1000006'] == 700:
    evt_multiplier = 20.0
    evgenConfig.minevents = 40000
if masses['1000006'] == 800:
    evt_multiplier = 20.0
    evgenConfig.minevents = 30000
if masses['1000006'] == 900:
    evt_multiplier = 20.0
    evgenConfig.minevents = 10000
if masses['1000006'] == 1000:
    evt_multiplier = 20.0
    evgenConfig.minevents = 10000
if masses['1000006'] == 1100:
    evt_multiplier = 20.0
    evgenConfig.minevents = 10000

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )
##################################
