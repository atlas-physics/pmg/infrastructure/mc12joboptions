# gluino -> stop UDD RPV

points = [(mg,500.) for mg in (900.,1100.,1300.,1500.,1700.,1900.)]

# DSIDs 204978-204983
index = runArgs.runNumber - 204978

if index>=0 and index<len(points):
  mg, mst = points[index]
  slhafile = "susy_stgl_%d_%d.slha"%(int(mst),int(mg))
else:
  raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

print 'Using slha:', slhafile

evgenConfig.description = 'RPV UDD Stop-Gluino Model - gl-gl production only, slha file: {0}'.format(slhafile)
evgenConfig.keywords = ['SUSY','RPV']
evgenConfig.contact  = ['thomas.gillam@cern.ch','christopher.young@cern.ch']

include("MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_Common.py") # changed to UEEE4
include("MC12JobOptions/Herwigpp_SUSYConfig.py")

## Add Herwig++ parameters for this process
cmds = buildHerwigppCommands(['gluino'], slhafile, 'TwoParticleInclusive')

print '*** Begin Herwig++ SUSY commands ***'
print cmds
print '*** End Herwig++ SUSY commands ***'

## Append our commands
topAlg.Herwigpp.Commands += cmds.splitlines()

## Clean up
del cmds
