include('MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py')

import bisect

dict_index_syst[-1] = 'none'

runNumber = runArgs.runNumber
therun = -1

class Point:
    def __init__(self,runnr,masses,syst,METfilter):
        self.runnr = runnr
        self.masses = masses
        self.syst = syst
        self.METfilter = METfilter

counter = 0
dict_runnr_point = {}

include('MC12JobOptions/SUSY_scharm_neut1_mc12points.py')

#pointdict = {
#    999001: ( (100, 1), '80-180' ),
#    ...
#    }

# original  183424 - 183474
# extension 186635 - 186671
start = 186635
end = 186671

for p in sorted(pointdict): # sort by key (DSID)
    if (int(p) < start) or (int(p) > end): continue
    
    point_mass = [float(pointdict[p][0][0]), float(pointdict[p][0][1])]
    METfilter = pointdict[p][1]

    runnr = start + counter
    dict_runnr_point[runnr] = Point(runnr,point_mass,-1,METfilter)
    counter+=1

therun = runNumber - start

nevts = 0

if therun>=0 and therun<=(end-start):
    log.info('Number of points for systematic studies: ' + str(len(dict_runnr_point)))
    point = dict_runnr_point[runNumber]
    m_scharm, m_LSP = point.masses

    masses['1000004'] = m_scharm #cL
    masses['2000004'] = 4500 #m_scharm #cR
    masses['1000022'] = m_LSP

    # MadGraph v5.1.5.11 (in 17.2.11 and onwards) requires m_scharmL/R = m_supL/R
    # Only scharm production is performed by MadGraph
    masses['1000002'] = masses['1000004']
    masses['2000002'] = masses['2000004']
    
    gentype='Scharm'
    decaytype='direct'
    njets=1
    syst_mod=dict_index_syst[point.syst]


    str_stringy_METfilter = point.METfilter
    if '-' in str_stringy_METfilter:
        lowerMET = str_stringy_METfilter.split('-')[0]
        upperMET = str_stringy_METfilter.split('-')[1]
        runArgs.METfilterUpperCut = float(upperMET)
        str_info_METfilter = lowerMET+" - "+upperMET+" GeV"
    else:
        lowerMET = str_stringy_METfilter
        upperMET = 9999.9
        str_info_METfilter = "> "+lowerMET+" GeV"
    
    runArgs.METfilterCut = float(lowerMET)
    
    use_METfilter=True

    
    deltaM = m_scharm - m_LSP


    if point.METfilter == '80': #i.e. inclusive
        evt_multiplier = 2
    else:
        if point.METfilter == '80-180':
            evt_multiplier = 5
        elif point.METfilter == '180-300':
            evt_multiplier = 20
        elif point.METfilter == '300':
            evt_multiplier = 80
            
    # modify for low mass / low delta-M samples
    if m_scharm == 250 and m_LSP == 150:
        if point.METfilter == '300':
            evt_multiplier = 200
            
    if m_scharm == 200 and m_LSP == 125:
        if point.METfilter == '180-300':
            evt_multiplier = 50
        elif point.METfilter == '300':
            evt_multiplier = 500
                
    elif m_scharm == 150:
        if point.METfilter == '180-300':
            evt_multiplier = 100
        elif point.METfilter == '300':
            evt_multiplier = 1000

    elif m_scharm == 100:
        if point.METfilter == '80-180':
            evt_multiplier = 10
        elif point.METfilter == '180-300':
            evt_multiplier = 500
        elif point.METfilter == '300':
            evt_multiplier = 5000


    if runArgs.maxEvents>0:
        evgenConfig.minevents = runArgs.maxEvents

        nevts=runArgs.maxEvents*evt_multiplier
        if nevts > 100000:
            #runArgs.maxEvents *= (100000/nevts) # scale down so will complete
            log.warning(str(runArgs.maxEvents)+' events requested with evt_multiplier '+str(evt_multiplier)+' - this requires MadGraph to generate '+str(nevts)+' events. nevts has been reset to 100000, job will probably fail')
            nevts = 100000


    else: # maxevents = -1, as in production system, defaults to 100000
        if ( (deltaM<400 and m_scharm<=550) or (m_scharm<=450) ): # have 100000 events
            nevts = 100000
            
        elif deltaM<=650: # have 50000 events
            runArgs.maxEvents = 50000
            nevts = 100000
            
        else: # have 25000 events
            runArgs.maxEvents = 25000
            nevts = 50000

        minevents_base = nevts/float(evt_multiplier)
        OK_minevents = [0, 1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 100000]
        i = bisect.bisect_left(OK_minevents, minevents_base+1)
        evgenConfig.minevents = OK_minevents[i-1]

    stringy = str(int(m_scharm))+'_'+str(int(m_LSP))+'_'+str_stringy_METfilter+'_'+syst_mod
    log.info('Registered generation of scharm pair production, scharm to charm+LSP, systematic uncertainties; grid point '+str(therun)+' decoded into mass point (' + str(m_scharm) + ' ' + str(m_LSP) + '), with ' + str(njets) + ' jets. Using MET filter ' +  str_info_METfilter + '. Systematic variation ' + syst_mod + '.')
    use_decays=False

evgenConfig.contact  = [ "william.kalderon@cern.ch" ]
evgenConfig.keywords += ['scharm2charmLSP','scharm']#,'signal_uncertainties']
evgenConfig.description = 'scharm direct pair production, scharm->charm+LSP in simplified model'#, including signal uncertainties'

# Adjust things to the new form of the post include
if runArgs.maxEvents>0:
    evt_multiplier = float(nevts) / float(runArgs.maxevents)
else:
    evt_multiplier = float(nevts) / 5000.

include('MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py')

