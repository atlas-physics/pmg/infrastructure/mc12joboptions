# Stop pair-production with stop > top LSP
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

points_TT_directTT = []
therun = -1
if runArgs.runNumber in range(204199,204201+1):
    therun = runArgs.runNumber-204199
    points_TT_directTT = [
        [180,1],[500,150],[600,1]
        ]
if therun>=0 and therun<len(points_TT_directTT):
    masses['1000006'] = points_TT_directTT[therun][0]
    masses['1000022'] = points_TT_directTT[therun][1]
    stringy = str(int(points_TT_directTT[therun][0]))+'_'+str(int(points_TT_directTT[therun][1]))
    gentype='TT'
    decaytype='directTT'
    njets=1
    evgenLog.info('Registered generation of stop pair production, stop to top+LSP; grid point '+str(therun)+' decoded into mass point ' + str(points_TT_directTT[therun]) + ', with ' + str(njets) + ' jets.')
    use_decays=False

evt_multiplier = 5.0

evgenConfig.contact  = [ "takashi.yamanaka@cern.ch" ]
evgenConfig.keywords += ['stop']
evgenConfig.description = 'stop direct pair production with simplified model'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )
