## SUSY Herwig++ jobOptions for squark-gluino grid with masses >= 1 TeV

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.MSUGRA')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_Squark_Gluino_Grid_mc12points.py' )
try:
    (msquark,mgluino) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_herwig_SM_SG_GG_direct_%s_%s_0.slha' % (msquark, mgluino)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino', 'squarks'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'squark-gluino grid generation with m_squark = %s, m_gluino = %s' % (msquark,mgluino)
evgenConfig.keywords = ['SUSY','gluino','squark']
evgenConfig.contact  = ['mireia.crispin.ortuzar@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
