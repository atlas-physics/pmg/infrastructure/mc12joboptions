# Squark-squark production, two step decay through a chargino and slepton
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

points2b = []
sumList   = [250,310,370,430,490,550,610,770,930,1090,1250,1410,1570,1730,1890,2050,2210,2370,2530,2690,2850]
toremove = [210,240,270,300,330,410,490,570,650,730,810,890,970,1050,1130,1197.5]
for sum in ([0,50,100,150,200]+sumList):
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*80 ]
    for diff in diffL:
        msq = (sum+diff)/2.
        if msq>1200 or msq-diff<0 or msq<200: continue
        if msq in toremove: continue
        point = [msq,msq-diff]
        points2b += [ point ]

# Standard grid points
therun = runArgs.runNumber-153044-len(points2b)
if therun>=0 and therun<len(points2b):
    evgenLog.info('Registered generation of intermediate slepton GG point '+str(therun))
    masses['1000021'] = points2b[therun][0]
    masses['1000022'] = points2b[therun][1]
    masses['1000024'] = 0.5*(points2b[therun][0]+points2b[therun][1])
    masses['1000011'] = 0.25*(3.*points2b[therun][1]+points2b[therun][0])
    masses['1000012'] = 0.25*(3.*points2b[therun][1]+points2b[therun][0])
    masses['1000013'] = 0.25*(3.*points2b[therun][1]+points2b[therun][0])
    masses['1000014'] = 0.25*(3.*points2b[therun][1]+points2b[therun][0])
    masses['1000015'] = 0.25*(3.*points2b[therun][1]+points2b[therun][0])
    masses['1000016'] = 0.25*(3.*points2b[therun][1]+points2b[therun][0])
    stringy = str(int(points2b[therun][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(masses['1000016']))+'_'+str(int(points2b[therun][1]))
    gentype='GG'
    decaytype='twostepCC_slepton'
    njets=1
    use_decays=False
    use_Tauola=False

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

