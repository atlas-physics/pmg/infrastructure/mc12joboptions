# Stop pair-production with a c-LSP decay
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

# Assigned run numbers the stop->charm+LSP grid, DS 157851-157908 
therun = runArgs.runNumber-157851
points_TT_directCC = [
    [100.0,70.0],  [100.0,80.0],  [100.0,85.0],  [100.0,90.0],
    [100.0,95.0],  [125.0,75.0],  [125.0,95.0],  [125.0,105.0],
    [125.0,115.0], [125.0,120.0], [150.0,75.0],  [150.0,90.0],
    [150.0,100.0], [150.0,130.0], [150.0,140.0], [150.0,145.0],
    [175.0,100.0], [175.0,125.0], [175.0,155.0], [175.0,160.0],
    [175.0,165.0], [175.0,170.0], [200.0,125.0], [200.0,150.0],
    [200.0,170.0], [200.0,180.0], [200.0,190.0], [200.0,195.0],
    [225.0,150.0], [225.0,165.0], [225.0,175.0], [225.0,205.0],
    [225.0,215.0], [225.0,220.0], [250.0,175.0], [250.0,200.0],
    [250.0,230.0], [250.0,235.0], [250.0,240.0], [250.0,245.0],
    [275.0,225.0], [275.0,245.0], [275.0,255.0], [275.0,265.0],
    [275.0,270.0], [300.0,240.0], [300.0,250.0], [300.0,280.0],
    [300.0,290.0], [300.0,295.0], [350.0,300.0], [350.0,330.0],
    [350.0,335.0], [350.0,340.0], [400.0,350.0], [400.0,370.0],
    [400.0,380.0], [400.0,390.0]]
if therun>=0 and therun<len(points_TT_directCC):
    masses['1000006'] = points_TT_directCC[therun][0]
    masses['1000022'] = points_TT_directCC[therun][1]
    stringy = str(int(points_TT_directCC[therun][0]))+'_'+str(int(points_TT_directCC[therun][1]))
    gentype='TT'
    decaytype='directCC'
    njets=1
    evgenLog.info('Registered generation of stop pair production, stop to charm+LSP; grid point '+str(therun)+' decoded into mass point ' + str(points_TT_directCC[therun]) + ', with ' + str(njets) + ' jets.')
    use_decays=False
    use_Tauola=False
    use_METfilter=True
    runArgs.METfilterCut = 80.0

    if runArgs.runNumber in range(157851,157851+58):
        therun = runArgs.runNumber - 157851
        deltaM = points_TT_directCC[therun][0]-points_TT_directCC[therun][1]
        if (deltaM<20.1):
            evt_multiplier = 20.0
        else:
            evt_multiplier = 15.0

 
class Point:
    def __init__(self,runnr,masses,syst,METfilter):
        self.runnr = runnr
        self.masses = masses
        self.syst = syst
        self.METfilter = METfilter

counter = 0
dict_runnr_point_sys = {}
for point_mass in [[100.0,70.0],
                   [100.0,85.0],
                   [100.0,95.0],
                   [200.0,125.0],
                   [200.0,170.0],
                   [200.0,195.0],
                   [300.0,240.0],
                   [300.0,280.0],
                   [300.0,295.0]]:
    for syst in range(0,8):
        runnr = 177602 + counter
        dict_runnr_point_sys[runnr] = Point(runnr,point_mass,syst,"METinclusive")
        counter+=1

for point_mass in [[100.0,70.0],
                   [100.0,85.0],
                   [100.0,95.0],
                   [200.0,125.0],
                   [200.0,170.0],
                   [200.0,195.0]]:
    for syst in range(0,8):
        for METfilter in ['MET180to300','MET300']:
            runnr = 177602 + counter
            dict_runnr_point_sys[runnr] = Point(runnr,point_mass,syst,METfilter)
            counter+=1

therun = runArgs.runNumber - 177602 #to 177769
if therun>=0 and therun<(len(dict_runnr_point_sys)):
    evgenLog.info('Number of points for systematic studies: ' + str(len(dict_runnr_point_sys)))
    point = dict_runnr_point_sys[runArgs.runNumber]
    m_stop, m_LSP = point.masses
    masses['1000006'] = m_stop
    masses['1000022'] = m_LSP
    gentype='TT'
    decaytype='directCC'
    njets=1
    str_stringy_METfilter = point.METfilter
    syst_mod=dict_index_syst[point.syst]
    if str_stringy_METfilter == 'MET180to300':    
        str_info_METfilter = "180 - 300 GeV"
        runArgs.METfilterCut = 180.0
        runArgs.METfilterUpperCut = 300.0
        use_METfilter=True
        nevts = 100000
    elif str_stringy_METfilter == "MET300":
        str_info_METfilter = "> 300 GeV"
        runArgs.METfilterCut = 300.0
        use_METfilter=True
        nevts = 100000
    elif str_stringy_METfilter == 'METinclusive':
        str_info_METfilter = " - no filter!"
        use_METfilter=False
        deltaM = m_stop - m_LSP
        if (deltaM<20.1):
            evt_multiplier = 3.0
        else:
            evt_multiplier = 2.0

        #fix for failing jobs for DSIDs 177609, 177633, 177641
        if runArgs.runNumber in [177609, 177633, 177641]:
            evt_multiplier = 4.0

        if runArgs.maxEvents>0:
            nevts=runArgs.maxEvents*evt_multiplier
        else:
            nevts=5000
            evgenConfig.minevents = nevts
            nevts = nevts*evt_multiplier

    stringy = str(int(m_stop))+'_'+str(int(m_LSP))+'_'+str_stringy_METfilter+'_'+syst_mod
    evgenLog.info('Registered generation of stop pair production, stop to charm+LSP, systematic uncertainties; grid point '+str(therun)+' decoded into mass point (' + str(m_stop) + ' ' + str(m_LSP) + '), with ' + str(njets) + ' jets. Using MET filter ' +  str_info_METfilter + '. Systematic variation ' + syst_mod + '.')
    use_decays=False
    use_Tauola=False
    

# Assigned run numbers the stop->charm+LSP grid, DS 176266-176321
therun = runArgs.runNumber-176266
points_TT_directCC = [
    [100.0,70.0],[100.0,80.0],[100.0,85.0],[100.0,90.0],[100.0,95.0],
    [125.0,75.0],[125.0,95.0],[125.0,105.0],[125.0,115.0],[125.0,120.0],
    [150.0,75.0],[150.0,90.0],[150.0,100.0],[150.0,130.0],[150.0,140.0],[150.0,145.0],
    [175.0,100.0],[175.0,125.0],[175.0,155.0],[175.0,160.0],[175.0,165.0],[175.0,170.0],
    [200.0,125.0],[200.0,150.0],[200.0,170.0],[200.0,180.0],[200.0,190.0],[200.0,195.0]]
if therun>=0 and therun<(len(points_TT_directCC)*2):
    syst_mod = None
    m_stop = points_TT_directCC[therun/2][0]
    m_LSP = points_TT_directCC[therun/2][1]
    masses['1000006'] = m_stop
    masses['1000022'] = m_LSP
    gentype='TT'
    decaytype='directCC'
    njets=1
    if (therun%2==0):
        str_info_METfilter = "180 - 300 GeV"
        str_stringy_METfilter = "MET180to300"
    else:
        str_info_METfilter = "> 300 GeV"
        str_stringy_METfilter = "MET300"
        
    stringy = str(int(points_TT_directCC[therun/2][0]))+'_'+str(int(points_TT_directCC[therun/2][1]))+'_'+str_stringy_METfilter
    evgenLog.info('Registered generation of stop pair production, stop to charm+LSP; grid point '+str(therun)+' decoded into mass point ' + str(points_TT_directCC[therun/2]) + ', with ' + str(njets) + ' jets. Using MET filter ' +  str_info_METfilter)
    use_decays=False
    use_Tauola=False
    use_METfilter=True

    nevts = 100000
    if therun%2==0:
        runArgs.METfilterCut = 180.0
        #runArgs.METfilterUpperCut = 300.0
        if m_stop == 100:
            eff = 0.0099
        elif (m_stop == 125) or (m_stop == 150):
            eff = 0.0220
        elif (m_stop == 175) or (m_stop == 200):
            eff = 0.0409
    else:
        runArgs.METfilterCut = 300.0
        #runArgs.METfilterUpperCut = 1000000.0
        if m_stop == 100:
            eff = 0.0011
        elif (m_stop == 125) or (m_stop == 150):
            eff = 0.0032
        elif (m_stop == 175) or (m_stop == 200):
            eff = 0.0046

if runArgs.runNumber in range(178983,178994+1):
    therun = runArgs.runNumber-178983
    points_TT_directCC = [[125,40],
                          [175,90],
                          [225,140],
                          [250,165]]
elif runArgs.runNumber in range(183958,183972+1):
    therun = runArgs.runNumber-183958
    points_TT_directCC = [[110,30],[110,100],[200,120],[250,170],[300,220]]
elif runArgs.runNumber in range(204692,204694+1):
    therun = runArgs.runNumber-204692
    points_TT_directCC = [[150,70]]
if therun>=0 and therun<(len(points_TT_directCC)*3):
    syst_mod = None
    m_stop = points_TT_directCC[therun/3][0]
    m_LSP = points_TT_directCC[therun/3][1]
    masses['1000006'] = m_stop
    masses['1000022'] = m_LSP
    gentype='TT'
    decaytype='directCC'
    njets=1
    if (therun%3==0):
        str_info_METfilter = "80 - 180 GeV"
        str_stringy_METfilter = 'MET80to180'
        runArgs.METfilterCut = 80.0
        runArgs.METfilterUpperCut = 180.0
    elif (therun%3==1):
        str_info_METfilter = "180 - 300 GeV"
        str_stringy_METfilter = 'MET180to300'
        runArgs.METfilterCut = 180.0
        runArgs.METfilterUpperCut = 300.0
    else:
        str_info_METfilter = "> 300 GeV"
        str_stringy_METfilter = "MET300"
        runArgs.METfilterCut = 300.0
        
    stringy = str(int(points_TT_directCC[therun/3][0]))+'_'+str(int(points_TT_directCC[therun/3][1]))+'_'+str_stringy_METfilter
    evgenLog.info('Registered generation of stop pair production, stop to charm+LSP; grid point '+str(therun)+' decoded into mass point ' + str(points_TT_directCC[therun/3]) + ', with ' + str(njets) + ' jets. Using MET filter ' +  str_info_METfilter)
    use_decays=False
    use_Tauola=False
    use_METfilter=True
    nevts = 100000

evgenConfig.contact  = [ "alexandru.dafinca@cern.ch" ]
evgenConfig.keywords += ['stop2charmLSP','stop','signal_uncertainties']
evgenConfig.description = 'stop direct pair production, stop->charm+LSP in simplified model, including signal uncertainties'

# Assigned run numbers the stop->charm+LSP grid, DS 183364 to 183423
therun = runArgs.runNumber-183364
points_TT_directCC = [
    [225.0,150.0],[225.0,165.0],[225.0,175.0],[225.0,205.0],[225.0,215.0],[225.0,220.0],
    [250.0,175.0],[250.0,200.0],[250.0,230.0],[250.0,235.0],[250.0,240.0],[250.0,245.0],
    [275.0,225.0],[275.0,245.0],[275.0,255.0],[275.0,265.0],[275.0,270.0],
    [300.0,240.0],[300.0,250.0],[300.0,280.0],[300.0,290.0],[300.0,295.0],
    [350.0,300.0],[350.0,330.0],[350.0,335.0],[350.0,340.0],
    [400.0,350.0],[400.0,370.0],[400.0,380.0],[400.0,390.0]]

if therun>=0 and therun<(len(points_TT_directCC)*2):
    m_stop = points_TT_directCC[therun/2][0]
    m_LSP = points_TT_directCC[therun/2][1]
    masses['1000006'] = m_stop
    masses['1000022'] = m_LSP
    gentype='TT'
    decaytype='directCC'
    njets=1
    if (therun%2==0):
        str_info_METfilter = "> 180 GeV"
        str_stringy_METfilter = "MET180"
    else:
        str_info_METfilter = "> 300 GeV"
        str_stringy_METfilter = "MET300"

    stringy = str(int(points_TT_directCC[therun/2][0]))+'_'+str(int(points_TT_directCC[therun/2][1]))+'_'+str_stringy_METfilter
    evgenLog.info('Registered generation of stop pair production, stop to charm+LSP; grid point '+str(therun)+' decoded into mass point ' + str(points_TT_directCC[therun/2]) + ', with ' + str(njets) + ' jets. Using MET filter ' +  str_info_METfilter)
    use_decays=False
    use_Tauola=False
    use_METfilter=True
    if therun%2==0:
        runArgs.METfilterCut = 180.0
    else:
        runArgs.METfilterCut = 300.0

    # Increase the number of MadGraph events for stop->charm+LSP with MET filter
    nevts = 100000
    evgenLog.info('For production system, the number of events X / production system job was calculated such that one gets at least X events if one requests 100000 from MadGraph. Therefore in all jobs the minevents is ignored and 100 000 events are requested to be produced with MG.')
    
    evgenConfig.contact  = [ "vincent.giangiobbe@cern.ch" ]
    evgenConfig.keywords += ['stop']
    evgenConfig.description = 'stop direct pair production, stop->charm+LSP in simplified model'


# Assigned run numbers the stop->charm+LSP grid, DS 204296 to 204304
therun = runArgs.runNumber-204296
points_TT_directCC = [ [225.0, 190.0], [250.0, 190.0], [250.0, 215.0],
                       [250.0, 248.0], [275.0, 200.0], [275.0, 235.0],
                       [275.0, 273.0], [300.0, 225.0], [300.0, 298.0] ]

if therun>=0 and therun<(len(points_TT_directCC)*2):
    m_stop = points_TT_directCC[therun][0]
    m_LSP = points_TT_directCC[therun][1]
        
    masses['1000006'] = m_stop
    masses['1000022'] = m_LSP
    gentype='TT'
    decaytype='directCC'
    njets=1
    str_info_METfilter = "> 180 GeV"
    str_stringy_METfilter = "MET180"

    stringy = str(int(points_TT_directCC[therun][0]))+'_'+str(int(points_TT_directCC[therun][1]))+'_'+str_stringy_METfilter
    evgenLog.info('Registered generation of stop pair production, stop to charm+LSP; grid point '+str(therun)+' decoded into mass point ' + str(points_TT_directCC[therun]) + ', with ' + str(njets) + ' jets. Using MET filter ' +  str_info_METfilter)
    use_decays=False
    use_Tauola=False
    use_METfilter=True
    runArgs.METfilterCut = 180.0

    # Increase the number of MadGraph events for stop->charm+LSP with MET filter
    nevts = 100000
    evgenLog.info('For production system, the number of events X / production system job was calculated such that one gets at least X events if one requests 100000 from MadGraph. Therefore in all jobs the minevents is ignored and 100 000 events are requested to be produced with MG.')
    
    evgenConfig.contact  = [ "vincent.giangiobbe@cern.ch" ]
    evgenConfig.keywords += ['stop']
    evgenConfig.description = 'stop direct pair production, stop->charm+LSP in simplified model, addition to the existing grid'


include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

