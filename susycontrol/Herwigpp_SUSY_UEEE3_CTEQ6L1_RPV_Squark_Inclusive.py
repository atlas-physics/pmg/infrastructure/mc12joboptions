## SUSY Herwig++ jobOptions example
## Author: Dominik Krauss (dkrauss@cern.ch)
## Contact: Mike Flowerdew (flowerde@cern.ch)

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.RPVInclusive')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# Set up Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# Add Herwig++ parameters for this process
slha_file = 'susy_RPV_Squark_Inclusive_LQD_template.slha'
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['squarks_L'], slha_file, 'TwoParticleInclusive', earlyCopy=True)

# Connect dataset ID numbers with the corresponding mass points
lqd_points = {204315: ( 600,  60), 204316: ( 800,  80), 204317: (1000,   10),
              204318: ( 600, 300), 204319: ( 800, 400), 204320: (1000,  500), 204321: (1200,  600),
	      204322: ( 800, 720), 204323: (1000, 900), 204324: (1200, 1080), 205085: (1400, 1260),
              204420: (1000, 100)}

# Get the particle masses
try:
  mSquark, mBino = lqd_points[runArgs.runNumber]
except KeyError:
  raise RuntimeError('DSID %s not found in grid point dictionaries. Aborting!' % runArgs.runNumber)

# Get spectrum file
fOut = open('spectrum.slha.tmp', 'w')

found_squark = 0
found_bino = False
for line in open(slha_file, 'r'):
  if found_squark < 6 and line.find('MSQUARK') != -1:
    line = line.replace('MSQUARK', str(mSquark))
    found_squark += 1
  elif not found_bino and line.find('MCHI10') != -1:
    line = line.replace('MCHI10', str(mBino))
    found_bino = True

  fOut.write(line)

fOut.close()

import os
os.rename('spectrum.slha.tmp', slha_file)

if found_squark != 6 or not found_bino:
  raise RuntimeError("Couldn't replace the particle masses in the slha template file. Aborting!")

# Define metadata
evgenConfig.description = 'Model with pair production of left-handed squarks and inclusive RPV decays'
evgenConfig.keywords = ['SUSY', 'RPV', 'Inclusive', 'Squarks', 'left-handed']
evgenConfig.contact  = ['flowerde@cern.ch']

# Print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds

