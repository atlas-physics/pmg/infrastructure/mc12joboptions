templatefile = 'susy_RPVMSSM_SM_gluinotochi_template.slha'

evgenConfig.description = 'RPV gluino generation - simplified model g->qqchi->5q '
evgenConfig.generators = ['Herwigpp']
evgenConfig.keywords = ['SUSY','RPV']
evgenConfig.contact  = ['lawrence.lee.jr@cern.ch']
evgenConfig.auxfiles += ['susy_RPVMSSM_SM_gluinotochi_template.slha', 'MSSM.model'] 

import os

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")
include("MC12JobOptions/Herwigpp_SUSYConfig.py")

## Add Herwig++ parameters for this process
sparticle_list = []
sparticle_list.append('gluino')
slha_file = templatefile
cmds = buildHerwigppCommands(sparticle_list, slha_file,'TwoParticleInclusive', earlyCopy=True)

print '*** Begin Herwig++ SUSY commands ***'
print cmds
print '*** End Herwig++ SUSY commands ***'

# define spectrum file name
include( 'MC12JobOptions/SUSY_RPV_UDD_GluinoToChi_mc12points.py' )
try:
    Mgo, Mchi = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

if not os.access(templatefile,os.R_OK):
    print 'ERROR: Could not get slha file'
else:
    oldcard = open(templatefile,'r')
    newcard = open('spectrum.slha','w')
    for line in oldcard:
        if '   1000021     6.07713704E+02' in line:
            newcard.write('   1000021     %s   # ~g  \n'%(Mgo))
        elif '   1000022     5.0E+03   ' in line:
            newcard.write('   1000022     %i \n'%(Mchi) )
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()
    os.rename('spectrum.slha',templatefile)

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()
del cmds




