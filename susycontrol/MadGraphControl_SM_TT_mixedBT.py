# Stop pair-production with stop > top LSP or stop > b chargino
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

# Will need to be generating more MadGraph events...
evt_multiplier=5.0

points_TT_mixedBT = []
therun=-1
if runArgs.runNumber in range(166472,166474+1):
    therun = runArgs.runNumber-164472
    points_TT_mixedBT = [[180.0,150.0,1.0],[250.0,150.0,75.0],[300.0,200.0,100.0]]
if therun>=0 and therun<len(points_TT_mixedBT):
    masses['1000006'] = points_TT_mixedBT[therun][0]
    masses['1000024'] = points_TT_mixedBT[therun][1]
    masses['1000022'] = points_TT_mixedBT[therun][2]
    stringy = str(points_TT_mixedBT[therun][0])+'_'+str(points_TT_mixedBT[therun][1])+'_'+str(points_TT_mixedBT[therun][0])
    gentype='TT'
    decaytype='mixedBT'
    njets=1
    evgenLog.info('Registered generation of stop pair prodcution, stop to top+LSP and b+chargino mixed; grid point '+str(therun)+' decoded into mass point '+str(points_TT_mixedBT[therun])+', with '+str(njets)+' jets.')
    use_decays=True
    if masses['1000024']-masses['1000022']<81.0:
        use_decays=False
        decaytype+='.offshell'

# With off-shell Ws
points_TT_mixedBT = []
therun = -1
if runArgs.runNumber in range(175358,175419+1):
    therun = runArgs.runNumber-175358
    points_TT_mixedBT = [
        [250,100,50],[300,100,50],[300,200,100],[350,100,50],
        [350,200,100],[350,300,150],[400,100,50],[400,200,100],
        [400,300,150],[450,100,50],[450,200,100],[450,300,150],
        [450,400,200],[500,100,50],[500,200,100],[500,300,150],
        [500,400,200],[550,100,50],[550,200,100],[550,300,150],
        [550,400,200],[550,500,250],[600,100,50],[600,200,100],
        [600,300,150],[600,400,200],[600,500,250],[650,100,50],
        [650,200,100],[650,300,150],[650,400,200],[650,500,250],
        [650,600,300],[700,100,50],[700,200,100],[700,300,150],
        [700,400,200],[700,500,250],[700,600,300],[750,100,50],
        [750,200,100],[750,300,150],[750,400,200],[750,500,250],
        [750,600,300],[750,700,350],[800,100,50],[800,200,100],
        [800,300,150],[800,400,200],[800,500,250],[800,600,300],
        [800,700,350],[850,100,50],[850,200,100],[850,300,150],
        [850,400,200],[850,500,250],[850,600,300],[850,700,350],
        [850,800,400],[850,840,420],
        ]
if runArgs.runNumber in range(157007,157018+1):
    therun = runArgs.runNumber - 157007
    points_TT_mixedBT = [
        [230,100,50],[250,130,65],[250,150,75],[275,100,50],
        [275,150,75],[275,190,95],[300,240,120],[350,320,160],
        [350,340,170],[400,390,195],[450,440,220],[500,490,245],
        ]
if runArgs.runNumber in range(204202,204283+1):
    therun = runArgs.runNumber - 204202
    points_TT_mixedBT = [
        [275,100,95],[300,105,100],[300,125,120],[350,155,150],
        [350,175,170],[400,105,100],[400,205,200],[400,225,220],
        [450,155,150],[450,255,250],[450,275,270],[500,105,100],
        [500,205,200],[500,255,250],[500,305,300],[500,325,320],
        [550,105,100],[550,155,150],[550,205,200],[550,255,250],
        [550,305,300],[550,355,350],[600,105,100],[600,155,150],
        [600,205,200],[600,255,250],[600,305,300],[600,355,350],
        [650,105,100],[650,155,150],[650,205,200],[650,255,250],
        [650,305,300],[650,355,350],[700,105,100],[700,155,150],
        [700,205,200],[700,255,250],[700,305,300],[700,355,350],
        [275,115,95],[300,120,100],[300,140,120],[350,170,150],
        [350,190,170],[400,120,100],[400,220,200],[400,240,220],
        [450,120,100],[450,170,150],[450,220,200],[450,270,250],
        [450,290,270],[500,120,100],[500,220,200],[500,270,250],
        [500,320,300],[500,340,320],[550,120,100],[550,170,150],
        [550,220,200],[550,270,250],[550,320,300],[550,370,350],
        [600,120,100],[600,170,150],[600,220,200],[600,270,250],
        [600,320,300],[600,370,350],[650,120,100],[650,170,150],
        [650,220,200],[650,270,250],[650,320,300],[650,370,350],
        [700,120,100],[700,170,150],[700,220,200],[700,270,250],
        [700,320,300],[700,370,350],
        ]
if runArgs.runNumber in range(205115,205115+1):
    therun = runArgs.runNumber - 205115
    points_TT_mixedBT = [
        [650,270,250],
        ]
    use_Tauola = False
    use_Photos = False
if therun>=0 and therun<len(points_TT_mixedBT):
    masses['1000006'] = points_TT_mixedBT[therun][0]
    masses['1000024'] = points_TT_mixedBT[therun][1]
    masses['1000022'] = points_TT_mixedBT[therun][2]
    stringy = str(int(points_TT_mixedBT[therun][0]))+'_'+str(int(points_TT_mixedBT[therun][1]))+'_'+str(int(points_TT_mixedBT[therun][2]))
    gentype = 'TT'
    decaytype = 'mixedBT'
    njets=1
    evgenLog.info('Registered generation of stop pair production, stop to top+neutralino1, b+chargino mixed; grid point '+str(points_TT_mixedBT[therun])+', with ' + str(njets)+' jets')
    use_decays = True
    if masses['1000024'] - masses['1000022'] < 81.0:
        decaytype += '.offshellW'
    else:
        decaytype += '.W'

evgenConfig.contact  = [ "Keisuke.Yoshihara@cern.ch" ]
evgenConfig.keywords += ['stop']
evgenConfig.description = 'stop direct pair production with simplified model'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

