# SUSY Herwig++ jobOptions for direct stop pair production grid
# Use SUSYHIT and bottom-up schema for SLHA files 

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.Stop')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

include ( 'MC12JobOptions/SUSY_Stop_Stau_mc12points.py' )
try:
    (stop, stau, mu) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

# define spectrum file name
slha_file = 'susy_Stop%s_Stau%s_mu%s.slha' % (stop, stau, mu)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['stop1'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'Stop pair production, with stop decaying into stau'
evgenConfig.keywords = ['SUSY','stop','stau']
evgenConfig.contact  = ['ilaria.besana@mi.infn.it','tommaso.lari@mi.infn.it']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

if (runArgs.runNumber >= 179893 and runArgs.runNumber <= 179971):
    # https://savannah.cern.ch/task/?44831
    include ( 'MC12JobOptions/TauFilter.py' )
    topAlg.TauFilter.Ntaus = 2
    topAlg.TauFilter.EtaMaxe = 2.8
    topAlg.TauFilter.EtaMaxmu = 2.8
    topAlg.TauFilter.EtaMaxhad = 2.8
    topAlg.TauFilter.Ptcute = 8000.
    topAlg.TauFilter.Ptcutmu = 8000.
    topAlg.TauFilter.Ptcuthad = 1000000000.

#==============================================================
#
# End of job options file
#
###############################################################
