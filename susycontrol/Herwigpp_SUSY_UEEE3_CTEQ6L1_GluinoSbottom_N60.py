## SUSY Herwig++ jobOptions for simplified model GluinoSbottom_N60

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.GluinoSbottom_N60')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_GluinoSbottom_N60_mc12points.py' )
try:
    (mgl,msb) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_GluinoSbottom_G%s_B%s_N60.slha' % (mgl, msb)

m_gluino = int(mgl)
m_sbottom = int(msb)
diff = m_gluino - m_sbottom

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
if (m_gluino == 700 or  diff == 20) : cmds = buildHerwigppCommands(['gluino','sbottom1'], slha_file)
else : cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GluinoSbottom grid generation with m_gluino = %s, m_sbottom = %s GeV' % (mgl,msb)
evgenConfig.keywords = ['SUSY','sbottom','gluino']
evgenConfig.contact  = ['antoine.marzin@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
