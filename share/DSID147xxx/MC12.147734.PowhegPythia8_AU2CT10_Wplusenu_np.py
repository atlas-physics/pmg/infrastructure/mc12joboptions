## POWHEG+Pythia8 Wplusenu_np

evgenConfig.description = "POWHEG+Pythia8 Wplusenu for neutron-proton beam, no lepton filter, AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "e", "neutron-proton beam"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplusenu_np"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
