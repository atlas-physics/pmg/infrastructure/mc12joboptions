include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Gamma + up to 3 jets with ME+PS. Slice in photon pT>15.0 GeV."
evgenConfig.contact  = [ "frank.siegert@cern.ch" ]
evgenConfig.keywords = [ "photon" ]
evgenConfig.inputconfcheck = "SinglePhotonPt15"

evgenConfig.process="""
(run){
  QCUT:=30.0
}(run)

(processes){
  Process 93 93 -> 22 93 93{2}
  Order_EW 1
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
  Integration_Error 0.1 {5,6};
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process
}(processes)

(selector){
  PT  22  15.0  E_CMS
  DeltaR  22  93  0.3  20.0
}(selector)
"""
