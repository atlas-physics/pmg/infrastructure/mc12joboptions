evgenConfig.description = "Dijet truth jet slice JZ6, with the AU2 NNPDF21NLO MPI tune"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia8_AU2_NNPDF21NLO_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 1100."]

include("MC12JobOptions/JetFilter_JZ6.py")
evgenConfig.minevents = 2000
