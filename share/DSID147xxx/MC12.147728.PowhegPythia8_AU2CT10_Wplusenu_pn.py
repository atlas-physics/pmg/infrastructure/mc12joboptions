## POWHEG+Pythia8 Wplusenu_pn

evgenConfig.description = "POWHEG+Pythia8 Wplusenu for proton-neutron beam, no lepton filter, AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "e", "proton-neutron beam"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplusenu_pn"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
