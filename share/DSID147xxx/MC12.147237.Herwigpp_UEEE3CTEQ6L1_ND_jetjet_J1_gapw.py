evgenConfig.description = "HERWIG++2.6 ND dijet production in EE3 tune with dijet and gap filter"
evgenConfig.keywords = ["QCD", "jets", "gaps"]
evgenConfig.contact = ["Marek Tasevsky <Marek.Tasevsky@cern.ch>"]
evgenConfig.minevents = 1000

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 17*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 35*GeV
"""

topAlg.Herwigpp.Commands += cmds.splitlines()

#
from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()

from GeneratorFilters.GeneratorFiltersConf import GapJetFilter

#Jet kinematic cuts
if not hasattr(topAlg, "GapJetFilter"):
    topAlg += GapJetFilter()
topAlg.GapJetFilter.JetContainer = "AntiKt4TruthJets"
topAlg.GapJetFilter.MinPt1 = 12.0
topAlg.GapJetFilter.MinPt2 = 12.0
topAlg.GapJetFilter.MaxPt1 = runArgs.ecmEnergy
topAlg.GapJetFilter.MaxPt2 = runArgs.ecmEnergy

#Particle kinematic cuts for gap distribution
topAlg.GapJetFilter.MinPtparticle = 0.0
topAlg.GapJetFilter.MaxEtaparticle = 4.9
topAlg.GapJetFilter.gapf = 6.0 # below this value the filtering is applied

###MC12 EE3CTEQ6L1 tune, parameters of double exponential fit for gap distr.
topAlg.GapJetFilter.c0 = 4.27924
topAlg.GapJetFilter.c1 = -1.46058
topAlg.GapJetFilter.c2 = -6.80617
topAlg.GapJetFilter.c3 = 0.00477506
topAlg.GapJetFilter.c4 = 1.46217
topAlg.GapJetFilter.c5 = -1.18136
topAlg.GapJetFilter.c6 = 0.0006199
topAlg.GapJetFilter.c7 = -2.54093

if "GapJetFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["GapJetFilter"]

