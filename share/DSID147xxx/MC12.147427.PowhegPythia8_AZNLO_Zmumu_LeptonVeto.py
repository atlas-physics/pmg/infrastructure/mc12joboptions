include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune="AZNLO_CTEQ6L1"
process="Z_mumu"

def powheg_override():
    # needed for AZNLO tune
    PowhegConfig.ptsqmin = 4.0

evgenConfig.description = "POWHEG+Pythia8 Z->mumu production with central lepton veto pt<10 GeV and AZNLO CT10 tune"
evgenConfig.keywords = ["EW", "Z", "leptonic", "mu"]

# compensate filter efficiency
evt_multiplier = 15

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/LeptonVeto.py")
topAlg.LeptonFilter.Ptcut = 10000.
topAlg.LeptonFilter.Etacut = 2.7
