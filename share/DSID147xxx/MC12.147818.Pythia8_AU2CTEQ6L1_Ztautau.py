## Pythia8 Z->tautau

evgenConfig.description = "Z->tautau production with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "tau"]

ver =  os.popen("cmt show versions External/Pythia8").read()
if 'Pythia8-01' in ver:
    include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
else:
    include("MC12JobOptions/Pythia82_AU2_CTEQ6L1_Common.py")

include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # create Z bosons
                            "PhaseSpace:mHatMin = 60.", # lower invariant mass
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 15"] # switch on Z->tautau decays
