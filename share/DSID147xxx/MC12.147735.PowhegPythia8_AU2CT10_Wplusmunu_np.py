## POWHEG+Pythia8 Wplusmunu_np

evgenConfig.description = "POWHEG+Pythia8 Wplusmunu for neutron-proton beam, no lepton filter, AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "mu", "neutron-proton beam"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplusmunu_np"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
