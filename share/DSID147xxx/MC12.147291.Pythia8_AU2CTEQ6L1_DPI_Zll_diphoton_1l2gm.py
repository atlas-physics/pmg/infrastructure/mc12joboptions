evgenConfig.description = "DPI production of Z/Gm*->ll and di-photon. Lepton filter of pT>10 GeV. Diphoton filter of pT>10 GeV."
evgenConfig.keywords = ["electroweak", "W", "Photon", "leptons", "l", "DPI"]
evgenConfig.contact = ["Lulu Liu <Lulu.Liu@cern.ch>"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # generate W bosons
                            "23:mMin = 10", # Z/Gm* mass down to 10 GeV
                            "SecondHard:generate = on", # second hard process
                            "SecondHard:TwoPhotons = on", # Prompt Photon
                            "PhaseSpace:sameForSecond = off", # turn off the default option 
                            "PhaseSpace:pTHatMinSecond = 5.", # pTMin on the second hard process
                            "23:onMode = off", # switch off all W decays
                            "23:onIfAny = 11", # switch on W->e,nu decays
                            "23:onIfAny = 13", # switch on W->u,nu decays
                            "23:onIfAny = 15"] # switch on W->t,nu decays

# 1-lepton filter
include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut    = 10.*GeV
topAlg.MultiLeptonFilter.Etacut   = 10.
topAlg.MultiLeptonFilter.NLeptons = 1

# 1-photon filter
include("MC12JobOptions/PhotonFilter.py")
topAlg.PhotonFilter.Ptcut     = 10.*GeV
topAlg.PhotonFilter.Etacut    = 10.
topAlg.PhotonFilter.NPhotons  = 2
