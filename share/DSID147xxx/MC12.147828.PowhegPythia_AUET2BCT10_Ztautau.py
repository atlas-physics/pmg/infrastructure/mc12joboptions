## POWHEG+PYTHIA Z->tautau

evgenConfig.description = "POWHEG+PYTHIA Z->tautau production with the AUET2B CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "tau"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Ztautau"

include("MC12JobOptions/PowhegPythia_AUET2B_CT10_Common.py")
include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")

## Specify Z decay mode
include("MC12JobOptions/Pythia_Decay_Ztautau.py")
