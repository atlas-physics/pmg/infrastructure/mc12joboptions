## POWHEG+Pythia8 Wplus->taunu

evgenConfig.description = "POWHEG+Pythia8 Wplus->taunu production without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplustaunu"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
