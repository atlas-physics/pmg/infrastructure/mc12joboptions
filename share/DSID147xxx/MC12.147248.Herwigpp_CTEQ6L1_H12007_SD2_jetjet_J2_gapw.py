evgenConfig.description = "HERWIG++2.6 SD dijet production in EE3 tune with dijet and gap filter"
evgenConfig.keywords = ["QCD", "jets", "gaps"]
evgenConfig.contact = ["Marek Tasevsky <Marek.Tasevsky@cern.ch>"]
evgenConfig.minevents = 5000

include ('MC12JobOptions/Herwigpp_Diffractive_CTEQ6L1_H12007.py')

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
cd /Herwig/Partons
set QCDExtractor:SecondPDF PomeronFlux
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 35*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 70*GeV
"""

topAlg.Herwigpp.Commands += cmds.splitlines()

from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()

## Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import GapJetFilter

#Jet kinematic cuts
if not hasattr(topAlg, "GapJetFilter"):
    topAlg += GapJetFilter()
topAlg.GapJetFilter.JetContainer = "AntiKt4TruthJets"
topAlg.GapJetFilter.MinPt1 = 12.0
topAlg.GapJetFilter.MinPt2 = 12.0
topAlg.GapJetFilter.MaxPt1 = runArgs.ecmEnergy
topAlg.GapJetFilter.MaxPt2 = runArgs.ecmEnergy

#Particle kinematic cuts for gap distribution
topAlg.GapJetFilter.MinPtparticle = 0.0
topAlg.GapJetFilter.MaxEtaparticle = 4.9
topAlg.GapJetFilter.gapf = 5.0 # below this value the filtering is applied

###MC12 AUT_C0 tune, parameters of double exponential fit for gap distribution
topAlg.GapJetFilter.c0 = 1.88273
topAlg.GapJetFilter.c1 = -0.939424
topAlg.GapJetFilter.c2 = -4.79717
topAlg.GapJetFilter.c3 = 0.0115068
topAlg.GapJetFilter.c4 = 2.51567
topAlg.GapJetFilter.c5 = -1.16511
topAlg.GapJetFilter.c6 = -0.00278959
topAlg.GapJetFilter.c7 = -2.00906

if "GapJetFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["GapJetFilter"]

