include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa 1.4.0 diboson llnn, TGC sample, up to 1 jet with ME+PS."
evgenConfig.keywords = ["diboson", "llnn", "ZZ","TGC"]
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "andrew.james.nelson@cern.ch" ]

evgenConfig.process="""
(run){
  ACTIVE[24]=0
  ACTIVE[25]=0
  ERROR=0.02
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 -> 90 90 91 91 93{1};
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;
}(processes)

(selector){
  Mass  11 -11  4.0  E_CMS
  Mass  13 -13  4.0  E_CMS
  Mass  15 -15  4.0  E_CMS
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP]
}(selector)
"""
