include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "tau+ tau- gamma gamma production with up to three jets in ME+PS"
evgenConfig.contact  = [ "benjamin.clifford.auerbach@cern.ch"]
evgenConfig.keywords = [ "EW", "Triboson", "gamma", "tau" ]
evgenConfig.inputconfcheck = "ZtautauGammaGamma"

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.20
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 -> 15 -15 22 22 93{1}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.02 {6,7}
  End process;
}(processes)
(selector){
  Mass 15 -15 15 E_CMS
  PT 22 10 E_CMS
  PT 90 8 E_CMS
  DeltaR 22 22 0.2 1000
  DeltaR 22 93 0.2 1000
  DeltaR 22 90 0.3 1000
  DeltaR 93 90 0.2 1000
}(selector)
"""
