include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "WWjj(j)->lnlnjj(j) like-sign Order_EW=4"
evgenConfig.keywords = ["VBS", "Vector-Boson Scattering", "Diboson"]
evgenConfig.contact  = ["philipp.anger@cern.ch"]
evgenConfig.minevents = 500

evgenConfig.process="""
(run){
  ME_SIGNAL_GENERATOR=Comix;
  PARTICLE_CONTAINER 901 leptons 11 13 15;
  PARTICLE_CONTAINER 902 antileptons -11 -13 -15;
  PARTICLE_CONTAINER 903 antineutrinos -12 -14 -16;
  PARTICLE_CONTAINER 904 neutrinos 12 14 16;
}(run)

(model){
  ACTIVE[6]  = 0;
  MASS[5]    = 4.2;
  WIDTH[6]   = 1.523;
  MASS[11]   = 0.000510997;
  MASS[13]   = 0.105658389;
  MASS[15]   = 1.77705;
  ACTIVE[25] = 1;
  MASS[25]   = 126.0;
  WIDTH[25]  = 0.00418;
  # GF scheme:
  GF                  = 1.16639E-5;
  M_W                 = 80.399;
  M_Z                 = 91.1876;
  VEV                 = 246.2184581;
  SIN2THETAW          = 0.2226265156;
  LAMBDA              = 0.5237570374;
  1/ALPHAQED(default) = 132.3466351;
}(model)

(beam){
  BEAM_1 = 2212; BEAM_ENERGY_1 = 4000;
  BEAM_2 = 2212; BEAM_ENERGY_2 = 4000;
}(beam)

(processes){
  Process 93 93 -> 93 93 901 903 901 903 93{1}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  Integration_Error 0.05;
  End process;

  Process 93 93 -> 93 93 902 904 902 904 93{1}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  Order_EW 4;
  CKKW sqr(20/E_CMS);
  Integration_Error 0.05;
  End process;
}(processes)

(selector){
  PT 90 5 E_CMS;
  NJetFinder 2 15. 0. 0.4 -1;
}(selector)
"""

evgenConfig.inputconfcheck = 'lnln_WW_ls'
