evgenConfig.description = "Dijet truth jet slice JZ3, with Pythia 6 AUET2B and R=0.4, min 1 muon 3.5 GeV pt cut"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

topAlg.Pythia.PythiaCommand += [
                                  "pysubs msel 0",
                                  "pysubs ckin 3 100.",
                                  "pysubs msub 11 1",
                                  "pysubs msub 12 1",
                                  "pysubs msub 13 1",
                                  "pysubs msub 68 1",
                                  "pysubs msub 28 1",
                                  "pysubs msub 53 1"]


#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
include("MC12JobOptions/JetFilter_JZ3R04.py")

include ( 'MC12JobOptions/MultiMuonFilter.py' )
topAlg.MultiMuonFilter.Ptcut = 3500.
topAlg.MultiMuonFilter.Etacut = 10.0
topAlg.MultiMuonFilter.NMuons = 1

evgenConfig.minevents = 1000
include("PutAlgsInSequence.py")
