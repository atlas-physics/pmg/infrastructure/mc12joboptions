evgenConfig.description = "POWHEG+Pythia8 ZZ_2e2mu mll>1.0GeV with lepton pt>3Gev & m4l>40GeV using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak"  ,"ZZ", "leptons" ]
evgenConfig.inputfilecheck = "Powheg.147564"
evgenConfig.minevents = 2000
evgenConfig.contact = ["Richard Batley <batley@hep.phy.cam.ac.uk>", "Tiesheng Dai <Tiesheng.Dai@cern.ch>"]

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/FourLeptonInvMassFilter.py")
FourLeptonInvMassFilter = topAlg.FourLeptonInvMassFilter
FourLeptonInvMassFilter.MinPt = 3000.
FourLeptonInvMassFilter.MaxEta = 2.8
FourLeptonInvMassFilter.MinMass = 40000.
FourLeptonInvMassFilter.MaxMass = 50000000.
