evgenConfig.description = "DPI production of W->lnu and W->lnu. Dilepton filter of pT>10 GeV."
evgenConfig.keywords = ["electroweak", "W", "leptons", "l", "DPI"]
evgenConfig.contact = ["Lulu Liu <Lulu.Liu@cern.ch>"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on", # generate W bosons
                            "SecondHard:generate = on", # second hard process
                            "SecondHard:SingleW = on", # another W
                            "24:onMode = off", # switch off all W decays
                            "24:onIfAny = 11", # switch on W->e,nu decays
                            "24:onIfAny = 13", # switch on W->u,nu decays
                            "24:onIfAny = 15"] # switch on W->t,nu decays

# di-lepton filter
include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 10.*GeV
topAlg.MultiLeptonFilter.Etacut = 10.
topAlg.MultiLeptonFilter.NLeptons = 2
