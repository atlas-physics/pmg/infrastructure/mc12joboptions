include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "nunu gamma gamma production with up to three jets in ME+PS"
evgenConfig.contact  = [ "benjamin.clifford.auerbach@cern.ch"]
evgenConfig.keywords = [ "EW", "Triboson", "gamma", "neutrino" ]
evgenConfig.inputconfcheck = "ZnunuGammaGamma"

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 -> 91 91 22 22 93{1}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.02 {6,7}
  End process;
}(processes)
(selector){
  PT 22 20 E_CMS
  PT 90 8 E_CMS
  DeltaR 22 22 0.2 1000
  DeltaR 22 93 0.2 1000
  DeltaR 22 90 0.3 1000
  DeltaR 93 90 0.2 1000
}(selector)
"""
