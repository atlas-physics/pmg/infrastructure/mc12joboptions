## Pythia8 W->mu nu

evgenConfig.description = "W->mu nu production with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "mu"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on", # create W bosons
                            "24:onMode = off", # switch off all W decays
                            "24:onIfAny = 13"] # switch on W->mu,nu decays
