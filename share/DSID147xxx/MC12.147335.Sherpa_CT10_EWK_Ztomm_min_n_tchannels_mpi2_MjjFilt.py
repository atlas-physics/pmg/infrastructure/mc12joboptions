include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "VBF-Z production, with Z/gamma* -> mm. Min_N_Tchannels option enabled. MPI variations (type-2)"
evgenConfig.keywords = [  "Sherpa", "VBF", "Min_N_Tchannels" , "Systematics", "MPI" ]
evgenConfig.contact = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "kiran.joshi@cern.ch" ]
evgenConfig.minevents = 200
evgenConfig.weighting = 0

evgenConfig.process="""
(run){
  EW_TCHAN_MODE=1
}(run)

(processes){
  Process 93 93 -> 13 -13 93 93 93{1}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05
  Min_N_TChannels 1
  End process;
}(processes)

(mi){
  SIGMA_ND_FACTOR 0.31
}(mi)

(selector){
  Mass 13 -13 40 E_CMS
  NJetFinder 2 15.0 0.0 0.4 1
}(selector)
"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.inputconfcheck = '147335.Sherpa_CT10_Ztomm2JetsEW1JetQCD15GeVM40_min_n_tchannels_mpi2'


try:    
  from JetRec.JetGetters import *
  antikt4=make_StandardJetGetter('AntiKt',0.4,'Truth',disable=False,outputCollectionName='AntiKt4TruthJets',useInteractingOnly=True,includeMuons=False)
  antikt4alg = antikt4.jetAlgorithmHandle()
  antikt4alg.OutputLevel = INFO
except Exception, e:    
  pass

from GeneratorFilters.GeneratorFiltersConf import VBFMjjIntervalFilter
topAlg += VBFMjjIntervalFilter()
topAlg.VBFMjjIntervalFilter.TruthJetContainerName="AntiKt4TruthJets"
topAlg.VBFMjjIntervalFilter.OutputLevel = INFO

StreamEVGEN.RequireAlgs +=  [ "VBFMjjIntervalFilter" ]
