## Pythia8 W->e nu

evgenConfig.description = "W->e nu production with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "e"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on", # create W bosons
                            "24:onMode = off", # switch off all W decays
                            "24:onIfAny = 11"] # switch on W->e,nu decays
