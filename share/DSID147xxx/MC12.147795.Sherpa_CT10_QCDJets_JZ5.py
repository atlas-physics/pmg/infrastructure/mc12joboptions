include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa QCD jets with ME+PS up to the fourth jet."
evgenConfig.keywords = [ "QCD" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch" ]
evgenConfig.minevents = 500

evgenConfig.process="""
(processes){
  Process 93 93 -> 93 93 93{2};
  Order_EW 0;
  Max_N_Quarks 4;
  CKKW sqr(20/E_CMS);
  Integration_Error 0.05 {4,5,6,7,8};
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6,7,8}
  End process;
}(processes)

(selector){
  NJetFinder  1  900.0  0.0  0.4 -1
}(selector)
"""

include("MC12JobOptions/JetFilter_JZ5.py")
