# prepared by Orel Gueta
include ( "MC12JobOptions/Sherpa_CT10_Common.py" )

"""
(processes){
  Process 93 93 -> 93 93 93{3}
  Order_EW 0
  CKKW sqr(15/E_CMS)
  Integration_Error 0.1 {5}
  End process
}(processes)

(selector){
  NJetFinder 1 270.0 0.0 0.6 -1 
}(selector)
"""

# NJetFinder <n> <ptmin> <etmin> <D parameter> [<exponent>] [<eta max>] [<mass max>]

topAlg.Sherpa_i.Parameters += [ "MI_HANDLER=None" ]

evgenConfig.minevents = 20
evgenConfig.weighting = 0
evgenConfig.description = "Sherpa JZ4 slice requiring 4 jets with pT > 10 GeV"
evgenConfig.keywords = ["Sherpa", "JZ4","multi-jet","4 jets"]
evgenConfig.contact = ["Orel Gueta"]
evgenConfig.inputconfcheck = "group.phys-gener.sherpa010401.1473"

include ( "FourJetFilter_JZ4.py" )
