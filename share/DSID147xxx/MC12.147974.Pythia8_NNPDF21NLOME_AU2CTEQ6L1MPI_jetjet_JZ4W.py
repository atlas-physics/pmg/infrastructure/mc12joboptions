evgenConfig.description = "Dijet truth jet slice JZ4W, with the AU2 CTEQ6L1 MPI tune and NNPDF21NLO matrix element PDF"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia8_NNPDF21NLOME_AU2CTEQ6L1MPI_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 250."]

include("MC12JobOptions/JetFilter_JZ4W.py")
evgenConfig.minevents = 1000
