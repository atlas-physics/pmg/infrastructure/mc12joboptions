## POWHEG+Pythia8 Zee_np

evgenConfig.description = "POWHEG+Pythia8 Zee for neutron-proton beam, m>60GeV, no lepton filter, AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "e", "neutron-proton beam"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Zee_np"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
