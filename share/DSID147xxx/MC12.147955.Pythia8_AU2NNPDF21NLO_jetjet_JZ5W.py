evgenConfig.description = "Dijet truth jet slice JZ5W, with the AU2 NNPDF21NLO MPI tune"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia8_AU2_NNPDF21NLO_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 600."]

include("MC12JobOptions/JetFilter_JZ5W.py")
evgenConfig.minevents = 2000
