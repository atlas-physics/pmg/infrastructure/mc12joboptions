include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "QCD: 2 to 2 with Massive c and b quarks (no electro-weak processes included)"
evgenConfig.keywords = [ ]
evgenConfig.contact  = [ "frank.siegert@cern.ch","christian.schillo@cern.ch","christopher.young@cern.ch" ]
evgenConfig.minevents = 100

evgenConfig.process="""
(run){
  ACTIVE[25]=0

  MASSIVE[4]=1
  MASSIVE[5]=1
}(run)

(processes){
  Process 93 93 -> 93 93
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 93 93 -> 5 -5
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 93 93 -> 4 -4
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 93 5 -> 93 5
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 93 -5 -> 93 -5
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 93 4 -> 93 4
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 93 -4 -> 93 -4
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 5 5 -> 5 5
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 5 -5 -> 93 93
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 5 -5 -> 5 -5
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 5 -5 -> 4 -4
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 5 4 -> 5 4
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 5 -4 -> 5 -4
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process -5 -5 -> -5 -5
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process -5 4 -> -5 4
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process -5 -4 -> -5 -4
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 4 4 -> 4 4
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 4 -4 -> 93 93
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 4 -4 -> 5 -5
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process 4 -4 -> 4 -4
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

  Process -4 -4 -> -4 -4
  Order_EW 0;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.02;
  Selector_File *|(coresel){|}(coresel) {2};
  End process;

}(processes)

(coresel){
  NJetFinder  2  30.0  0.0  0.4  -1  999.0  10.0
}(coresel)
"""

# Take tau BR's into account:
topAlg.Sherpa_i.CrossSectionScaleFactor=1.0

# Overwrite default MC11 widths with LO widths for top and W
topAlg.Sherpa_i.Parameters += [ "WIDTH[6]=1.47211", "WIDTH[24]=2.035169" ]

include("MC12JobOptions/FourJet50Filter.py")

