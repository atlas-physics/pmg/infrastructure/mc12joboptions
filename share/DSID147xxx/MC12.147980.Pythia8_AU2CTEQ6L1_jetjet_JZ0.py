evgenConfig.description = "Dijet truth jet slice JZ0, with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
## Run with a min bias process: no pThat cut possible
topAlg.Pythia8.Commands += \
    ["SoftQCD:minBias = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]

include("MC12JobOptions/JetFilter_JZ0.py")
evgenConfig.minevents = 5000
