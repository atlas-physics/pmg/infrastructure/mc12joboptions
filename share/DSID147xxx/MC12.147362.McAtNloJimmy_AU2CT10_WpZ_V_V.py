## MC@NLO+HERWIG+JIMMY config -- just include a standard HERWIG+JIMMY tune and enable MC@NLO mode
      
      
include('MC12JobOptions/Jimmy_AUET2_CT10_Common.py')
      
topAlg.Herwig.HerwigCommand += ['iproc mcatnlo']
      
include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')
	
evgenConfig.generators += [ 'McAtNlo', 'Herwig' ]
evgenConfig.description = 'MC@NLO VV->inclusive using CT10 PDF and fHerwig with AUET2_CT10 configuration'
evgenConfig.keywords = ['EW', 'diboson']
evgenConfig.contact = ['alexander.oh@cern.ch']
evgenConfig.inputfilecheck = 'group.phys-gener.McAtNloJimmy_AUET2CT10.*.WpZ_V_V_m0p3_1p0_0p3_7TeV'

