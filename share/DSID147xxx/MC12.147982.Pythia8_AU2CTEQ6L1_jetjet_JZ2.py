evgenConfig.description = "Dijet truth jet slice JZ2, with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 40."]

include("MC12JobOptions/JetFilter_JZ2.py")
evgenConfig.minevents = 2000
