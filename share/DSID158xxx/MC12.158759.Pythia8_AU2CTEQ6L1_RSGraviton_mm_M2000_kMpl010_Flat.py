#--------------------------------------------------------------
# Author 1: S. Ask (Cambrdige U.), 21 May 2012
# Author 2: D. Hayden (RHUL.), 3rd August 2012
# Based on Generators/Pythia8_i/share/jobOptions.pythia8.py
#--------------------------------------------------------------

evgenConfig.description = "G*->mm production with no lepton filter and AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["electroweak", "G*", "RS", "leptons"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ['ExtraDimensionsG*:gg2G* = on']
topAlg.Pythia8.Commands += ['ExtraDimensionsG*:ffbar2G* = on']
topAlg.Pythia8.Commands += ['ExtraDimensionsG*:SMinBulk = off']
topAlg.Pythia8.Commands += ['ExtraDimensionsG*:kappaMG = 0.54']

topAlg.Pythia8.Commands += ['5100039:m0 = 2000.']
topAlg.Pythia8.Commands += ['5100039:onMode = off']
topAlg.Pythia8.Commands += ['5100039:onIfAny = 13']
topAlg.Pythia8.Commands += ['5100039:mMin = 50.']

topAlg.Pythia8.Commands += ['PhaseSpace:mHatMin = 50.']

topAlg.Pythia8.UserHook = "GravFlat"
