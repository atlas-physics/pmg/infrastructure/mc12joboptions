evgenConfig.description = "W*->enu production  AU2 CTEQ6L1 tune mWstar=3000"
evgenConfig.keywords = ["electroweak", "W*", "leptons"]
#
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_CalcHep.py")
include("MC12JobOptions/Pythia8_Photos.py")

evgenConfig.inputfilecheck = 'WStar' 
