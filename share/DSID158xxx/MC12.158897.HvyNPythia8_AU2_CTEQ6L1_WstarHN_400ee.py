evgenConfig.description = 'HvyN+Pythia8 heavy neutrinos, mN=400, channel=ee with CTEQ6L1 PDF'
evgenConfig.keywords = ['heavynu']
evgenConfig.generators += ['HvyN']
evgenConfig.inputfilecheck = 'HN400ee'
include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_LHEF.py')