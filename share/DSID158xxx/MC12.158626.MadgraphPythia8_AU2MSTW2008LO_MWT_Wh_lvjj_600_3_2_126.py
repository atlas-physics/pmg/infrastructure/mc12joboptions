#  choose the pdf
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

include("MC12JobOptions/Pythia8_MadGraph.py")

#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10"]

evgenConfig.description = "MWT WH -> lvjj 600_3_2"
evgenConfig.keywords = ["MWT", "Dibosons", "Multileptons"]
evgenConfig.inputfilecheck = 'MWT'
