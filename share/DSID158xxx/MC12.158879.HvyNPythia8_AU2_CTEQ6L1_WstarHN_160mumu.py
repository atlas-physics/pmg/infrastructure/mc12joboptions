evgenConfig.description = 'HvyN+Pythia8 heavy neutrinos, mN=160, channel=mumu with CTEQ6L1 PDF'
evgenConfig.keywords = ['heavynu']
evgenConfig.generators += ['HvyN']
evgenConfig.inputfilecheck = 'HN160mumu'
include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_LHEF.py')