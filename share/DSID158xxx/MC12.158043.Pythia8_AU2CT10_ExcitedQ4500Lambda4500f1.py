evgenConfig.description = "Excited quark 4500 GeV, with the AU2 CT10 tune"
evgenConfig.keywords = ["excitedquark"]
evgenConfig.contact = ["Nele Boelaert"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += ["ExcitedFermion:dg2dStar = on",
                           "ExcitedFermion:ug2uStar = on",
                           "4000001:m0 = 4500", # d* mass
                           "4000002:m0 = 4500", # u* mass
                           "ExcitedFermion:Lambda = 4500",
                           "ExcitedFermion:coupF = 1.0", # SU(2) coupling
                           "ExcitedFermion:coupFprime = 1.0", # U(1) coupling
                           "ExcitedFermion:coupFcol = 1.0", # SU(3) coupling
                           "4000001:mayDecay = on", # d* -> d g, d gam, d Z0, d W-
                           "4000002:mayDecay = on"] # u* -> u g, u gam, u Z0, u W+

#include("MC12JobOptions/Pythia8_Photos.py")
