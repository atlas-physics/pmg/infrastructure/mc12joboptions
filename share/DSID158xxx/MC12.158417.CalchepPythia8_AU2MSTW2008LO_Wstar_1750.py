###############################################################
#
# Job options file CalcHEP+Pythia8
#
#-----------------------------------------------------------------------------
evgenConfig.description = "CalcHEP production of Wstar"
evgenConfig.keywords = ["Wstar"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_CalcHep.py")
include("MC12JobOptions/Pythia8_Photos.py")

evgenConfig.inputfilecheck = "Wstar"

###############################################################

