###############################################################
#
# Job options file for HvGen
#
#==============================================================

## Config for Py8 tune AU2 with MSTW2008LO
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

## Configure Pythia8 to read input events from an LHEF file
include("MC12JobOptions/Pythia8_LHEF.py")

## ... Photos
include("MC12JobOptions/Pythia8_Photos.py")

evgenConfig.description = "HvGen+Pythia8 Zprime: MZprime=1 TeV, Mvpi=50 GeV, ctau_vpi=500mm"
evgenConfig.keywords = ["Hidden Valley", "Zprime"]
evgenConfig.generators += ["Pythia8", "Lhef"]
evgenConfig.inputfilecheck = "HVZprime"

topAlg.Pythia8.Commands += ["ParticleDecays:limitTau0 = off"] # Allow long-lived particles to decay
topAlg.Pythia8.Commands += ["ParticleDecays:tau0Max = 1000"] # the longest lived particle species lifetime you want
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "SLHA:useDecayTable = on",
                            "SLHA:keepSM =off",
                            "SLHA:minMassSM = 0.",
                            "Check:epTolErr = 100."]
topAlg.Pythia8.Commands += ["35:name = vpi_c",
                            "35:mMin = 0.0000", # Mass in GeV
                            "35:mMax = 200.000", # Mass in GeV                          
                            "36:name = vpi_0",
                            "36:mMin = 0.0000000", # Mass in GeV
                            "36:mMax = 200.00000000", # Mass in GeV
                            ] 


#
#==============================================================
#
# End of job options file
#
###############################################################
