include ("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_LHEF.py")
evgenConfig.inputfilecheck = "EffDM"
evgenConfig.description = "Wimp pair monophoton, operator D5, MWIMP=700 GeV, M*=1TeV, pta>120 GeV"
evgenConfig.keywords = ["WIMP"]
topAlg.Pythia8.Commands +=["1000022:all = chid chid~ 2 0 0 700. 0"]
