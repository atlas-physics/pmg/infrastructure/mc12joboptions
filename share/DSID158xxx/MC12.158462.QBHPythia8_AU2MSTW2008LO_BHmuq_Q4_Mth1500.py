evgenConfig.description = "Quantum black holes (M_th = 1500 GeV, charge: 4/3) decaying to one muon and one jet."
evgenConfig.contact = ["areinsch@uoregon.edu", "mshamim@cern.ch", "gingrich@ualberta.ca"]
evgenConfig.keywords = ["exotics", "blackholes", "mu", "jets"]
evgenConfig.generators += ["Lhef"]
evgenConfig.inputfilecheck = "BH"

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py" )
