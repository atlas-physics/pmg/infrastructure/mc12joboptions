evgenConfig.description = "Vector-like pair production, BB singlet, with Protos+Pythia6, tune AUET2B MSTW2008LO"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["exotics","top","heavyquark","4thgen","VLQ"]
evgenConfig.contact  = ["Nuno.Castro@cern.ch"]
evgenConfig.inputfilecheck = "BBS_M600"

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

include("MC12JobOptions/LeptonFilter.py")
include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")
