#  choose the pdf
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

include("MC12JobOptions/Pythia8_LHEF.py")

#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10"]

evgenConfig.description = "LSTC Zpi -> lljj M(rho_t) = 500 GeV, LHE files produced with Pythia6.4.26"
evgenConfig.keywords = ["LSTC", "Dibosons", "Multileptons"]
evgenConfig.generators = ["Lhef", "Pythia8"]
evgenConfig.inputfilecheck = 'LSTC'
