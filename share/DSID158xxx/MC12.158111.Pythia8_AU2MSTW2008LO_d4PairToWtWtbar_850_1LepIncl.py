m_bprime=850.0

evgenConfig.description = "FourthGeneration b prime ("+str(m_bprime)+") to ttbar"
evgenConfig.keywords = ["Exotics", "FourGeneration" ,"bprime"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["FourthBottom:all = off"]
topAlg.Pythia8.Commands += ["FourthBottom:gg2bPrimebPrimebar = on"]
topAlg.Pythia8.Commands += ["FourthBottom:qqbar2bPrimebPrimebar = on"]
topAlg.Pythia8.Commands += ["7:m0 ="+str(m_bprime) ]
topAlg.Pythia8.Commands += ["7:mWidth = 2.24394"]
topAlg.Pythia8.Commands += ["7:doForceWidth = on"]

topAlg.Pythia8.Commands += ["FourthGeneration:VubPrime = 0.0"]
topAlg.Pythia8.Commands += ["FourthGeneration:VcbPrime = 0.0"]
topAlg.Pythia8.Commands += ["FourthGeneration:VtbPrime = 1"]
topAlg.Pythia8.Commands += ["FourthGeneration:VtPrimebPrime = 0.0"]

# MultiLeptonFilter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10.*GeV
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]
