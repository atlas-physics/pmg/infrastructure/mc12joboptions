MODEL = 'regge'
CASE = 'gluino'
MASS = 1300
PROC = 'gg'
GBALLPROB = 0.1

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " stable generic gluino gball0.1 1300GeV"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS)
