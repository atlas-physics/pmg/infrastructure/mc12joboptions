evgenConfig.description = "ALPGEN+Pythia Z(->mumu)+5jets process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "Z", "mu"]
evgenConfig.inputfilecheck = "ZmumuNp5"

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

