

evgenConfig.description = "Powheg+Pythia not all-hadronic ttbar production with TTbarWToLeptonFilter - HERAPDF15NLO_EIG  + CTEQ61L PDFs, P2011C tune"
evgenConfig.keywords = ["top", "ttbar", "leptonic"]
evgenConfig.contact  = ["gia.khoriauli@cern.ch"]
evgenConfig.inputfilecheck = "ttbar"

include("MC12JobOptions/PowhegPythia_Perugia2011C_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")


