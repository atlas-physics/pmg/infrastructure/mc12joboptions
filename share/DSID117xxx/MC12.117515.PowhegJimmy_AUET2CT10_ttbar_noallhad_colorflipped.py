evgenConfig.description = "POWHEG+fHerwig/Jimmy ttbar no allhadr. color flipped production and AUET2 CT10 tune"
evgenConfig.keywords = ["top", "ttbar", "leptonic"]
evgenConfig.contact  = ["benjamin.philip.nachman@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar_noallhad' 

include('MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py')

evgenConfig.generators += [ 'Powheg' ]

include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')
