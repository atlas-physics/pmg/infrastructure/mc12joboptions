include("MC12JobOptions/PowhegControl_preInclude.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Perugia 2011c tune, no spin correlation, at least one lepton filter'
evgenConfig.keywords    = [ 'top', 'ttbar', 'nonallhad' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'alexander.grohsjean@desy.de' ]

process="tt_tt"
postGenerator="Pythia6TauolaPhotos"
postGeneratorTune ="Perugia2011C"

# compensate filter efficiency
evt_multiplier = 3.

include("MC12JobOptions/PowhegControl_postInclude.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC12JobOptions/TTbarWToLeptonFilter.py')
topAlg.TTbarWToLeptonFilter.NumLeptons = -1
topAlg.TTbarWToLeptonFilter.Ptcut = 0.
