include("MC12JobOptions/PowhegControl_preInclude.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia6 ttbar production with Perugia 2011c tune and LeptonVeto mtt slice 1100-1300 GeV"
evgenConfig.keywords = ["top", "ttbar", "hadronic"]
evgenConfig.contact  = ["Christoph.Wasicki@cern.ch"]
evgenConfig.minevents      = 2000   

postGenerator="Pythia6TauolaPhotos"
process="tt_all"

# compensate filter efficiency
evt_multiplier = 1e3

include("MC12JobOptions/PowhegControl_postInclude.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC12JobOptions/TTbarMassFilter.py')
topAlg.TTbarMassFilter.TopPairMassLowThreshold  = 1100000.
topAlg.TTbarMassFilter.TopPairMassHighThreshold = 1300000.

include('MC12JobOptions/TTbarWToLeptonVeto.py')
