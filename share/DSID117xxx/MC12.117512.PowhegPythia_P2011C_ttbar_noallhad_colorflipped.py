evgenConfig.description = "POWHEG+Pythia6 ttbar no allhadr. color flipped production and Pythia with Perugia2011C tune"
evgenConfig.keywords = ["top", "ttbar", "leptonic"]
evgenConfig.contact  = ["benjamin.philip.nachman@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar_noallhad'

include("MC12JobOptions/PowhegPythia_Perugia2011C_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
