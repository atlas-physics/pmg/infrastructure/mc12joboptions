#--------------------------------------------------------------
# Powheg_ttbar setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.nEvents = 15000
PowhegConfig.hdamp   = 172.5
PowhegConfig.PDF     = 10800
## Other PoWHEG command go here
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with new, main31-style shower
#--------------------------------------------------------------

include('MC12JobOptions/Pythia8_A14_CTEQ6L1_Common.py')
include("MC12JobOptions/Pythia8_LHEF.py")
topAlg.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 2',
                             'TimeShower:pTmaxMatch = 2']
                           
topAlg.Pythia8.UserHook  = 'Main31'

topAlg.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
topAlg.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

# Lepton filter (-1: non-allhadronic events, 2 dileptonic, ...)  
include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons=-1

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with CT10, hdamp=172.5, A14 tune and main31'
evgenConfig.keywords    = [ 'top', 'semileptonic' ]
evgenConfig.contact     = [ 'jmonk@cern.ch', 'alexander.grohsjean@desy.de' ]
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.minevents   = 5000


