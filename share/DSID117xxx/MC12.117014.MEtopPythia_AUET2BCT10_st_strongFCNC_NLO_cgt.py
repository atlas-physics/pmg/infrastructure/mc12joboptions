evgenConfig.description = "MEtop+Pythia strong FCNC inclusive direct top at NLO & top+light quark production with the AUET2B_CT10 tune"
evgenConfig.keywords = ["singletop","FCNC","leptonic"]
evgenConfig.contact  = ["conrad.friedrich@cern.ch"]
evgenConfig.inputfilecheck = "MEtop10.117014.st_strongFCNC_cgt"

include ( "MC12JobOptions/MEtopPythia_AUET2B_CT10_Common.py" )

include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

