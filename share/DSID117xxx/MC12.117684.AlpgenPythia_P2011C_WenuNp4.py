evgenConfig.description = "ALPGEN+Pythia W(->enu)+4jets process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "W", "e"]
evgenConfig.inputfilecheck = "WenuNp4"

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

