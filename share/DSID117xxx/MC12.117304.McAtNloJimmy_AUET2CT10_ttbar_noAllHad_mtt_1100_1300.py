evgenConfig.description = "McAtNlo+fHerwig/Jimmy ttbar production with TTbarWToLeptonFilter and TTbarMassFilter for 1100 < mtt < 1300 - CT10 PDF, AUET2 tune"
evgenConfig.generators = ["McAtNlo", "Herwig"]
evgenConfig.keywords = ["top", "ttbar", "leptonic", "mtt"]
evgenConfig.contact  = ["neil.james.cooper-smith@cern.ch"]
evgenConfig.inputfilecheck = "ttbar"
evgenConfig.minevents=1000
	
include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand+=[ "iproc mcatnlo" ]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")

include("MC12JobOptions/TTbarMassFilter.py")
topAlg.TTbarMassFilter.TopPairMassLowThreshold  = 1100000.
topAlg.TTbarMassFilter.TopPairMassHighThreshold = 1300000.

include("MC12JobOptions/TTbarWToLeptonFilter.py")
