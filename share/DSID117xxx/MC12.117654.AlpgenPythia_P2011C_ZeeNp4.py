evgenConfig.description = "ALPGEN+Pythia Z(->ee)+4jets process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "Z", "e"]
evgenConfig.inputfilecheck = "ZeeNp4"

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

