include("MC12JobOptions/PowhegControl_preInclude.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Perugia 2011C tune, at least one lepton filter with top width 0.1 GeV'
evgenConfig.keywords    = [ 'top', 'ttbar', 'nonallhad', 'topwidth' ]
evgenConfig.contact     = [ 'cescobar@cern.ch', 'alexander.grohsjean@desy.de' ]

process="tt_all"
postGenerator="Pythia6TauolaPhotos"
postGeneratorTune ="Perugia2011C"

def powheg_override():
    PowhegConfig.tdec_twidth = 0.1

# compensate filter efficiency
evt_multiplier = 3.

include("MC12JobOptions/PowhegControl_postInclude.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC12JobOptions/TTbarWToLeptonFilter.py')
topAlg.TTbarWToLeptonFilter.NumLeptons = -1
topAlg.TTbarWToLeptonFilter.Ptcut = 0.
