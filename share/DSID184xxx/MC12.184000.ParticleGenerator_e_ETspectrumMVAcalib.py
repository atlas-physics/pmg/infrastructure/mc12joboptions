evgenConfig.description = "Single electrons with user-defined ET in [0-3] TeV for MVA calibration"
evgenConfig.keywords = ["singleparticle", "e"]

include("MC12JobOptions/ParticleGenerator_ETspectrumMVAcalib_Common.py")  # to build the histo-based ET spectrum

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE 

topAlg.ParticleGenerator.orders = [
 "PDGcode: sequence -11 11",
 "et: histogram evgen",
 "eta: flat -2.5 2.5",
 "phi: flat -3.14159 3.14159"
 ]

StreamEVGEN.RequireAlgs +=  [ "ParticleGenerator" ]
