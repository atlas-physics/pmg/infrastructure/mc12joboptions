evgenConfig.description = "AcerMCPythia6 singletop-tchan-l production with P2011C CTEQ6L1 tune"
evgenConfig.generators = ["AcerMC", "Pythia"]
evgenConfig.keywords = ["top", "singletop", "tchan", "leptonic"]
evgenConfig.contact  = ["liza.mijovic@cern.ch"]
evgenConfig.inputfilecheck = "singletop_tchan_lept"
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")
topAlg.Pythia.PythiaCommand += ["pyinit user acermc"]
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

include("MC12JobOptions/MultiMuonFilter.py")
topAlg.MultiMuonFilter.Ptcut = 15000.
topAlg.MultiMuonFilter.Etacut = 2.7
topAlg.MultiMuonFilter.NMuons = 1
