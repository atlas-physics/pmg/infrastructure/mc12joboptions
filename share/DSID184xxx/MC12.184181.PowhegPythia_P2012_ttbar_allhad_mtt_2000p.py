include("MC12JobOptions/PowhegControl_preInclude.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia6 ttbar production with Perugia 2012 tune and LeptonVeto mtt slice 2000+ GeV"
evgenConfig.keywords = ["top", "ttbar", "hadronic"]
evgenConfig.contact  = ["lily.asquith@cern.ch"]
evgenConfig.minevents      = 50   

postGenerator="Pythia6TauolaPhotos"
postGeneratorTune ="Perugia2012"
process="tt_all"

# compensate filter efficiency
evt_multiplier = 50e3

include("MC12JobOptions/PowhegControl_postInclude.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC12JobOptions/TTbarMassFilter.py')
topAlg.TTbarMassFilter.TopPairMassLowThreshold  = 2000000.
topAlg.TTbarMassFilter.TopPairMassHighThreshold = 14000000.

include('MC12JobOptions/TTbarWToLeptonVeto.py')
