evgenConfig.description = "Double diffractive events, with the A2 MSTW2008LO tune"
evgenConfig.keywords = ["QCD", "minbias"]

include("MC12JobOptions/Pythia8_A2_MSTW2008LO_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["SoftQCD:doubleDiffractive = on"]
