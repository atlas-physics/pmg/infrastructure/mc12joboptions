#--------------------------------------------------------------
# http://bbgen.web.cern.ch/bbgen/bruce/fluka_beam_gas_arc_4TeV/flukaIR15.html
# W. H. Bell <W.Bell@cern.ch>
#--------------------------------------------------------------

include( "MC12JobOptions/MC12_BeamHaloGenerator_Common.py")

# Default settings in common JO
topAlg.BeamHaloGeneratorAlg.inputType="FLUKA-RB"  
topAlg.BeamHaloGeneratorAlg.interfacePlane = -22600.0

# The probability of the event being flipped about the x-y plane.
topAlg.BeamHaloGeneratorAlg.flipProbability = 1.0
topAlg.BeamHaloGeneratorAlg.enableFlip = True

# Accept all event (do not apply any filter)
topAlg.BeamHaloGeneratorAlg.generatorSettings = []

evgenConfig.inputfilecheck = 'group.phys-gener.beamhalo.013063.RBruceBeamGasB1_8TeV_20MeV_cutoff'
evgenConfig.description = "Non collision beam backgraund generator" 
evgenConfig.keywords = ["beamhalo","beamgas", "pileup" ] 
evgenConfig.minevents = 5000
