evgenConfig.description = "MadGraph5+Pythia8 for Bulk RS Graviton (m=1200GeV) to->tt"
evgenConfig.contact = ["james.ferrando@glasgow.ac.uk", "jiahang.zhong@cern.ch","shoaleh@lps.umontreal.ca"] 
evgenConfig.keywords = ["graviton","ttbar","BulkRS"]
evgenConfig.inputfilecheck = "BulkRSGraviton1200Ttbar"
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
