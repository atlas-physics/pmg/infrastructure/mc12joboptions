## POWHEG+Pythia8 DYee_4500M5000

evgenConfig.description = "POWHEG+Pythia8 Z/gamma*e with mass cut DYee_4500M5000GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "e"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYee_4500M5000"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
