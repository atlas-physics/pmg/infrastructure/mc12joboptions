evgenConfig.inputfilecheck = 'cZBM'
evgenConfig.description = "Colored Zee-Babu Model DQul_3000_1000"
evgenConfig.keywords = ["Exotics","cZBM"]
evgenConfig.contact = ["kenji.hamano@cern.ch"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include ("MC12JobOptions/Pythia8_MadGraph.py")
evgenConfig.generators += ["MadGraph", "Pythia8"]
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10"]
    
