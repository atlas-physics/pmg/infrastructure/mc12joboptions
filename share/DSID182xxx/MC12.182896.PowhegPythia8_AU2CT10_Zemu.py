evgenConfig.description = "POWHEG+Pythia8 Z->emu production without lepton filter and AU2 CT10 tune"
evgenConfig.contact = ["Jan Kretzschmar"]
evgenConfig.keywords = ["LFV", "electroweak", "Z", "leptons", "e+mu"]
evgenConfig.inputfilecheck = "Zemu"


include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

