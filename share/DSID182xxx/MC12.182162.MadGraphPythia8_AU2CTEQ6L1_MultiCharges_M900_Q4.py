
###############################################################
#
# Job options file for Evgen MadGraph MultiCharges Generation
#
#==============================================================
include ( "MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py" )

include("MC12JobOptions/Pythia8_Tauola.py" ) #TAUOLA
include("MC12JobOptions/Pythia8_MadGraph.py") #MadGraph
include("MC12JobOptions/Pythia8_Photos.py" ) #Photos


#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.description = "Drell-Yan MultiCharges generation for Mass = 900 GeV, Charge = 4.0e with MadGraph+Pythia8 and the AU2 CTEQ6L1 tune in MC12"
evgenConfig.keywords = ["exotics", "MultiCharges", "Drell-Yan"]
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.contact = ["Yury.Smirnov@cern.ch"]

evgenConfig.inputfilecheck = 'MultiCharges'

evgenConfig.specialConfig = 'MASS=900;CHARGE=4.0;preInclude=SimulationJobOptions/preInclude.Qball.py;MDT_QballConfig=True'

#--------------------------------------------------------------
# Edit PDGTABLE.MeV with Q-ball mass
#--------------------------------------------------------------
ALINE1="M 10000400                         900.E+03       +0.0E+00 -0.0E+00 QBall        +"
ALINE2="W 10000400                         0.E+00         +0.0E+00 -0.0E+00 QBall        +"


import os
import sys

pdgmod = os.path.isfile('PDGTABLE.MeV')
if pdgmod is True:
    os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
f=open('PDGTABLE.MeV','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2
