evgenConfig.description = "Wimp pair monophoton with light mediator, dmV, M_WIMP=400 GeV, M_M=300 GeV, width(M)=M/3, pta>120 GeV"
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.keywords = ["WIMP light mediator"]
evgenConfig.contact = ["genest@lpsc.in2p3.fr"]

include ("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_LHEF.py")

evgenConfig.inputfilecheck = "dmV_DM400_MM300_W3"

#particle data = name antiname spin=2s+1 3xcharge colour mass width (left out, so set to 0: mMin mMax tau0)
topAlg.Pythia8.Commands +=["1000022:all = chi chi~ 2 0 0 400. 0"]
