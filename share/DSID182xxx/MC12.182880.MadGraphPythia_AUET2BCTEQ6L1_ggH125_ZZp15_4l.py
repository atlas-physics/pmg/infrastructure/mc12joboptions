evgenConfig.generators += ["MadGraph"]
evgenConfig.description = "MadGraph gg H->ZZ'->4l (mH=125GeV, mZ'=15GeV l=e,mu)"
evgenConfig.keywords = ["nonSMhiggs","Z","leptonic","e","mu"]
evgenConfig.contact = ["theodota.lagouri@cern.ch"]

evgenConfig.inputfilecheck= 'madgraph.182880.ggh125_ZZp15_4l'


# --> use the following pythia tune if CTEQ6l1 pdf has been used
include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )

## ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )
## ... Photos
include ( "MC12JobOptions/Pythia_Photos.py" )

topAlg.Pythia.PythiaCommand += ["pyinit user madgraph",
                                "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                                "pydat1 parj 90 20000", # Turn off FSR.
                                "pydat3 mdcy 15 1 0"  ] # Turn off tau decays.
