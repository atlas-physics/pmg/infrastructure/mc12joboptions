# W' -> WZ -> lvqq

# Wprime Mass (in GeV)
M_Wprime = 4000.0  

evgenConfig.description = "Wprime->WZ->lvqq "+str(M_Wprime)+" GeV with SSM+EGM coupling"
evgenConfig.contact = ["koji.terashi@cern.ch"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.keywords = ["Exotics", "EGM", "SSM", "Wprime"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on",
                            "Wprime:coup2WZ = 1.",    # Wprime Coupling to WZ
                            "34:m0 = "+str(M_Wprime), # Wprime mass
                            "34:onMode = off",
                            "34:onIfAll = 23 24",
                            "23:onMode = off",
                            "23:onIfAny = 1 2 3 4 5",
                            "24:onMode = off",
                            "24:onIfAny = 11 13 15",
                            "Init:showAllParticleData = on",
                            "Next:numberShowEvent = 5"]

topAlg.Pythia8.Commands += ["34:mMin = 1500.",       # minimum mass cut
                            "PhaseSpace:mHatMin = 1500."]
