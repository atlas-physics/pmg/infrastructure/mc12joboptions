evgenConfig.generators = ["Pythia"] 
evgenConfig.contact = ["Katharine Leney (at SPAMNOTcern.ch)"] 
evgenConfig.description = "LQ3 ->top + nu_tau, M_LQ = 700 GeV, Pythia6"
evgenConfig.keywords    = [ "exotics", "leptoquark" ]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

topAlg.Pythia.PythiaCommand +=  ["pysubs msel 0"          ,
                             #   "pysubs msub 162 1"      ,     # qg    -> lLQ
                                 "pysubs msub 163 1"      ,     # gg    -> LQLQ
                                 "pysubs msub 164 1"      ,     # qqbar -> LQLQ
                                 "pydat2 pmas 42 1 700."  ,
                                 "pysubs ckin 41 600.0"   ,
                                 "pysubs ckin 42 800.0"   ,
                                 "pysubs ckin 43 600.0"   ,
                                 "pysubs ckin 44 800.0"   ,
                                 "pydat3 brat 539 1."     ,
                                 "pydat3 kfdp 539 1 6"    ,
                                 "pydat3 kfdp 539 2 16"  ,
                                 "pydat1 paru 151 0.01"
                                 ]

topAlg.Pythia.PythiaCommand += ["pyinit dumpr 1 20"]



