evgenConfig.description = "WIMP Light Mediator model generation with MadGraph/Pythia6 in MC12"
evgenConfig.keywords = ["WimpPair"]
evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.contact = ["steven.schramm@cern.ch"]

include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1",     # Changes listing of particles in log file
                          	 "pyinit pylistf 1",      # Changes listing of particles in log file
                                 "pyinit dumpr 1 2",
				]            


evgenConfig.inputfilecheck = 'dmV_DM400_MM1000_W3_QCUT300'

# Additional bit for ME/PS matching
phojf=open('./pythia_card.dat', 'w')
phojinp = """
      !...Matching parameters...
      IEXCFILE=0 
      showerkt=T
      qcut=300
      imss(21)=24
      imss(22)=24  
    """
    
phojf.write(phojinp)
phojf.close()
