evgenConfig.description = "MadGraph+Pythia8 production JO Ts single production Tsbj with Ts->ht with the AU2 CTEQ6L1 tune Ts Mass = 700 Lambda = 2 with fastsim and fullsim" 
evgenConfig.keywords = ["exotics", "top", "semileptonic"] 
evgenConfig.contact = ["timothy.robert.andeen@cern.ch"] 
evgenConfig.inputfilecheck = "TsSingleProd" 
evgenConfig.generators = ["MadGraph","Pythia8"] 
 
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py") 
include("MC12JobOptions/Pythia8_Photos.py") 
include("MC12JobOptions/Pythia8_LHEF.py") 
include("MC12JobOptions/LeptonFilter.py") 
 
topAlg.LeptonFilter.Ptcut = 20000. 
