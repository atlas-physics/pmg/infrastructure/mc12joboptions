evgenConfig.description = "CI->ee production with MSTW2008LO tune"
evgenConfig.keywords = ["Contact Interaction", "CIee", "electrons", "ee"]
evgenConfig.contact = [ "daniel.hayden@cern.ch" ]
evgenConfig.generators = [ "Pythia8" ]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += ["ContactInteractions:QCffbar2eebar = on"] # process
topAlg.Pythia8.Commands += ["ContactInteractions:Lambda = 5000"] # mass scale
topAlg.Pythia8.Commands += ["ContactInteractions:etaRR = -1"] # interference
topAlg.Pythia8.Commands += ["PhaseSpace:mHatMin = 600"] # min invariant mass
topAlg.Pythia8.Commands += ["PhaseSpace:mHatMax = 1200"] # max invariant mass
