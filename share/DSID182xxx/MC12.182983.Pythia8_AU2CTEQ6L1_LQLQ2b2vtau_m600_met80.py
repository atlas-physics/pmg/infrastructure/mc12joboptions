
evgenConfig.generators = ["Pythia8"] 
evgenConfig.contact = ["Katharine Leney (at SPAMNOTcern.ch)"] 
evgenConfig.description = "LQ3 ->b + nu_tau, M_LQ = 600 GeV, Pythia 8, Efficiency ~95%"
evgenConfig.keywords    = [ "exotics", "leptoquark" ]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

topAlg.Pythia8.Commands += ["LeptoQuark:gg2LQLQbar = on"]
topAlg.Pythia8.Commands += ["LeptoQuark:qqbar2LQLQbar = on"]
topAlg.Pythia8.Commands += ["LeptoQuark:kCoup = 0.01"]
topAlg.Pythia8.Commands += ["42:0:products = 5 16"]
topAlg.Pythia8.Commands += ["42:m0 = 600"]
topAlg.Pythia8.Commands += ["42:mMin = 500"]
topAlg.Pythia8.Commands += ["42:mMax = 700"]

# MET filter
include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
METPartTruth.TruthCollectionName="GEN_EVENT"
topAlg.METAlg+=METPartTruth

if not hasattr(topAlg, "METFilter"):
    from GeneratorFilters.GeneratorFiltersConf import METFilter
    topAlg += METFilter()

if "METFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  [ "METFilter" ]

topAlg.METFilter.MissingEtCut = 80*GeV
topAlg.METFilter.MissingEtCalcOption = 1
