# Z' -> ZH -> ll+all

evgenConfig.description = "Zprime(->ZH->ll+all) 4000 GeV in default LRSM"
evgenConfig.contact = ["koji.terashi@cern.ch"]
evgenConfig.generators = ["Pythia"]
evgenConfig.keywords = ["Exotics", "LRSM", "Zprime", "ZH"]

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")

topAlg.Pythia.PythiaCommand += ["pysubs msel 0",
                                
                                # Z',Z,g production with interference                                
                                "pysubs msub 141 1", 

                                # Z' mass
                                "pydat2 pmas 32 1 4000",

                                # Z' mass lower cut
                                "pysubs ckin 1 1500", 

                                # Z' decay
                                "pydat3 mdme 289 1 0",   # d dbar
                                "pydat3 mdme 290 1 0",   # u ubar
                                "pydat3 mdme 291 1 0",   # s sbar
                                "pydat3 mdme 292 1 0",   # c cbar
                                "pydat3 mdme 293 1 0",   # b bbar
                                "pydat3 mdme 294 1 0",   # t tbar
                                
                                "pydat3 mdme 297 1 0",   # e- e+
                                "pydat3 mdme 298 1 0",   # nu_e nubar_e
                                "pydat3 mdme 299 1 0",   # mu- mu+
                                "pydat3 mdme 300 1 0",   # nu_mu nubar_mu
                                "pydat3 mdme 301 1 0",   # tau+ tau-
                                "pydat3 mdme 302 1 0",   # nu_tau nubar_tau
                                
                                "pydat3 mdme 305 1 0",   # W+ W-
                                "pydat3 mdme 306 1 0",   # H+ H-
                                "pydat3 mdme 307 1 0",   # Z gamma

                                # Z' decay to Zh
                                "pydat3 mdme 308 1 1",   # Z h

                                "pydat3 mdme 309 1 0",   # h A
                                "pydat3 mdme 310 1 0",   # H A

                                # Z decay to ll (l=e,mu,tau,nu)
                                "pydat3 mdme 174 1 0",   # dbar d
                                "pydat3 mdme 175 1 0",   # ubar u
                                "pydat3 mdme 176 1 0",   # sbar s
                                "pydat3 mdme 177 1 0",   # cbar c
                                "pydat3 mdme 178 1 0",   # bbar b
                                "pydat3 mdme 179 1 0",   # tbar t
                                "pydat3 mdme 182 1 1",   # e- e+
                                "pydat3 mdme 183 1 1",   # nu_e nubar_e
                                "pydat3 mdme 184 1 1",   # mu- mu+
                                "pydat3 mdme 185 1 1",   # nu_mu nubar_mu
                                "pydat3 mdme 186 1 1",   # mu- mu+
                                "pydat3 mdme 187 1 1",   # nu_tau nubar_tau

                                # H mass
                                "pydat2 pmas 25 1 125",

                                # H decay to all
                                "pydat3 mdme 210 1 1",   # d dbar
                                "pydat3 mdme 211 1 1",   # u ubar
                                "pydat3 mdme 212 1 1",   # s sbar
                                "pydat3 mdme 213 1 1",   # c cbar
                                "pydat3 mdme 214 1 1",   # b bbar
                                "pydat3 mdme 215 1 1",   # t tbar
                                "pydat3 mdme 218 1 1",   # e- e+
                                "pydat3 mdme 219 1 1",   # mu- mu+
                                "pydat3 mdme 220 1 1",   # tau- tau+
                                "pydat3 mdme 222 1 1",   # g g
                                "pydat3 mdme 223 1 1",   # gamma gamma
                                "pydat3 mdme 224 1 1",   # gamma Z
                                "pydat3 mdme 225 1 1",   # Z Z
                                "pydat3 mdme 226 1 1",   # W W

                                # Turn off FSR for Photos
                                "pydat1 parj 90 20000",

                                # Turn off tau decays
                                "pydat3 mdcy 15 1 0",

                                "pystat 1 3 4 5",
                                "pyinit dumpr 1 5",
                                "pyinit pylistf 1",
                                "pyinit pylisti 12"]

## ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )

## ... Photos
include ( "MC12JobOptions/Pythia_Photos.py" )
