evgenConfig.description = "McAtNlo+fHerwig/Jimmy ttbar production dilep with VBF Filter - CT10 PDF, AUET2 tune"
evgenConfig.generators = ["McAtNlo", "Herwig"]
evgenConfig.keywords = ["top", "ttbar", "dileptonic"]
evgenConfig.contact  = ["biagio.di.micco@cern.ch"]
evgenConfig.inputfilecheck = "mcatnlo.*ttbar_dilepton"
	
include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand+=[ "iproc mcatnlo" ]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")


include("MC12JobOptions/MultiObjectsFilter.py")
topAlg.MultiObjectsFilter.PtCut = 5000.
topAlg.MultiObjectsFilter.EtaCut = 10.0 
topAlg.MultiObjectsFilter.UseEle = True
topAlg.MultiObjectsFilter.UseMuo = True
topAlg.MultiObjectsFilter.UseJet = False
topAlg.MultiObjectsFilter.UsePho = False 
topAlg.MultiObjectsFilter.UseSumPt = False
topAlg.MultiObjectsFilter.PtCutEach = [5000.,5000.]

include("MC12JobOptions/VBFForwardJetsFilter.py")

VBFForwardJetsFilter = topAlg.VBFForwardJetsFilter
VBFForwardJetsFilter.JetMinPt=15.*GeV
VBFForwardJetsFilter.JetMaxEta=5.0
VBFForwardJetsFilter.NJets=2
VBFForwardJetsFilter.Jet1MinPt=15.*GeV
VBFForwardJetsFilter.Jet1MaxEta=5.0
VBFForwardJetsFilter.Jet2MinPt=15.*GeV
VBFForwardJetsFilter.Jet2MaxEta=5.0
VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2=False
VBFForwardJetsFilter.MassJJ=350.*GeV
VBFForwardJetsFilter.DeltaEtaJJ=2.0
VBFForwardJetsFilter.TruthJetContainer="AntiKt4TruthJets"
VBFForwardJetsFilter.LGMinPt=15.*GeV
VBFForwardJetsFilter.LGMaxEta=2.5
VBFForwardJetsFilter.DeltaRJLG=0.05
VBFForwardJetsFilter.RatioPtJLG=0.3

evgenConfig.minevents=2000

