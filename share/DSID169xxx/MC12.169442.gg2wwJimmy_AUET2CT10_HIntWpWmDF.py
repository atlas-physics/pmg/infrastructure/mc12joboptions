
evgenConfig.generators += [ 'gg2ww', 'Herwig' ] 
evgenConfig.description = 'gg2VV WW->lnulnu, S+B+I terms, using CT10 PDF and fHerwig with AUET2_CT10 configuration'
evgenConfig.keywords = ['diboson', 'leptonic', 'EW']
evgenConfig.contact = ['doug.schouten@triumf.ca']
evgenConfig.inputfilecheck = "gg2VV312.169442.ggWWlvlv_sigbkg_df"

include('MC12JobOptions/Jimmy_AUET2_CT10_Common.py')

topAlg.Herwig.HerwigCommand += ["iproc lhef",
                                "maxpr 10"]

include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')

