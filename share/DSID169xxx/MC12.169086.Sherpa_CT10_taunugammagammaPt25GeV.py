include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "tau+- nu gamma gamma production with up to two jets in ME+PS."
evgenConfig.keywords = [ "EW","gamma","tau","neutrino" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "Junichi.Tanaka@cern.ch" ]

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(processes){
  Process 93 93 ->  16 -15 22 22 93{2}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  Scales LOOSE_METS{MU_F2}{MU_R2} {6}
  End process;

  Process 93 93 ->  -16 15 22 22 93{2}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  Scales LOOSE_METS{MU_F2}{MU_R2} {6}
  End process;
}(processes)

(selector){
  Mass 90 90 40 E_CMS
  Mass 91 91 1.5 E_CMS
  PT 22  25 E_CMS
  PT 90   8 E_CMS
  DeltaR 22 93 0.1 1000
  DeltaR 90 22 0.1 1000
}(selector)
"""
evgenConfig.inputconfcheck = "169086.Sherpa_CT10_taunugammagammaPt25GeV"
evgenConfig.minevents=2000
