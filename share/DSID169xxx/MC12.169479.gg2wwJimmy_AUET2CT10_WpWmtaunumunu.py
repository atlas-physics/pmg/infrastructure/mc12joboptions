
evgenConfig.generators = [ 'gg2ww', 'Herwig' ]
evgenConfig.description = 'gg2WW WW->lnulnu using CT10 PDF and fHerwig with AUET2_CT10 configuration'
evgenConfig.keywords = ['diboson', 'leptonic', 'EW']
evgenConfig.contact = ['doug.schouten@triumf.ca']
evgenConfig.inputfilecheck = "gg2WW0312.169479.WpWm"

include('MC12JobOptions/Jimmy_AUET2_CT10_Common.py')

topAlg.Herwig.HerwigCommand += [ "iproc lhef",
                                 "maxpr 10" ]

include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')

