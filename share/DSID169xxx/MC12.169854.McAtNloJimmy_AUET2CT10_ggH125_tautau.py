evgenConfig.description = 'ggH->tautau using CT10 PDF and fHerwig with AUET2_CT10 configuration'
evgenConfig.keywords = ['SMHiggs', 'tau', 'ggF']
evgenConfig.contact = ['andres.jorge.tanasijczuk@cern.ch']
evgenConfig.generators = [ 'McAtNlo', 'Herwig' ]
evgenConfig.inputfilecheck = 'mcatnlo409.169854.ggHtautau'

include('MC12JobOptions/Jimmy_AUET2_CT10_Common.py')

topAlg.Herwig.HerwigCommand += [ "iproc mcatnlo",
                                 "maxpr 10" ]

include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')

