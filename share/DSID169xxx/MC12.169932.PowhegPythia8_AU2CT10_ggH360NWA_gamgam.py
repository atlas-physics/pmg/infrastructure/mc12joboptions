evgenConfig.description = "POWHEG+PYTHIA8 using NWA, ggH H->gamgam with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "gamma"]
evgenConfig.inputfilecheck = "ggH_NW_SM_M360"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 22 22'
                            ]

