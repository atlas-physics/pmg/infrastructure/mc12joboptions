include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

evgenConfig.generators += ["MadGraph","Pythia8"]

evgenConfig.description = "Pythia 8 for LHE files from MadGraph, AU12 CTEQ6L1 tune"
evgenConfig.keywords = ['nonSMhiggs','WH','tau','mu']
evgenConfig.inputfilecheck = 'WH125_taumu'
