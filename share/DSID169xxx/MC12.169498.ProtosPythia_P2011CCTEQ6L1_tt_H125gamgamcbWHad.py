evgenConfig.description = "Protos PYTHIA6 tt -> (H --> yy)cW(Had)b with Perugia2011C"
evgenConfig.keywords = ["BSMtop", "Higgs", "gamma"]
evgenConfig.generators += ["Protos" ]
evgenConfig.inputfilecheck = "HctWHad"

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")
## ... Tauola
include("MC12JobOptions/Pythia_Tauola.py")
## ... Photos
include("MC12JobOptions/Pythia_Photos.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos",   # Use PROTOS
                                "pyinit dumpr 1 12",
                                "pyinit pylistf 1",
                                # H decays
                                "pydat3 mdme 210 1 0",  # Turn off H decays except gg
                                "pydat3 mdme 211 1 0",
                                "pydat3 mdme 212 1 0",
                                "pydat3 mdme 213 1 0",
                                "pydat3 mdme 214 1 0",
                                "pydat3 mdme 215 1 0",
                                "pydat3 mdme 218 1 0",
                                "pydat3 mdme 219 1 0",
                                "pydat3 mdme 220 1 0",
                                "pydat3 mdme 222 1 0",
                                "pydat3 mdme 223 1 1",
                                "pydat3 mdme 224 1 0",
                                "pydat3 mdme 225 1 0",
                                "pydat3 mdme 226 1 0"
                                ]

