include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
## ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )

## ... Photos -- don't use it as we have photons generated by MadGraph
#include ( "MC12JobOptions/Pythia_Photos.py" )

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                                 "pydat1 parj 90 20000",  # Turn off FSR (have photons generated in MadGraph)
                                 ]

evgenConfig.inputfilecheck = 'group.phys-gener.madgraph.169703.ttgamgam_lvbjjb_fullme'
evgenConfig.generators += ["MadGraph"]
evgenConfig.description = 'ttgamgam in the ljets mode using MadGraph full ME calculations, showered by Pythia'
evgenConfig.keywords =["EW","ttbar","diphoton","ljets","semileptonic"]
evgenConfig.contact = ["loginov@fnal.gov"]
