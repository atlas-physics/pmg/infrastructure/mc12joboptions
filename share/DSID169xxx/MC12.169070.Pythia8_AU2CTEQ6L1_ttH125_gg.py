evgenConfig.description = "PYTHIA8 ttH H->2gluon with AU2 CT10"
evgenConfig.generators = ["Pythia8"]
evgenConfig.keywords = ["SMhiggs", "ttH", "gluon","inclusive"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 125',
                            'HiggsSM:gg2Httbar = on',
                            'HiggsSM:qqbar2Httbar = on',
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 21 21'
                            ]
