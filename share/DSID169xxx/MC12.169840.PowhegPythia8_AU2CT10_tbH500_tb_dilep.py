evgenConfig.description = "POWHEG+PYTHIA8 tHch Hch->tb, top dilep-ch with AU2,CT10"
evgenConfig.keywords = ["nonSMhiggs", "Hch","top","bottom","dileptonic"]
evgenConfig.inputfilecheck = "tHpl_2HDM_M500"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [ 'Higgs:useBSM = on',
                             '37:onMode = off',
                             '37:onIfMatch = 5 6',
                             '24:onMode = off',#decay of W
                             '24:onIfAny = 11 12 13 14 15 16'
                             ]
