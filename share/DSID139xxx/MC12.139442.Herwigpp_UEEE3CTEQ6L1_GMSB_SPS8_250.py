# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_gmsb.250_500_1_15_1_1.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino', 'squarks', 'sbottoms', 'stops', 'gauginos', 'sleptons', 'staus', 'sneutrinos'], slha_file)

# define metadata
evgenConfig.description = 'SPS8 grid generation with Lambda=240'
evgenConfig.keywords = ['SUSY', 'GMSB', 'SPS8']
evgenConfig.contact = [ 'helenka.przysiezniak@cern.ch', 'wolfgang.ehrenfeld@cern.ch']

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds
