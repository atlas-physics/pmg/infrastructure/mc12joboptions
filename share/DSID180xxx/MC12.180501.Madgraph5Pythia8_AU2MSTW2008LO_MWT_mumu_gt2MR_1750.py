evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = "TC MWT R1/R2->mumu decay NO lepton filter and AU2 MSTW2008LO tune"
evgenConfig.keywords = ["Exotics","Technicolor","MWT","leptons"]
evgenConfig.contact = ['thrynova@mail.cern.ch']

include ("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_MadGraph.py")

topAlg.Pythia8.Commands += [
    "Tune:pp = 5",
    "PDF:useLHAPDF = on",
    "PDF:LHAPDFset = MSTW2008lo68cl.LHgrid",
    "MultipartonInteractions:bProfile = 4",
    "MultipartonInteractions:a1 = 0.01",
    "MultipartonInteractions:pT0Ref = 1.87",
    "MultipartonInteractions:ecmPow = 0.28",
    "BeamRemnants:reconnectRange = 5.32",
    "SpaceShower:rapidityOrder=0"]

evgenConfig.inputfilecheck = 'mwt_mumu'
