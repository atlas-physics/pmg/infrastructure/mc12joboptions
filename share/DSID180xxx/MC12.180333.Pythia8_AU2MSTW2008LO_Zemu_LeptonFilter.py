
evgenConfig.description = "Z->emu production with the AU2 MSTW2008LO tune 12.2012"
evgenConfig.keywords = ["exotics", "Z", "e", "mu", "LFV"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # create Z bosons
                            "PhaseSpace:mHatMin = 60.",       # lower invariant mass
                            "23:onMode = off",                # switch off all Z decays
                            "23:oneChannel = 1 0.5 0 11 -13", # Z->e-mu+
                            "23:addChannel = 1 0.5 0 -11 13"] # Z->e+mu-

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 5000.    
topAlg.MultiLeptonFilter.Etacut = 2.8
topAlg.MultiLeptonFilter.NLeptons = 2
