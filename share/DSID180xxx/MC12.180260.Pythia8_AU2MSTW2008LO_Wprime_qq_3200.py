

M_Wprime = 3200.0  

evgenConfig.description = "Wprime "+str(M_Wprime)+" GeV, with the MSTW tune"
evgenConfig.keywords = ["Wprime"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on",
                            "34:m0 = "+str(M_Wprime), # Wprime mass
                            "Wprime:coup2WZ = 0", #Wprime Coupling to WZ set to off
                            "34:onMode = off", #turn off all Wprime decays
                            "34:onIfAny = 1 2 3 4 5 6 " #Wprime->qq 
                           ]

  