include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "ADD->mm production, 1200M, MS4000"
evgenConfig.keywords = [ "ADD" ]

evgenConfig.process="""
(processes){
  #
  # jet jet -> m+ m-
  #
  Process 93 93 -> 13 -13 93{1};
  CKKW sqr(20.0/E_CMS);
  End process;
}(processes)

(selector){
  Mass 13 -13 1200.0 10000.0
}(selector)

(model){
  MODEL = ADD

  N_ED  = 2          
  M_S   = 4000      
  M_CUT = 4000
  KK_CONVENTION = 1   

  MASS[39] = 100. 
  MASS[40] = 100.   

}(model)

(me){
  ME_SIGNAL_GENERATOR = Amegic
}(me)


"""
   


evgenConfig.minevents = 5000
evgenConfig.weighting = 0
