LQmass = 850

evgenConfig.description = "Pair produced scalar leptoquarks (M = "+str(LQmass)+" GeV) to di-electron jet channel"
evgenConfig.keywords = [ "Exotics", "Leptoquark" ]
evgenConfig.contact = [ "Vikas.Bansal@cern.ch" ]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["LeptoQuark:gg2LQLQbar = on",
                            "LeptoQuark:qqbar2LQLQbar = on",
                            "LeptoQuark:kCoup = 0.01",                            
                            "42:0:products = 2 11",
                            "42:m0 = "+ str(LQmass),
                            "42:mMin ="+ str(LQmass-200),
                            "42:mMax ="+ str(LQmass+200)]
