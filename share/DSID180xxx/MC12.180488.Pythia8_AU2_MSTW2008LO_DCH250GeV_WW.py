###############################################################
#
# Job options file
# Pythia8 Doubly Charged Higgs WW mode
# contact: Kenji Hamano
#==============================================================
evgenConfig.description = "Pythia8 DCH production JO example with the AU2 MSTW2008 tune WW mode"
evgenConfig.keywords = ["Exotics", "DCH", "H++", "SSWW"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
    "9900042:m0 = 250.", # H++_R mass [GeV]
    "9900041:m0 = 250.", # H++_L mass [GeV]         
    "LeftRightSymmmetry:ffbar2HLHL=on", # double HL pair production
    "LeftRightSymmmetry:ffbar2HRHR=on", # double HR pair production  
    
    # set all couplings to leptons to 0.0
    "LeftRightSymmmetry:coupHee=0.0",
    "LeftRightSymmmetry:coupHmue=0.0",
    "LeftRightSymmmetry:coupHmumu=0.0",
    "LeftRightSymmmetry:coupHtaue=0.0",
    "LeftRightSymmmetry:coupHtaumu=0.0",
    "LeftRightSymmmetry:coupHtautau=0.0",
    ]

# 2-lepton filter
include('MC12JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut       = 4000
MultiLeptonFilter.Etacut      = 4
MultiLeptonFilter.NLeptons = 2


