evgenConfig.description = "Color scalar octet using MadGraph/Pythia"
evgenConfig.keywords = ["exotics", "scalarOctet"]
evgenConfig.inputfilecheck = 'ColorOctet_s8_400'

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")
