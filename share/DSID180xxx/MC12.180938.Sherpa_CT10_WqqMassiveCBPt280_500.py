include( "MC12JobOptions/Sherpa_CT10_Common.py" )
evgenConfig.description = "Wqq + jets, with c and b-quarks treated as massive"
evgenConfig.keywords = [ "W", "hadronic" ]
evgenConfig.contact  = ["ayana@cern.ch"]
evgenConfig.inputconfcheck = "WqqMassiveCBPt280_500"
evgenConfig.minevents = 200
evgenConfig.process="""
(run){
  MASSIVE[4]=1
  MASSIVE[5]=1 
  ME_SIGNAL_GENERATOR=Comix
}(run)

(processes){
  Process 93 93 -> 24[a] 93 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> 24[a] 93 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -24[a] 93 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -24[a] 93 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> 24[a] 93 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> 24[a] 93 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -24[a] 93 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -24[a] 93 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> 24[a] 93 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> 24[a] 93 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -24[a] 93 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -24[a] 93 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> 24[a] 93 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> 24[a] 93 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -24[a] 93 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -24[a] 93 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> 24[a] 93 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> 24[a] 93 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -24[a] 93 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -24[a] 93 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -4 -4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -4 -4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -4 -4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -4 -4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -4 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -4 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -4 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -4 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -4 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -4 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -4 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -4 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -5 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -5 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -5 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -5 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -4 -> -4 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -4 -4 -5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -4 -4 -5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -4 -4 -5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -4 -4 -5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -4 -5 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -4 -5 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -4 -5 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -4 -5 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -4 -5 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -4 -5 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -4 -5 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -4 -5 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -5 -5 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -5 -5 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -5 -5 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -5 -5 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -5 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -5 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -5 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> -5 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 -4 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 -4 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 -4 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 -4 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 -5 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 -5 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 -5 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 -5 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 4 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 4 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 4 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 4 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 4 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 4 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 4 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 4 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -4 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -5 4 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -5 4 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -5 4 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -5 4 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -5 5 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -5 5 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -5 5 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> -5 5 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> 4 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> 4 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> 4 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 4 -> 4 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -4 -4 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -4 -4 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -4 -4 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -4 -4 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -4 4 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -4 4 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -4 4 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -4 4 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -4 5 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -4 5 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -4 5 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -4 5 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -5 5 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -5 5 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -5 5 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> -5 5 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> 5 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> 5 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> 5 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 5 -> 5 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -4 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -4 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -4 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -4 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -4 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -4 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -4 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -4 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -5 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -5 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -5 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -5 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 4 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 4 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 4 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 4 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -4 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -5 5 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -5 5 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -5 5 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 93 -> -5 5 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 -5 -> -4 -5 -5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 -5 -> -4 -5 -5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 -5 -> -4 -5 -5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 -5 -> -4 -5 -5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 -5 -> -5 -5 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 -5 -> -5 -5 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 -5 -> -5 -5 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 -5 -> -5 -5 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 -5 -> -5 -5 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 -5 -> -5 -5 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 -5 -> -5 -5 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 -5 -> -5 -5 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -4 -5 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -4 -5 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -4 -5 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -4 -5 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 -5 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 -5 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 -5 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 -5 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 4 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 4 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 4 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 4 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 4 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 4 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 4 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 4 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 4 -> -5 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 -4 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 -4 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 -4 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 -4 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 -5 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 -5 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 -5 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 -5 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 4 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 4 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 4 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 4 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 4 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 4 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 4 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 4 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -4 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -5 4 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -5 4 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -5 4 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -5 4 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -5 5 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -5 5 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -5 5 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> -5 5 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> 4 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> 4 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> 4 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 5 -> 4 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -4 -5 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -4 -5 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -4 -5 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -4 -5 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -4 -5 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -4 -5 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -4 -5 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -4 -5 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -5 -5 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -5 -5 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -5 -5 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -5 -5 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -5 4 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -5 4 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -5 4 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -5 4 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -5 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -5 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -5 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -5 93 -> -5 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> -4 4 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> -4 4 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> -4 4 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> -4 4 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> -5 4 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> -5 4 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> -5 4 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> -5 4 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> 4 4 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> 4 4 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> 4 4 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> 4 4 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> 4 4 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> 4 4 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> 4 4 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> 4 4 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> 4 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> 4 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> 4 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 4 -> 4 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> -4 4 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> -4 4 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> -4 4 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> -4 4 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> -5 5 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> -5 5 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> -5 5 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> -5 5 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> 4 4 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> 4 4 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> 4 4 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> 4 4 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> 4 5 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> 4 5 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> 4 5 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> 4 5 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> 5 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> 5 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> 5 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 5 -> 5 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -4 4 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -4 4 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -4 4 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -4 4 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -4 4 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -4 4 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -4 4 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -4 4 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -5 4 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -5 4 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -5 4 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -5 4 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -5 5 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -5 5 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -5 5 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> -5 5 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> 4 4 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> 4 4 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> 4 4 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> 4 4 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> 4 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> 4 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> 4 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 93 -> 4 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 5 -> -4 5 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 5 -> -4 5 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 5 -> -4 5 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 5 -> -4 5 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 5 -> 4 5 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 5 -> 4 5 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 5 -> 4 5 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 5 -> 4 5 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 5 -> 5 5 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 5 -> 5 5 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 5 -> 5 5 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 5 -> 5 5 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> -4 4 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> -4 4 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> -4 4 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> -4 4 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> -4 5 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> -4 5 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> -4 5 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> -4 5 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> -5 5 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> -5 5 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> -5 5 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> -5 5 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> 4 5 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> 4 5 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> 4 5 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> 4 5 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> 5 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> 5 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> 5 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 93 -> 5 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 -4 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 -4 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 -4 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 -4 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 -5 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 -5 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 -5 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 -5 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 4 4 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 4 4 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 4 4 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 4 4 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 4 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 4 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 4 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 4 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -4 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -5 4 5 24[a] 93{1}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -5 4 5 24[a] 93{1}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -5 4 5 -24[a] 93{1}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -5 4 5 -24[a] 93{1}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -5 5 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -5 5 24[a] 93{2}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -5 5 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> -5 5 -24[a] 93{2}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> 4 24[a] 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> 4 24[a] 93{3}
  Decay 24[a] -> 4 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> 4 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> 4 -24[a] 93{3}
  Decay -24[a] -> -4 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

}(processes)

(selwminus){
  DecayMass -24 2.0 E_CMS
  Decay(PPerp(p[0]+p[1])) -24 280.0 500.0
  PT 93 1.0 E_CMS
  PT 4 1.0 E_CMS
  PT -4 1.0 E_CMS
  PT 5 1.0 E_CMS
  PT -5 1.0 E_CMS
}(selwminus)

(selwplus){
  DecayMass 24 2.0 E_CMS
  Decay(PPerp(p[0]+p[1])) 24 280.0 500.0
  PT 93 1.0 E_CMS
  PT 4 1.0 E_CMS
  PT -4 1.0 E_CMS
  PT 5 1.0 E_CMS
  PT -5 1.0 E_CMS
}(selwplus)
"""

