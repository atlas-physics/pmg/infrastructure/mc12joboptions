evgenConfig.description = "Pythia8 gamma + jet sample. 120 < Mgg < 200 GeV"
evgenConfig.keywords = ["Exotics", "jets", "gamma" ]
evgenConfig.minevents = 200

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PromptPhoton:gg2ggamma = on",
                            "PhaseSpace:pTHatMin = 18",
                            "PhaseSpace:mHatMin = 120."]

from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
topAlg += DirectPhotonFilter()
DirectPhotonFilter = topAlg.DirectPhotonFilter
DirectPhotonFilter.Ptcut = 20000.
DirectPhotonFilter.Etacut =  2.7
DirectPhotonFilter.NPhotons = 2
StreamEVGEN.RequireAlgs +=  [ "DirectPhotonFilter" ]

from GeneratorFilters.GeneratorFiltersConf import MassRangeFilter
topAlg += MassRangeFilter()
MassRangeFilter = topAlg.MassRangeFilter
MassRangeFilter.EtaCut  = 2.7
MassRangeFilter.EtaCut2 = 2.7
MassRangeFilter.PartId  = 22
MassRangeFilter.PartId2 = 22
MassRangeFilter.PtCut = 20000.
MassRangeFilter.PtCut2 = 20000.
MassRangeFilter.InvMassMin = 120000.
MassRangeFilter.InvMassMax = 200000.
StreamEVGEN.RequireAlgs += [ "MassRangeFilter" ]
