evgenConfig.description = "Protos+Pythia6 for same sign tops ttLL"

evgenConfig.keywords = ["tt", "top"]

evgenConfig.inputfilecheck = "ttLL"

evgenConfig.generators = ["Protos", "Pythia"]

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")

include("MC12JobOptions/Pythia_Tauola.py" )
include("MC12JobOptions/Pythia_Photos.py" )

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

