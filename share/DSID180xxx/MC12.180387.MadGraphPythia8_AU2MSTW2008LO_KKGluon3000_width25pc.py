include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")


evgenConfig.description = "MadGraph5Pythia8 mstw2008LO; pp -> o1 -> ttbar ALLchannels, where o1=colour octet state in topBSM model; GAMMA/M=25%; M=3000 GeV"
evgenConfig.keywords = ["MadGraph", "Pythia", "mstw2008LO", "KKGluon"]
evgenConfig.generators += ["MadGraph","Pythia8"]
evgenConfig.inputfilecheck = "KKGluonTTbar"
