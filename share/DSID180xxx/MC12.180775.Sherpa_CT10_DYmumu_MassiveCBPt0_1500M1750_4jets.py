include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z/gamma* -> mumu + up to 4 jets. Massive B and C quarks. Di-lepton invariant mass 1500-1750 GeV"
evgenConfig.keywords = [ "Z/gamma", "leptonic", "mu", "heavyquark" ]
evgenConfig.contact  = [ "giovanni.siragusa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.process="DYmumu"
evgenConfig.inputconfcheck = "DYmumu_MassiveCBPt0"


"""
(run){
  MASSIVE[4]=1
  MASSIVE[5]=1
}(run)

(processes){
Process 93  93 -> 13 -13 93{4}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 5  -5 -> 13 -13 93{4}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 4 -4 -> 13 -13 93{4}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 93 93 -> 13 -13 5 -5 93{2}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 5 -5 -> 13 -13 5 -5 93{2}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 93 5 -> 13 -13 5 93{3}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 93 -5 -> 13 -13 -5 93{3}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 93 93 -> 13 -13 4 -4 93{2}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 93  4 -> 13 -13 4 93{3}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 93 -4 -> 13 -13 -4 93{3}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process  4 -4 -> 13 -13 4 -4 93{2}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process  5 -5 -> 13 -13 4 -4 93{2}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process  4 -4 -> 13 -13 5 -5 93{2}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 93  5 -> 13 -13 5 4 -4 93{1}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 93  4 -> 13 -13 4 5 -5 93{1}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 93 -5 -> 13 -13 -5 4 -4 93{1}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 93 -4 -> 13 -13 -4 5 -5 93{1}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process  4  5 -> 13 -13 4 5 93{2}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process -4  5 -> 13 -13 -4 5 93{2}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process  4 -5 -> 13 -13 4 -5 93{2}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process -4 -5 -> 13 -13 -4 -5 93{2}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
End process;

}(processes)

(selector){
  Mass 13 -13 1500 1750
}(selector)

(run){
  NLO_Mode 3
}(run)
"""
