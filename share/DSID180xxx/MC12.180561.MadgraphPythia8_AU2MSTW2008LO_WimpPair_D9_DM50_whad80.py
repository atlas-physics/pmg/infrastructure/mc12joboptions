include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 2",
                            "Next:numberShowEvent = 2",
                            ]

evgenConfig.description = "mono w hadronic leading jet pT > 80GeV : D9 DM 50 GeV"
evgenConfig.keywords = ["mono w", "hadronic"]
evgenConfig.inputfilecheck = 'whad'
