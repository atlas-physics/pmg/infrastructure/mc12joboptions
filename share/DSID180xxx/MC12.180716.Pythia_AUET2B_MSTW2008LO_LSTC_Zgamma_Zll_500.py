evgenConfig.description = "LSTC Zgamma 500 point"
evgenConfig.keywords = ["LSTC", "Zgamma", "500"]
evgenConfig.contact = ["louis.helary@cern.ch"]

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")

MrhoT=500.
MW=80.399
MaT=MrhoT*1.1
MPiT=MrhoT-MW


topAlg.Pythia.PythiaCommand += ["pysubs msel 0",
                       "pysubs msub 194 0",
                       "pysubs msub 361 0",
                       "pysubs msub 362 0",
                       "pysubs msub 363 0",
                       "pysubs msub 364 0",
                       "pysubs msub 365 0",
                       "pysubs msub 366 0",
                       "pysubs msub 368 0",
                       "pysubs msub 370 0",
                       "pysubs msub 371 0",
                       "pysubs msub 372 0",
                       "pysubs msub 373 0",
                       "pysubs msub 374 0",
                       "pysubs msub 375 0",
                       "pysubs msub 376 0",
                       "pysubs msub 367 0",
                       "pysubs msub 377 0",
                       "pysubs msub 378 0",
                       "pysubs msub 379 1",
                       "pysubs msub 380 0",
                       "pysubs ckin 1 "+str(MrhoT-40),
                       "pysubs ckin 2 "+str(MrhoT+40)]

topAlg.Pythia.PythiaCommand += ["pydat3 mdme 174 1 0",
                        "pydat3 mdme 175 1 0",
                        "pydat3 mdme 176 1 0",
                        "pydat3 mdme 177 1 0",
                        "pydat3 mdme 178 1 0",
                        "pydat3 mdme 179 1 0",
                        "pydat3 mdme 182 1 1",
                        "pydat3 mdme 183 1 0",
                        "pydat3 mdme 184 1 1",
                        "pydat3 mdme 185 1 0",
                        "pydat3 mdme 186 1 0",
                        "pydat3 mdme 187 1 0"]

topAlg.Pythia.PythiaCommand += ["pydat2 pmas 3000111 1 "+str(MPiT),
                        "pydat2 pmas 3000211 1 "+str(MPiT),
                        "pydat2 pmas 3000221 1 "+str(MPiT*2),
                        "pydat2 pmas 3000115 1 "+str(MaT),
                        "pydat2 pmas 3000215 1 "+str(MaT),
                        "pydat2 pmas 3000113 1 "+str(MrhoT),
                        "pydat2 pmas 3000213 1 "+str(MrhoT),
                        "pydat2 pmas 3000223 1 "+str(MrhoT)]

topAlg.Pythia.PythiaCommand += ["pypars mstp 42 0"]
topAlg.Pythia.PythiaCommand += ["pypars mstp 32 4"]
topAlg.Pythia.PythiaCommand += ["pytcsm rtcm 47 1.",
                        "pytcsm rtcm 12 "+str(MrhoT),
                        "pytcsm rtcm 13 "+str(MrhoT),
                        "pytcsm rtcm 48 "+str(MrhoT),
                        "pytcsm rtcm 49 "+str(MrhoT),
                        "pytcsm rtcm 50 "+str(MrhoT),
                        "pytcsm rtcm 51 "+str(MrhoT),
                        "pytcsm rtcm 2 1.",
                        "pytcsm rtcm 3 0.3333333"]

topAlg.Pythia.PythiaCommand += ["pydat1 parj 90 20000",
                         "pydat3 mdcy 15 1 0"]

include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

include("MC12JobOptions/PhotonFilter.py")
PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 30000.
PhotonFilter.Etacut = 3.
PhotonFilter.NPhotons = 1

include("MC12JobOptions/LeptonFilter.py")
LeptonFilter = topAlg.LeptonFilter
LeptonFilter.Ptcut = 15000.
LeptonFilter.Etacut = 3.

