evgenConfig.description = "CI->mumu production with MSTW2008LO tune"
evgenConfig.keywords = ["Contact Interaction", "CImumu", "muons", "mumu"]
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
 
 
topAlg.Pythia8.Commands += ["ContactInteractions:QCffbar2mumubar = on"] # process
topAlg.Pythia8.Commands += ["ContactInteractions:Lambda = 20000"] # mass scale
topAlg.Pythia8.Commands += ["ContactInteractions:etaLR = 1"] # interference
topAlg.Pythia8.Commands += ["PhaseSpace:mHatMin = 600"] # min invariant mass
topAlg.Pythia8.Commands += ["PhaseSpace:mHatMax = 1200"] # max invariant mass
