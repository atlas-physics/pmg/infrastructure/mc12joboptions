include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "ADD->mm production, 600M1200, MS3000"
evgenConfig.keywords = [ "ADD" ]

evgenConfig.process="""
(processes){
  #
  # jet jet -> m+ m-
  #
  Process 93 93 -> 13 -13 93{1};
  CKKW sqr(20.0/E_CMS);
  End process;
}(processes)

(selector){
  Mass 13 -13 600.0 1200.0
}(selector)

(model){
  MODEL = ADD

  N_ED  = 2          
  M_S   = 3000      
  M_CUT = 3000
  KK_CONVENTION = 1   

  MASS[39] = 100. 
  MASS[40] = 100.   

}(model)

(me){
  ME_SIGNAL_GENERATOR = Amegic
}(me)


"""
   


evgenConfig.minevents = 5000
evgenConfig.weighting = 0
