evgenConfig.description = "positive excited muon m*=750GeV, Lambda=5TeV (m m* -> m m q q)"
evgenConfig.keywords = ["exotics","compositeness","ExcitedMuon"]
evgenConfig.inputfilecheck = "muStar" 

include ( "MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include ( "MC12JobOptions/Pythia8_CompHep.py" )
