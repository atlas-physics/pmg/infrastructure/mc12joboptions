evgenConfig.description = "High pT Hadronic W pT > 350"
evgenConfig.keywords = ["Whad"] 

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")	 

topAlg.Pythia8.Commands += ["PartonLevel:FSR = on"] # turn on FSR (no Photons interface to Pythia8)
topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMin = 350."]
topAlg.Pythia8.Commands += ["WeakBosonAndParton:qqbar2Wg = on"]
topAlg.Pythia8.Commands += ["WeakBosonAndParton:qg2Wq = on"]
topAlg.Pythia8.Commands += ["24:onMode = off"] # switch off all W decays
topAlg.Pythia8.Commands += ["24:onIfAny = 1 2 3 4 5"] # switch on W->had

