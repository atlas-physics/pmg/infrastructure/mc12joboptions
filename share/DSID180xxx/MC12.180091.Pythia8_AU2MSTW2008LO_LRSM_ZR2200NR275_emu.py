evgenConfig.keywords = [ "LRSM","heavy neutrino","ZR" ]
evgenConfig.contact  = [ "kirill.skovpen@cern.ch" ]
evgenConfig.description = "pp->ZR(2200)->NN(275)->lljjjj; l=e,mu; Ne-Nmu mixing"

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += [
                            "LeftRightSymmmetry:ffbar2ZR = on",
                            "9900023:m0 = 2200",
                            "9900012:m0 = 275",
                            "9900014:m0 = 275",
                            "9900016:m0 = 275",
                            "9900023:onMode = off",
                            "9900023:8:products = 9900012 9900014",
                            "9900023:11:products = 9900014 9900012",
                            "9900023:8:onMode = on",
                            "9900023:11:onMode = on"
                            ]
