include('MC12JobOptions/AlpgenPythia_SUSY_P2011C_DY.py')
evgenConfig.minevents = 10
evgenConfig.inputconfcheck = "ZtautauNp3Excl_Mll10to40"

include("MC12JobOptions/MultiElecMuTauFilter.py")
MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
topAlg.MultiElecMuTauFilter.NLeptons  = 2
topAlg.MultiElecMuTauFilter.MinPt = 5000.
topAlg.MultiElecMuTauFilter.MaxEta = 2.8
topAlg.MultiElecMuTauFilter.IncludeHadTaus = 1
