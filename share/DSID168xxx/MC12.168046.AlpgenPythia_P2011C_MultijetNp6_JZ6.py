include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np6_JZ6'
evgenConfig.minevents = 25
evgenConfig.description = 'pp -> 6 or more light jets, for leading jet pT range = 1500-2000 GeV'
