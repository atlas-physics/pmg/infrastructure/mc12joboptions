include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np4_incl_JZ5'
evgenConfig.minevents = 100
evgenConfig.description = 'pp -> 4 or more light jets, for leading jet pT range = 1000-1500 GeV'
