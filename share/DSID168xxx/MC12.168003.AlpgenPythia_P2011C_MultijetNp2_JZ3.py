include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np2_JZ3'
evgenConfig.minevents = 5000
evgenConfig.description = 'pp -> 2 light jets, for leading jet pT range = 200-500 GeV'
