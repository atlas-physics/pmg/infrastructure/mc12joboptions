include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np2_JZ5'
evgenConfig.minevents = 5000
evgenConfig.description = 'pp -> 2 light jets, for leading jet pT range = 1000-1500 GeV'
