include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np3_JZ6'
evgenConfig.minevents = 5000
evgenConfig.description = 'pp -> 3 light jets, for leading jet pT range = 1500-2000 GeV'
