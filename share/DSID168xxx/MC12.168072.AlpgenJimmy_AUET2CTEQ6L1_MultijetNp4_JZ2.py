include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np4_JZ2'
evgenConfig.description = 'pp -> 4 light jets, for leading jet pT range = 80-200 GeV'
evgenConfig.minevents = 200
