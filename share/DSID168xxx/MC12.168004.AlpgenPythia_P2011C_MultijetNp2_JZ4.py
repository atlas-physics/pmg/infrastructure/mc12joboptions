include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np2_JZ4'
evgenConfig.minevents = 5000
evgenConfig.description = 'pp -> 2 light jets, for leading jet pT range = 500-1000 GeV'
