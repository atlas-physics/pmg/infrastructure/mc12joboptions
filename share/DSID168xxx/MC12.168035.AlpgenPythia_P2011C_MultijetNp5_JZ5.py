include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np5_JZ5'
evgenConfig.minevents = 50
evgenConfig.description = 'pp -> 5 light jets, for leading jet pT range = 1000-1500 GeV'
