include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

slha_file = "susy_GGMHiggsinoNLSP_600_200.slha"
topAlg.Pythia.SusyInputFile = slha_file

topAlg.Pythia.PythiaCommand += [
                           "pysubs msel 0",
                           "pymssm imss 1 11",
                           "pymssm imss 11 1",
                           "pymssm imss 21 50",
                           "pymssm imss 22 50",
                           "pysubs msub 243 1",
                           "pysubs msub 244 1",
                           "pysubs msub 216 1",
                           "pysubs msub 217 1",
                           "pysubs msub 220 1",
                           "pysubs msub 226 1",
                           "pysubs msub 229 1",
                           "pysubs msub 230 1"]

evgenConfig.contact = [ "Natalia.Panikashvili@cern.ch" ]
evgenConfig.description = "PYTHIA 6 Higgsino-like NLSP GGM with tan beta = 1.5"
evgenConfig.process = "PYTHIA 6 used since PYTHIA 8.165 does not support decays to gravitinos"
evgenConfig.keywords = ["SUSY", "GGM", "higgsino", "nlsp"]
evgenConfig.auxfiles += [ slha_file ]

from GeneratorFilters.GeneratorFiltersConf import ZtoLeptonFilter
topAlg += ZtoLeptonFilter()
StreamEVGEN.RequireAlgs  = [ "ZtoLeptonFilter" ]


