#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     5.00000000e+01   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.23131700e+02   # h
        35     1.00658810e+03   # H
        36     1.00000000e+03   # A
        37     1.01125910e+03   # H+
   1000001     3.00058500e+03   # dL
   1000002     2.99952200e+03   # uL
   1000003     3.00058500e+03   # sL
   1000004     2.99952220e+03   # cL
   1000005     2.99270120e+03   # b1
   1000006     2.83304170e+03   # t1
   1000011     3.00037110e+03   # eL
   1000012     2.99930790e+03   # snue
   1000013     3.00037110e+03   # muL
   1000014     2.99930790e+03   # snum
   1000015     9.49737000e+01   # ta1
   1000016     2.99930790e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     4.89617000e+01   # N1
   1000023     2.66437600e+02   # N2
   1000024     2.66589300e+02   # C1
   1000025    -3.57105100e+02   # N3
   1000035     3.91705500e+02   # N4
   1000037     3.92894200e+02   # C2
   2000001     3.00010690e+03   # dR
   2000002     2.99978590e+03   # uR
   2000003     3.00010690e+03   # sR
   2000004     2.99978610e+03   # cR
   2000005     3.00797360e+03   # b2
   2000006     3.16324050e+03   # t2
   2000011     3.00032100e+03   # eR
   2000013     3.00032100e+03   # muR
   2000015     3.00038550e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -2.04457900e-02   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07389768e-01   # O_{11}
  1  2    -7.06823681e-01   # O_{12}
  2  1     7.06823681e-01   # O_{21}
  2  2     7.07389768e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.95937390e-01   # O_{11}
  1  2     7.18102465e-01   # O_{12}
  2  1    -7.18102465e-01   # O_{21}
  2  2     6.95937390e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2     1.00000000e+00   # O_{12}
  2  1    -1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.91299510e-01   # 
  1  2    -7.45902000e-03   # 
  1  3     1.29776210e-01   # 
  1  4    -2.06923000e-02   # 
  2  1    -7.07227400e-02   # 
  2  2    -8.29804420e-01   # 
  2  3     4.38696240e-01   # 
  2  4    -3.37591830e-01   # 
  3  1     7.55451100e-02   # 
  3  2    -8.51559900e-02   # 
  3  3    -6.95125520e-01   # 
  3  4    -7.09818540e-01   # 
  4  1    -8.13443300e-02   # 
  4  2     5.51468250e-01   # 
  4  3     5.54530680e-01   # 
  4  4    -6.17868660e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -7.85811720e-01   # Umix_{11}
  1  2     6.18465720e-01   # Umix_{12}
  2  1    -6.18465720e-01   # Umix_{21}
  2  2    -7.85811720e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -8.79018960e-01   # Vmix_{11}
  1  2     4.76786790e-01   # Vmix_{12}
  2  1    -4.76786790e-01   # Vmix_{21}
  2  2    -8.79018960e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     3.50000000e+02   # mu(Q)
     2     5.00000000e+01   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     5.00000000e+01   # M1
     2     3.00000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     3.00000000e+03   # m_eR
    35     3.00000000e+03   # m_muR
    36     8.47000000e+01   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     1.75000000e+04   # A_e
  2  2     1.75000000e+04   # A_mu
  3  3     1.75000000e+04   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.74679797e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.20441360e-02    2      1000022         1                       
      2.21390400e-01    2      1000023         1                       
      3.21070640e-03    2      1000025         1                       
      1.04478020e-01    2      1000035         1                       
      4.09766050e-01    2     -1000024         2                       
      2.49110760e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.75320750e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.01972070e-02    2      1000022         2                       
      2.35166040e-01    2      1000023         2                       
      1.66270610e-03    2      1000025         2                       
      9.35730860e-02    2      1000035         2                       
      5.11662900e-01    2      1000024         1                       
      1.47738040e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.74679797e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.20441390e-02    2      1000022         3                       
      2.21390440e-01    2      1000023         3                       
      3.21070710e-03    2      1000025         3                       
      1.04478050e-01    2      1000035         3                       
      4.09765960e-01    2     -1000024         4                       
      2.49110710e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.75320750e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.01972050e-02    2      1000022         4                       
      2.35165980e-01    2      1000023         4                       
      1.66270580e-03    2      1000025         4                       
      9.35730640e-02    2      1000035         4                       
      5.11662960e-01    2      1000024         3                       
      1.47738080e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  8.76431425e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.54140440e-02    2      1000022         5                       
      1.97660400e-01    2      1000023         5                       
      1.59507350e-01    2      1000025         5                       
      6.00267610e-02    2      1000035         5                       
      3.55597910e-01    2     -1000024         6                       
      1.56971650e-01    2     -1000037         6                       
      3.48218980e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  8.42754894e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.65748330e-02    2      1000022         6                       
      3.13866440e-02    2      1000023         6                       
      1.70052600e-01    2      1000025         6                       
      2.52223340e-01    2      1000035         6                       
      8.41626380e-02    2      1000024         5                       
      4.25599960e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.69377252e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.83120560e-01    2      1000022         1                       
      4.92797610e-03    2      1000023         1                       
      5.55196400e-03    2      1000025         1                       
      6.39953650e-03    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.77439275e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.83120680e-01    2      1000022         2                       
      4.92796070e-03    2      1000023         2                       
      5.55193010e-03    2      1000025         2                       
      6.39949130e-03    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.69377252e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.83120560e-01    2      1000022         3                       
      4.92797610e-03    2      1000023         3                       
      5.55196400e-03    2      1000025         3                       
      6.39953650e-03    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.77439275e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.83120620e-01    2      1000022         4                       
      4.92796020e-03    2      1000023         4                       
      5.55193050e-03    2      1000025         4                       
      6.39949040e-03    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  9.09003024e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.44103370e-03    2      1000022         5                       
      3.97168030e-02    2      1000023         5                       
      1.93302930e-01    2      1000025         5                       
      2.04405010e-01    2      1000035         5                       
      1.48917090e-05    2      1000021         5                       
      4.60430910e-02    2     -1000024         6                       
      4.64375760e-01    2     -1000037         6                       
      5.07004480e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.20210396e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.82982560e-01    2      1000024         5                       
      1.22105070e-01    2      1000037         5                       
      1.49236780e-01    2           23   1000006                       
      3.28291240e-02    2           24   1000005                       
      2.42576340e-02    2           24   2000005                       
      3.29915470e-02    2      1000022         6                       
      1.21325370e-01    2      1000023         6                       
      1.70780300e-01    2      1000025         6                       
      6.34916500e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.08667577e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.91939700e-02    2      1000022        11                       
      2.29531330e-01    2      1000023        11                       
      5.71844050e-04    2      1000025        11                       
      7.66789620e-02    2      1000035        11                       
      3.75652520e-01    2     -1000024        12                       
      2.28371370e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.09023117e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.41010040e-02    2      1000022        12                       
      1.90054160e-01    2      1000023        12                       
      4.81364690e-03    2      1000025        12                       
      1.05995250e-01    2      1000035        12                       
      4.69478610e-01    2      1000024        11                       
      1.35557350e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.08667577e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.91939700e-02    2      1000022        13                       
      2.29531330e-01    2      1000023        13                       
      5.71844050e-04    2      1000025        13                       
      7.66789620e-02    2      1000035        13                       
      3.75652520e-01    2     -1000024        14                       
      2.28371370e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.09023117e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.41010040e-02    2      1000022        14                       
      1.90054160e-01    2      1000023        14                       
      4.81364690e-03    2      1000025        14                       
      1.05995250e-01    2      1000035        14                       
      4.69478610e-01    2      1000024        13                       
      1.35557350e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  2.58107525e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  5.20028443e+02   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.40115670e-03    2      1000022        16                       
      1.49479870e-02    2      1000023        16                       
      3.78599100e-04    2      1000025        16                       
      8.33665390e-03    2      1000035        16                       
      4.76982740e-02    2      1000024        15                       
      2.77416950e-02    2      1000037        15                       
      8.93495620e-01    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  1.52449334e+01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.83120440e-01    2      1000022        11                       
      4.92798630e-03    2      1000023        11                       
      5.55198500e-03    2      1000025        11                       
      6.39956630e-03    2      1000035        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  1.52449334e+01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.83120440e-01    2      1000022        13                       
      4.92798630e-03    2      1000023        13                       
      5.55198500e-03    2      1000025        13                       
      6.39956630e-03    2      1000035        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  5.20810255e+02   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.48008070e-03    2      1000022        15                       
      2.34249140e-02    2      1000023        15                       
      1.34726340e-02    2      1000025        15                       
      1.45145760e-02    2      1000035        15                       
      2.94759500e-02    2     -1000024        16                       
      1.79193880e-02    2     -1000037        16                       
      4.47226790e-01    2           36   1000015                       
      4.46485730e-01    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  2.51432501e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.80041920e-03    3      1000024         1        -2             
      9.80041920e-03    3     -1000024         2        -1             
      9.80041920e-03    3      1000024         3        -4             
      9.80041920e-03    3     -1000024         4        -3             
      3.81287150e-02    3      1000024         5        -6             
      3.81287150e-02    3     -1000024         6        -5             
      5.85349560e-03    3      1000037         1        -2             
      5.85349560e-03    3     -1000037         2        -1             
      5.85349560e-03    3      1000037         3        -4             
      5.85349560e-03    3     -1000037         4        -3             
      1.89803030e-01    3      1000037         5        -6             
      1.89803030e-01    3     -1000037         6        -5             
      2.79184310e-03    3      1000022         1        -1             
      2.79184310e-03    3      1000022         3        -3             
      1.56109440e-04    3      1000022         5        -5             
      2.67208000e-02    3      1000022         6        -6             
      1.31639880e-04    2      1000023        21                       
      1.12314730e-02    3      1000023         1        -1             
      1.12314730e-02    3      1000023         3        -3             
      4.26856200e-03    3      1000023         5        -5             
      2.85656230e-02    3      1000023         6        -6             
      8.56881840e-04    2      1000025        21                       
      1.51097270e-04    3      1000025         1        -1             
      1.51097270e-04    3      1000025         3        -3             
      2.32589410e-02    3      1000025         5        -5             
      1.38497950e-01    3      1000025         6        -6             
      6.95701690e-04    2      1000035        21                       
      5.33505810e-03    3      1000035         1        -1             
      5.33505810e-03    3      1000035         3        -3             
      2.60342080e-02    3      1000035         5        -5             
      1.93181540e-01    3      1000035         6        -6             
      6.69210710e-05    2     -1000005         5                       
      6.69210710e-05    2      1000005        -5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  2.34752835e-01   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.43500460e-02    2      1000022        23                       
      9.15902850e-02    2      1000022        25                       
      4.22029790e-01    2      1000015       -15                       
      4.22029790e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  1.05661952e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.49494760e-02    2      1000024       -24                       
      4.49494760e-02    2     -1000024        24                       
      1.38102870e-01    2      1000022        23                       
      5.56512440e-04    3      1000023         2        -2             
      7.17493010e-04    3      1000023         1        -1             
      7.17493010e-04    3      1000023         3        -3             
      5.56512440e-04    3      1000023         4        -4             
      7.41156750e-04    3      1000023         5        -5             
      1.62773680e-04    3      1000023        11       -11             
      1.62773680e-04    3      1000023        13       -13             
      1.63418010e-04    3      1000023        15       -15             
      3.23918070e-04    3      1000023        12       -12             
      3.23918070e-04    3      1000023        14       -14             
      3.23918070e-04    3      1000023        16       -16             
      5.71486090e-02    2      1000022        25                       
      3.55049910e-01    2      1000015       -15                       
      3.55049910e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  1.39363527e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.39547760e-01    2      1000024       -24                       
      2.39547760e-01    2     -1000024        24                       
      4.89474310e-02    2      1000022        23                       
      3.64013510e-03    2      1000023        23                       
      6.77249060e-02    2      1000022        25                       
      1.32450650e-02    2      1000023        25                       
      1.93673420e-01    2      1000015       -15                       
      1.93673420e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  2.35508802e-01   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.80902350e-01    2      1000022        24                       
      8.19097700e-01    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  1.28471883e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.34209330e-01    2      1000022        24                       
      2.58757770e-01    2      1000023        24                       
      1.37684930e-05    3      1000025         2        -1             
      1.37684930e-05    3      1000025         4        -3             
      4.15506450e-01    2     -1000015        16                       
      1.74461810e-01    2      1000024        23                       
      1.70371330e-02    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.95445992e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.86401130e-04    2           13       -13                       
      5.33216740e-02    2           15       -15                       
      2.23658960e-03    2            3        -3                       
      7.26134420e-01    2            5        -5                       
      3.15634610e-02    2            4        -4                       
      2.03324640e-03    2           22        22                       
      3.98490430e-02    2           21        21                       
      6.88186520e-03    3           24        11       -12             
      6.88186520e-03    3           24        13       -14             
      6.88186520e-03    3           24        15       -16             
      2.06455960e-02    3           24        -2         1             
      2.06455960e-02    3           24        -4         3             
      6.88186520e-03    3          -24       -11        12             
      6.88186520e-03    3          -24       -13        14             
      6.88186520e-03    3          -24       -15        16             
      2.06455960e-02    3          -24         2        -1             
      2.06455960e-02    3          -24         4        -3             
      8.92781590e-04    3           23        12       -12             
      8.92781590e-04    3           23        14       -14             
      8.92781590e-04    3           23        16       -16             
      4.49327980e-04    3           23        11       -11             
      4.49327980e-04    3           23        13       -13             
      4.49327980e-04    3           23        15       -15             
      1.53935960e-03    3           23         2        -2             
      1.53935960e-03    3           23         4        -4             
      1.98307450e-03    3           23         1        -1             
      1.98307450e-03    3           23         3        -3             
      1.98307450e-03    3           23         5        -5             
      7.74725620e-03    2      1000022   1000022                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  6.63013478e+01   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.72327600e-04    2           13       -13                       
      7.79973340e-02    2           15       -15                       
      3.19248270e-03    2            3        -3                       
      7.87274000e-01    2            5        -5                       
      2.71648340e-04    2            6        -6                       
      6.42828640e-04    2      1000022   1000022                       
      6.20337810e-03    2      1000022   1000023                       
      7.11240150e-03    2      1000022   1000025                       
      2.15461690e-03    2      1000022   1000035                       
      9.09895920e-03    2      1000023   1000023                       
      1.27125610e-02    2      1000023   1000025                       
      8.04512180e-04    2      1000023   1000035                       
      3.61259040e-04    2      1000025   1000025                       
      1.03033370e-02    2      1000025   1000035                       
      3.30118320e-03    2      1000035   1000035                       
      2.23960560e-02    2      1000024  -1000024                       
      4.12191080e-03    2      1000037  -1000037                       
      2.58896290e-02    2      1000024  -1000037                       
      2.58896290e-02    2     -1000024   1000037                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  6.58200000e+01   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.72533100e-04    2           13       -13                       
      7.80571700e-02    2           15       -15                       
      3.19506650e-03    2            3        -3                       
      7.88557290e-01    2            5        -5                       
      2.89100720e-04    2            6        -6                       
      1.12075090e-05    2           21        21                       
      6.57850350e-04    2      1000022   1000022                       
      6.87071260e-03    2      1000022   1000023                       
      6.00119730e-03    2      1000022   1000025                       
      2.63806010e-03    2      1000022   1000035                       
      1.34295310e-02    2      1000023   1000023                       
      7.07152020e-03    2      1000023   1000025                       
      1.57069830e-03    2      1000023   1000035                       
      6.65844190e-04    2      1000025   1000025                       
      4.24593270e-03    2      1000025   1000035                       
      9.07622650e-03    2      1000035   1000035                       
      3.28553920e-02    2      1000024  -1000024                       
      1.17540220e-02    2      1000037  -1000037                       
      1.63903050e-02    2      1000024  -1000037                       
      1.63903050e-02    2     -1000024   1000037                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  5.94579946e+01   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.05070370e-04    2           14       -13                       
      8.73762970e-02    2           16       -15                       
      3.28736590e-03    2            4        -3                       
      7.63967160e-01    2            6        -5                       
      1.40071340e-02    2      1000024   1000022                       
      1.29357850e-04    2      1000024   1000023                       
      2.91968060e-02    2      1000024   1000025                       
      3.67910640e-02    2      1000024   1000035                       
      4.90351210e-03    2      1000037   1000022                       
      4.82033490e-02    2      1000037   1000023                       
      1.15964650e-02    2      1000037   1000025                       
      2.36410140e-04    2      1000037   1000035                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
