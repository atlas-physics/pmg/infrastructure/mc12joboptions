#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     5.00000000e+01   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.23124000e+02   # h
        35     1.00658890e+03   # H
        36     1.00000000e+03   # A
        37     1.01125950e+03   # H+
   1000001     3.00058500e+03   # dL
   1000002     2.99952200e+03   # uL
   1000003     3.00058500e+03   # sL
   1000004     2.99952220e+03   # cL
   1000005     2.99793510e+03   # b1
   1000006     2.83292750e+03   # t1
   1000011     3.00037110e+03   # eL
   1000012     2.99930790e+03   # snue
   1000013     3.00037110e+03   # muL
   1000014     2.99930790e+03   # snum
   1000015     9.49737000e+01   # ta1
   1000016     2.99930790e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     3.95134000e+01   # N1
   1000023     8.97597000e+01   # N2
   1000024     8.28942000e+01   # C1
   1000025    -1.27366700e+02   # N3
   1000035     2.08093700e+02   # N4
   1000037     2.09199100e+02   # C2
   2000001     3.00010690e+03   # dR
   2000002     2.99978590e+03   # uR
   2000003     3.00010690e+03   # sR
   2000004     2.99978610e+03   # cR
   2000005     3.00275730e+03   # b2
   2000006     3.16334280e+03   # t2
   2000011     3.00032100e+03   # eR
   2000013     3.00032100e+03   # muR
   2000015     3.00038550e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -2.04547800e-02   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07389513e-01   # O_{11}
  1  2    -7.06823936e-01   # O_{12}
  2  1     7.06823936e-01   # O_{21}
  2  2     7.07389513e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.71073368e-01   # O_{11}
  1  2     7.41390946e-01   # O_{12}
  2  1    -7.41390946e-01   # O_{21}
  2  2     6.71073368e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2     1.00000000e+00   # O_{12}
  2  1    -1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     8.35078480e-01   # 
  1  2    -1.32239390e-01   # 
  1  3     4.99752100e-01   # 
  1  4    -1.88160360e-01   # 
  2  1     5.01742660e-01   # 
  2  2     5.16741510e-01   # 
  2  3    -5.35746690e-01   # 
  2  4     4.40690430e-01   # 
  3  1     1.78954930e-01   # 
  3  2    -2.00962770e-01   # 
  3  3    -6.27347890e-01   # 
  3  4    -7.30769340e-01   # 
  4  1    -1.37379720e-01   # 
  4  2     8.21647760e-01   # 
  4  3     2.63928090e-01   # 
  4  4    -4.86172830e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -3.76647060e-01   # Umix_{11}
  1  2     9.26356790e-01   # Umix_{12}
  2  1    -9.26356790e-01   # Umix_{21}
  2  2    -3.76647060e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -7.01578320e-01   # Vmix_{11}
  1  2     7.12592360e-01   # Vmix_{12}
  2  1    -7.12592360e-01   # Vmix_{21}
  2  2    -7.01578320e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     1.10000000e+02   # mu(Q)
     2     5.00000000e+01   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     5.00000000e+01   # M1
     2     1.60000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     3.00000000e+03   # m_eR
    35     3.00000000e+03   # m_muR
    36     8.47000000e+01   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     5.50000000e+03   # A_e
  2  2     5.50000000e+03   # A_mu
  3  3     5.50000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.80045037e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.70280090e-02    2      1000022         1                       
      5.98686150e-02    2      1000023         1                       
      1.80925970e-02    2      1000025         1                       
      2.36026420e-01    2      1000035         1                       
      9.41467660e-02    2     -1000024         2                       
      5.64837690e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.80704494e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.42779730e-04    2      1000022         2                       
      1.22642180e-01    2      1000023         2                       
      9.34604740e-03    2      1000025         2                       
      2.08362880e-01    2      1000035         2                       
      3.25972650e-01    2      1000024         1                       
      3.33533470e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.80045037e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.70280150e-02    2      1000022         3                       
      5.98686260e-02    2      1000023         3                       
      1.80926010e-02    2      1000025         3                       
      2.36026470e-01    2      1000035         3                       
      9.41467580e-02    2     -1000024         4                       
      5.64837630e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.80704494e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.42779680e-04    2      1000022         4                       
      1.22642160e-01    2      1000023         4                       
      9.34604560e-03    2      1000025         4                       
      2.08362850e-01    2      1000035         4                       
      3.25972680e-01    2      1000024         3                       
      3.33533560e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  8.86070837e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.74814160e-01    2      1000022         5                       
      1.51963610e-01    2      1000023         5                       
      1.08597390e-01    2      1000025         5                       
      1.76880750e-02    2      1000035         5                       
      3.97154750e-01    2     -1000024         6                       
      1.13367920e-01    2     -1000037         6                       
      3.64140350e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  8.65198817e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.36971910e-02    2      1000022         6                       
      8.48658680e-02    2      1000023         6                       
      1.56888280e-01    2      1000025         6                       
      2.34388310e-01    2      1000035         6                       
      1.59279390e-01    2      1000024         5                       
      3.50881010e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.69377252e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.97804330e-01    2      1000022         1                       
      2.51543970e-01    2      1000023         1                       
      3.19411200e-02    2      1000025         1                       
      1.87105330e-02    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.77439275e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.97804330e-01    2      1000022         2                       
      2.51543970e-01    2      1000023         2                       
      3.19411010e-02    2      1000025         2                       
      1.87104980e-02    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.69377252e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.97804330e-01    2      1000022         3                       
      2.51543970e-01    2      1000023         3                       
      3.19411200e-02    2      1000025         3                       
      1.87105330e-02    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.77439275e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.97804390e-01    2      1000022         4                       
      2.51543970e-01    2      1000023         4                       
      3.19411050e-02    2      1000025         4                       
      1.87105000e-02    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  9.37446590e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.63742860e-02    2      1000022         5                       
      8.95737930e-02    2      1000023         5                       
      1.85144600e-01    2      1000025         5                       
      1.28643160e-01    2      1000035         5                       
      1.56319260e-01    2     -1000024         6                       
      3.57049700e-01    2     -1000037         6                       
      4.68951650e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.21934050e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.84656340e-01    2      1000024         5                       
      2.18745320e-02    2      1000037         5                       
      1.47424850e-01    2           23   1000006                       
      2.67860350e-02    2           24   1000005                       
      2.91421580e-02    2           24   2000005                       
      5.13704380e-02    2      1000022         6                       
      1.05721010e-01    2      1000023         6                       
      2.05562740e-01    2      1000025         6                       
      2.74620220e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.14040385e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.25519290e-02    2      1000022        11                       
      1.91232410e-01    2      1000023        11                       
      3.19965720e-03    2      1000025        11                       
      1.68183510e-01    2      1000035        11                       
      8.64103510e-02    2     -1000024        12                       
      5.18422190e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.14405339e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.06449660e-01    2      1000022        12                       
      1.76588260e-02    2      1000023        12                       
      2.71961110e-02    2      1000025        12                       
      2.42854360e-01    2      1000035        12                       
      2.99447890e-01    2      1000024        11                       
      3.06393120e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.14040385e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.25519290e-02    2      1000022        13                       
      1.91232410e-01    2      1000023        13                       
      3.19965720e-03    2      1000025        13                       
      1.68183510e-01    2      1000035        13                       
      8.64103510e-02    2     -1000024        14                       
      5.18422190e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.14405339e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.06449660e-01    2      1000022        14                       
      1.76588260e-02    2      1000023        14                       
      2.71961110e-02    2      1000025        14                       
      2.42854360e-01    2      1000035        14                       
      2.99447890e-01    2      1000024        13                       
      3.06393120e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  3.32005044e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.19854700e-01    2      1000022        15                       
      1.10115470e-02    2      1000023        15                       
      6.91337510e-02    2     -1000024        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  1.02184342e+02   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.31700010e-02    2      1000022        16                       
      7.16142680e-03    2      1000023        16                       
      1.10292140e-02    2      1000025        16                       
      9.84880780e-02    2      1000035        16                       
      2.46262710e-01    2      1000024        15                       
      1.44731640e-01    2      1000037        15                       
      4.49156910e-01    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  1.52449334e+01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.97804330e-01    2      1000022        11                       
      2.51543970e-01    2      1000023        11                       
      3.19411310e-02    2      1000025        11                       
      1.87105560e-02    2      1000035        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  1.52449334e+01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.97804330e-01    2      1000022        13                       
      2.51543970e-01    2      1000023        13                       
      3.19411310e-02    2      1000025        13                       
      1.87105560e-02    2      1000035        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  1.02235131e+02   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.95532300e-02    2      1000022        15                       
      1.19181030e-01    2      1000023        15                       
      5.84197190e-02    2      1000025        15                       
      7.81680050e-02    2      1000035        15                       
      3.49960030e-02    2     -1000024        16                       
      2.09959810e-01    2     -1000037        16                       
      2.25047570e-01    2           36   1000015                       
      2.24674700e-01    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  3.03401862e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.91949130e-03    3      1000024         1        -2             
      1.91949130e-03    3     -1000024         2        -1             
      1.91949130e-03    3      1000024         3        -4             
      1.91949130e-03    3     -1000024         4        -3             
      9.59472130e-02    3      1000024         5        -6             
      9.59472130e-02    3     -1000024         6        -5             
      1.14230090e-02    3      1000037         1        -2             
      1.14230090e-02    3     -1000037         2        -1             
      1.14230090e-02    3      1000037         3        -4             
      1.14230090e-02    3     -1000037         4        -3             
      1.39331270e-01    3      1000037         5        -6             
      1.39331270e-01    3     -1000037         6        -5             
      1.14989850e-04    2      1000022        21                       
      2.40534380e-03    3      1000022         1        -1             
      2.40534380e-03    3      1000022         3        -3             
      2.54183390e-02    3      1000022         5        -5             
      1.02656930e-02    3      1000022         6        -6             
      3.27915240e-04    2      1000023        21                       
      2.96252640e-03    3      1000023         1        -1             
      2.96252640e-03    3      1000023         3        -3             
      2.93610670e-02    3      1000023         5        -5             
      5.88442790e-02    3      1000023         6        -6             
      8.76249400e-04    2      1000025        21                       
      7.70984510e-04    3      1000025         1        -1             
      7.70984510e-04    3      1000025         3        -3             
      3.69115020e-02    3      1000025         5        -5             
      1.13750900e-01    3      1000025         6        -6             
      1.68431590e-04    2      1000035        21                       
      1.00357830e-02    3      1000035         1        -1             
      1.00357830e-02    3      1000035         3        -3             
      1.63041220e-02    3      1000035         5        -5             
      1.51380420e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  5.69081791e-05   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.89857940e-04    2      1000022        22                       
      1.32405500e-04    3     -1000024         2        -1             
      4.41351690e-05    3     -1000024        12       -11             
      4.41351690e-05    3     -1000024        14       -13             
      1.32405500e-04    3      1000024         1        -2             
      4.41351690e-05    3      1000024        11       -12             
      4.41351690e-05    3      1000024        13       -14             
      1.32405500e-04    3     -1000024         4        -3             
      4.41351690e-05    3     -1000024        16       -15             
      1.32405500e-04    3      1000024         3        -4             
      4.41351690e-05    3      1000024        15       -16             
      3.38807590e-02    3      1000022         2        -2             
      4.36866280e-02    3      1000022         1        -1             
      4.36866280e-02    3      1000022         3        -3             
      3.38807590e-02    3      1000022         4        -4             
      3.99385020e-02    3      1000022         5        -5             
      9.85435120e-03    3      1000022        11       -11             
      9.85435120e-03    3      1000022        13       -13             
      7.24382160e-01    3      1000022        15       -15             
      1.96838620e-02    3      1000022        12       -12             
      1.96838620e-02    3      1000022        14       -14             
      1.96838620e-02    3      1000022        16       -16             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  5.62131694e-02   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.59299670e-04    3     -1000024         2        -1             
      2.53099890e-04    3     -1000024        12       -11             
      2.53099890e-04    3     -1000024        14       -13             
      7.59299670e-04    3      1000024         1        -2             
      2.53099890e-04    3      1000024        11       -12             
      2.53099890e-04    3      1000024        13       -14             
      7.59299670e-04    3     -1000024         4        -3             
      2.53099890e-04    3     -1000024        16       -15             
      7.59299670e-04    3      1000024         3        -4             
      2.53099890e-04    3      1000024        15       -16             
      2.26622050e-03    3      1000022         2        -2             
      2.92173610e-03    3      1000022         1        -1             
      2.92173610e-03    3      1000022         3        -3             
      2.26622050e-03    3      1000022         4        -4             
      2.90531550e-03    3      1000022         5        -5             
      6.62739800e-04    3      1000022        11       -11             
      6.62739800e-04    3      1000022        13       -13             
      6.62291420e-04    3      1000022        15       -15             
      1.31930660e-03    3      1000022        12       -12             
      1.31930660e-03    3      1000022        14       -14             
      1.31930660e-03    3      1000022        16       -16             
      5.11703550e-05    3      1000023         2        -2             
      6.59563300e-05    3      1000023         1        -1             
      6.59563300e-05    3      1000023         3        -3             
      5.11703550e-05    3      1000023         4        -4             
      5.41098380e-05    3      1000023         5        -5             
      1.49659790e-05    3      1000023        11       -11             
      1.49659790e-05    3      1000023        13       -13             
      1.44841560e-05    3      1000023        15       -15             
      2.97811690e-05    3      1000023        12       -12             
      2.97811690e-05    3      1000023        14       -14             
      2.97811690e-05    3      1000023        16       -16             
      4.87897570e-01    2      1000015       -15                       
      4.87897570e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  6.22234827e-01   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.38957150e-01    2      1000024       -24                       
      4.38957150e-01    2     -1000024        24                       
      7.48616130e-03    2      1000022        23                       
      1.64395420e-02    2      1000023        23                       
      2.10053620e-04    3      1000025         2        -2             
      2.71061140e-04    3      1000025         1        -1             
      2.71061140e-04    3      1000025         3        -3             
      2.10053620e-04    3      1000025         4        -4             
      2.66848890e-04    3      1000025         5        -5             
      6.14768430e-05    3      1000025        11       -11             
      6.14768430e-05    3      1000025        13       -13             
      6.13045850e-05    3      1000025        15       -15             
      1.22235390e-04    3      1000025        12       -12             
      1.22235390e-04    3      1000025        14       -14             
      1.22235390e-04    3      1000025        16       -16             
      5.43498570e-03    2      1000022        25                       
      4.54725140e-02    2      1000015       -15                       
      4.54725140e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  5.82942166e-05   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.47848820e-01    3      1000022         2        -1             
      2.47848820e-01    3      1000022         4        -3             
      8.25868100e-02    3      1000022       -11        12             
      8.25868100e-02    3      1000022       -13        14             
      3.39128790e-01    3      1000022       -15        16             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  4.41537533e-01   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.30881900e-03    2      1000022        24                       
      5.45441690e-01    2      1000023        24                       
      1.87252160e-02    2      1000025        24                       
      1.05138430e-01    2     -1000015        16                       
      3.05896040e-01    2      1000024        23                       
      2.34899150e-02    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  1.13459284e-02   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.14640370e-05    2           13       -13                       
      2.33034960e-02    2           15       -15                       
      9.77471470e-04    2            3        -3                       
      3.17350030e-01    2            5        -5                       
      1.37823880e-02    2            4        -4                       
      9.17679160e-04    2           22        22                       
      1.73974580e-02    2           21        21                       
      3.00249670e-03    3           24        11       -12             
      3.00249670e-03    3           24        13       -14             
      3.00249670e-03    3           24        15       -16             
      9.00748930e-03    3           24        -2         1             
      9.00748930e-03    3           24        -4         3             
      3.00249670e-03    3          -24       -11        12             
      3.00249670e-03    3          -24       -13        14             
      3.00249670e-03    3          -24       -15        16             
      9.00748930e-03    3          -24         2        -1             
      9.00748930e-03    3          -24         4        -3             
      3.89420460e-04    3           23        12       -12             
      3.89420460e-04    3           23        14       -14             
      3.89420460e-04    3           23        16       -16             
      1.95991410e-04    3           23        11       -11             
      1.95991410e-04    3           23        13       -13             
      1.95991410e-04    3           23        15       -15             
      6.71449930e-04    3           23         2        -2             
      6.71449930e-04    3           23         4        -4             
      8.64992910e-04    3           23         1        -1             
      8.64992910e-04    3           23         3        -3             
      8.64992910e-04    3           23         5        -5             
      5.66451130e-01    2      1000022   1000022                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  7.05685583e+01   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.55859540e-04    2           13       -13                       
      7.32807370e-02    2           15       -15                       
      2.99942910e-03    2            3        -3                       
      7.39666460e-01    2            5        -5                       
      2.55446010e-04    2            6        -6                       
      1.02224960e-02    2      1000022   1000022                       
      1.08559760e-02    2      1000022   1000023                       
      3.13039010e-03    2      1000022   1000025                       
      4.68077140e-03    2      1000022   1000035                       
      1.84228880e-03    2      1000023   1000023                       
      8.79004040e-03    2      1000023   1000035                       
      4.01724220e-03    2      1000025   1000025                       
      2.38421320e-02    2      1000025   1000035                       
      4.70445770e-03    2      1000035   1000035                       
      4.78324700e-02    2      1000024  -1000024                       
      5.87702780e-03    2      1000037  -1000037                       
      2.88733620e-02    2      1000024  -1000037                       
      2.88733620e-02    2     -1000024   1000037                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  7.01399175e+01   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.55740310e-04    2           13       -13                       
      7.32474920e-02    2           15       -15                       
      2.99819490e-03    2            3        -3                       
      7.39968480e-01    2            5        -5                       
      2.71287080e-04    2            6        -6                       
      1.05169320e-05    2           21        21                       
      1.05987380e-02    2      1000022   1000022                       
      1.16447340e-02    2      1000022   1000023                       
      2.56324720e-03    2      1000022   1000025                       
      4.67922590e-03    2      1000022   1000035                       
      2.03241780e-03    2      1000023   1000023                       
      1.31305090e-05    2      1000023   1000025                       
      1.00136170e-02    2      1000023   1000035                       
      3.90249190e-03    2      1000025   1000025                       
      1.99431390e-02    2      1000025   1000035                       
      6.57542750e-03    2      1000035   1000035                       
      5.08040300e-02    2      1000024  -1000024                       
      8.62856120e-03    2      1000037  -1000037                       
      2.59248190e-02    2      1000024  -1000037                       
      2.59248190e-02    2     -1000024   1000037                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  6.37111606e+01   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.84724050e-04    2           14       -13                       
      8.15488320e-02    2           16       -15                       
      3.06811910e-03    2            4        -3                       
      7.13015260e-01    2            6        -5                       
      2.12666110e-02    2      1000024   1000022                       
      1.30622600e-02    2      1000024   1000023                       
      1.17775700e-02    2      1000024   1000025                       
      4.18545790e-02    2      1000024   1000035                       
      1.72374620e-02    2      1000037   1000022                       
      6.03052450e-02    2      1000037   1000023                       
      3.63467000e-02    2      1000037   1000025                       
      2.32783800e-04    2      1000037   1000035                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
