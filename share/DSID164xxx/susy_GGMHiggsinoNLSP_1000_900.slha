#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     9.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05433584E+01   # W+
        25     1.26000000E+02   # h
        35     2.00236272E+03   # H
        36     2.00000000E+03   # A
        37     2.00082808E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.50044976E+03   # ~d_L
   2000001     1.50008462E+03   # ~d_R
   1000002     1.49963474E+03   # ~u_L
   2000002     1.49983075E+03   # ~u_R
   1000003     1.50044976E+03   # ~s_L
   2000003     1.50008462E+03   # ~s_R
   1000004     1.49963474E+03   # ~c_L
   2000004     1.49983075E+03   # ~c_R
   1000005     1.49902923E+03   # ~b_1
   2000005     1.50150910E+03   # ~b_2
   1000006     1.47628558E+03   # ~t_1
   2000006     1.53908360E+03   # ~t_2
   1000011     1.50028057E+03   # ~e_L
   2000011     1.50025384E+03   # ~e_R
   1000012     1.49946545E+03   # ~nu_eL
   1000013     1.50028057E+03   # ~mu_L
   2000013     1.50025384E+03   # ~mu_R
   1000014     1.49946545E+03   # ~nu_muL
   1000015     1.49946747E+03   # ~tau_1
   2000015     1.50106861E+03   # ~tau_2
   1000016     1.49946545E+03   # ~nu_tauL
   1000021     1.00000000E+03   # ~g
   1000022     8.47495809E+02   # ~chi_10
   1000023    -9.00169155E+02   # ~chi_20
   1000025     1.00000076E+03   # ~chi_30
   1000035     1.05267358E+03   # ~chi_40
   1000024     8.57323355E+02   # ~chi_1+
   1000037     1.04293401E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     2.46509985E-01   # N_11
  1  2    -4.41705959E-01   # N_12
  1  3     6.13625049E-01   # N_13
  1  4    -6.06294460E-01   # N_14
  2  1     4.60322476E-03   # N_21
  2  2    -8.24818331E-03   # N_22
  2  3    -7.06742952E-01   # N_23
  2  4    -7.07407364E-01   # N_24
  3  1     8.73217961E-01   # N_31
  3  2     4.87329859E-01   # N_32
  3  3     3.03456854E-06   # N_33
  3  4    -3.57814158E-06   # N_34
  4  1     4.20368480E-01   # N_41
  4  2    -7.53212979E-01   # N_42
  4  3    -3.52106091E-01   # N_43
  4  4     3.63293062E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.72993600E-01   # U_11
  1  2     8.81065862E-01   # U_12
  2  1     8.81065862E-01   # U_21
  2  2     4.72993600E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.87429837E-01   # V_11
  1  2     8.73162158E-01   # V_12
  2  1     8.73162158E-01   # V_21
  2  2     4.87429837E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.08203644E-01   # cos(theta_t)
  1  2     7.06008214E-01   # sin(theta_t)
  2  1    -7.06008214E-01   # -sin(theta_t)
  2  2     7.08203644E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.52975987E-01   # cos(theta_b)
  1  2     7.57378611E-01   # sin(theta_b)
  2  1    -7.57378611E-01   # -sin(theta_b)
  2  2     6.52975987E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.01179236E-01   # cos(theta_tau)
  1  2     7.12985048E-01   # sin(theta_tau)
  2  1    -7.12985048E-01   # -sin(theta_tau)
  2  2     7.01179236E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89747918E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     9.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51486641E+02   # vev(Q)              
         4     3.90319394E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53880766E-01   # gprime(Q) DRbar
     2     6.34092846E-01   # g(Q) DRbar
     3     1.11186346E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.06648203E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.76340893E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80362239E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     1.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.85095148E+06   # M^2_Hd              
        22     1.50251100E+03   # M^2_Hu              
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     1.17590141E-05   # gluino decays
#          BR         NDA      ID1       ID2
     4.64116953E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.84232100E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
#           BR         NDA      ID1       ID2       ID3
     3.49058121E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     5.74635966E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.70836900E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     4.45919785E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.49058121E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     5.74635966E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.70836900E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     4.45919785E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.41993871E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     7.51442021E-05    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     4.83488427E-02    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     4.83488427E-02    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     4.83488427E-02    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     4.83488427E-02    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000024     1.45014896E-07   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.38840257E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.38840257E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.12947673E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.12947673E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.64241412E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000023     6.21585513E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.22730141E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     6.36717805E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     8.23268599E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     6.36717805E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     8.23268599E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     7.60729806E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     1.87540994E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.87540994E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.85874826E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     3.74176777E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     3.74176777E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     3.74176777E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.72989505E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.72989505E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     7.72989505E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     7.72989505E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.57667034E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.57667034E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.57667034E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.57667034E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.55478392E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.55478392E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
