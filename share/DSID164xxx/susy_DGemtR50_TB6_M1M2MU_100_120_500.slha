#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19693000e+02   # h
        35     1.00709770e+03   # H
        36     1.00000000e+03   # A
        37     1.00966200e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99900000e+03   # b1
   1000006     2.83487720e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     1.05259200e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     9.69821000e+01   # N1
   1000023     1.13558200e+02   # N2
   1000024     1.12549900e+02   # C1
   1000025    -5.04631500e+02   # N3
   1000035     5.14091100e+02   # N4
   1000037     5.14459200e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00165700e+03   # b2
   2000006     3.16163060e+03   # t2
   2000011     1.05254800e+02   # eR
   2000013     1.05254900e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68787850e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07377087e-01   # O_{11}
  1  2    -7.06836372e-01   # O_{12}
  2  1     7.06836372e-01   # O_{21}
  2  2     7.07377087e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.43902393e-01   # O_{11}
  1  2     7.65107645e-01   # O_{12}
  2  1    -7.65107645e-01   # O_{21}
  2  2     6.43902393e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.63548480e-01   # 
  1  2    -2.29849500e-01   # 
  1  3     1.29247990e-01   # 
  1  4    -4.51512200e-02   # 
  2  1    -2.49722750e-01   # 
  2  2    -9.56298170e-01   # 
  2  3     1.42201600e-01   # 
  2  4    -5.39543400e-02   # 
  3  1     4.27144000e-02   # 
  3  2    -7.52273600e-02   # 
  3  3    -6.99895200e-01   # 
  3  4    -7.08988010e-01   # 
  4  1     8.59560500e-02   # 
  4  2    -1.64328720e-01   # 
  4  3    -6.87911030e-01   # 
  4  4     7.01702770e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -9.71174660e-01   # Umix_{11}
  1  2     2.38369000e-01   # Umix_{12}
  2  1    -2.38369000e-01   # Umix_{21}
  2  2    -9.71174660e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -9.95860220e-01   # Vmix_{11}
  1  2     9.08980900e-02   # Vmix_{12}
  2  1    -9.08980900e-02   # Vmix_{21}
  2  2    -9.95860220e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     5.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.00000000e+02   # M1
     2     1.20000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     9.62000000e+01   # m_eR
    35     9.62000000e+01   # m_muR
    36     9.66000000e+01   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     3.00000000e+03   # A_e
  2  2     3.00000000e+03   # A_mu
  3  3     3.00000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.81013025e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.46287480e-02    2      1000022         1                       
      2.74048890e-01    2      1000023         1                       
      2.15901920e-03    2      1000025         1                       
      1.01275200e-02    2      1000035         1                       
      6.23547910e-01    2     -1000024         2                       
      3.54879680e-02    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.81631588e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.38642300e-04    2      1000022         2                       
      3.31245300e-01    2      1000023         2                       
      1.41912900e-03    2      1000025         2                       
      6.88093480e-03    2      1000035         2                       
      6.54365780e-01    2      1000024         1                       
      5.15018680e-03    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.81013025e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.46287600e-02    2      1000022         3                       
      2.74048950e-01    2      1000023         3                       
      2.15901970e-03    2      1000025         3                       
      1.01275220e-02    2      1000035         3                       
      6.23547790e-01    2     -1000024         4                       
      3.54879610e-02    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.81631588e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.38642070e-04    2      1000022         4                       
      3.31245300e-01    2      1000023         4                       
      1.41912860e-03    2      1000025         4                       
      6.88093300e-03    2      1000035         4                       
      6.54365900e-01    2      1000024         3                       
      5.15018780e-03    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.46111374e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.86770030e-02    2      1000022         5                       
      1.35348350e-01    2      1000023         5                       
      3.00527180e-03    2      1000025         5                       
      1.43685440e-03    2      1000035         5                       
      3.09391860e-01    2     -1000024         6                       
      4.08076170e-01    2     -1000037         6                       
      8.40645280e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.93667204e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.37417390e-02    2      1000022         6                       
      7.68003020e-02    2      1000023         6                       
      2.09032460e-01    2      1000025         6                       
      2.75628060e-01    2      1000035         6                       
      1.35112810e-01    2      1000024         5                       
      2.69684610e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.69094412e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.28945420e-01    2      1000022         1                       
      6.23479190e-02    2      1000023         1                       
      1.72731110e-03    2      1000025         1                       
      6.97938820e-03    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.76311625e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.28945480e-01    2      1000022         2                       
      6.23479190e-02    2      1000023         2                       
      1.72729150e-03    2      1000025         2                       
      6.97930530e-03    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.69094412e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.28945420e-01    2      1000022         3                       
      6.23479190e-02    2      1000023         3                       
      1.72731110e-03    2      1000025         3                       
      6.97938820e-03    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.76311625e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.28945480e-01    2      1000022         4                       
      6.23479190e-02    2      1000023         4                       
      1.72729130e-03    2      1000025         4                       
      6.97930480e-03    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.80999708e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.40434950e-02    2      1000022         5                       
      1.22282510e-01    2      1000023         5                       
      9.12363830e-03    2      1000025         5                       
      1.64212470e-02    2      1000035         5                       
      2.74315420e-01    2     -1000024         6                       
      4.52965680e-01    2     -1000037         6                       
      9.08480210e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.02595277e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.58062090e-01    2      1000024         5                       
      1.52792140e-01    2      1000037         5                       
      1.69275810e-01    2           23   1000006                       
      2.74502520e-02    2           24   1000005                       
      3.63370220e-02    2           24   2000005                       
      4.05075810e-02    2      1000022         6                       
      7.74673070e-02    2      1000023         6                       
      1.96160260e-01    2      1000025         6                       
      1.41947640e-01    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.14953978e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.72868010e-02    2      1000022        11                       
      3.62921740e-01    2      1000023        11                       
      7.69637470e-04    2      1000025        11                       
      3.93100360e-03    2      1000035        11                       
      5.72507920e-01    2     -1000024        12                       
      3.25828750e-02    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.15268139e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.74973680e-01    2      1000022        12                       
      2.03361570e-01    2      1000023        12                       
      2.79658050e-03    2      1000025        12                       
      1.28208020e-02    2      1000035        12                       
      6.01314660e-01    2      1000024        11                       
      4.73261110e-03    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.14953978e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.72868010e-02    2      1000022        13                       
      3.62921740e-01    2      1000023        13                       
      7.69637470e-04    2      1000025        13                       
      3.93100360e-03    2      1000035        13                       
      5.72507920e-01    2     -1000024        14                       
      3.25828750e-02    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.15268139e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.74973680e-01    2      1000022        14                       
      2.03361570e-01    2      1000023        14                       
      2.79658050e-03    2      1000025        14                       
      1.28208020e-02    2      1000035        14                       
      6.01314660e-01    2      1000024        13                       
      4.73261110e-03    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  1.09895981e-02   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.19422673e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.73239310e-01    2      1000022        16                       
      2.01345820e-01    2      1000023        16                       
      2.76886020e-03    2      1000025        16                       
      1.26937200e-02    2      1000035        16                       
      5.95650370e-01    2      1000024        15                       
      9.35326330e-03    2      1000037        15                       
      4.94865560e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  1.13351818e-02   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  1.13342058e-02   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.19102197e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.71036530e-02    2      1000022        15                       
      3.59424020e-01    2      1000023        15                       
      3.19289630e-03    2      1000025        15                       
      6.23697460e-03    2      1000035        15                       
      5.66824440e-01    2     -1000024        16                       
      3.22594340e-02    2     -1000037        16                       
      2.48161660e-03    2           36   1000015                       
      2.47714460e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  1.97148505e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.95979420e-02    3      1000024         1        -2             
      1.95979420e-02    3     -1000024         2        -1             
      1.95979420e-02    3      1000024         3        -4             
      1.95979420e-02    3     -1000024         4        -3             
      6.73392860e-02    3      1000024         5        -6             
      6.73392860e-02    3     -1000024         6        -5             
      1.05748270e-03    3      1000037         1        -2             
      1.05748270e-03    3     -1000037         2        -1             
      1.05748270e-03    3      1000037         3        -4             
      1.05748270e-03    3     -1000037         4        -3             
      1.34879110e-01    3      1000037         5        -6             
      1.34879110e-01    3     -1000037         6        -5             
      6.30066420e-05    2      1000022        21                       
      6.18695930e-03    3      1000022         1        -1             
      6.18695930e-03    3      1000022         3        -3             
      6.14974600e-03    3      1000022         5        -5             
      2.36114410e-02    3      1000022         6        -6             
      7.71651180e-05    2      1000023        21                       
      1.78638880e-02    3      1000023         1        -1             
      1.78638880e-02    3      1000023         3        -3             
      1.77358800e-02    3      1000023         5        -5             
      5.26670550e-02    3      1000023         6        -6             
      1.12721090e-03    2      1000025        21                       
      1.19861990e-04    3      1000025         1        -1             
      1.19861990e-04    3      1000025         3        -3             
      1.04653070e-03    3      1000025         5        -5             
      1.56872540e-01    3      1000025         6        -6             
      9.11114500e-04    2      1000035        21                       
      6.86935440e-04    3      1000035         1        -1             
      6.86935440e-04    3      1000035         3        -3             
      1.58556460e-03    3      1000035         5        -5             
      2.01378960e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  2.11090087e-03   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.69359460e-01    2      2000011       -11                       
      1.69359460e-01    2     -2000011        11                       
      1.69345110e-01    2      2000013       -13                       
      1.69345110e-01    2     -2000013        13                       
      1.61295400e-01    2      1000015       -15                       
      1.61295400e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  3.77148751e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.06339530e-01    2      1000024       -24                       
      3.06339530e-01    2     -1000024        24                       
      1.44077550e-01    2      1000022        23                       
      1.73181760e-01    2      1000023        23                       
      3.04109250e-02    2      1000022        25                       
      3.18064210e-02    2      1000023        25                       
      5.67995010e-04    2      2000011       -11                       
      5.67995010e-04    2     -2000011        11                       
      5.67994950e-04    2      2000013       -13                       
      5.67994950e-04    2     -2000013        13                       
      2.78611810e-03    2      1000015       -15                       
      2.78611810e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  3.77928342e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.14311710e-01    2      1000024       -24                       
      3.14311710e-01    2     -1000024        24                       
      3.59883940e-02    2      1000022        23                       
      3.81741600e-02    2      1000023        23                       
      1.26469360e-01    2      1000022        25                       
      1.52394530e-01    2      1000023        25                       
      2.34608520e-03    2      2000011       -11                       
      2.34608520e-03    2     -2000011        11                       
      2.34608470e-03    2      2000013       -13                       
      2.34608470e-03    2     -2000013        13                       
      4.48292870e-03    2      1000015       -15                       
      4.48292870e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  3.91273333e-06   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.89950040e-02    3      1000022         2        -1             
      1.89950040e-02    3      1000022         4        -3             
      6.33075880e-03    3      1000022       -11        12             
      6.33075880e-03    3      1000022       -13        14             
      6.33075970e-03    3      1000022       -15        16             
      9.43017720e-01    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  3.71193323e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.67429370e-02    2      1000022        24                       
      3.88128460e-01    2      1000023        24                       
      4.40602840e-03    2     -1000015        16                       
      3.11760340e-01    2      1000024        23                       
      2.68962260e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56196285e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96553710e-04    2           13       -13                       
      5.62218240e-02    2           15       -15                       
      2.35940000e-03    2            3        -3                       
      7.69088570e-01    2            5        -5                       
      3.34392340e-02    2            4        -4                       
      1.85505110e-03    2           22        22                       
      3.96490470e-02    2           21        21                       
      4.93902110e-03    3           24        11       -12             
      4.93902110e-03    3           24        13       -14             
      4.93902110e-03    3           24        15       -16             
      1.48170630e-02    3           24        -2         1             
      1.48170630e-02    3           24        -4         3             
      4.93902110e-03    3          -24       -11        12             
      4.93902110e-03    3          -24       -13        14             
      4.93902110e-03    3          -24       -15        16             
      1.48170630e-02    3          -24         2        -1             
      1.48170630e-02    3          -24         4        -3             
      5.66809550e-04    3           23        12       -12             
      5.66809550e-04    3           23        14       -14             
      5.66809550e-04    3           23        16       -16             
      2.85269580e-04    3           23        11       -11             
      2.85269580e-04    3           23        13       -13             
      2.85269580e-04    3           23        15       -15             
      9.77309300e-04    3           23         2        -2             
      9.77309300e-04    3           23         4        -4             
      1.25901530e-03    3           23         1        -1             
      1.25901530e-03    3           23         3        -3             
      1.25901530e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  9.94064610e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.61368640e-05    2           13       -13                       
      7.48585910e-03    2           15       -15                       
      3.06399890e-04    2            3        -3                       
      7.55544530e-02    2            5        -5                       
      1.25749930e-01    2            6        -6                       
      6.62222080e-05    2           21        21                       
      4.26190470e-04    2           24       -24                       
      2.16385800e-04    2           23        23                       
      6.67358750e-03    2      1000022   1000022                       
      1.55879450e-02    2      1000022   1000023                       
      9.49514280e-02    2      1000022   1000025                       
      3.12108380e-02    2      1000022   1000035                       
      9.09552160e-03    2      1000023   1000023                       
      1.11209060e-01    2      1000023   1000025                       
      3.47852740e-02    2      1000023   1000035                       
      3.79409720e-02    2      1000024  -1000024                       
      2.23438860e-01    2      1000024  -1000037                       
      2.23438860e-01    2     -1000024   1000037                       
      1.68275330e-03    2           25        25                       
      5.21422370e-05    2      2000011  -2000011                       
      5.21314800e-05    2      2000013  -2000013                       
      4.91041100e-05    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  9.78706953e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.63919960e-05    2           13       -13                       
      7.55902430e-03    2           15       -15                       
      3.09408930e-04    2            3        -3                       
      7.63635780e-02    2            5        -5                       
      1.35014060e-01    2            6        -6                       
      8.03493170e-05    2           21        21                       
      8.85808280e-03    2      1000022   1000022                       
      2.10420900e-02    2      1000022   1000023                       
      3.57233730e-02    2      1000022   1000025                       
      8.43196510e-02    2      1000022   1000035                       
      1.24987720e-02    2      1000023   1000023                       
      3.98423260e-02    2      1000023   1000025                       
      9.85939280e-02    2      1000023   1000035                       
      5.18227220e-02    2      1000024  -1000024                       
      2.13765040e-01    2      1000024  -1000037                       
      2.13765040e-01    2     -1000024   1000037                       
      4.16196180e-04    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  9.71197545e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.68531680e-05    2           14       -13                       
      7.69111140e-03    2           16       -15                       
      2.91851930e-04    2            4        -3                       
      1.90211360e-01    2            6        -5                       
      2.54145530e-02    2      1000024   1000022                       
      1.73979590e-03    2      1000024   1000023                       
      2.34602110e-01    2      1000024   1000025                       
      2.28527990e-01    2      1000024   1000035                       
      1.50858350e-02    2      1000037   1000022                       
      2.95979500e-01    2      1000037   1000023                       
      4.29085340e-04    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
