include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "QCD multi-jet production"
evgenConfig.keywords = ["QCD", "multi-jet" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Sherpa_CT10_jetjet_pT40"

evgenConfig.process="""
(run){
  % general settings
  MASSIVE_PS 4 5;
  EVENT_GENERATION_MODE=PartiallyUnweighted

  % scales, tags for scale variations
  SP_NLOCT 1; FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE QCD;

  % tags for process setup
  NJET:=0; LJET:=2; QCUT:=20.;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  ABS_ERROR=3e5
  ERROR=0.99

  % other setup
  MASSIVE[15] 1;
}(run)

(processes){
  Process 93 93 -> 93 93 93{NJET};
  Order_EW 0; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 6 {6,7,8};
  End process;
}(processes)

(selector){
  # NJetFinder <n> <ptmin> <etmin> <D parameter> [<exponent>] [<eta max>] [<mass max>]
  NJetFinder    1   40.0    0.0     0.4            -1
  NJetFinder    2   10.0    0.0     0.4            -1
}(selector)
"""
