evgenConfig.description = "Dijet truth jet slice JZ2W, with Pythia 6 AUET2B and EvtGen Afterburner"
evgenConfig.keywords = ["QCD", "jets", "EvtGen"]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

topAlg.Pythia.PythiaCommand += [
                                  "pysubs msel 0",
                                  "pysubs ckin 3 60.",
                                  "pysubs msub 11 1",
                                  "pysubs msub 12 1",
                                  "pysubs msub 13 1",
                                  "pysubs msub 68 1",
                                  "pysubs msub 28 1",
                                  "pysubs msub 53 1"]


include("MC12JobOptions/JetFilter_JZ2W.py")
evgenConfig.minevents = 500
include("MC12JobOptions/Pythia_EvtGenAfterburner.py")

include("MC12JobOptions/LowPtMuonFilter.py")
include("PutAlgsInSequence.py")

