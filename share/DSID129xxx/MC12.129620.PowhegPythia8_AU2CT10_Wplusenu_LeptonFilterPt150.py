## POWHEG+Pythia8 Wplus->enu, 150<ptlepton<200 GeV

evgenConfig.description = "POWHEG+Pythia8 Wplus->enu production with high 150<ptlepton<200 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "e", "150<ptlepton<200 GeV"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplusenu"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 150000.
topAlg.LeptonFilter.Etacut = 10.0
topAlg.LeptonFilter.PtcutMax = 200000.
evgenConfig.minevents = 500
