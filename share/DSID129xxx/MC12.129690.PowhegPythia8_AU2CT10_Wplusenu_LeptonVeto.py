## POWHEG+Pythia8 Wplus->enu central lepton veto

evgenConfig.description = "POWHEG+Pythia8 Wplus->enu production with central lepton veto pt<15 GeV and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "e", "ptlepton<15 GeV", "|etalepton|>2.7"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplusenu"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

# lepton veto
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
StreamEVGEN.VetoAlgs += ["LeptonFilter"]
topAlg.LeptonFilter.Ptcut = 15000.
topAlg.LeptonFilter.Etacut = 2.7

evgenConfig.minevents = 2000
