evgenConfig.description = "Inelastic minimum bias events using EPOS"
evgenConfig.keywords = ["QCD", "minbias"]

include("MC12JobOptions/Epos_Base_Fragment.py")
