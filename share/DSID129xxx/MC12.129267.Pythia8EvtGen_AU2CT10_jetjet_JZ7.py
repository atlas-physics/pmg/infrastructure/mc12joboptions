evgenConfig.description = "Dijet truth jet slice JZ7, with the AU2 CT10 tune and EvtGen Afterburner"
evgenConfig.keywords = ["QCD", "jets", "EvtGen"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 1400."]

include("MC12JobOptions/JetFilter_JZ7.py")
evgenConfig.minevents = 5000
include("MC12JobOptions/Pythia8_EvtGenAfterburner.py")
include("PutAlgsInSequence.py")

