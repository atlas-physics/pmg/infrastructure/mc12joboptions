evgenConfig.description = "Dijet truth jet slice JZ5W, with the AU2 CT10 tune and EvtGen Afterburner"
evgenConfig.keywords = ["QCD", "jets", "EvtGen"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 600."]

include("MC12JobOptions/JetFilter_JZ5W.py")
evgenConfig.minevents = 100

include("MC12JobOptions/Pythia8_EvtGenAfterburner.py")
include("PutAlgsInSequence.py")


