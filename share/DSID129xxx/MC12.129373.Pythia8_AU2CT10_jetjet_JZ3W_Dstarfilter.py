evgenConfig.description = "Dijet truth jet slice JZ3W, with the AU2 CT10 tune and Dstar filter"
evgenConfig.keywords = ["QCD", "jets", "DstarFilter"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 100."]

include("MC12JobOptions/JetFilter_JZ3W.py")
evgenConfig.minevents = 20

include("MC12JobOptions/DstarFilter.py")
include("PutAlgsInSequence.py")


