include ( "MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py" )
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )
  
evgenConfig.description = 'Wtaunu + 2 photon + 2 jets'
evgenConfig.keywords =["triboson","W","gamma","WtaunugammagammaNp2"]
evgenConfig.contact=['nhduongvn1@gmail.com']
evgenConfig.inputfilecheck = 'WtaunugammagammaNp2'
evgenConfig.minevents = 5000
  

