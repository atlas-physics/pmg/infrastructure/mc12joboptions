## POWHEG+Pythia8 Z->mumu exactly one central lepton filter

evgenConfig.description = "POWHEG+Pythia8 Z->mumu production with exactly one central lepton pt>15 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "mu", "exactly one |etalepton|<2.7 ptlepton>15 GeV"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Zmumu"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 15000.
topAlg.LeptonFilter.Etacut = 2.7

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter("DiLeptonVeto")
StreamEVGEN.VetoAlgs += ["DiLeptonVeto"]
topAlg.DiLeptonVeto.Ptcut = 15000.
topAlg.DiLeptonVeto.Etacut = 2.7
topAlg.DiLeptonVeto.NLeptons = 2

evgenConfig.minevents = 2000
