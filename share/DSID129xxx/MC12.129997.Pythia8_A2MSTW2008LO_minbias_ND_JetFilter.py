#based on 119997 (changed cuts for JetFilter)
evgenConfig.description = "Non-diffractive inelastic events, with the A2 MSTW2008LO tune"
evgenConfig.keywords = ["QCD", "minbias"]

include("MC12JobOptions/Pythia8_A2_MSTW2008LO_Common.py")
#include("MC12JobOptions/PhotosPythia8_Fragment.py")

topAlg.Pythia8.Commands += ["SoftQCD:minBias = on"]
#include("JetFilter_MinbiasHigh.py")
## Min bias truth jet filter for min bias sample preparation (high slice)

from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()
#a6alg = make_StandardJetGetter('AntiKt', 0.6, 'Truth').jetAlgorithmHandle()
#evgenConfig.saveJets = True

from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
topAlg += QCDTruthJetFilter()
topAlg.QCDTruthJetFilter.MinPt = 3.5*GeV
topAlg.QCDTruthJetFilter.MaxPt = 8000.*GeV
topAlg.QCDTruthJetFilter.MaxEta = 3.0
topAlg.QCDTruthJetFilter.TruthJetContainer = "AntiKt4TruthJets"
topAlg.QCDTruthJetFilter.DoShape = False
StreamEVGEN.RequireAlgs = ["QCDTruthJetFilter"]


