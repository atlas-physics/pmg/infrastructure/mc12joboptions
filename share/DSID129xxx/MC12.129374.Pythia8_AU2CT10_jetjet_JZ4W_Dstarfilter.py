evgenConfig.description = "Dijet truth jet slice JZ4W, with the AU2 CT10 tune and Dstar filter"
evgenConfig.keywords = ["QCD", "jets", "DstarFilter"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 400."]

include("MC12JobOptions/JetFilter_JZ4W.py")
evgenConfig.minevents = 20

include("MC12JobOptions/DstarFilter.py")
include("PutAlgsInSequence.py")



