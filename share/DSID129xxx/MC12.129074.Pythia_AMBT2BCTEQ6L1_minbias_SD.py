evgenConfig.description = "Single-diffractive inelastic events, with PYTHIA6"
evgenConfig.keywords = ["QCD", "minbias", "diffraction"]

include("MC12JobOptions/Pythia_AMBT2B_CTEQ6L1_Common.py")

topAlg.Pythia.PythiaCommand += \
    ["pysubs msel 0",
     "pysubs msub 92 1",
     "pysubs msub 93 1"]
