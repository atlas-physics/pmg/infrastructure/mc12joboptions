# MC@NLO + Herwig++ Z/gamma* -> ee
evgenConfig.description = "MC@NLO + Herwig++ Z/gamma*->ee, M>60 GeV, without lepton filter, CT10 and EEE4 tune"
evgenConfig.contact = ["Marc Goulette", "Jan Kretzschmar"]
evgenConfig.keywords = ["electroweak", "Z", "leptons", "e"]

evgenConfig.inputfilecheck = "McAtNlo.*Zee"

include("MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_LHEF_Common.py" )
evgenConfig.generators += ["McAtNlo"]

# Include QED radiation (not yet on by default)
topAlg.Herwigpp.Commands += ["insert /Herwig/EventHandlers/LHEHandler:PostSubProcessHandlers[0] /Herwig/QEDRadiation/QEDRadiationHandler"]

# for technical reasons need to generate MC@NLO events with lower mass cut and discard later
include("MC12JobOptions/ParentChildFilter.py")
topAlg.ParentChildFilter.PDGParent = [23]  # Select Z
topAlg.ParentChildFilter.MassMinParent = 60000.  # min mass 60 GeV
topAlg.ParentChildFilter.EtaRangeParent = 1e99  # catch also Z with 0 pt
topAlg.ParentChildFilter.PDGChild = [-11, 11]   # Select e+ or e- in Z decay
topAlg.ParentChildFilter.PtMinChild = 0.  # no filtering on child
