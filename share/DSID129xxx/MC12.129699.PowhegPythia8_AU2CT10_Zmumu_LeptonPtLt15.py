## POWHEG+Pythia8 Z->mumu central lepton pt<15 GeV

evgenConfig.description = "POWHEG+Pythia8 Z->mumu production with central lepton pt>15 GeV veto and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "mu", "ptlepton<15 GeV", "|etalepton|<2.7"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Zmumu"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

# low pt central lepton
include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 0.
topAlg.LeptonFilter.PtcutMax = 15000.
topAlg.LeptonFilter.Etacut = 2.7

evgenConfig.minevents = 1000
