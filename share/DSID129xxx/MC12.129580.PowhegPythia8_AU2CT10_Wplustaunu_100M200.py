## POWHEG+Pythia8 Wplustaunu_100M200

evgenConfig.description = "POWHEG+Pythia8 Wplustaunu with mass cut 100<M<200GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplustaunu_100M200"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")
