## POWHEG+Pythia8 DYee_8M20_DiLeptonFilter

evgenConfig.description = "POWHEG+Pythia8 DYee with mass cut 8<M<20GeV with central di-lepton filter pT>10,15 GeV and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "e"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYee_8M20"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 10000.
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.NLeptons = 2

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 15000.
topAlg.LeptonFilter.Etacut = 2.7

evgenConfig.minevents = 1000
