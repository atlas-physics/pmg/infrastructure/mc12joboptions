## Pythia8 photon induced di-lepton, gammagamma -> mumu

evgenConfig.description = "gammagamma -> mumu production with MRST2004QED, 600<M<1500GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "leptons", "muons", "600<M<1500GeV"]

include("MC12JobOptions/Pythia8_MRST2004QED_Common.py")

topAlg.Pythia8.Commands += [
    "SpaceShower:pTdampMatch = 1",
    "PhotonCollision:gmgm2mumu= on", # gg->mumu
    "PhaseSpace:mHatMin = 600.", # lower invariant mass
    "PhaseSpace:mHatMax = 1500." # upper invariant mass
]
