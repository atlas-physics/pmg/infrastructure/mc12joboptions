evgenConfig.description = "Dijet truth jet slice JZ2W, with the AU2 CT10 tune and EvtGen Afterburner and Dstar filter"
evgenConfig.keywords = ["QCD", "jets", "EvtGen", "Dstar"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 40."]

include("MC12JobOptions/JetFilter_JZ2.py")
evgenConfig.minevents = 100


## Extra chunk of config for JZxW slices, to be included in each JetFilter_JZxW.py file
## *after* the appropriate *_JZx.py fragment has been included.

## Use pT shaping in the truth jet filter
assert hasattr(topAlg, "QCDTruthJetFilter")
topAlg.QCDTruthJetFilter.DoShape = True


include("MC12JobOptions/Pythia8_EvtGenAfterburner.py")
#
# Note:  This forced D*+ but NOT D*- because we didnt' want two forced
# decays in the same event
topAlg.EvtInclusiveDecay.userDecayFile = "DstarP2D0PiP_D02Kpi_evtgen.DEC"


include("MC12JobOptions/DstarFilter.py")

include("MC12JobOptions/PutAlgsInSequence.py")

evgenConfig.auxfiles += [ 'DstarP2D0PiP_D02Kpi_evtgen.DEC']

## temporary fix to remove unwanted output## the pt balance error will find any unbalanced events
include("EvgenJobTransforms/Generate_TestHepMC.py")
topAlg.TestHepMC.EnergyDifferenceError=1000000000.0

## Configure CountHepMC to disable the default UseEventWeight setting
## NB. CountHepMC will be later pushed to the end of the alg sequence
from TruthExamples.TruthExamplesConf import CountHepMC
topAlg += CountHepMC()
topAlg.CountHepMC.UseEventWeight = False
