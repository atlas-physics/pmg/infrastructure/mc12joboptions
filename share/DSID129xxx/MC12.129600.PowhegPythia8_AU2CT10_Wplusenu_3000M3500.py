## POWHEG+Pythia8 Wplusenu_3000M3500

evgenConfig.description = "POWHEG+Pythia8 Wplusenu with mass cut 3000<M<3500GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "e"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplusenu_3000M3500"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
