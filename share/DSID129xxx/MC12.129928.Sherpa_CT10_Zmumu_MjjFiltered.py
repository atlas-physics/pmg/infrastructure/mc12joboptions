include("MC12JobOptions/Sherpa_CT10_Common.py")
	
evgenConfig.description = "Z/gamma* -> mm + up to 4 jets using Sherpa's built-in MENLOPS prescription."
evgenConfig.keywords = ["Z","jets"]
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch","andrew.pilkington@cern.ch" ]
evgenConfig.minevents = 200
evgenConfig.weighting = 0

#7TeV and 8TeV tarballs:
# group.phys-gener.sherpa010400.147771.Sherpa_Zmumu_7TeV.TXT.mc12_v1
# group.phys-gener.sherpa010400.147771.Sherpa_Zmumu_8TeV.TXT.mc12_v1
	
evgenConfig.process="""
(processes){
  Process 93 93 -> 13 -13 93{4}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  NLO_QCD_Part BVIRS {2};
  ME_Generator Amegic {2};
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;
}(processes)

(selector){
  Mass 13 -13 40 E_CMS
}(selector)

(run){
  NLO_Mode 3
}(run)
"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.inputconfcheck = '147771.Sherpa_Zmumu'


try:    
  from JetRec.JetGetters import *
  antikt4=make_StandardJetGetter('AntiKt',0.4,'Truth',disable=False,outputCollectionName='AntiKt4TruthJets',useInteractingOnly=True,includeMuons=False)
  antikt4alg = antikt4.jetAlgorithmHandle()
  antikt4alg.OutputLevel = INFO
except Exception, e:    
  pass


from GeneratorFilters.GeneratorFiltersConf import VBFMjjIntervalFilter
topAlg += VBFMjjIntervalFilter()
topAlg.VBFMjjIntervalFilter.TruthJetContainerName="AntiKt4TruthJets"
topAlg.VBFMjjIntervalFilter.OutputLevel = INFO

StreamEVGEN.RequireAlgs +=  [ "VBFMjjIntervalFilter" ]

