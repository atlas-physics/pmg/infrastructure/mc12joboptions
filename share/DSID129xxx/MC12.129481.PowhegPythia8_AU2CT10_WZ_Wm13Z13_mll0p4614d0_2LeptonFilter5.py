## POWHEG+Pythia8 WZ
evgenConfig.description = "POWHEG+Pythia8 WZ production and AU2 CT10 tune, cut on all SFOS pair mll>2*m(l from Z)+250MeV, dilepton filter pt>5GeV, eta<2.7"
evgenConfig.keywords = ["EW", "diboson", "leptonic"]
evgenConfig.inputfilecheck = "Powheg_CT10.*.WZ_Wm13Z13_mll0p4614d0"
evgenConfig.minevents  = 5000
evgenConfig.contact = ["Alexander Oh <alexander.oh@cern.ch>"]

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.NLeptons = 2
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.Ptcut = 5.*GeV


