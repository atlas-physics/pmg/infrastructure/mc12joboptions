include("MC12JobOptions/Sherpa_CT10_Common.py")
evgenConfig.description = "VBF-W production, includes dibosons"
evgenConfig.keywords = [ "Sherpa", "VBF", "EW","diboson"]
evgenConfig.contact  = [ "rking@cern.ch" ]
evgenConfig.minevents = 1000



evgenConfig.process="""
(processes){
!positron
Process 93 93  -> 13 -14 93 93 93{1}
Order_EW 4
Integration_Error 5.e-2
CKKW  sqr(15/E_CMS)

End process;

!electron
Process 93 93  -> -13 14 93 93 93{1}
Order_EW 4
Integration_Error 5.e-2
CKKW  sqr(15/E_CMS)

End process;

}(processes)

(selector){
Mass -13 14 10 E_CMS
Mass 13 -14 10 E_CMS
NJetFinder  2  15.0  0.0  0.4 1
}(selector)

(model){
ACTIVE[6]=0
}(model)
"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.inputconfcheck = '129919.Sherpa_CT10_Wmunu2JetsEW1JetQCD15GeV'
