evgenConfig.description = "Pythia8 gammajet sample. jetjet, gamma+jet events with at least one hard process or parton shower photon with pT > 1000 GeV"
evgenConfig.keywords = ["egamma", "performance", "jets", "gamma", "QCD"]
evgenConfig.minevents = 100

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

## Configure Pythia
topAlg.Pythia8.Commands += ["HardQCD:gg2gg = on",
                            "HardQCD:gg2qqbar = on",
                            "HardQCD:qg2qg = on",
                            "HardQCD:qq2qq = on",
                            "HardQCD:qqbar2gg = on",
                            "HardQCD:qqbar2qqbarNew = on",
                            "PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PhaseSpace:pTHatMin = 995"]

include("MC12JobOptions/DirectPhotonFilter.py")
topAlg.DirectPhotonFilter.Ptcut = 1000000.
