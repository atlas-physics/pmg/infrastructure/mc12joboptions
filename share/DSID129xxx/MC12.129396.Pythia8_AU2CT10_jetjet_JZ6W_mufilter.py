evgenConfig.description = "Dijet truth jet slice JZ6W, with the AU2 CT10 tune and muon filter"
evgenConfig.keywords = ["QCD", "jets", "mufilter"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 1100."]

include("MC12JobOptions/JetFilter_JZ6W.py")
evgenConfig.minevents = 200

include("MC12JobOptions/LowPtMuonFilter.py")
include("PutAlgsInSequence.py")

