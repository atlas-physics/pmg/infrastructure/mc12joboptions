## POWHEG+Pythia8 DYtautau_7M20_LeptonFilter

evgenConfig.description = "POWHEG+Pythia8 DYtautau with mass cut 7<M<20GeV with di-lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "tau", "ptlepton>8GeV", "etalepton<2.8"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYtautau_7M20"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 8000.
topAlg.MultiLeptonFilter.Etacut = 2.8
topAlg.MultiLeptonFilter.NLeptons = 2
evgenConfig.minevents = 20
