## POWHEG+Pythia8 DYmumu_2500M2750

evgenConfig.description = "POWHEG+Pythia8 DYmumu with mass cut 2500<M<2750GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "mu"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYmumu_2500M2750"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
