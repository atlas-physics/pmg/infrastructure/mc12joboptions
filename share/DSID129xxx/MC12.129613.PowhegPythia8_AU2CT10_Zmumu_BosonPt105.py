## POWHEG+Pythia8 Zmumu

evgenConfig.description = "POWHEG+Pythia8 Zmumu with Boson Pt>105 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "mu"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Zmumu"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
topAlg += ParentChildFilter()

ParentChildFilter = topAlg.ParentChildFilter
ParentChildFilter.PDGParent = [23]  # Select Z
ParentChildFilter.PDGChild = [-13, 13]   # Select mu+ or mu- in Z decay
ParentChildFilter.PtMinParent = 105000.  # min Pt 105 GeV
StreamEVGEN.RequireAlgs += ["ParentChildFilter"]
