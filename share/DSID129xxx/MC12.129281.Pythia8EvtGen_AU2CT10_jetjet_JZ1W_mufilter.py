evgenConfig.description = "Dijet truth jet slice JZ1W, with the AU2 CT10 tune,EvtGen Afterburner and mu filter"
evgenConfig.keywords = ["QCD", "jets", "EvtGen"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 10."]

include("MC12JobOptions/JetFilter_JZ1.py")
evgenConfig.minevents = 20

## Extra chunk of config for JZxW slices, to be included in each JetFilter_JZxW.py file
## *after* the appropriate *_JZx.py fragment has been included.

## Use pT shaping in the truth jet filter
assert hasattr(topAlg, "QCDTruthJetFilter")
topAlg.QCDTruthJetFilter.DoShape = True


include("MC12JobOptions/Pythia8_EvtGenAfterburner.py")
include("MC12JobOptions/LowPtMuonFilter.py")
include("MC12JobOptions/PutAlgsInSequence.py")

## temporary fix to remove unwanted output
## the pt balance error will find any unbalanced events
include("EvgenJobTransforms/Generate_TestHepMC.py")
topAlg.TestHepMC.EnergyDifferenceError=1000000000.0

## Configure CountHepMC to disable the default UseEventWeight setting
## NB. CountHepMC will be later pushed to the end of the alg sequence
from TruthExamples.TruthExamplesConf import CountHepMC
topAlg += CountHepMC()
topAlg.CountHepMC.UseEventWeight = False

