## POWHEG+Pythia8 DYtautau_180M250

evgenConfig.description = "POWHEG+Pythia8 DYtautau with mass cut 180<M<250GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "tau"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYtautau_180M250"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")
