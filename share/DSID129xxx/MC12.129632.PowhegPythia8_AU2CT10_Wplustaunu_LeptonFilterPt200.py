## POWHEG+Pythia8 Wplus->taunu, 200<ptlepton<300 GeV

evgenConfig.description = "POWHEG+Pythia8 Wplus->taunu production with high 200<ptlepton<300 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau", "200<ptlepton<300 GeV"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplustaunu"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 200000.
topAlg.LeptonFilter.Etacut = 10.0
topAlg.LeptonFilter.PtcutMax = 300000.
evgenConfig.minevents = 1000
