evgenConfig.description = "Inelastic minimum bias events, with PYTHIA6"
evgenConfig.keywords = ["QCD", "minbias", "pileup"]

include("MC12JobOptions/Pythia_AMBT2B_CTEQ6L1_Common.py")

topAlg.Pythia.PythiaCommand += \
    ["pysubs msel 0",
     # ND
     "pysubs msub 11 1",
     "pysubs msub 12 1",
     "pysubs msub 13 1",
     "pysubs msub 28 1",
     "pysubs msub 53 1",
     "pysubs msub 68 1",
     "pysubs msub 95 1",
     # SD
     "pysubs msub 92 1",
     "pysubs msub 93 1",
     # DD
     "pysubs msub 94 1"]
