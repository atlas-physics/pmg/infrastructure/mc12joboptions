##############################################################
# Job options fragment for pp->Upsilon(1S)->e(3)e(18)X  
# Created: 18 June 2012 by Gabriella.Pasztor@cern.ch
# Xsec: 8.5 nb, MultiElectronFilter effi: 50%, 50 min / 100 ev (17.2.3.5)
##############################################################

# Common Pythia8B config
include ("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

# General evgen config options
evgenConfig.description = "Inclusive pp->Upsilon(1S)->e(3)e(18) production"
evgenConfig.keywords = ["egamma","Upsilon(1S)","electrons"]
evgenConfig.minevents = 1000

# General bb -> Bottomonium fragment
include ("MC12JobOptions/Pythia8B_Bottomonium_Common.py")

# Photos++
include("MC12JobOptions/Pythia8B_Photos.py")

# Close all Upsilon(1S) decays apart from Upsilon(1S) --> ee
topAlg.Pythia8B.Commands += ['553:onMode = off']
topAlg.Pythia8B.Commands += ['553:2:onMode = on']

# Signal topology - only events containing this sequence will be accepted 
topAlg.Pythia8B.SignalPDGCodes = [553,-11,11]

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 11
topAlg.Pythia8B.TriggerStatePtCut = 18.0
topAlg.Pythia8B.TriggerStateEtaCut = 2.7
topAlg.Pythia8B.NTriggerStates = 1

# Filter
include("MC12JobOptions/MultiElectronFilter.py")
topAlg.MultiElectronFilter.Ptcut = 3000.
topAlg.MultiElectronFilter.Etacut =  2.7
topAlg.MultiElectronFilter.NElectrons =  2
