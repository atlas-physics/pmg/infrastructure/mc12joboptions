## Pythia8 W->taunu with alternative tune
## author: Martin Flechl, Apr 3, 2012

evgenConfig.description = "W->tau nu production with the A2 CTEQ6L1 tune"
evgenConfig.keywords = ["EW", "W", "tau"]

include("MC12JobOptions/Pythia8_A2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on", # create W bosons
                            "PhaseSpace:mHatMin = 60.", # lower invariant mass
                            "24:onMode = off", # switch off all W decays
                            "24:onIfAny = 15"] # switch on W->tau,nu decays
