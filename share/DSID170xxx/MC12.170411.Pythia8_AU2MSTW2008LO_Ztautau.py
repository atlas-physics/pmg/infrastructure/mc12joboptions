## Pythia8 Z->tautau  with alternative PDF set
## author: Martin Flechl, Apr 3, 2012

evgenConfig.description = "Z->tautau production with the AU2 MSTW2008LO tune"
evgenConfig.keywords = ["EW", "Z", "tau"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # create Z bosons
                            "PhaseSpace:mHatMin = 60.", # lower invariant mass
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 15"] # switch on Z->tautau decays
