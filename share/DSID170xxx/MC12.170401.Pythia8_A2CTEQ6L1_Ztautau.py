## Pythia8 Z->tautau with alternative tune
## author: Martin Flechl, Apr 3, 2012

evgenConfig.description = "Z->tautau production with the A2 CTEQ6L1 tune"
evgenConfig.keywords = ["EW", "Z", "tau"]

include("MC12JobOptions/Pythia8_A2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # create Z bosons
                            "PhaseSpace:mHatMin = 60.", # lower invariant mass
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 15"] # switch on Z->tautau decays
