################################################################
#
# gg2ZZ2.0/JIMMY/HERWIG gg -> ZZ, with Z -> 4leptons 
include('MC12JobOptions/Jimmy_AUET2_CT10_Common.py')

topAlg.Herwig.HerwigCommand += ['iproc mcatnlo','modbos 1 3', 'modbos 2 3',
                          'maxpr 10']

include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')

evgenConfig.generators += [ 'McAtNlo', 'Herwig' ]
evgenConfig.description = 'gg2ZZ, Z-> 4mu events showered with Jimmy/Herwig'
evgenConfig.keywords = ['diboson', 'leptonic']
evgenConfig.contact = ['daniela.rebuzzi@cern.ch']
evgenConfig.inputfilecheck = 'gg2ZZ.116602.ZZ4mu'
