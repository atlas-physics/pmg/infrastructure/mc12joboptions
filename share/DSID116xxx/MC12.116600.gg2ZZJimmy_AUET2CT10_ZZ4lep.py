
################################################################
#
# gg2ZZ2.0/JIMMY/HERWIG gg -> ZZ, with Z -> 4leptons 
include('MC12JobOptions/Jimmy_AUET2_CT10_Common.py')

topAlg.Herwig.HerwigCommand += ['iproc mcatnlo','modbos 1 5', 'modbos 2 5',
                          'maxpr 10']

include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')

include('MC12JobOptions/MultiLeptonFilter.py')
topAlg.MultiLeptonFilter.Ptcut = 5000.
topAlg.MultiLeptonFilter.Etacut = 10
topAlg.MultiLeptonFilter.NLeptons = 3

evgenConfig.generators += [ 'McAtNlo', 'Herwig' ]
evgenConfig.description = 'gg2ZZ, Z-> 4lep events showered with Jimmy/Herwig'
evgenConfig.keywords = ['diboson', 'leptonic']
evgenConfig.contact = ['daniela.rebuzzi@cern.ch']
evgenConfig.inputfilecheck = 'gg2ZZ.116600.ZZ4lep'
