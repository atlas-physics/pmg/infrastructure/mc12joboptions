include('MC12JobOptions/Jimmy_AUET2_CT10_Common.py')

topAlg.Herwig.HerwigCommand += ['iproc mcatnlo']

include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')

evgenConfig.generators += [ 'McAtNlo', 'Herwig' ]
evgenConfig.description = 'MC@NLO h->mumu using CT10 PDF and fHerwig with AUET2_CT10 configuration'
evgenConfig.keywords = ['nonSMHiggs', 'mu']
evgenConfig.contact = ['yi.yang@cern.ch']
evgenConfig.inputfilecheck = 'mcatnlo0402.116115.a08p5_mumu'

include ( 'MC12JobOptions/MultiMuonFilter.py' )
topAlg.MultiMuonFilter.Ptcut = 2500.
topAlg.MultiMuonFilter.Etacut = 3.0
topAlg.MultiMuonFilter.NMuons = 2
