################################################################
#
# gg2ZZ2.0/JIMMY/HERWIG gg -> ZZ, with Z -> 4e
include('MC12JobOptions/Jimmy_AUET2_CT10_Common.py')

topAlg.Herwig.HerwigCommand += ['iproc mcatnlo','modbos 1 2', 'modbos 2 2',
                          'maxpr 10']

include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')

evgenConfig.generators += [ 'McAtNlo', 'Herwig' ]
evgenConfig.description = 'gg2ZZ, Z-> 4e events showered with Jimmy/Herwig'
evgenConfig.keywords = ['diboson', 'leptonic']
evgenConfig.contact = ['daniela.rebuzzi@cern.ch']
evgenConfig.inputfilecheck = 'gg2ZZ.116601.ZZ4e'
