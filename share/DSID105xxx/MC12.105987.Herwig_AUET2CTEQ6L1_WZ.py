evgenConfig.description = "Herwig WZ all filtered for one lepton"
evgenConfig.generators = ["Herwig"]
evgenConfig.keywords = ["diboson", "semileptonic"]
evgenConfig.contact  = ["thorsten.kuhl@cern.ch"]
#
include ( "MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc 12820"]    # note that 10000 is added to the herwig process number
# ... Tauola
include ( "MC12JobOptions/Jimmy_Tauola.py" )
# ... Photos
include ( "MC12JobOptions/Jimmy_Photos.py" )
# Add the filters:
# Electron or Muon filter
include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 10000.
topAlg.LeptonFilter.Etacut = 2.8
