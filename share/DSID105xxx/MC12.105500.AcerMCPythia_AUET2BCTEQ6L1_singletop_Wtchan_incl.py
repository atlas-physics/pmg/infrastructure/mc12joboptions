evgenConfig.description = "AcerMCPythia6 singletop-Wtchan-incl production with AUET2B CTEQ6L1 tune"
evgenConfig.generators = ["AcerMC", "Pythia"]
evgenConfig.keywords = ["top", "singletop", "Wtchan", "incl"]
evgenConfig.contact  = ["liza.mijovic@cern.ch"]
evgenConfig.inputfilecheck = "singletop"

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
topAlg.Pythia.PythiaCommand += ["pyinit user acermc"]
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
