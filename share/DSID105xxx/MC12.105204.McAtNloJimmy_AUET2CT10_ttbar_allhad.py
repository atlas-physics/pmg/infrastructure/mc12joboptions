evgenConfig.description = "McAtNlo+fHerwig/Jimmy ttbar all-hadronic production with TTbarWToLeptonFilter veto on leptons - CT10 PDF, AUET2 tune"
evgenConfig.generators = ["McAtNlo", "Herwig"]
evgenConfig.keywords = ["top", "ttbar", "hadronic"]
evgenConfig.contact  = ["christoph.wasicki@cern.ch"]
evgenConfig.inputfilecheck = "ttbar"

include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand+=[ "iproc mcatnlo" ]

include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons=0
