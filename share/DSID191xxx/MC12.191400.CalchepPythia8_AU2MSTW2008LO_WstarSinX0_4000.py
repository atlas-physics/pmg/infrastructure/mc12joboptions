###############################################################
#
# Job options file CalcHEP+Pythia8
#
#-----------------------------------------------------------------------------
evgenConfig.description = "Calchep production of W*->QQ of mass 4000GeV produced with AU2 MSTW2008LO tune"
evgenConfig.keywords = ["electroweak","Wstar","quarks"]
evgenConfig.inputfilecheck = "Wstar"
evgenConfig.contact=["Harinder.Singh.Bawa@cern.ch"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_CalcHep.py")
###############################################################
