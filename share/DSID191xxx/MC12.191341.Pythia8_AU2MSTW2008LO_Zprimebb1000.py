evgenConfig.description = "Pythia8 Z'(1000 GeV)->bb with A2 MSTW2008LO tune"
evgenConfig.keywords = ["EW", "Zprime", "bb"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact = ["Ning.Zhou@cern.ch"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on", #create Z' bosons
"PhaseSpace:mHatMin = 200.", #lower invariant mass
"Zprime:gmZmode = 3",
"32:m0 = 1000", #set Z' mass [GeV]
"32:onMode = off", #switch off all Z' decays
"32:onIfAny = 5"] #switch on Z'->bb decay
