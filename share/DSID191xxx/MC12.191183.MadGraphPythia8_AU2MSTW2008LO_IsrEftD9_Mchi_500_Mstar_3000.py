include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description = "IsrEftD9_Mchi_500_Mstar_3000"
evgenConfig.keywords = ["Z", "lepton"]
evgenConfig.inputfilecheck = 'IsrEftD9_Mchi_500_Mstar_3000'
evgenConfig.contact = ["Shawn McKee <smckee@umich.edu>"]
