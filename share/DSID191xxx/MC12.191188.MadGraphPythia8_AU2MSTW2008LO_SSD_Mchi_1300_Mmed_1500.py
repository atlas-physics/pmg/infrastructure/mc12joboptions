include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description = "SSD_Mchi_1300_Mmed_1500"
evgenConfig.keywords = ["Z", "lepton"]
evgenConfig.inputfilecheck = 'SSD_Mchi_1300_Mmed_1500'
evgenConfig.contact = ["Shawn McKee <smckee@umich.edu>"]
