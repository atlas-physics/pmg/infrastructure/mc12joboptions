#--------------------------------------------------------------
# Author 1: S. Ask (Cambrdige U.), 21 May 2012
# Author 2: D. Hayden (RHUL.), 3rd August 2012
# Modifications for Wprime->e,mu flat sample at sqrt(s) = 8 TeV by Nikos Tsirintanis, Mihail Chizhov 18th September 2013
# Modifications for Wprime->e,mu flat sample at sqrt(s) = 13 TeV by Nikos Tsirintanis, Mihail Chizhov 16th January 2015
#--------------------------------------------------------------

evgenConfig.description = "Wprime->e,mu + nu production with AU2 MSTW2008LO tune"
evgenConfig.keywords = ["electroweak", "Wprime", "leptons"]
evgenConfig.contact = ["Nikolaos.Tsirintanis@cern.ch"] 
evgenConfig.generators = ["Pythia8"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands +=['NewGaugeBoson:ffbar2Wprime = on'] 
topAlg.Pythia8.Commands +=['PhaseSpace:mHatMin = 25.0'] 
topAlg.Pythia8.Commands +=['34:m0 =1000.0']
topAlg.Pythia8.Commands +=['34:onMode = off']
topAlg.Pythia8.Commands +=['34:onIfAny = 11,12']
topAlg.Pythia8.Commands +=['34:onIfAny = 13,14']

topAlg.Pythia8.UserHook = "WprimeFlat"
topAlg.Pythia8.UserModes += ["WprimeFlat:EnergyMode = 13"]
