# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_sq_neut_1500_550.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['squarks'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GGM photon + b + MET gluino-neutralino grid generation with msq=1500 mneut=550'
evgenConfig.keywords = ['SUSY', 'GGM', 'gluino', 'bino', 'higgsino']
evgenConfig.contact = [ 'andrew.kuhl@cern.ch']

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds
