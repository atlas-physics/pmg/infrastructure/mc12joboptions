#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.20000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05402904E+01   # W+
        25     1.26000000E+02   # h
        35     2.00399517E+03   # H
        36     2.00000000E+03   # A
        37     2.00209072E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.50045131E+03   # ~d_L
   2000001     1.50008449E+03   # ~d_R
   1000002     1.49963307E+03   # ~u_L
   2000002     1.49983100E+03   # ~u_R
   1000003     1.50045131E+03   # ~s_L
   2000003     1.50008449E+03   # ~s_R
   1000004     1.49963307E+03   # ~c_L
   2000004     1.49983100E+03   # ~c_R
   1000005     1.50002431E+03   # ~b_1
   2000005     1.50051647E+03   # ~b_2
   1000006     1.50384068E+03   # ~t_1
   2000006     1.51222928E+03   # ~t_2
   1000011     1.50028237E+03   # ~e_L
   2000011     1.50025346E+03   # ~e_R
   1000012     1.49946403E+03   # ~nu_eL
   1000013     1.50028237E+03   # ~mu_L
   2000013     1.50025346E+03   # ~mu_R
   1000014     1.49946403E+03   # ~nu_muL
   1000015     1.50016137E+03   # ~tau_1
   2000015     1.50037655E+03   # ~tau_2
   1000016     1.49946403E+03   # ~nu_tauL
   1000021     1.20000000E+03   # ~g
   1000022     1.10972605E+02   # ~chi_10
   1000023    -1.20295835E+02   # ~chi_20
   1000025     1.00000076E+03   # ~chi_30
   1000035     1.00932347E+03   # ~chi_40
   1000024     1.13299630E+02   # ~chi_1+
   1000037     1.00713864E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.87199166E-02   # N_11
  1  2    -8.75349484E-02   # N_12
  1  3     7.09027453E-01   # N_13
  1  4    -6.98028705E-01   # N_14
  2  1     8.02231797E-03   # N_21
  2  2    -1.44136746E-02   # N_22
  2  3    -7.02645154E-01   # N_23
  2  4    -7.11349194E-01   # N_24
  3  1     8.73757940E-01   # N_31
  3  2     4.86361041E-01   # N_32
  3  3     2.55510547E-06   # N_33
  3  4    -3.88264973E-06   # N_34
  4  1     4.83851630E-01   # N_41
  4  2    -8.69240755E-01   # N_42
  4  3    -5.97482924E-02   # N_43
  4  4     8.20868480E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.40454366E-02   # U_11
  1  2     9.97254869E-01   # U_12
  2  1     9.97254869E-01   # U_21
  2  2     7.40454366E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.01907066E-01   # V_11
  1  2     9.94793923E-01   # V_12
  2  1     9.94793923E-01   # V_21
  2  2     1.01907066E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.15355109E-01   # cos(theta_t)
  1  2     6.98761095E-01   # sin(theta_t)
  2  1    -6.98761095E-01   # -sin(theta_t)
  2  2     7.15355109E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     3.56838238E-01   # cos(theta_b)
  1  2     9.34166191E-01   # sin(theta_b)
  2  1    -9.34166191E-01   # -sin(theta_b)
  2  2     3.56838238E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.57899680E-01   # cos(theta_tau)
  1  2     7.53105578E-01   # sin(theta_tau)
  2  1    -7.53105578E-01   # -sin(theta_tau)
  2  2     6.57899680E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89788782E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.20000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51045808E+02   # vev(Q)              
         4     3.96437320E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.54241073E-01   # gprime(Q) DRbar
     2     6.36463306E-01   # g(Q) DRbar
     3     1.10869123E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.07029565E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.77736003E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80487409E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.70490441E+06   # M^2_Hd              
        22     7.59718353E+05   # M^2_Hu              
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     5.72077495E-02   # gluino decays
#          BR         NDA      ID1       ID2
     8.86186349E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     9.21030254E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.06180524E-12    2     1000025        21   # BR(~g -> ~chi_30 g)
     3.62830435E-06    2     1000035        21   # BR(~g -> ~chi_40 g)
#           BR         NDA      ID1       ID2       ID3
     4.63064717E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     9.57898349E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     4.25634751E-05    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.56518408E-04    3     1000035         1        -1   # BR(~g -> ~chi_40 d  db)
     3.59372882E-04    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.43499114E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.70778159E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.21653189E-04    3     1000035         2        -2   # BR(~g -> ~chi_40 u  ub)
     4.63064717E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     9.57898349E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     4.25634751E-05    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.56518408E-04    3     1000035         3        -3   # BR(~g -> ~chi_40 s  sb)
     3.59372882E-04    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.43499114E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.70778159E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.21653189E-04    3     1000035         4        -4   # BR(~g -> ~chi_40 c  cb)
     6.90676450E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.45175839E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     4.21748509E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.55542361E-04    3     1000035         5        -5   # BR(~g -> ~chi_40 b  bb)
     2.13702549E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.14219655E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     7.58416917E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     7.58416917E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.43602207E-04    3     1000037         1        -2   # BR(~g -> ~chi_2+ d  ub)
     3.43602207E-04    3    -1000037         2        -1   # BR(~g -> ~chi_2- u  db)
     7.58416917E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     7.58416917E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     3.43602207E-04    3     1000037         3        -4   # BR(~g -> ~chi_2+ s  cb)
     3.43602207E-04    3    -1000037         4        -3   # BR(~g -> ~chi_2- c  sb)
     2.72898632E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.72898632E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
     5.81477291E-07    3     1000037         5        -6   # BR(~g -> ~chi_2+ b  tb)
     5.81477291E-07    3    -1000037         6        -5   # BR(~g -> ~chi_2- t  bb)
#
#         PDG            Width
DECAY   1000024     7.05509295E-11   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.73536250E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.73536250E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.24512403E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.24512403E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     3.90269380E-03    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000023     8.77415493E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.38291316E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     8.28102970E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.07028294E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     8.28102970E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.07028294E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     2.43609666E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.43609666E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.93676721E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     4.85814247E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     4.85814247E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     4.85814247E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.85436220E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.85436220E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.85436220E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.85436220E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.28479438E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.28479438E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.28479438E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.28479438E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.67698813E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.67698813E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
