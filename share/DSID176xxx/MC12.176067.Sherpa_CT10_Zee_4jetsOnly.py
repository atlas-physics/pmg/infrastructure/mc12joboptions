include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z/gamma* -> ee + exactly 4 jets using Sherpa's built-in MENLOPS prescription."
evgenConfig.keywords = [ "Z", "ee", "4jetsOnly" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "christopher.young@cern.ch" ]
evgenConfig.minevents = 500

evgenConfig.process="""
(processes){
  Process 93 93 -> 11 -11 93 93 93 93
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  End process;
}(processes)

(selector){
  Mass 11 -11 40 E_CMS
}(selector)
"""

topAlg.Sherpa_i.ResetWeight = 0
