evgenConfig.description = "POWHEG+PYTHIA8 tHch Hch->tb with AU2,CT10"
evgenConfig.keywords = ["nonSMhiggs", "Hch","top","bottom","semileptonic"]
evgenConfig.inputfilecheck = "tHpl_2HDM_M350"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [ 'Higgs:useBSM = on',
                             '37:onMode = off',
                             '37:onIfMatch = 5 6',
                             'TimeShower:pTmaxMatch=1',
                             'TimeShower:alphaSvalue=0.118'
                             ]

include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.Ptcut = 0
topAlg.TTbarWToLeptonFilter.NumLeptons = 1
