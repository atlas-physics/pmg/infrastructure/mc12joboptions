evgenConfig.description = "POWHEG+fHerwig/Jimmy ttbar production with a Dilepton filter and AUET2 CT10 tune"
evgenConfig.keywords = ["top", "ttbar", "dilepton"]
evgenConfig.contact  = ["jeffrey.wetter@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar' 
evgenConfig.minevents=1000

include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")


evgenConfig.generators += [ "Powheg"]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")

topAlg.TTbarWToLeptonFilter.NumLeptons = 2
topAlg.TTbarWToLeptonFilter.Ptcut = 0.0 



