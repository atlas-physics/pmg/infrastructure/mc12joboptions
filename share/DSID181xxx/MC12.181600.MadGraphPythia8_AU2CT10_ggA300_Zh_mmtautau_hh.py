include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

## For debugging only: print out some Pythia config info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on", "Next:numberShowLHA = 10", "Next:numberShowEvent = 10"]

evgenConfig.description = "non SM higgs A(300)->Zh(125)->mumutautau(->hadhad)"
evgenConfig.keywords = ["nonSMhiggs","ZH","mu","tau"]
evgenConfig.inputfilecheck = 'madgraph.181600.AU2CT10_ggA300_Zh_mmtautau_hh'

#topAlg.Pythia8.Commands += [
#    '25:onMode = off',#decay of Higgs
#    '25:onIfMatch = 15 15'
#    ]

# ... Filter H->VV->Children
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 15
topAlg.XtoVVDecayFilter.StatusParent = 2
topAlg.XtoVVDecayFilter.PDGChild1 = [111,130,211,221,223,310,311,321,323]
topAlg.XtoVVDecayFilter.PDGChild2 = [111,130,211,221,223,310,311,321,323]
