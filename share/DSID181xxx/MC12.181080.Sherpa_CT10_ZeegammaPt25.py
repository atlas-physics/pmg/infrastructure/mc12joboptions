include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z to e e gamma production with up to three jets in ME+PS and pT_gamma>25 GeV, |eta_gamma|<3 and |eta_lepton|<3."
evgenConfig.keywords = [ "EW", "electron", "gamma" ]
evgenConfig.inputconfcheck = "eegammaPt25"
evgenConfig.contact  = [ "benjamin.kaplan@cern.ch" ]

evgenConfig.process="""
(run){
  ACTIVE[25]=0
}(run)

(processes){
  Process 93 93 ->  11 -11 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;
}(processes)

(selector){
  Mass 90 90 40 7000
  PT 22  25 7000
  PseudoRapidity 11 -3 3
  PseudoRapidity -11 -3 3
  PseudoRapidity 22 -3 3
  DeltaR 22 90 0.1 1000
  DeltaR 22 93 0.1 1000
}(selector)
"""
