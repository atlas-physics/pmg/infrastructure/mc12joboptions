###############################################################
#                                                              
# Job options file for Alpgen with Pythia                      
# Antonio Salvucci <antonio.salvucci@cern.ch> / June 2013      
#                                                              
#==============================================================
include('MC12JobOptions/AlpgenControl_Zbb_Pythia.py') 
evgenConfig.minevents = 20 
