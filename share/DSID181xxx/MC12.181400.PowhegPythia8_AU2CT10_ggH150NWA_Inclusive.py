evgenConfig.description = "POWHEG+PYTHIA8 ggH H->inclusive with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "inclusive"]
evgenConfig.inputfilecheck = "ggH_SM_M150"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )
include("MC12JobOptions/Pythia8_H125_HXSWG_InclusiveBRs.py")

