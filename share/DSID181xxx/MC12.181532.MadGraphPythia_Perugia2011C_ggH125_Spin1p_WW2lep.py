evgenConfig.description = "MG5+Pythia gg H->WW->lvlv, spin1p configuration"
evgenConfig.keywords = ["Higgs","WW", "diboson", "leptonic"]
evgenConfig.inputfilecheck = 'Spin1p'

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")
evgenConfig.generators += [ "MadGraph" ]
## ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )
## ... Photos
include ( "MC12JobOptions/Pythia_Photos.py" )

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                                 ]

evgenConfig.minevents=2000

### THE FOLLOWING LINES ARE ONLY NECESSARY IF YOU WANT THE MATCHING ME/PS
## the parameters need to be tuned
## see documentation of MadGraph matching in: http://cp3wks05.fynu.ucl.ac.be/twik...
phojf=open('./pythia_card.dat', 'w')
phojinp = """
! exclusive or inclusive matching
IEXCFILE=0
showerkt=T
qcut=40
"""
phojf.write(phojinp)
phojf.close()
