evgenConfig.description = "H0 350.0 GeV, with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["H0","hh","yybb"]
evgenConfig.contact  = [ "james.saxon@cern.ch"]
evgenConfig.minevents = 5000

#Tune
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

#Pythia8 Commands
topAlg.Pythia8.Commands += ["Higgs:useBSM on", "HiggsBSM:allH2 on",
                            "25:m0 125.0", #H0 mass
                            "35:m0 350.0", #H0 mass
                            "35:onMode = off", #turn off all H0 decays
                            "35:onIfMatch = 25 25", #H0->hh
                            "SLHA:file = narrowWidth.slha",
                            "25:onMode = off", #turn off all h decays
                            "25:oneChannel = on 0.5 100 5 -5 ", # turn on b bbar
                            "25:addChannel = on 0.5 100 22 22 "] # turn on gam gam

# Generator Filters
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 35
topAlg.XtoVVDecayFilter.PDGParent = 25
topAlg.XtoVVDecayFilter.StatusParent = 22
topAlg.XtoVVDecayFilter.PDGChild1 = [5]
topAlg.XtoVVDecayFilter.PDGChild2 = [22]

StreamEVGEN.RequireAlgs = ["XtoVVDecayFilter"]

# set width for heavy Higgs
nwf=open('./narrowWidth.slha', 'w')
nwfinput = """#           PDG      WIDTH
DECAY   35  0.01
#          BR         NDA          ID1       ID2       ID3       ID4
1       2       25      25
"""
nwf.write(nwfinput)
nwf.close()
