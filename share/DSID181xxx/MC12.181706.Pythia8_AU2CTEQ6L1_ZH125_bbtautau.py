evgenConfig.description = "PYTHIA8 ZH Z->bb H->tautau with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "ZH", "tau"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:m0 = 125',
                            '25:mWidth = 0.00407',
                            '25:onMode = off',
                            '25:doForceWidth = true',
                            '25:onIfMatch = 15 15',
                            'HiggsSM:ffbar2HZ = on',
                            '23:onMode = off',
                            '23:onIfAny = 5'
                            ]

