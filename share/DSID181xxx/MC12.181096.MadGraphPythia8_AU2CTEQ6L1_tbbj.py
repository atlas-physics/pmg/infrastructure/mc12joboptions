evgenConfig.description = "Madgraph tbbj using AU2, CTEQ6L1"
evgenConfig.keywords = ["top"]
evgenConfig.inputfilecheck = "madgraph.181096.tbbj"

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

## For debugging only: print out some Pythia config info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10"]

