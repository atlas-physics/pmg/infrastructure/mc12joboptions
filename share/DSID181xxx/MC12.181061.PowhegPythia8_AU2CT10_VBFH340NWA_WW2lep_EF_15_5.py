evgenConfig.description = "POWHEG+PYTHIA8 using NWA, VBF H->WW->lvlv with AU2,CT10 "
evgenConfig.keywords = ["SMhiggs", "VBF", "W","leptonic"]
evgenConfig.inputfilecheck = "VBFH_NW_SM_M340"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 24 24',
                            '24:onMode = off',#decay of W
                            '24:mMin = 2.0',
                            '24:onIfMatch = 11 12',
                            '24:onIfMatch = 13 14',
                            '24:onIfMatch = 15 16'
                            ]

# ... Filter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter("Multi1TLeptonFilter")
topAlg += MultiLeptonFilter("Multi2LLeptonFilter")

topAlg.Multi1TLeptonFilter = topAlg.Multi1TLeptonFilter
topAlg.Multi1TLeptonFilter.Ptcut = 15000.
topAlg.Multi1TLeptonFilter.Etacut = 5.0
topAlg.Multi1TLeptonFilter.NLeptons = 1

topAlg.Multi2LLeptonFilter = topAlg.Multi2LLeptonFilter
topAlg.Multi2LLeptonFilter.Ptcut = 5000.
topAlg.Multi2LLeptonFilter.Etacut = 5.0
topAlg.Multi2LLeptonFilter.NLeptons = 2

StreamEVGEN.RequireAlgs  = [ "Multi1TLeptonFilter" ]
StreamEVGEN.RequireAlgs += [ "Multi2LLeptonFilter" ]
