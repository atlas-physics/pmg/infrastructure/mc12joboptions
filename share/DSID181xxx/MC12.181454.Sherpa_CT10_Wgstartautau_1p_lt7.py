include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "W(->lv) + gamma*(->tautau) with tautau between 2mtau and 7 GeV"
evgenConfig.keywords    = ["diboson", "Wgamma*", "Wgammastar"]
evgenConfig.contact     = ["david.hall@cern.ch"]
evgenConfig.inputconfcheck = "181454.Sherpa_CT10_Wgstartautau_1p_lt7"

evgenConfig.process = """
(run){
  INT_MINSIJ_FACTOR=1.e-14
  ERROR=0.01
  ACTIVE[25]=0
  MASSIVE[11]=1
  MASSIVE[13]=1
  MASSIVE[15]=1
  PARTICLE_CONTAINER 991[m:-1] leptons 11 -11 13 -13 15 -15;
}(run)

(processes){
  Process 93 93 -> 15 -15 -11 12 93{1};
  Order_EW 4
  CKKW sqr(25.0/E_CMS);
  Integration_Error 0.02 {5,6}
  Selector_File *|(selector1){|}(selector1)
  End process;

  Process 93 93 -> 15 -15 -13 14 93{1};
  Order_EW 4
  CKKW sqr(25.0/E_CMS);
  Integration_Error 0.02 {5,6}
  Selector_File *|(selector1){|}(selector1)
  End process;

  Process 93 93 -> 15 -15 -15 16 93{1};
  Order_EW 4
  CKKW sqr(25.0/E_CMS);
  Integration_Error 0.02 {5,6}
  Selector_File *|(selector2){|}(selector2)
  End process;

  Process 93 93 -> 15 -15 11 -12 93{1};
  Order_EW 4
  CKKW sqr(25.0/E_CMS);
  Integration_Error 0.02 {5,6}
  Selector_File *|(selector1){|}(selector1)
  End process;

  Process 93 93 -> 15 -15 13 -14 93{1};
  Order_EW 4
  CKKW sqr(25.0/E_CMS);
  Integration_Error 0.02 {5,6}
  Selector_File *|(selector1){|}(selector1)
  End process;

  Process 93 93 -> 15 -15 15 -16 93{1};
  Order_EW 4
  CKKW sqr(25.0/E_CMS);
  Integration_Error 0.02 {5,6}
  Selector_File *|(selector2){|}(selector2)
  End process;
}(processes)

(selector1){
  "m" 15,-15 3.55,7.0
  "PT" 991 5.0,E_CMS:5.0,E_CMS [PT_UP]
  "Eta" 991 -1000,1000:-1000,1000:-3.0,3.0:-3.0,3.0 [ETA_UP]
}(selector1)

(selector2){
  MinSelector {
    "m" 15,-15 3.55,7.0:3.55,E_CMS
    "m" 15,-15 3.55,E_CMS:3.55,7.0
  }
  "PT" 991 5.0,E_CMS:5.0,E_CMS [PT_UP]
  "Eta" 991 -1000,1000:-1000,1000:-3.0,3.0:-3.0,3.0 [ETA_UP]
}(selector2)
"""
