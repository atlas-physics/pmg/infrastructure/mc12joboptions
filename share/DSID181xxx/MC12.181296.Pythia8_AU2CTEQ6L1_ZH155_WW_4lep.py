evgenConfig.description = "PYTHIA8 ZH Z->ll, H->WW->lvlv with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "ZH", "W","leptonic"]

# ... Pythia
include ( "MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py" )
# ... Photos
include ( "MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
    '25:m0 = 155',
    '25:mWidth = 0.0303',
    '25:onMode = off',
    '25:doForceWidth = true',
    '25:onIfMatch = 24 24',
    'HiggsSM:ffbar2HZ = on',
    '23:onMode = off',                           
    '23:onIfAny = 11 13 15',
    '24:mMin = 2.0',
    '24:onMode = off',                           
    '24:onIfMatch = 11 12',
    '24:onIfMatch = 13 14',
    '24:onIfMatch = 15 16'
    ]
