evgenConfig.description = "POWHEG+Pythia8 using PowHel ttH H->gamgam (inclusive top decays) with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ttH", "inclusive"]
evgenConfig.contact = ["Elizaveta.Shabalina@cern.ch"]
evgenConfig.inputfilecheck = "PowHel-ttH_130"
evgenConfig.minevents = 2000

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
include("MC12JobOptions/Pythia8_Higgs_gamgam.py")
