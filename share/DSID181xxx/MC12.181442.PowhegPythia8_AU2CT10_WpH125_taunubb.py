evgenConfig.description = "POWHEG+Pythia8 W->taunuH->bb production and AU2 CT10 tune"
evgenConfig.keywords = ["WH" ,"NLO", "Powheg", "pythia8", "bottom","electron"]
evgenConfig.inputfilecheck = 'powheg.181442.WptaunuH_SM_M125' 

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',
                            '25:onIfMatch = 5 5'
                            ]
