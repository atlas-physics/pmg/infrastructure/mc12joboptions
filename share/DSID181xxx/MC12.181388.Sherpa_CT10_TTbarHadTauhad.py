include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Ttbar HadTauhad with up to 3 additional jets treating B and C quarks as massive"
evgenConfig.keywords = [ ]
evgenConfig.contact  = [ "frank.siegert@cern.ch","christian.schillo@cern.ch","christopher.young@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck="181388.Sherpa_CT10_TTbarHadTauhadMassiveCB"

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  MASSIVE[15]=1

  MASSIVE[4]=1
  MASSIVE[5]=1

  SOFT_SPIN_CORRELATIONS=1 
}(run)

(processes){

Process 93 93 -> 6[a] -6[b] 93{3}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
End process;

Process 93 93 -> 6[a] -6[b] 5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 93 -> 6[a] -6[b] 4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 5 -> 6[a] -6[b] 5 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 5 -> 6[a] -6[b] 5 5 -5  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 5 -> 6[a] -6[b] 5 4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -5 -> 6[a] -6[b] -5 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -5 -> 6[a] -6[b] 5 -5 -5  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -5 -> 6[a] -6[b] -5 4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 4 -> 6[a] -6[b] 4 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 4 -> 6[a] -6[b] 5 -5 4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 4 -> 6[a] -6[b] 4 4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -4 -> 6[a] -6[b] -4 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -4 -> 6[a] -6[b] 5 -5 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -4 -> 6[a] -6[b] 4 -4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 5 -> 6[a] -6[b] 5 5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -5 -> 6[a] -6[b] 93{3}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
End process;

Process 5 -5 -> 6[a] -6[b] 5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -5 -> 6[a] -6[b] 4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 4 -> 6[a] -6[b] 5 4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -4 -> 6[a] -6[b] 5 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -5 -5 -> 6[a] -6[b] -5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -5 4 -> 6[a] -6[b] -5 4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -5 -4 -> 6[a] -6[b] -5 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 4 -> 6[a] -6[b] 4 4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 -4 -> 6[a] -6[b] 93{3}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
End process;

Process 4 -4 -> 6[a] -6[b] 5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 -4 -> 6[a] -6[b] 4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -4 -4 -> 6[a] -6[b] -4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -2 1
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;
 
Process 93 93 -> 6[a] -6[b] 93{3}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 93 -> 6[a] -6[b] 5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 93 -> 6[a] -6[b] 4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 5 -> 6[a] -6[b] 5 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 5 -> 6[a] -6[b] 5 5 -5  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 5 -> 6[a] -6[b] 5 4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -5 -> 6[a] -6[b] -5 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -5 -> 6[a] -6[b] 5 -5 -5  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -5 -> 6[a] -6[b] -5 4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 4 -> 6[a] -6[b] 4 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 4 -> 6[a] -6[b] 5 -5 4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 4 -> 6[a] -6[b] 4 4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -4 -> 6[a] -6[b] -4 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -4 -> 6[a] -6[b] 5 -5 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -4 -> 6[a] -6[b] 4 -4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 5 -> 6[a] -6[b] 5 5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -5 -> 6[a] -6[b] 93{3}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -5 -> 6[a] -6[b] 5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -5 -> 6[a] -6[b] 4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 4 -> 6[a] -6[b] 5 4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -4 -> 6[a] -6[b] 5 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -5 -5 -> 6[a] -6[b] -5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -5 4 -> 6[a] -6[b] -5 4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -5 -4 -> 6[a] -6[b] -5 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 4 -> 6[a] -6[b] 4 4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 -4 -> 6[a] -6[b] 93{3}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 -4 -> 6[a] -6[b] 5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 -4 -> 6[a] -6[b] 4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -4 -4 -> 6[a] -6[b] -4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 2 -1
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;
 
Process 93 93 -> 6[a] -6[b] 93{3}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
End process;

Process 93 93 -> 6[a] -6[b] 5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 93 -> 6[a] -6[b] 4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 5 -> 6[a] -6[b] 5 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 5 -> 6[a] -6[b] 5 5 -5  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 5 -> 6[a] -6[b] 5 4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -5 -> 6[a] -6[b] -5 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -5 -> 6[a] -6[b] 5 -5 -5  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -5 -> 6[a] -6[b] -5 4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 4 -> 6[a] -6[b] 4 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 4 -> 6[a] -6[b] 5 -5 4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 4 -> 6[a] -6[b] 4 4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -4 -> 6[a] -6[b] -4 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -4 -> 6[a] -6[b] 5 -5 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -4 -> 6[a] -6[b] 4 -4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 5 -> 6[a] -6[b] 5 5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -5 -> 6[a] -6[b] 93{3}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
End process;

Process 5 -5 -> 6[a] -6[b] 5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -5 -> 6[a] -6[b] 4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 4 -> 6[a] -6[b] 5 4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -4 -> 6[a] -6[b] 5 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -5 -5 -> 6[a] -6[b] -5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -5 4 -> 6[a] -6[b] -5 4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -5 -4 -> 6[a] -6[b] -5 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 4 -> 6[a] -6[b] 4 4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 -4 -> 6[a] -6[b] 93{3}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
End process;

Process 4 -4 -> 6[a] -6[b] 5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 -4 -> 6[a] -6[b] 4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -4 -4 -> 6[a] -6[b] -4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> -15 16
Decay -24[d] -> -4 3
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;
 
Process 93 93 -> 6[a] -6[b] 93{3}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 93 -> 6[a] -6[b] 5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 93 -> 6[a] -6[b] 4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 5 -> 6[a] -6[b] 5 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 5 -> 6[a] -6[b] 5 5 -5  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 5 -> 6[a] -6[b] 5 4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -5 -> 6[a] -6[b] -5 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -5 -> 6[a] -6[b] 5 -5 -5  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -5 -> 6[a] -6[b] -5 4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 4 -> 6[a] -6[b] 4 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 4 -> 6[a] -6[b] 5 -5 4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 4 -> 6[a] -6[b] 4 4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -4 -> 6[a] -6[b] -4 93{2}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -4 -> 6[a] -6[b] 5 -5 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 93 -4 -> 6[a] -6[b] 4 -4 -4  
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 5 -> 6[a] -6[b] 5 5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -5 -> 6[a] -6[b] 93{3}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -5 -> 6[a] -6[b] 5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -5 -> 6[a] -6[b] 4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 4 -> 6[a] -6[b] 5 4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 5 -4 -> 6[a] -6[b] 5 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -5 -5 -> 6[a] -6[b] -5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -5 4 -> 6[a] -6[b] -5 4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -5 -4 -> 6[a] -6[b] -5 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 4 -> 6[a] -6[b] 4 4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 -4 -> 6[a] -6[b] 93{3}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 -4 -> 6[a] -6[b] 5 -5 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process 4 -4 -> 6[a] -6[b] 4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;

Process -4 -4 -> 6[a] -6[b] -4 -4 93{1}
Order_EW 4
Decay 6[a] -> 24[c] 5
Decay -6[b] -> -24[d] -5
Decay 24[c] -> 4 -3
Decay -24[d] -> 15 -16
CKKW sqr(30/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
Integration_Error 0.1 {7,8,9,10}
Cut_Core 1 
End process;
 
}(processes)

(selector){
  DecayMass 24 40.0 120.0
  DecayMass -24 40.0 120.0
  DecayMass 6 150.0 200.0
  DecayMass -6 150.0 200.0
  PT 4 1.0 E_CMS
  PT -4 1.0 E_CMS
  PT 5 1.0 E_CMS
  PT -5 1.0 E_CMS
}(selector)
"""

# Take tau BR's into account:
topAlg.Sherpa_i.Parameters += [ "DECAYFILE=HadronDecaysTauH.dat" ]
topAlg.Sherpa_i.CrossSectionScaleFactor=0.648

# Overwrite default MC11 widths with LO widths for top and W
topAlg.Sherpa_i.Parameters += [ "WIDTH[6]=1.47211", "WIDTH[24]=2.035169" ]

