include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Diphoton + jets production using ME+PS with up to three jets from the matrix element. Apply filter requiring two prompt photons and 60 GeV < Mgamgam < 80 GeV."
evgenConfig.keywords = ["jets", "photon", "diphoton"]
evgenConfig.contact  = ["tatsuya.masubuchi@cern.ch", "junichi.tanaka@cern.ch","frank.siegert@cern.ch"]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = '181604.Sherpa_CT10_gam35gam25_3j_Myy60to80GeV'

evgenConfig.process = """
(run){
  SCALES VAR{Abs2(p[2]+p[3])/4.0}
  ME_QED = Off
  QCUT:=7.0
}(run)

(processes){
  Process 21 21 -> 22 22
  Loop_Generator gg_yy
  End process;

  Process 93 93 -> 22 22 93{3}
  Order_EW 2
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/(Abs2(p[2]+p[3])/4.0));
  Integration_Error 0.1;
  End process
}(processes)

(selector){
  PT      22      14.0 E_CMS
  Mass    22  22  60.0 80.0
  DeltaR  22  93  0.3  100.0
}(selector)
"""

## Filter
from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
topAlg += DirectPhotonFilter("Direct1PhotonFilter")
topAlg += DirectPhotonFilter("Direct2PhotonFilter")

topAlg.Direct2PhotonFilter.Ptcut = 25000.
topAlg.Direct2PhotonFilter.Etacut =  2.7
topAlg.Direct2PhotonFilter.NPhotons = 2

topAlg.Direct1PhotonFilter.Ptcut = 35000.
topAlg.Direct1PhotonFilter.Etacut =  2.7
topAlg.Direct1PhotonFilter.NPhotons = 1

StreamEVGEN.RequireAlgs  = [ "Direct1PhotonFilter" ]
StreamEVGEN.RequireAlgs += [ "Direct2PhotonFilter" ]
