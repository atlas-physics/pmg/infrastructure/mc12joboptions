evgenConfig.description = "PYTHIA8 VBF H->ZZ ZZ->4lep with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "VBF","Z","leptonic"]

#include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 200' ,
                            '25:mWidth = 1.421204',
                            '25:onMode = off',
                            '25:doForceWidth = true',
                            '25:onIfMatch = 23 23',
                            'HiggsSM:ff2Hff(t:ZZ) = on',
                            'HiggsSM:ff2Hff(t:WW) = on',
                            '24:onMode = off',
                            '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16',
                            '23:mMin = 2.0',
                            '23:onMode = off',
                            '23:onIfMatch = 11 11',
                            '23:onIfMatch = 13 13',
                            '23:onIfMatch = 15 15'
                            ]
 
