include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Wgammagamma decaying hadronically to qqgammagamma, merged with up to two additional QCD jets in ME+PS." 
evgenConfig.keywords = [ "EW", "Diboson" ]
evgenConfig.inputconfcheck = "gammagammaWtoqq"
evgenConfig.minevents = 1000
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "junichi.tanaka@cern.ch" ]

evgenConfig.process="""
(run){
  ACTIVE[25]=0
}(run)

(processes){
  Process 93 93 -> 22 22 24[a] 93{2}
  Decay 24[a] -> 94 94
  Selector_File *|(selector1){|}(selector1)
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.05
  End process;

  Process 93 93 -> 22 22 -24[a] 93{2}
  Decay -24[a] -> 94 94
  Selector_File *|(selector2){|}(selector2)
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.05
  End process;
}(processes)

(selector1){
  PT 22 25.0 E_CMS
  DeltaR 22 22 0.2 1000.0
  DeltaR 22 93 0.2 1000.0
  DecayMass  24 40.0 2000.0
}(selector1)

(selector2){
  PT 22 25.0 E_CMS
  DeltaR 22 22 0.2 1000.0
  DeltaR 22 93 0.2 1000.0
  DecayMass -24 40.0 2000.0
}(selector2)
"""
