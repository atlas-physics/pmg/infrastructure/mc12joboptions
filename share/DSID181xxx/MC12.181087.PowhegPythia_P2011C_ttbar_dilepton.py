evgenConfig.description = "POWHEG+Pythia6 ttbar production with dilepton filter and PythiaPerugia2011C tune"
evgenConfig.keywords = ["top", "ttbar", "dilepton"]
evgenConfig.contact  = ["jeffrey.wetter@cern.ch","tatsuya.masubuchi@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar'
evgenConfig.minevents=1000

include("MC12JobOptions/PowhegPythia_Perugia2011C_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons = 2
topAlg.TTbarWToLeptonFilter.Ptcut = 0.0
