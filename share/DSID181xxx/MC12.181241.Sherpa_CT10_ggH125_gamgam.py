include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "H -> gamma gamma + up to 3 jets, produced by gluon-gluon fusion, using Sherpa's ME+PS with CKKW matching."
evgenConfig.keywords = [ "SMhiggs", "ggF", "GammaGamma" ]
evgenConfig.contact  = [ "kiran.joshi@cern.ch" ]
evgenConfig.minevents = 5000

evgenConfig.process="""
(processes){
  Process 93 93 -> 25[a] 93{3}
  Decay 25[a] -> 22 22
  Order_EW 1
  Integration_Error 0.05 {2,3,4}
  Integration_Error 0.10 {5}
  CKKW  sqr(15/E_CMS)
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;
}(processes)

(model){
  MODEL SM+EHC
  MASS[25] 125.0
}(model)

(me){
  ME_SIGNAL_GENERATOR = Amegic
  SCALES=STRICT_METS{MU_F2}{MU_R2}{MU_Q2}
  EXCLUSIVE_CLUSTER_MODE=1
}(me)
"""

evgenConfig.inputconfcheck = 'ggH125_gamgam'
topAlg.Sherpa_i.ResetWeight = 0
