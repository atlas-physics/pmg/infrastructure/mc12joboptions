
evgenConfig.description = "POWHEG+Pythia8 Diboson MSTW pdf and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak"  ,"Diboson" ]
evgenConfig.inputfilecheck = "WW"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Paolo Francavilla <francav@cern.ch>"]

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

