evgenConfig.description = "ALPGEN+Jimmy ttcc dilept process with AUET2 tune and pdf CTEQ6L1"
evgenConfig.keywords = ["EW","jets","ttbar","charm"]
evgenConfig.inputfilecheck = "ttccinclNp0"
evgenConfig.minevents = 50

include( 'MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py' )
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )

include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons = 2
topAlg.TTbarWToLeptonFilter.Ptcut = 0.0
