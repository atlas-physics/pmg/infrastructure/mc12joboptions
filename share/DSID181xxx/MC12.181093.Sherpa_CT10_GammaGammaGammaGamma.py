include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "4-photon + jets production using ME+PS with up to two jets from the matrix element."
evgenConfig.keywords = ["jets", "photon"]
evgenConfig.contact  = ["frank.siegert@cern.ch"]
evgenConfig.inputconfcheck = "GammaGammaGammaGamma"
evgenConfig.minevents = 5000

evgenConfig.process = """
(run){
  ME_QED = Off
  QCUT:=7.0
}(run)

(processes){
  Process 93 93 -> 22 22 22 22 93{1}
  Order_EW 4
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/(Abs2(p[2]+p[3]+p[4]+p[5])/4.0));
  Integration_Error 0.1;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6}
  End process
}(processes)

(selector){
  "PT"  22  15.0,E_CMS:15.0,E_CMS:10.0,E_CMS:5.0,E_CMS [PT_UP]
  DeltaR  22  93  0.2  100.0
}(selector)
"""
