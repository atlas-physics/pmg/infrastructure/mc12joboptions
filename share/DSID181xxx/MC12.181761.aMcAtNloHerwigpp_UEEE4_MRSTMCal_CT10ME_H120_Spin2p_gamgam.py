## Herwig++ config for the MRSTMCal UE-EE-4 tune series with an NLO ME PDF
include("MC12JobOptions/Herwigpp_UEEE4_MRSTMCal_CT10ME_Common.py")

## Add to commands
cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
create ThePEG::ParticleData graviton
setup graviton 5000004 graviton 120.0 0.00351 15.0 5.62185e-11 0 0 5 1
"""
topAlg.Herwigpp.Commands += cmds.splitlines()

from Herwigpp_i import config as hw
topAlg.Herwigpp.Commands += hw.lhef_cmds(filename="events.lhe", nlo=True).splitlines()

del cmds  

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators = ["aMcAtNlo", "Herwigpp"]
evgenConfig.minevents=5000
evgenConfig.keywords = ["Higgs", "spin2 nlo signal"]
evgenConfig.description = "Spin2 aMC@NLO+Herwig++ - CT10f4 PDF for hard scatter and MRSTMCal with UEE4 tune for shower+MPI"
evgenConfig.contact = ["florian.bernlochner@cern.ch"]
evgenConfig.inputfilecheck = "X2to2a_M120"
