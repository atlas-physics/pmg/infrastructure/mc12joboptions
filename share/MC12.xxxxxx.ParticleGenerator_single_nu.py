evgenConfig.description = "Single neutrinos with fixed eta and E: purely for pile-up/lumi testing"
evgenConfig.keywords = ["single particle", "neutrino"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = \
    ["PDGcode: constant 12",
     "e: constant 50000",
     "eta: constant 0.0",
     "phi: flat -3.14159 3.14159"]
