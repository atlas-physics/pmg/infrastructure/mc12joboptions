include( "MC12JobOptions/Sherpa_CT10_Common.py" )
evgenConfig.description = "Wenu + jets, with c and b-quarks treated as massive, C Jet filter and B hadron veto applied"
evgenConfig.keywords = [ "W", "leptonic", "e", "heavyquark", "renorm025" ]
evgenConfig.contact = ["vakhtang.tsiskaridze@cern.ch", "frank.siegert@cern.ch"]
evgenConfig.inputconfcheck = "WenuMassiveCBPt500"
evgenConfig.minevents = 200
evgenConfig.process="""
(run){
  MASSIVE[13]=1
  MASSIVE[15]=1

  MASSIVE[4]=1
  MASSIVE[5]=1

  ME_SIGNAL_GENERATOR=Comix

  RENSCALEFAC:=0.25
  SCALES=METS{MU_F2}{RENSCALEFAC*MU_R2}
}(run)

(processes){
  Process 93 93 -> 90 91 93 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> 90 91 93 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 93 -> 90 91 93 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> 90 91 93 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> 90 91 93 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process;

}(processes)

(selector){
  Mass 90 91 1.7 E_CMS
  PT2 90 91 500.0 E_CMS
  PT 93 1.0 E_CMS
  PT 4 1.0 E_CMS
  PT -4 1.0 E_CMS
  PT 5 1.0 E_CMS
  PT -5 1.0 E_CMS
}(selector)
"""

include("MC12JobOptions/CFilterBVeto.py")
