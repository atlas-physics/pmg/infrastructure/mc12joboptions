include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Electroweak production of 2 neutrinos and 2 jets, merged with up to three additional QCD jet in ME+PS."
evgenConfig.keywords = ["EW", "Diboson" ]
evgenConfig.contact  = ["Takashi.Yamanakat@cern.ch"]
evgenConfig.minevents = 2000

evgenConfig.process="""
(run){
  ACTIVE[6]=0
  ACTIVE[25]=0
  MASSIVE[4]=1
  MASSIVE[5]=1
  MASSIVE[11]=1
  MASSIVE[13]=1
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 ->  91 91 24 93{3}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  91 91 24 93{3}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  91 91 24 93{3}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  91 91 24 93{3}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  91 91 24 93{3}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  91 91 24 
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  91 91 24 93{1}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  91 91 24 93{2}
  Decay 24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;


  Process 93 93 ->  91 91 24 93{3}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  91 91 24 93{3}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  91 91 24 93{3}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  91 91 24 93{3}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  91 91 24 93{3}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  91 91 24 
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  91 91 24 93{1}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  91 91 24 93{2}
  Decay 24 -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;


  Process 93 93 ->  91 91 -24 93{3}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  91 91 -24 93{3}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  91 91 -24 93{3}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  91 91 -24 93{3}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  91 91 -24 93{3}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  91 91 -24 
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  91 91 -24 93{1}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  91 91 -24 93{2}
  Decay -24 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;


  Process 93 93 ->  91 91 -24 93{3}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  91 91 -24 93{3}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  91 91 -24 93{3}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  91 91 -24 93{3}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  91 91 -24 93{3}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  91 91 -24 
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  91 91 -24 93{1}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  91 91 -24 93{2}
  Decay -24 -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

}(processes)

(selector){
  NJetFinder 2 15.0 0.0 0.4 -1 5.0 10.0
}(selector)
"""
