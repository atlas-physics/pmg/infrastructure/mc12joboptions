include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Electroweak production of 2 neutrinos and 2 jets, merged with up to three additional QCD jet in ME+PS."
evgenConfig.keywords = ["EW", "Diboson" ]
evgenConfig.contact  = ["Takashi.Yamanaka@cern.ch"]
evgenConfig.minevents = 1000

evgenConfig.process="""
(run){
  ACTIVE[6]=0
  ACTIVE[25]=0
  MASSIVE[4]=1
  MASSIVE[5]=1
  MASSIVE[11]=1
  MASSIVE[13]=1
  MASSIVE[15]=1
  PARTICLE_CONTAINER 9923[m:-1] Zgamma 23 22;
}(run)

(processes){
  Process 93 93 ->  91 91 9923 93{3}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 -4 -> 91 91 9923 93{3}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1 
  End process;

  Process 5 -5 -> 91 91 9923 93{3}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 -> 91 91 9923 5 -5 93{1}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 5 -5 -> 91 91 9923 5 -5 93{1}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 5 -> 91 91 9923 5 93{2}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -5 -> 91 91 9923 -5 93{2}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 -> 91 91 9923 4 -4 93{1}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93  4 -> 91 91 9923 4 93{2}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 -> 91 91 9923 -4 93{2}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4 -4 -> 91 91 9923 4 -4 93{1}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  5 -5 -> 91 91 9923 4 -4 93{1}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4 -4 -> 91 91 9923 5 -5 93{1}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93  5 -> 91 91 9923 5 4 -4 
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93  4 -> 91 91 9923 4 5 -5 
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -5 -> 91 91 9923 -5 4 -4 
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 -> 91 91 9923 -4 5 -5 
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4  5 -> 91 91 9923 4 5 93{1}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4  5 -> 91 91 9923 -4 5 93{1}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4 -5 -> 91 91 9923 4 -5 93{1}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 -5 -> 91 91 9923 -4 -5 93{1}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7} 
  Cut_Core 1 
  End process;

  Process 4 4 -> 91 91 9923 4 4 93{1}
  Decay 9923 -> 94 94
  Orcer_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1 
  End process;

  Process -4 -4 -> 91 91 9923 -4 -4 93{1}
  Decay 9923 -> 94 94
  Orcer_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1 
  End process;

  Process 5 5 ->  91 91 9923 5 5 93{1}
  Decay 9923 -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;


  Process 93 93 ->  91 91 9923 93{3}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 5 -5 -> 91 91 9923 93{3}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 -4 -> 91 91 9923 93{3}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1 
  End process;

  Process 93 93 -> 91 91 9923 5 -5 93{1}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 5 -5 -> 91 91 9923 5 -5 93{1}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 5 -> 91 91 9923 5 93{2}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -5 -> 91 91 9923 -5 93{2}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 -> 91 91 9923 4 -4 93{1}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93  4 -> 91 91 9923 4 93{2}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 -> 91 91 9923 -4 93{2}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4 -4 -> 91 91 9923 4 -4 93{1}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  5 -5 -> 91 91 9923 4 -4 93{1}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4 -4 -> 91 91 9923 5 -5 93{1}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93  5 -> 91 91 9923 5 4 -4 
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93  4 -> 91 91 9923 4 5 -5 
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -5 -> 91 91 9923 -5 4 -4 
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 -> 91 91 9923 -4 5 -5 
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4  5 -> 91 91 9923 4 5 93{1}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4  5 -> 91 91 9923 -4 5 93{1}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4 -5 -> 91 91 9923 4 -5 93{1}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 -5 -> 91 91 9923 -4 -5 93{1}
  Decay 9923 -> 4 -4 
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7} 
  Cut_Core 1 
  End process;

  Process 4 4 -> 91 91 9923 4 4 93{1}
  Decay 9923 -> 4 -4
  Orcer_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1 
  End process;

  Process -4 -4 -> 91 91 9923 -4 -4 93{1}
  Decay 9923 -> 4 -4
  Orcer_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1 
  End process;

  Process 5 5 ->  91 91 9923 5 5 93{1}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -5 -5 ->  91 91 9923 -5 -5 93{1}
  Decay 9923 -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;


  Process 93 93 ->  91 91 9923 93{3}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 5 -5 -> 91 91 9923 93{3}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 -4 -> 91 91 9923 93{3}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1 
  End process;

  Process 93 93 -> 91 91 9923 5 -5 93{1}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 5 -5 -> 91 91 9923 5 -5 93{1}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 5 -> 91 91 9923 5 93{2}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -5 -> 91 91 9923 -5 93{2}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 -> 91 91 9923 4 -4 93{1}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93  4 -> 91 91 9923 4 93{2}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 -> 91 91 9923 -4 93{2}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4 -4 -> 91 91 9923 4 -4 93{1}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  5 -5 -> 91 91 9923 4 -4 93{1}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4 -4 -> 91 91 9923 5 -5 93{1}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93  5 -> 91 91 9923 5 4 -4 
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93  4 -> 91 91 9923 4 5 -5 
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -5 -> 91 91 9923 -5 4 -4 
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 -> 91 91 9923 -4 5 -5
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4  5 -> 91 91 9923 4 5 93{1}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4  5 -> 91 91 9923 -4 5 93{1}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4 -5 -> 91 91 9923 4 -5 93{1}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 -5 -> 91 91 9923 -4 -5 93{1}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7} 
  Cut_Core 1 
  End process;

  Process 4 4 -> 91 91 9923 4 4 93{1}
  Decay 9923 -> 5 -5
  Orcer_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1 
  End process;

  Process -4 -4 -> 91 91 9923 -4 -4 93{1}
  Decay 9923 -> 5 -5
  Orcer_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1 
  End process;

  Process 5 5 ->  91 91 9923 5 5 93{1}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -5 -5 ->  91 91 9923 -5 -5 93{1}
  Decay 9923 -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

}(processes)

(selector){
  NJetFinder 2 15.0 0.0 0.4 -1 5.0 10.0
  DecayMass 9923 7.0 E_CMS
}(selector)
"""
