include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "WW decaying semileptonically to munuqq, merged with up to three additional QCD jet in ME+PS."
evgenConfig.keywords = [ "EW", "Diboson" ]
evgenConfig.contact  = ["Takashi.Yamanaka@cern.ch"]
evgenConfig.minevents = 2000

evgenConfig.process="""
(run){
  ACTIVE[6]=0
  ACTIVE[25]=0
  MASSIVE[4]=1
  MASSIVE[5]=1
  MASSIVE[11]=1
  MASSIVE[13]=1
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 ->  -24[a] 24[b] 93{3}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 -4 -> -24[a] 24[b] 93{3}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 94
  Orcer_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1 
  End process;

  Process 93 93 -> -24[a] 24[b] 4 -4 93{1}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93  4 -> -24[a] 24[b] 4 93{2}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 -> -24[a] 24[b] -4 93{2}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4 -4 -> -24[a] 24[b] 4 -4 93{1}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 4 ->  24[a] -24[b] 4 4 93{1}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 -4 ->  24[a] -24[b] -4 -4 93{1}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;


  Process 93 93 ->  -24[a] 24[b] 93{3}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 -4 -> -24[a] 24[b] 93{3}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 4
  Orcer_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1 
  End process;

  Process 93 93 -> -24[a] 24[b] 4 -4 93{1}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93  4 -> -24[a] 24[b] 4 93{2}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 -> -24[a] 24[b] -4 93{2}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4 -4 -> -24[a] 24[b] 4 -4 93{1}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 4 ->  24[a] -24[b] 4 4 93{1}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 -4 ->  24[a] -24[b] -4 -4 93{1}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;


  Process 93 93 ->  24[a] -24[b] 93{3}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 -4 -> 24[a] -24[b] 93{3}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 94
  Orcer_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1 
  End process;

  Process 93 93 -> 24[a] -24[b] 4 -4 93{1}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93  4 -> 24[a] -24[b] 4 93{2}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 -> 24[a] -24[b] -4 93{2}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4 -4 -> 24[a] -24[b] 4 -4 93{1}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 4 ->  -24[a] 24[b] 4 4 93{1}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 -4 ->  -24[a] 24[b] -4 -4 93{1}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;


  Process 93 93 ->  24[a] -24[b] 93{3}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 -4 -> 24[a] -24[b] 93{3}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 -4
  Orcer_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1 
  End process;

  Process 93 93 -> 24[a] -24[b] 4 -4 93{1}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93  4 -> 24[a] -24[b] 4 93{2}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 -> 24[a] -24[b] -4 93{2}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process  4 -4 -> 24[a] -24[b] 4 -4 93{1}
  Decay 24[a] -> -13 14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 4 ->  -24[a] 24[b] 4 4 93{1}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 -4 ->  -24[a] 24[b] -4 -4 93{1}
  Decay -24[a] -> 13 -14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;


  Process 93 93 ->  24[a] 24[b] 93 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 4 ->  24[a] 24[b] 93 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 -4 ->  24[a] 24[b] 93 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 ->  24[a] 24[b] 93 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 4 ->  24[a] 24[b] 93 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  24[a] 24[b] 4 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  24[a] 24[b] -4 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 ->  24[a] 24[b] 4 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 4 ->  24[a] 24[b] -4 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  24[a] 24[b] 4 4 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  24[a] 24[b] -4 -4 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;


  Process 93 93 ->  24[a] 24[b] 93 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 4 ->  24[a] 24[b] 93 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 -4 ->  24[a] 24[b] 93 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 ->  24[a] 24[b] 93 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 4 ->  24[a] 24[b] 93 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  24[a] 24[b] 4 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  24[a] 24[b] -4 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 ->  24[a] 24[b] 4 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 4 ->  24[a] 24[b] -4 93 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  24[a] 24[b] 4 4 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  24[a] 24[b] -4 -4 93{1}
  Decay 24[a] -> -13 14
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;


  Process 93 93 ->  -24[a] -24[b] 93 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 4 ->  -24[a] -24[b] 93 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 -4 ->  -24[a] -24[b] 93 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 ->  -24[a] -24[b] 93 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 4 ->  -24[a] -24[b] 93 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  -24[a] -24[b] 4 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  -24[a] -24[b] -4 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 ->  -24[a] -24[b] 4 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 4 ->  -24[a] -24[b] -4 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  -24[a] -24[b] 4 4 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  -24[a] -24[b] -4 -4 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;


  Process 93 93 ->  -24[a] -24[b] 93 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 4 4 ->  -24[a] -24[b] 93 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 -4 ->  -24[a] -24[b] 93 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 ->  -24[a] -24[b] 93 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 4 ->  -24[a] -24[b] 93 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  -24[a] -24[b] 4 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  -24[a] -24[b] -4 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 -4 ->  -24[a] -24[b] 4 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 4 ->  -24[a] -24[b] -4 93 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  -24[a] -24[b] 4 4 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process 93 93 ->  -24[a] -24[b] -4 -4 93{1}
  Decay -24[a] -> 13 -14
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

}(processes)
"""
