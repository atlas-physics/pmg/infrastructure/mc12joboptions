#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.46589425E+03
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.34        # version number                      
#
BLOCK MODSEL  # Model selection
     1     1   # mSUGRA                                            
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         1     1.00000000E+02   # m0                  
         2     2.50000000E+02   # m1%2                
         3     1.00000000E+01   # tanbeta             
         4     1.00000000E+00   # sign(mu)            
         5    -1.00000000E+02   # A0                  
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     4.65894250E+02   # EWSB_scale          
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04847306E+01   # W+
        25     1.09944814E+02   # h
        35     3.95061603E+02   # H
        36     3.94652125E+02   # A
        37     4.03076945E+02   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.17765846E+03   # ~d_L
   2000001     5.15619556E+03   # ~d_R
   1000002     5.12260526E+03   # ~u_L
   2000002     5.15890743E+03   # ~u_R
   1000003     5.17765846E+03   # ~s_L
   2000003     5.15619556E+03   # ~s_R
   1000004     5.12260526E+03   # ~c_L
   2000004     5.15890743E+03   # ~c_R
   1000005     5.16907880E+03   # ~b_1
   2000005     5.16235964E+03   # ~b_2
   1000006     9.00000000E+02   # ~t_1
   2000006     5.16526704E+03   # ~t_2
   1000011     5.00783618E+03   # ~e_L
   2000011     5.12821157E+03   # ~e_R
   1000012     5.14863960E+03   # ~nu_eL
   1000013     5.00783618E+03   # ~mu_L
   2000013     5.12821157E+03   # ~mu_R
   1000014     5.14863960E+03   # ~nu_muL
   1000015     4.33337347E+03   # ~tau_1
   2000015     5.04807298E+03   # ~tau_2
   1000016     4.83975742E+03   # ~nu_tauL
   1000021     1.30000000E+03   # ~g
   1000022     8.80000000E+02   # ~chi_10
   1000023     4.80301328E+03   # ~chi_20
   1000025    -5.10584586E+03   # ~chi_30
   1000035     5.17904756E+03   # ~chi_40
   1000024     4.79676634E+03   # ~chi_1+
   1000037     5.18107884E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.85365149E-01   # N_11
  1  2    -5.63708172E-02   # N_12
  1  3     1.50968191E-01   # N_13
  1  4    -5.55559078E-02   # N_14
  2  1     1.06011099E-01   # N_21
  2  2     9.39731843E-01   # N_22
  2  3    -2.80737798E-01   # N_23
  2  4     1.63865793E-01   # N_24
  3  1     6.12646703E-02   # N_31
  3  2    -9.07084513E-02   # N_32
  3  3    -6.95185707E-01   # N_33
  3  4    -7.10447359E-01   # N_34
  4  1     1.18590934E-01   # N_41
  4  2    -3.24805127E-01   # N_42
  4  3    -6.44291647E-01   # N_43
  4  4     6.82148146E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.11526811E-01   # U_11
  1  2     4.11240652E-01   # U_12
  2  1     4.11240652E-01   # U_21
  2  2     9.11526811E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.70474216E-01   # V_11
  1  2     2.41204883E-01   # V_12
  2  1     2.41204883E-01   # V_21
  2  2     9.70474216E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     5.52971576E-01   # cos(theta_t)
  1  2     8.33200118E-01   # sin(theta_t)
  2  1    -8.33200118E-01   # -sin(theta_t)
  2  2     5.52971576E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.30133242E-01   # cos(theta_b)
  1  2     3.67222211E-01   # sin(theta_b)
  2  1    -3.67222211E-01   # -sin(theta_b)
  2  2     9.30133242E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     2.84520650E-01   # cos(theta_tau)
  1  2     9.58669912E-01   # sin(theta_tau)
  2  1    -9.58669912E-01   # -sin(theta_tau)
  2  2     2.84520650E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.14169459E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.65894250E+02  # DRbar Higgs Parameters
         1     3.52307595E+02   # mu(Q)               
         2     9.75127163E+00   # tanbeta(Q)          
         3     2.45019343E+02   # vev(Q)              
         4     1.62474709E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  4.65894250E+02  # The gauge couplings
     1     3.60962223E-01   # gprime(Q) DRbar
     2     6.46342415E-01   # g(Q) DRbar
     3     1.09643637E+00   # g3(Q) DRbar
#
BLOCK AU Q=  4.65894250E+02  # The trilinear couplings
  1  1    -6.83371102E+02   # A_u(Q) DRbar
  2  2    -6.83371102E+02   # A_c(Q) DRbar
  3  3    -5.06244568E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  4.65894250E+02  # The trilinear couplings
  1  1    -8.59260696E+02   # A_d(Q) DRbar
  2  2    -8.59260696E+02   # A_s(Q) DRbar
  3  3    -7.96839768E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  4.65894250E+02  # The trilinear couplings
  1  1    -2.53317942E+02   # A_e(Q) DRbar
  2  2    -2.53317942E+02   # A_mu(Q) DRbar
  3  3    -2.51561628E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  4.65894250E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.79078600E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  4.65894250E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.39555961E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  4.65894250E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.01153117E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.65894250E+02  # The soft SUSY breaking masses at the scale Q
         1     1.01473112E+02   # M_1                 
         2     1.91554089E+02   # M_2                 
         3     5.86419847E+02   # M_3                 
        21     3.23204645E+04   # M^2_Hd              
        22    -1.25091441E+05   # M^2_Hu              
        31     1.95452732E+02   # M_eL                
        32     1.95452732E+02   # M_muL               
        33     1.94612848E+02   # M_tauL              
        34     1.35952517E+02   # M_eR                
        35     1.35952517E+02   # M_muR               
        36     1.33481225E+02   # M_tauR              
        41     5.45693259E+02   # M_q1L               
        42     5.45693259E+02   # M_q2L               
        43     4.97700412E+02   # M_q3L               
        44     5.27681504E+02   # M_uR                
        45     5.27681504E+02   # M_cR                
        46     4.23537258E+02   # M_tR                
        47     5.25587342E+02   # M_dR                
        48     5.25587342E+02   # M_sR                
        49     5.22280285E+02   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.44629811E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.39741168E+00   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.19607562E-11   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E-00    2     1000022         4   # BR(~t_1 -> ~chi_10 c )
#     9.85527005E-01    2     1000022         4   # BR(~t_1 -> ~chi_10 c )
#     7.59811421E-03    2     1000022         2   # BR(~t_1 -> ~chi_10 u )
#           BR         NDA      ID1       ID2       ID3       ID4
#     6.87488055E-03    4     1000022         5   3000001   4000001   # BR(~t_1 -> chi_10 b f fbarprime)
#
#         PDG            Width
DECAY   2000006     9.89033329E+03   # stop2 decays
#          BR         NDA      ID1       ID2
     2.40721736E-04    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.18715647E-05    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.57129952E-05    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.82674852E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     4.48093915E-06    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.92041851E-06    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     1.10566314E-05    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     9.71426751E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     2.48010442E+04   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.75723921E-05    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.30489451E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.77921159E-08    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.92113300E-05    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.15814648E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     5.11632206E-06    2     1000006       -37   # BR(~b_1 -> ~t_1    H-)
     9.88313568E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.09494237E+03   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.25185116E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.31089271E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.48276492E-07    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.26312050E-06    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.00266838E-02    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.89749609E-06    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     9.29424511E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     2.85873095E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.10424195E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.65484273E-03    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.71253667E-07    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.48686289E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.93753781E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.97266472E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.56894299E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.26411334E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.17015758E-07    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.64297612E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.90776916E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.91300787E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.89536688E-03    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.34563435E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.81703834E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.90372241E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.89113509E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.16861867E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.21253278E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.65570660E-08    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.90828092E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.85873095E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.10424195E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.65484273E-03    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.71253667E-07    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.48686289E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.93753781E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.97266472E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.56894299E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.26411334E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.17015758E-07    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.64297612E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.90776916E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.91300787E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.89536688E-03    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.34563435E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.81703834E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.90372241E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.89113509E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.16861867E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.21253278E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.65570660E-08    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.90828092E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     5.13700022E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.28205377E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.59543946E-02    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.58402285E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.43199992E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99814410E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.85279383E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.10816143E-07    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     5.13700022E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.28205377E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.59543946E-02    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.58402285E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.43199992E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99814410E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.85279383E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.10816143E-07    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.89564251E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
#
#         PDG            Width
DECAY   2000015     9.37912042E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     6.56671065E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.78840354E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.08619988E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.35048279E-01    2     1000016       -24   # BR(~tau_2 -> ~nu_tauL W-)
     5.41315501E-06    2     1000015        25   # BR(~tau_2 -> ~tau_1   h)
     1.47819505E-06    2     1000015        35   # BR(~tau_2 -> ~tau_1   H)
     2.16383687E-06    2     1000015        36   # BR(~tau_2 -> ~tau_1   A)
     5.94400955E-01    2     1000015        23   # BR(~tau_2 -> ~tau_1   Z)
#
#         PDG            Width
DECAY   1000012     8.40279717E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     8.83165499E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.32364344E-02    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.08892072E-05    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     8.35871774E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     8.40279717E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     8.83165499E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.32364344E-02    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.08892072E-05    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     8.35871774E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     5.11167030E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.35389379E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.98004091E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.31393937E-04    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02236957E-06    2    -1000015       -37   # BR(~nu_tauL -> ~tau_1+ H-)
     8.64304404E-01    2    -1000015       -24   # BR(~nu_tauL -> ~tau_1+ W-)
#
#         PDG            Width
DECAY   1000024     8.81963052E+02   # chargino1+ decays
#          BR         NDA      ID1       ID2
     4.24254358E-02    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     7.87629725E-05    2    -1000015        16   # BR(~chi_1+ -> ~tau_1+  nu_tau)
     9.55812851E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     1.68295059E-03    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
#
#         PDG            Width
DECAY   1000037     5.80548216E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.52870041E-05    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     2.05727833E-06    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.52870041E-05    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     2.05727833E-06    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     9.31684458E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.65611418E-06    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.36214984E-07    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     3.36214984E-07    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     4.70585668E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.71196580E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.71196580E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.30622959E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.80131991E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     2.06831536E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.57938634E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.22713529E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     3.56490394E-03    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.21287517E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.50753519E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.83839364E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     5.94588086E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.43655546E-02    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     8.45041199E-03    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     2.43114857E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.43114857E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     5.84537173E-04    2     1000015       -15   # BR(~chi_20 -> ~tau_1-   tau+)
     5.84537173E-04    2    -1000015        15   # BR(~chi_20 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000025     9.60710957E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.65073945E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.83168892E-03    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.62780688E-03    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.62780688E-03    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.11042510E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.46461719E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.84719134E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.20036667E-06    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     5.46902209E-02    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     5.46902209E-02    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     5.09520043E-08    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     5.09520043E-08    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     5.09520043E-08    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     5.09520043E-08    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     1.53456051E-05    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.53456051E-05    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     2.15070968E-07    2     2000015       -15   # BR(~chi_30 -> ~tau_2-   tau+)
     2.15070968E-07    2    -2000015        15   # BR(~chi_30 -> ~tau_2+   tau-)
     1.77581543E-06    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.77581543E-06    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     2.52995193E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.81267783E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.55324179E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.91220040E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.91220040E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.03170585E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.54370749E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.84125066E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     7.24021962E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.74578979E-05    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     4.74578979E-05    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.33019443E-06    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.33019443E-06    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     1.34445771E-06    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     1.34445771E-06    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     3.79827346E-07    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     3.79827346E-07    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     4.74578979E-05    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     4.74578979E-05    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.33019443E-06    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.33019443E-06    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     1.34445771E-06    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     1.34445771E-06    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     3.79827346E-07    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     3.79827346E-07    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.45752966E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.45752966E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.45558995E-07    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.45558995E-07    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     6.77840055E-07    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     6.77840055E-07    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     1.20242247E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.20242247E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     2.84789270E-07    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     2.84789270E-07    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.20242247E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.20242247E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     2.84789270E-07    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     2.84789270E-07    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     4.37212543E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.37212543E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.19047866E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.19047866E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     8.91547221E-07    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     8.91547221E-07    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     8.91547221E-07    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     8.91547221E-07    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.04457098E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.04457098E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
