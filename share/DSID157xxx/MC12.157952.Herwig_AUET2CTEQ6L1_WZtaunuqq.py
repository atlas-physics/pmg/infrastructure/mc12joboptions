evgenConfig.description = "Herwig WZ in taunuqq final state"
evgenConfig.generators = ["Herwig"]
evgenConfig.keywords = ["diboson", "tau"]


include ( "MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc 12820",
                                "modbos 1 4",
                                "modbos 2 1"]    # note that 10000 is added to the herwig process number
# ... Tauola
include ( "MC12JobOptions/Jimmy_Tauola.py" )
# ... Photos
include ( "MC12JobOptions/Jimmy_Photos.py" )
# Add the filters:
