include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "ttbar + up to 3 jets with scale and merging variations in ME+PS. Decay modes: had had."
evgenConfig.keywords = [ "ttbar","ckkw40" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch","christian.schillo@cern.ch" ]
evgenConfig.minevents = 2000

evgenConfig.process="""
(run){
  MASSIVE[15]=1
}(run)

(processes){
  # qqb qqb
  Process 93 93 ->  6[a] -6[b] 93{3}
  Order_EW 4
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> 94 94
  Decay -24[d] -> 94 94
  CKKW sqr(40/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10}; # the quarks in the decay don't count
  End process
}(processes)

(selector){
  DecayMass 24 40.0 120.0
  DecayMass -24 40.0 120.0
  DecayMass 6 150.0 200.0
  DecayMass -6 150.0 200.0
}(selector)
"""

# Take tau BR's into account:
topAlg.Sherpa_i.CrossSectionScaleFactor=1.0

# Overwrite default MC11 widths with LO widths for top and W
topAlg.Sherpa_i.Parameters += [ "WIDTH[6]=1.47211", "WIDTH[24]=2.035169" ]
