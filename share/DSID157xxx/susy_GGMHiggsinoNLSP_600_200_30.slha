#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     6.00000000E+02   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+02   # mu(EWSB)            
        25     3.00000000E+01   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04961776E+01   # W+
        25     1.26000000E+02   # h
        35     2.00001110E+03   # H
        36     2.00000000E+03   # A
        37     2.00198426E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.50116504E+03   # ~d_L
   2000001     1.50021895E+03   # ~d_R
   1000002     1.49905318E+03   # ~u_L
   2000002     1.49956200E+03   # ~u_R
   1000003     1.50116504E+03   # ~s_L
   2000003     1.50021895E+03   # ~s_R
   1000004     1.49905318E+03   # ~c_L
   2000004     1.49956200E+03   # ~c_R
   1000005     1.49527270E+03   # ~b_1
   2000005     1.50609678E+03   # ~b_2
   1000006     1.50741563E+03   # ~t_1
   2000006     1.50828637E+03   # ~t_2
   1000011     1.50072738E+03   # ~e_L
   2000011     1.50065676E+03   # ~e_R
   1000012     1.49861490E+03   # ~nu_eL
   1000013     1.50072738E+03   # ~mu_L
   2000013     1.50065676E+03   # ~mu_R
   1000014     1.49861490E+03   # ~nu_muL
   1000015     1.49710408E+03   # ~tau_1
   2000015     1.50427364E+03   # ~tau_2
   1000016     1.49861490E+03   # ~nu_tauL
   1000021     6.00000000E+02   # ~g
   1000022     1.94554306E+02   # ~chi_10
   1000023    -2.03258914E+02   # ~chi_20
   1000025     1.00000076E+03   # ~chi_30
   1000035     1.00870485E+03   # ~chi_40
   1000024     1.98253658E+02   # ~chi_1+
   1000037     1.00667563E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     3.96720014E-02   # N_11
  1  2    -7.11346776E-02   # N_12
  1  3     7.13797507E-01   # N_13
  1  4    -6.95599819E-01   # N_14
  2  1     2.54296375E-02   # N_21
  2  2    -4.55971027E-02   # N_22
  2  3    -7.00016090E-01   # N_23
  2  4    -7.12216057E-01   # N_24
  3  1     8.73339369E-01   # N_31
  3  2     4.87112252E-01   # N_32
  3  3     1.05321783E-06   # N_33
  3  4    -4.62991679E-06   # N_34
  4  1     4.84833361E-01   # N_41
  4  2    -8.69239231E-01   # N_42
  4  3    -2.16931389E-02   # N_43
  4  4     9.42824480E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.69899290E-02   # U_11
  1  2     9.99635706E-01   # U_12
  2  1     9.99635706E-01   # U_21
  2  2     2.69899290E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.17207708E-01   # V_11
  1  2     9.93107423E-01   # V_12
  2  1     9.93107423E-01   # V_21
  2  2     1.17207708E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     8.89112969E-01   # cos(theta_t)
  1  2     4.57687807E-01   # sin(theta_t)
  2  1    -4.57687807E-01   # -sin(theta_t)
  2  2     8.89112969E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.75497459E-01   # cos(theta_b)
  1  2     7.37362315E-01   # sin(theta_b)
  2  1    -7.37362315E-01   # -sin(theta_b)
  2  2     6.75497459E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03615895E-01   # cos(theta_tau)
  1  2     7.10580518E-01   # sin(theta_tau)
  2  1    -7.10580518E-01   # -sin(theta_tau)
  2  2     7.03615895E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -3.35195556E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.00000000E+02   # mu(Q)               
         2     2.99999978E+01   # tanbeta(Q)          
         3     2.50907387E+02   # vev(Q)              
         4     3.79819801E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.54252035E-01   # gprime(Q) DRbar
     2     6.35197926E-01   # g(Q) DRbar
     3     1.12076976E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.03936491E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.56276536E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     3.03371782E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     6.00000000E+02   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.80382065E+06   # M^2_Hd              
        22    -3.24525017E+05   # M^2_Hu              
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     2.40847161E-04   # gluino decays
#          BR         NDA      ID1       ID2
     8.99767029E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.08958007E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
#           BR         NDA      ID1       ID2       ID3
     8.17066484E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.41713426E-04    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.35594041E-04    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.10246585E-04    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     8.17066484E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.41713426E-04    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.35594041E-04    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.10246585E-04    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.47121243E-01    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.32569385E-01    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.92502030E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     7.74606980E-03    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.53648070E-03    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.53648070E-03    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.53648070E-03    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.53648070E-03    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.37411613E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.37411613E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000024     7.36340203E-10   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.60403392E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.60403392E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.20135359E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.20135359E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     3.89224982E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000023     4.62734110E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.89574934E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.16614120E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.50767463E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.16614120E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.50767463E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     3.43374239E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.43374239E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.63236448E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.84997888E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.84997888E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.84997888E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.55834013E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.55834013E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.55834013E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.55834013E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     8.52787027E-03    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     8.52787027E-03    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     8.52787027E-03    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     8.52787027E-03    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.66819817E-03    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.66819817E-03    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
