include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "single topbar + W + up to 3 jets in ME+PS. Decay modes: tau from topbar / W to all leptons."
evgenConfig.keywords = [ ]
evgenConfig.contact  = [ "frank.siegert@cern.ch","christian.schillo@cern.ch","christopher.young@cern.ch" ]
evgenConfig.minevents = 2000

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  MASSIVE[15]=1
  CKMORDER=1
}(run)

(processes){
  # single top + W
  Process 93 93 ->  -6[a] 24[b] 93{3}
  Order_EW 4
  Decay -6[a] -> -24[c] -5
  Decay 24[b] -> -15 16
  Decay -24[c] -> 15 -16
  No_Decay 6
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  Print_Graphs : Process
  End process;

  # single top + W
  Process 93 93 ->  -6[a] 24[b] 93{3}
  Order_EW 4
  Decay -6[a] -> -24[c] -5
  Decay 24[b] -> 90 91
  Decay -24[c] -> 15 -16
  No_Decay 6
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  Print_Graphs : Process
  End process;
}(processes)

(selector){
  DecayMass -24 40.0 120.0
  DecayMass 24 40.0 E_CMS
  DecayMass -6 150.0 200.0
}(selector)
"""

# Take tau BR's into account:
topAlg.Sherpa_i.CrossSectionScaleFactor=1.0

# Overwrite default MC11 widths with LO widths for top and W
topAlg.Sherpa_i.Parameters += [ "WIDTH[6]=1.47211", "WIDTH[24]=2.035169" ]
