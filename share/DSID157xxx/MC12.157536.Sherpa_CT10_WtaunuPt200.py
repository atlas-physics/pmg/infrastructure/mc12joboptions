include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "W -> tau v + up to 5 jets with pT_W>200 GeV."
evgenConfig.keywords = [ "W" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch" ]
evgenConfig.minevents = 1000

evgenConfig.process="""
(run){
  MASSIVE[11]=1
  MASSIVE[13]=1
  SOFT_SPIN_CORRELATIONS=1
}(run)

(processes){
  Process 93 93 -> 90 91 93 93{4}
  Order_EW 2
  CKKW sqr(30/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  #Enhance_Factor 5.0 {4,5,6,7,8}
  End process;
}(processes)

(selector){
  Mass 90 91 1.7 E_CMS
  PT2  90 91 200.0 E_CMS
}(selector)
"""
