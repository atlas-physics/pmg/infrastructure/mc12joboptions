evgenConfig.description = "ALPGEN+Pythia W(->lnu)+cc process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "W"]
evgenConfig.inputfilecheck = "WccNp0"
evgenConfig.minevents = 5000

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )
