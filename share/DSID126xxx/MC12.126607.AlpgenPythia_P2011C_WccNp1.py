evgenConfig.description = "ALPGEN+Pythia W(->lnu)+cc+1jet process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "W"]
evgenConfig.inputfilecheck = "WccNp1"
evgenConfig.minevents = 5000

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )
