
evgenConfig.description = "H0 340.0 GeV, with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["H0","hh","lyy"]

#Tune
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

#Photos
#include("MC12JobOptions/Pythia8_Photos.py")

#Pythia8 Commands
topAlg.Pythia8.Commands += ["Higgs:useBSM on", "HiggsBSM:allH2 on",
                           "35:m0 340.0", #H0 mass
                           "35:onMode = off", #turn off all H0 decays
                           "35:onIfMatch = 25 25", #H0->hh

		           "25:onMode = off", #turn off all h decays
	                   "25:onIfAny = 22 23 24"] #h->y/W/Z
#                          "24:onMode = off" #turn off all W decays
#	                   "24:onIfAny = 11 13 15" #W->l
#	                   "23:onMode = off" #turn off all Z decays
#                          "23:onIfAny = 12 14 16" #Z->vv
#	                   "23:onIfAny = 11 13 15" #Z->ll

# Generator Filters

from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()
MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 1
MultiElecMuTauFilter.MinPt = 10000.
MultiElecMuTauFilter.MaxEta = 2.7
MultiElecMuTauFilter.IncludeHadTaus = 0

from GeneratorFilters.GeneratorFiltersConf import DiPhotonFilter
topAlg += DiPhotonFilter()
DiPhotonFilter = topAlg.DiPhotonFilter
DiPhotonFilter.PtCut1st = 20000.
DiPhotonFilter.PtCut2nd = 20000.
DiPhotonFilter.MassCutFrom = 120000.
DiPhotonFilter.MassCutTo = 130000.

#MessageSvc.infoLimit = 100000
#MessageSvc.debugLimit = 100000
#DiPhotonFilter.OutputLevel = DEBUG

StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter","DiPhotonFilter" ]


