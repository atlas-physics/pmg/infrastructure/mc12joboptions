################################################################
#
# gg2ZZ2.0/JIMMY/HERWIG gg -> ZZ, with Z -> 4leptons (eemumu, eetautau, mumutautau) 
include('MC12JobOptions/Jimmy_AUET2_CT10_Common.py')

topAlg.Herwig.HerwigCommand += ['iproc mcatnlo','modbos 1 5', 'modbos 2 5',
                          'maxpr 10'] # Z decay actually performed in gg2ZZ

include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')

include('MC12JobOptions/MultiLeptonFilter.py')
topAlg.MultiLeptonFilter.Ptcut = 5000.
topAlg.MultiLeptonFilter.Etacut = 6
topAlg.MultiLeptonFilter.NLeptons = 2

evgenConfig.generators += [ 'McAtNlo', 'Herwig' ]
evgenConfig.description = 'gg2ZZ, Z-> 4lep events (eemm, eett, mmtt) showered with Jimmy/Herwig 2-lepton filter'
evgenConfig.keywords = ['diboson', 'leptonic']
evgenConfig.contact = ['daniela.rebuzzi@cern.ch','estel.perez.codina@cern.ch']
evgenConfig.inputfilecheck = 'gg2ZZ.179396.ZZ4lep_2lepFilt' # use the same input events as 116600, reorganized in different files
