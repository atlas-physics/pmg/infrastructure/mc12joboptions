evgenConfig.description = "aMcAtNlo+Herwig ZZ ee+mumu production, m(ll) > 25 GeV"
evgenConfig.keywords = ["ZZ", "diboson", "leptonic"]
evgenConfig.contact  = ["Antoine Marzin <antoine.marzin@cern.ch>"]
evgenConfig.inputfilecheck = 'ZZ_2e2mu_mll25'


include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc lhef"]
evgenConfig.generators += [ "aMcAtNlo" ]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
 
