
MASS=800
MASSX=600
CASE='stop'
DECAY='false'
MODEL='generic'
STOPPED = 'true'

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " generic stop 800GeV 600GeV stopped"
evgenConfig.specialConfig = "CASE={case};CHARGE=999;DECAY={decay};MASS={mass};MASSX={massx};MODEL={model};preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(case=CASE, decay=DECAY, mass=MASS, massx=MASSX, model=MODEL)

