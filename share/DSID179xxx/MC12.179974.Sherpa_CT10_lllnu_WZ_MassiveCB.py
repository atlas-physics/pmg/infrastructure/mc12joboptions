include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa 1.4.1 diboson lllnu, inclusive WZ, up to 3 jets with ME+PS, massive c,b."
evgenConfig.keywords = ["diboson", "llln", "lllnu", "WZ" ]
evgenConfig.contact  = [ "Takashi.Yamanaka@cern.ch" ]
evgenConfig.minevents = 1000

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ACTIVE[6]=0
  ERROR=0.02
  MASSIVE[11]=1
  MASSIVE[13]=1
  MASSIVE[15]=1
  MASSIVE[4]=1
  MASSIVE[5]=1
  PARTICLE_CONTAINER 991[m:-1] leptons 11 -11 13 -13 15 -15;
}(run)

(processes){
  Process 93 93 -> 11 -11 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 11 -11 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 11 -11 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 11 -11 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 11 -11 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 11 -11 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 11 -11 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 11 -11 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


  Process 93 93 -> 11 -11 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 11 -11 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 11 -11 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 11 -11 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 11 -11 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 11 -11 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 11 -11 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 11 -11 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;



    Process 93 93 -> 11 -11 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 11 -11 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 11 -11 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 11 -11 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 11 -11 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 11 -11 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 11 -11 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 11 -11 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


    Process 93 93 -> 13 -13 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 13 -13 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 13 -13 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 13 -13 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 13 -13 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 13 -13 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 13 -13 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 13 -13 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


    Process 93 93 -> 13 -13 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 13 -13 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 13 -13 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 13 -13 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 13 -13 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 13 -13 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 13 -13 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 13 -13 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


  Process 93 93 -> 13 -13 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 13 -13 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 13 -13 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 13 -13 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 13 -13 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 13 -13 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 13 -13 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 13 -13 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


    Process 93 93 -> 15 -15 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 15 -15 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 15 -15 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 15 -15 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 15 -15 -11 12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 15 -15 -11 12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 15 -15 -11 12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 15 -15 -11 12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


  Process 93 93 -> 15 -15 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 15 -15 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 15 -15 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 15 -15 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 15 -15 -13 14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 15 -15 -13 14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 15 -15 -13 14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 15 -15 -13 14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


  Process 93 93 -> 15 -15 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 15 -15 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 15 -15 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 15 -15 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 15 -15 -15 16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 15 -15 -15 16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 15 -15 -15 16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 15 -15 -15 16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  
  Process 93 93 -> 11 -11 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 11 -11 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 11 -11 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 11 -11 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 11 -11 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 11 -11 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 11 -11 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 11 -11 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


  Process 93 93 -> 11 -11 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 11 -11 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 11 -11 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 11 -11 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 11 -11 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 11 -11 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 11 -11 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 11 -11 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


  Process 93 93 -> 11 -11 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 11 -11 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 11 -11 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 11 -11 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 11 -11 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 11 -11 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 11 -11 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 11 -11 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


    Process 93 93 -> 13 -13 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 13 -13 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 13 -13 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 13 -13 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 13 -13 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 13 -13 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 13 -13 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 13 -13 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


    Process 93 93 -> 13 -13 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 13 -13 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 13 -13 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 13 -13 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 13 -13 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 13 -13 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 13 -13 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 13 -13 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


    Process 93 93 -> 13 -13 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 13 -13 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 13 -13 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 13 -13 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 13 -13 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 13 -13 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 13 -13 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 13 -13 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


    Process 93 93 -> 15 -15 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 15 -15 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 15 -15 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 15 -15 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 15 -15 11 -12 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 15 -15 11 -12 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 15 -15 11 -12 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 15 -15 11 -12 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


    Process 93 93 -> 15 -15 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 15 -15 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 15 -15 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 15 -15 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 15 -15 13 -14 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 15 -15 13 -14 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 15 -15 13 -14 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 15 -15 13 -14 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;


   Process 93 93 -> 15 -15 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> 15 -15 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 15 -15 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 15 -15 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 15 -15 15 -16 93{3}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5 15 -15 15 -16 
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5 15 -15 15 -16 93{1}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4 15 -15 15 -16 93{2}
  Order_EW 4
  Integration_Error 0.05 {6,7}
  CKKW sqr(20.0/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  Cut_Core 1
  End process;

}(processes)

(selector){
  Mass  11 -11  0.1  E_CMS
  "PT" 991 5.0,E_CMS:5.0,E_CMS [PT_UP]
}(selector)
"""
