include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Dibosons decaying semileptonically to taunuqq, merged with up to three additional QCD jets in ME+PS." 
evgenConfig.keywords = [ "EW", "Diboson" ]
evgenConfig.inputconfcheck = "VVtotaunuqq"
evgenConfig.minevents = 5000
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch" ]

evgenConfig.process="""
(run){
  ACTIVE[6]=0
  ACTIVE[25]=0
  PARTICLE_CONTAINER 9923[m:-1] Zgamma 23 22;
  RENSCALEFAC:=4.0
}(run)

(processes){
  Process 93 93 -> 24[a] 9923[b] 93{2}
  Selector_File *|(selector2){|}(selector2)
  Decay 24[a] -> -15 16
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7}
  Integration_Error 0.05 {5,6,7}
  End process;

  Process 93 93 -> 24[a] 24[b] 93{2}
  Decay 24[a] -> -15 16
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7}
  Integration_Error 0.05 {5,6,7}
  End process;

  Process 93 93 -> 24[a] -24[b] 93{2}
  Decay 24[a] -> -15 16
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7}
  Integration_Error 0.05 {5,6,7}
  End process;

  Process 93 93 -> -24[a] 9923[b] 93{2}
  Selector_File *|(selector2){|}(selector2)
  Decay -24[a] -> 15 -16
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7}
  Integration_Error 0.05 {5,6,7}
  End process;

  Process 93 93 -> -24[a] 24[b] 93{2}
  Decay -24[a] -> 15 -16
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7}
  Integration_Error 0.05 {5,6,7}
  End process;

  Process 93 93 -> -24[a] -24[b] 93{2}
  Decay -24[a] -> 15 -16
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{RENSCALEFAC*MU_R2} {6,7}
  Integration_Error 0.05 {5,6,7}
  End process;

}(processes)

(selector){
}(selector)

(selector2){
  DecayMass 9923 7.0 E_CMS
}(selector2)
"""
