#-------------------------------------------------------------------------
#
# RPV displaced vertex with decay to electrons
# 
# contact :  Hyeon Jin Kim
#
evgenConfig.description = "RPV Long-lived, DV->e+jets events, with PYTHIA6"
evgenConfig.keywords = ["RPV", "lambdaprime", "electron"]

#The default tune for Pythia samples (for MC12)
include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )

topAlg.Pythia.PythiaCommand += ["pysubs msel 0"]  # !
## Switch off Pythia color strgins, they don't work correctly for late decaying particles!
topAlg.Pythia.PythiaCommand += ["pypars mstp 95 0"]

topAlg.Pythia.PythiaCommand += [
    "pysubs msub 271 1",  # squark pair production 
    "pysubs msub 272 1",  # squark pair production 
    "pysubs msub 273 1",  # squark pair production 
    "pysubs msub 274 1",  # squark pair production 
    "pysubs msub 275 1",  # squark pair production 
    "pysubs msub 276 1",  # squark pair production 
    "pysubs msub 277 1",  # squark pair production 
    "pysubs msub 278 1",  # squark pair production 
    "pysubs msub 279 1",  # squark pair production 
    "pysubs msub 280 1"   # squark pair production
    ]
#######
topAlg.Pythia.PythiaCommand += [
    "pymssm imss 1 1", # general MSSM !!
    "pymssm imss 52 3", #switch on RPV lambda^prime coupling
    "pymsrv rvlamp 111 0.0002" # setting lambda^prime coupling strength for electronss; need to change with LSPmass 
    ]
 
topAlg.Pythia.PythiaCommand += [
    "pydat1 mstj 22 3", # allow long decay length !
    "pydat1 parj 72 100000.", # max length set to 100m
    "pydat3 mdme 2205 1 0", # switch explicetly off RPV decays to neutralinos (for electrons)
    "pydat3 mdme 2206 1 0" # switch explicetly off RPV decays to neutralinos (for electrons) 
    ]

topAlg.Pythia.PythiaCommand += [
    "pymssm rmss 1 110.", #M1 gaugino
    "pymssm rmss 2 5000.", #M2 gaugino
    "pymssm rmss 3 5000.", #M3 gluino
    "pymssm rmss 4 1000.", #mu higgsino, default=800  , make chi10 lighter than other gauginos
    "pymssm rmss 5 2.",  #tan beta , default=2  , make chi10 lighter than other gauginos
    "pymssm rmss 6 5000.", #left slepton, makes sneutrino mass high (also eL), to surpress LQD decay to neutrinos
    "pymssm rmss 7 5000.", #right slepton, irrelevant in LQD
    "pymssm rmss 8 1000.", #left squark
    "pymssm rmss 9 1000.", #right squark
    "pymssm rmss 10 5000.", #third generation #set high values to surpress production
    "pymssm rmss 11 5000.", #third generation
    "pymssm rmss 12 5000.", #third generation
    "pymssm rmss 13 5000.", #third generation
    "pymssm rmss 14 5000." #third generation
    ]

#---------------------------------------------------------------
# Includes
#---------------------------------------------------------------

# ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )

# ... Photos
#include ( "MC12JobOptions/Pythia_Photos.py" )

#---------------------------------------------------------------
#  Decay Length Filter
#---------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import DecayLengthFilter
topAlg += DecayLengthFilter()

DecayLengthFilter = topAlg.DecayLengthFilter
DecayLengthFilter.McEventCollection = "GEN_EVENT"
# specify desired decay region:
DecayLengthFilter.Rmin = 0
DecayLengthFilter.Rmax = 300
DecayLengthFilter.Zmin = 0
DecayLengthFilter.Zmax = 2720
DecayLengthFilter.particle_id = 1000022;

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs += [ "DecayLengthFilter" ]

#---------------------------------------------------------------
#End of job options file
#
###############################################################

