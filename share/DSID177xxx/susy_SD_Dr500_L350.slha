#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.46589425E+03
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.34        # version number                      
#
BLOCK MODSEL  # Model selection
     1     1   #  soft terms at weak scale                         
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         1     1.00000000E+02   # m0                  
         2     2.50000000E+02   # m1%2                
         3     1.00000000E+01   # tanbeta             
         4     1.00000000E+00   # sign(mu)            
         5    -1.00000000E+02   # A0                  
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     4.65894250E+02   # EWSB_scale          
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        25     1.25144814E+02   # h
        35     3.95061603E+02   # H
        36     3.94652125E+02   # A
        37     4.03076945E+02   # H+
   1000001     5.12260526E+03   # ~d_L
   2000001     5.00000000E+02   # ~d_R
   1000002     5.12260526E+03   # ~u_L
   2000002     5.15890743E+03   # ~u_R
   1000003     5.12260526E+03   # ~s_L
   2000003     5.00000000E+02   # ~s_R
   1000004     5.12260526E+03   # ~c_L
   2000004     5.15890743E+03   # ~c_R
   1000005     5.16907880E+03   # ~b_1
   2000005     5.16235964E+03   # ~b_2
   1000006     5.12148310E+03   # ~t_1
   2000006     5.16526704E+03   # ~t_2
   1000011     5.00783618E+03   # ~e_L
   2000011     5.12821157E+03   # ~e_R
   1000012     5.14863960E+03   # ~nu_eL
   1000013     5.00783618E+03   # ~mu_L
   2000013     5.12821157E+03   # ~mu_R
   1000014     5.14863960E+03   # ~nu_muL
   1000015     5.33337347E+03   # ~tau_1
   2000015     5.04807298E+03   # ~tau_2
   1000016     5.83975742E+03   # ~nu_tauL
   1000021     1.01232343E+04   # ~g
   1000022     3.50000000E+02   # ~chi_10
   1000023     4.25000000E+02   # ~chi_20
   1000025     3.10584586E+03   # ~chi_30
   1000035     4.17904756E+03   # ~chi_40
   1000024     1.79676634E+03   # ~chi_1+
   1000037     2.18107884E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     0.00000000E+00   # N_11
  1  2     1.00000000E+00   # N_12
  1  3     0.00000000E+00   # N_13
  1  4     0.00000000E+00   # N_14
  2  1     1.00000000E+00   # N_21
  2  2     0.00000000E+00   # N_22
  2  3     0.00000000E+00   # N_23
  2  4     0.00000000E+00   # N_24
  3  1     0.00000000E+00   # N_31
  3  2     0.00000000E+00   # N_32
  3  3     0.00000000E+00   # N_33
  3  4     1.00000000E+00   # N_34
  4  1     0.00000000E+00   # N_41
  4  2     0.00000000E+00   # N_42
  4  3     1.00000000E+00   # N_43
  4  4     0.00000000E+00   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.11526811E-01   # U_11
  1  2     4.11240652E-01   # U_12
  2  1     4.11240652E-01   # U_21
  2  2     9.11526811E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.70474216E-01   # V_11
  1  2     2.41204883E-01   # V_12
  2  1     2.41204883E-01   # V_21
  2  2     9.70474216E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     5.52971576E-01   # cos(theta_t)
  1  2     8.33200118E-01   # sin(theta_t)
  2  1    -8.33200118E-01   # -sin(theta_t)
  2  2     5.52971576E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.30133242E-01   # cos(theta_b)
  1  2     3.67222211E-01   # sin(theta_b)
  2  1    -3.67222211E-01   # -sin(theta_b)
  2  2     9.30133242E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     2.84520650E-01   # cos(theta_tau)
  1  2     9.58669912E-01   # sin(theta_tau)
  2  1    -9.58669912E-01   # -sin(theta_tau)
  2  2     2.84520650E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.14169459E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.65894250E+02  # DRbar Higgs Parameters
         1     3.52307595E+02   # mu(Q)               
         2     9.75127163E+00   # tanbeta(Q)          
         3     2.45019343E+02   # vev(Q)              
         4     1.62474709E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  4.65894250E+02  # The gauge couplings
     1     3.60962223E-01   # gprime(Q) DRbar
     2     6.46342415E-01   # g(Q) DRbar
     3     1.09643637E+00   # g3(Q) DRbar
#
BLOCK AU Q=  4.65894250E+02  # The trilinear couplings
  1  1    -6.83371102E+02   # A_u(Q) DRbar
  2  2    -6.83371102E+02   # A_c(Q) DRbar
  3  3    -5.06244568E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  4.65894250E+02  # The trilinear couplings
  1  1    -8.59260696E+02   # A_d(Q) DRbar
  2  2    -8.59260696E+02   # A_s(Q) DRbar
  3  3    -7.96839768E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  4.65894250E+02  # The trilinear couplings
  1  1    -2.53317942E+02   # A_e(Q) DRbar
  2  2    -2.53317942E+02   # A_mu(Q) DRbar
  3  3    -2.51561628E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  4.65894250E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.79078600E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  4.65894250E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.39555961E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  4.65894250E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.01153117E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.65894250E+02  # The soft SUSY breaking masses at the scale Q
         1     1.01473112E+02   # M_1                 
         2     1.91554089E+02   # M_2                 
         3     5.86419847E+02   # M_3                 
        21     3.23204645E+04   # M^2_Hd              
        22    -1.25091441E+05   # M^2_Hu              
        31     1.95452732E+02   # M_eL                
        32     1.95452732E+02   # M_muL               
        33     1.94612848E+02   # M_tauL              
        34     1.35952517E+02   # M_eR                
        35     1.35952517E+02   # M_muR               
        36     1.33481225E+02   # M_tauR              
        41     5.45693259E+02   # M_q1L               
        42     5.45693259E+02   # M_q2L               
        43     4.97700412E+02   # M_q3L               
        44     5.27681504E+02   # M_uR                
        45     5.27681504E+02   # M_cR                
        46     4.23537258E+02   # M_tR                
        47     1.00000000E+02   # M_dR                
        48     5.25587342E+02   # M_sR                
        49     5.22280285E+02   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.44629811E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.27411880E+03   # gluino decays
#          BR         NDA      ID1       ID2
     3.68942174E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     3.68942174E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.63379390E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.63379390E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     3.68942174E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     3.68942174E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     3.65338566E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     3.65338566E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     3.68942174E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     3.68942174E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.63379390E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.63379390E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     3.68942174E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     3.68942174E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     3.65338566E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     3.65338566E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     3.65071558E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     3.65071558E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     3.66186990E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     3.66186990E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     3.64311880E-02    2     1000006        -6   # BR(~g -> ~t_1  tb)
     3.64311880E-02    2    -1000006         6   # BR(~g -> ~t_1* t )
     3.71224965E-02    2     2000006        -6   # BR(~g -> ~t_2  tb)
     3.71224965E-02    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     8.20108890E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.93071128E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.42321030E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.93161679E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.14323150E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.47263430E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   2000006     7.85893212E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     2.70946460E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.09729241E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     3.32047177E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.21840832E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.41926073E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   1000005     7.99925067E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.18591733E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.46058470E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.40355381E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     3.32927105E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.31471762E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
#
#         PDG            Width
DECAY   2000005     2.26350377E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.80632112E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.12747586E-01    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     8.44350688E-03    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.76753404E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.21423391E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
#
#         PDG            Width
DECAY   1000002     5.61561335E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     3.81735921E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.31828394E-02    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.73852628E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.12286112E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
#
#         PDG            Width
DECAY   2000002     1.19255322E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     2.56033964E-34    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.00000000E+00    2     1000023         2   # BR(~u_R -> ~chi_20 u)
#
#         PDG            Width
DECAY   1000001     5.57042081E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.84832926E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.32897911E-02    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.10364503E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.15127805E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
#
#         PDG            Width
DECAY   2000001     3.15037578E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.67501782E-34    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.00000000E+00    2     1000023         1   # BR(~d_R -> ~chi_20 d)
#
#         PDG            Width
DECAY   1000004     5.61561335E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.81735921E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.31828394E-02    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.73852628E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.12286112E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
#
#         PDG            Width
DECAY   2000004     1.19255322E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.56033964E-34    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.00000000E+00    2     1000023         4   # BR(~c_R -> ~chi_20 c)
#
#         PDG            Width
DECAY   1000003     5.57042081E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.84832926E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.32897911E-02    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.10364503E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.15127805E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
#
#         PDG            Width
DECAY   2000003     3.15037578E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.67501782E-34    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.00000000E+00    2     1000023         3   # BR(~s_R -> ~chi_20 s)
#
#         PDG            Width
DECAY   1000011     5.78775238E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.56051205E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.10531295E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.53564542E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     7.98529582E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.62218491E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.56296305E-34    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.00000000E+00    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     5.78775238E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.56051205E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.10531295E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.53564542E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     7.98529582E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.62218491E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.56296305E-34    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.00000000E+00    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     3.11851968E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     5.70371391E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     8.22258772E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.18384468E-03    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.15135665E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.84578273E-04    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     5.58807098E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.41697484E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.43452059E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.82257240E-03    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.19122941E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     9.39049434E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.05390182E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.50154239E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.08728170E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.13429780E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.76878110E-02    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.05390182E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.50154239E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.08728170E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.13429780E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.76878110E-02    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     1.98398642E+03   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.21438365E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.77456637E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.89666754E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.42284318E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
     8.97456382E-08    2    -1000015       -37   # BR(~nu_tauL -> ~tau_1+ H-)
     1.83627776E-10    2    -2000015       -37   # BR(~nu_tauL -> ~tau_2+ H-)
     2.29068548E-02    2    -1000015       -24   # BR(~nu_tauL -> ~tau_1+ W-)
     9.40785134E-01    2    -2000015       -24   # BR(~nu_tauL -> ~tau_2+ W-)
#
#         PDG            Width
DECAY   1000024     5.85945658E+03   # chargino1+ decays
#          BR         NDA      ID1       ID2
     9.99880512E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     9.13820908E-05    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
     2.81056724E-05    2     1000023        37   # BR(~chi_1+ -> ~chi_20  H+)
#
#         PDG            Width
DECAY   1000037     1.41352477E+03   # chargino2+ decays
#          BR         NDA      ID1       ID2
     7.21913312E-03    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.88530745E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.23392375E-03    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.30835551E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.07842582E-04    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     5.34851992E-11   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.63523517E-05    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     4.48684088E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     4.48684088E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     4.48684088E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     4.48684088E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.14799463E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     1.47454271E-01    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.47454271E-01    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.40859469E-01    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.31887906E-01    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.31887906E-01    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.94862429E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     2.69680063E+03   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.10561628E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.10561628E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     4.36961559E-01    2     1000037       -24   # BR(~chi_30 -> ~chi_2+   W-)
     4.36961559E-01    2    -1000037        24   # BR(~chi_30 -> ~chi_2-   W+)
     2.87838343E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.68348336E-05    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.86427960E-05    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     9.31424030E-04    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     1.19220470E-05    2     1000023        35   # BR(~chi_30 -> ~chi_20   H )
     5.46047818E-06    2     1000023        36   # BR(~chi_30 -> ~chi_20   A )
     3.89986919E-05    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     3.89986919E-05    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     1.94579916E-06    2     1000037       -37   # BR(~chi_30 -> ~chi_2+   H-)
     1.94579916E-06    2    -1000037        37   # BR(~chi_30 -> ~chi_2-   H+)
#
#         PDG            Width
DECAY   1000035     1.93285279E+04   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.10942945E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.10942945E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.87649529E-01    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     3.87649529E-01    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     6.78826225E-06    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.08514512E-04    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.63318951E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.18057630E-06    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.63371534E-04    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.08496953E-04    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     6.96111692E-04    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     6.96111692E-04    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     1.35079125E-04    2     1000037       -37   # BR(~chi_40 -> ~chi_2+   H-)
     1.35079125E-04    2    -1000037        37   # BR(~chi_40 -> ~chi_2-   H+)
