include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Dibosons decaying semileptonically to mumuqq, merged with up to three additional QCD jets in ME+PS." 
evgenConfig.keywords = [ "EW", "Diboson" ]
evgenConfig.inputconfcheck = "VVtomumuqq"
evgenConfig.minevents = 5000
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch" ]

evgenConfig.process="""
(run){
  ACTIVE[6]=0
  ACTIVE[25]=0
  PARTICLE_CONTAINER 9923[m:-1] Zgamma 23 22;
}(run)

(processes){
  Process 93 93 -> 9923[a] 9923[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.05 {5,6,7}
  End process;

  Process 93 93 -> 9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.05 {5,6,7}
  End process;

  Process 93 93 -> 9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.05 {5,6,7}
  End process;
}(processes)

(selector){
  DecayMass 9923 7.0 E_CMS
}(selector)
"""
