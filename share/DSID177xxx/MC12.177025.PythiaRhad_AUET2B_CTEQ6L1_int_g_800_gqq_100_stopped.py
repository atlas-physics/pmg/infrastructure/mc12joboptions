MODEL = 'intermediate'
CASE = 'gluino'
MASS = 800
MASSX = 100
DECAY = 'false'
STOPPED = 'true'

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " intermediate gluino 800GeV g/qq 100GeV stopped"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};DECAY={decay};MASSX={massx};CHARGE=999;preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS, decay = DECAY, massx=MASSX)
