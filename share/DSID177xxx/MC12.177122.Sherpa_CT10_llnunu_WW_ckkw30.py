include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa 1.4.0 diboson llnunu, exclusive WW only, high mass sample, up to 3 jets with ME+PS, merging cut variation"
evgenConfig.keywords = ["diboson", "llnunu", "llnn", "WW", "ckkw30"]
evgenConfig.contact  = ["frank.siegert@cern.ch", "christian.schillo@cern.ch"]

evgenConfig.process="""
(run){
  ACTIVE[6]=0
  ACTIVE[25]=0
  ERROR=0.02
  MASSIVE[11]=1
  MASSIVE[13]=1
  MASSIVE[15]=1
  PARTICLE_CONTAINER 991[m:-1] leptons 11 -11 13 -13 15 -15;
}(run)

(processes){
  Process 93 93 -> 24[a] -24[b] 93{2};
  Decay 24[a] -> -11 91
  Decay -24[b] -> 11 91
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;
  
  Process 93 93 -> 24[a] -24[b] 93{2};
  Decay 24[a] -> -13 91
  Decay -24[b] -> 13 91
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;
    
  Process 93 93 -> 24[a] -24[b] 93{2};
  Decay 24[a] -> -15 91
  Decay -24[b] -> 15 91
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;

  Process 93 93 -> 24[a] -24[b] 93{2};
  Decay 24[a] -> -11 91
  Decay -24[b] -> 13 91
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;

  Process 93 93 -> 24[a] -24[b] 93{2};
  Decay 24[a] -> -13 91
  Decay -24[b] -> 11 91
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;

  Process 93 93 -> 24[a] -24[b] 93{2};
  Decay 24[a] -> -11 91
  Decay -24[b] -> 15 91
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;

  Process 93 93 -> 24[a] -24[b] 93{2};
  Decay 24[a] -> -15 91
  Decay -24[b] -> 11 91
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;

  Process 93 93 -> 24[a] -24[b] 93{2};
  Decay 24[a] -> -13 91
  Decay -24[b] -> 15 91
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;

  Process 93 93 -> 24[a] -24[b] 93{2};
  Decay 24[a] -> -15 91
  Decay -24[b] -> 13 91
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;
}(processes)

(selector){
  Mass  11 -11  4.0  E_CMS
  Mass  13 -13  4.0  E_CMS
  Mass  15 -15  4.0  E_CMS
  "PT" 991 5.0,E_CMS:5.0,E_CMS [PT_UP]
}(selector)
"""
