# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_sq_bino_1300_900.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['squarks', 'sbottoms', 'stops'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GGM squark-bino grid generation with msq=1300 mbino=900'
evgenConfig.keywords = ['SUSY', 'GGM', 'squark', 'bino']
evgenConfig.contact = [ 'helenka.przysiezniak@cern.ch']

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds
