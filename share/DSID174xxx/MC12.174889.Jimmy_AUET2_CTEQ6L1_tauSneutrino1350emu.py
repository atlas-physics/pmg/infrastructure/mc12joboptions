include ( "MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py" )
evgenConfig.saveJets = True

topAlg.Herwig.HerwigCommand += [ "iproc 14080",
                          "susyfile susy_RPVSneut_M_1350_emu_isawig.txt", 
                          "taudec TAUOLA",
                          "effmin 0.0001" ]

# ... Tauola
include ( "MC12JobOptions/Jimmy_Tauola.py" )

# ... Photos
include ( "MC12JobOptions/Jimmy_Photos.py" )

evgenConfig.description = 'Sneutrino simplified model with lambda_233 R-parity violation and mass 1350' 
evgenConfig.keywords = ['SUSY', 'RPV', 'Lambda233', 'Sneutrino']
evgenConfig.contact  = ['kzengel@cern.ch']
evgenConfig.auxfiles += ['susy_RPVSneut_M_1350_emu_isawig.txt']
