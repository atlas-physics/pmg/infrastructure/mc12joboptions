evgenConfig.description = "POWHEG+Pythia6 ttbar production with a 1 MeV lepton filter and AUET2B CT10 tune and facsc 0.5"
evgenConfig.keywords = ["top", "ttbar", "leptonic"]
evgenConfig.contact  = ["mark.hohlfeld@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar'

include("MC12JobOptions/PowhegPythia_AUET2B_CT10_Common.py")

##To fix in MC13: "Lhef" -> "Powheg"; don't use Tauola.
evgenConfig.generators += [ "Powheg"]
include("MC12JobOptions/Pythia_Tauola.py")

include("MC12JobOptions/Pythia_Photos.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")
##
