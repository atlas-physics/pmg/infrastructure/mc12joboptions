#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@cern.ch           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.3b                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.39156850E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1     SOFTSUSY    # spectrum calculator               
     2     3.4.0       # version number                    
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27908970E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16637870E-05   # G_F [GeV^-2]
         3     1.18400000E-01   # alpha_S(M_Z)^MSbar
         4     9.11876000E+01   # M_Z pole mass
         5     4.18000000E+00   # mb(mb)^MSbar
         6     1.73200000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     4.38500000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.41040000E+03   # M_1(MX)             
         2     4.20230000E+02   # M_2(MX)             
         3     1.06763000E+03   # M_3(MX)             
        11    -4.15504000E+03   # At(MX)              
        12     2.55770000E+02   # Ab(MX)              
        13     1.02876000E+03   # Atau(MX)            
        23     1.52400000E+03   # mu(MX)              
        26     3.77985000E+03   # mA(pole)            
        31     3.02033000E+03   # meL(MX)             
        32     3.02033000E+03   # mmuL(MX)            
        33     7.86590000E+02   # mtauL(MX)           
        34     3.16135000E+03   # meR(MX)             
        35     3.16135000E+03   # mmuR(MX)            
        36     2.38023000E+03   # mtauR(MX)           
        41     2.26647000E+03   # mqL1(MX)            
        42     2.26647000E+03   # mqL2(MX)            
        43     3.99884000E+03   # mqL3(MX)            
        44     3.31667000E+03   # muR(MX)             
        45     3.31667000E+03   # mcR(MX)             
        46     3.83269000E+03   # mtR(MX)             
        47     2.05343000E+03   # mdR(MX)             
        48     2.05343000E+03   # msR(MX)             
        49     6.96960000E+02   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03728334E+01   # W+
        25     1.27135476E+02   # h
        35     3.77984828E+03   # H
        36     3.77985000E+03   # A
        37     3.78077256E+03   # H+
         5     4.81876030E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.31380500E+03   # ~d_L
   2000001     2.10121448E+03   # ~d_R
   1000002     2.31257212E+03   # ~u_L
   2000002     3.35526081E+03   # ~u_R
   1000003     2.31380500E+03   # ~s_L
   2000003     2.10121448E+03   # ~s_R
   1000004     2.31257212E+03   # ~c_L
   2000004     3.35526081E+03   # ~c_R
   1000005     7.80502337E+02   # ~b_1
   2000005     3.99684836E+03   # ~b_2
   1000006     3.77418281E+03   # ~t_1
   2000006     4.02329895E+03   # ~t_2
   1000011     3.02591267E+03   # ~e_L
   2000011     3.17518793E+03   # ~e_R
   1000012     3.02454109E+03   # ~nu_eL
   1000013     3.02591267E+03   # ~mu_L
   2000013     3.17518793E+03   # ~mu_R
   1000014     3.02454109E+03   # ~nu_muL
   1000015     7.70714358E+02   # ~tau_1
   2000015     2.38162482E+03   # ~tau_2
   1000016     7.68280004E+02   # ~nu_tauL
   1000021     1.27299298E+03   # ~g
   1000022     4.44943752E+02   # ~chi_10
   1000023     1.56760568E+03   # ~chi_20
   1000025    -1.56785044E+03   # ~chi_30
   1000035     2.37707506E+03   # ~chi_40
   1000024     4.45111157E+02   # ~chi_1+
   1000037     1.57073687E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -4.12223928E-04   # N_11
  1  2     9.98381729E-01   # N_12
  1  3    -5.44354492E-02   # N_13
  1  4     1.64479756E-02   # N_14
  2  1     3.89342136E-02   # N_21
  2  2     5.01055951E-02   # N_22
  2  3     7.05348042E-01   # N_23
  2  4    -7.06015365E-01   # N_24
  3  1    -7.71191095E-03   # N_31
  3  2     2.68621665E-02   # N_32
  3  3     7.06423766E-01   # N_33
  3  4     7.07237169E-01   # N_34
  4  1     9.99211931E-01   # N_41
  4  2    -1.33315709E-03   # N_42
  4  3    -2.20541139E-02   # N_43
  4  4     3.29750700E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.97041839E-01   # U_11
  1  2    -7.68607227E-02   # U_12
  2  1     7.68607227E-02   # U_21
  2  2     9.97041839E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99730339E-01   # V_11
  1  2    -2.32217376E-02   # V_12
  2  1     2.32217376E-02   # V_21
  2  2     9.99730339E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     3.17503013E-01   # cos(theta_t)
  1  2     9.48257263E-01   # sin(theta_t)
  2  1    -9.48257263E-01   # -sin(theta_t)
  2  2     3.17503013E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     8.58213813E-03   # cos(theta_b)
  1  2     9.99963173E-01   # sin(theta_b)
  2  1    -9.99963173E-01   # -sin(theta_b)
  2  2     8.58213813E-03   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     9.99708917E-01   # cos(theta_tau)
  1  2     2.41263605E-02   # sin(theta_tau)
  2  1    -2.41263605E-02   # -sin(theta_tau)
  2  2     9.99708917E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.34391728E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  3.91568500E+03  # DRbar Higgs Parameters
         1     1.55685244E+03   # mu(Q)MSSM           
         2     4.27140209E+01   # tan                 
         3     2.41845775E+02   # higgs               
         4     1.55240087E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  3.91568500E+03  # The gauge couplings
     1     3.65773002E-01   # gprime(Q) DRbar
     2     6.40551366E-01   # g(Q) DRbar
     3     1.02214080E+00   # g3(Q) DRbar
#
BLOCK AU Q=  3.91568500E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3    -4.15503999E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  3.91568500E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     2.55770006E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  3.91568500E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     1.02876000E+03   # A_tau(Q) DRbar
#
BLOCK Yu Q=  3.91568500E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.10968088E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  3.91568500E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.95971308E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  3.91568500E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     4.63990218E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  3.91568500E+03  # The soft SUSY breaking masses at the scale Q
         1     2.41040000E+03   # M_1(Q)              
         2     4.20230000E+02   # M_2(Q)              
         3     1.06763000E+03   # M_3(Q)              
        21     1.20733448E+07   # mH1^2(Q)            
        22    -2.10012537E+06   # mH2^2(Q)            
        31     3.02033000E+03   # meL(Q)              
        32     3.02033000E+03   # mmuL(Q)             
        33     7.86590000E+02   # mtauL(Q)            
        34     3.16135000E+03   # meR(Q)              
        35     3.16135000E+03   # mmuR(Q)             
        36     2.38023000E+03   # mtauR(Q)            
        41     2.26647000E+03   # mqL1(Q)             
        42     2.26647000E+03   # mqL2(Q)             
        43     3.99883999E+03   # mqL3(Q)             
        44     3.31667000E+03   # muR(Q)              
        45     3.31667000E+03   # mcR(Q)              
        46     3.83268999E+03   # mtR(Q)              
        47     2.05343000E+03   # mdR(Q)              
        48     2.05343000E+03   # msR(Q)              
        49     6.96959984E+02   # mbR(Q)              
#
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP   IP1 IP2 IP2
    1.00272977E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.82448485E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.82448758E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99998504E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    5.49595285E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    5.48099099E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.00272977E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.82448485E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.82448758E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999999E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.01967114E-09        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999999E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.01967114E-09        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    8.85387125E-01        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.76877679E-13        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    5.93523378E-13        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.01967114E-09        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999999E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.44580324E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.18880414E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.00000000E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     2.40140460E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.38855514E-03    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     6.68322935E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.30177832E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.20664582E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.49174797E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.29985678E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.94872021E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     9.19731148E-04    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
#
#         PDG            Width
DECAY   2000006     2.81171905E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.98846968E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     6.96024549E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.45412220E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.61855410E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.00768859E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.01009512E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.27639845E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     6.70258657E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.51411308E-03    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     8.62671765E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.72748215E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.90404756E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.09595244E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     2.84993874E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.57922901E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.45415284E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.45674583E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.97346615E-04    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.10479879E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.31684623E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.27605906E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     4.95626903E-03    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.77016043E-03    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.47045392E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     1.08399109E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.75659850E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     8.19932681E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.59748683E-05    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.55566951E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.65515289E-05    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     7.66742545E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.83838933E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     6.85828970E-09    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.88059052E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.52224159E-06    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.04456322E-02    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.89514033E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.08473444E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     7.75856321E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.52538654E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.99147381E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.54721922E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.91358275E-04    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     7.67335919E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     6.22814663E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     2.97641118E-09    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.82041738E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.28180997E-07    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.99993948E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     8.84995021E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.85650369E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.03071990E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.00815448E-05    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.97682823E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.33748674E-05    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     7.03595611E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.38259152E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.41648943E-09    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.31856395E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.08631478E-06    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.42216085E-02    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.85723110E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     8.85412297E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     9.86102782E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.69010214E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.50401173E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.96649163E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.66316955E-04    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     7.04292301E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     4.66604924E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.12933653E-09    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.93856637E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.11216575E-07    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.99991746E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59831256E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.27316049E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.61310016E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.26379117E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.63034220E-02    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.53162405E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.16417639E-03    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     3.27554252E+00   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.42771806E-07    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.47372783E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.75486344E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.95349943E-01    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.59831255E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.27316048E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.61310013E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.26379114E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.63034218E-02    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.53162406E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.16417640E-03    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     3.27554248E+00   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.42771814E-07    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.47372786E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.75486345E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.95349943E-01    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.18336705E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.34580974E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     6.65419026E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.38151646E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     6.57638107E-04    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.18648619E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.18550022E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.23674469E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.30902853E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.35401173E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
     2.57411476E-01    2     1000016       -24   # BR(~tau_2 -> ~nu_tauL W-)
     1.35969631E-01    2     1000015        25   # BR(~tau_2 -> ~tau_1   h)
     1.32040045E-01    2     1000015        23   # BR(~tau_2 -> ~tau_1   Z)
#
#         PDG            Width
DECAY   1000012     3.60100801E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.27218132E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.42457975E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.79208865E-04    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.63887041E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.55874318E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.97178865E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.60100801E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.27218133E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.42457975E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.79208865E-04    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.63887042E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.55874317E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.97178865E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.15475237E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.32424454E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.67575546E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     3.96514880E-15   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     1.93859510E-02    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.40266430E-03    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.78211380E-01    2     1000022       211             # BR(~chi_1+ -> ~chi_10 pi+)
#
#         PDG            Width
DECAY   1000037     1.77907161E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.63637941E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.08872010E-01    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     7.33254135E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.76122346E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.71361183E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.79273266E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.77077997E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.97405944E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.72231926E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     1.72231926E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     1.40503040E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.82915496E-01    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
     1.82915496E-01    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
     5.46497356E-02    2     1000015       -15   # BR(~chi_20 -> ~tau_1-   tau+)
     5.46497356E-02    2    -1000015        15   # BR(~chi_20 -> ~tau_1+   tau-)
     8.10257050E-05    2     1000016       -16   # BR(~chi_20 -> ~nu_tau1  nu_taub)
     8.10257050E-05    2    -1000016        16   # BR(~chi_20 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000025     1.76482757E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.37645431E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.72621623E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     1.72621623E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     3.98589121E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.83982497E-01    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     1.83982497E-01    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.45413767E-02    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.45413767E-02    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     1.02331897E-04    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.02331897E-04    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     1.37626070E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.23837720E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.48217882E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.50955899E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     3.56217691E-04    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.56217691E-04    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.59253834E-02    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     8.59253834E-02    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     2.58948460E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.27257104E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.44627699E-03    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.14532393E-04    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.14532393E-04    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.13483454E-04    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     1.13483454E-04    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     7.32085821E-03    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     7.32085821E-03    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.14500042E-04    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.14500042E-04    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.13483454E-04    2     1000003        -3   # BR(~chi_40 -> ~s _L      sb)
     1.13483454E-04    2    -1000003         3   # BR(~chi_40 -> ~s _L*     s )
     7.32085821E-03    2     2000003        -3   # BR(~chi_40 -> ~s _R      sb)
     7.32085821E-03    2    -2000003         3   # BR(~chi_40 -> ~s _R*     s )
     1.22006767E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.22006767E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     9.15890761E-02    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     9.15890761E-02    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     9.24585687E-02    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     9.24585687E-02    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     4.85694707E-03   # h decays
#          BR         NDA      ID1       ID2
     6.08291627E-01    2           5        -5   # BR(h -> b       bb     )
     5.43264581E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.92305254E-04    2         -13        13   # BR(h -> mu+     mu-    )
     2.09135014E-04    2           3        -3   # BR(h -> s       sb     )
     1.81014049E-02    2           4        -4   # BR(h -> c       cb     )
     6.89494587E-02    2          21        21   # BR(h -> g       g      )
     2.08524554E-03    2          22        22   # BR(h -> gam     gam    )
     1.54847624E-03    2          22        23   # BR(h -> Z       gam    )
     2.18240184E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80557052E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.16044616E+02   # H decays
#          BR         NDA      ID1       ID2
     6.07842091E-01    2           5        -5   # BR(H -> b       bb     )
     1.11738430E-01    2         -15        15   # BR(H -> tau+    tau-   )
     4.22978001E-04    2         -13        13   # BR(H -> mu+     mu-    )
     1.82727151E-04    2           3        -3   # BR(H -> s       sb     )
     7.12696334E-09    2           4        -4   # BR(H -> c       cb     )
     7.03206338E-04    2           6        -6   # BR(H -> t       tb     )
     8.15431203E-06    2          21        21   # BR(H -> g       g      )
     8.22698121E-09    2          22        22   # BR(H -> gam     gam    )
     4.74661526E-09    2          23        22   # BR(H -> Z       gam    )
     1.52725972E-07    2          24       -24   # BR(H -> W+      W-     )
     7.62306408E-08    2          23        23   # BR(H -> Z       Z      )
     6.38381695E-07    2          25        25   # BR(H -> h       h      )
     1.47508088E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     2.16426604E-05    2     1000037  -1000037   # BR(H -> ~chi_2+ ~chi_2-)
     9.06918530E-02    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     9.06918530E-02    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     7.38247095E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.95138144E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.41345486E-05    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     3.76433944E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.27705970E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.10597618E-06    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.39161280E-04    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     5.94465903E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
     1.06845482E-05    2     1000015  -1000015   # BR(H -> ~tau_1- ~tau_1+)
     2.43627960E-03    2     1000015  -2000015   # BR(H -> ~tau_1- ~tau_2+)
     2.43627960E-03    2     2000015  -1000015   # BR(H -> ~tau_2- ~tau_1+)
     1.03643199E-07    2     1000016  -1000016   # BR(H -> ~nu_tauL ~nu_tauL*)
#
#         PDG            Width
DECAY        36     1.16047516E+02   # A decays
#          BR         NDA      ID1       ID2
     6.07839963E-01    2           5        -5   # BR(A -> b       bb     )
     1.11735963E-01    2         -15        15   # BR(A -> tau+    tau-   )
     4.22968266E-04    2         -13        13   # BR(A -> mu+     mu-    )
     1.82730004E-04    2           3        -3   # BR(A -> s       sb     )
     7.17344216E-09    2           4        -4   # BR(A -> c       cb     )
     7.05009789E-04    2           6        -6   # BR(A -> t       tb     )
     1.49368717E-05    2          21        21   # BR(A -> g       g      )
     3.35811702E-08    2          22        22   # BR(A -> gam     gam    )
     1.15591457E-08    2          23        22   # BR(A -> Z       gam    )
     1.54916190E-07    2          23        25   # BR(A -> Z       h      )
     1.60639593E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     9.55724216E-05    2     1000037  -1000037   # BR(A -> ~chi_2+ ~chi_2-)
     9.05810767E-02    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     9.05810767E-02    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     8.03995355E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.86933310E-05    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.04717428E-05    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     5.26210915E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.77405295E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.56694470E-06    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     4.35185851E-05    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     2.44161667E-03    2     1000015  -2000015   # BR(A -> ~tau_1- ~tau_2+)
     2.44161667E-03    2    -1000015   2000015   # BR(A -> ~tau_1+ ~tau_2-)
#
#         PDG            Width
DECAY        37     1.01351239E+02   # H+ decays
#          BR         NDA      ID1       ID2
     9.25604657E-04    2           4        -5   # BR(H+ -> c       bb     )
     1.27969287E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     4.84418321E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.25603950E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.04720985E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.06130584E-04    2           4        -3   # BR(H+ -> c       sb     )
     5.50981717E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.77579336E-07    2          24        25   # BR(H+ -> W+      h      )
     8.67706105E-15    2          24        36   # BR(H+ -> W+      A      )
     1.97409448E-10    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.04682339E-01    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.04211362E-01    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     5.12016922E-06    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     1.04736001E-01    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.68764977E-06    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.72820327E-04    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.97617393E-06    2    -1000015   1000016   # BR(H+ -> ~tau_1+ ~nu_tauL)
     5.59863038E-03    2    -2000015   1000016   # BR(H+ -> ~tau_2+ ~nu_tauL)
