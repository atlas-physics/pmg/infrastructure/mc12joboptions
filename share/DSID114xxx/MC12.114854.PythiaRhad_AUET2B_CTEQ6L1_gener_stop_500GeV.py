MODEL = 'generic'
CASE = 'stop'
MASS = 500

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " stable generic stop 500GeV"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS)
