MODEL = 'generic'
CASE = 'stop'
MASS = 200

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " stable generic stop 200GeV"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS)
