evgenConfig.description = "ZH Z->ll H->bb with UEEE3, CTEQ6L1"
evgenConfig.keywords = ["SMHiggs", "bottom","Z","leptonic"]

include ( "MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_CT10ME_Common.py" )

## Add to commands
cmds = """
## Set up Powheg truncated shower
set /Herwig/Shower/Evolver:HardEmissionMode POWHEG
## Use 2-loop alpha_s
create Herwig::O2AlphaS /Herwig/AlphaQCD_O2
set /Herwig/Generators/LHCGenerator:StandardModelParameters:QCD/RunningAlphaS /Herwig/AlphaQCD_O2

## Set up Higgs + Z process at NLO (set jet pT cut to zero so no cut on Z decay products)
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/PowhegMEPP2ZH
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:FactorizationScaleOption Dynamic
set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV

## Higgs particle
set /Herwig/Particles/h0:NominalMass 150*GeV
set /Herwig/Particles/h0:Width 0.0173*GeV
set /Herwig/Particles/h0:WidthCut 0.0173*GeV
set /Herwig/Particles/h0:WidthLoCut 0.0173*GeV
set /Herwig/Particles/h0:WidthUpCut 0.0173*GeV
set /Herwig/Particles/h0/h0->W+,W-;:OnOff 0
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff 0
set /Herwig/Particles/h0/h0->b,bbar;:OnOff 1
set /Herwig/Particles/h0/h0->c,cbar;:OnOff 0
set /Herwig/Particles/h0/h0->g,g;:OnOff 0
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff 0
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff 0
set /Herwig/Particles/h0/h0->t,tbar;:OnOff 0
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff 0

## Z boson
set /Herwig/Particles/Z0/Z0->b,bbar;:OnOff 0
set /Herwig/Particles/Z0/Z0->c,cbar;:OnOff 0
set /Herwig/Particles/Z0/Z0->d,dbar;:OnOff 0
set /Herwig/Particles/Z0/Z0->e-,e+;:OnOff 1
set /Herwig/Particles/Z0/Z0->mu-,mu+;:OnOff 1
set /Herwig/Particles/Z0/Z0->nu_e,nu_ebar;:OnOff 0
set /Herwig/Particles/Z0/Z0->nu_mu,nu_mubar;:OnOff 0
set /Herwig/Particles/Z0/Z0->nu_tau,nu_taubar;:OnOff 0
set /Herwig/Particles/Z0/Z0->s,sbar;:OnOff 0
set /Herwig/Particles/Z0/Z0->tau-,tau+;:OnOff 1
set /Herwig/Particles/Z0/Z0->u,ubar;:OnOff 0

"""

## Set commands
topAlg.Herwigpp.Commands += cmds.splitlines()

