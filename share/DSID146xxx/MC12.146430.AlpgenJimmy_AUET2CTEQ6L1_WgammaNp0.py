evgenConfig.description = 'ALPGEN+Herwig Wgamma 0p with 8 GeV gamma cut and AUET2 CTEQ6L1 tune'
evgenConfig.keywords = ['EW', 'W', 'gamma','leptonic']
evgenConfig.inputfilecheck = 'alpgen.*WgammaNp0_pt20'
evgenConfig.contact = ['zhijun.liang@cern.ch','biagio.di.micco@cern.ch']
evgenConfig.process = 'alpgen_process'
evgenConfig.minevents = 5000

include("MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py")
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
