evgenConfig.description = "POWHEG+PYTHIA8 MSSM ggH H->tautau->mumu with AU2,CT10"
evgenConfig.keywords = ["nonSMhiggs", "ggF", "mu","leptonic"]
evgenConfig.inputfilecheck = "ggH_MA300TB60"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 13 13'
                           ]
