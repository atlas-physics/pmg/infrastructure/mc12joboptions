include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Gamma + up to 4 jets with ME+PS and narrow jets (pt>35,20). Slice in photon pT>15.0 GeV."
evgenConfig.keywords = [ ]
evgenConfig.contact  = [ "Junichi.Tanaka@cern.ch","Tatsuya.Masubuchi@cern.ch","frank.siegert@cern.ch" ]
evgenConfig.minevents = 50

"""
(run){
  QCUT:=30.0
}(run)

(processes){
  Process 93 93 -> 22 93 93{3}
  Order_EW 1
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
  Integration_Error 0.1 {5,6};
  End process
}(processes)

(selector){
  PT  22  15.0  E_CMS
  DeltaR  22  93  0.3  20.0
}(selector)
"""

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import JetFilter
topAlg += JetFilter("Jet1Filter")
topAlg += JetFilter("Jet2Filter")

Jet1Filter = topAlg.Jet1Filter
Jet1Filter.JetNumber = 2
Jet1Filter.EtaRange = 2.7
Jet1Filter.JetThreshold = 20000.;  # 20 GeV
Jet1Filter.JetType = False; # true is a cone, false is a grid
Jet1Filter.GridSizeEta = 2; # sets the number of (approx 0.06 size) eta
Jet1Filter.GridSizePhi = 2; # sets the number of (approx 0.06 size) phi cells

Jet2Filter = topAlg.Jet2Filter
Jet2Filter.JetNumber = 1
Jet2Filter.EtaRange = 2.7
Jet2Filter.JetThreshold = 35000.;  # 35 GeV
Jet2Filter.JetType = False; # true is a cone, false is a grid
Jet2Filter.GridSizeEta = 2; # sets the number of (approx 0.06 size) eta
Jet2Filter.GridSizePhi = 2; # sets the number of (approx 0.06 size) phi cells

#--------------------------------------------------------------
# Pool Output Options
#--------------------------------------------------------------
StreamEVGEN.RequireAlgs += ["Jet1Filter"]
StreamEVGEN.RequireAlgs += ["Jet2Filter"]
