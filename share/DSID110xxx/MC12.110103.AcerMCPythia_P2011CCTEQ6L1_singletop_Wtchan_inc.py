evgenConfig.description = "AcerMCPythia6 singletop-Wtchan-incl production with P2011C CTEQ6L1 tune"
evgenConfig.generators = ["AcerMC", "Pythia"]
evgenConfig.keywords = ["top", "singletop", "Wtchan", "inclusive"]
evgenConfig.contact  = ["liza.mijovic@cern.ch"]
evgenConfig.inputfilecheck = "singletop_Wtchan_inc"

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")
topAlg.Pythia.PythiaCommand += ["pyinit user acermc"]
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
