evgenConfig.description = "Powheg+fHerwig/Jimmy ttbar production with a two lepton filter and AUET2 CT10 tune"
evgenConfig.keywords = ["top", "ttbar", "dileptonic"]
evgenConfig.contact  = ["christoph.wasicki@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar' 

include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")
evgenConfig.generators += [ "Lhef"]

include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons=2 #require two leptons
