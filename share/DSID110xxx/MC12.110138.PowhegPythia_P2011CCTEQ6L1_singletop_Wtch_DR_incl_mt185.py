evgenConfig.description = "POWHEG+Pythia6 Wt inclusive production with PythiaPerugia2011C tune"
evgenConfig.keywords = ["top", "Wt", "inclusive", "DR"]
evgenConfig.contact  = ["huaqiao.zhang@cern.ch"]
evgenConfig.inputfilecheck = 'singletop_Wtch_DR_incl_mt185'

include("MC12JobOptions/PowhegPythia_Perugia2011C_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
