#--------------------------------------------------------------
# Powheg_ttbar setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_tt_Common.py')

PowhegConfig.nEvents = 15000
PowhegConfig.hdamp   = 167.5
PowhegConfig.mass_t  = 167.5
PowhegConfig.width_t = 1.189
PowhegConfig.PDF     = 10800

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
include('MC12JobOptions/PowhegPythia_Perugia2012_Common.py')
include('MC12JobOptions/Pythia_Tauola.py')
include('MC12JobOptions/Pythia_Photos.py')

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
# Lepton filter (-1: non-allhadronic events, 2 dileptonic, ...)
include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons=0

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with 10800, hdamp=167.5, mtop=167.5, and P2011C tune'
evgenConfig.keywords    = [ 'top' , 'allhad']
evgenConfig.contact     = [ 'aknue@cern.ch']
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.minevents   = 5000
