evgenConfig.description = 'MG+Pythia6  single top + Z production with Perugia 2012 tune'
evgenConfig.keywords = [ 'SM','singleTop','tZ','lepton']
evgenConfig.contact  = ["alhroobl@cern.ch"]
evgenConfig.inputfilecheck = "tZ_tchannel_LO_noAllHad_CTEQ6L1_8TeV"

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
include("MC12JobOptions/MadGraphPythia_Perugia2012_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
