evgenConfig.description = "ttbar FCNC production ttbar->bW(->lnu)Hc with Protos+Pythia, with P2011C CTEQ6L1 tune"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["topproperties","top","Higgs","FCNC"]
evgenConfig.contact  = ["Sergey.Burdin@cern.ch","clement.helsens@cern.ch"]
evgenConfig.inputfilecheck = "TTbar_tWb_tHc"

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")
topAlg.Pythia.PythiaCommand += ["pyinit user protos"]
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
