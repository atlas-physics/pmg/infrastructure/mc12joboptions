evgenConfig.description = "POWHEG+Pythia6 s-channel Wlep production with PythiaPerugia2011C tune"
evgenConfig.keywords = ["top", "s-channel", "Wlep"]
evgenConfig.contact  = ["cunfeng.feng@cern.ch"]
evgenConfig.inputfilecheck = 'singletop_sch_wlep_mt185'

include("MC12JobOptions/PowhegPythia_Perugia2011C_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
