evgenConfig.description = "MadGraph5+Pythia8 for qqbar > H+ -> tb (mH=800 GeV)"
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.keywords = ["ChargedHiggs"]
evgenConfig.contact  = ["lironbarak83@gmail.com"]
evgenConfig.inputfilecheck = "ChargedH_Schannel_2HDM_M800"

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
