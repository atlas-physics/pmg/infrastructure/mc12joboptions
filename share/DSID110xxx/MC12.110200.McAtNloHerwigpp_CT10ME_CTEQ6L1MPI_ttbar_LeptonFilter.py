evgenConfig.description = "McAtNlo+Herwig++ ttbar production with TTbarWToLeptonFilter - CT10 PDF, EE3 tune"
evgenConfig.generators = ["McAtNlo", "Herwigpp"]
evgenConfig.keywords = ["top", "ttbar", "leptonic"]
evgenConfig.contact  = ["orel.gueta@cern.ch"]
evgenConfig.inputfilecheck = "ttbar"

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_CT10ME_LHEF_Common.py")
include("MC12JobOptions/TTbarWToLeptonFilter.py")
