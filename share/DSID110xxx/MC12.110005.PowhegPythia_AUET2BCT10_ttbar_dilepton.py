evgenConfig.description = "Powheg+Pythia6 ttbar production with a two lepton filter and AUET2B CT10 tune"
evgenConfig.keywords = ["top", "ttbar", "dileptonic"]
evgenConfig.contact  = ["christoph.wasicki@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar'

include("MC12JobOptions/PowhegPythia_AUET2B_CT10_Common.py")
evgenConfig.generators += [ "Lhef"]

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons=2 #require two leptons

