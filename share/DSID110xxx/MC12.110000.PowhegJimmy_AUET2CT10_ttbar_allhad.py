evgenConfig.description = 'POWHEG+fHerwig/Jimmy ttbar production with an allhadronic filter and AUET2 CT10 tune'
evgenConfig.keywords = ['top', 'ttbar', 'allhad']
evgenConfig.contact  = ['christoph.wasicki@cern.ch', 'alexander.grohsjean@desy.de']
evgenConfig.inputfilecheck = 'ttbar' 

include('MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py')

evgenConfig.generators += [ 'Powheg' ]
include('MC12JobOptions/Jimmy_Tauola.py')

include('MC12JobOptions/Jimmy_Photos.py')
include('MC12JobOptions/TTbarWToLeptonFilter.py')
topAlg.TTbarWToLeptonFilter.NumLeptons=0 

