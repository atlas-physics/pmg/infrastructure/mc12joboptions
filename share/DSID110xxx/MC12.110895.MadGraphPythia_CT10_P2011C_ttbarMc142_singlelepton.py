include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand +=  [
   "pyinit user madgraph",
   "pypars mstp 143 1",
   "pydat2 pmas 4 1 1.42"
   ]

include ( "MC12JobOptions/Pythia_Photos.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/TTbarWToLeptonFilter.py" )

evgenConfig.generators += ["MadGraph"]

evgenConfig.description = "MadGraph(CT10) Pythia Perugia 2011c ttbar McPS142 up to 3 additional partons"
evgenConfig.keywords = ["top", "ttbar", "singlelepton" ]
evgenConfig.inputfilecheck = 'madgraph.110870.ttbar_0to3inc_nodecay'
evgenConfig.contact  = ["alexander.grohsjean@desy.de"]
evgenConfig.minevents=5000

topAlg.TTbarWToLeptonFilter.NumLeptons = 1
topAlg.TTbarWToLeptonFilter.Ptcut = 1.
   
#### Add matching parameters ####  
phojf=open('./pythia_card.dat', 'w')
phojinp = """
!...Matching parameters...
IEXCFILE=0
qcut=40
"""
phojf.write(phojinp)
phojf.close()
