include("MC12JobOptions/PowhegControl_preInclude.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Jimmy ttbar production with AUET2 tune,at least one lepton filter"
evgenConfig.keywords    = [ "top", "ttbar", "nonallhad" ]
evgenConfig.contact     = [ "james.robinson@cern.ch", "alexander.grohsjean@desy.de" ]

postGenerator="JimmyTauolaPhotos"
process="tt_all"
postGeneratorTune = "AUET2_CT10"

# compensate filter efficiency
evt_multiplier = 3.

include("MC12JobOptions/PowhegControl_postInclude.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons = -1
topAlg.TTbarWToLeptonFilter.Ptcut = 0.
