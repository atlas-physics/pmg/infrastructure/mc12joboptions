evgenConfig.description = "ALPGEN+Pythia W(->lnu)+bb+2jets process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "W"]
evgenConfig.inputfilecheck = "WbbNp2"
evgenConfig.minevents = 5000

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )
