evgenConfig.description = "aMC@NLO(using Madgraph5 ver 2.0.0b3)+fHerwig/Jimmy t-channel single top production and AUET2 CT10 tune"
evgenConfig.generators = ["aMcAtNlo","Herwig"]
evgenConfig.keywords = ["top", "singletop", "tchan", "leptonic"]
evgenConfig.contact  = ["dominic.hirschbuehl@cern.ch"]
evgenConfig.inputfilecheck = "singletop_tchan2to3nlo_lept"

include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand+=[ "iproc lhef" ]

include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
