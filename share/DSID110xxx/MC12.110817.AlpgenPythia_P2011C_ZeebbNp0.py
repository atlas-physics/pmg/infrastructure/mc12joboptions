evgenConfig.description = "ALPGEN+Pythia Z(->ee)+bb process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "Z"]
evgenConfig.inputfilecheck = "ZeebbNp0"
evgenConfig.minevents = 5000

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )
