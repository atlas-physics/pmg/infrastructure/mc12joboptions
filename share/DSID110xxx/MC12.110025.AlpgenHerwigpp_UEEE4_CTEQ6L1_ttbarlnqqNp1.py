evgenConfig.description = "Alpgen+Herwig++ ttbar semi leptonic + 1 jets with UEEE4 tune and CTEQ6L1 PDF"
evgenConfig.keywords = ["ttbar","semileptonic"]
evgenConfig.inputfilecheck = "ttbarlnqqNp1"
evgenConfig.contact  = ["m.k.bugge@fys.uio.no"] 
evgenConfig.minevents = 500

include('MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_Alpgen_Common.py')
