evgenConfig.description = "ALPGEN+Pythia Z(->mumu)+bb+3jets process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "Z"]
evgenConfig.inputfilecheck = "ZmumubbNp3"
evgenConfig.minevents = 5000

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )
