## Herwig++ config for the MRSTMCal UE-EE-4 tune series with an NLO ME PDF
include("MC12JobOptions/Herwigpp_UEEE4_MRSTMCal_CT10ME_Common.py")

from Herwigpp_i import config as hw
topAlg.Herwigpp.Commands += hw.lhef_cmds(filename="events.lhe", nlo=True).splitlines()

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators = ["aMcAtNlo", "Herwigpp"]
evgenConfig.keywords = ["top", "singletop", "tchan","4-flavour"]
evgenConfig.description = "t-channel single top 2->3 aMC@NLO+Herwig++ - CT10f4 PDF for hard scatter and MRSTMCal with UEE4 tune for shower+MPI"
evgenConfig.contact = ["dominic.hirschbuehl@cern.ch"]
evgenConfig.inputfilecheck = "singletop_tchan2to3nlo_lept"
