evgenConfig.description = "POWHEG+Pythia6 Wt-channel inclusive production under DR (facsc=0.5, rensc=1) with Perugia2012 tune"
evgenConfig.keywords = ["top", "Wt","DR", "Incl"]
evgenConfig.contact  = ["cunfeng.feng@cern.ch"]
evgenConfig.inputfilecheck = 'singletop_Wtchan_incl_DR'

include("MC12JobOptions/PowhegPythia_Perugia2012_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

