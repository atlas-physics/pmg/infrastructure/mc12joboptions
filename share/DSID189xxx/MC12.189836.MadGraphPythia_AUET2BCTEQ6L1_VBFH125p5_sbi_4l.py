evgenConfig.description = "MG5MCtNLO+PYTHIA6 VBF H->ZZ->4l sbi (l=e,mu)"
evgenConfig.keywords    = ["Interferometry", "VBF", "Z","leptonic", "Higgs"]
evgenConfig.contact     = ["Maria Hoffmann <maria.hoffmann@cern.ch>"]

TMPBASEIF = 'VBFH125p5_sbi_4l'

if runArgs.ecmEnergy == 7000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_7TeV'
elif runArgs.ecmEnergy == 8000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_8TeV'
elif runArgs.ecmEnergy == 13000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_13TeV'
elif runArgs.ecmEnergy == 14000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_14TeV'
else:
    raise Exception("Incompatible inputGeneratorFile")

evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
#include("MC12JobOptions/Pythia_AUET2B_CT10_Common.py")
include("MC12JobOptions/Pythia_Tauola.py" )
include("MC12JobOptions/Pythia_Photos.py")

evgenConfig.generators += [ "MadGraph", "Pythia" ]

topAlg.Pythia.PythiaCommand += [ "pyinit user lhef",
                                 "pyinit pylisti -1",
                                 "pyinit pylistf 1",
                                 "pyinit dumpr 1 2",
                                 "pydat3 mdcy 15 1 0",# Turn off tau decays.
                                 "pydat3 mdcy 23 1 0",# Turn off Z decays.
                                 "pydat3 mdcy 25 1 0",# Turn off Higgs deca
                               ]


