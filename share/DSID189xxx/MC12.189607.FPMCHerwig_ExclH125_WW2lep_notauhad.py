evgenConfig.description = "Exclusive Higgs via HepMC"          
evgenConfig.keywords = ["QCD"  ,"H->WW", "exclusive" ]                                                                                    
evgenConfig.generators += ['FPMC']
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch>"]                                                                              
evgenConfig.contact += ["Last Feremenga <lferemen@cern.ch>"]                                                                              
evgenConfig.inputfilecheck = 'fpmc'
include("MC12JobOptions/HepMCReadFromFile_Common.py")
evgenConfig.tune = "none"

            






