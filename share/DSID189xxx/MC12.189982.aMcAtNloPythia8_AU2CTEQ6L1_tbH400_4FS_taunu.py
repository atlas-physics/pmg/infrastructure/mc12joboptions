evgenConfig.description = "MG5_aMCatNLO+Pythia8 for gg>t(b)ChargedHiggs"
evgenConfig.keywords = ["ChargedHiggs"]
evgenConfig.inputfilecheck = "tHpl_4FS"
evgenConfig.generators = ["MadGraph", "aMcAtNlo", "Pythia8"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

topAlg.Pythia8.Commands += [ 'Higgs:useBSM = on',
                             '37:onMode = off',
                             '37:onIfMatch = 15 16',
                "SpaceShower:pTmaxMatch = 1",            
                "SpaceShower:pTmaxFudge = 1",            
                "SpaceShower:MEcorrections = off",       
                "TimeShower:pTmaxMatch = 1",             
                "TimeShower:pTmaxFudge = 1",             
                "TimeShower:MEcorrections = off",        
                "TimeShower:globalRecoil = on",          
                "TimeShower:limitPTmaxGlobal = on",      
                "TimeShower:nMaxGlobalRecoil = 1",       
                "TimeShower:globalRecoilMode = 2",       
                "TimeShower:nMaxGlobalBranch = 1."
                             ]

evgenConfig.minevents=5000
