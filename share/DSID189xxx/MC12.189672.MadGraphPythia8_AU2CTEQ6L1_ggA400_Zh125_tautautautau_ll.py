include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

## For debugging only: print out some Pythia config info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on", "Next:numberShowLHA = 10", "Next:numberShowEvent = 10"]

evgenConfig.description = "non SM higgs A(400)->Zh(125)->tautautautau(->leplep)"
evgenConfig.keywords = ["nonSMhiggs","Zh","tau"]
evgenConfig.inputfilecheck = 'AZh_tautautautau_leplep_mA400'

evgenConfig.minevents=1000

# ... Filter  H->VV->Children
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 15
topAlg.XtoVVDecayFilter.StatusParent = 2
topAlg.XtoVVDecayFilter.PDGChild1 = [11,13]
topAlg.XtoVVDecayFilter.PDGChild2 = [11,13]
