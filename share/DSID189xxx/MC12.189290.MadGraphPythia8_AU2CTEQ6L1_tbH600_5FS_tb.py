evgenConfig.description = "MadGraph5+Pythia8 for gg>t(b)ChargedHiggs"
evgenConfig.keywords = ["ChargedHiggs"]
evgenConfig.inputfilecheck = "tHpl_5FS"
evgenConfig.generators = ["MadGraph", "Pythia8"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

topAlg.Pythia8.Commands += [ 'Higgs:useBSM = on',
                             '37:onMode = off',
                             '37:onIfMatch = 5 6'
                             ]

include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.Ptcut = 0
topAlg.TTbarWToLeptonFilter.NumLeptons = 1

evgenConfig.minevents=5000
