include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

## For debugging only: print out some Pythia config info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on", "Next:numberShowLHA = 10", "Next:numberShowEvent = 10"]

evgenConfig.description = "non SM higgs A(800)->Zh(125)->tautautautau(->hadhad)"
evgenConfig.keywords = ["nonSMhiggs","Zh","tau"]
evgenConfig.inputfilecheck = 'AZh_tautautautau_hadhad_mA800'

evgenConfig.minevents=4000

# ... Filter  H->VV->Children
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 15
topAlg.XtoVVDecayFilter.StatusParent = 2
topAlg.XtoVVDecayFilter.PDGChild1 = [111,130,211,221,223,310,311,321,323]
topAlg.XtoVVDecayFilter.PDGChild2 = [111,130,211,221,223,310,311,321,323]
