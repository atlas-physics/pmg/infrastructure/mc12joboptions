evgenConfig.description = "POWHEG+Pythia8 gg->ZH, Z->ee, H->WW->lvlv production and AU2 CT10 tune"
evgenConfig.keywords = ["ggZH" ,"NLO", "Powheg", "pythia8", "W","electron","leptonic"]
evgenConfig.inputfilecheck = 'powheg.189340.ggZeeH_SM_M125' 

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 24 24',
                            '24:onMode = off',#decay of W
                            '24:mMin = 2.0',
                            '24:onIfMatch = 11 12',
                            '24:onIfMatch = 13 14',
                            '24:onIfMatch = 15 16'
                            ]
