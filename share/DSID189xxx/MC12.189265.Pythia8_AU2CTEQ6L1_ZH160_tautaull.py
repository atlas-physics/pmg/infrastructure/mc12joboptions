evgenConfig.description = "PYTHIA8 ZH H->tautau->ll with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "ZH", "tau","leptonic"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:m0 = 160',
                            '25:mWidth = 0.0831',
                            '25:onMode = off',
                            '25:doForceWidth = true',
                            '25:onIfMatch = 15 15',
                            'HiggsSM:ffbar2HZ = on',
                            '23:onMode = off',
                            '23:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                            ]

# ... Filter H->VV->Children
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 15
topAlg.XtoVVDecayFilter.StatusParent = 2
topAlg.XtoVVDecayFilter.PDGChild1 = [11,13]
topAlg.XtoVVDecayFilter.PDGChild2 = [11,13]
