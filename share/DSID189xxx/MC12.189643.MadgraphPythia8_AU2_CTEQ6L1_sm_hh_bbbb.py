include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_MadGraph.py')

evgenConfig.inputfilecheck = 'group.phys-gener.madgraph5.189643.sm_hh_bbbb_8TeV'
evgenConfig.description = 'Standard Model di-Higgs production, to bbbb, with Madgraph. Includes the box'
evgenConfig.keywords =["SM", "hh", "non-resonant", "4b", "bbbb"]
evgenConfig.contact = ["David.Wardrope@cern.ch"]


#Pythia8 Commands
#topAlg.Pythia8.Commands += ["25:oneChannel = on 0.5 100 5 -5 "] # turn on b bbar
topAlg.Pythia8.Commands += ["25:onMode = off", "25:onIfMatch = 5 -5"]#turn on b bbar

