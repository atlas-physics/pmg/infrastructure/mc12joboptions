evgenConfig.description = "phantom+PYTHIA8 pp->2e2mu with CTEQ6L1 10SMW SBI"
evgenConfig.keywords    = ["SMhiggs", "VBF", "Z","leptonic", "width"]
evgenConfig.contact     = ["Hass AbouZeid <hass.abouzeid@cern.ch>"]
evgenConfig.generators  = ["Lhef", "Photos"]

TMPBASEIF = 'VBFH125p5_10SMW_qq_2e2mu'

if runArgs.ecmEnergy == 7000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_7TeV'
elif runArgs.ecmEnergy == 8000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_8TeV'
elif runArgs.ecmEnergy == 13000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_13TeV'
elif runArgs.ecmEnergy == 14000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_14TeV'
else:
    raise Exception("Incompatible inputGeneratorFile")

evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py")
topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '39:onMode = off',#decay of Higgs
                            '23:onMode = off'#decay of Z boson
                            '24:onMode = off'#decay of W boson
                            ]
