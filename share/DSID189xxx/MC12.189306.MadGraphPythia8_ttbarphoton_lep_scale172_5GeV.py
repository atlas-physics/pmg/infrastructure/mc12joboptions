#
# Job options file for MadGraph ttbar+photon showering with Pythia8
# by Andrey Loginov (Yale)
#
evgenConfig.description = "ttbar + photon production @ 14 TeV, no-allhadronic"
evgenConfig.keywords = ["ttbar", "ttgamma", "photon", "top"]
evgenConfig.contact  = ["Andrey Loginov <andrey.loginov@yale.edu>"]
evgenConfig.inputfilecheck = 'ttbarphoton_lep_scale172p5GeV_14TeV'
#
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Tauola.py")
include("MC12JobOptions/Pythia8_Photos.py")
#
topAlg.Pythia8.Commands += [
    'PartonLevel:FSR  = off' #turn off native pythia FSR 
    ]
#
evgenConfig.generators += [ "MadGraph", "Pythia8" ]

# OUTPUT PRINTOUT LEVEL
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
# you can override this for individual modules if necessary
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel               = 5
