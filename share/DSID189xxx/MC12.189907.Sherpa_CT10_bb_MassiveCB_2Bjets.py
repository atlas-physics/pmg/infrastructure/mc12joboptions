evgenConfig.description = "Sherpa bbb SM background"
evgenConfig.keywords = [ "bb jets" ]
evgenConfig.contact  = ["timb@slac.stanford.edu"]
evgenConfig.inputconfcheck = 'Sherpa_CT10_bb_MassiveCB'
evgenConfig.weighting = 0

sherpaRunCard="""
(run){
  MASSIVE[4]=1
  MASSIVE[5]=1
  PARTICLE_CONTAINER 98[m:-1] qcd 1 -1 2 -2 3 -3 4 -4 5 -5 21;
}(run);

(me){
  EVENT_GENERATION_MODE = PartiallyUnweighted
}(me)





# 1 the class anything -> b
(processes){
Process 93 5 -> 93 5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order_EW 0;
End process;
Process 93 -5 -> 93 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order_EW 0;
End process;

# 2 the class anything -> b b~
Process 93 93 -> 5 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order_EW 0;
End process;
Process 5 -5 -> 5 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order_EW 0;
End process;

# 3 the class anything -> b b
Process 5 5 -> 5 5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order_EW 0;
End process;
Process -5 -5 -> -5 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order_EW 0;
End process;

# 4 the class anything -> b b b~
Process 93 5 -> 5 -5 5 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;
Process 93 -5 -> 5 -5 -5 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;

# 5 the class anything -> b b b~ b~
Process 93 93 -> 5 -5 5 -5;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;
Process 5 -5 -> 5 -5 5 -5;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;

# 6 the class anything -> b b b b~
Process 5 5 -> 5 5 5 -5;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;
Process -5 -5 -> -5 -5 -5 5;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;





# 7 the class c+x -> b
Process 4 5 -> 4 5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order_EW 0;
End process;
Process 4 5 -> 4 5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;
Process 4 -5 -> 4 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order_EW 0;
End process;
Process 4 -5 -> 4 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;

# 8 the class c~+x -> b
Process -4 5 -> -4 5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order_EW 0;
End process;
Process -4 5 -> -4 5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;
Process -4 -5 -> -4 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order_EW 0;
End process;
Process -4 -5 -> -4 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;

# 9 the class charm+x -> b b~
Process 4 -4 -> 5 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order_EW 0;
End process;
Process 4 93 -> 4 5 -5 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;
Process -4 93 -> -4 5 -5 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;

# 10 the class x + x  -> b b~ c c~ or c c~ -> b b~ b b~
Process 5 -5 -> 5 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;
Process 4 -4 -> 5 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;
Process 93 93 -> 5 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;
Process 4 -4 -> 5 -5 5 -5;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;

# 11 the class charm+x -> b b b~
Process 4 5 -> 5 -5 5 4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;
Process 4 -5 -> 5 -5 -5 4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;
Process -4 5 -> 5 -5 5 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;
Process -4 -5 -> 5 -5 -5 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;

# 12 the class b b -> b b c c~
Process 5 5 -> 5 5 4 -4
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
Process -5 -5 -> -5 -5 4 -4
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order_EW 0;
End process;



}(processes);



(selector){
   "PT" 98 100.,1.e10 [PT_UP];
}(selector)


"""

include("MC12JobOptions/Sherpa_CT10_Common.py")
topAlg.Sherpa_i.ResetWeight=False

from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()


from GeneratorFilters.GeneratorFiltersConf import LeadingDiBjetFilter
topAlg += LeadingDiBjetFilter()
LeadingDiBjetFilter = topAlg.LeadingDiBjetFilter

LeadingDiBjetFilter.LeadJetPtMin=130.*GeV
LeadingDiBjetFilter.BottomPtMin=5.0*GeV
LeadingDiBjetFilter.BottomEtaMax=2.9
LeadingDiBjetFilter.JetPtMin=40.*GeV
LeadingDiBjetFilter.JetEtaMax=2.9
LeadingDiBjetFilter.DeltaRFromTruth=0.3
LeadingDiBjetFilter.TruthContainerName="AntiKt4TruthJets"
LeadingDiBjetFilter.AcceptSomeLightEvents=False


StreamEVGEN.RequireAlgs += [ "LeadingDiBjetFilter" ]
