evgenConfig.description = "PYTHIA8 ttH dileptonic H->bbar with AU2 CT10"
evgenConfig.keywords = ["SMhiggs", "ttH", "bottom","dileptonic"]

#include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 125',
                            '25:mWidth = 0.00407',
                            '25:doForceWidth = true',
                            'HiggsSM:gg2Httbar = on',
                            'HiggsSM:qqbar2Httbar = on',
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 5 5',
                            '24:onMode = off',#decay of W
                            '24:onIfAny = 11 12 13 14 15 16'
                            ]

include("MC12JobOptions/BoostedHiggsFilter.py")
topAlg.HiggsFilter.Ptcut=0.
topAlg.HiggsFilter.Ptmaxcut=150000.
