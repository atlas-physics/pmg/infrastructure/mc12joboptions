evgenConfig.description = "gg2VV-3.1.6 gg->(H125p5 with 25SM width)->VV->2e2nu using CT10nnlo PDF and PowhegPythia8 with AU2 CT10 tune"
evgenConfig.keywords    = ["ggVV", "gg2VV-3.1.6", "V","leptonic"]
evgenConfig.contact     = ["Maria Hoffmann <Maria.Hoffmann@cern.ch>"]

TMPBASEIF = 'ggH125p5_25SMW_gg_VV_2e2nu_m2l4_2pt3'
if runArgs.ecmEnergy == 7000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_7TeV'
elif runArgs.ecmEnergy == 8000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_8TeV'
elif runArgs.ecmEnergy == 13000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_13TeV'
elif runArgs.ecmEnergy == 14000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_14TeV'
else:
    raise Exception("Incompatible inputGeneratorFile")

evgenConfig.minevents = 5000

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '23:onMode = off',#decay of Z
			    '24:onMode = off',#decay of W
                           ]
