include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "4 b-quarks inclusive production"
evgenConfig.keywords = [ "4b" ]
evgenConfig.contact  = [ "olivier.arnaez@cern.ch", "danilo.ferreiradelima@glasgow.ac.uk" ]
evgenConfig.minevents = 50
evgenConfig.inputconfcheck="189723.Sherpa_CT10_4b"

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  MASSIVE[15]=1

  MASSIVE[4]=1
  MASSIVE[5]=1

  SOFT_SPIN_CORRELATIONS=1 
}(run)

(processes){
  Process 93 93 -> 5 5 -5 -5
  Order_EW 0;
  Integration_Error 0.01 {4};
  CKKW sqr(30/E_CMS)
  End process;
}(processes)

(me){
  ME_SIGNAL_GENERATOR Comix;
  SCALES VAR{0.0625*(PPerp2(p[3])+PPerp2(p[4])+PPerp2(p[5])+PPerp2(p[2]))}
}(me)

(selector){
  PT 5 25 E_CMS
  PseudoRapidity 5 -2.8 2.8
  PT -5 25 E_CMS
  PseudoRapidity -5 -2.8 2.8
  DeltaR 5 -5 0.3 10.
  DeltaR 5 5 0.3 10.
}(selector)
"""

# Take tau BR's into account:
topAlg.Sherpa_i.Parameters += [ "DECAYFILE=HadronDecaysTauHH.dat" ]
topAlg.Sherpa_i.CrossSectionScaleFactor=1.

# Overwrite default MC11 widths with LO widths for top and W
topAlg.Sherpa_i.Parameters += [ "WIDTH[6]=1.47211", "WIDTH[24]=2.035169" ]
