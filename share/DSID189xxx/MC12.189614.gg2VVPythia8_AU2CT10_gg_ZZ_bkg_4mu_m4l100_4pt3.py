evgenConfig.description = "gg2VV-3.1.6 gg->ZZ->4mu background using CT10nnlo PDF and PowhegPythia8 with AU2 CT10 tune"
evgenConfig.keywords    = ["ggZZ", "gg2VV-3.1.6", "Z","leptonic"]
evgenConfig.contact     = ["Alessandro Calandri <Alessandro.Calandri@cern.ch>"]

TMPBASEIF = 'gg_ZZ_bkg_4mu_m4l100_4pt3'
if runArgs.ecmEnergy == 7000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_7TeV'
elif runArgs.ecmEnergy == 8000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_8TeV'
elif runArgs.ecmEnergy == 13000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_13TeV'
elif runArgs.ecmEnergy == 14000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_14TeV'
else:
    raise Exception("Incompatible inputGeneratorFile")

evgenConfig.minevents = 5000

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '23:onMode = off',#decay of Z
                           ]
