include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_MadGraph.py')
include('MC12JobOptions/Pythia8_Photos.py')

evgenConfig.inputfilecheck = 'H350_hh_yybb'
evgenConfig.description = 'standard model dihiggs production, to yybb, with Madgraph.  includes the box'
evgenConfig.keywords =["SM", "hh", "resonant", "gamgam bb", "yybb"]
evgenConfig.contact = ["james.saxon@cern.ch"]

#Pythia8 Commands
topAlg.Pythia8.Commands += ["25:oneChannel = on 0.5 100 5 -5 ", # turn on b bbar
                            "25:addChannel = on 0.5 100 22 22 "] # turn on gam gam

# Generator Filters
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 1560
topAlg.XtoVVDecayFilter.PDGParent = 25
topAlg.XtoVVDecayFilter.StatusParent = 22
topAlg.XtoVVDecayFilter.PDGChild1 = [5]
topAlg.XtoVVDecayFilter.PDGChild2 = [22]
StreamEVGEN.RequireAlgs = ["XtoVVDecayFilter"]

