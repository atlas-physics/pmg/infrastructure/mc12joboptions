evgenConfig.description = "ALPGEN+Jimmy Z(->ee)+5jets process with AUET2 tune and pdf CTEQ6L1"
evgenConfig.keywords = ["EW", "Z", "e"]
evgenConfig.inputfilecheck = "ZeeNp5"
evgenConfig.minevents = 5000

include("MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py")
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
