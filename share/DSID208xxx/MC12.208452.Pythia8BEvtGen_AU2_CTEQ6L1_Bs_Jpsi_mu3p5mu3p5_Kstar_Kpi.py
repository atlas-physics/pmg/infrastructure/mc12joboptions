##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B_s0 -> J/psi(mu3.5mu3.5) K*0(K+pi-) 
##############################################################
f = open("BS0_JPSI_KSTAR_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Alias my_K*0   K*0\n")
f.write("Alias my_J/psi J/psi\n")
f.write("Decay B_s0\n")
f.write("1.0000  my_K*0     my_J/psi       PHSP; \n")
f.write("Enddecay\n")
f.write("Decay my_K*0\n")
f.write("1.0000    K+    pi-           VSS;\n")
f.write("Enddecay\n")
f.write("Decay my_J/psi\n")
f.write("1.0000    mu+   mu-   PHOTOS VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

##############################################################

evgenConfig.description = "Exclusive B_s0->K*0_Kpi_J/psi_mu3p5mu3p5 production"
evgenConfig.keywords    = ["exclusiveBtoK*0","B_s0","K*0", "J/psi", "dimuons"]
evgenConfig.minevents   = 200

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_EvtGenAfterburner.py")
include("MC12JobOptions/BSignalFilter.py")

#include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")
#to synchronise this with the exclusive pythia decays (file above)
#we explicitly set the following options
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 10.']

topAlg.Pythia8B.QuarkPtCut      = 0.0
topAlg.Pythia8B.AntiQuarkPtCut  = 7.0
topAlg.Pythia8B.QuarkEtaCut     = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.6
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents         = True

topAlg.Pythia8B.NHadronizationLoops = 1

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 0
topAlg.Pythia8B.SignalPDGCodes = [531]
topAlg.EvtInclusiveDecay.userDecayFile = "BS0_JPSI_KSTAR_USER.DEC"

topAlg.BSignalFilter.LVL1MuonCutOn  = True
topAlg.BSignalFilter.LVL2MuonCutOn  = True
topAlg.BSignalFilter.LVL1MuonCutPT  = 3500
topAlg.BSignalFilter.LVL1MuonCutEta = 2.6
topAlg.BSignalFilter.LVL2MuonCutPT  = 3500
topAlg.BSignalFilter.LVL2MuonCutEta = 2.6

topAlg.BSignalFilter.B_PDGCode = 531
topAlg.BSignalFilter.Cuts_Final_hadrons_switch = True
topAlg.BSignalFilter.Cuts_Final_hadrons_pT     = 500.0
topAlg.BSignalFilter.Cuts_Final_hadrons_eta    = 2.6
