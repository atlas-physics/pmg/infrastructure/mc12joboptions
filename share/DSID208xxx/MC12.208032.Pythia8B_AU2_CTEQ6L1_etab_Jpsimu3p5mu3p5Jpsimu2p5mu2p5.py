##############################################################
# etaB To Jpsimu2p5mu2p5Jpsimu2p5mu2p5
# author: A. Chisholm - S. Leontsinis - S. Turchikhin
##############################################################

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "eta_b(1S)->J/psi(mu2.5mu2.5)J/psi(mu2.5mu2.5) production"
evgenConfig.keywords = ["eta_b","dimuons","Jpsi"]
evgenConfig.minevents = 2000

include("MC12JobOptions/Pythia8B_Bottomonium_Common.py")

# Process for eta_b(0)1S
topAlg.Pythia8B.Commands += ['Bottomonium:gg2bbbar(3S1)[1S0(8)]g    = on']
topAlg.Pythia8B.Commands += ['Bottomonium:qg2bbbar(3S1)[1S0(8)]q    = on']
topAlg.Pythia8B.Commands += ['Bottomonium:qqbar2bbbar(3S1)[1S0(8)]g = on']

topAlg.Pythia8B.Commands += ['10551:name = Fake eta_b'] 
topAlg.Pythia8B.Commands += ['10551:m0 = 9.398'] 
topAlg.Pythia8B.Commands += ['10551:mWidth = 0.011'] 

topAlg.Pythia8B.Commands += ['10551:oneChannel = 1 .0001 100 443 443']  

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 5.']

# Close all J/psi decays apart from J/psi->mumu
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']

# Trigger pattern selections
topAlg.Pythia8B.TriggerPDGCode     = 13
topAlg.Pythia8B.TriggerStatePtCut  = [3.5, 2.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [2, 4]

