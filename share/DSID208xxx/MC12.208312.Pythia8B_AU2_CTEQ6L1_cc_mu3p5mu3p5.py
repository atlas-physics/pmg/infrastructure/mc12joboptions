# Inclusive ccbar->mumuX sample with 3.5 GeV cut on the pT of the final muons

evgenConfig.description = "Inclusive cc->mu3.5mu3.5X production"
evgenConfig.keywords = ["inclusive","ccbar","dimuons"]
evgenConfig.minevents = 200

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

topAlg.Pythia8B.Commands += ['HardQCD:all = on']
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 15.']
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

# Quark cuts
topAlg.Pythia8B.SelectBQuarks = False
topAlg.Pythia8B.SelectCQuarks = True
topAlg.Pythia8B.QuarkPtCut = 7.0
topAlg.Pythia8B.AntiQuarkPtCut = 7.0
topAlg.Pythia8B.QuarkEtaCut = 4.5
topAlg.Pythia8B.AntiQuarkEtaCut = 4.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = False

topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.NHadronizationLoops = 15

# List of B-species
include("Pythia8B_i/BPDGCodes.py")

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.6
topAlg.Pythia8B.MinimumCountPerCut = [2]
