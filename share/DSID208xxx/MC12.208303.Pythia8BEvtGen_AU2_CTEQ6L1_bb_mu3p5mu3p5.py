evgenConfig.description = "Inclusive bb->mu3.5mu3.5X production"
evgenConfig.keywords = ["inclusive","bbbar","dimuons"]
evgenConfig.minevents = 100

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_EvtGenAfterburner.py") 
include("MC12JobOptions/MultiMuonFilter.py")
include("MC12JobOptions/DiLeptonMassFilter.py")

topAlg.Pythia8B.Commands += ['HardQCD:all = on'] 
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 9.']
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.QuarkPtCut = 5.0
topAlg.Pythia8B.AntiQuarkPtCut = 5.0
topAlg.Pythia8B.QuarkEtaCut = 3.5
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = False
topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.NHadronizationLoops = 30

include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.TriggerPDGCode = 0
topAlg.EvtInclusiveDecay.allowAllKnownDecays = False
topAlg.MultiMuonFilter.Ptcut = 3500.
topAlg.MultiMuonFilter.Etacut = 3.0
topAlg.MultiMuonFilter.NMuons = 2
topAlg.DiLeptonMassFilter.MinPt = 3500.
topAlg.DiLeptonMassFilter.MaxEta = 3.0
topAlg.DiLeptonMassFilter.MinMass = 4000.
topAlg.DiLeptonMassFilter.AllowSameCharge = False
