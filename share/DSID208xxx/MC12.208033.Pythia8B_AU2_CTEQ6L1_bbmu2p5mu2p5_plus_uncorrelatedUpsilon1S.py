evgenConfig.description = "Inclusive bb->mu4mu4 production with Upsilon(1S) production in second hard process"
evgenConfig.keywords = ["inclusive","bbbar","dimuons","Upsilon"]
evgenConfig.minevents = 200

include("MC12JobOptions/Pythia82B_AU2_CTEQ6L1_Common.py")

topAlg.Pythia8B.Commands += ['HardQCD:all = on'] 
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

topAlg.Pythia8B.Commands += ['553:onMode = off']
topAlg.Pythia8B.Commands += ['553:3:onMode = on']
topAlg.Pythia8B.Commands += ['SecondHard:generate = on']
topAlg.Pythia8B.Commands += ['SecondHard:Bottomonium = on']
topAlg.Pythia8B.SignalPDGCodes += [553,-13,13]
topAlg.Pythia8B.SignalPtCuts = [0,2.5,2.5]
topAlg.Pythia8B.SignalEtaCuts = [2.5,2.5,2.5]

topAlg.Pythia8B.Commands += ['Bottomonium:O(3PJ)[3P0(1)] = [0,0,0]']
topAlg.Pythia8B.Commands += ['Bottomonium:O(3PJ)[3S1(8)] = [0,0,0]']
topAlg.Pythia8B.Commands += ['Bottomonium:O(3DJ)[3D1(1)] = [0,0,0]']
topAlg.Pythia8B.Commands += ['Bottomonium:O(3DJ)[3P0(8)] = [0,0,0]']


# Quark cuts
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.QuarkPtCut = 4.0
topAlg.Pythia8B.AntiQuarkPtCut = 4.0
topAlg.Pythia8B.QuarkEtaCut = 3.5
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = False

topAlg.Pythia8B.VetoDoubleBEvents = False
topAlg.Pythia8B.NHadronizationLoops = 15

# List of B-species
include("Pythia8B_i/BPDGCodes.py")

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [2.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [4]

