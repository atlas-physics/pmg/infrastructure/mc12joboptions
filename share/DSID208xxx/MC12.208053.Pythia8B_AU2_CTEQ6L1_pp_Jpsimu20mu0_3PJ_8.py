##############################################################
# Job options fragment for pp->J/psi(mu20mu0)X  
##############################################################
include("MC12JobOptions/Pythia82B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "Inclusive pp->ccbar 3PJ_8 ->J/psi(mu20mu0) production"
evgenConfig.keywords = ["charmonium","dimuons","octet"]
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8B_Charmonium_Common.py") 
topAlg.Pythia8B.Commands += ['Charmonium:all = off']
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 40.'] 
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [443,-13,13]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [20]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [1]


topAlg.Pythia8B.Commands+=['Charmonium:gg2ccbar(3S1)[3PJ(8)]g = on,on']
topAlg.Pythia8B.Commands+=['Charmonium:qg2ccbar(3S1)[3PJ(8)]q = on,on']
topAlg.Pythia8B.Commands+=['Charmonium:qqbar2ccbar(3S1)[3PJ(8)]g = on,on']

