############################################################################
# Job options for Pythia8B_i generation of B0->J/psi(mumu)Kpi Lambda -> p pi
############################################################################
evgenConfig.description = "B0->J/psi(mumu)Kpi Lambda->ppi"
evgenConfig.keywords = ["BtoJpsi","dimuons","heavy baryons"]
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

# Hard process
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
#
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 10.'] 

#
# increase strangeness production
#
topAlg.Pythia8B.Commands += ['StringFlav:probStoUD = 0.6'] 
topAlg.Pythia8B.Commands += ['StringFlav:probSQtoQQ = 1.'] 

# Event selection
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.VetoDoubleCEvents = False
#
topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.QuarkEtaCut = 1000.
topAlg.Pythia8B.AntiQuarkPtCut = 10.0
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

#
# Xi_bc0 (marginal yield, for consistency):
#
topAlg.Pythia8B.Commands += ['5142:m0 = 6.989']  # expectation
topAlg.Pythia8B.Commands += ['5142:tau0 = 0.1520'] # B_c value from PDG 2017
#
#
topAlg.Pythia8B.Commands += ['5142:onMode = 2']
#
topAlg.Pythia8B.Commands += ['5142:addChannel = 3 1. 0  3122 -511']
#

#
# Xi_b0 -> Xi_bc0 (for actual production):
#
topAlg.Pythia8B.Commands += ['5232:m0 = 6.989']  # expectation for Xi_bc0
topAlg.Pythia8B.Commands += ['5232:tau0 = 0.1520'] # B_c value from PDG 2017
#
#
topAlg.Pythia8B.Commands += ['5232:onMode = 2']
#
topAlg.Pythia8B.Commands += ['5232:addChannel = 3 1. 0  3122 -511']
#

#
# Xi'_b0 -> Xi_b0 + gamma
#
topAlg.Pythia8B.Commands += ['5322:m0 = 7.006']  # to allow the decay

#
# Xi*_b0 -> Xi_b0 + gamma
#
topAlg.Pythia8B.Commands += ['5324:m0 = 7.007']  # to allow the decay

#
# J/psi:
#
topAlg.Pythia8B.Commands += ['443:m0 = 3.096900']  # PDG 2017
topAlg.Pythia8B.Commands += ['443:mWidth = 0.0000929'] # PDG 2017
#
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']

#
# B_d:
#
topAlg.Pythia8B.Commands += ['511:m0 = 5.27963']  # PDG 2017
topAlg.Pythia8B.Commands += ['511:tau0 = 0.4557'] # PDG 2017
#
# B_d decays:
#
topAlg.Pythia8B.Commands += ['511:onMode = 3']
#
# channels 850-869 have onMode = 2 in Pythia8 default
#
topAlg.Pythia8B.Commands += ['511:850:onMode = 0']
topAlg.Pythia8B.Commands += ['511:851:onMode = 0']
topAlg.Pythia8B.Commands += ['511:852:onMode = 0']
topAlg.Pythia8B.Commands += ['511:853:onMode = 0']
topAlg.Pythia8B.Commands += ['511:854:onMode = 0']
topAlg.Pythia8B.Commands += ['511:855:onMode = 0']
topAlg.Pythia8B.Commands += ['511:856:onMode = 0']
topAlg.Pythia8B.Commands += ['511:857:onMode = 0']
topAlg.Pythia8B.Commands += ['511:858:onMode = 0']
topAlg.Pythia8B.Commands += ['511:859:onMode = 0']
topAlg.Pythia8B.Commands += ['511:860:onMode = 0']
topAlg.Pythia8B.Commands += ['511:861:onMode = 0']
topAlg.Pythia8B.Commands += ['511:862:onMode = 0']
topAlg.Pythia8B.Commands += ['511:863:onMode = 0']
topAlg.Pythia8B.Commands += ['511:864:onMode = 0']
topAlg.Pythia8B.Commands += ['511:865:onMode = 0']
topAlg.Pythia8B.Commands += ['511:866:onMode = 0']
topAlg.Pythia8B.Commands += ['511:867:onMode = 0']
topAlg.Pythia8B.Commands += ['511:868:onMode = 0']
topAlg.Pythia8B.Commands += ['511:869:onMode = 0']
#
# channels 870-889 have onMode = 3 in Pythia8 default
#
#topAlg.Pythia8B.Commands += ['511:870:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:871:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:872:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:873:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:874:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:875:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:876:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:877:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:878:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:879:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:880:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:881:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:882:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:883:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:884:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:885:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:886:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:887:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:888:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:889:onMode = 0']
#
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.088 0  443 321 -211']
#
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.071 0  443 9000311']
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.690 0  443     313']
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.003 0  443  100313']
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.059 0  443   10311']
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.063 0  443     315']
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.003 0  443   30313']
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.002 0  443     317']
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.001 0  443 9020311']
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.004 0  443 9010315']
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.002 0  443     319']
#
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.005 0  -10044123 321']
#
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.019 0  -44123 321']
#

#
# K*(800)0 -> K+ pi- # Belle, PRD 90 (2014) 112009
#
topAlg.Pythia8B.Commands += ['9000311:new = K*(800)0 K*(800)bar0 3 0 0 0.931 0.578 0.63325 2.18273 0.']
topAlg.Pythia8B.Commands += ['9000311:addChannel = 1 1. 0 321 -211']

#
# K*(892)0 -> K+ pi-
#
topAlg.Pythia8B.Commands += ['313:m0 = 0.89555']  # PDG 2017
topAlg.Pythia8B.Commands += ['313:mWidth = 0.0473'] # PDG 2017
#
topAlg.Pythia8B.Commands += ['313:mMin = 0.63325']
topAlg.Pythia8B.Commands += ['313:mMax = 2.18273']
#
topAlg.Pythia8B.Commands += ['313:onMode = 3']
topAlg.Pythia8B.Commands += ['313:0:onMode = 1']

#
# K*(1410)0 -> K+ pi- # PDG 2017
#
topAlg.Pythia8B.Commands += ['100313:new = K*(1410)0 K*(1410)bar0 3 0 0 1.421 0.236 0.63325 2.18273 0.']
topAlg.Pythia8B.Commands += ['100313:addChannel = 1 1. 0 321 -211']

#
# K*_0(1430)0 -> K+ pi-
#
topAlg.Pythia8B.Commands += ['10311:m0 = 1.425']  # PDG 2017
topAlg.Pythia8B.Commands += ['10311:mWidth = 0.270'] # PDG 2017
#
topAlg.Pythia8B.Commands += ['10311:mMin = 0.63325']
topAlg.Pythia8B.Commands += ['10311:mMax = 2.18273']
#
topAlg.Pythia8B.Commands += ['10311:onMode = 3']
topAlg.Pythia8B.Commands += ['10311:0:onMode = 1']
#

#
# K*_2(1430)0 -> K+ pi-
#
topAlg.Pythia8B.Commands += ['315:m0 = 1.4324']  # PDG 2017
topAlg.Pythia8B.Commands += ['315:mWidth = 0.109'] # PDG 2017
#
topAlg.Pythia8B.Commands += ['315:mMin = 0.63325']
topAlg.Pythia8B.Commands += ['315:mMax = 2.18273']
#
topAlg.Pythia8B.Commands += ['315:onMode = 3']
topAlg.Pythia8B.Commands += ['315:0:onMode = 1']

#
# K*(1680)0 -> K+ pi-
#
topAlg.Pythia8B.Commands += ['30313:m0 = 1.718']  # PDG 2017
topAlg.Pythia8B.Commands += ['30313:mWidth = 0.322'] # PDG 2017
#
topAlg.Pythia8B.Commands += ['30313:mMin = 0.63325']
topAlg.Pythia8B.Commands += ['30313:mMax = 2.18273']
#
topAlg.Pythia8B.Commands += ['30313:onMode = 3']
topAlg.Pythia8B.Commands += ['30313:0:onMode = 1']

#
# K*_3(1780)0 -> K+ pi- # PDFG 2017
#
topAlg.Pythia8B.Commands += ['317:new = K*_3(1780)0 K*_3(1780)bar0 7 0 0 1.776 0.159 0.63325 2.18273 0.']
topAlg.Pythia8B.Commands += ['317:addChannel = 1 1. 0 321 -211']

#
# K*_0(1950)0 -> K+ pi- # PDFG 2017
#
topAlg.Pythia8B.Commands += ['9020311:new = K*_0(1950)0 K*_0(1950)bar0 1 0 0 1.945 0.201 0.63325 2.18273 0.']
topAlg.Pythia8B.Commands += ['9020311:addChannel = 1 1. 0 321 -211']

#
# K*_2(1980)0 -> K+ pi- # PDFG 2017
#
topAlg.Pythia8B.Commands += ['9010315:new = K*_2(1980)0 K*_2(1980)bar0 5 0 0 1.974 0.376 0.63325 2.18273 0.']
topAlg.Pythia8B.Commands += ['9010315:addChannel = 1 1. 0 321 -211']

#
# K*_4(2045)0 -> K+ p- # PDFG 2017
#
topAlg.Pythia8B.Commands += ['319:new = K*_4(2045)0 K*_4(2045)bar0 9 0 0 2.045 0.198 0.63325 2.18273 0.']
topAlg.Pythia8B.Commands += ['319:addChannel = 1 1. 0 321 -211']

#
# X(4430)+ -> J/psi pi+
#
topAlg.Pythia8B.Commands += ['10044123:new = X(4430)+ X(4430)- 3 3 0 4.478 0.181 3.2365 4.7859 0.']
topAlg.Pythia8B.Commands += ['10044123:addChannel = 1 1. 0 443 211']

#
# X(4200)+ -> J/psi pi+
#
topAlg.Pythia8B.Commands += ['44123:new = X(4200)+ X(4200)- 3 3 0 4.196 0.370 3.2365 4.7859 0.']
topAlg.Pythia8B.Commands += ['44123:addChannel = 1 1. 0 443 211']

#theApp.ReflexPluginDebugLevel = 1000

topAlg.Pythia8B.OutputLevel = INFO
#topAlg.Pythia8B.OutputLevel = DEBUG
topAlg.Pythia8B.NHadronizationLoops = 20

topAlg.Pythia8B.SignalPDGCodes = [ -5232 ]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.TriggerOppositeCharges = True
topAlg.Pythia8B.MinimumCountPerCut = [2]

include("MC12JobOptions/ParentChildFilter.py")
topAlg.ParentChildFilter.PDGParent  = [5232]
topAlg.ParentChildFilter.PtMinParent =  9000.
topAlg.ParentChildFilter.EtaRangeParent = 3.
topAlg.ParentChildFilter.PDGChild = [3122]
topAlg.ParentChildFilter.PtMinChild = 1000.
topAlg.ParentChildFilter.EtaRangeChild = 1000.
