#######################################################################
# Job options fragment for bb->X(3872)(->pi,pi,Jpsi(mu4mu4))
#######################################################################
evgenConfig.description = "bb->X(3872)(->pi,pi,Jpsi(mu4mu4))"
evgenConfig.keywords = ["charmonium","dimuons"]
evgenConfig.minevents = 200

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_inclusiveBChic1_Common.py")

# Hard process
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 6.'] # Equivalent of CKIN3

# Quark cuts
topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 4.0
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
#topAlg.Pythia8B.NHadronizationLoops = 4

topAlg.Pythia8B.Commands += ['20443:m0 = 3.87168']
topAlg.Pythia8B.Commands += ['20443:mMin = 3.87168']
topAlg.Pythia8B.Commands += ['20443:mMax = 3.87168']

#Turn all decays off, then allow decay number 7, which is ->
topAlg.Pythia8B.Commands += ['20443:0:products = 443 211 -211']
topAlg.Pythia8B.Commands += ['20443:onMode = off']
topAlg.Pythia8B.Commands += ['20443:0:onMode = on']
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [20443,443,-13,13,-211,211] #mu+ mu- pi- pi+
topAlg.Pythia8B.SignalPtCuts = [0.0, 0.0, 3.5, 3.5, 0.38, 0.38]
topAlg.Pythia8B.SignalEtaCuts = [100.0, 100.0, 2.5, 2.5, 2.5, 2.5]
