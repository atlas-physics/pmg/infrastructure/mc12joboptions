######################################################
# Job options for Pythia8B_i generation of Bs->mu4mu4   
######################################################
evgenConfig.description = "Exclusive Bs->mu4mu4 production"
evgenConfig.keywords = ["exclusiveB","Bs","dimuons"]
evgenConfig.minevents = 5000
 
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 10.']
topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 10.0
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

topAlg.Pythia8B.Commands += ['531:addChannel = 2 1.0 0 -13 13']
topAlg.Pythia8B.SignalPDGCodes = [531,-13,13]

topAlg.Pythia8B.NHadronizationLoops = 4

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [4.0]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [2]

