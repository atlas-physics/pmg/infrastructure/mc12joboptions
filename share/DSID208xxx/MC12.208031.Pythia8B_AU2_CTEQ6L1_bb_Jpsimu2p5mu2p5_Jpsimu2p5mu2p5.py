##############################################################
# J/psi(mu2.5mu2.5)X + J/psi(mu2.5mu2.5)X
# S. Leontsinis - A. Chisholm
# For the study of associated production J/\psi+J/\psi (both non-prompt)
##############################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

evgenConfig.description = "Inclusive bb->J/psi(mu2.5mu2.5)X + J/psi(mu2.5mu2.5)X production"
evgenConfig.keywords = ["chamonium", "dimuons", "Jpsi"]
evgenConfig.minevents = 500

# Hard process
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Uncomment for MSEL1
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 6.'] # Equivalent of CKIN3
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

# Quark cuts
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.QuarkPtCut = 4.0
topAlg.Pythia8B.AntiQuarkPtCut = 4.0
topAlg.Pythia8B.QuarkEtaCut = 2.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.UserSelection = 'BJPSIINCLUSIVE'

topAlg.Pythia8B.Commands += ['511:onMode  = off']
topAlg.Pythia8B.Commands += ['511:onIfAny = 443']
topAlg.Pythia8B.Commands += ['521:onMode  = off']
topAlg.Pythia8B.Commands += ['521:onIfAny = 443']
topAlg.Pythia8B.Commands += ['531:onMode  = off']
topAlg.Pythia8B.Commands += ['531:onIfAny = 443']
topAlg.Pythia8B.Commands += ['541:onMode  = off']
topAlg.Pythia8B.Commands += ['541:onIfAny = 443']
topAlg.Pythia8B.Commands += ['5122:onMode = off']
topAlg.Pythia8B.Commands += ['5122:onIfAny = 443']
topAlg.Pythia8B.Commands += ['5132:onMode = off']
topAlg.Pythia8B.Commands += ['5132:onIfAny = 443']
topAlg.Pythia8B.Commands += ['5232:onMode = off']
topAlg.Pythia8B.Commands += ['5232:onIfAny = 443']
topAlg.Pythia8B.Commands += ['5332:onMode = off']
topAlg.Pythia8B.Commands += ['5332:onIfAny = 443']

topAlg.Pythia8B.Commands += ['-511:onMode  = off']
topAlg.Pythia8B.Commands += ['-511:onIfAny = 443']
topAlg.Pythia8B.Commands += ['-521:onMode  = off']
topAlg.Pythia8B.Commands += ['-521:onIfAny = 443']
topAlg.Pythia8B.Commands += ['-531:onMode  = off']
topAlg.Pythia8B.Commands += ['-531:onIfAny = 443']
topAlg.Pythia8B.Commands += ['-541:onMode  = off']
topAlg.Pythia8B.Commands += ['-541:onIfAny = 443']
topAlg.Pythia8B.Commands += ['-5122:onMode = off']
topAlg.Pythia8B.Commands += ['-5122:onIfAny = 443']
topAlg.Pythia8B.Commands += ['-5132:onMode = off']
topAlg.Pythia8B.Commands += ['-5132:onIfAny = 443']
topAlg.Pythia8B.Commands += ['-5232:onMode = off']
topAlg.Pythia8B.Commands += ['-5232:onIfAny = 443']
topAlg.Pythia8B.Commands += ['-5332:onMode = off']
topAlg.Pythia8B.Commands += ['-5332:onIfAny = 443']

# Close all J/psi decays apart from J/psi->mumu
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:onIfAny = 13']

# Signal topology - only events containing this sequence will be accepted 
topAlg.Pythia8B.SignalPDGCodes = [443,-13,13]

# Number of repeat-hadronization loops
topAlg.Pythia8B.NHadronizationLoops = 2

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [2.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [4]
