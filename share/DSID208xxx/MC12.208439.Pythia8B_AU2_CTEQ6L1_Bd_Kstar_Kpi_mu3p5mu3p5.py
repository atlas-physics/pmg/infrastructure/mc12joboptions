######################################################################
# Job options for Pythia8B_i generation of Bd -> K*0(K+Pi-)mu3p5mu3p5.
######################################################################

evgenConfig.description = "Signal Bd->Kstar(K+Pi-)mu3p5mu3p5"
evgenConfig.keywords    = [ "exclusiveB", "Bd", "dimuons" ]
evgenConfig.minevents   = 500

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

topAlg.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 7.' ]
topAlg.Pythia8B.QuarkPtCut      = 0.0
topAlg.Pythia8B.AntiQuarkPtCut  = 7.0
topAlg.Pythia8B.QuarkEtaCut     = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.6
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

topAlg.Pythia8B.Commands += [ '511:onIfMatch = 313 -13 13' ]
topAlg.Pythia8B.SignalPDGCodes = [   511,   313,  321, -211,   -13,    13 ]
topAlg.Pythia8B.SignalPtCuts   = [   0.0,   0.0,  0.5,  0.5,   0.0,   0.0 ] # no muon pT cut here - see trigger cuts below
topAlg.Pythia8B.SignalEtaCuts  = [ 102.5, 102.5,  2.6,  2.6, 102.5, 102.5 ] # no muon eta cut here - see trigger cuts below

topAlg.Pythia8B.NHadronizationLoops = 1

topAlg.Pythia8B.TriggerPDGCode     = 13
topAlg.Pythia8B.TriggerStatePtCut  = [ 3.5 ]
topAlg.Pythia8B.TriggerStateEtaCut = 2.6
topAlg.Pythia8B.MinimumCountPerCut = [ 2 ]
