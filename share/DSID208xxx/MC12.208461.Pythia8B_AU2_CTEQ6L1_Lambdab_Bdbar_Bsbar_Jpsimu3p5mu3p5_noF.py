########################################################################
# Job options for Pythia8B_i generation of B0/Bs/Lambda_b->J/psi(mumu)hh
########################################################################
evgenConfig.description = "Background B0/Bs/Lambda_b->J/psi(mumu)hh"
evgenConfig.keywords = ["BtoJpsi","dimuons","pentaquarks"]
evgenConfig.minevents = 1000

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

# Hard process
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
#
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.'] 

# Event selection
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.VetoDoubleCEvents = False
#
topAlg.Pythia8B.QuarkPtCut = 8.0
topAlg.Pythia8B.QuarkEtaCut = 3.5
topAlg.Pythia8B.AntiQuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkEtaCut = 1000.
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

#
# J/psi:
#
topAlg.Pythia8B.Commands += ['443:m0 = 3.096916']  # PDG 2015
topAlg.Pythia8B.Commands += ['443:mWidth = 0.0000929'] # PDG 2015
#
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']

#
# Lambda_b:
#
topAlg.Pythia8B.Commands += ['5122:m0 = 5.61951']  # PDG 2015
topAlg.Pythia8B.Commands += ['5122:tau0 = 0.4395'] # PDG 2015
#
# Lambda_b decays:
#
topAlg.Pythia8B.Commands += ['5122:onMode = 3']
#
topAlg.Pythia8B.Commands += ['5122:addChannel = 2 0.02 0 443 2212 -211']
#
topAlg.Pythia8B.Commands += ['5122:addChannel = 2 0.15 0 43122  443']
topAlg.Pythia8B.Commands += ['5122:addChannel = 2 0.07 0 53122  443']
topAlg.Pythia8B.Commands += ['5122:addChannel = 2 0.04 0  3126  443']
topAlg.Pythia8B.Commands += ['5122:addChannel = 2 0.03 0 13126  443']
topAlg.Pythia8B.Commands += ['5122:addChannel = 2 0.05 0 23124  443']
topAlg.Pythia8B.Commands += ['5122:addChannel = 2 0.19 0  3128  443']
topAlg.Pythia8B.Commands += ['5122:addChannel = 2 0.18 0 23126  443']
topAlg.Pythia8B.Commands += ['5122:addChannel = 2 0.02 0  3120  443']
topAlg.Pythia8B.Commands += ['5122:addChannel = 2 0.01 0 13120  443']
#
topAlg.Pythia8B.Commands += ['5122:addChannel = 2 0.12 0 443124 -321']
topAlg.Pythia8B.Commands += ['5122:addChannel = 2 0.12 0 443126 -321']

#
# Lambda*(1800) 1/2-
#
topAlg.Pythia8B.Commands += ['43122:new = L*(1800) L*(1800)_bar 2 0 0 1.800 0.300 1.9 2.523 0.']
topAlg.Pythia8B.Commands += ['43122:addChannel = 1 1. 0 2212 -321']
#
# Lambda*(1810) 1/2+
#
topAlg.Pythia8B.Commands += ['53122:new = L*(1810) L*(1810)_bar 2 0 0 1.810 0.150 1.9 2.523 0.']
topAlg.Pythia8B.Commands += ['53122:addChannel = 1 1. 0 2212 -321']
#
# Lambda*(1820) 5/2+
#
topAlg.Pythia8B.Commands += ['3126:new = L*(1820) L*(1820)_bar 6 0 0 1.820 0.080 1.9 2.523 0.']
topAlg.Pythia8B.Commands += ['3126:addChannel = 1 1. 0 2212 -321']
#
# Lambda*(1830) 5/2-
#
topAlg.Pythia8B.Commands += ['13126:new = L*(1830) L*(1830)_bar 6 0 0 1.830 0.095 1.9 2.523 0.']
topAlg.Pythia8B.Commands += ['13126:addChannel = 1 1. 0 2212 -321']
#
# Lambda*(1890) 3/2+
#
topAlg.Pythia8B.Commands += ['23124:new = L*(1890) L*(1890)_bar 4 0 0 1.890 0.100 1.9 2.523 0.']
topAlg.Pythia8B.Commands += ['23124:addChannel = 1 1. 0 2212 -321']
#
# Lambda*(2100) 7/2-
#
topAlg.Pythia8B.Commands += ['3128:new = L*(2100) L*(2100)_bar 8 0 0 2.100 0.200 1.9 2.523 0.']
topAlg.Pythia8B.Commands += ['3128:addChannel = 1 1. 0 2212 -321']
#
# Lambda*(2110) 5/2+
#
topAlg.Pythia8B.Commands += ['23126:new = L*(2110) L*(2110)_bar 6 0 0 2.110 0.200 1.9 2.523 0.']
topAlg.Pythia8B.Commands += ['23126:addChannel = 1 1. 0 2212 -321']
#
# Lambda*(2350) 9/2+
#
topAlg.Pythia8B.Commands += ['3120:new = L*(2350) L*(2350)_bar 10 0 0 2.350 0.150 1.9 2.523 0.']
topAlg.Pythia8B.Commands += ['3120:addChannel = 1 1. 0 2212 -321']
#
# Lambda*(2585) ?, let it be 9/2-
#
topAlg.Pythia8B.Commands += ['13120:new = L*(2585) L*(2585)_bar 10 0 0 2.585 0.200 1.9 2.523 0.']
topAlg.Pythia8B.Commands += ['13120:addChannel = 1 1. 0 2212 -321']

#
# P_c+(4380), 3/2-, with ATLAS mass and width
#
topAlg.Pythia8B.Commands += ['443124:new = Pc(4380)+ Pc(4380)bar- 4 0 0 4.286 0.111 4.036 5.125 0.']
topAlg.Pythia8B.Commands += ['443124:addChannel = 1 1. 0 443 2212']

#
# P_c+(4450), 5/2+, with ATLAS mass and width
#
topAlg.Pythia8B.Commands += ['443126:new = Pc(4450)+ Pc(4450)bar- 6 0 0 4.421 0.101 4.036 5.125 0.']
topAlg.Pythia8B.Commands += ['443126:addChannel = 1 1. 0 443 2212']

#
# B_d:
#
topAlg.Pythia8B.Commands += ['511:m0 = 5.27961']  # PDG 2015
topAlg.Pythia8B.Commands += ['511:tau0 = 0.4557'] # PDG 2015
#
# B_b decays:
#
topAlg.Pythia8B.Commands += ['511:onMode = 2']
#
# channels 850-869 have onMode = 2 in Pythia8 default
#
#topAlg.Pythia8B.Commands += ['511:850:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:851:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:852:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:853:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:854:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:855:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:856:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:857:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:858:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:859:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:860:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:861:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:862:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:863:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:864:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:865:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:866:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:867:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:868:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:869:onMode = 0']
#
# channels 870-889 have onMode = 3 in Pythia8 default
#
topAlg.Pythia8B.Commands += ['511:870:onMode = 0']
topAlg.Pythia8B.Commands += ['511:871:onMode = 0']
topAlg.Pythia8B.Commands += ['511:872:onMode = 0']
topAlg.Pythia8B.Commands += ['511:873:onMode = 0']
topAlg.Pythia8B.Commands += ['511:874:onMode = 0']
topAlg.Pythia8B.Commands += ['511:875:onMode = 0']
topAlg.Pythia8B.Commands += ['511:876:onMode = 0']
topAlg.Pythia8B.Commands += ['511:877:onMode = 0']
topAlg.Pythia8B.Commands += ['511:878:onMode = 0']
topAlg.Pythia8B.Commands += ['511:879:onMode = 0']
topAlg.Pythia8B.Commands += ['511:880:onMode = 0']
topAlg.Pythia8B.Commands += ['511:881:onMode = 0']
topAlg.Pythia8B.Commands += ['511:882:onMode = 0']
topAlg.Pythia8B.Commands += ['511:883:onMode = 0']
topAlg.Pythia8B.Commands += ['511:884:onMode = 0']
topAlg.Pythia8B.Commands += ['511:885:onMode = 0']
topAlg.Pythia8B.Commands += ['511:886:onMode = 0']
topAlg.Pythia8B.Commands += ['511:887:onMode = 0']
topAlg.Pythia8B.Commands += ['511:888:onMode = 0']
topAlg.Pythia8B.Commands += ['511:889:onMode = 0']
#
topAlg.Pythia8B.Commands += ['511:addChannel = 3 0.20 0  443 321 -211']
#
topAlg.Pythia8B.Commands += ['511:addChannel = 3 0.06 0  443 315']
topAlg.Pythia8B.Commands += ['511:addChannel = 3 0.26 0  443 10311']
topAlg.Pythia8B.Commands += ['511:addChannel = 3 0.23 0  443 30313']
topAlg.Pythia8B.Commands += ['511:addChannel = 3 0.03 0  443 317']
topAlg.Pythia8B.Commands += ['511:addChannel = 3 0.08 0  443 9020311']
topAlg.Pythia8B.Commands += ['511:addChannel = 3 0.02 0  443 319']
#
topAlg.Pythia8B.Commands += ['511:addChannel = 3 0.02 0  443 211 -211']
#
topAlg.Pythia8B.Commands += ['511:addChannel = 3 0.01 0  443 225']
topAlg.Pythia8B.Commands += ['511:addChannel = 3 0.05 0  443 100113']
topAlg.Pythia8B.Commands += ['511:addChannel = 3 0.02 0  443 30113']
#
topAlg.Pythia8B.Commands += ['511:addChannel = 3 0.02 0  443 321 -321']
#

#
# K*_2(1430)0 -> K+ pi-
#
topAlg.Pythia8B.Commands += ['315:m0 = 1.4256']  # PDG 2015
topAlg.Pythia8B.Commands += ['315:mWidth = 0.0985'] # PDG 2015
#
topAlg.Pythia8B.Commands += ['315:mMin = 1.475']
topAlg.Pythia8B.Commands += ['315:mMax = 2.183']
#
topAlg.Pythia8B.Commands += ['315:onMode = 2']
topAlg.Pythia8B.Commands += ['315:0:onMode = 1']

#
# K*_0(1430)0 -> K+ pi-
#
topAlg.Pythia8B.Commands += ['10311:m0 = 1.425']  # PDG 2015
topAlg.Pythia8B.Commands += ['10311:mWidth = 0.270'] # PDG 2015
#
topAlg.Pythia8B.Commands += ['10311:mMin = 1.475']
topAlg.Pythia8B.Commands += ['10311:mMax = 2.183']
#
topAlg.Pythia8B.Commands += ['10311:onMode = 2']
topAlg.Pythia8B.Commands += ['10311:0:onMode = 1']

#
# K*(1680)0 -> K+ pi-
#
topAlg.Pythia8B.Commands += ['30313:m0 = 1.717']  # PDG 2015
topAlg.Pythia8B.Commands += ['30313:mWidth = 0.322'] # PDG 2015
#
topAlg.Pythia8B.Commands += ['30313:mMin = 1.475']
topAlg.Pythia8B.Commands += ['30313:mMax = 2.183']
#
topAlg.Pythia8B.Commands += ['30313:onMode = 2']
topAlg.Pythia8B.Commands += ['30313:0:onMode = 1']

#
# K*_3(1780)0 -> K+ pi-
#
topAlg.Pythia8B.Commands += ['317:new = K*_3(1780)0 K*_3(1780)bar0 7 0 0 1.776 0.159 1.475 2.183 0.']
topAlg.Pythia8B.Commands += ['317:addChannel = 1 1. 0 321 -211']

#
# K*_0(1950)0 -> K+ pi-
#
topAlg.Pythia8B.Commands += ['9020311:new = K*_0(1950)0 K*_0(1950)bar0 1 0 0 1.945 0.201 1.475 2.183 0.']
topAlg.Pythia8B.Commands += ['9020311:addChannel = 1 1. 0 321 -211']

#
# K*_4(2045)0 -> K+ p-
#
topAlg.Pythia8B.Commands += ['319:new = K*_4(2045)0 K*_4(2045)bar0 9 0 0 2.045 0.198 1.475 2.183 0.']
topAlg.Pythia8B.Commands += ['319:addChannel = 1 1. 0 321 -211']

#
# f_2(1270)0 -> pi+ pi-
#
topAlg.Pythia8B.Commands += ['225:m0 = 1.2755']  # PDG 2015
topAlg.Pythia8B.Commands += ['225:mWidth = 0.1867'] # PDG 2015
#
topAlg.Pythia8B.Commands += ['225:mMin = 1.325']
topAlg.Pythia8B.Commands += ['225:mMax = 2.183']
#
topAlg.Pythia8B.Commands += ['225:onMode = off']
topAlg.Pythia8B.Commands += ['225:0:onMode = 1']

#
# rho(1450)0 -> pi+ pi-
#
topAlg.Pythia8B.Commands += ['100113:m0 = 1.465']  # PDG 2015
topAlg.Pythia8B.Commands += ['100113:mWidth = 0.400'] # PDG 2015
#
topAlg.Pythia8B.Commands += ['100113:mMin = 1.325']
topAlg.Pythia8B.Commands += ['100113:mMax = 2.183']
#
topAlg.Pythia8B.Commands += ['100113:onMode = off']
topAlg.Pythia8B.Commands += ['100113:0:onMode = 1']

#
# rho(1700)0 -> pi+ pi-
#
topAlg.Pythia8B.Commands += ['30113:new = rho(1700)0 void 3 0 0 1.720 0.250 1.325 2.183 0.']
topAlg.Pythia8B.Commands += ['30113:addChannel = 1 1. 0 211 -211']

#
# B_u:
#
topAlg.Pythia8B.Commands += ['521:m0 = 5.27929']  # PDG 2015
topAlg.Pythia8B.Commands += ['521:tau0 = 0.4911'] # PDG 2015
#

#
# B_s:
#
topAlg.Pythia8B.Commands += ['531:m0 = 5.36679']  # PDG 2015
topAlg.Pythia8B.Commands += ['531:tau0 = 0.4527'] # PDG 2015
#
# B_s decays:
#
topAlg.Pythia8B.Commands += ['531:onMode = 2']
#
# channels 242-243 have onMode = 2 in Pythia8 default
#
#topAlg.Pythia8B.Commands += ['531:242:onMode = 0']
#topAlg.Pythia8B.Commands += ['531:243:onMode = 0']
#
# channels 244-245 have onMode = 3 in Pythia8 default
#
topAlg.Pythia8B.Commands += ['531:244:onMode = 0']
topAlg.Pythia8B.Commands += ['531:245:onMode = 0']
#
topAlg.Pythia8B.Commands += ['531:addChannel = 3 0.16 0  443 321 -321']
topAlg.Pythia8B.Commands += ['531:addChannel = 3 0.18 0  443 335']
topAlg.Pythia8B.Commands += ['531:addChannel = 3 0.04 0  443 9020225']
topAlg.Pythia8B.Commands += ['531:addChannel = 3 0.09 0  443 100333']
topAlg.Pythia8B.Commands += ['531:addChannel = 3 0.11 0  443 20335']
topAlg.Pythia8B.Commands += ['531:addChannel = 3 0.05 0  443 9050225']
#
topAlg.Pythia8B.Commands += ['531:addChannel = 3 0.02 0  443 -321 211']
#
topAlg.Pythia8B.Commands += ['531:addChannel = 3 0.10 0  443 211 -211']
topAlg.Pythia8B.Commands += ['531:addChannel = 3 0.03 0  443 10221']
topAlg.Pythia8B.Commands += ['531:addChannel = 3 0.17 0  443 9030221']
topAlg.Pythia8B.Commands += ['531:addChannel = 3 0.05 0  443 30221']

#
# f_2(1525)0 -> K+ K-
#
topAlg.Pythia8B.Commands += ['335:m0 = 1.525']  # PDG 2015
topAlg.Pythia8B.Commands += ['335:mWidth = 0.073'] # PDG 2015
#
topAlg.Pythia8B.Commands += ['335:mMin = 1.500']
topAlg.Pythia8B.Commands += ['335:mMax = 2.270']
#
topAlg.Pythia8B.Commands += ['335:onMode = off']
topAlg.Pythia8B.Commands += ['335:0:onMode = 1']

#
# f_2(1640)0 -> K+ K-
#
topAlg.Pythia8B.Commands += ['9020225:new = f_2(1640)0 void 5 0 0 1.639 0.099 1.500 2.270 0.']
topAlg.Pythia8B.Commands += ['9020225:addChannel = 1 1. 0 321 -321']

#
# phi(1680)0 -> K+ K-
#
topAlg.Pythia8B.Commands += ['100333:new = phi(1680)0 void 3 0 0 1.680 0.150 1.500 2.270 0.']
topAlg.Pythia8B.Commands += ['100333:addChannel = 1 1. 0 321 -321']

#
# f_2(1750)0 -> K+ K- (LHCb from BELLE)
#
topAlg.Pythia8B.Commands += ['20335:new = f_2(1750)0 void 5 0 0 1.737 0.151 1.500 2.270 0.']
topAlg.Pythia8B.Commands += ['20335:addChannel = 1 1. 0 321 -321']

#
# f_2(1950)0 -> K+ K- (LHCb from BELLE)
#
topAlg.Pythia8B.Commands += ['9050225:new = f_2(1980)0 void 5 0 0 1.980 0.297 1.500 2.270 0.']
topAlg.Pythia8B.Commands += ['9050225:addChannel = 1 1. 0 321 -321']


#
# f_0(1370)0 -> pi+ pi-
#
topAlg.Pythia8B.Commands += ['10221:m0 = 1.350']  # Pythia8
topAlg.Pythia8B.Commands += ['10221:mWidth = 0.200'] # Pythia8
#
topAlg.Pythia8B.Commands += ['10221:mMin = 1.325']
topAlg.Pythia8B.Commands += ['10221:mMax = 2.270']
#
topAlg.Pythia8B.Commands += ['10221:onMode = off']
topAlg.Pythia8B.Commands += ['10221:3:onMode = 1']

#
# f_0(1500) -> pi+ pi-
#
topAlg.Pythia8B.Commands += ['9030221:new = f_0(1500)0 void 1 0 0 1.504 0.109 1.325 2.270 0.']
topAlg.Pythia8B.Commands += ['9030221:addChannel = 1 1. 0 211 -211']

#
# f_0(1790) -> pi+ pi- (LHCb from BES)
#
topAlg.Pythia8B.Commands += ['30221:new = f_0(1790)0 void 1 0 0 1.790 0.270 1.325 2.270 0.']
topAlg.Pythia8B.Commands += ['30221:addChannel = 1 1. 0 211 -211']

#theApp.ReflexPluginDebugLevel = 1000

topAlg.Pythia8B.OutputLevel = INFO
#topAlg.Pythia8B.OutputLevel = DEBUG
topAlg.Pythia8B.NHadronizationLoops = 2


topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.TriggerOppositeCharges = True
topAlg.Pythia8B.MinimumCountPerCut = [2]

#include("MC12JobOptions/ParentsTracksFilter.py")
##include("Generators/GeneratorFilters/share/ParentsTracksFilter.py")
#topAlg.ParentsTracksFilter.PDGParent  = [5122,511,531]
#topAlg.ParentsTracksFilter.PtMinParent =  11000.
#topAlg.ParentsTracksFilter.EtaRangeParent = 2.3
#topAlg.ParentsTracksFilter.PtMinLeptons = 3500.
#topAlg.ParentsTracksFilter.EtaRangeLeptons = 2.5
#topAlg.ParentsTracksFilter.PtMinHadrons = 1600.
#topAlg.ParentsTracksFilter.EtaRangeHadrons = 2.7
#topAlg.ParentsTracksFilter.NumMinTracks = 4
#topAlg.ParentsTracksFilter.NumMaxTracks = 4
#topAlg.ParentsTracksFilter.NumMinLeptons = 2
#topAlg.ParentsTracksFilter.NumMaxLeptons = 2
#topAlg.ParentsTracksFilter.NumMinOthers = 0
#topAlg.ParentsTracksFilter.NumMaxOthers = 0
