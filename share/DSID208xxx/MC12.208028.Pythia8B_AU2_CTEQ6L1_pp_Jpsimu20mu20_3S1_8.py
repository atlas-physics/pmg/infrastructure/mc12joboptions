##############################################################
# Job options fragment for pp->J/psi(mu20mu20)X  
##############################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "Inclusive pp->ccbar 3S1_8 ->J/psi(mu20mu20) production"
evgenConfig.keywords = ["charmonium","dimuons","octet"]
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8B_Charmonium_Common.py") 
topAlg.Pythia8B.Commands += ['Charmonium:all = off']
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 40.'] 
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [443,-13,13]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [20.]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [2]

topAlg.Pythia8B.Commands+=['Charmonium:gg2QQbar[3S1(8)]g = on']
topAlg.Pythia8B.Commands+=['Charmonium:qg2QQbar[3S1(8)]q = on']
topAlg.Pythia8B.Commands+=['Charmonium:qqbar2QQbar[3S1(8)]g = on']
