##################################################################
# Job options fragment for Bs->mu5.5mu5.5 for trigger LS1 studies.
##################################################################

evgenConfig.description = "Exclusive Bs->mu5.5mu5.5 production"
evgenConfig.keywords    = [ "exclusiveB", "Bs", "dimuons" ]
evgenConfig.minevents   = 1000

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
#include("MC12JobOptions/Pythia8B_Photos.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

topAlg.Pythia8B.Commands += [ 'PhaseSpace:pTHatMin = 10.' ]
topAlg.Pythia8B.Commands += [ '531:addChannel = 2 1.0 0 -13 13' ]
#topAlg.Pythia8B.Commands += [ '531:onMode = off' ]
topAlg.Pythia8B.Commands += [ '531:onIfMatch = -13 13' ]
topAlg.Pythia8B.SignalPDGCodes = [ 531, -13,13 ]

topAlg.Pythia8B.TriggerPDGCode     = 13
topAlg.Pythia8B.TriggerStateEtaCut = 2.7
topAlg.Pythia8B.TriggerStatePtCut  = [ 5.5 ]
topAlg.Pythia8B.MinimumCountPerCut = [ 2 ]

topAlg.Pythia8B.QuarkPtCut                = 0.
topAlg.Pythia8B.AntiQuarkPtCut            = 10.
topAlg.Pythia8B.QuarkEtaCut               = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut           = 3.
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents         = True
topAlg.Pythia8B.NHadronizationLoops       = 4
