evgenConfig.description = "bb->mu4mu4 production, only b->c->mu+, bbar->cbar->mu-"
evgenConfig.keywords = ["inclusive","bbbar","dimuons"]
evgenConfig.minevents = 200

f = open("AbAbbar.DEC","w")
f.write("""
Alias MyD0 D0 
Alias MyD+ D+ 
Alias MyD_s+ D_s+ 
Alias MyLambda_c+ Lambda_c+ 
Alias MyD*+ D*+ 
Alias MyD_2*0 D_2*0 
Alias MyD'_10 D'_10 
Alias MyD_2*+ D_2*+ 
Alias MyD(2S)+ D(2S)+ 
Alias MyD_0*0 D_0*0 
Alias MyLambda_c(2593)+ Lambda_c(2593)+ 
Alias MyD_10 D_10 
Alias MyD_0*+ D_0*+ 
Alias MyD*(2S)+ D*(2S)+ 
Alias MyD_s0*+ D_s0*+ 
Alias MyD_s*+ D_s*+ 
Alias MyD*(2S)0 D*(2S)0 
Alias MyD*0 D*0 
Alias MyD_s1+ D_s1+ 
Alias MyD_s2*+ D_s2*+ 
Alias MyD'_1+ D'_1+ 
Alias MyXi_c+ Xi_c+ 
Alias MySigma_c+ Sigma_c+ 
Alias MySigma_c*0 Sigma_c*0 
Alias MySigma_c++ Sigma_c++ 
Alias MyXi'_c0 Xi'_c0 
Alias MyXi_c0 Xi_c0 
Alias MyD(2S)0 D(2S)0 
Alias MyXi_c*0 Xi_c*0 
Alias MyXi_c*+ Xi_c*+ 
Alias MyLambda_c(2625)+ Lambda_c(2625)+ 
Alias MyXi'_c+ Xi'_c+ 
Alias MyOmega_c0 Omega_c0 
Alias MySigma_c0 Sigma_c0 
Alias MyD_1+ D_1+ 
Alias MySigma_c*+ Sigma_c*+ 
Alias MySigma_c*++ Sigma_c*++ 

Alias Myanti-D0 anti-D0 
Alias MyD- D- 
Alias MyD_s- D_s- 
Alias Myanti-Lambda_c- anti-Lambda_c- 
Alias MyD*- D*- 
Alias Myanti-D_2*0 anti-D_2*0 
Alias Myanti-D'_10 anti-D'_10 
Alias MyD_2*- D_2*- 
Alias MyD(2S)- D(2S)- 
Alias Myanti-D_0*0 anti-D_0*0 
Alias Myanti-Lambda_c(2593)- anti-Lambda_c(2593)- 
Alias Myanti-D_10 anti-D_10 
Alias MyD_0*- D_0*- 
Alias MyD*(2S)- D*(2S)- 
Alias MyD_s0*- D_s0*- 
Alias MyD_s*- D_s*- 
Alias Myanti-D*(2S)0 anti-D*(2S)0 
Alias Myanti-D*0 anti-D*0 
Alias MyD_s1- D_s1- 
Alias MyD_s2*- D_s2*- 
Alias MyD'_1- D'_1- 
Alias Myanti-Xi_c- anti-Xi_c- 
Alias Myanti-Sigma_c- anti-Sigma_c- 
Alias Myanti-Sigma_c*0 anti-Sigma_c*0 
Alias Myanti-Sigma_c-- anti-Sigma_c-- 
Alias Myanti-Xi'_c0 anti-Xi'_c0 
Alias Myanti-Xi_c0 anti-Xi_c0 
Alias Myanti-D(2S)0 anti-D(2S)0 
Alias Myanti-Xi_c*0 anti-Xi_c*0 
Alias Myanti-Xi_c*- anti-Xi_c*- 
Alias Myanti-Lambda_c(2625)- anti-Lambda_c(2625)- 
Alias Myanti-Xi'_c- anti-Xi'_c- 
Alias Myanti-Omega_c0 anti-Omega_c0 
Alias Myanti-Sigma_c0 anti-Sigma_c0 
Alias MyD_1- D_1- 
Alias Myanti-Sigma_c*- anti-Sigma_c*- 
Alias Myanti-Sigma_c*-- anti-Sigma_c*-- 

ChargeConj MyD0                Myanti-D0 
ChargeConj MyD+                MyD-
ChargeConj MyD_s+              MyD_s- 
ChargeConj MyLambda_c+         Myanti-Lambda_c- 
ChargeConj MyD*+               MyD*- 
ChargeConj MyD_2*0             Myanti-D_2*0 
ChargeConj MyD'_10             Myanti-D'_10 
ChargeConj MyD_2*+             MyD_2*- 
ChargeConj MyD(2S)+            MyD(2S)- 
ChargeConj MyD_0*0             Myanti-D_0*0 
ChargeConj MyLambda_c(2593)+   Myanti-Lambda_c(2593)- 
ChargeConj MyD_10              Myanti-D_10 
ChargeConj MyD_0*+             MyD_0*- 
ChargeConj MyD*(2S)+           MyD*(2S)- 
ChargeConj MyD_s0*+            MyD_s0*- 
ChargeConj MyD_s*+             MyD_s*- 
ChargeConj MyD*(2S)0           Myanti-D*(2S)0 
ChargeConj MyD*0               Myanti-D*0 
ChargeConj MyD_s1+             MyD_s1- 
ChargeConj MyD_s2*+            MyD_s2*- 
ChargeConj MyD'_1+             MyD'_1- 
ChargeConj MyXi_c+             Myanti-Xi_c- 
ChargeConj MySigma_c+          Myanti-Sigma_c- 
ChargeConj MySigma_c*0         Myanti-Sigma_c*0 
ChargeConj MySigma_c++         Myanti-Sigma_c-- 
ChargeConj MyXi'_c0            Myanti-Xi'_c0 
ChargeConj MyXi_c0             Myanti-Xi_c0 
ChargeConj MyD(2S)0            Myanti-D(2S)0 
ChargeConj MyXi_c*0            Myanti-Xi_c*0 
ChargeConj MyXi_c*+            Myanti-Xi_c*- 
ChargeConj MyLambda_c(2625)+   Myanti-Lambda_c(2625)- 
ChargeConj MyXi'_c+            Myanti-Xi'_c- 
ChargeConj MyOmega_c0          Myanti-Omega_c0 
ChargeConj MySigma_c0          Myanti-Sigma_c0 
ChargeConj MyD_1+              MyD_1- 
ChargeConj MySigma_c*+         Myanti-Sigma_c*- 
ChargeConj MySigma_c*++        Myanti-Sigma_c*--       

Decay B0
  0.050100000 MyD*- e+ nu_e PHOTOS HQET 0.77 1.33 0.92; #[Reconstructed PDG2011]
  0.021700000 MyD- e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.0054 MyD_1- e+ nu_e PHOTOS ISGW2;
  0.0020 MyD_0*- e+ nu_e PHOTOS ISGW2;
  0.0050 MyD'_1- e+ nu_e PHOTOS ISGW2;
  0.0022 MyD_2*- e+ nu_e PHOTOS ISGW2;
  0.0003 MyD*- pi0 e+ nu_e PHOTOS GOITY_ROBERTS;
  0.004900000 Myanti-D*0 pi- e+ nu_e PHOTOS GOITY_ROBERTS; #[Reconstructed PDG2011]
  0.0010 MyD- pi0 e+ nu_e PHOTOS GOITY_ROBERTS;
  0.0000 Myanti-D0 pi- e+ nu_e PHOTOS GOITY_ROBERTS;
  0.015000000 MyD*- tau+ nu_tau ISGW2; #[Reconstructed PDG2011]
  0.011000000 MyD- tau+ nu_tau ISGW2; #[Reconstructed PDG2011]
  0.0013 MyD_1- tau+ nu_tau ISGW2;
  0.0013 MyD_0*- tau+ nu_tau ISGW2;
  0.0020 MyD'_1- tau+ nu_tau ISGW2;
  0.0020 MyD_2*- tau+ nu_tau ISGW2;
  0.000000 MyD(2S)- e+ nu_e PHOTOS ISGW2;
  0.000000 D*(2S)- e+ nu_e PHOTOS ISGW2;
  0.000000 MyD(2S)- tau+ nu_tau ISGW2;
  0.000000 D*(2S)- tau+ nu_tau ISGW2;
  0.000030000 MyD_s- K+ PHSP; #[Reconstructed PDG2011]
  0.000021900 MyD_s*- K+ SVS; #[Reconstructed PDG2011]
  0.000035000 K*+ MyD_s- SVS; #[Reconstructed PDG2011]
  0.000032000 MyD_s*- K*+ SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0; #[Reconstructed PDG2011]
  0.000211000 MyD- D+ PHSP; #[Reconstructed PDG2011]
  0.000305 MyD*- D+ SVS;
  0.000610000 D*+ MyD- SVS; #[Reconstructed PDG2011]
  0.000820000 MyD*- D*+ SVV_HELAMP 0.56 0.0 0.96 0.0 0.47 0.0; #[Reconstructed PDG2011]
  0.007200000 MyD- D_s+ PHSP; #[Reconstructed PDG2011]
  0.008000000 MyD*- D_s+ SVS; #[Reconstructed PDG2011]
  0.007400000 D_s*+ MyD- SVS; #[Reconstructed PDG2011]
  0.017700000 D_s*+ MyD*- SVV_HELAMP 0.4904 0.0 0.7204 0.0 0.4904 0.0; #[Reconstructed PDG2011]
  0.0006 MyD'_1- D_s+ SVS;
  0.0012 MyD'_1- D_s*+ SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0012 MyD_1- D_s+ SVS;
  0.0024 MyD_1- D_s*+ SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0042 MyD_2*- D_s+ STS;
  0.0040 MyD_2*- D_s*+ PHSP;
  0.0018 D_s+ MyD- pi0 PHSP;
  0.0037 D_s+ Myanti-D0 pi- PHSP;
  0.0018 D_s*+ MyD- pi0 PHSP;
  0.0037 D_s*+ Myanti-D0 pi- PHSP;
  0.0030 D_s+ MyD- pi- pi+ PHSP;
  0.0022 D_s+ MyD- pi0 pi0 PHSP;
  0.0022 D_s+ Myanti-D0 pi- pi0 PHSP;
  0.0030 D_s*+ MyD- pi- pi+ PHSP;
  0.0022 D_s*+ MyD- pi0 pi0 PHSP;
  0.0022 D_s*+ Myanti-D0 pi- pi0 PHSP;
  0.001700000 MyD- D0 K+ PHSP; #[Reconstructed PDG2011]
  0.004600000 MyD- D*0 K+ PHSP; #[Reconstructed PDG2011]
  0.003100000 MyD*- D0 K+ PHSP; #[Reconstructed PDG2011]
  0.011800000 MyD*- D*0 K+ PHSP; #[Reconstructed PDG2011]
  0.0015 MyD- D+ K0 PHSP;
  0.0018 MyD*- D+ K0 PHSP;
  0.0047 MyD- D*+ K0 PHSP;
  0.007800000 MyD*- D*+ K0 PHSP; #[Reconstructed PDG2011]
  0.0005 D0 Myanti-D0 K0 PHSP;
  0.0005 D*0 Myanti-D0 K0 PHSP;
  0.0015 D0 Myanti-D*0 K0 PHSP;
  0.0015 D*0 Myanti-D*0 K0 PHSP;
  0.0025 MyD- D0 K*+ PHSP;
  0.0025 MyD*- D0 K*+ PHSP;
  0.0025 MyD- D*0 K*+ PHSP;
  0.0050 MyD*- D*0 K*+ PHSP;
  0.0025 MyD- D+ K*0 PHSP;
  0.0025 MyD*- D+ K*0 PHSP;
  0.0025 MyD- D*+ K*0 PHSP;
  0.0050 MyD*- D*+ K*0 PHSP;
  0.0005 D0 Myanti-D0 K*0 PHSP;
  0.0005 D0 Myanti-D*0 K*0 PHSP;
  0.0005 D*0 Myanti-D0 K*0 PHSP;
  0.0010 D*0 Myanti-D*0 K*0 PHSP;
  0.002760000 MyD*- pi+ SVS; #[Reconstructed PDG2011]
  0.002680000 MyD- pi+ PHSP; #[Reconstructed PDG2011]
  0.007110000 rho+ MyD- SVS; #[Reconstructed PDG2011]
  0.006800000 rho+ MyD*- SVV_HELAMP 0.317 0.19 0.936 0.0 0.152 1.47; #[Reconstructed PDG2011]
  0.0005 MyD- pi+ pi0 PHSP;
  0.008200000 MyD*- pi+ pi0 PHSP; #[Reconstructed PDG2011]
  0.000840000 Myanti-D0 pi- pi+ PHSP; #[Reconstructed PDG2011]
  0.000620000 Myanti-D*0 pi- pi+ PHSP; #[Reconstructed PDG2011]
  0.0005 Myanti-D*0 pi0 pi0 PHSP;
  0.006000000 a_1+ MyD- SVS; #[Reconstructed PDG2011]
  0.000000000 MyD- rho0 pi+ PHSP; #[Reconstructed PDG2011]
  0.0011 MyD- rho+ pi0 PHSP;
  0.002000000 MyD- pi- pi+ pi+ PHSP; #[Reconstructed PDG2011]
  0.0010 MyD- pi0 pi+ pi0 PHSP;
  0.0010 Myanti-D0 pi- pi+ pi0 PHSP;
  0.0001 Myanti-D0 pi0 pi0 pi0 PHSP;
  0.013000000 MyD*- a_1+ SVV_HELAMP 0.458 0.0 0.866 0.0 0.458 0.0; #[Reconstructed PDG2011]
  0.005700000 MyD*- rho0 pi+ PHSP; #[Reconstructed PDG2011]
  0.0010 MyD*- rho+ pi0 PHSP;
  0.0000 MyD*- pi- pi+ pi+ PHSP;
  0.0010 MyD*- pi0 pi+ pi0 PHSP;
  0.0010 Myanti-D*0 pi- pi+ pi0 PHSP;
  0.0001 Myanti-D*0 pi0 pi0 pi0 PHSP;
  0.0001 MyD_1- pi+ SVS;
  0.0001 MyD'_1- pi+ SVS;
  0.00006 MyD_0*- pi+ PHSP;
  0.000215 MyD_2*- pi+ STS;
  0.0004 MyD_1- rho+ PHSP;
  0.0013 MyD'_1- rho+ PHSP;
  0.0022 MyD_2*- rho+ PHSP;
  0.000214000 MyD*- K+ SVS; #[Reconstructed PDG2011]
  0.000200000 MyD- K+ PHSP; #[Reconstructed PDG2011]
  0.000330000 MyD*- K*+ SVV_HELAMP 0.283 0.0 0.932 0.0 0.228 0.0; #[Reconstructed PDG2011]
  0.000450000 K*+ MyD- SVS; #[Reconstructed PDG2011]
  0.000036 Myanti-D*0 anti-K0 SVS;
  0.000052 Myanti-D0 anti-K0 PHSP;
  0.000042000 K*0 Myanti-D0 SVS; #[Reconstructed PDG2011]
  0.00004 Myanti-D*0 K*0 SVV_HELAMP 1. 0. 1. 0. 1. 0.;
  0.000261000 Myanti-D0 pi0 PHSP; #[Reconstructed PDG2011]
  0.000170000 Myanti-D*0 pi0 SVS; #[Reconstructed PDG2011]
  0.000320000 rho0 Myanti-D0 SVS; #[Reconstructed PDG2011]
  0.00029 Myanti-D*0 rho0 SVV_HELAMP 0.283 0.0 0.932 0.0 0.228 0.0;
  0.000202000 Myanti-D0 eta PHSP; #[Reconstructed PDG2011]
  0.000200000 Myanti-D*0 eta SVS; #[Reconstructed PDG2011]
  0.000125 Myanti-D0 eta' PHSP;
  0.000123000 Myanti-D*0 eta' SVS; #[Reconstructed PDG2011]
  0.000259000 omega Myanti-D0 SVS; #[Reconstructed PDG2011]
  0.000330000 Myanti-D*0 omega SVV_HELAMP 0.283 0.0 0.932 0.0 0.228 0.0; #[Reconstructed PDG2011]
  0.0016 D_s0*+ MyD- PHSP;
  0.0015 MyD*- D_s0*+ SVS;
  0.003500000 D_s1+ MyD- SVS; #[Reconstructed PDG2011]
  0.009300000 MyD*- D_s1+ SVV_HELAMP 0.4904 0. 0.7204 0. 0.4904 0.; #[Reconstructed PDG2011]
  0.000880000 MyD- K+ anti-K*0 PHSP; #[Reconstructed PDG2011]
  0.001290000 MyD*- K+ anti-K*0 PHSP; #[Reconstructed PDG2011]
  0.002800000 MyD- omega pi+ PHSP; #[Reconstructed PDG2011]
  0.002890000 MyD*- omega pi+ PHSP; #[Reconstructed PDG2011]
  0.000490000 MyD- K0 pi+ PHSP; #[Reconstructed PDG2011]
  0.000000000 MyD*- K0 pi+ PHSP; #[Reconstructed PDG2011]
  0.00043 D'_s1+ MyD- PHSP;
  0.00083 D'_s1+ MyD*- PHSP;
  0.017600000 MyD*- pi+ pi+ pi- pi0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.004700000 MyD*- pi+ pi+ pi+ pi- pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000110000 MyD_s- pi+ K0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000052000 Myanti-D0 K0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000046000 Myanti-D0 K+ pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000036000 Myanti-D*0 K0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000170000 K0 D0 Myanti-D0 pi0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000114000 Myanti-D0 p+ anti-p- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000028000 MyD_s- anti-Lambda0 p+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000103000 Myanti-D*0 p+ anti-p- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.001500000 MyD*- p+ anti-n0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000338000 MyD- p+ anti-p- pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000500000 MyD*- p+ anti-p- pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000020000 Myanti-Lambda_c- p+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000630000 Myanti-Lambda_c- p+ pi+ pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000120000 Myanti-Sigma_c*-- p+ pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000150000 Myanti-Sigma_c0 p+ pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000220000 Myanti-Sigma_c-- p+ pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay B+
  0.056800000 Myanti-D*0 e+ nu_e PHOTOS HQET 0.77 1.33 0.92; #[Reconstructed PDG2011]
  0.022300000 Myanti-D0 e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.0040 Myanti-D_10 e+ nu_e PHOTOS ISGW2;
  0.0024 Myanti-D_0*0 e+ nu_e PHOTOS ISGW2;
  0.0007 Myanti-D'_10 e+ nu_e PHOTOS ISGW2;
  0.0018 Myanti-D_2*0 e+ nu_e PHOTOS ISGW2;
  0.006100000 MyD*- pi+ e+ nu_e PHOTOS GOITY_ROBERTS; #[Reconstructed PDG2011]
  0.0003 Myanti-D*0 pi0 e+ nu_e PHOTOS GOITY_ROBERTS;
  0.0000 MyD- pi+ e+ nu_e PHOTOS GOITY_ROBERTS;
  0.0010 Myanti-D0 pi0 e+ nu_e PHOTOS GOITY_ROBERTS;
  0.020000000 Myanti-D*0 tau+ nu_tau ISGW2; #[Reconstructed PDG2011]
  0.007000000 Myanti-D0 tau+ nu_tau ISGW2; #[Reconstructed PDG2011]
  0.0013 Myanti-D_10 tau+ nu_tau ISGW2;
  0.0013 Myanti-D_0*0 tau+ nu_tau ISGW2;
  0.0020 Myanti-D'_10 tau+ nu_tau ISGW2;
  0.0020 Myanti-D_2*0 tau+ nu_tau ISGW2;
  0.000000 Myanti-D(2S)0 e+ nu_e PHOTOS ISGW2;
  0.000000 Myanti-D*(2S)0 e+ nu_e PHOTOS ISGW2;
  0.000000 Myanti-D(2S)0 tau+ nu_tau ISGW2;
  0.000000 Myanti-D*(2S)0 tau+ nu_tau ISGW2;
  0.010000000 Myanti-D0 D_s+ PHSP; #[Reconstructed PDG2011]
  0.008200000 Myanti-D*0 D_s+ SVS; #[Reconstructed PDG2011]
  0.007600000 D_s*+ Myanti-D0 SVS; #[Reconstructed PDG2011]
  0.017100000 D_s*+ Myanti-D*0 SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0; #[Reconstructed PDG2011]
  0.0006 Myanti-D'_10 D_s+ SVS;
  0.0012 Myanti-D'_10 D_s*+ SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0012 Myanti-D_10 D_s+ SVS;
  0.0024 Myanti-D_10 D_s*+ SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0042 Myanti-D_2*0 D_s+ STS;
  0.0040 Myanti-D_2*0 D_s*+ PHSP;
  0.0036 D_s+ MyD- pi+ PHSP;
  0.0018 D_s+ Myanti-D0 pi0 PHSP;
  0.0037 D_s*+ MyD- pi+ PHSP;
  0.0018 D_s*+ Myanti-D0 pi0 PHSP;
  0.0033 D_s+ MyD- pi+ pi0 PHSP;
  0.0033 D_s+ Myanti-D0 pi+ pi- PHSP;
  0.0008 D_s+ Myanti-D0 pi0 pi0 PHSP;
  0.0033 D_s*+ MyD- pi+ pi0 PHSP;
  0.0033 D_s*+ Myanti-D0 pi+ pi- PHSP;
  0.0008 D_s*+ Myanti-D0 pi0 pi0 PHSP;
  0.0017 Myanti-D0 D+ K0 PHSP;
  0.0052 Myanti-D0 D*+ K0 PHSP;
  0.0031 Myanti-D*0 D+ K0 PHSP;
  0.007800000 Myanti-D*0 D*+ K0 PHSP; #[Reconstructed PDG2011]
  0.002100000 Myanti-D0 D0 K+ PHSP; #[Reconstructed PDG2011]
  0.0018 Myanti-D*0 D0 K+ PHSP;
  0.004700000 Myanti-D0 D*0 K+ PHSP; #[Reconstructed PDG2011]
  0.005300000 Myanti-D*0 D*0 K+ PHSP; #[Reconstructed PDG2011]
  0.0005 D+ MyD- K+ PHSP;
  0.0005 D*+ MyD- K+ PHSP;
  0.001500000 D+ MyD*- K+ PHSP; #[Reconstructed PDG2011]
  0.0015 D*+ MyD*- K+ PHSP;
  0.0025 Myanti-D0 D+ K*0 PHSP;
  0.0025 Myanti-D*0 D+ K*0 PHSP;
  0.0025 Myanti-D0 D*+ K*0 PHSP;
  0.0050 Myanti-D*0 D*+ K*0 PHSP;
  0.0025 Myanti-D0 D0 K*+ PHSP;
  0.0025 Myanti-D*0 D0 K*+ PHSP;
  0.0025 Myanti-D0 D*0 K*+ PHSP;
  0.0050 Myanti-D*0 D*0 K*+ PHSP;
  0.0005 D+ MyD- K*+ PHSP;
  0.0005 D*+ MyD- K*+ PHSP;
  0.0005 D+ MyD*- K*+ PHSP;
  0.0010 D*+ MyD*- K*+ PHSP;
  0.000380000 D+ Myanti-D0 PHSP; #[Reconstructed PDG2011]
  0.000390000 D*+ Myanti-D0 SVS; #[Reconstructed PDG2011]
  0.000630000 Myanti-D*0 D+ SVS; #[Reconstructed PDG2011]
  0.000810000 Myanti-D*0 D*+ SVV_HELAMP 0.56 0.0 0.96 0.0 0.47 0.0; #[Reconstructed PDG2011]
  0.005190000 Myanti-D*0 pi+ SVS; #[Reconstructed PDG2011]
  0.004840000 Myanti-D0 pi+ PHSP; #[Reconstructed PDG2011]
  0.013400000 rho+ Myanti-D0 SVS; #[Reconstructed PDG2011]
  0.009800000 Myanti-D*0 rho+ SVV_HELAMP 0.283 1.13 0.932 0.0 0.228 0.95; #[Reconstructed PDG2011]
  0.0005 Myanti-D0 pi0 pi+ PHSP;
  0.0005 Myanti-D*0 pi0 pi+ PHSP;
  0.001070000 MyD- pi+ pi+ PHSP; #[Reconstructed PDG2011]
  0.001350000 MyD*- pi+ pi+ PHSP; #[Reconstructed PDG2011]
  0.004000000 a_1+ Myanti-D0 SVS; #[Reconstructed PDG2011]
  0.000200000 Myanti-D0 rho0 pi+ PHSP; #[Reconstructed PDG2011]
  0.006800000 Myanti-D0 pi- pi+ pi+ PHSP; #[Reconstructed PDG2011]
  0.019000000 Myanti-D*0 a_1+ SVV_HELAMP 0.458 0.0 0.866 0.0 0.200 0.0; #[Reconstructed PDG2011]
  0.00042 Myanti-D*0 rho0 pi+ PHSP;
  0.010300000 Myanti-D*0 pi- pi+ pi+ PHSP; #[Reconstructed PDG2011]
  0.0020 MyD- rho+ pi+ PHSP;
  0.0020 MyD- pi0 pi+ pi+ PHSP;
  0.0020 MyD*- rho+ pi+ PHSP;
  0.015000000 MyD*- pi0 pi+ pi+ PHSP; #[Reconstructed PDG2011]
  0.0005 Myanti-D*0 rho+ pi0 PHSP;
  0.0005 Myanti-D*0 pi+ pi0 pi0 PHSP;
  0.000876 Myanti-D_10 pi+ SVS;
  0.0005 Myanti-D'_10 pi+ SVS;
  0.00052 Myanti-D_2*0 pi+ STS;
  0.0007 Myanti-D_10 rho+ PHSP;
  0.0022 Myanti-D'_10 rho+ PHSP;
  0.0038 Myanti-D_2*0 rho+ PHSP;
  0.00061 Myanti-D_0*0 pi+ PHSP;
  0.000368000 Myanti-D0 K+ PHSP; #[Reconstructed PDG2011]
  0.000421000 Myanti-D*0 K+ SVS; #[Reconstructed PDG2011]
  0.000530000 K*+ Myanti-D0 SVS; #[Reconstructed PDG2011]
  0.000810000 Myanti-D*0 K*+ SVV_HELAMP 0.283 0.0 0.932 0.0 0.228 0.0; #[Reconstructed PDG2011]
  0.00075 Myanti-D0 D_s0*+ PHSP;
  0.00090 Myanti-D*0 D_s0*+ SVS;
  0.003100000 D_s1+ Myanti-D0 SVS; #[Reconstructed PDG2011]
  0.012000000 Myanti-D*0 D_s1+ SVV_HELAMP 0.4904 0.0 0.7204 0.0 0.4904 0.0; #[Reconstructed PDG2011]
  0.000550000 Myanti-D0 K+ anti-K0 PHSP; #[Reconstructed PDG2011]
  0.000750000 Myanti-D0 K+ anti-K*0 PHSP; #[Reconstructed PDG2011]
  0.00150 Myanti-D*0 K+ anti-K*0 PHSP;
  0.002750000 Myanti-D0 omega pi+ PHSP; #[Reconstructed PDG2011]
  0.004500000 Myanti-D*0 omega pi+ PHSP; #[Reconstructed PDG2011]
  0.00045 Myanti-D0 D'_s1+ PHSP;
  0.00094 Myanti-D*0 D'_s1+ PHSP;
  0.001500000 Myanti-D*0 K+ K*0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.018000000 Myanti-D*0 pi- pi+ pi+ pi0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.005700000 Myanti-D*0 pi+ pi+ pi+ pi- pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.002600000 MyD*- pi+ pi+ pi+ pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000180000 MyD_s- pi+ K+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000145000 MyD_s*- pi+ K+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000011000 MyD_s- K+ K+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000280000 Myanti-Lambda_c- p+ pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.001800000 Myanti-Lambda_c- p+ pi+ pi0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.002300000 Myanti-Lambda_c- p+ pi+ pi+ pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000035000 Myanti-Sigma_c0 p+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000440000 Myanti-Sigma_c0 p+ pi0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000440000 Myanti-Sigma_c0 p+ pi- pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000280000 Myanti-Sigma_c-- p+ pi+ pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay B_s0
  0.0210 MyD_s- e+ nu_e PHOTOS ISGW2;
  0.0490 MyD_s*- e+ nu_e PHOTOS ISGW2;
  0.0040 MyD_s1- e+ nu_e PHOTOS ISGW2;
  0.0040 MyD_s0*- e+ nu_e PHOTOS ISGW2;
  0.0070 MyD_s2*- e+ nu_e PHOTOS ISGW2;
  0.0080 MyD_s- tau+ nu_tau ISGW2;
  0.0160 MyD_s*- tau+ nu_tau ISGW2;
  0.0018 MyD_s1- tau+ nu_tau ISGW2;
  0.0018 MyD_s0*- tau+ nu_tau ISGW2;
  0.0028 MyD_s2*- tau+ nu_tau ISGW2;
  0.010400000 MyD_s- D_s+ PHSP; #[Reconstructed PDG2011]
  0.0099 D_s*+ MyD_s- SVS;
  0.0099 MyD_s*- D_s+ SVS;
  0.0197 MyD_s*- D_s*+ SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0096 D_s+ MyD- anti-K0 PHSP;
  0.0096 D_s*+ MyD- anti-K0 PHSP;
  0.0096 D_s*+ Myanti-D0 K- PHSP;
  0.0024 D_s+ MyD- pi0 anti-K0 PHSP;
  0.0048 D_s+ Myanti-D0 pi- anti-K0 PHSP;
  0.0048 D_s+ MyD- pi+ K- PHSP;
  0.0024 D_s+ Myanti-D0 pi0 K- PHSP;
  0.0024 D_s*+ MyD- pi0 anti-K0 PHSP;
  0.0048 D_s*+ Myanti-D0 pi- anti-K0 PHSP;
  0.0048 D_s*+ MyD- pi+ K- PHSP;
  0.0024 D_s*+ Myanti-D0 pi0 K- PHSP;
  0.0150 MyD_s*- D*0 K+ PHSP;
  0.0150 MyD_s*- D*+ K0 PHSP;
  0.0050 MyD_s*- D0 K+ PHSP;
  0.0050 MyD_s*- D+ K0 PHSP;
  0.0050 MyD_s- D*0 K+ PHSP;
  0.0050 MyD_s- D*+ K0 PHSP;
  0.0020 MyD_s- D0 K+ PHSP;
  0.0020 MyD_s- D+ K0 PHSP;
  0.0030 MyD_s*- D*0 K*+ PHSP;
  0.0030 MyD_s*- D*+ K*0 PHSP;
  0.0050 MyD_s*- D0 K*+ PHSP;
  0.0050 MyD_s*- D+ K*0 PHSP;
  0.0025 MyD_s- D*0 K*+ PHSP;
  0.0025 MyD_s- D*+ K*0 PHSP;
  0.0025 MyD_s- D0 K*+ PHSP;
  0.0025 MyD_s- D+ K*0 PHSP;
  0.0017 D_s+ MyD- PHSP;
  0.0017 MyD*- D_s+ SVS;
  0.0017 D_s*+ MyD- SVS;
  0.0017 D_s*+ MyD*- SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0007 D+ MyD- anti-K0 PHSP;
  0.0007 D*+ MyD- anti-K0 PHSP;
  0.0007 D*+ Myanti-D0 K- PHSP;
  0.0003 D+ MyD- pi0 anti-K0 PHSP;
  0.0007 D+ Myanti-D0 pi- anti-K0 PHSP;
  0.0003 D+ MyD- pi+ K- PHSP;
  0.0007 D+ Myanti-D0 pi0 K- PHSP;
  0.0003 D*+ MyD- pi0 anti-K0 PHSP;
  0.0007 D*+ Myanti-D0 pi- anti-K0 PHSP;
  0.0003 D*+ MyD- pi+ K- PHSP;
  0.0007 D*+ Myanti-D0 pi0 K- PHSP;
  0.00015 MyD_s*- K+ SVS;
  0.000150000 MyD_s- K+ PHSP; #[Reconstructed PDG2011]
  0.00030 MyD_s*- K*+ SVV_HELAMP 0.0283 0.0 0.932 0.0 0.228 0.0;
  0.00030 K*+ MyD_s- SVS;
  0.0008 MyD_s1- pi+ SVS;
  0.0021 MyD_s1- rho+ SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0013 MyD_s2*- pi+ STS;
  0.0027 MyD_s*- pi+ SVS;
  0.003200000 MyD_s- pi+ PHSP; #[Reconstructed PDG2011]
  0.0073 rho+ MyD_s- SVS;
  0.0070 MyD_s*- rho+ SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0085 a_1+ MyD_s- SVS;
  0.0009 MyD_s- rho0 pi+ PHSP;
  0.0009 MyD_s- rho+ pi0 PHSP;
  0.008400000 MyD_s- pi- pi+ pi+ PHSP; #[Reconstructed PDG2011]
  0.0009 MyD_s- pi0 pi+ pi0 PHSP;
  0.0122 MyD_s*- a_1+ SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0010 MyD_s*- rho0 pi+ PHSP;
  0.0010 MyD_s*- rho+ pi0 PHSP;
  0.0077 MyD_s*- pi- pi+ pi+ PHSP;
  0.0010 MyD_s*- pi0 pi+ pi0 PHSP;
  0.0002 Myanti-D*0 anti-K0 SVS;
  0.0002 Myanti-D0 anti-K0 PHSP;
  0.0002 anti-K*0 Myanti-D0 SVS;
  0.0002 Myanti-D*0 anti-K*0 SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
Enddecay

Decay anti-Lambda_b0
  0.050000000 Myanti-Lambda_c- e+ nu_e PHSP; #[Reconstructed PDG2011]
  0.006300000 Myanti-Lambda_c(2593)- e+ nu_e PHSP; #[Reconstructed PDG2011]
  0.011000000 Myanti-Lambda_c(2625)- e+ nu_e PHSP; #[Reconstructed PDG2011]
  0.00000 Myanti-Sigma_c0 pi- e+ nu_e PHSP;
  0.00000 Myanti-Sigma_c- pi0 e+ nu_e PHSP;
  0.00000 Myanti-Sigma_c-- pi+ e+ nu_e PHSP;
  0.00000 Myanti-Sigma_c*0 pi- e+ nu_e PHSP;
  0.00000 Myanti-Sigma_c*- pi0 e+ nu_e PHSP;
  0.00000 Myanti-Sigma_c*-- pi+ e+ nu_e PHSP;
  0.01720 Myanti-Lambda_c- tau+ nu_tau PHSP;
  0.00430 Myanti-Lambda_c(2593)- tau+ nu_tau PHSP;
  0.00320 Myanti-Lambda_c(2625)- tau+ nu_tau PHSP;
  0.00000 Myanti-Sigma_c0 pi- tau+ nu_tau PHSP;
  0.00000 Myanti-Sigma_c- pi0 tau+ nu_tau PHSP;
  0.00000 Myanti-Sigma_c-- pi+ tau+ nu_tau PHSP;
  0.00000 Myanti-Sigma_c*0 pi- tau+ nu_tau PHSP;
  0.00000 Myanti-Sigma_c*- pi0 tau+ nu_tau PHSP;
  0.00000 Myanti-Sigma_c*-- pi+ tau+ nu_tau PHSP;
  0.008800000 Myanti-Lambda_c- pi+ PHSP; #[Reconstructed PDG2011]
  0.02200 Myanti-Lambda_c- pi+ pi+ pi- PHSP;
  0.00055 Myanti-Lambda_c- K+ PHSP;
  0.02200 Myanti-Lambda_c- D_s+ PHSP;
  0.04400 Myanti-Lambda_c- D_s*+ PHSP;
  0.01000 Myanti-Lambda_c- rho+ PHSP;
  0.02000 Myanti-Lambda_c- a_1+ PHSP;
  0.00080 anti-n0 Myanti-D0 PHSP;
  0.00060 Myanti-Sigma_c- pi+ PHSP;
  0.00060 Myanti-Sigma_c0 pi0 PHSP;
  0.00040 Myanti-Sigma_c0 eta PHSP;
  0.00050 Myanti-Sigma_c0 eta' PHSP;
  #0.00030 Myanti-Xi_c0 anti-K0 PHSP;
  #0.00050 Myanti-Xi'_c0 anti-K0 PHSP;
  0.056000000 Myanti-Lambda_c- pi- pi+ e+ nu_e PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay MyD*-
  0.6770 Myanti-D0 pi- VSS;
  0.3070 MyD- pi0 VSS;
  0.0160 MyD- gamma VSP_PWAVE;
Enddecay

Decay Myanti-D*0
  0.6190 Myanti-D0 pi0 VSS;
  0.3810 Myanti-D0 gamma VSP_PWAVE;
Enddecay

Decay MyD_s*-
  0.942000000 MyD_s- gamma VSP_PWAVE; #[Reconstructed PDG2011]
  0.058000000 MyD_s- pi0 VSS; #[Reconstructed PDG2011]
Enddecay

Decay MyD_0*-
  0.3333 MyD- pi0 PHSP;
  0.6667 Myanti-D0 pi- PHSP;
Enddecay

Decay Myanti-D_0*0
  0.3333 Myanti-D0 pi0 PHSP;
  0.6667 MyD- pi+ PHSP;
Enddecay

Decay MyD_1-
  0.3333 MyD*- pi0 VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.6667 Myanti-D*0 pi- VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay

Decay Myanti-D_10
  0.3333 Myanti-D*0 pi0 VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.6667 MyD*- pi+ VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay

Decay MyD'_1-
  0.3333 MyD*- pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.6667 Myanti-D*0 pi- VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay

Decay Myanti-D'_10
  0.3333 Myanti-D*0 pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.6667 MyD*- pi+ VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay

Decay MyD_2*-
  0.1030 MyD*- pi0 TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2090 Myanti-D*0 pi- TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2290 MyD- pi0 TSS;
  0.4590 Myanti-D0 pi- TSS;
Enddecay

Decay Myanti-D_2*0
  0.1030 Myanti-D*0 pi0 TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2090 MyD*- pi+ TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2290 Myanti-D0 pi0 TSS;
  0.4590 MyD- pi+ TSS;
Enddecay

Decay MyD_s0*-
  1.000 MyD_s- pi0 PHSP;
Enddecay

Decay D'_s1-
  0.5000 MyD*- anti-K0 VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.5000 Myanti-D*0 K- VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.0000 gamma MyD_s*- PHSP;
  0.0000 gamma MyD_s- PHSP;
Enddecay

Decay MyD_s1-
  0.80000 MyD_s*- pi0 PARTWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.20000 MyD_s- gamma VSP_PWAVE;
Enddecay

Decay MyD_s2*-
  0.0500 MyD*- anti-K0 TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.0500 Myanti-D*0 K- TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.4300 MyD- anti-K0 TSS;
  0.4700 Myanti-D0 K- TSS;
Enddecay

Decay Myanti-D(2S)0
  0.3333 Myanti-D*0 pi0 SVS;
  0.6667 MyD*- pi+ SVS;
Enddecay

Decay MyD(2S)-
  0.3333 MyD*- pi0 SVS;
  0.6667 Myanti-D*0 pi- SVS;
Enddecay

Decay Myanti-D*(2S)0
  0.1667 Myanti-D*0 pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.3333 MyD*- pi+ VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1667 Myanti-D0 pi0 VSS;
  0.3333 MyD- pi+ VSS;
Enddecay

Decay D*(2S)-
  0.1667 MyD*- pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.3333 Myanti-D*0 pi- VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1667 MyD- pi0 VSS;
  0.3333 Myanti-D0 pi- VSS;
Enddecay

Decay Myanti-Sigma_c0
  1.0000 Myanti-Lambda_c- pi+ PHSP;
Enddecay

Decay Myanti-Sigma_c-
  1.0000 Myanti-Lambda_c- pi0 PHSP;
Enddecay

Decay Myanti-Sigma_c--
  1.0000 Myanti-Lambda_c- pi- PHSP;
Enddecay

Decay Myanti-Xi'_c-
  0.8200 gamma Myanti-Xi_c- PHSP;
Enddecay

Decay Myanti-Xi'_c0
  1.0000 gamma Myanti-Xi_c0 PHSP;
Enddecay

Decay Myanti-Sigma_c*0
  1.0000 Myanti-Lambda_c- pi+ PHSP;
Enddecay

Decay Myanti-Sigma_c*-
  1.0000 Myanti-Lambda_c- pi0 PHSP;
Enddecay

Decay Myanti-Sigma_c*--
  1.0000 Myanti-Lambda_c- pi- PHSP;
Enddecay

Decay Myanti-Lambda_c(2593)-
  0.096585366 Myanti-Sigma_c-- pi+ PHSP; #[Reconstructed PDG2011]
  0.096585366 Myanti-Sigma_c0 pi- PHSP; #[Reconstructed PDG2011]
  0.190000000 Myanti-Lambda_c- pi- pi+ PHSP; #[Reconstructed PDG2011]
  0.096585366 Myanti-Sigma_c- pi0 PHSP; #[Reconstructed PDG2011]
  0.036219512 Myanti-Lambda_c- pi0 pi0 PHSP; #[Reconstructed PDG2011]
  0.004024390 Myanti-Lambda_c- gamma PHSP; #[Reconstructed PDG2011]
  0.240000000 Myanti-Sigma_c*-- pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.240000000 Myanti-Sigma_c*0 pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay Myanti-Lambda_c(2625)-
  0.670000000 Myanti-Lambda_c- pi- pi+ PHSP; #[Reconstructed PDG2011]
  0.320294118 Myanti-Lambda_c- pi0 PHSP; #[Reconstructed PDG2011]
  0.009705882 Myanti-Lambda_c- gamma PHSP; #[Reconstructed PDG2011]
Enddecay

Decay MyD-
  0.055000000 K*0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.094000000 K0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002773020 K_10 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002927076 K_2*0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.003312218 pi0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002002736 eta mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000385142 eta' mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002500000 rho0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002156793 omega mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.039000000 K+ pi- mu- anti-nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.001078397 K0 pi0 mu- anti-nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.000382000 mu- anti-nu_mu PHOTOS SLN; #[Reconstructed PDG2011]
Enddecay

Decay Myanti-D0
  0.019800000 K*+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.033100000 K+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000818960 K_1+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.001380270 K_2*+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002370000 pi+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002024397 rho+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.001012198 K0 pi+ mu- anti-nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.000552108 K+ pi0 mu- anti-nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.000000000 mu- mu+ PHSP; #[Reconstructed PDG2011]
  #
  0.021700000 K*+ e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.035500000 K+ e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000760000 K_1+ e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.001380270 K_2*+ e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.038900000 K+ pi- PHSP; #[Reconstructed PDG2011]
Enddecay

Decay MyD_s-
  0.018309605 phi mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.022845082 eta mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.008186726 eta' mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002058115 K0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000762265 K*0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.005800000 mu- anti-nu_mu PHOTOS SLN; #[Reconstructed PDG2011]
  #
  0.024900000 phi e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.026700000 eta e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.009900000 eta' e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002058115 K0 e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000762265 K*0 e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002300000 omega pi- SVS; #[Reconstructed PDG2011]
  0.000200000 rho0 pi- SVS; #[Reconstructed PDG2011]
  0.000304906 rho- pi0 SVS; #[Reconstructed PDG2011]
Enddecay

Decay Myanti-Lambda_c-
  0.020000000 mu- anti-nu_mu anti-Lambda0 PYTHIA 42; #[Reconstructed PDG2011]
  0.00500 mu- anti-nu_mu anti-Sigma0 PYTHIA 42;
  0.00500 mu- anti-nu_mu anti-Sigma*0 PYTHIA 42;
  0.00300 mu- anti-nu_mu anti-n0 PYTHIA 42;
  0.00200 mu- anti-nu_mu anti-Delta0 PYTHIA 42;
  0.00600 mu- anti-nu_mu anti-p- pi+ PYTHIA 42;
  0.00600 mu- anti-nu_mu anti-n0 pi0 PYTHIA 42;
  #
  0.021000000 e- anti-nu_e anti-Lambda0 PYTHIA 42; #[Reconstructed PDG2011]
  0.00500 e- anti-nu_e anti-Sigma0 PYTHIA 42;
  0.00500 e- anti-nu_e anti-Sigma*0 PYTHIA 42;
  0.00300 e- anti-nu_e anti-n0 PYTHIA 42;
  0.00200 e- anti-nu_e anti-Delta0 PYTHIA 42;
  0.00600 e- anti-nu_e anti-p- pi+ PYTHIA 42;
  0.00600 e- anti-nu_e anti-n0 pi0 PYTHIA 42;
  0.008600000 anti-Delta-- K+ PYTHIA 0; #[Reconstructed PDG2011]
  0.02500 anti-Delta-- K*+ PYTHIA 0;
  0.023000000 anti-p- K0 PYTHIA 0; #[Reconstructed PDG2011]
  0.016000000 anti-p- K*0 PYTHIA 0; #[Reconstructed PDG2011]
  0.00500 anti-Delta- K0 PYTHIA 0;
  0.010700000 anti-Lambda0 pi- PYTHIA 0; #[Reconstructed PDG2011]
Enddecay

Decay anti-B0
  0.050100000 MyD*+ e- anti-nu_e PHOTOS HQET 0.77 1.33 0.92; #[Reconstructed PDG2011]
  0.021700000 MyD+ e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.0054 MyD_1+ e- anti-nu_e PHOTOS ISGW2;
  0.0020 MyD_0*+ e- anti-nu_e PHOTOS ISGW2;
  0.0050 MyD'_1+ e- anti-nu_e PHOTOS ISGW2;
  0.0022 MyD_2*+ e- anti-nu_e PHOTOS ISGW2;
  0.0003 MyD*+ pi0 e- anti-nu_e PHOTOS GOITY_ROBERTS;
  0.004900000 MyD*0 pi+ e- anti-nu_e PHOTOS GOITY_ROBERTS; #[Reconstructed PDG2011]
  0.0010 MyD+ pi0 e- anti-nu_e PHOTOS GOITY_ROBERTS;
  0.0000 MyD0 pi+ e- anti-nu_e PHOTOS GOITY_ROBERTS;
  0.015000000 MyD*+ tau- anti-nu_tau ISGW2; #[Reconstructed PDG2011]
  0.011000000 MyD+ tau- anti-nu_tau ISGW2; #[Reconstructed PDG2011]
  0.0013 MyD_1+ tau- anti-nu_tau ISGW2;
  0.0013 MyD_0*+ tau- anti-nu_tau ISGW2;
  0.0020 MyD'_1+ tau- anti-nu_tau ISGW2;
  0.0020 MyD_2*+ tau- anti-nu_tau ISGW2;
  0.000000 MyD(2S)+ e- anti-nu_e PHOTOS ISGW2;
  0.000000 MyD*(2S)+ e- anti-nu_e PHOTOS ISGW2;
  0.000000 MyD(2S)+ tau- anti-nu_tau ISGW2;
  0.000000 MyD*(2S)+ tau- anti-nu_tau ISGW2;
  0.000030000 MyD_s+ K- PHSP; #[Reconstructed PDG2011]
  0.000021900 MyD_s*+ K- SVS; #[Reconstructed PDG2011]
  0.000035000 K*- MyD_s+ SVS; #[Reconstructed PDG2011]
  0.000032000 MyD_s*+ K*- SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0; #[Reconstructed PDG2011]
  0.000211000 D- MyD+ PHSP; #[Reconstructed PDG2011]
  0.000305 MyD*+ D- SVS;
  0.000610000 D*- MyD+ SVS; #[Reconstructed PDG2011]
  0.000820000 MyD*+ D*- SVV_HELAMP 0.47 0.0 0.96 0.0 0.56 0.0; #[Reconstructed PDG2011]
  0.007200000 MyD+ D_s- PHSP; #[Reconstructed PDG2011]
  0.008000000 MyD*+ D_s- SVS; #[Reconstructed PDG2011]
  0.007400000 D_s*- MyD+ SVS; #[Reconstructed PDG2011]
  0.017700000 MyD*+ D_s*- SVV_HELAMP 0.4904 0.0 0.7204 0.0 0.4904 0.0; #[Reconstructed PDG2011]
  0.0006 MyD'_1+ D_s- SVS;
  0.0012 MyD'_1+ D_s*- SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0012 MyD_1+ D_s- SVS;
  0.0024 MyD_1+ D_s*- SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0042 MyD_2*+ D_s- STS;
  0.0040 MyD_2*+ D_s*- PHSP;
  0.0018 D_s- MyD+ pi0 PHSP;
  0.0037 D_s- MyD0 pi+ PHSP;
  0.0018 D_s*- MyD+ pi0 PHSP;
  0.0037 D_s*- MyD0 pi+ PHSP;
  0.0030 D_s- MyD+ pi+ pi- PHSP;
  0.0022 D_s- MyD+ pi0 pi0 PHSP;
  0.0022 D_s- MyD0 pi+ pi0 PHSP;
  0.0030 D_s*- MyD+ pi+ pi- PHSP;
  0.0022 D_s*- MyD+ pi0 pi0 PHSP;
  0.0022 D_s*- MyD0 pi+ pi0 PHSP;
  0.001700000 MyD+ anti-D0 K- PHSP; #[Reconstructed PDG2011]
  0.004600000 MyD+ anti-D*0 K- PHSP; #[Reconstructed PDG2011]
  0.003100000 MyD*+ anti-D0 K- PHSP; #[Reconstructed PDG2011]
  0.011800000 MyD*+ anti-D*0 K- PHSP; #[Reconstructed PDG2011]
  0.0015 MyD+ D- anti-K0 PHSP;
  0.0018 MyD*+ D- anti-K0 PHSP;
  0.0047 MyD+ D*- anti-K0 PHSP;
  0.007800000 MyD*+ D*- anti-K0 PHSP; #[Reconstructed PDG2011]
  0.0005 MyD0 anti-D0 anti-K0 PHSP;
  0.000120000 MyD*0 anti-D0 anti-K0 PHSP; #[Reconstructed PDG2011]
  0.0015 MyD0 anti-D*0 anti-K0 PHSP;
  0.0015 MyD*0 anti-D*0 anti-K0 PHSP;
  0.0025 MyD+ anti-D0 K*- PHSP;
  0.0025 MyD*+ anti-D0 K*- PHSP;
  0.0025 MyD+ anti-D*0 K*- PHSP;
  0.0050 MyD*+ anti-D*0 K*- PHSP;
  0.0025 MyD+ D- anti-K*0 PHSP;
  0.0025 MyD*+ D- anti-K*0 PHSP;
  0.0025 MyD+ D*- anti-K*0 PHSP;
  0.0050 MyD*+ D*- anti-K*0 PHSP;
  0.0005 anti-D0 MyD0 anti-K*0 PHSP;
  0.0005 anti-D0 MyD*0 anti-K*0 PHSP;
  0.0005 anti-D*0 MyD0 anti-K*0 PHSP;
  0.0010 anti-D*0 MyD*0 anti-K*0 PHSP;
  0.002760000 MyD*+ pi- SVS; #[Reconstructed PDG2011]
  0.002680000 MyD+ pi- PHSP; #[Reconstructed PDG2011]
  0.007110000 rho- MyD+ SVS; #[Reconstructed PDG2011]
  0.006800000 rho- MyD*+ SVV_HELAMP 0.152 1.47 0.936 0 0.317 0.19; #[Reconstructed PDG2011]
  0.0005 MyD+ pi- pi0 PHSP;
  0.008200000 MyD*+ pi- pi0 PHSP; #[Reconstructed PDG2011]
  0.000840000 MyD0 pi+ pi- PHSP; #[Reconstructed PDG2011]
  0.000620000 MyD*0 pi+ pi- PHSP; #[Reconstructed PDG2011]
  0.0005 MyD*0 pi0 pi0 PHSP;
  0.006000000 a_1- MyD+ SVS; #[Reconstructed PDG2011]
  0.000000000 MyD+ rho0 pi- PHSP; #[Reconstructed PDG2011]
  0.0011 MyD+ rho- pi0 PHSP;
  0.002000000 MyD+ pi+ pi- pi- PHSP; #[Reconstructed PDG2011]
  0.0010 MyD+ pi0 pi- pi0 PHSP;
  0.0010 MyD0 pi+ pi- pi0 PHSP;
  0.0001 MyD0 pi0 pi0 pi0 PHSP;
  0.013000000 MyD*+ a_1- SVV_HELAMP 0.200 0.0 0.866 0.0 0.458 0.0; #[Reconstructed PDG2011]
  0.005700000 MyD*+ rho0 pi- PHSP; #[Reconstructed PDG2011]
  0.0010 MyD*+ rho- pi0 PHSP;
  0.0000 MyD*+ pi+ pi- pi- PHSP;
  0.0010 MyD*+ pi0 pi- pi0 PHSP;
  0.0010 MyD*0 pi+ pi- pi0 PHSP;
  0.0001 MyD*0 pi0 pi0 pi0 PHSP;
  0.0001 MyD_1+ pi- SVS;
  0.0001 MyD'_1+ pi- SVS;
  0.00006 MyD_0*+ pi- PHSP;
  0.000215 MyD_2*+ pi- STS;
  0.0004 MyD_1+ rho- PHSP;
  0.0013 MyD'_1+ rho- PHSP;
  0.0022 MyD_2*+ rho- PHSP;
  0.000214000 MyD*+ K- SVS; #[Reconstructed PDG2011]
  0.000200000 MyD+ K- PHSP; #[Reconstructed PDG2011]
  0.000330000 MyD*+ K*- SVV_HELAMP 0.228 0.0 0.932 0.0 0.283 0.0; #[Reconstructed PDG2011]
  0.000450000 K*- MyD+ SVS; #[Reconstructed PDG2011]
  0.000036 MyD*0 K0 SVS;
  0.000052 MyD0 K0 PHSP;
  0.000042000 anti-K*0 MyD0 SVS; #[Reconstructed PDG2011]
  0.00004 MyD*0 anti-K*0 SVV_HELAMP 1. 0. 1. 0. 1. 0.;
  0.000261000 MyD0 pi0 PHSP; #[Reconstructed PDG2011]
  0.000170000 MyD*0 pi0 SVS; #[Reconstructed PDG2011]
  0.000320000 rho0 MyD0 SVS; #[Reconstructed PDG2011]
  0.00029 MyD*0 rho0 SVV_HELAMP 0.228 0.0 0.932 0.0 0.283 0.0;
  0.000202000 MyD0 eta PHSP; #[Reconstructed PDG2011]
  0.000200000 MyD*0 eta SVS; #[Reconstructed PDG2011]
  0.000125 MyD0 eta' PHSP;
  0.000123000 MyD*0 eta' SVS; #[Reconstructed PDG2011]
  0.000259000 omega MyD0 SVS; #[Reconstructed PDG2011]
  0.000330000 MyD*0 omega SVV_HELAMP 0.228 0.0 0.932 0.0 0.283 0.0; #[Reconstructed PDG2011]
  0.0016 D_s0*- MyD+ PHSP;
  0.0015 MyD*+ D_s0*- SVS;
  0.003500000 D_s1- MyD+ SVS; #[Reconstructed PDG2011]
  0.009300000 MyD*+ D_s1- SVV_HELAMP 0.4904 0. 0.7204 0. 0.4904 0.; #[Reconstructed PDG2011]
  0.00088 MyD+ K- anti-K*0 PHSP;
  0.00129 MyD*+ K- anti-K*0 PHSP;
  0.002800000 MyD+ omega pi- PHSP; #[Reconstructed PDG2011]
  0.002890000 MyD*+ omega pi- PHSP; #[Reconstructed PDG2011]
  0.00049 MyD+ K0 pi- PHSP;
  0.0003 MyD*+ K0 pi- PHSP;
  0.00043 D'_s1- MyD+ PHSP;
  0.00083 D'_s1- MyD*+ PHSP;
  0.000490000 MyD+ anti-K0 pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000880000 MyD+ K- K*0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000000000 MyD*+ anti-K0 pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.001290000 MyD*+ K- K*0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.017600000 MyD*+ pi- pi- pi+ pi0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.004700000 MyD*+ pi- pi- pi- pi+ pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000110000 MyD_s+ pi- anti-K0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000052000 MyD0 anti-K0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000046000 MyD0 K- pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000036000 MyD*0 anti-K0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000170000 anti-K0 anti-D0 MyD0 pi0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000114000 MyD0 anti-p- p+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000028000 MyD_s+ Lambda0 anti-p- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000103000 MyD*0 anti-p- p+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.001500000 MyD*+ anti-p- n0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000338000 MyD+ anti-p- p+ pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000500000 MyD*+ anti-p- p+ pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000020000 MyLambda_c+ anti-p- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000630000 MyLambda_c+ anti-p- pi- pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000120000 MySigma_c*++ anti-p- pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000150000 MySigma_c0 anti-p- pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000220000 MySigma_c++ anti-p- pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay B-
  0.056800000 MyD*0 e- anti-nu_e PHOTOS HQET 0.77 1.33 0.92; #[Reconstructed PDG2011]
  0.022300000 MyD0 e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.0040 MyD_10 e- anti-nu_e PHOTOS ISGW2;
  0.0024 MyD_0*0 e- anti-nu_e PHOTOS ISGW2;
  0.0007 MyD'_10 e- anti-nu_e PHOTOS ISGW2;
  0.0018 MyD_2*0 e- anti-nu_e PHOTOS ISGW2;
  0.006100000 MyD*+ pi- e- anti-nu_e PHOTOS GOITY_ROBERTS; #[Reconstructed PDG2011]
  0.0003 MyD*0 pi0 e- anti-nu_e PHOTOS GOITY_ROBERTS;
  0.0000 MyD+ pi- e- anti-nu_e PHOTOS GOITY_ROBERTS;
  0.0010 MyD0 pi0 e- anti-nu_e PHOTOS GOITY_ROBERTS;
  0.020000000 MyD*0 tau- anti-nu_tau ISGW2; #[Reconstructed PDG2011]
  0.007000000 MyD0 tau- anti-nu_tau ISGW2; #[Reconstructed PDG2011]
  0.0013 MyD_10 tau- anti-nu_tau ISGW2;
  0.0013 MyD_0*0 tau- anti-nu_tau ISGW2;
  0.0020 MyD'_10 tau- anti-nu_tau ISGW2;
  0.0020 MyD_2*0 tau- anti-nu_tau ISGW2;
  0.000000 MyD(2S)0 e- anti-nu_e PHOTOS ISGW2;
  0.000000 MyD*(2S)0 e- anti-nu_e PHOTOS ISGW2;
  0.000000 MyD(2S)0 tau- anti-nu_tau ISGW2;
  0.000000 MyD*(2S)0 tau- anti-nu_tau ISGW2;
  0.010000000 MyD0 D_s- PHSP; #[Reconstructed PDG2011]
  0.008200000 MyD*0 D_s- SVS; #[Reconstructed PDG2011]
  0.007600000 D_s*- MyD0 SVS; #[Reconstructed PDG2011]
  0.017100000 D_s*- MyD*0 SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0; #[Reconstructed PDG2011]
  0.0006 MyD'_10 D_s- SVS;
  0.0012 MyD'_10 D_s*- SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0012 MyD_10 D_s- SVS;
  0.0024 MyD_10 D_s*- SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0042 MyD_2*0 D_s- STS;
  0.0040 MyD_2*0 D_s*- PHSP;
  0.0036 D_s- MyD+ pi- PHSP;
  0.0018 D_s- MyD0 pi0 PHSP;
  0.0037 D_s*- MyD+ pi- PHSP;
  0.0018 D_s*- MyD0 pi0 PHSP;
  0.0033 D_s- MyD+ pi- pi0 PHSP;
  0.0033 D_s- MyD0 pi- pi+ PHSP;
  0.0008 D_s- MyD0 pi0 pi0 PHSP;
  0.0033 D_s*- MyD+ pi- pi0 PHSP;
  0.0033 D_s*- MyD0 pi- pi+ PHSP;
  0.0008 D_s*- MyD0 pi0 pi0 PHSP;
  0.0017 MyD0 D- anti-K0 PHSP;
  0.0052 MyD0 D*- anti-K0 PHSP;
  0.0031 MyD*0 D- anti-K0 PHSP;
  0.007800000 MyD*0 D*- anti-K0 PHSP; #[Reconstructed PDG2011]
  0.002100000 MyD0 anti-D0 K- PHSP; #[Reconstructed PDG2011]
  0.0018 MyD*0 anti-D0 K- PHSP;
  0.004700000 MyD0 anti-D*0 K- PHSP; #[Reconstructed PDG2011]
  0.005300000 MyD*0 anti-D*0 K- PHSP; #[Reconstructed PDG2011]
  0.0005 D- MyD+ K- PHSP;
  0.0005 D*- MyD+ K- PHSP;
  0.001500000 D- MyD*+ K- PHSP; #[Reconstructed PDG2011]
  0.0015 D*- MyD*+ K- PHSP;
  0.0025 MyD0 D- anti-K*0 PHSP;
  0.0025 MyD*0 D- anti-K*0 PHSP;
  0.0025 MyD0 D*- anti-K*0 PHSP;
  0.0050 MyD*0 D*- anti-K*0 PHSP;
  0.0025 MyD0 anti-D0 K*- PHSP;
  0.0025 MyD*0 anti-D0 K*- PHSP;
  0.0025 MyD0 anti-D*0 K*- PHSP;
  0.0050 MyD*0 anti-D*0 K*- PHSP;
  0.0005 D- MyD+ K*- PHSP;
  0.0005 D*- MyD+ K*- PHSP;
  0.0005 D- MyD*+ K*- PHSP;
  0.0010 D*- MyD*+ K*- PHSP;
  0.000380000 D- MyD0 PHSP; #[Reconstructed PDG2011]
  0.000390000 D*- MyD0 SVS; #[Reconstructed PDG2011]
  0.000630000 MyD*0 D- SVS; #[Reconstructed PDG2011]
  0.000810000 MyD*0 D*- SVV_HELAMP 0.47 0.0 0.96 0.0 0.56 0.0; #[Reconstructed PDG2011]
  0.005190000 MyD*0 pi- SVS; #[Reconstructed PDG2011]
  0.004840000 MyD0 pi- PHSP; #[Reconstructed PDG2011]
  0.013400000 rho- MyD0 SVS; #[Reconstructed PDG2011]
  0.009800000 MyD*0 rho- SVV_HELAMP 0.228 0.95 0.932 0.0 0.283 1.13; #[Reconstructed PDG2011]
  0.0005 MyD0 pi0 pi- PHSP;
  0.0005 MyD*0 pi0 pi- PHSP;
  0.001070000 MyD+ pi- pi- PHSP; #[Reconstructed PDG2011]
  0.001350000 MyD*+ pi- pi- PHSP; #[Reconstructed PDG2011]
  0.004000000 a_1- MyD0 SVS; #[Reconstructed PDG2011]
  0.000200000 MyD0 rho0 pi- PHSP; #[Reconstructed PDG2011]
  0.006800000 MyD0 pi+ pi- pi- PHSP; #[Reconstructed PDG2011]
  0.019000000 MyD*0 a_1- SVV_HELAMP 0.200 0.0 0.866 0.0 0.458 0.0; #[Reconstructed PDG2011]
  0.00042 MyD*0 rho0 pi- PHSP;
  0.010300000 MyD*0 pi+ pi- pi- PHSP; #[Reconstructed PDG2011]
  0.0020 MyD+ rho- pi- PHSP;
  0.0020 MyD+ pi0 pi- pi- PHSP;
  0.0020 MyD*+ rho- pi- PHSP;
  0.015000000 MyD*+ pi0 pi- pi- PHSP; #[Reconstructed PDG2011]
  0.0005 MyD*0 rho- pi0 PHSP;
  0.0005 MyD*0 pi- pi0 pi0 PHSP;
  0.000876 MyD_10 pi- SVS;
  0.0005 MyD'_10 pi- SVS;
  0.00052 MyD_2*0 pi- STS;
  0.0007 MyD_10 rho- PHSP;
  0.0022 MyD'_10 rho- PHSP;
  0.0038 MyD_2*0 rho- PHSP;
  0.00061 MyD_0*0 pi- PHSP;
  0.000368000 MyD0 K- PHSP; #[Reconstructed PDG2011]
  0.000421000 MyD*0 K- SVS; #[Reconstructed PDG2011]
  0.000530000 K*- MyD0 SVS; #[Reconstructed PDG2011]
  0.000810000 MyD*0 K*- SVV_HELAMP 0.228 0.0 0.932 0.0 0.283 0.0; #[Reconstructed PDG2011]
  0.00075 MyD0 D_s0*- PHSP;
  0.0009 MyD*0 D_s0*- SVS;
  0.003100000 D_s1- MyD0 SVS; #[Reconstructed PDG2011]
  0.012000000 MyD*0 D_s1- SVV_HELAMP 0.4904 0.0 0.7204 0.0 0.4904 0.0; #[Reconstructed PDG2011]
  0.00055 MyD0 K- anti-K0 PHSP;
  0.00075 MyD0 K- anti-K*0 PHSP;
  0.001500000 MyD*0 K- anti-K*0 PHSP; #[Reconstructed PDG2011]
  0.002750000 MyD0 omega pi- PHSP; #[Reconstructed PDG2011]
  0.004500000 MyD*0 omega pi- PHSP; #[Reconstructed PDG2011]
  0.00045 MyD0 D'_s1- PHSP;
  0.00094 MyD*0 D'_s1- PHSP;
  0.000550000 MyD0 K- K0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000750000 MyD0 K- K*0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.018000000 MyD*0 pi+ pi- pi- pi0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.005700000 MyD*0 pi- pi- pi- pi+ pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.002600000 MyD*+ pi- pi- pi- pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000180000 MyD_s+ pi- K- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000145000 MyD_s*+ pi- K- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000011000 MyD_s+ K- K- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000280000 MyLambda_c+ anti-p- pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.001800000 MyLambda_c+ anti-p- pi- pi0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.002300000 MyLambda_c+ anti-p- pi- pi- pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000035000 MySigma_c0 anti-p- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000440000 MySigma_c0 anti-p- pi0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000440000 MySigma_c0 anti-p- pi+ pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000280000 MySigma_c++ anti-p- pi- pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay anti-B_s0
  0.0210 MyD_s+ e- anti-nu_e PHOTOS ISGW2;
  0.0490 MyD_s*+ e- anti-nu_e PHOTOS ISGW2;
  0.0040 MyD_s1+ e- anti-nu_e PHOTOS ISGW2;
  0.0040 MyD_s0*+ e- anti-nu_e PHOTOS ISGW2;
  0.0070 MyD_s2*+ e- anti-nu_e PHOTOS ISGW2;
  0.0080 MyD_s+ tau- anti-nu_tau ISGW2;
  0.0160 MyD_s*+ tau- anti-nu_tau ISGW2;
  0.0018 MyD_s1+ tau- anti-nu_tau ISGW2;
  0.0018 MyD_s0*+ tau- anti-nu_tau ISGW2;
  0.0028 MyD_s2*+ tau- anti-nu_tau ISGW2;
  0.010400000 D_s- MyD_s+ PHSP; #[Reconstructed PDG2011]
  0.0099 MyD_s*+ D_s- SVS;
  0.0099 D_s*- MyD_s+ SVS;
  0.0197 D_s*- MyD_s*+ SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0096 D_s- MyD+ K0 PHSP;
  0.0096 D_s*- MyD+ K0 PHSP;
  0.0096 D_s*- MyD0 K+ PHSP;
  0.0024 D_s- MyD+ pi0 K0 PHSP;
  0.0048 D_s- MyD0 pi+ K0 PHSP;
  0.0048 D_s- MyD+ pi- K+ PHSP;
  0.0024 D_s- MyD0 pi0 K+ PHSP;
  0.0024 D_s*- MyD+ pi0 K0 PHSP;
  0.0048 D_s*- MyD0 pi+ K0 PHSP;
  0.0048 D_s*- MyD+ pi- K+ PHSP;
  0.0024 D_s*- MyD0 pi0 K+ PHSP;
  0.0150 MyD_s*+ anti-D*0 K- PHSP;
  0.0150 MyD_s*+ D*- anti-K0 PHSP;
  0.0050 MyD_s*+ anti-D0 K- PHSP;
  0.0050 MyD_s*+ D- anti-K0 PHSP;
  0.0050 MyD_s+ anti-D*0 K- PHSP;
  0.0050 MyD_s+ D*- anti-K0 PHSP;
  0.0020 MyD_s+ anti-D0 K- PHSP;
  0.0020 MyD_s+ D- anti-K0 PHSP;
  0.0030 MyD_s*+ anti-D*0 K*- PHSP;
  0.0030 MyD_s*+ D*- anti-K*0 PHSP;
  0.0050 MyD_s*+ anti-D0 K*- PHSP;
  0.0050 MyD_s*+ D- anti-K*0 PHSP;
  0.0025 MyD_s+ anti-D*0 K*- PHSP;
  0.0025 MyD_s+ D*- anti-K*0 PHSP;
  0.0025 MyD_s+ anti-D0 K*- PHSP;
  0.0025 MyD_s+ D- anti-K*0 PHSP;
  0.0017 D_s- MyD+ PHSP;
  0.0017 MyD*+ D_s- SVS;
  0.0017 D_s*- MyD+ SVS;
  0.0017 D_s*- MyD*+ SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0007 D- MyD+ K0 PHSP;
  0.0007 D*- MyD+ K0 PHSP;
  0.0007 D*- MyD0 K+ PHSP;
  0.0003 D- MyD+ pi0 K0 PHSP;
  0.0007 D- MyD0 pi+ K0 PHSP;
  0.0003 D- MyD+ pi- K+ PHSP;
  0.0007 D- MyD0 pi0 K+ PHSP;
  0.0003 D*- MyD+ pi0 K0 PHSP;
  0.0007 D*- MyD0 pi+ K0 PHSP;
  0.0003 D*- MyD+ pi- K+ PHSP;
  0.0007 D*- MyD0 pi0 K+ PHSP;
  0.00015 MyD_s*+ K- SVS;
  0.000150000 MyD_s+ K- PHSP; #[Reconstructed PDG2011]
  0.00030 MyD_s*+ K*- SVV_HELAMP 0.228 0.0 0.932 0.0 0.0283 0.0;
  0.00030 K*- MyD_s+ SVS;
  0.0008 MyD_s1+ pi- SVS;
  0.0021 MyD_s1+ rho- SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0013 MyD_s2*+ pi- STS;
  0.0027 MyD_s*+ pi- SVS;
  0.003200000 MyD_s+ pi- PHSP; #[Reconstructed PDG2011]
  0.0073 rho- MyD_s+ SVS;
  0.0070 MyD_s*+ rho- SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0085 a_1- MyD_s+ SVS;
  0.0009 MyD_s+ rho0 pi- PHSP;
  0.0009 MyD_s+ rho- pi0 PHSP;
  0.008400000 MyD_s+ pi+ pi- pi- PHSP; #[Reconstructed PDG2011]
  0.0009 MyD_s+ pi0 pi- pi0 PHSP;
  0.0122 MyD_s*+ a_1- SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0010 MyD_s*+ rho0 pi- PHSP;
  0.0010 MyD_s*+ rho- pi0 PHSP;
  0.0077 MyD_s*+ pi+ pi- pi- PHSP;
  0.0010 MyD_s*+ pi0 pi- pi0 PHSP;
  0.0002 MyD*0 K0 SVS;
  0.0002 MyD0 K0 PHSP;
  0.0002 K*0 MyD0 SVS;
  0.0002 MyD*0 K*0 SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
Enddecay

Decay Lambda_b0
  0.050000000 MyLambda_c+ e- anti-nu_e PHSP; #[Reconstructed PDG2011]
  0.006300000 MyLambda_c(2593)+ e- anti-nu_e PHSP; #[Reconstructed PDG2011]
  0.011000000 MyLambda_c(2625)+ e- anti-nu_e PHSP; #[Reconstructed PDG2011]
  0.00000 MySigma_c0 pi+ e- anti-nu_e PHSP;
  0.00000 MySigma_c+ pi0 e- anti-nu_e PHSP;
  0.00000 MySigma_c++ pi- e- anti-nu_e PHSP;
  0.00000 MySigma_c*0 pi+ e- anti-nu_e PHSP;
  0.00000 MySigma_c*+ pi0 e- anti-nu_e PHSP;
  0.00000 MySigma_c*++ pi- e- anti-nu_e PHSP;
  0.01720 MyLambda_c+ tau- anti-nu_tau PHSP;
  0.00430 MyLambda_c(2593)+ tau- anti-nu_tau PHSP;
  0.00320 MyLambda_c(2625)+ tau- anti-nu_tau PHSP;
  0.00000 MySigma_c0 pi+ tau- anti-nu_tau PHSP;
  0.00000 MySigma_c+ pi0 tau- anti-nu_tau PHSP;
  0.00000 MySigma_c++ pi- tau- anti-nu_tau PHSP;
  0.00000 MySigma_c*0 pi+ tau- anti-nu_tau PHSP;
  0.00000 MySigma_c*+ pi0 tau- anti-nu_tau PHSP;
  0.00000 MySigma_c*++ pi- tau- anti-nu_tau PHSP;
  0.008800000 MyLambda_c+ pi- PHSP; #[Reconstructed PDG2011]
  0.02200 MyLambda_c+ pi- pi+ pi- PHSP;
  0.00055 MyLambda_c+ K- PHSP;
  0.02200 MyLambda_c+ D_s- PHSP;
  0.04400 MyLambda_c+ D_s*- PHSP;
  0.01000 MyLambda_c+ rho- PHSP;
  0.02000 MyLambda_c+ a_1- PHSP;
  0.00080 n0 MyD0 PHSP;
  0.00060 MySigma_c+ pi- PHSP;
  0.00060 MySigma_c0 pi0 PHSP;
  0.00040 MySigma_c0 eta PHSP;
  0.00050 MySigma_c0 eta' PHSP;
  #0.00030 MyXi_c0 K0 PHSP;
  #0.00050 MyXi'_c0 K0 PHSP;
  0.056000000 MyLambda_c+ pi+ pi- e- anti-nu_e PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay MyD*+
  0.6770 MyD0 pi+ VSS;
  0.3070 MyD+ pi0 VSS;
  0.0160 MyD+ gamma VSP_PWAVE;
Enddecay

Decay MyD*0
  0.619000000 MyD0 pi0 VSS; #[Reconstructed PDG2011]
  0.381000000 MyD0 gamma VSP_PWAVE; #[Reconstructed PDG2011]
Enddecay

Decay MyD_s*+
  0.942000000 MyD_s+ gamma VSP_PWAVE; #[Reconstructed PDG2011]
  0.058000000 MyD_s+ pi0 VSS; #[Reconstructed PDG2011]
Enddecay

Decay MyD_0*+
  0.3333 MyD+ pi0 PHSP;
  0.6667 MyD0 pi+ PHSP;
Enddecay

Decay MyD_0*0
  0.3333 MyD0 pi0 PHSP;
  0.6667 MyD+ pi- PHSP;
Enddecay

Decay MyD_1+
  0.3333 MyD*+ pi0 VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.6667 MyD*0 pi+ VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay

Decay MyD_10
  0.3333 MyD*0 pi0 VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.6667 MyD*+ pi- VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay

Decay MyD'_1+
  0.3333 MyD*+ pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.6667 MyD*0 pi+ VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay

Decay MyD'_10
  0.6667 MyD*+ pi- VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.3333 MyD*0 pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay

Decay MyD_2*+
  0.1030 MyD*+ pi0 TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2090 MyD*0 pi+ TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2290 MyD+ pi0 TSS;
  0.4590 MyD0 pi+ TSS;
Enddecay

Decay MyD_2*0
  0.2090 MyD*+ pi- TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.1030 MyD*0 pi0 TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2290 MyD0 pi0 TSS;
  0.4590 MyD+ pi- TSS;
Enddecay

Decay MyD_s0*+
  1.000 MyD_s+ pi0 PHSP;
Enddecay

Decay D'_s1+
  0.5000 MyD*+ K0 VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.5000 MyD*0 K+ VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.0000 gamma MyD_s*+ PHSP;
  0.0000 gamma MyD_s+ PHSP;
Enddecay

Decay MyD_s1+
  0.80000 MyD_s*+ pi0 PARTWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.20000 MyD_s+ gamma VSP_PWAVE;
Enddecay

Decay MyD_s2*+
  0.0500 MyD*+ K0 TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.0500 MyD*0 K+ TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.4300 MyD+ K0 TSS;
  0.4700 MyD0 K+ TSS;
Enddecay

Decay MyD(2S)0
  0.6667 MyD*+ pi- SVS;
  0.3333 MyD*0 pi0 SVS;
Enddecay

Decay MyD(2S)+
  0.3333 MyD*+ pi0 SVS;
  0.6667 MyD*0 pi+ SVS;
Enddecay

Decay MyD*(2S)0
  0.3333 MyD*+ pi- VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1667 MyD*0 pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1667 MyD0 pi0 VSS;
  0.3333 MyD+ pi- VSS;
Enddecay

Decay MyD*(2S)+
  0.1667 MyD*+ pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.3333 MyD*0 pi+ VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1667 MyD+ pi0 VSS;
  0.3333 MyD0 pi+ VSS;
Enddecay

Decay MySigma_c0
  1.0000 MyLambda_c+ pi- PHSP;
Enddecay

Decay MySigma_c+
  1.0000 MyLambda_c+ pi0 PHSP;
Enddecay

Decay MySigma_c++
  1.0000 MyLambda_c+ pi+ PHSP;
Enddecay

Decay MyXi'_c+
  0.8200 gamma MyXi_c+ PHSP;
Enddecay

Decay MyXi'_c0
  1.0000 gamma MyXi_c0 PHSP;
Enddecay

Decay MySigma_c*0
  1.0000 MyLambda_c+ pi- PHSP;
Enddecay

Decay MySigma_c*+
  1.0000 MyLambda_c+ pi0 PHSP;
Enddecay

Decay MySigma_c*++
  1.0000 MyLambda_c+ pi+ PHSP;
Enddecay

Decay MyLambda_c(2593)+
  0.096585366 MySigma_c++ pi- PHSP; #[Reconstructed PDG2011]
  0.096585366 MySigma_c0 pi+ PHSP; #[Reconstructed PDG2011]
  0.190000000 MyLambda_c+ pi+ pi- PHSP; #[Reconstructed PDG2011]
  0.096585366 MySigma_c+ pi0 PHSP; #[Reconstructed PDG2011]
  0.036219512 MyLambda_c+ pi0 pi0 PHSP; #[Reconstructed PDG2011]
  0.004024390 MyLambda_c+ gamma PHSP; #[Reconstructed PDG2011]
  0.240000000 MySigma_c*++ pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.240000000 MySigma_c*0 pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay MyLambda_c(2625)+
  0.670000000 MyLambda_c+ pi+ pi- PHSP; #[Reconstructed PDG2011]
  0.320294118 MyLambda_c+ pi0 PHSP; #[Reconstructed PDG2011]
  0.009705882 MyLambda_c+ gamma PHSP; #[Reconstructed PDG2011]
Enddecay

Decay MyD+
  0.055000000 anti-K*0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.094000000 anti-K0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002773020 anti-K_10 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002927076 anti-K_2*0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.003312218 pi0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002002736 eta mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000385142 eta' mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002500000 rho0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002156793 omega mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.039000000 K- pi+ mu+ nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.001078397 anti-K0 pi0 mu+ nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.000382000 mu+ nu_mu PHOTOS SLN; #[Reconstructed PDG2011]
Enddecay

Decay MyD0
  0.019800000 K*- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.033100000 K- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000815539 K_1- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.001374504 K_2*- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002370000 pi- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002015940 rho- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.001007970 anti-K0 pi- mu+ nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.000549802 K- pi0 mu+ nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.000000000 mu+ mu- PHSP; #[Reconstructed PDG2011]
  #
  0.021700000 K*- e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.035500000 K- e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000760000 K_1- e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.001374504 K_2*- e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.038900000 K- pi+ PHSP; #[Reconstructed PDG2011]
Enddecay

Decay MyD_s+
  0.018309605 phi mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.022845082 eta mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.008186726 eta' mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002058115 anti-K0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000762265 anti-K*0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.005800000 mu+ nu_mu PHOTOS SLN; #[Reconstructed PDG2011]
  #
  0.024900000 phi e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.026700000 eta e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.009900000 eta' e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002058115 anti-K0 e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000762265 anti-K*0 e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002300000 omega pi+ SVS; #[Reconstructed PDG2011]
  0.000200000 rho0 pi+ SVS; #[Reconstructed PDG2011]
  0.000304906 rho+ pi0 SVS; #[Reconstructed PDG2011]
Enddecay

Decay MyLambda_c+
  0.020000000 mu+ nu_mu Lambda0 PYTHIA 42; #[Reconstructed PDG2011]
  0.00500 mu+ nu_mu Sigma0 PYTHIA 42;
  0.00500 mu+ nu_mu Sigma*0 PYTHIA 42;
  0.00300 mu+ nu_mu n0 PYTHIA 42;
  0.00200 mu+ nu_mu Delta0 PYTHIA 42;
  0.00600 mu+ nu_mu p+ pi- PYTHIA 42;
  0.00600 mu+ nu_mu n0 pi0 PYTHIA 42;
  #
  0.021000000 e+ nu_e Lambda0 PYTHIA 42; #[Reconstructed PDG2011]
  0.00500 e+ nu_e Sigma0 PYTHIA 42;
  0.00500 e+ nu_e Sigma*0 PYTHIA 42;
  0.00300 e+ nu_e n0 PYTHIA 42;
  0.00200 e+ nu_e Delta0 PYTHIA 42;
  0.00600 e+ nu_e p+ pi- PYTHIA 42;
  0.00600 e+ nu_e n0 pi0 PYTHIA 42;
  0.008600000 Delta++ K- PYTHIA 0; #[Reconstructed PDG2011]
  0.02500 Delta++ K*- PYTHIA 0;
  0.023000000 p+ anti-K0 PYTHIA 0; #[Reconstructed PDG2011]
  0.016000000 p+ anti-K*0 PYTHIA 0; #[Reconstructed PDG2011]
  0.00500 Delta+ anti-K0 PYTHIA 0;
  0.010700000 Lambda0 pi+ PYTHIA 0; #[Reconstructed PDG2011]
Enddecay

End

""")

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_EvtGenAfterburner.py") 
include("MC12JobOptions/MultiMuonFilter.py")
include("MC12JobOptions/DiLeptonMassFilter.py")

topAlg.Pythia8B.Commands += ['HardQCD:all = on'] 
#topAlg.Pythia8B.Commands += ['HardQCD:gg2bbbar = on']
#topAlg.Pythia8B.Commands += ['HardQCD:qqbar2bbbar = on'] 
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 9.']
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

# Quark cuts
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.QuarkPtCut = 5.0
topAlg.Pythia8B.AntiQuarkPtCut = 5.0
topAlg.Pythia8B.QuarkEtaCut = 3.5
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = False
topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.VetoDoubleCEvents = False
topAlg.Pythia8B.NHadronizationLoops = 50
topAlg.Pythia8B.NDecayLoops = 1

# List of B-species
include("Pythia8B_i/BPDGCodes.py")

topAlg.EvtInclusiveDecay.userDecayFile = "AbAbbar.DEC"
topAlg.EvtInclusiveDecay.allowAllKnownDecays = False

topAlg.EvtInclusiveDecay.maxNRepeatedDecays = 150

topAlg.EvtInclusiveDecay.applyUserSelection = True
topAlg.EvtInclusiveDecay.userSelRequireOppositeSignedMu = True
topAlg.EvtInclusiveDecay.userSelMu1MinPt = 3500.
topAlg.EvtInclusiveDecay.userSelMu2MinPt = 3500.
topAlg.EvtInclusiveDecay.userSelMu1MaxEta = 3.0
topAlg.EvtInclusiveDecay.userSelMu2MaxEta = 3.0
topAlg.EvtInclusiveDecay.userSelMinDimuMass = 4000.
topAlg.EvtInclusiveDecay.userSelMaxDimuMass = 10000.

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 0
topAlg.MultiMuonFilter.Ptcut = 3500.
topAlg.MultiMuonFilter.Etacut = 3.0
topAlg.MultiMuonFilter.NMuons = 2
topAlg.DiLeptonMassFilter.MinPt = 3500.
topAlg.DiLeptonMassFilter.MaxEta = 3.0
topAlg.DiLeptonMassFilter.MinMass = 4000.
topAlg.DiLeptonMassFilter.MaxMass = 10000.
topAlg.DiLeptonMassFilter.MinDilepPt = 6000.
topAlg.DiLeptonMassFilter.AllowSameCharge = False

