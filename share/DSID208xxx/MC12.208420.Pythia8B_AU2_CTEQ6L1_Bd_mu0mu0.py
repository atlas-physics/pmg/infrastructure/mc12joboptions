######################################################
# Job options for Pythia8B_i generation of Bd->mu0mu0  
######################################################
evgenConfig.description = "Exclusive Bd->mu0mu0 generation"
evgenConfig.keywords = ["exclusiveB","Bd","dimuons"]
evgenConfig.minevents = 5000
 
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")
# the file above includes also the options commented below:
#topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
#topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
#topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
#topAlg.Pythia8B.SelectBQuarks = True
#topAlg.Pythia8B.SelectCQuarks = False
#include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 5.']

topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 2.5
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 4.0
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents = True

topAlg.Pythia8B.Commands += ['511:addChannel = 2 1.0 0 -13 13']
topAlg.Pythia8B.SignalPDGCodes = [511,-13,13]

topAlg.Pythia8B.NHadronizationLoops = 1

topAlg.Pythia8B.TriggerPDGCode = 0
#topAlg.Pythia8B.TriggerStatePtCut = [0.0]
#topAlg.Pythia8B.TriggerStateEtaCut = 102.5
#topAlg.Pythia8B.MinimumCountPerCut = [1]

