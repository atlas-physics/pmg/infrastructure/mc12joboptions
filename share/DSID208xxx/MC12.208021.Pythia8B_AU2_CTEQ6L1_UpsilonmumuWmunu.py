##############################################################
# Upsilon(mu2.5mu2.5)X + W->munu  
# M. Watson - S. Leontsinis
# For the study of associated production W+Y
##############################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "pp->Upsilon(mumu) + W->munu via second hard process"
evgenConfig.keywords = ["bottomonium","dimuons","Upsilon","W"]
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8B_Bottomonium_Common.py") 
topAlg.Pythia8B.Commands += ['553:onMode = off']
topAlg.Pythia8B.Commands += ['553:3:onMode = on']
topAlg.Pythia8B.Commands += ['24:onMode = off']
topAlg.Pythia8B.Commands += ['24:7:onMode = on']
topAlg.Pythia8B.Commands += ['SecondHard:generate = on']
topAlg.Pythia8B.Commands += ['SecondHard:SingleW = on']
topAlg.Pythia8B.SignalPDGCodes = [553,-13,13]
topAlg.Pythia8B.SignalPtCuts = [0.0,2.5,2.5]
topAlg.Pythia8B.SignalEtaCuts = [102.5,2.5,2.5]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [20.0]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [1]
