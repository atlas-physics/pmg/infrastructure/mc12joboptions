#######################################################################
# Job options for Pythia8B_i generation of Bs->J/psi(mu3p5m3p5)phi(KK).
#######################################################################

evgenConfig.description = "Signal Bs->J/psi(mu3p5mu3p5)phi(KK)"
evgenConfig.keywords    = [ "exclusiveB", "Bs", "dimuons" ]
evgenConfig.minevents   = 50

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

topAlg.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 8.' ]
topAlg.Pythia8B.QuarkPtCut      = 0.0
topAlg.Pythia8B.AntiQuarkPtCut  = 8.0
topAlg.Pythia8B.QuarkEtaCut     = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

topAlg.Pythia8B.Commands += [ '531:onIfMatch = 443 333' ]
topAlg.Pythia8B.Commands += [ '443:onMode = off' ]
topAlg.Pythia8B.Commands += [ '443:onIfMatch = -13 13' ]
topAlg.Pythia8B.SignalPDGCodes = [   531,   443,   -13,    13,   333, 321, -321 ]
topAlg.Pythia8B.SignalPtCuts   = [   0.0,   0.0,   0.0,   0.0,   0.0, 0.5,  0.5 ] # no muon pT cut here - see trigger cuts below
topAlg.Pythia8B.SignalEtaCuts  = [ 102.5, 102.5, 102.5, 102.5, 102.5, 2.6,  2.6 ] # no muon eta cut here - see trigger cuts below
# Select the required method in the userselections file.
topAlg.Pythia8B.UserSelection = 'BJPSIPHI_TRANS'
# Pass the requested physical parameters to the algorithm.
topAlg.Pythia8B.UserSelectionVariables = [ 1.0, 1.2, 1.0, 0.542, 0.227, 0.0, 0.6596, 0.0810, 17.68, 0.15, 2.95, 3.15, 2.98 ]
# Meaning of each double in the array - userVars[x]:
# [0] 0(flat)/1(angle)
# [1] prob_limit
# [2] tag mode
# [3] A0^2        0.542  +/- 0.011            PDG2013
# [4] Al^2        0.227  +/- 0.010            PDG2013
# [5] As^2        0.042  +/- 0.015 +/- 0.018  Phys. Rev. Lett. 108, 101803 (2012)
# [6] GammaS      0.6596 +/- 0.0048           PDG2013
# [7] DeltaGamma  0.081  +/- 0.011            PDG2013
# [8] DeltaM      17.68  +/- 0.08             PDG2013
# [9] phiS        0.15   +/- 0.18 +/- 0.06    Phys. Rev. Lett. 108, 101803 (2012) /added/
#[10] delta_p     2.95   +/- 0.37 +/- 0.12    Phys. Rev. Lett. 108, 101803 (2012)
#[11] delta_l     3.15   +/- 0.22             PDG2013
#[12] delta_s     2.98   +/- 0.36 +/- 0.12    Phys. Rev. Lett. 108, 101803 (2012) /added/

topAlg.Pythia8B.NHadronizationLoops = 2

topAlg.Pythia8B.TriggerPDGCode     = 13
topAlg.Pythia8B.TriggerStatePtCut  = [ 3.5 ]
topAlg.Pythia8B.TriggerStateEtaCut = 2.6
topAlg.Pythia8B.MinimumCountPerCut = [ 2 ]
