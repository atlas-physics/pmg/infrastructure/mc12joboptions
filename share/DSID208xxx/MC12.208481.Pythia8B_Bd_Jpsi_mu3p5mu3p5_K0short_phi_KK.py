########################################################################
# Job options for Pythia8B_i generation of B0 -> J/psi(mumu) K_S0 phi
########################################################################
evgenConfig.description = "B0 -> J/psi K_S0 phi(KK)"
evgenConfig.keywords = ["BtoJpsi","dimuons","pentaquarks"]
evgenConfig.minevents = 1000

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

# Hard process
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
#
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.']

# Event selection
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.VetoDoubleCEvents = False
#
topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.QuarkEtaCut = 1000.
topAlg.Pythia8B.AntiQuarkPtCut = 8.0
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

# J/psi
topAlg.Pythia8B.Commands += ['443:m0 = 3.0969']        # PDG 2017
topAlg.Pythia8B.Commands += ['443:mWidth = 0.0000929'] # PDG 2017
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']

# B_d:
topAlg.Pythia8B.Commands += ['511:m0 = 5.27963']  # PDG 2017
topAlg.Pythia8B.Commands += ['511:tau0 = 0.4557'] # PDG 2017

# B_b decays: channels 850-869 have onMode = 2 in Pythia8 default
topAlg.Pythia8B.Commands += ['511:onMode = 3']

topAlg.Pythia8B.Commands += ['511:850:onMode = 0']
topAlg.Pythia8B.Commands += ['511:851:onMode = 0']
topAlg.Pythia8B.Commands += ['511:852:onMode = 0']
topAlg.Pythia8B.Commands += ['511:853:onMode = 0']
topAlg.Pythia8B.Commands += ['511:854:onMode = 0']
topAlg.Pythia8B.Commands += ['511:855:onMode = 0']
topAlg.Pythia8B.Commands += ['511:856:onMode = 0']
topAlg.Pythia8B.Commands += ['511:857:onMode = 0']
topAlg.Pythia8B.Commands += ['511:858:onMode = 0']
topAlg.Pythia8B.Commands += ['511:859:onMode = 0']
topAlg.Pythia8B.Commands += ['511:860:onMode = 0']
topAlg.Pythia8B.Commands += ['511:861:onMode = 0']
topAlg.Pythia8B.Commands += ['511:862:onMode = 0']
topAlg.Pythia8B.Commands += ['511:863:onMode = 0']
topAlg.Pythia8B.Commands += ['511:864:onMode = 0']
topAlg.Pythia8B.Commands += ['511:865:onMode = 0']
topAlg.Pythia8B.Commands += ['511:866:onMode = 0']
topAlg.Pythia8B.Commands += ['511:867:onMode = 0']
topAlg.Pythia8B.Commands += ['511:868:onMode = 0']
topAlg.Pythia8B.Commands += ['511:869:onMode = 0']

topAlg.Pythia8B.Commands += ['511:addChannel = 2 1. 0 443 310 333']

topAlg.Pythia8B.OutputLevel = INFO
topAlg.Pythia8B.NHadronizationLoops = 4

topAlg.Pythia8B.SignalPDGCodes = [511,443,13,-13,310,333,321,-321]
topAlg.Pythia8B.SignalPtCuts = [0.,0.,0.,0.,1.,0.,0.8,0.8]
topAlg.Pythia8B.SignalEtaCuts = [1000.,1000.,1000.,1000.,1000.,1000.,2.5,2.5]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.TriggerOppositeCharges = True
topAlg.Pythia8B.MinimumCountPerCut = [2]
