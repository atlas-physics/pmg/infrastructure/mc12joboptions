## POWHEG+Pythia8 Wminmunu_nn

evgenConfig.description = "POWHEG+Pythia8 Wminmunu for neutron-neutron beam, no lepton filter, AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "mu", "neutron-neutron beam"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wminmunu_nn"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
