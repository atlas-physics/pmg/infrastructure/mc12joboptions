## A.A.,A.O., Feb '14
evgenConfig.description = "Dijet truth jet slice JZ0Mpt4R04, with the AU2 CT10 tune"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
## Run with a min bias process: no pThat cut possible
topAlg.Pythia8.Commands += \
    ["SoftQCD:minBias = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]

include("MC12JobOptions/JetFilter_JZ0Mpt4R04.py")
evgenConfig.minevents = 5000
