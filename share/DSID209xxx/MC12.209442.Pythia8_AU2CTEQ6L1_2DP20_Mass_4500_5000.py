evgenConfig.description = "Pythia8 diphoton sample. gammagamma events with at least two photons with pT > 20 GeV, with mass cut 4500<Mgg<5000 GeV"
evgenConfig.keywords = ["exotics", "photons"]
evgenConfig.contact = [ "daniel.hayden@cern.ch", "fmalek@lpsc.in2p3.fr" ]
evgenConfig.minevents = 500


include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

## Configure Pythia
topAlg.Pythia8.Commands += ["PromptPhoton:ffbar2gammagamma = on",
                            "PromptPhoton:gg2gammagamma = on",
                            "PhaseSpace:pTHatMin = 18",
                            "PhaseSpace:mHatMin = 4500.",
                            "PhaseSpace:mHatMax = 5000."]

#-------------------------------------------------------------
# Filter
#-------------------------------------------------------------

include("MC12JobOptions/DirectPhotonFilter.py")

topAlg.DirectPhotonFilter.Ptcut = 20000.
topAlg.DirectPhotonFilter.Etacut =  2.7
topAlg.DirectPhotonFilter.NPhotons = 2


