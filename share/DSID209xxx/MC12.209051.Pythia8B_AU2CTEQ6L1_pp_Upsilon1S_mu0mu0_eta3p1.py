##############################################################
# Job options fragment for pp->Upsilon1S(mu0mu0)X, eta < 3.1
##############################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "pp->Upsi(mu0mu0)"
evgenConfig.keywords = ["bottomonium","dimuons"]
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8B_Bottomonium_Common.py")
topAlg.Pythia8B.Commands += ['553:onMode = off']
topAlg.Pythia8B.Commands += ['553:3:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [553,-13,13]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [0.0]
topAlg.Pythia8B.TriggerStateEtaCut = 3.1
topAlg.Pythia8B.MinimumCountPerCut = [2]

