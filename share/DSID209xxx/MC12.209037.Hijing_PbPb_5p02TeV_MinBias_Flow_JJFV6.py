###############################################################
#
# Job options file for Hijing generation of
# Pb + Pb collisions at 5020 GeV/(colliding nucleon pair)
#
# Flow with new LHC physics flow full v2-v6
# using function jjia_minbias_new
# =========================================
# Version for > FlowAfterburner-00-02-00
# =========================================
#
# Andrzej Olszewski
#
# November 2014
#==============================================================

# use common fragment
include("MC12JobOptions/Hijing_Common.py")

from FlowAfterburner.FlowAfterburnerConf import AddFlowByShifting
topAlg += AddFlowByShifting()

evgenConfig.description = "Hijing"
evgenConfig.keywords = ["PbPb", "MinBias", "Flow"]
evgenConfig.contact = ["Andrzej.Olszewski@ifj.edu.pl"]

#----------------------
# Hijing Parameters
#----------------------
Hijing = topAlg.Hijing
Hijing.McEventKey = "HIJING_EVENT"
Hijing.Initialize = ["efrm 5020.", "frame CMS", "proj A", "targ A",
                    "iap 208", "izp 82", "iat 208", "izt 82",
# simulation of minimum-bias events
                    "bmin 0", "bmax 20",
# turns OFF jet quenching:
                    "ihpr2 4 0",
# Jan24,06 turns ON decays charm and  bottom but not pi0, lambda, ... 
                    "ihpr2 12 2",
# turns ON retaining of particle history - truth information:
                    "ihpr2 21 1"]


AddFlowByShifting = topAlg.AddFlowByShifting
AddFlowByShifting.McTruthKey    = "HIJING_EVENT"
AddFlowByShifting.McFlowKey     = "GEN_EVENT"

#----------------------
# Flow Parameters
#----------------------
AddFlowByShifting.FlowFunctionName = "jjia_minbias_new"
AddFlowByShifting.FlowImplementation = "exact"

AddFlowByShifting.RandomizePhi  = 0
