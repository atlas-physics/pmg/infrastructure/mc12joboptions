## POWHEG+Pythia8 Wplusenu_nn

evgenConfig.description = "POWHEG+Pythia8 Wplusenu for neutron-neutron beam, no lepton filter, AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "e", "neutron-neutron beam"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplusenu_nn"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
