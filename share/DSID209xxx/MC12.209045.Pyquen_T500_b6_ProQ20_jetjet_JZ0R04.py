evgenConfig.description = "Dijet truth jet slice JZ0, with Pythia 6 Pro-Q20 plus Pyquen jet quenching moved and R=0.4"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pyquen_T500_b6_ProQ20_Common.py")

# use min bias ND for this slice
topAlg.Pyquench.PythiaCommand += [  "pysubs msel 0",
                           # ND
                           "pysubs msub 11 1",
                           "pysubs msub 12 1",
                           "pysubs msub 13 1",
                           "pysubs msub 28 1",
                           "pysubs msub 53 1",
                           "pysubs msub 68 1",
                           "pysubs msub 95 1",
                           ]

include("MC12JobOptions/JetFilter_JZ0Mpt4R04.py")
evgenConfig.minevents = 5000

