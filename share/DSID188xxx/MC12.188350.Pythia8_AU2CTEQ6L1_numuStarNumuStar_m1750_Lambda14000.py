
evgenConfig.description = "Excited   numuStarNumuStar  mStar=1750 Laambda=14000, with the AU2 CTEQ6L1 tune"
evgenConfig.contact = ["Olya Igonkina"]

#Excited Lepton ID
leptID = 4000014

# Excited lepton Mass (in GeV)
M_ExNu = 1750.

# Mass Scale parameter (Lambda, in GeV)
M_Lam = 14000.

# this is double excited lepton production
include("MC12JobOptions/Pythia8_AU2CTEQ6L1_ExcitedNueNue.py")

