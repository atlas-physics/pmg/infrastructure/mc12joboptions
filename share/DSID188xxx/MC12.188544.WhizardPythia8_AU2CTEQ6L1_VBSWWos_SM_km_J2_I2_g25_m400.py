############################################################################################################
# Whizard lhef showered with Pythia8
# Ulrike Schnoor <ulrike.schnoor@cern.ch>
# Generators/MC12JobOptions/tags/MC12JobOptions-00-05-70/share/MC12.xxxxxx.LHEFPythia8_AU2CTEQ6L1_example.py
############################################################################################################                                                                       
evgenConfig.description = "Whizard+Pythia8  VBSos qq-->lvlvjj spin2 isospin2 g2.5 mass400"
evgenConfig.keywords = ["EW"]
evgenConfig.inputfilecheck = "VBSWWos_SM_km_J2_I2_g25_m400"

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

evgenConfig.contact = ["Jiaming Yu <jiamingy@umich.edu>"]

evgenConfig.generators = ["Whizard", "Pythia8" ]
