evgenConfig.description = "POWHEG+PYTHIA8 ggH H->AA->4photons with AU2,CT10, mH=900, mA=440"
evgenConfig.keywords = ["SMhiggs", "ggH", "A","4photons","4gamma","multiphoton"]
evgenConfig.contact  = [ "j.beacham@cern.ch" ]
evgenConfig.generators += [ "Powheg", "Pythia8" ]
evgenConfig.inputfilecheck = "ggH_SM_M900"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
    "35:onMode = off", # decay of our Higgs
    "35:addChannel 1 1 101 36 36",
    "35:onIfMatch = 36 36", # decay H --> AA
    "36:onMode = off",
    "36:addChannel 1 1 101 22 22",
    "36:m0 = 440.0", # set A mass
    "36:mMin = 2.0",
    "36:onIfMatch = 22 22" # decay A --> 2photon
    ]
