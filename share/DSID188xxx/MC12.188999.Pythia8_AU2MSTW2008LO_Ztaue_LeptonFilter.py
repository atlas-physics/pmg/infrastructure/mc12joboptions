
evgenConfig.description = "Z->tau e production with the AU2 MSTW2008LO tune 12.2012"
evgenConfig.keywords = ["exotics", "Z", "tau", "electron", "LFV"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact = ["Hartger Weits"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # create Z bosons
                            "PhaseSpace:mHatMin = 60.",       # lower invariant mass
                            "23:onMode = off",                # switch off all Z decays
                            "23:oneChannel = 1 0.5 0 15 -11", # Z->tau-e+
                            "23:addChannel = 1 0.5 0 -15 11"] # Z->tau+e-

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 5000.    
topAlg.MultiLeptonFilter.Etacut = 2.8
topAlg.MultiLeptonFilter.NLeptons = 1
