#  choose the pdf
# ---------------
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10"]

evgenConfig.generators += ["MadGraph","Pythia8"]
evgenConfig.contact = ["Frederick.Dallaire@cern.ch"]
evgenConfig.description = "MWT R1,R2 -> Zh -> vv+jj, M(A)=1600 GeV, gtilde = 2, LHEF produced with MadGraph4 v4.4.52"
evgenConfig.keywords = ["MWT Dibosons Multileptons"]
evgenConfig.inputfilecheck = 'MWT'
