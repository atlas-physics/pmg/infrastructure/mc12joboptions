evgenConfig.contact = [ "daniel.hayden@cern.ch" ]
evgenConfig.generators = [ "Pythia8" ]
evgenConfig.description = "Z'->ee production"
evgenConfig.keywords = ["HeavyBoson", "Zprime", "leptons"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

# Zprime resonance mass (in GeV)
ZprimeMass = 3000
ckin1 = 4000
ckin2 = 4500

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",  # create Z' bosons
                            "Zprime:gmZmode = 0",                  # Z',Z,g with interference
                            "Zprime:vd = 0.942860477",
                            "Zprime:ad = -0.733762222",
                            "Zprime:vu = -0.524663968",
                            "Zprime:au = 0.733762222",
                            "Zprime:ve = 0.106467459",
                            "Zprime:ae = -0.733762222",
                            "Zprime:vnue = -0.313647381",
                            "Zprime:anue = -0.313647381",
                            "PhaseSpace:mHatMin = "+str(ckin1),    # lower invariant mass
                            "PhaseSpace:mHatMax = "+str(ckin2),    # upper invariant mass
                            "32:onMode = off",                     # switch off all Z decays
                            "32:onIfAny  = 11",                    # switch on Z->ee decays
                            "32:m0 = "+str(ZprimeMass)]
