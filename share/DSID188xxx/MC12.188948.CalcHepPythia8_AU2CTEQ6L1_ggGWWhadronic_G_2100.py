evgenConfig.contact = ["angel.campoverde@cern.ch"]
evgenConfig.description = "JO with the AU2 CTEQ6L1 tune for RS Graviton to WW->qqqq with calcHEP v3.4 for a Graviton mass of 2100 GeV"
evgenConfig.keywords = ["RSGraviton",  "diboson",  "hadronic", "dijet_resonance", "schan", "exotics"]
evgenConfig.inputfilecheck = "RSGWWqqqq"
evgenConfig.saveJets = True
evgenConfig.generators = ["CalcHep", "Pythia8"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
