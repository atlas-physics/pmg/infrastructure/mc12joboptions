evgenConfig.description = 'ttgam to ljets with extra photons using MadGraph/Photos/Pythia'
evgenConfig.keywords =["EW","ttbar","photon"]
evgenConfig.contact = ["loginov@fnal.gov"]
evgenConfig.inputfilecheck = 'MadGraph.167734.ttgam'
evgenConfig.generators += ["MadGraph"]

include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )

## ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )

## PHOTOS config for PYTHIA -- adjusted for the sample (!!!!)
## Disable native QED FSR
assert hasattr(topAlg, "Pythia")
topAlg.Pythia.PythiaCommand += ["pydat1 parj 90 20000"]

from Photos_i.Photos_iConf import Photos
topAlg += Photos()

topAlg.Photos.PhotosCommand = [
    "photos pmode 1",
    "photos xphcut 0.01",
    "photos alpha -1.",
    "photos interf 1",
    "photos isec 1",
    "photos itre 0",
    "photos iexp 1",
    "photos iftop 1"]

evgenConfig.generators += [ "Photos" ]

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                                 "pydat1 parj 90 20000",  # Turn off FSR (use Photos instead)
                                 "pydat3 mdcy 15 1 0"    # Turn off tau decays (use Tauola instead)
                                 ]
