evgenConfig.description = 'ttgam_ljets using MadGraph/Pythia'
evgenConfig.keywords =["EW","ttbar","gamma","semileptonic"]
evgenConfig.contact = ["loginov@fnal.gov"]
evgenConfig.inputfilecheck = 'MadGraph.167734.ttgam'
evgenConfig.generators += ["MadGraph"]

## ... PDF
include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )

## ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                                 "pydat1 parj 90 20000",  # Turn off FSR (have photons generated in MadGraph)
                                 "pydat3 mdcy 15 1 0"    # Turn off tau decays (use Tauola instead)
                                 ]
