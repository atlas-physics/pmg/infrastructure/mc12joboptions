include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "VBF-Z production, with Z/gamma* -> tt. Yukawa tau disabled"
evgenConfig.keywords = [ "Sherpa", "VBF" ]
evgenConfig.contact  = [ "kiran.joshi@cern.ch" ]
evgenConfig.minevents = 100

evgenConfig.process="""
(run){
  YUKAWA_TAU=0.0
}(run)

(processes){
  Process 93 93 -> 15 -15 93 93 93{1}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05
  End process;
}(processes)

(selector){
  Mass 15 -15 40 E_CMS
  NJetFinder 2 15.0 0.0 0.4 1
}(selector)

"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.inputconfcheck = 'Sherpa_CT10_Ztt2JetsEWinctch_Mll40_NoYtau'
