include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
## ... Tauola
#include ( "MC12JobOptions/Pythia_Tauola.py" )

## ... Photos
include ( "MC12JobOptions/Pythia_Photos.py" )


topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                                 "pydat1 parj 90 20000", # Turn off FSR.
                                 "pydat3 mdme 190 1 0",
                                 "pydat3 mdme 191 1 0",
                                 "pydat3 mdme 192 1 0",
                                 "pydat3 mdme 193 1 -1",
                                 "pydat3 mdme 194 1 0",
                                 "pydat3 mdme 195 1 0",
                                 "pydat3 mdme 196 1 0",
                                 "pydat3 mdme 197 1 -1",
                                 "pydat3 mdme 198 1 0",
                                 "pydat3 mdme 199 1 0",
                                 "pydat3 mdme 200 1 0",
                                 "pydat3 mdme 201 1 -1",
                                 "pydat3 mdme 202 1 -1",
                                 "pydat3 mdme 203 1 -1",
                                 "pydat3 mdme 204 1 -1",
                                 "pydat3 mdme 205 1 -1",
                                 "pydat3 mdme 206 1 1",#Wenue
                                 "pydat3 mdme 207 1 1",#mu
                                 "pydat3 mdme 208 1 1",
                                 #"pydat3 mdcy 15 1 0"   # Turn off tau decays.
                                 ]

evgenConfig.inputfilecheck = 'madgraph.167006.WWWStar_lnulnulnu'
evgenConfig.generators += ["MadGraph"]
evgenConfig.description = 'WWWStar using MadGraph/Pythia'
evgenConfig.keywords =["EW","W","leptonic"]
