evgenConfig.description = "JHU+PYTHIA8 gg+qq H H->gamgam with AU2,CTEQ6L1 SpinCP 2m"
evgenConfig.keywords = ["SMhiggs", "ggF", "gamma", "SpinCP"]
evgenConfig.inputfilecheck = "gg_qq_H125_gamgam_Spin2m_CTEQ6L1"

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_JHU.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '39:onMode = off',#decay of Higgs
                            ]
