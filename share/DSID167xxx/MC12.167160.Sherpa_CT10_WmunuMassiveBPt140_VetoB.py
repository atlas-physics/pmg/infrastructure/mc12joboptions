include( "MC12JobOptions/Sherpa_CT10_Common.py" )

evgenConfig.description = "Wmunu + jets, with b-quarks treated as massive"
evgenConfig.keywords = [ "Wmunu", "Heavy Flavour" ]
evgenConfig.contact  = ["Paul Thompson <thompson@mail.cern.ch>", "frank.siegert@cern.ch"]
evgenConfig.inputconfcheck = "WmunuMassiveBPt140"
evgenConfig.process="""
(run){
  MASSIVE[11]=1
  MASSIVE[15]=1
  MASSIVE[5]=1

  ME_SIGNAL_GENERATOR=Comix
}(run)

(processes){
  Process 93 93 -> 90 91 93 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;

  Process -5 5 -> 90 91 93 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;

  Process -5 -5 -> -5 -5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;

  Process -5 5 -> -5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;
  
  Process -5 93 -> -5 -5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;

  Process -5 93 -> -5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;
  
  Process 5 5 -> 5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;

  Process 5 93 -> -5 5 5 90 91 93{1}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}  
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;

  Process 5 93 -> 5 90 91 93{3}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;

  Process 93 93 -> -5 5 90 91 93{2}
  Order_EW 2; Max_N_Quarks 6
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;
}(processes)

(selector){
  Mass 90 91 1.7 E_CMS
  PT2 90 91 140.0 E_CMS
  PT 5 1.0 E_CMS
  PT -5 1.0 E_CMS
}(selector)
"""

from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
topAlg += HeavyFlavorHadronFilter()
HeavyFlavorHadronFilter = topAlg.HeavyFlavorHadronFilter
HeavyFlavorHadronFilter.RequestBottom=True
HeavyFlavorHadronFilter.RequestCharm=False
HeavyFlavorHadronFilter.Request_cQuark=False
HeavyFlavorHadronFilter.Request_bQuark=False
HeavyFlavorHadronFilter.RequestSpecificPDGID=False
HeavyFlavorHadronFilter.RequireTruthJet=False
HeavyFlavorHadronFilter.BottomPtMin=0*GeV
HeavyFlavorHadronFilter.BottomEtaMax=4.0
StreamEVGEN.VetoAlgs += [ "HeavyFlavorHadronFilter" ]
