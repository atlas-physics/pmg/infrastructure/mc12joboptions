evgenConfig.description = "130GeV Higgs decay to Z(mumu)Gamma from MCFM interfaced to Pythia8 in MC12"
evgenConfig.keywords = ["SMhiggs","ggF","Z","gamma","leptonic","mu"]
evgenConfig.contact = ["dartyin@cern.ch"]
evgenConfig.inputfilecheck = 'MCFM63.167624.ggH130Zmumugamma'

include ( "MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py" )
include("MC12JobOptions/Pythia8_MCFM.py")
include ( "MC12JobOptions/Pythia8_Photos.py" ) 

topAlg.Pythia8.Commands += [ "Init:showAllParticleData = on",
                             "Next:numberShowLHA = 10",
                             "Next:numberShowEvent = 10"
                             ]
