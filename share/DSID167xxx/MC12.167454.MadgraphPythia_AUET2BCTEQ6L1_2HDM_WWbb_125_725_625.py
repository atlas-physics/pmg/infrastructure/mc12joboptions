include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )
topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1", "pyinit pylistf 1","pyinit dumpr 1 2"
                                ]
evgenConfig.generators += ["MadGraph"]
evgenConfig.description = "2HDM: H0 -> H+-W -> WWh -> WWbb"
evgenConfig.keywords = ["nonSMhiggs","semileptonic","exotics"]
evgenConfig.inputfilecheck = '2HDM_125_725_625'
