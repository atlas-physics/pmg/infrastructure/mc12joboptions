evgenConfig.description = "Zee with massive C and B quarks, C Jet filter and B hadron veto applied"
evgenConfig.keywords = [ "Z", "leptonic", "e", "heavyquark" ]
evgenConfig.contact  = ["Paul Thompson <thompson@mail.cern.ch>", "frank.siegert@cern.ch"]
evgenConfig.process="Zee"
evgenConfig.inputconfcheck = "ZeeMassiveCBPt0"
evgenConfig.minevents = 2000

include( "MC12JobOptions/Sherpa_CT10_Common.py" )

"""
(run){
  MASSIVE[4]=1
  MASSIVE[5]=1
}(run)

(processes){
Process 93  93 -> 11 -11 93{4}
Order_EW 2
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 5  -5 -> 11 -11 93{4}
Order_EW 2
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 4 -4 -> 11 -11 93{4}
Order_EW 2
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
End process;

Process 93 93 -> 11 -11 5 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 5 -5 -> 11 -11 5 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93 5 -> 11 -11 5 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93 -5 -> 11 -11 -5 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93 93 -> 11 -11 4 -4 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93  4 -> 11 -11 4 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93 -4 -> 11 -11 -4 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process  4 -4 -> 11 -11 4 -4 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process  5 -5 -> 11 -11 4 -4 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process  4 -4 -> 11 -11 5 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93  5 -> 11 -11 5 4 -4 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93  4 -> 11 -11 4 5 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93 -5 -> 11 -11 -5 4 -4 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process 93 -4 -> 11 -11 -4 5 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process  4  5 -> 11 -11 4 5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process -4  5 -> 11 -11 -4 5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process  4 -5 -> 11 -11 4 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;

Process -4 -5 -> 11 -11 -4 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1
End process;
}(processes)

(selector){
Mass 11 -11 40 E_CMS
PT 93 1.0 E_CMS
PT 4 1.0 E_CMS
PT -4 1.0 E_CMS
PT 5 1.0 E_CMS
PT -5 1.0 E_CMS
}(selector)
"""

from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
HeavyFlavorBHadronFilter = HeavyFlavorHadronFilter(name="HeavyFlavorBHadronFilter")
HeavyFlavorBHadronFilter.RequestBottom=True
HeavyFlavorBHadronFilter.RequestCharm=False
HeavyFlavorBHadronFilter.Request_cQuark=False
HeavyFlavorBHadronFilter.Request_bQuark=False
HeavyFlavorBHadronFilter.RequestSpecificPDGID=False
HeavyFlavorBHadronFilter.RequireTruthJet=False
HeavyFlavorBHadronFilter.BottomPtMin=0*GeV
HeavyFlavorBHadronFilter.BottomEtaMax=4.0
#
HeavyFlavorCHadronFilter = HeavyFlavorBHadronFilter.clone('HeavyFlavorCHadronFilter')
HeavyFlavorCHadronFilter.RequestBottom=False
HeavyFlavorCHadronFilter.RequestCharm=True
HeavyFlavorCHadronFilter.CharmPtMin=0*GeV
HeavyFlavorCHadronFilter.CharmEtaMax=4.0
#
topAlg += HeavyFlavorBHadronFilter
topAlg += HeavyFlavorCHadronFilter
StreamEVGEN.RequireAlgs += [ "HeavyFlavorCHadronFilter" ]
StreamEVGEN.VetoAlgs += [ "HeavyFlavorBHadronFilter" ]
