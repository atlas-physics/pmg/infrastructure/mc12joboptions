evgenConfig.description = 'ttgam_wbwb using MadGraph/Photos/Pythia/'
evgenConfig.keywords =["EW","ttbar","gamma","semileptonic"]
evgenConfig.contact = ["loginov@fnal.gov"]
evgenConfig.inputfilecheck = 'MadGraph.167735.ttgam_t2wb2lvb_tbar2wb2jjb'
evgenConfig.generators += ["MadGraph"]

## ... PDF
include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )

## ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )

## ... Photos
include ( "MC12JobOptions/Pythia_Photos.py" )

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                                 "pydat1 parj 90 20000",  # Turn off FSR (use Photos instead)
                                 "pydat3 mdcy 15 1 0"    # Turn off tau decays (use Tauola instead)
                                 ]
