evgenConfig.description = "PYTHIA8 ZH H->WW with >= 2 lepton filter with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "ZH", "W","leptonic", "hadronic"]

# ... Pythia
include ( "MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py" )
# ... Photos
include ( "MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                             '25:m0 = 125',
                             '25:mWidth = 0.380',
                             '25:onMode = off',
                             '25:doForceWidth = true',
                             '25:onIfMatch = 24 24',
                             'HiggsSM:ffbar2HZ = on',
                             '23:onMode = off',                           
                             '23:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16',
                             '24:mMin = 2.0',
                             '24:onMode = off',                           
                             '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                             ]


if not hasattr(topAlg, "VHtoVVDiLepFilter"):
    from GeneratorFilters.GeneratorFiltersConf import VHtoVVDiLepFilter
    topAlg += VHtoVVDiLepFilter()

if "VHtoVVDiLepFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["VHtoVVDiLepFilter"]

topAlg.VHtoVVDiLepFilter.PDGGrandParent = 25
topAlg.VHtoVVDiLepFilter.PDGParent = 24
topAlg.VHtoVVDiLepFilter.PDGAssoc = 23
topAlg.VHtoVVDiLepFilter.StatusCode = 22
topAlg.VHtoVVDiLepFilter.PDGChildren = [11,13,15]

#--------------------------------------------------------------
