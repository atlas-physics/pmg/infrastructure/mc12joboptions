evgenConfig.description = "PYTHIA8 ZH H->mumu with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "ZH", "mu"]

#include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:m0 = 100',
                            '25:mWidth = 0.00249',
                            '25:onMode = off',
                            '25:doForceWidth = true',
                            '25:onIfMatch = 13 13',
                            'HiggsSM:ffbar2HZ = on',
                            '23:onMode = off',
                            '23:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                            ]
