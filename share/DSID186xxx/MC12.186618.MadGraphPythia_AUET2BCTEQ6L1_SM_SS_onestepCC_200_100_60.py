include('MC12JobOptions/MadGraphControl_SM_SS_onestepCC.py')
include('MC12JobOptions/METFilter.py')
topAlg.METFilter.MissingEtCut = 100*GeV
include ( 'MC12JobOptions/MultiLeptonFilter.py' )
topAlg.MultiLeptonFilter.Ptcut = 5000.
topAlg.MultiLeptonFilter.Etacut = 2.8
topAlg.MultiLeptonFilter.NLeptons = 1
evgenConfig.minevents=100
