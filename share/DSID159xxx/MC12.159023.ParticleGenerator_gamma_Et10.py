evgenConfig.description = "Single photons with ET=10 GeV"
evgenConfig.keywords = ["singleparticle", "gamma"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: constant 22",
 "et: constant 10000",
 "eta: flat -2.5 2.5",
 "phi: flat -3.14159 3.14159"
 ]
