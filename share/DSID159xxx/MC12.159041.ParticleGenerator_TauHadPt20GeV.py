## Single-tau, pt=20 GeV
## author: Martin Flechl, Apr 3, 2012

evgenConfig.description = "Single-particle hadronic tau, pt=20 GeV"
evgenConfig.keywords = ["tau", "hadronic"]

include("MC12JobOptions/ParticleGenerator_Common.py")
include("MC12JobOptions/Photos_Fragment.py")
include("MC12JobOptions/Tauola_HadronicDecay_Fragment.py")

topAlg.ParticleGenerator.orders = [
             "PDGcode: sequence  15 -15",
             "pt: constant 20000",
             "eta: flat -2.7 2.7",
             "phi: flat -3.14159 3.14159"
]
