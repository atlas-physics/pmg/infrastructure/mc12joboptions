evgenConfig.description = "Single Lambda with Pt=5GeV"
evgenConfig.keywords = ["singleparticle", "lambda"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: sequence -3122 3122",
 "pt:  constant 5000",
 "eta: flat -2.7 2.7",
 "phi: flat -3.14159 3.14159"
 ]
