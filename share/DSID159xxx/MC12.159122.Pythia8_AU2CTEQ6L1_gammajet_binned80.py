evgenConfig.description = "Pythia8 gamma+jet performance sample with 80 < photon pT < 150 GeV"
evgenConfig.keywords = ["egamma", "performance", "jets"]
evgenConfig.contact = ["Matt Relich"]

## Configure Pythia
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
topAlg.Pythia8.Commands += ["PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PhaseSpace:pTHatMin = 40"]

# Set up photon filter
include("MC12JobOptions/LeadingPhotonFilter.py")
topAlg.LeadingPhotonFilter.PtMin = 80000.
topAlg.LeadingPhotonFilter.PtMax = 150000.
