evgenConfig.description = "Single neutron with Pt=0.2-30GeV Eta=2.2-2.3"
evgenConfig.keywords = ["singleparticle", "neutron"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: constant 2112",
 "pt: flat 200.0 30000.",
 "eta: flat 2.2 2.3",
 "phi: flat -3.14159 3.14159"
 ]
