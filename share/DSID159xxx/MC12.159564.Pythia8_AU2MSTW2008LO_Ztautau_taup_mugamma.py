evgenConfig.description = "Z->tautau, tau+ ->mu+ gamma production, NO lepton filter and AU2 MSTW2008LO tune"
evgenConfig.keywords = ["electroweak", "Z ", "tau", "leptons"]
evgenConfig.contact = ['Olga.Igonkina_@_cern.ch']

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # create Z/gamma* bosons, fill interference
                            "PhaseSpace:mHatMin = 20.", # lower invariant mass
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 15", # switch on Z->tau tau decays
                            "15:1:onMode = 3",
                            "15:2:onMode = 3",
                            "15:3:onMode = 3",
                            "15:4:onMode = 3",
                            "15:5:onMode = 3",
                            "15:6:onMode = 3",
                            "15:7:onMode = 3",
                            "15:8:onMode = 3",
                            "15:9:onMode = 3",
                            "15:10:onMode = 3",
                            "15:11:onMode = 3",
                            "15:12:onMode = 3",
                            "15:13:onMode = 3",
                            "15:14:onMode = 3",
                            "15:15:onMode = 3",
                            "15:16:onMode = 3",
                            "15:17:onMode = 3",
                            "15:18:onMode = 3",
                            "15:19:onMode = 3",
                            "15:20:onMode = 3",
                            "15:21:onMode = 3",
                            "15:22:onMode = 3",
                            "15:23:onMode = 3",
                            "15:24:onMode = 3",
                            "15:25:onMode = 3",
                            "15:26:onMode = 3",
                            "15:27:onMode = 3",
                            "15:28:onMode = 3",
                            "15:29:onMode = 3",
                            "15:30:onMode = 3",
                            "15:31:onMode = 3",
                            "15:32:onMode = 3",
                            "15:33:onMode = 3",
                            "15:34:onMode = 3",
                            "15:35:onMode = 3",
                            "15:36:onMode = 3",
                            "15:37:onMode = 3",
                            "15:addChannel = 2 1.0 0 13 22"  # tau+ decay
]

