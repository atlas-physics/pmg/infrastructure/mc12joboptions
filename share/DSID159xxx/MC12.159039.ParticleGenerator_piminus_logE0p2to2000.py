evgenConfig.description = "Single Pi- with log E in [0.2-2000] GeV"
evgenConfig.keywords = ["singleparticle", "piminus"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
  "PDGcode: constant -211",
  "e: log 200. 2000000.",
  "eta: flat -5.5 5.5",
  "phi: flat -3.14159 3.14159"
 ]
