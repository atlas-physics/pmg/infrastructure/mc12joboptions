# W' -> WZ jO: Aug. 2012 - G. Azuelos 
# wprime mass in GeV
m_wprime=2000.0

evgenConfig.description = "EGM W prime ("+str(m_wprime)+") to WZ leptonic"
evgenConfig.keywords = ["Exotics", "EGM", "SSM", "Wprime"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")


# turn on the W' process
topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on"]
# set mass and disable all decay modes except W' -> WZ -> lv ll
topAlg.Pythia8.Commands += ["34:m0 ="+str(m_wprime) ]
topAlg.Pythia8.Commands += ["34:onMode = off"]
topAlg.Pythia8.Commands += ["34:onIfAll = 23 24"]
topAlg.Pythia8.Commands += ["23:onMode = off",
                            "23:onIfAny = 11 13",
                            "24:onMode = off",
                            "24:onIfAny = 11 13 15",
                            "Init:showAllParticleData = on",
                            "Next:numberShowEvent = 5"]

# W' coupling to WZ
topAlg.Pythia8.Commands += ["Wprime:coup2WZ=1."]

