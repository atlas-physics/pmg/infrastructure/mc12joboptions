evgenConfig.description = "Single photons with flat ET in [500-1000] GeV"
evgenConfig.keywords = ["singleparticle", "gamma"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE 
topAlg.ParticleGenerator.orders = [
 "PDGcode: constant 22",
 "et: flat 500000 1000000",
 "eta: flat -2.5 2.5",
 "phi: flat -3.14159 3.14159"
 ]
