evgenConfig.description = "Single pi0s with flat ET in [80-500] GeV"
evgenConfig.keywords = ["singleparticle", "pi0"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: constant 111",
 "et: flat 80000 500000",
 "eta: flat -2.5 2.5",
 "phi: flat -3.14159 3.14159"
 ]
