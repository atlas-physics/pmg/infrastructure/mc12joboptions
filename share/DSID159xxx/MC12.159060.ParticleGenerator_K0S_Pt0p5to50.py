evgenConfig.description = "Single K^0_S with flat Pt in [0.5-50] GeV"
evgenConfig.keywords = ["singleparticle", "K0S"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: constant 310",
 "pt:  flat 500 50000",
 "eta: flat -2.7 2.7",
 "phi: flat -3.14159 3.14159"
 ]
