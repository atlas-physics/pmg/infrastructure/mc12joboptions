###############################################################
# Pythia8 H_v -> pi_v pi_v
# contact: Daniel Blackburn (danielrb@u.washington.edu)
#===============================================================
evgenConfig.description = "Hidden Valley Higgs to heavy fermions" 
evgenConfig.keywords = ["Hidden Valley", "long-lived neutral particles", "pi_v"]

# Specify MSTW2008LO PDF
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["ParticleDecays:limitTau0 = off"] # Allow long-lived particles to decay
 
topAlg.Pythia8.Commands += ["35:name = H_v"]       # Set H_v name
topAlg.Pythia8.Commands += ["36:name = pi_v"]      # Set pi_v name

topAlg.Pythia8.Commands += ["Higgs:useBSM = on"]   # Turn on BSM Higgses
topAlg.Pythia8.Commands += ["HiggsBSM:gg2H2 = on"] # Turn on gg->H_v production

topAlg.Pythia8.Commands += ["35:onMode = off"]     # Turn off all H_v decays
topAlg.Pythia8.Commands += ["35:onIfAll = 36 36"]  # Turn back on H_v->pi_vpi_v

topAlg.Pythia8.Commands += ["35:m0 = 100"]         # Set H_v mass

topAlg.Pythia8.Commands += ["36:m0 = 10"]          # Set pi_v mass
topAlg.Pythia8.Commands += ["36:tau0 = 450"]       # Set pi_v lifetime
 
