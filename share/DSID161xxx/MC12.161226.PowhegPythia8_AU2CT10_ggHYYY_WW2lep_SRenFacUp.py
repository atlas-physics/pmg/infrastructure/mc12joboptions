evgenConfig.description = "POWHEG+PYTHIA8 ggH H->WW->lvlv with AU2,CT10, Renomalization/Factorization scale up"
evgenConfig.keywords = ["SMhiggs", "ggF", "W","leptonic"]
evgenConfig.inputfilecheck = "ggH_SM_M125"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 24 24',
                            '24:onMode = off',#decay of W
                            '24:mMin = 2.0',
                            '24:onIfMatch = 11 12',
                            '24:onIfMatch = 13 14',
                            '24:onIfMatch = 15 16'
                            ]
