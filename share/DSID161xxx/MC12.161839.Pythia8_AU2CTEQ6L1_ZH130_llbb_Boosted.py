evgenConfig.description = "PYTHIA8 ZH Z->ll H->bb with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "ZH","leptonic","bottom"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 130',
                            '25:mWidth = 0.00491',
                            '25:doForceWidth = true', 
                            'HiggsSM:ffbar2HZ = on',
                            '25:onMode = off',
                            '25:onIfMatch = 5 5',
                            '23:onMode = off',
                            '23:onIfMatch = 11 11',
                            '23:onIfMatch = 13 13',
                            '23:onIfMatch = 15 15',
                            'PhaseSpace:pTHatMin = 100'
                            ]
