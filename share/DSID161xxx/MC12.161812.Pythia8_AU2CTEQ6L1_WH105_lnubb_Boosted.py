evgenConfig.description = "PYTHIA8 Boosted WH H->bb with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "WH","bottom"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 105',
                            '25:mWidth = 0.00265',
                            '25:doForceWidth = true',                     
                            '25:onMode = off',
                            '25:onIfMatch = 5 5',
                            'HiggsSM:ffbar2HW = on',
                            '24:onMode = off',
                            '24:onIfMatch = 11 12',
                            '24:onIfMatch = 13 14',
                            '24:onIfMatch = 15 16',
                            'PhaseSpace:pTHatMin = 100'
                            ]

