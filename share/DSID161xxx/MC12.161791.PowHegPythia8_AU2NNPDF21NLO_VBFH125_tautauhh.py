evgenConfig.description = "POWHEG+PYTHIA8 VBF H->tautau->hh with AU2,NNPDF21NLO"
evgenConfig.keywords = ["SMhiggs", "VBF", "tau","hadronic"]
evgenConfig.inputfilecheck = "VBFH_SM_M125"

include("MC12JobOptions/Pythia8_AU2_NNPDF21NLO_Common.py")
include("MC12JobOptions/Pythia8_Powheg.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 15 15'
                            ]

# ... Filter H->VV->Children
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 15
topAlg.XtoVVDecayFilter.StatusParent = 2
topAlg.XtoVVDecayFilter.PDGChild1 = [111,130,211,221,223,310,311,321,323]
topAlg.XtoVVDecayFilter.PDGChild2 = [111,130,211,221,223,310,311,321,323]
