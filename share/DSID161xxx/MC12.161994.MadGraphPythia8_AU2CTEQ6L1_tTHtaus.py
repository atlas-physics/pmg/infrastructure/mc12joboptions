evgenConfig.description = " Pythia8 + Madgraph , AUET2B_CTEQ6L1"
evgenConfig.keywords = ["ttH","tau" ]
evgenConfig.inputfilecheck = "tTHtaus"

evgenConfig.generators += ["Pythia8" , "MadGraph" ]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

topAlg.Pythia8.Commands += \
                         [
                             "15:onMode = on" 
#                             "15:offIfAny = 11 13" 
                         ] 

include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

