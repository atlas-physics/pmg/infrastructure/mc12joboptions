include('MC12JobOptions/AlpgenControl_ttbar.py')
evgenConfig.description = "Alpgen+Pythia ttbar dileptonic with P2012 tune with 4inc additional jets"
evgenConfig.contact  = ["alexander.grohsjean@desy.de"]
evgenConfig.keywords = ["top", "ttbar", "dileptonic"]
evgenConfig.inputconfcheck = "201024.CTEQ6L1_ttbar"
evgenConfig.minevents = 500
