include('MC12JobOptions/AlpgenControl_ttbar.py')
evgenConfig.description = "Alpgen+Pythia ttbar radHi single lepton with P2012 tune with ktfac=0.5 and 4inc additional jets"
evgenConfig.contact  = ["alexander.grohsjean@desy.de"]
evgenConfig.keywords = ["top", "ttbar", "singlelepton"]
evgenConfig.inputconfcheck = "201234.ktfac0.5_CTEQ6L1_ttbar"
evgenConfig.minevents = 500
