include('MC12JobOptions/AlpgenControl_ttbar.py')
evgenConfig.description = "Alpgen+Pythia ttbar allhad with P2012 tune with 2 additional jets"
evgenConfig.contact  = ["alexander.grohsjean@desy.de"]
evgenConfig.keywords = ["top", "ttbar", "allhad"]
evgenConfig.inputconfcheck = "201422.CTEQ6L1_ttbar"
evgenConfig.minevents = 2000
