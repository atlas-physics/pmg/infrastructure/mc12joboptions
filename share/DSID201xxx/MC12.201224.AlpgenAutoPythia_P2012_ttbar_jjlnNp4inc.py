include('MC12JobOptions/AlpgenControl_ttbar.py')
evgenConfig.description = "Alpgen+Pythia ttbar single lepton with P2012 tune with 4inc additional jets"
evgenConfig.contact  = ["alexander.grohsjean@desy.de"]
evgenConfig.keywords = ["top", "ttbar", "singlelepton"]
evgenConfig.inputconfcheck = "201224.CTEQ6L1_ttbar"
evgenConfig.minevents = 500
