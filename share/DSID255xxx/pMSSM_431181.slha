#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.4                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50017206E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.4  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.3.10       # version number                    
#
BLOCK MODSEL  # Model selection
     1     0   #  ewkh                                             
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27944000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16637000E-05   # G_F [GeV^-2]
         3     1.18500002E-01   # alpha_S(M_Z)^MSbar
         4     9.11876000E+01   # M_Z pole mass
         5     4.17999983E+00   # mb(mb)^MSbar
         6     1.73039734E+02   # mt pole mass
         7     1.77699000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         1     5.00000000E+03   # m0                  
         2    -3.22270923E+03   # m12                 
         3     3.61961670E+01   # tanb                
         5     1.00000000E+02   # A0                  
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00172063E+03   # MX                  
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03827062E+01   # W+
        25     1.24894276E+02   # h
        35     1.09451597E+03   # H
        36     1.09449231E+03   # A
        37     1.09796455E+03   # H+
         5     4.82046057E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.13083198E+03   # ~d_L
   2000001     5.12271065E+03   # ~d_R
   1000002     5.13034786E+03   # ~u_L
   2000002     5.12515001E+03   # ~u_R
   1000003     5.13083198E+03   # ~s_L
   2000003     5.12271065E+03   # ~s_R
   1000004     5.13034786E+03   # ~c_L
   2000004     5.12515001E+03   # ~c_R
   1000005     5.08856183E+03   # ~b_1
   2000005     5.10177017E+03   # ~b_2
   1000006     5.06399059E+03   # ~t_1
   2000006     5.09147409E+03   # ~t_2
   1000011     5.01145196E+03   # ~e_L
   2000011     5.00988759E+03   # ~e_R
   1000012     5.01047785E+03   # ~nu_eL
   1000013     5.01145196E+03   # ~mu_L
   2000013     5.00988759E+03   # ~mu_R
   1000014     5.01047785E+03   # ~nu_muL
   1000015     4.99646961E+03   # ~tau_1
   2000015     5.00502232E+03   # ~tau_2
   1000016     5.00385662E+03   # ~nu_tauL
   1000021     4.35902180E+03   # ~g
   1000022     1.19830748E+02   # ~chi_10
   1000023    -1.98208876E+02   # ~chi_20
   1000025     2.36711519E+02   # ~chi_30
   1000035    -3.19679619E+03   # ~chi_40
   1000024     1.22456562E+02   # ~chi_1+
   1000037     2.42771143E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -4.72133982E-03   # N_11
  1  2    -7.65502138E-01   # N_12
  1  3     5.40662745E-01   # N_13
  1  4    -3.48809378E-01   # N_14
  2  1     9.95366968E-03   # N_21
  2  2     1.56602716E-01   # N_22
  2  3     6.82407489E-01   # N_23
  2  4     7.13930342E-01   # N_24
  3  1    -7.77667031E-03   # N_31
  3  2     6.24084902E-01   # N_32
  3  3     4.91938658E-01   # N_33
  3  4    -6.07004049E-01   # N_34
  4  1     9.99909074E-01   # N_41
  4  2    -3.19693969E-04   # N_42
  4  3    -4.14199095E-04   # N_43
  4  4    -1.34747700E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.95344359E-01   # U_11
  1  2     7.18676716E-01   # U_12
  2  1    -7.18676716E-01   # U_21
  2  2    -6.95344359E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -8.80272281E-01   # V_11
  1  2     4.74468873E-01   # V_12
  2  1    -4.74468873E-01   # V_21
  2  2    -8.80272281E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.42087115E-01   # cos(theta_t)
  1  2     9.89854157E-01   # sin(theta_t)
  2  1    -9.89854157E-01   # -sin(theta_t)
  2  2     1.42087115E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.95658131E-01   # cos(theta_b)
  1  2     9.30853704E-02   # sin(theta_b)
  2  1    -9.30853704E-02   # -sin(theta_b)
  2  2     9.95658131E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     1.30837115E-01   # cos(theta_tau)
  1  2     9.91403878E-01   # sin(theta_tau)
  2  1    -9.91403878E-01   # -sin(theta_tau)
  2  2     1.30837115E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -2.91559232E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00172063E+03  # DRbar Higgs Parameters
         1     1.84126360E+02   # mu(Q)MSSM           
         2     3.50206491E+01   # tan                 
         3     2.39875337E+02   # higgs               
         4     1.20525116E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  5.00172063E+03  # The gauge couplings
     1     3.66333856E-01   # gprime(Q) DRbar
     2     6.43995250E-01   # g(Q) DRbar
     3     9.90168306E-01   # g3(Q) DRbar
#
BLOCK AU Q=  5.00172063E+03  # The trilinear couplings
  1  1     1.00000025E+02   # A_u(Q) DRbar
  2  2     1.00000025E+02   # A_c(Q) DRbar
  3  3     1.00000045E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00172063E+03  # The trilinear couplings
  1  1     1.00000011E+02   # A_d(Q) DRbar
  2  2     1.00000011E+02   # A_s(Q) DRbar
  3  3     1.00000022E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00172063E+03  # The trilinear couplings
  1  1     1.00000007E+02   # A_e(Q) DRbar
  2  2     1.00000007E+02   # A_mu(Q) DRbar
  3  3     1.00000007E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00172063E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.16905105E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00172063E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.64785976E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00172063E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     3.61970410E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00172063E+03  # The soft SUSY breaking masses at the scale Q
         1    -3.22270923E+03   # M_1(Q)              
         2     1.43764389E+02   # M_2(Q)              
         3     4.00000000E+03   # M_3(Q)              
        21     1.43868963E+06   # M^2_Hd              
        22     6.08426643E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.99999998E+03   # M_q1L               
        42     4.99999998E+03   # M_q2L               
        43     4.99999997E+03   # M_q3L               
        44     4.99999998E+03   # M_uR                
        45     4.99999998E+03   # M_cR                
        46     4.99999996E+03   # M_tR                
        47     4.99999998E+03   # M_dR                
        48     4.99999998E+03   # M_sR                
        49     4.99999998E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.45622192E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.95872502E-01   # gluino decays
#          BR         NDA      ID1       ID2
     1.27934027E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
     6.23144121E-04    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.82559844E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
     3.26782082E-08    2     1000035        21   # BR(~g -> ~chi_40 g)
#           BR         NDA      ID1       ID2       ID3
     1.35766190E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.99827389E-04    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     9.26871764E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     5.61356267E-05    3     1000035         1        -1   # BR(~g -> ~chi_40 d  db)
     1.36489947E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.25731067E-04    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     9.18784491E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.90278697E-04    3     1000035         2        -2   # BR(~g -> ~chi_40 u  ub)
     1.35766190E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.99827389E-04    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     9.26871764E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     5.61356267E-05    3     1000035         3        -3   # BR(~g -> ~chi_40 s  sb)
     1.36489947E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.25731067E-04    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     9.18784491E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.90278697E-04    3     1000035         4        -4   # BR(~g -> ~chi_40 c  cb)
     3.50061451E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.34169466E-02    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.68697063E-02    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     5.89787535E-05    3     1000035         5        -5   # BR(~g -> ~chi_40 b  bb)
     3.43054983E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.07398367E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     9.61888954E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     1.73118892E-04    3     1000035         6        -6   # BR(~g -> ~chi_40 t  tb)
     2.92197803E-02    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.92197803E-02    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.74845155E-02    3     1000037         1        -2   # BR(~g -> ~chi_2+ d  ub)
     1.74845155E-02    3    -1000037         2        -1   # BR(~g -> ~chi_2- u  db)
     2.92197803E-02    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.92197803E-02    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.74845155E-02    3     1000037         3        -4   # BR(~g -> ~chi_2+ s  cb)
     1.74845155E-02    3    -1000037         4        -3   # BR(~g -> ~chi_2- c  sb)
     6.62751925E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     6.62751925E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
     1.26086403E-01    3     1000037         5        -6   # BR(~g -> ~chi_2+ b  tb)
     1.26086403E-01    3    -1000037         6        -5   # BR(~g -> ~chi_2- t  bb)
#
#         PDG            Width
DECAY   1000006     1.61509097E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.79951749E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.23400586E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     9.10054250E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.32090775E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     2.00499156E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.01300966E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     1.62589615E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.67498074E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.01774845E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.84816448E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.45622104E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.40857167E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.44619994E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.21969767E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.29385630E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.70805622E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.14510369E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.79554204E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.72786962E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.56540286E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     2.15478898E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.95689930E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.47521283E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     7.06172434E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     6.45228229E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.46684050E-01    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.31137437E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.46334154E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.15606167E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.98539408E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     3.66900393E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     8.82783946E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.34027356E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.72301116E-03    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     8.82217032E-02    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.10869867E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.53617594E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.02448124E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     3.12853513E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.16924183E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     8.14208419E-06    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.61330754E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.20316574E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.38519996E-01    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.61413698E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     8.82780655E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.33415095E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.45419256E-03    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.90683426E-02    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.13078770E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.20669748E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.35071014E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     3.13190820E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.82477005E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     2.28264625E-06    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.01299480E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.17659251E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.87877946E-02    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.61193616E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     8.82783946E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.34027356E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.72301116E-03    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     8.82217032E-02    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.10869867E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.53617594E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.02448124E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     3.12853513E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.16924183E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     8.14208419E-06    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.61330754E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.20316574E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.38519996E-01    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.61413698E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     8.82780655E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.33415095E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.45419256E-03    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.90683426E-02    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.13078770E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.20669748E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.35071014E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     3.13190820E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.82477005E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     2.28264625E-06    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.01299480E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.17659251E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.87877946E-02    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.61193616E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.42064608E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.89796599E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     8.45160618E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.23088889E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.66021812E-02    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.11000729E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.31059996E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     9.40481090E+00   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.33323520E-05    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.80929905E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.71253040E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.99484485E-01    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.42064608E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.89796599E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     8.45160618E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.23088889E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.66021812E-02    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.11000729E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.31059996E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     9.40481090E+00   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.33323520E-05    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.80929905E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.71253040E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.99484485E-01    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.60121669E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.60009152E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.56095315E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.45871456E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.55048513E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.76151999E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     9.81078755E-02    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     7.63940189E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.83484967E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.19712428E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.59853280E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     3.22064658E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.18746198E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.13737847E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.42244216E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.87062846E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.30964572E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.26568640E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.66476130E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     4.98183675E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.44227581E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.42244216E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.87062846E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.30964572E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.26568640E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.66476130E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     4.98183675E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.44227581E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     7.71360495E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.55544529E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.07800765E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.05242095E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.03622241E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.01468590E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.01304554E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.45651067E-10   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.71131396E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.71131396E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.23711560E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.23711560E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03140875E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.47031914E-01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.68934099E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.31065901E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     5.66195812E-03   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.37089589E-05    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     2.50448913E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     3.24825242E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     2.50448913E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     3.24825242E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     3.12653449E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     7.44871841E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     7.44871841E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     7.40495817E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.48922067E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.48922067E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.48922093E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.31132519E-01    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.31132519E-01    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.31132519E-01    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.31132519E-01    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.37108312E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.37108312E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.37108312E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.37108312E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.36168475E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.36168475E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.79735583E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.37707886E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.93114606E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.93114606E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
#
#         PDG            Width
DECAY   1000035     1.44778462E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.78851411E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     6.38735878E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.11956424E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     3.30789538E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     3.30789538E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.63002173E-02    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     9.63002173E-02    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     1.98993005E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.94505597E-02    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.77574913E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     8.03536880E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     6.47099538E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     4.32518220E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.60739247E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.15696431E-02    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     3.47036375E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.91798472E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.91798472E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     5.60787858E-02    2     1000037       -37   # BR(~chi_40 -> ~chi_2+   H-)
     5.60787858E-02    2    -1000037        37   # BR(~chi_40 -> ~chi_2-   H+)
#
#         PDG            Width
DECAY        25     4.08275908E-03   # h decays
#          BR         NDA      ID1       ID2
     6.12999368E-01    2           5        -5   # BR(h -> b       bb     )
     6.60503913E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.33797166E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.59007993E-04    2           3        -3   # BR(h -> s       sb     )
     1.91997138E-02    2           4        -4   # BR(h -> c       cb     )
     6.87292694E-02    2          21        21   # BR(h -> g       g      )
     2.33621008E-03    2          22        22   # BR(h -> gam     gam    )
     1.51507781E-03    2          22        23   # BR(h -> Z       gam    )
     2.03181551E-01    2          24       -24   # BR(h -> W+      W-     )
     2.52956142E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.24480744E+01   # H decays
#          BR         NDA      ID1       ID2
     5.26479521E-01    2           5        -5   # BR(H -> b       bb     )
     8.57315875E-02    2         -15        15   # BR(H -> tau+    tau-   )
     3.03099843E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.94999011E-04    2           3        -3   # BR(H -> s       sb     )
     1.22744647E-08    2           4        -4   # BR(H -> c       cb     )
     1.22110737E-03    2           6        -6   # BR(H -> t       tb     )
     4.16421231E-05    2          21        21   # BR(H -> g       g      )
     1.81175141E-07    2          22        22   # BR(H -> gam     gam    )
     1.77796811E-08    2          23        22   # BR(H -> Z       gam    )
     4.76299202E-06    2          24       -24   # BR(H -> W+      W-     )
     2.35949356E-06    2          23        23   # BR(H -> Z       Z      )
     1.52280965E-05    2          25        25   # BR(H -> h       h      )
     2.31275684E-23    2          36        36   # BR(H -> A       A      )
     8.94867133E-20    2          23        36   # BR(H -> Z       A      )
     1.02720479E-01    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.99618906E-02    2     1000037  -1000037   # BR(H -> ~chi_2+ ~chi_2-)
     6.77642405E-02    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     6.77642405E-02    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     4.35006796E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.61958342E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.85971881E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.82547290E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     9.78828196E-05    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.45245682E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     3.24485473E+01   # A decays
#          BR         NDA      ID1       ID2
     5.26502979E-01    2           5        -5   # BR(A -> b       bb     )
     8.57324027E-02    2         -15        15   # BR(A -> tau+    tau-   )
     3.03099541E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.95021668E-04    2           3        -3   # BR(A -> s       sb     )
     1.18757763E-08    2           4        -4   # BR(A -> c       cb     )
     1.26509762E-03    2           6        -6   # BR(A -> t       tb     )
     6.78445341E-05    2          21        21   # BR(A -> g       g      )
     5.53521211E-08    2          22        22   # BR(A -> gam     gam    )
     3.15391171E-08    2          23        22   # BR(A -> Z       gam    )
     4.62787008E-06    2          23        25   # BR(A -> Z       h      )
     1.14845961E-01    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.10212726E-02    2     1000037  -1000037   # BR(A -> ~chi_2+ ~chi_2-)
     5.61492779E-02    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     5.61492779E-02    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     4.92252113E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.67208791E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.63823147E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.22204059E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.41394937E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.68216243E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.16191232E+01   # H+ decays
#          BR         NDA      ID1       ID2
     8.40788834E-04    2           4        -5   # BR(H+ -> c       bb     )
     8.82604321E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     3.12037161E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     5.38100424E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.93795335E-05    2           2        -3   # BR(H+ -> u       sb     )
     3.98601309E-04    2           4        -3   # BR(H+ -> c       sb     )
     5.18613872E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.81971286E-06    2          24        25   # BR(H+ -> W+      h      )
     2.09336197E-11    2          24        36   # BR(H+ -> W+      A      )
     5.79553565E-05    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     8.34059860E-02    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.12869848E-01    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.52645086E-01    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     4.20018858E-02    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     5.63927320E-04    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
