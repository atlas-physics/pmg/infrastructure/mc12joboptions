#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     3.00000000E+03   # M_1                 
         2     8.00000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     2.00000000E+01   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04968931E+01   # W+
        25     1.25000000E+02   # h
        35     2.00003346E+03   # H
        36     2.00000000E+03   # A
        37     2.00203766E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.00057738E+03   # ~d_L
   2000001     3.00010864E+03   # ~d_R
   1000002     2.99953116E+03   # ~u_L
   2000002     2.99978272E+03   # ~u_R
   1000003     3.00057738E+03   # ~s_L
   2000003     3.00010864E+03   # ~s_R
   1000004     2.99953116E+03   # ~c_L
   2000004     2.99978272E+03   # ~c_R
   1000005     2.99898100E+03   # ~b_1
   2000005     3.00170682E+03   # ~b_2
   1000006     3.00357598E+03   # ~t_1
   2000006     3.00404345E+03   # ~t_2
   1000011     3.00036014E+03   # ~e_L
   2000011     3.00032590E+03   # ~e_R
   1000012     2.99931385E+03   # ~nu_eL
   1000013     3.00036014E+03   # ~mu_L
   2000013     3.00032590E+03   # ~mu_R
   1000014     2.99931385E+03   # ~nu_muL
   1000015     2.99945326E+03   # ~tau_1
   2000015     3.00123356E+03   # ~tau_2
   1000016     2.99931385E+03   # ~nu_tauL
   1000021     3.00000000E+03   # ~g
   1000022     1.44417920E+02   # ~chi_10
   1000023    -1.53302794E+02   # ~chi_20
   1000025     8.08224371E+02   # ~chi_30
   1000035     3.00066050E+03   # ~chi_40
   1000024     1.47685936E+02   # ~chi_1+
   1000037     8.08273874E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     1.12868822E-02   # N_11
  1  2    -8.80856701E-02   # N_12
  1  3     7.16289394E-01   # N_13
  1  4    -6.92129341E-01   # N_14
  2  1     9.54126907E-03   # N_21
  2  2    -5.65463592E-02   # N_22
  2  3    -6.97396137E-01   # N_23
  2  4    -7.14387921E-01   # N_24
  3  1     2.08281421E-03   # N_31
  3  2     9.94506478E-01   # N_32
  3  3     2.37895111E-02   # N_33
  3  4    -1.01914602E-01   # N_34
  4  1     9.99888610E-01   # N_41
  4  2    -5.37695504E-04   # N_42
  4  3    -1.48034389E-03   # N_43
  4  4     1.48420722E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -3.37738558E-02   # U_11
  1  2     9.99429501E-01   # U_12
  2  1     9.99429501E-01   # U_21
  2  2     3.37738558E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.44989058E-01   # V_11
  1  2     9.89433259E-01   # V_12
  2  1     9.89433259E-01   # V_21
  2  2     1.44989058E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     8.76747059E-01   # cos(theta_t)
  1  2     4.80951759E-01   # sin(theta_t)
  2  1    -4.80951759E-01   # -sin(theta_t)
  2  2     8.76747059E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.43441923E-01   # cos(theta_b)
  1  2     7.65494933E-01   # sin(theta_b)
  2  1    -7.65494933E-01   # -sin(theta_b)
  2  2     6.43441923E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.00273191E-01   # cos(theta_tau)
  1  2     7.13874960E-01   # sin(theta_tau)
  2  1    -7.13874960E-01   # -sin(theta_tau)
  2  2     7.00273191E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.02674431E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.99999983E+01   # tanbeta(Q)          
         3     2.50693719E+02   # vev(Q)              
         4     3.93525317E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53671053E-01   # gprime(Q) DRbar
     2     6.33670091E-01   # g(Q) DRbar
     3     1.08193249E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.91835219E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.03370121E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01097612E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     3.00000000E+03   # M_1                 
         2     8.00000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.76164914E+06   # M^2_Hd              
        22    -1.58190810E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     1.65799826E-05   # gluino decays
#          BR         NDA      ID1       ID2
     2.05795835E-01    2     1000002        -2   # BR(~g -> ~u_L  ub)
     2.05795835E-01    2    -1000002         2   # BR(~g -> ~u_L* u )
     4.42041650E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     4.42041650E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     2.05795835E-01    2     1000004        -4   # BR(~g -> ~c_L  cb)
     2.05795835E-01    2    -1000004         4   # BR(~g -> ~c_L* c )
     4.42041650E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     4.42041650E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
#
#         PDG            Width
DECAY   1000006     8.69121593E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.73339045E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.87662141E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.55845175E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.22771739E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.40642557E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   2000006     9.06338205E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     2.35384565E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.53551383E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.02162378E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.63156503E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.76913109E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   1000005     4.08455679E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     8.27894610E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.70679638E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     9.92876034E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     5.50349018E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.10505954E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
#
#         PDG            Width
DECAY   2000005     5.33619367E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.35880726E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.67006971E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.15031161E-01    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.41546074E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.43133995E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
#
#         PDG            Width
DECAY   1000002     3.09989863E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     2.84426634E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.15336344E-03    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.29016005E-01    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.61705466E-02    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.50815819E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
#
#         PDG            Width
DECAY   2000002     1.46705782E-03   # sup_R decays
#          BR         NDA      ID1       ID2
     5.73524159E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.09600107E-01    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.68757337E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
#
#         PDG            Width
DECAY   1000001     3.09465254E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.13529743E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.31040616E-03    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.29211213E-01    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     8.79233153E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.65462958E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.91625269E-07    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67782102E-04   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.71999943E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.08511594E-01    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.68314396E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.65702358E-03    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.09989863E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.84426634E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.15336344E-03    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.29016005E-01    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.61705466E-02    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.50815819E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
#
#         PDG            Width
DECAY   2000004     1.46705782E-03   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.73524159E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.09600107E-01    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.68757337E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
#
#         PDG            Width
DECAY   1000003     3.09465254E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.13529743E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.31040616E-03    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.29211213E-01    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     8.79233153E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.65462958E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.91625269E-07    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67782102E-04   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.71999943E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.08511594E-01    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.68314396E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.65702358E-03    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.09488479E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     2.57810053E-03    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.01060710E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.30182421E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     8.79102904E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.65349768E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     3.30148670E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.73523576E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.09599779E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.68766446E-02    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.09488479E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     2.57810053E-03    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.01060710E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.30182421E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     8.79102904E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.65349768E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     3.30148670E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.73523576E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.09599779E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.68766446E-02    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.88536177E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     8.66749862E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.05404451E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.59955391E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     7.90911531E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     5.23738025E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.93040144E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.72854199E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.33347916E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.75602025E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     4.84530694E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     5.55324694E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.09922348E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.42757964E-03    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.47199615E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.28031874E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.61728861E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     6.50895664E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.09922348E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.42757964E-03    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.47199615E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.28031874E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.61728861E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     6.50895664E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.33932271E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.18113468E-03    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.36615877E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.04446193E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     8.68357215E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     6.04170792E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.87995799E-10   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.64604242E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.64604242E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.21534824E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.21534824E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.77218666E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.35947096E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.54700488E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.44515918E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.57726396E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.43057198E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
#DECAY   1000022     0.00000000E+00   # neutralino1 decays
##
##         PDG            Width
DECAY   1000023     5.40290925E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.58021813E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.06901496E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.38232845E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.06901496E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.38232845E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     3.14897423E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.14897423E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.43860401E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.28238606E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.28238606E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.28238606E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.79519466E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.79519466E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.79519466E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.79519466E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.26506608E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.26506608E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.26506608E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.26506608E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.84079996E-03    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.84079996E-03    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     6.36112811E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.88202160E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.84205016E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.57095085E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.57095085E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.65732635E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     6.70519622E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
#
#         PDG            Width
DECAY   1000035     9.67272515E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.41706388E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.18150565E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.53471722E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.83348448E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.83348448E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.23899051E-03    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     6.23899051E-03    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     1.10912137E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.21968083E-02    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.75746136E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     7.92281760E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.59724685E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.06355583E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.82676163E-03    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.90079191E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     6.02478875E-06    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.77573802E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.77573802E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     3.50370885E-05    2     1000037       -37   # BR(~chi_40 -> ~chi_2+   H-)
     3.50370885E-05    2    -1000037        37   # BR(~chi_40 -> ~chi_2-   H+)
     3.62173887E-08    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     3.62173887E-08    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.52135449E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.52135449E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     1.98541596E-10    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     1.98541596E-10    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     3.48003333E-08    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     3.48003333E-08    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     3.62173887E-08    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     3.62173887E-08    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.52135449E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.52135449E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     1.98541596E-10    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     1.98541596E-10    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     3.48003333E-08    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     3.48003333E-08    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     7.71738461E-09    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     7.71738461E-09    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.83822248E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.83822248E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     7.71738461E-09    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     7.71738461E-09    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.83822248E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.83822248E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.55674590E-07    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.55674590E-07    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.55674590E-07    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.55674590E-07    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.55674590E-07    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.55674590E-07    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
