#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     3.00000000E+03   # M_1                 
         2     8.00000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05004102E+01   # W+
        25     1.25000000E+02   # h
        35     2.00054285E+03   # H
        36     2.00000000E+03   # A
        37     2.00187187E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.00053595E+03   # ~d_L
   2000001     3.00010082E+03   # ~d_R
   1000002     2.99956480E+03   # ~u_L
   2000002     2.99979835E+03   # ~u_R
   1000003     3.00053595E+03   # ~s_L
   2000003     3.00010082E+03   # ~s_R
   1000004     2.99956480E+03   # ~c_L
   2000004     2.99979835E+03   # ~c_R
   1000005     2.99991800E+03   # ~b_1
   2000005     3.00072116E+03   # ~b_2
   1000006     3.00303899E+03   # ~t_1
   2000006     3.00463347E+03   # ~t_2
   1000011     3.00033433E+03   # ~e_L
   2000011     3.00030245E+03   # ~e_R
   1000012     2.99936312E+03   # ~nu_eL
   1000013     3.00033433E+03   # ~mu_L
   2000013     3.00030245E+03   # ~mu_R
   1000014     2.99936312E+03   # ~nu_muL
   1000015     3.00009615E+03   # ~tau_1
   2000015     3.00054167E+03   # ~tau_2
   1000016     2.99936312E+03   # ~nu_tauL
   1000021     3.00000000E+03   # ~g
   1000022     1.42948545E+02   # ~chi_10
   1000023    -1.52269289E+02   # ~chi_20
   1000025     8.08650627E+02   # ~chi_30
   1000035     3.00067012E+03   # ~chi_40
   1000024     1.45384858E+02   # ~chi_1+
   1000037     8.08695505E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     1.27093350E-02   # N_11
  1  2    -9.90265312E-02   # N_12
  1  3     7.14772096E-01   # N_13
  1  4    -6.92194387E-01   # N_14
  2  1     7.92468357E-03   # N_21
  2  2    -4.70062408E-02   # N_22
  2  3    -6.98304775E-01   # N_23
  2  4    -7.14211491E-01   # N_24
  3  1     2.18978174E-03   # N_31
  3  2     9.93973773E-01   # N_32
  3  3     3.81848466E-02   # N_33
  3  4    -1.02729064E-01   # N_34
  4  1     9.99885432E-01   # N_41
  4  2    -5.45577179E-04   # N_42
  4  3    -3.63446652E-03   # N_43
  4  4     1.46838669E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.40501833E-02   # U_11
  1  2     9.98538220E-01   # U_12
  2  1     9.98538220E-01   # U_21
  2  2     5.40501833E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.46070173E-01   # V_11
  1  2     9.89274231E-01   # V_12
  2  1     9.89274231E-01   # V_21
  2  2     1.46070173E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.57058439E-01   # cos(theta_t)
  1  2     6.53347166E-01   # sin(theta_t)
  2  1    -6.53347166E-01   # -sin(theta_t)
  2  2     7.57058439E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.78661718E-01   # cos(theta_b)
  1  2     8.77999408E-01   # sin(theta_b)
  2  1    -8.77999408E-01   # -sin(theta_b)
  2  2     4.78661718E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.81334765E-01   # cos(theta_tau)
  1  2     7.31971952E-01   # sin(theta_tau)
  2  1    -7.31971952E-01   # -sin(theta_tau)
  2  2     6.81334765E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.98523410E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     4.99999949E+00   # tanbeta(Q)          
         3     2.50744278E+02   # vev(Q)              
         4     4.04121978E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53665465E-01   # gprime(Q) DRbar
     2     6.33727731E-01   # g(Q) DRbar
     3     1.08193116E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.08399360E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.76664693E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.11244303E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     3.00000000E+03   # M_1                 
         2     8.00000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.84437749E+06   # M^2_Hd              
        22    -1.49966966E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     1.42851745E-05   # gluino decays
#          BR         NDA      ID1       ID2
     2.05810422E-01    2     1000002        -2   # BR(~g -> ~u_L  ub)
     2.05810422E-01    2    -1000002         2   # BR(~g -> ~u_L* u )
     4.41895780E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     4.41895780E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     2.05810422E-01    2     1000004        -4   # BR(~g -> ~c_L  cb)
     2.05810422E-01    2    -1000004         4   # BR(~g -> ~c_L* c )
     4.41895780E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     4.41895780E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
#
#         PDG            Width
DECAY   1000006     8.87563413E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.81954310E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.91668795E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.75526218E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.91679149E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     8.71451240E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   2000006     8.89768780E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     2.44858717E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.68128734E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.79991604E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.58637032E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.50376357E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   1000005     1.88791228E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.81779208E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.29191337E-03    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.20480566E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     5.99223354E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.55826246E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
#
#         PDG            Width
DECAY   2000005     6.18060074E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.36053770E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.18525041E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.27868488E-01    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.94517670E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.71068054E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
#
#         PDG            Width
DECAY   1000002     3.10029062E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     3.59501461E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.97177094E-04    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.28645666E-01    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.64161608E-02    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.50545981E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
#
#         PDG            Width
DECAY   2000002     1.50881745E-03   # sup_R decays
#          BR         NDA      ID1       ID2
     7.07113825E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.74752271E-01    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.81339036E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
#
#         PDG            Width
DECAY   1000001     3.09540974E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.96286671E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.05480853E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.28764475E-01    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.25200331E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.64114406E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     7.68090017E-07    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.78084557E-04   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.05539420E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.74140563E-01    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.80940833E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.22593321E-03    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.10029062E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.59501461E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.97177094E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.28645666E-01    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.64161608E-02    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.50545981E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
#
#         PDG            Width
DECAY   2000004     1.50881745E-03   # scharm_R decays
#          BR         NDA      ID1       ID2
     7.07113825E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.74752271E-01    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.81339036E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
#
#         PDG            Width
DECAY   1000003     3.09540974E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.96286671E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.05480853E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.28764475E-01    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.25200331E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.64114406E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     7.68090017E-07    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.78084557E-04   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.05539420E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.74140563E-01    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.80940833E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.22593321E-03    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.09565320E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.25761343E-03    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.98505203E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.29788633E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.25167347E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.64003575E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     3.39541829E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.07113129E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.74752058E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.81348130E-02    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.09565320E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.25761343E-03    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.98505203E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.29788633E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.25167347E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.64003575E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     3.39541829E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.07113129E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.74752058E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.81348130E-02    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.46331329E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.57453519E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.56195406E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.20815525E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.49599979E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     6.45917171E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.67924730E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.87601117E-03    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     8.12484393E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.28424667E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     3.33563498E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     6.61240915E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.09965094E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     4.33346192E-03    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.01716824E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.27608167E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.64184343E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     6.50622768E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.09965094E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     4.33346192E-03    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.01716824E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.27608167E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.64184343E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     6.50622768E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.11516888E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     4.31187517E-03    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.01210131E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.25976217E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.13040432E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     6.47395764E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.85184910E-11   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.72758289E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.72758289E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.24252835E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.24252835E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     5.97775116E-03    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.97062281E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.55097923E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.45708048E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.56636951E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.42557079E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
#DECAY   1000022     0.00000000E+00   # neutralino1 decays
##
##         PDG            Width
DECAY   1000023     8.56526920E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.26848693E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     8.52429920E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.10224601E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     8.52429920E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.10224601E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     2.51085812E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.51085812E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.99689305E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     5.00922537E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     5.00922537E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     5.00922537E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.52020947E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.52020947E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.52020947E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.52020947E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.17340505E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.17340505E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.17340505E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.17340505E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.57944465E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.57944465E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     6.97206188E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.22707489E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.11174449E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.56189676E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.56189676E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.91819452E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.23559978E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
#
#         PDG            Width
DECAY   1000035     9.76865054E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.95561540E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.46647768E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.30597295E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.86743766E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.86743766E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.38075498E-03    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     6.38075498E-03    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     1.39372864E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.18611847E-02    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.39912002E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     5.39708885E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.26862943E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.06171776E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     5.20588729E-03    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.42935058E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     2.63527453E-05    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.41906274E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.41906274E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     5.04557068E-05    2     1000037       -37   # BR(~chi_40 -> ~chi_2+   H-)
     5.04557068E-05    2    -1000037        37   # BR(~chi_40 -> ~chi_2-   H+)
     3.43483955E-08    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     3.43483955E-08    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.43905793E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.43905793E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.12244451E-10    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.12244451E-10    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     3.66692317E-08    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     3.66692317E-08    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     3.43483955E-08    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     3.43483955E-08    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.43905793E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.43905793E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.12244451E-10    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.12244451E-10    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     3.66692317E-08    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     3.66692317E-08    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     9.54993393E-09    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     9.54993393E-09    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.58866415E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.58866415E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     9.54993393E-09    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     9.54993393E-09    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.58866415E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.58866415E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.45202442E-07    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.45202442E-07    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.45202442E-07    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.45202442E-07    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.45202442E-07    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.45202442E-07    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
