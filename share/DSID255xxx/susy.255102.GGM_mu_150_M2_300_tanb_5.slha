#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     3.00000000E+03   # M_1                 
         2     3.00000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04956251E+01   # W+
        25     1.25000000E+02   # h
        35     2.00054643E+03   # H
        36     2.00000000E+03   # A
        37     2.00186036E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.00053591E+03   # ~d_L
   2000001     3.00010017E+03   # ~d_R
   1000002     2.99956419E+03   # ~u_L
   2000002     2.99979964E+03   # ~u_R
   1000003     3.00053591E+03   # ~s_L
   2000003     3.00010017E+03   # ~s_R
   1000004     2.99956419E+03   # ~c_L
   2000004     2.99979964E+03   # ~c_R
   1000005     2.99991754E+03   # ~b_1
   2000005     3.00072093E+03   # ~b_2
   1000006     3.00303474E+03   # ~t_1
   2000006     3.00462857E+03   # ~t_2
   1000011     3.00033559E+03   # ~e_L
   2000011     3.00030051E+03   # ~e_R
   1000012     2.99936380E+03   # ~nu_eL
   1000013     3.00033559E+03   # ~mu_L
   2000013     3.00030051E+03   # ~mu_R
   1000014     2.99936380E+03   # ~nu_muL
   1000015     3.00009573E+03   # ~tau_1
   2000015     3.00054141E+03   # ~tau_2
   1000016     2.99936380E+03   # ~nu_tauL
   1000021     3.00000000E+03   # ~g
   1000022     1.25482654E+02   # ~chi_10
   1000023    -1.54617796E+02   # ~chi_20
   1000025     3.28469445E+02   # ~chi_30
   1000035     3.00066570E+03   # ~chi_40
   1000024     1.29289101E+02   # ~chi_1+
   1000037     3.29267204E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     1.15276773E-02   # N_11
  1  2    -3.41431378E-01   # N_12
  1  3     7.02758393E-01   # N_13
  1  4    -6.24037153E-01   # N_14
  2  1     7.99420732E-03   # N_21
  2  2    -9.97495647E-02   # N_22
  2  3    -6.87381576E-01   # N_23
  2  4    -7.19369645E-01   # N_24
  3  1     5.53871054E-03   # N_31
  3  2     9.34598546E-01   # N_32
  3  3     1.83368730E-01   # N_33
  3  4    -3.04747091E-01   # N_34
  4  1     9.99886258E-01   # N_41
  4  2    -4.43191774E-04   # N_42
  4  3    -3.62213947E-03   # N_43
  4  4     1.46340595E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.61488753E-01   # U_11
  1  2     9.65206523E-01   # U_12
  2  1     9.65206523E-01   # U_21
  2  2     2.61488753E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.42196180E-01   # V_11
  1  2     8.96918356E-01   # V_12
  2  1     8.96918356E-01   # V_21
  2  2     4.42196180E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.57471490E-01   # cos(theta_t)
  1  2     6.52868242E-01   # sin(theta_t)
  2  1    -6.52868242E-01   # -sin(theta_t)
  2  2     7.57471490E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.78349360E-01   # cos(theta_b)
  1  2     8.78169625E-01   # sin(theta_b)
  2  1    -8.78169625E-01   # -sin(theta_b)
  2  2     4.78349360E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.78708131E-01   # cos(theta_tau)
  1  2     7.34408111E-01   # sin(theta_tau)
  2  1    -7.34408111E-01   # -sin(theta_tau)
  2  2     6.78708131E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.98539204E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     4.99999949E+00   # tanbeta(Q)          
         3     2.49941609E+02   # vev(Q)              
         4     4.05023215E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53660774E-01   # gprime(Q) DRbar
     2     6.35948839E-01   # g(Q) DRbar
     3     1.08193441E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.10775870E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.79029487E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.12794387E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     3.00000000E+03   # M_1                 
         2     3.00000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.82384618E+06   # M^2_Hd              
        22    -1.52448514E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     1.42860465E-05   # gluino decays
#          BR         NDA      ID1       ID2
     2.06376377E-01    2     1000002        -2   # BR(~g -> ~u_L  ub)
     2.06376377E-01    2    -1000002         2   # BR(~g -> ~u_L* u )
     4.36236232E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     4.36236232E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     2.06376377E-01    2     1000004        -4   # BR(~g -> ~c_L  cb)
     2.06376377E-01    2    -1000004         4   # BR(~g -> ~c_L* c )
     4.36236232E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     4.36236232E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
#
#         PDG            Width
DECAY   1000006     9.10832364E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.74125231E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.00365774E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.09281767E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     3.66115216E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.84656030E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   2000006     9.18745256E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     1.58554273E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.55398193E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.70138957E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.24322427E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.91586149E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   1000005     1.99325812E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.67108150E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.67904636E-03    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.03776227E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     5.13470114E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.32363797E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
#
#         PDG            Width
DECAY   2000005     6.54846223E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.29086225E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.96021702E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.25609675E-01    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.74421823E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.81099663E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
#
#         PDG            Width
DECAY   1000002     3.54634577E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     3.90349612E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.26837508E-03    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.90768149E-01    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.32576443E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.34352072E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
#
#         PDG            Width
DECAY   2000002     1.49910177E-03   # sup_R decays
#          BR         NDA      ID1       ID2
     5.86104416E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.81353775E-01    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.32541809E-01    2     1000025         2   # BR(~u_R -> ~chi_30 u)
#
#         PDG            Width
DECAY   1000001     3.54103208E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     4.00976741E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.47484183E-03    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.90026133E-01    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.64446128E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.19956066E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     6.71333309E-07    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.75644276E-04   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.84807798E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.80731449E-01    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.32249141E-01    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.21161258E-03    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.54634577E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.90349612E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.26837508E-03    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.90768149E-01    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.32576443E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.34352072E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
#
#         PDG            Width
DECAY   2000004     1.49910177E-03   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.86104416E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.81353775E-01    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.32541809E-01    2     1000025         4   # BR(~c_R -> ~chi_30 c)
#
#         PDG            Width
DECAY   1000003     3.54103208E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     4.00976741E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.47484183E-03    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.90026133E-01    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.64446128E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.19956066E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     6.71333309E-07    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.75644276E-04   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.84807798E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.80731449E-01    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.32249141E-01    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.21161258E-03    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.54146228E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.81201219E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.07923593E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.92527438E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.64358477E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.19837357E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     3.37354978E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.86103777E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.81353640E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.32542583E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.54146228E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.81201219E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.07923593E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.92527438E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.64358477E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.19837357E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     3.37354978E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.86103777E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.81353640E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.32542583E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.65720076E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     6.24142609E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.61457643E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.74463682E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     7.98908840E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     5.81616597E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.93144931E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.44409777E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.19486209E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.01440773E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     2.41355302E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     6.38034098E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.54556965E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     4.10327187E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.67512667E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.88264023E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.32596539E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.34431593E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.54556965E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     4.10327187E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.67512667E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.88264023E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.32596539E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.34431593E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.56118205E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     4.08528292E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.65901473E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.87000259E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.36102316E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.32385582E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.04203690E-09   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.59453891E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.59453891E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.19818058E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.19818058E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     4.14561011E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.05338449E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.82108166E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.94217319E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.63349804E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.60324711E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     1.00000000E-10   # neutralino1 decays
##
##         PDG            Width
DECAY   1000023     3.41359782E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.31298956E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     4.92419240E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     6.36270459E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     4.92419240E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     6.36270459E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.94266759E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     1.44788705E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.44788705E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.41451752E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     2.88728999E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     2.88728999E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     2.88728999E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     8.89031857E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     8.89031857E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     8.89031857E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     8.89031857E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.96344904E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.96344904E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.96344904E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.96344904E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.89165843E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.89165843E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.09219987E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.48097571E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.95858605E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     3.29454749E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.29454749E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.24551448E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     5.87069159E-03    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
#
#         PDG            Width
DECAY   1000035     9.76946955E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.78106279E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.47195993E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.21774882E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.50404207E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.50404207E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.29384998E-02    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     4.29384998E-02    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     1.13454597E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.16010793E-02    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.25841324E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     5.49627927E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.17975362E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.98176286E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     3.00869814E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.14904642E-03    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     2.19055328E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.08583545E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.08583545E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     3.38334959E-03    2     1000037       -37   # BR(~chi_40 -> ~chi_2+   H-)
     3.38334959E-03    2    -1000037        37   # BR(~chi_40 -> ~chi_2-   H+)
     3.41454906E-08    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     3.41454906E-08    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.39379703E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.39379703E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     4.78777090E-10    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     4.78777090E-10    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     3.61813453E-08    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     3.61813453E-08    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     3.41454906E-08    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     3.41454906E-08    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.39379703E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.39379703E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     4.78777090E-10    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     4.78777090E-10    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     3.61813453E-08    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     3.61813453E-08    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     9.23195214E-09    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     9.23195214E-09    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.52648849E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.52648849E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     9.23195214E-09    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     9.23195214E-09    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.52648849E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.52648849E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.44004266E-07    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.44004266E-07    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.44004266E-07    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.44004266E-07    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.44004266E-07    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.44004266E-07    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
