include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune='AU2_CT10'
process="ZZ_eemumu"

evgenConfig.description = "POWHEG+Pythia8 ZZ -> 2e2mu production mll>4GeV with dilepton filter pt>5GeV using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["EW"  ,"diboson", "leptonic"]
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch>"]

# compensate filter efficiency
evt_multiplier = 1.5

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.NLeptons = 2
topAlg.MultiLeptonFilter.Etacut = 10
topAlg.MultiLeptonFilter.Ptcut = 5.*GeV
