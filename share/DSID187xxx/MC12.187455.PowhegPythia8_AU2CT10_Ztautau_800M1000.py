include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
process="Z_tautau"

def powheg_override():
    # define mass window
    PowhegConfig.mass_low = 800.0
    PowhegConfig.mass_high = 1000.0 

evgenConfig.description = "POWHEG+Pythia8 Z->tautau production without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "tau"]

include("MC12JobOptions/PowhegControl_postInclude.py")
