## Herwig++ config for the CTEQ6L1 UE-EE-4 tune series with NLO events read from an LHEF file
include("MC12JobOptions/Herwigpp_UEEE4_MRSTMCal_CT10ME_Common.py")

from Herwigpp_i import config as hw
topAlg.Herwigpp.Commands += hw.lhef_cmds(filename="events.lhe", nlo=True).splitlines()

evgenConfig.generators += ["Powheg","Herwigpp"]
evgenConfig.description = "POWHEG+Herwigpp ttbar production with a 1 MeV lepton filter and Herwigpp UEEE4 tune"
evgenConfig.keywords = ["top", "ttbar", "leptonic"]
evgenConfig.contact  = ["Thorsten.Kuhl@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar'

include("MC12JobOptions/Herwigpp_EvtGen.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")
