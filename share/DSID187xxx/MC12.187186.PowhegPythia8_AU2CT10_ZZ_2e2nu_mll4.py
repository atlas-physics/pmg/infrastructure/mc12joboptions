include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune='AU2_CT10'
process="ZZ_eenunu"

evgenConfig.description = "POWHEG+Pythia8 ZZ -> 2e2nu production using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["EW"  ,"diboson", "leptonic"]
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch>"]

include("MC12JobOptions/PowhegControl_postInclude.py")
