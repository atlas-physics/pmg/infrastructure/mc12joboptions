include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
process="WpWm_ee"

evgenConfig.description = "POWHEG+Pythia8 WpWm_ee production without filter using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak"  ,"WW", "leptons" ]
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch>"]

include("MC12JobOptions/PowhegControl_postInclude.py")
