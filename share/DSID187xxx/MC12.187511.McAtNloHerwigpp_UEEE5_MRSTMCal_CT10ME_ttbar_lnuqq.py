evgenConfig.generators = ["McAtNlo", "Herwigpp"]
evgenConfig.keywords = ["top", "ttbar", "semileptonic" ]
evgenConfig.description = "ttbar single lepton with MC@NLO+Herwig++ - CT10 PDF for hard scatter and LO** with UEEE5 tune for shower+MPI" 
evgenConfig.inputfilecheck = 'ttbar' 
evgenConfig.contact = ["Orel Gueta","liza.mijovic@cern.ch"] 

include("MC12JobOptions/Herwigpp_UEEE5_MRSTMCal_CT10ME_LHEF_Common.py") 
include("MC12JobOptions/TTbarWToLeptonFilter.py") 
topAlg.TTbarWToLeptonFilter.NumLeptons = 1 
