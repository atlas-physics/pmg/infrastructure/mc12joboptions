evgenConfig.generators = ["McAtNlo", "Herwigpp"]
evgenConfig.keywords = ["top", "ttbar", "dileptonic" ]
evgenConfig.description = "ttbar dileptons with MC@NLO+Herwig++ - CT10 PDF for hard scatter and LO** with UEEE5 tune for shower+MPI"
evgenConfig.inputfilecheck = 'ttbar'
evgenConfig.contact = ["Orel Gueta","liza.mijovic@cern.ch"]
evgenConfig.minevents = 1000

include("MC12JobOptions/Herwigpp_UEEE5_MRSTMCal_CT10ME_LHEF_Common.py")
include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons = 2

