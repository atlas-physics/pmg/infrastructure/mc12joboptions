## Job options file for Herwig++, QCD jet slice production
## Andy Buckley, April '10
## Lily Asquith, June '12
## Orel Gueta, March '14

include("MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_Common.py")

evgenConfig.description = "QCD dijet production JZ5W with CTEQ6L1 PDF and EE4 tune"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact = ["Orel Gueta"]

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 500*GeV
"""

topAlg.Herwigpp.Commands += cmds.splitlines()

include("MC12JobOptions/JetFilter_JZ5W.py")
evgenConfig.minevents = 200
