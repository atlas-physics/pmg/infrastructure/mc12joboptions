
evgenConfig.description = "POWHEG+Pythia8 ttbar production with a 1 MeV lepton filter and AUET2B CT10 tune"
evgenConfig.keywords = ["top" ,"ttbar", "leptonic", "powheg", "pythia8" ]
evgenConfig.inputfilecheck = 'ttbar' 

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")

#Don't rur Tauola with Py8!
#include("MC12JobOptions/Pythia8_Tauola.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")
#topAlg.TTbarWToLeptonFilter.NumLeptons=-1 #require at least one lepton (not yet released)

