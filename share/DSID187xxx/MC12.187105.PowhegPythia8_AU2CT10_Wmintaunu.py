include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
process="Wm_taunu"

evgenConfig.description = "POWHEG+Pythia8 Wmin->taunu production without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau"]

include("MC12JobOptions/PowhegControl_postInclude.py")
