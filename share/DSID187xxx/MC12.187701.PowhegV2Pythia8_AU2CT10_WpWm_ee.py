#--------------------------------------------------------------
# Powheg WW setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_WW_Common.py')
PowhegConfig.decay_mode = 'e+e-'
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with main31 and AU2 CT10 tune
#--------------------------------------------------------------
include('MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py')
include('MC12JobOptions/Pythia8_Photos.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 WpWm_ee production without filter using CT10 pdf and AU2 CT10 tune'
evgenConfig.keywords    = [ 'electroweak', 'WW', 'leptons' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'oldrich.kepka@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
evgenConfig.minevents   = 5000
evgenConfig.inputconfcheck = "WpWm_ee" 