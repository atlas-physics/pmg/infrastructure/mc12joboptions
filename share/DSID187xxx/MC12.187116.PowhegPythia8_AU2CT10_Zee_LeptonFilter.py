include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
process="Z_ee"

evgenConfig.description = "POWHEG+Pythia8 Z->ee production with central lepton pt>15 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "e", "ptlepton>15 GeV", "|etalepton|<2.7"]

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 15000.
topAlg.LeptonFilter.Etacut = 2.7
