## POWHEG+Pythia8 Z->mumu with central di-lepton and hard photon filters

evgenConfig.description = "POWHEG+Pythia8 Z->mumu production with di-lepton (pT > 8 GeV, |eta|<2.6) and QED FSR photon (pT > 10 GeV, |eta|<2.5) filters"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "mu", "dilepton filter", "photon filter"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Zmumu"

#include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")

## Config for Py8 tune AU2 with CT10
include("MC12JobOptions/Pythia8_Base_Fragment.py")

topAlg.Pythia8.Commands += [
    "Tune:pp = 5",
    "PDF:useLHAPDF = on",
    "PDF:pSet=LHAPDF5:CT10.LHgrid",
    "MultipartonInteractions:bProfile = 4",
    "MultipartonInteractions:a1 = 0.10",
    "MultipartonInteractions:pT0Ref = 1.70",
    "MultipartonInteractions:ecmPow = 0.16",
    "BeamRemnants:reconnectRange = 4.67",
    "SpaceShower:rapidityOrder=0"]
evgenConfig.tune = "AU2 CT10"

include("MC12JobOptions/Pythia8_Powheg.py")
include("MC12JobOptions/Pythia8_Photos.py")
topAlg.Photospp.ZMECorrection = True

include("MC12JobOptions/DirectPhotonFilter.py")
topAlg.DirectPhotonFilter.Ptcut = 10000.
topAlg.DirectPhotonFilter.Etacut = 2.5

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 8000.
topAlg.MultiLeptonFilter.Etacut = 2.6
topAlg.MultiLeptonFilter.NLeptons = 2

evgenConfig.minevents = 200
