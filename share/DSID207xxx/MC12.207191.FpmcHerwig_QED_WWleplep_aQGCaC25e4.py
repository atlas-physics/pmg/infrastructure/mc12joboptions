evgenConfig.description = "aQGC aCW=2.5e-4 with 500GeV cutoff in exclusive gamgam->WW->lnulnu no hadronic tau"          
evgenConfig.keywords = ["QED" ,"WW", "exclusive", "AQGC" ]                                                                                    

evgenConfig.contact = ["Chav Chhiv Chau <chav.chhiv.chau@cern.ch>"]
evgenConfig.generators += ["FPMC"]
evgenConfig.inputfilecheck = 'FpmcHerwig_aQGCac25e4'
include("MC12JobOptions/HepMCReadFromFile_Common.py")
evgenConfig.tune = "none"
