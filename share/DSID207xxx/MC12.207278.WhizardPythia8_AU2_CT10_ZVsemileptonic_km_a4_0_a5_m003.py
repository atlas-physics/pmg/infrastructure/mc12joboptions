############################################################################################################
# Electroweak production of Z(jj)W(lnu)jj + Z(jj)Z(ll)jj
# Whizard LHEF showered with Pythia8
# with anomalous quartic gauge couplings
# Brian Lindquist <blindqui@cern.ch>
############################################################################################################ 
evgenConfig.description = "Whizard+Pythia8 production JO with aQGC and the AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "diboson","VBS"]
evgenConfig.inputfilecheck = "whizard211.207278"
include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

evgenConfig.minevents = 5000
evgenConfig.contact = ["Brian Lindquist <blindqui@cern.ch>"]

evgenConfig.generators = ["Whizard", "Pythia8" ]
