#--------------------------------------------------------------
# Powheg Wj setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Wj_Common.py')
PowhegConfig.idvecbos=-24
PowhegConfig.bornktmin=0.
PowhegConfig.bornsuppfact=0.
PowhegConfig.PDF=13000
PowhegConfig.nEvents *= 1.9
PowhegConfig.generate()


#--------------------------------------------------------------
# Pythia8 showering with main31 and CTEQ6L1
#--------------------------------------------------------------
include('./MC12JobOpts/Pythia8_AZNLO_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_Photos.py')
include('MC12JobOptions/Pythia8_Powheg_Main31.py')
topAlg.Photospp.PhotonSplitting=True
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 W+j production with AZNLO tune'
evgenConfig.keywords    = [ 'SM', 'W', 'jet' ]
evgenConfig.contact     = [ 'fabrice.balli@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]

include('./MC12JobOpts/OneLeptonFilter.py')
