evgenConfig.description = "ND minimum bias, with Monash tune"
evgenConfig.keywords = ["QCD", "minBias", "ND"]

include("MC12JobOptions/Pythia8_Monash_Common.py")

topAlg.Pythia8.Commands += ["SoftQCD:nonDiffractive = on"]
