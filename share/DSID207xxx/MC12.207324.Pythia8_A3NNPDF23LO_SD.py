evgenConfig.description = "Single-diffractive events, with the A3 MSTW2008LO tune. Default PomFlux1"
evgenConfig.keywords = ["QCD", "minbias"]

#include("MC12JobOptions/Pythia8_A2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_A3_NNPDF23LO_Common.py")

topAlg.Pythia8.Commands += ["SoftQCD:singleDiffractive = on"]
