## Job options file for Herwig++, min bias production

evgenConfig.description = "Herwig++ MB  with the CTEQ6L1 EE5 tune"
evgenConfig.generators = ["Herwigpp"]
evgenConfig.keywords = ["minBias"]

include ("MC12JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py")


cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEMinBias
set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV
set /Herwig/Cuts/QCDCuts:MHatMin 0.0*GeV
set /Herwig/Cuts/QCDCuts:X1Min 0.01
set /Herwig/Cuts/QCDCuts:X2Min 0.01
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
erase /Herwig/EventHandlers/LHCHandler:PostSubProcessHandlers 0
"""

topAlg.Herwigpp.Commands += cmds.splitlines()

	
