evgenConfig.description = "Gamma + up to 4 jets with ME+PS, slice in photon pT and with massive C and B quarks"
evgenConfig.keywords = [ "Z", "photon", "heavyFlavour" ]
evgenConfig.contact  = ["Paul Thompson <thompson@mail.cern.ch>", "frank.siegert@cern.ch"]
evgenConfig.process="SinglePhoton"
evgenConfig.inputconfcheck = "Sherpa_CT10_SinglePhotonMassiveCBPt15_CFilterBVeto"
evgenConfig.minevents = 100

include( "MC12JobOptions/Sherpa_CT10_Common.py" )

"""
(run){
  MASSIVE[4]=1
  MASSIVE[5]=1
  QCUT:=30.0
}(run)

(processes){
Process 93  93 -> 22 93 93{3}
Order_EW 1
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process 5  -5 -> 22 93 93{3}
Order_EW 1
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process 4 -4 -> 22 93 93{3}
Order_EW 1
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process 93 93 -> 22 5 -5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 5 -5 -> 22 5 -5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 93 5 -> 22 5 93{3}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process 93 -5 -> 22 -5 93{3}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process 93 93 -> 22 4 -4 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 93  4 -> 22 4 93{3}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process 93 -4 -> 22 -4 93{3}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process  4 -4 -> 22 4 -4 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process  5 -5 -> 22 4 -4 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process  4 -4 -> 22 5 -5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 93  5 -> 22 5 4 -4 93{1}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 93  4 -> 22 4 5 -5 93{1}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 93 -5 -> 22 -5 4 -4 93{1}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 93 -4 -> 22 -4 5 -5 93{1}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process  4  5 -> 22 4 5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process -4  5 -> 22 -4 5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process  4 -5 -> 22 4 -5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process -4 -5 -> 22 -4 -5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;
}(processes)

(selector){
DeltaR  22  93  0.3  20.0
PT 22 15.0 E_CMS
PT 93 1.0 E_CMS
PT 4 1.0 E_CMS
PT -4 1.0 E_CMS
PT 5 1.0 E_CMS
PT -5 1.0 E_CMS
}(selector)
"""


from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
HeavyFlavorBHadronFilter = HeavyFlavorHadronFilter(name="HeavyFlavorBHadronFilter")
HeavyFlavorBHadronFilter.RequestBottom=True
HeavyFlavorBHadronFilter.RequestCharm=False
HeavyFlavorBHadronFilter.Request_cQuark=False
HeavyFlavorBHadronFilter.Request_bQuark=False
HeavyFlavorBHadronFilter.RequestSpecificPDGID=False
HeavyFlavorBHadronFilter.RequireTruthJet=False
HeavyFlavorBHadronFilter.BottomPtMin=0*GeV
HeavyFlavorBHadronFilter.BottomEtaMax=4.0
#
HeavyFlavorCHadronFilter = HeavyFlavorBHadronFilter.clone('HeavyFlavorCHadronFilter')
HeavyFlavorCHadronFilter.RequestBottom=False
HeavyFlavorCHadronFilter.RequestCharm=True
HeavyFlavorCHadronFilter.CharmPtMin=0*GeV
HeavyFlavorCHadronFilter.CharmEtaMax=4.0
#
topAlg += HeavyFlavorBHadronFilter
topAlg += HeavyFlavorCHadronFilter
StreamEVGEN.RequireAlgs += [ "HeavyFlavorCHadronFilter" ]
StreamEVGEN.VetoAlgs += [ "HeavyFlavorBHadronFilter" ]
