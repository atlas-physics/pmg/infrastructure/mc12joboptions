include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa W+/W- -> munu + 0,1,2j@NLO + 3,4j@LO"
evgenConfig.keywords = ["W" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "Sherpa_CT10_Wmunu"

evgenConfig.process="""
(run){
  %INIT_ONLY 1
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=4; LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
}(run)

(processes){
  Process 93 93 -> 13 -14 93{NJET};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01;
  SCALES LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  End process;

  Process 93 93 -> -13 14 93{NJET};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01;
  SCALES LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  End process;
}(processes)

(selector){
  Mass 13 -14 2.0 E_CMS
  Mass -13 14 2.0 E_CMS
}(selector)
"""
