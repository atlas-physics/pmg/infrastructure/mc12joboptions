include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")

evgenConfig.description = "ttbar with a W->leptons filter"

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEHeavyQuark
set /Herwig/MatrixElements/MEHeavyQuark:QuarkType Top
"""
topAlg.Herwigpp.Commands += cmds.splitlines()
#topAlg.Herwigpp.InFileDump = "foo.in"

include("MC12JobOptions/TTbarWToLeptonFilter.py")
