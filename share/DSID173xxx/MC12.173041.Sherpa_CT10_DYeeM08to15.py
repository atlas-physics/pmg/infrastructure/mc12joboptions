include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z/gamma* -> ee + up to 5 jets using Sherpa's built-in ME+PS prescription, in the mass bin 8<m_ll<15"
evgenConfig.keywords = [ "EW", "Z", "DY" ]
evgenConfig.inputconfcheck = "DYeeM08to15"
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch" ]

evgenConfig.process="""
(processes){
  Process 93 93 -> 11 -11 93 93{4}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  End process;
}(processes)

(selector){
  Mass 11 -11 8.0 15.0
  "PT"  90  9.0,E_CMS:5.0,E_CMS  [PT_UP]
  PseudoRapidity  90  -2.8  2.8
}(selector)

(run){
}(run)
"""
