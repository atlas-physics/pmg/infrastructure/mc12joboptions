# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_wino_bino_650_250.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )

# Overriding standard neutralino settings for GGMWinoBino config
sparticles['neutralinos'] = ['~chi_20','~chi_30','~chi_40']
cmds = buildHerwigppCommands(['charginos','neutralinos'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GGM wino-bino grid generation with mwino=650 mbino=250'
evgenConfig.keywords = ['SUSY', 'GGM', 'wino', 'bino']
evgenConfig.contact = [ 'helenka.przysiezniak@cern.ch']

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds
