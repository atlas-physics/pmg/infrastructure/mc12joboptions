include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z/gamma* -> tautau + 4 to 5 jets using Sherpa's built-in MENLOPS prescription."
evgenConfig.keywords = [ "Z", "tautau", "4jets" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "christopher.young@cern.ch" ]
evgenConfig.minevents = 500

evgenConfig.process="""
(processes){
  Process 93 93 -> 15 -15 93 93 93 93 93{1}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  End process;
}(processes)

(selector){
  Mass 15 -15 40 E_CMS
}(selector)

(run){
  YUKAWA_TAU=0
  SOFT_SPIN_CORRELATIONS=1
}(run)
"""

topAlg.Sherpa_i.ResetWeight = 0
