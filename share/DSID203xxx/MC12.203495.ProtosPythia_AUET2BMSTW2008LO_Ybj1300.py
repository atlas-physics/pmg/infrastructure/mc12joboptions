evgenConfig.description = "Protos+Pythia for Ybj production with AUET2B tune and MSTW2008LO pdf"
evgenConfig.keywords = ["Ybj", "single production"]
evgenConfig.inputfilecheck = "Protos22.203495.Ybj"
evgenConfig.contact  = ["biedermd@physik.hu-berlin.de"]
evgenConfig.generators = ["Protos", "Pythia"]
# ... Pythia

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]
################################################################

