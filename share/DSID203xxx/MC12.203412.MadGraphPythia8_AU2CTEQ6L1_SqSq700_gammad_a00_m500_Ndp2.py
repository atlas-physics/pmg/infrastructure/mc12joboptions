evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description = "MadGraph+Pythia8 production JO with the AU2 CTEQ6L1 tune.  Darkphotons via squarks pair production with the hidden sector parameters, m = 500 MeV, alpha_d =0, N_gammad = 2."
evgenConfig.keywords = ["QCD", "lepton-jet", "HiddenValley", "boosted objects", "SUSY"]
evgenConfig.inputfilecheck = "SqSq700-gammad"
evgenConfig.contact = ["mahsan@cern.ch"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
