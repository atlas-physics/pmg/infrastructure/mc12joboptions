evgenConfig.description = "Quantum black holes (M_th = 3.0 TeV) decaying to two opposite sign electrons."
evgenConfig.contact = ["gingrich@ualberta.ca", "marc.bret.87@gmail.com"]
evgenConfig.keywords = ["exotics", "blackholes", "QBH", "dilepton"]
evgenConfig.generators += ["QBH"]
evgenConfig.inputfilecheck = "QBH_ee_n6_Mth03000"

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_LHEF.py")
