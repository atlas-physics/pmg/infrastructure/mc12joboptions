include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

evgenConfig.description = "HVT V0->Zh->bbbb (Model A, gV=1.0) with AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["Exotics", "HVT"]
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.inputfilecheck = 'HVT_Zh_bbbb_600GeV_Ag1'

evgenConfig.contact = ["Massimiliano Bellomo"]
