###############################################################
#
# Job options file
# Pythia8 Z' --> A+ph --> 3ph
# April 2014
#===============================================================

evgenConfig.description = "Zprime(400)->A(200)+ph production with the AU2 MSTW2008LO tune"
evgenConfig.keywords = [ "multiphoton", "a0", "exotics", "Zprime" ]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",
                            "Zprime:gmZmode = 3",
                            "Zprime:universality = off",
                            "Zprime:vt = 0.000000000001", # t and b channels need to stay on,
                            "Zprime:at = 0.000000000001", # so we set their couplings very small
                            "Zprime:vb = 0.000000000001",
                            "Zprime:ab = 0.000000000001",
                            "32:addChannel 1 0.5 101 36 22", # add Z'->A+ph channels
                            "32:addChannel 1 0.5 101 22 36", # add Z'->A+ph channels
                            "32:m0 = 400.0", # set Zprime mass
                            "PhaseSpace:mHatMin = 399.9",
                            "PhaseSpace:mHatMax = 400.1",
                            "32:onIfMatch = 36 22",
                            "32:onIfMatch = 22 36",
                            "32:offIfAny = 1 2 3 4 11 12 13 14 15 16",
                            "36:onMode = off",
                            "36:addChannel 1 1 101 22 22",
                            "36:m0 = 200.0", # set A mass
                            "36:mMin = 2.0",
                            "36:onIfMatch = 22 22" # decay A --> 2photon
                            ]

#==============================================================
#
# End of job options file
#
###############################################################
