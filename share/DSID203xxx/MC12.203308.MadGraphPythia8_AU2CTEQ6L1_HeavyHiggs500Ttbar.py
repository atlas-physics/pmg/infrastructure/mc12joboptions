evgenConfig.description = "MadGraph5+Pythia8 for Heavy Higgs (m=500GeV) to->tt"
evgenConfig.contact = ["james.ferrando@glasgow.ac.uk", "madalina.stanescu.bellu@cern.ch"] 
evgenConfig.keywords = ["Heavy Scalar","ttbar", "Heavy Higgs"]
evgenConfig.inputfilecheck = "HeavyHiggs500Ttbar"
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
