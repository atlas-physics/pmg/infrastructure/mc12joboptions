include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

evgenConfig.description = "Bulk RS Graviton->HH->bbbb (c=k/Mpl=1.0) with AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["Exotics", "Graviton" ,"WarpedED"]
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.inputfilecheck = 'SMRS_G300_hh_bbbb'

evgenConfig.contact = ["Massimiliano Bellomo"]
