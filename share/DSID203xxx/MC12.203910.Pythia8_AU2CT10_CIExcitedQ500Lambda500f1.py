

evgenConfig.description = "Excited quark from contact interactions with Pythia8, AU2 tune and CT10 PDF, m=500 GeV"
evgenConfig.keywords = ["CI", "excitedquark"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact  = [ "caterina.doglioni@cern.ch" ]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += [
          
			"ExcitedFermion:qq2dStarq = on",
			"ExcitedFermion:qq2uStarq = on",                 
                        "4000001:m0 = 500", # d* mass
                        "4000002:m0 = 500", # u* mass
                        "ExcitedFermion:Lambda =  500",
                        "ExcitedFermion:coupF = 1.0", # SU(2) coupling
                        "ExcitedFermion:coupFprime = 1.0", # U(1) coupling
                        "ExcitedFermion:coupFcol = 1.0", # SU(3) coupling
                        "4000001:mayDecay = on", # d* -> d g, d gam, d Z0, d W-
                        "4000002:mayDecay = on"] # u* -> u g, u gam, u Z0, u W+


  