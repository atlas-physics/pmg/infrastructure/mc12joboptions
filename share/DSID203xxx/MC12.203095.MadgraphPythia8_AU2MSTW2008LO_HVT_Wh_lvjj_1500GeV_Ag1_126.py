#  choose the pdf
# ---------------
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10"]

evgenConfig.generators += ["MadGraph","Pythia8"]
evgenConfig.contact = ["Frederick.Dallaire@cern.ch"]
evgenConfig.description = "VC -> Wh -> lv+jj, M(VC)=1500 GeV, Model Ag1, LHEF produced with MadGraph5 v1.5.10"
evgenConfig.keywords = ["HVT", "Dibosons", "Multileptons"]
evgenConfig.inputfilecheck = 'HVT'
