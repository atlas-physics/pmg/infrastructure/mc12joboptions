include("MC12JobOptions/Pythia8_A2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.description = "s-channel EFT for mono-photon"
evgenConfig.keywords = ["EFT", "mono-photon", "gamma"]
evgenConfig.contact  = ["andrew.james.nelson@cern.ch"]

evgenConfig.inputfilecheck = 'schan_eft'

