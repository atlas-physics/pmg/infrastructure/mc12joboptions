evgenConfig.description = "MadGraph5+Pythia8 production JO with the AU2 MSTW2008LO tune for 400 GeV bstar (RH)"

evgenConfig.keywords = ["Bprime","single Production"]

evgenConfig.inputfilecheck = "bstarWtInclusive"

evgenConfig.generators = ["MadGraph", "Pythia8"]

evgenConfig.contact =  ['laura.rehnisch@cern.ch']

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

assert hasattr(topAlg, "Pythia8")
topAlg.Pythia8.LHEFile = "events.lhe"
topAlg.Pythia8.CollisionEnergy = int(runArgs.ecmEnergy)
