evgenConfig.description = "RPV Long-lived, DV->e+jets events, with PYTHIA6"
evgenConfig.keywords = ["RPV", "lambdaprime", "elsectron"]

#The default tune for Pythia samples (for MC12)
include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )

topAlg.Pythia.PythiaCommand += [
    "pysubs msel 40",  #  squark and gluino production
    "pymssm imss 1 11" # Select SLHA
    ]

slha_file = "susy_DV_RPV_1_1.slha" #SLHA input file
topAlg.Pythia.SusyInputFile = slha_file

#=### Switch off Pythia color strgins, they don't work correctly for late decaying particles!
topAlg.Pythia.PythiaCommand += ["pypars mstp 95 0"]

topAlg.Pythia.PythiaCommand += [
    "pymssm imss 52 3", #switch on RPV lambda^prime coupling
    "pymsrv rvlamp 111 0.000002", # setting lambda^prime coupling strength for Electrons; need to change with LSPmass 
    "pydat1 mstj 22 3", # allow long decay length !
    "pydat1 parj 72 100000.", # max length set to 100m
    "pydat3 mdme 2205 1 0", # switch explicetly off RPV decays to neutrinos (for electrons)
    "pydat3 mdme 2206 1 0" # switch explicetly off RPV decays to neutrinos (for electrons)
    ]

#---------------------------------------------------------------
# Includes
#---------------------------------------------------------------
# ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )

# ... Photos
#include ( "MC12JobOptions/Pythia_Photos.py" )

evgenConfig.auxfiles += [ slha_file ]

