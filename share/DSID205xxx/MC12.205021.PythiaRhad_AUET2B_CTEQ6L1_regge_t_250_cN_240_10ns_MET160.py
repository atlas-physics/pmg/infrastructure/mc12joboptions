rhconf=runArgs.jobConfig[0].split("_")

MODEL = 'regge'
CASE = 'stop'
MASS = int(rhconf[5])
MASSX = int(rhconf[7])
DECAY = 'false'
LIFETIME = rhconf[8].replace("ns","").replace("p","0.")
TYPE = rhconf[6]

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " "+MODEL+" "+CASE+" "+str(MASS)+"GeV"+TYPE+" "+str(MASSX)+"GeV "+LIFETIME+"ns"

DECAYTYPE=""
if TYPE=="tt": DECAYTYPE="NOGLUINOGLUONDECAY=True;NOGLUINOLIGHTSQUARKDECAY=True;"

evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};DECAY={decay};LIFETIME={lifetime};MASSX={massx};CHARGE=999;{decaytype}preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS, decay = DECAY, lifetime = LIFETIME,massx=MASSX, decaytype=DECAYTYPE)

include( "MC12JobOptions/METRhadFilter.py" )
