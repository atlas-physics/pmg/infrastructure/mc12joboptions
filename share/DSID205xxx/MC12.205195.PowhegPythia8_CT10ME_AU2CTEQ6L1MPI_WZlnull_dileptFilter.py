#--------------------------------------------------------------
# Powheg WZ setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_WZ_Common.py')
PowhegConfig.decay_mode = 'WZlvll'

PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1

PowhegConfig.withnegweights = 1 

if evgenConfig.minevents > 0:
    PowhegConfig.nEvents = 3 * evgenConfig.minevents
else:
    PowhegConfig.nEvents = 3 * 5000

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()
	
#--------------------------------------------------------------
# Pythia8 showering with main31 and CTEQ6L1
#--------------------------------------------------------------
include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_Powheg_Main31.py')
	
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 WZ production with CT10 ME and AU2 CTEQ6L1 tune'
evgenConfig.keywords    = [ 'SM', 'WZ' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]

# ... Filter
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter("Multi2LLeptonFilter")

Multi2LLeptonFilter = topAlg.Multi2LLeptonFilter
Multi2LLeptonFilter.MinPt = 10000.
Multi2LLeptonFilter.MaxEta = 5.0
Multi2LLeptonFilter.NLeptons = 2
Multi2LLeptonFilter.IncludeHadTaus = 0

StreamEVGEN.RequireAlgs += [ "Multi2LLeptonFilter" ]

