include("MC12JobOptions/Pythia8_A2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.description = "WBF SUSY"
evgenConfig.keywords = ["WBF", "SUSY", "Compressed spectra"]
evgenConfig.contact  = ["andrew.james.nelson@cern.ch"]
evgenConfig.minevents = 4000

evgenConfig.inputfilecheck = 'bw_susy'

evgenConfig.auxfiles += [ "susy_bw_susy_200_150.202323.slha" ]

topAlg.Pythia8.Commands += ["SLHA:file = susy_bw_susy_200_150.202323.slha"]
