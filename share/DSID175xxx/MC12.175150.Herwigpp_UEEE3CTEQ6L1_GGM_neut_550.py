# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_neut_550.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gauginos'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GGM photon + b + MET neutralino line generation with mneut=550'
evgenConfig.keywords = ['SUSY', 'GGM', 'gluino', 'bino', 'higgsino']
evgenConfig.contact = [ 'andrew.kuhl@cern.ch']

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds
