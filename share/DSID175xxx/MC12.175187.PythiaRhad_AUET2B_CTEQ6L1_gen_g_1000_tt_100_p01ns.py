MODEL = 'generic'
CASE = 'gluino'
MASS = 1000
MASSX = 100
DECAY = 'false'
LIFETIME = '0.01'

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " generic gluino 1000GeV tt 100GeV 0.01ns"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};DECAY={decay};LIFETIME={lifetime};MASSX={massx};CHARGE=999;NOGLUINOGLUONDECAY=True;NOGLUINOLIGHTSQUARKDECAY=True;preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS, decay = DECAY, lifetime = LIFETIME, massx=MASSX)
