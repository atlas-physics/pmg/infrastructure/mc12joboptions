MODEL = 'generic'
CASE = 'gluino'
MASS = 1000
MASSX = 620
DECAY = 'false'
LIFETIME = '0.1'

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " generic gluino 1000GeV tt 620GeV 0.1ns"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};DECAY={decay};LIFETIME={lifetime};MASSX={massx};CHARGE=999;NOGLUINOGLUONDECAY=True;NOGLUINOLIGHTSQUARKDECAY=True;preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS, decay = DECAY, lifetime = LIFETIME, massx=MASSX)
