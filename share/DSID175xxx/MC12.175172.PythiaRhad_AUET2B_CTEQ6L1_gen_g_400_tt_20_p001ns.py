MODEL = 'generic'
CASE = 'gluino'
MASS = 400
MASSX = 20
DECAY = 'false'
LIFETIME = '0.001'

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " generic gluino 400GeV tt 20GeV 0.001ns"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};DECAY={decay};LIFETIME={lifetime};MASSX={massx};CHARGE=999;NOGLUINOGLUONDECAY=True;NOGLUINOLIGHTSQUARKDECAY=True;preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS, decay = DECAY, lifetime = LIFETIME, massx=MASSX)
