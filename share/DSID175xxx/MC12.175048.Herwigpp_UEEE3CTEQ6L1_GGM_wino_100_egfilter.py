# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_wino_100.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )

cmds = buildHerwigppCommands(['gauginos'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GGM wino grid generation with mwino=100'
evgenConfig.keywords = ['SUSY', 'GGM', 'wino']
evgenConfig.contact = [ 'Jovan.Mitrevski@cern.ch']

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
topAlg += ElectronFilter()
 
ElectronFilter = topAlg.ElectronFilter
ElectronFilter.Ptcut = 80000.
ElectronFilter.Etacut = 2.45

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()
 
PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 80000.
PhotonFilter.Etacut = 2.45
PhotonFilter.NPhotons = 1

StreamEVGEN.AcceptAlgs +=  [ "ElectronFilter", "PhotonFilter" ]
