evgenConfig.description = "gammagamma->mumu with Budnev parameterization, central lepton filter pt>10 GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "coherent", "leptons", "exclusive"]
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch"]

include("MC12JobOptions/Herwigpp_QED_Common.py")
from Herwigpp_i import config as hw

cmds = """\

# Cuts
cd /Herwig/Cuts
set QCDCuts:ScaleMin 0.0*GeV
set QCDCuts:X1Min 0
set QCDCuts:X2Min 0
set QCDCuts:X1Max 1.
set QCDCuts:X2Max 1.
erase QCDCuts:MultiCuts 0
set QCDCuts:MHatMin 20*GeV
set QCDCuts:MHatMax 60*GeV

# Selected the hard process

cd /Herwig/MatrixElements
insert SimpleQCD:MatrixElements 0 /Herwig/MatrixElements/MEgg2ff
set /Herwig/MatrixElements/MEgg2ff:Process Muon
"""

topAlg.Herwigpp.Commands += cmds.splitlines()

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 10000.
topAlg.LeptonFilter.Etacut = 2.8

evgenConfig.minevents = 5000


# To avoid warning from displaced vertices, bugfix needed in herwig++
from TruthExamples.TruthExamplesConf import TestHepMC
testHepMC = TestHepMC()
testHepMC.MaxTransVtxDisp = 1000000 
testHepMC.MaxVtxDisp      = 1000000000 
topAlg += testHepMC
