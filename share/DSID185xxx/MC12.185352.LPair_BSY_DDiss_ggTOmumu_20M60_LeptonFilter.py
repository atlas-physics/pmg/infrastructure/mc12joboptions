
evgenConfig.description = "gammagamma -> mumu production with LPAIR DoubleDiss, 20<M<60GeV, central lepton filter pt>10 GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "dissociation", "leptons", "muons", "20<M<60GeV", "lepton filter"]
evgenConfig.contact = ["Mateusz Dyndal <mateusz.dyndal@cern.ch>"]

evgenConfig.minevents = 5000

if runArgs.ecmEnergy == 7000:
   evgenConfig.inputfilecheck = 'DDiss_ggTOmumu_7TeV_20M60'
elif runArgs.ecmEnergy == 8000:
   evgenConfig.inputfilecheck = 'DDiss_ggTOmumu_8TeV_20M60'
else:
   raise Exception("Incompatible inputGeneratorFile")

include("MC12JobOptions/HepMCReadFromFile_Common.py")
evgenConfig.tune = "none"
