include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z -> mumu + up to 5 jets with pT_Z>70 GeV."
evgenConfig.keywords = ["Z","mu"]
evgenConfig.inputconfcheck = "Zmumu"
evgenConfig.contact  = [ "james.henderson@cern.ch" ]

evgenConfig.process="""
(run){
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 -> 13 -13 93 93{4}
  Order_EW 2
  CKKW sqr(30/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  !Enhance_Factor 4.0 {4,5,6,7,8}
  End process;
}(processes)

(selector){
  Mass 13 -13 40.0 E_CMS
  PT2  13 -13 70.0 E_CMS
}(selector)
"""
