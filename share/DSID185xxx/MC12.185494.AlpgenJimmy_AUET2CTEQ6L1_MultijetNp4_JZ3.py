evgenConfig.description = 'alpgen_mutlijets'
evgenConfig.contact = ['zach.marshall@cern.ch','Michael.Stoebe@cern.ch']
evgenConfig.process = 'alpgen_Njet_4p'
evgenConfig.inputfilecheck = 'Multijet'
evgenConfig.keywords = ["SM","Multijet","QCD","jets"]
evgenConfig.minevents = 50

include('AlpGenControl/MC12.AlpGenHerwig.py')

include("MC12JobOptions/JetFilter_JZ3.py")
