###############################################################
#
# Job options file for POWHEG with Pythia8
# C. Johnson, 15.08.2014 <christian.johnson@cern.ch>
#
#==============================================================

#--------------------------------------------------------------
# Configuration for Evgen Job Transforms
#--------------------------------------------------------------
evgenConfig.description    = "POWHEG+Pythia8 VBF_W mu nu production, muR*0.5 muF*0.5 and AU2 CT10 tune"
evgenConfig.keywords       = [ "electroweak","W","2jet","VBF","tChannel","electron" ]
evgenConfig.contact        = [ "christian.johnson@cern.ch" ]
evgenConfig.generators    += [ "Powheg", "Pythia8" ]
evgenConfig.inputfilecheck = "Wplus_munu"
evgenConfig.minevents      = 10000

## Pythia8 Showering
include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Powheg.py")