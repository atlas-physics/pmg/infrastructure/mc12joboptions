include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np2_JZ1'
evgenConfig.minevents = 2000
evgenConfig.description = 'pp -> 2 light jets, four-jet filter with p_{T}^{1} - 30-80 GeV and p_{T}^{2-4} > 15 GeV and Double Parton Scatter filter'

# Include the four jet filter and set the pT cuts here
include("MC12JobOptions/FourJetFilter_JZ1.py")
topAlg.QCDTruthMultiJetFilter.MinLeadJetPt = 30.*GeV
topAlg.QCDTruthMultiJetFilter.MaxLeadJetPt = 80.*GeV
topAlg.QCDTruthMultiJetFilter.NjetMinPt = 15.*GeV

include('MC12JobOptions/MultiParticleFilter_DPS_Jimmy.py')
