
evgenConfig.description = "gammagamma -> mumu production with LPAIR SingleDiss, M>200GeV, central lepton filter pt>10 GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "dissociation", "leptons", "muons", "M>200GeV", "lepton filter"]
evgenConfig.contact = ["Mateusz Dyndal <mateusz.dyndal@cern.ch>"]

evgenConfig.minevents = 5000

if runArgs.ecmEnergy == 7000:
   evgenConfig.inputfilecheck = 'SDiss_ggTOmumu_7TeV_200M'
elif runArgs.ecmEnergy == 8000:
   evgenConfig.inputfilecheck = 'SDiss_ggTOmumu_8TeV_200M'
else:
   raise Exception("Incompatible inputGeneratorFile")

include("MC12JobOptions/HepMCReadFromFile_Common.py")
evgenConfig.tune = "none"
