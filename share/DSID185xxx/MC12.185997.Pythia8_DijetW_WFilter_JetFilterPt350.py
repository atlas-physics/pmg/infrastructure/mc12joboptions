evgenConfig.description = "Dijet, W emission with truth jet 350 GeV filter (AU2 CT10)"
evgenConfig.keywords = ["QCD", "jets"]

ver =  os.popen("cmt show versions External/Pythia8").read()
if 'Pythia8-01' in ver[:50]:
    include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
else:
    include("MC12JobOptions/Pythia82_AU2_CT10_Common.py")

topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 175.",
     "TimeShower:weakShower = on",
     #"WeakSingleBoson:ffbar2W = on"
     "WeakBosonAndParton:qqbar2Wg = on",
     "WeakBosonAndParton:qg2Wq = on",
     "WeakBosonAndParton:ffbar2Wgm = on",
     "WeakBosonAndParton:fgm2Wf = on,"
     "WeakShower:vetoWeakJets = on",
     "WeakShower:vetoQCDJets = on",
     #"SpaceShower:weakShower = on"
     ]

include("MC12JobOptions/JetFilter_JZ4.py")
topAlg.QCDTruthJetFilter.MinPt = 350.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 8000.*GeV

from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
if "ParticleFilter" not in topAlg:
  topAlg += ParticleFilter()
if "ParticleFilter" not in StreamEVGEN.RequireAlgs:
  StreamEVGEN.RequireAlgs += ["ParticleFilter"]
topAlg.ParticleFilter.Ptcut = 0.
topAlg.ParticleFilter.Etacut = 999.
topAlg.ParticleFilter.PDG = 24
topAlg.ParticleFilter.StatusReq = -1

evgenConfig.minevents = 50
