include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np3_JZ1'
evgenConfig.minevents = 2000
evgenConfig.description = 'pp -> 3 light jets, four-jet filter with p_{T}^{1} - 30-80 GeV and p_{T}^{2-4} > 15 GeV'

# Include the four jet filter and set the pT cuts here
include("MC12JobOptions/FourJetFilter_JZ1.py")
topAlg.QCDTruthMultiJetFilter.MinLeadJetPt = 30.*GeV
topAlg.QCDTruthMultiJetFilter.MaxLeadJetPt = 80.*GeV
topAlg.QCDTruthMultiJetFilter.NjetMinPt = 15.*GeV
