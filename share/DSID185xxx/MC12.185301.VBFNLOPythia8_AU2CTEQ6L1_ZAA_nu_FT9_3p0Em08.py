evgenConfig.description = "VBFNLO+Pythia8 production for AQGC Z(nu nu) + 2 photons with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["triboson", "EW", "gamma", "invisible", "AQGC"]
evgenConfig.inputfilecheck = "VBFNLO.185301.ZAA_nu_FT9_3p0Em08"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Evgeny Soldatov <Evgeny.Soldatov@cern.ch>"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")
