include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "ttbar gamma  production "
evgenConfig.contact  = [ "zhijun.liang@cern.ch"]
evgenConfig.keywords = [ "EW", "ttbar", "gamma" ]


evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.10
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 ->  6[a] -6[b] 22
  Decay 6[a] -> 5 24[c]
  Decay -6[b] -> -5 -24[d]
  Decay 24[c] -> -11 12
  Decay -24[d] ->15 -16
  CKKW sqr(20/E_CMS)
  Max_Order_EW 5
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1
  Print_Graphs;
  End process;

  Process 93 93 ->  6[a] -6[b]
  Decay 6[a] -> 5 24[c]
  Decay -6[b] -> -5 -24[d] 
  Decay 24[c] -> -11 12 22
  Decay -24[d] ->15 -16
  CKKW sqr(20/E_CMS)
  Max_Order_EW 5
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1
  Print_Graphs;
  End process;

  Process 93 93 ->  6[a] -6[b]
  Decay 6[a] -> 5 24[c]
  Decay -6[b] -> -5 -24[d] 
  Decay 24[c] -> -11 12 
  Decay -24[d] ->15 -16 22
  CKKW sqr(20/E_CMS)
  Max_Order_EW 6
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1
  Print_Graphs;
  End process;

}(processes)
(selector){

  PT 22 10 E_CMS
  PT 90 8 E_CMS
}(selector)
"""
