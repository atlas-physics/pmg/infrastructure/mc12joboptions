evgenConfig.description = "POWHEG+Pythia8 WpWm->em production using CT10 PDF and AU2 CT10 tune, for scale variation study: scale nom" 
evgenConfig.keywords = ["electroweak", "WW", "leptons"] 
evgenConfig.inputfilecheck = "Powheg_CT10.*185367" 
evgenConfig.minevents = 5000 
evgenConfig.contact = ["Yusheng Wu <Yusheng.Wu@cern.ch>"] 
include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py") 
include("MC12JobOptions/Pythia8_Photos.py") 

