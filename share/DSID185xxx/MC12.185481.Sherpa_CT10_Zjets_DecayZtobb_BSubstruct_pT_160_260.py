## Sherpa boosted Z->bb
## Made to replace 147181/147182 where EW order 2 digrams other
## than Z->bb were included
## Author: Luke Lambourne (luke.lambourne@cern.ch)

evgenConfig.description = "High pT Z/gamma* -> bb + 1 or 2 jets."
evgenConfig.keywords = ["Z", "bottom", "hadronic", "boosted"]
evgenConfig.contact = [ "luke.lambourne@cern.ch" ]

include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.process="""
(model){
  PARTICLE_CONTAINER 98[m:-1] zgamma 23 22;
}

(processes){
  Process 93 93 -> 98[a] 93 93{1}
  Decay 98[a] -> 5 -5
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {4}
  End process;

  Process 93 5 -> 98[a] 5 93{1}
  Decay 98[a] -> 5 -5
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {4}
  End process;

  Process 93 -5 -> 98[a] -5 93{1}
  Decay 98[a] -> 5 -5
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {4}
  End process;

  Process 5 -5 -> 98[a] 93 93{1}
  Decay 98[a] -> 5 -5
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {4}
  End process;
  
  Process 93 93 -> 98[a] 5 -5
  Decay 98[a] -> 5 -5
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {4}
  End process;
  
  Process 5 -5 -> 98[a] 5 -5
  Decay 98[a] -> 5 -5
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {4}
  End process;
}(processes)

(selector){
  Mass 5 -5 60 E_CMS
  PT2  5 -5 70 E_CMS
}(selector)

(run){
  MASSIVE[5]=1
}(run)
"""

topAlg.Sherpa_i.ResetWeight = 0

include("MC12JobOptions/Zbb_160_260_Filter.py")
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "185481.Ztobb_160_260"
