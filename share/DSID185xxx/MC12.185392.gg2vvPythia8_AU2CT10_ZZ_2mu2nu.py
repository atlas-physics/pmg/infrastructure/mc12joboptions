################################################################
#
# gg2VV 3.1.6/Powheg/Pythia8 gg -> (H) -> ZZ, with ZZ -> 2mu2nu_(e/tau)
# 
# accounting for nu_mu by multiplied xsec on AMI

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '24:onMode = off',#decay of W
                            '23:onMode = off',#decay of Z
                            ]

evgenConfig.generators += [ 'gg2vv','Pythia8' ]
evgenConfig.description = 'gg2VV, (H->)ZZ-> 2mu2nu_(e/tau) using CT10 PDF and PowhegPythia8 with AU2 CT10 tune, mll>4GeV without filter, accounting for nu_mu by multiplied xsec on AMI'
evgenConfig.keywords = ['diboson', 'leptonic', 'EW']
evgenConfig.contact = ['jochen.meyer@cern.ch']
evgenConfig.inputfilecheck = 'gg2VV0316.185392.ZZ_2mu2nu'
