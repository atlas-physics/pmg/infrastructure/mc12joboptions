## POWHEG+Pythia8 DYmumu_6M12

evgenConfig.description = "POWHEG+Pythia8 Z/gamma*->mumu with mass cut 6<M<12GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "mu"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYmumu_6M12"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
