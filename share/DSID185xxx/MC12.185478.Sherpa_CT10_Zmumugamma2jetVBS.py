
include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "mu+ mu- gamma +dijet vector boson scattering (VBS), EWK contribution only, high mass filter M(ll)>40GeV"
evgenConfig.contact  = [ "zhijun.liang@cern.ch"]
evgenConfig.keywords = [ "EW", "VBS", "gamma", "dijet" ]
evgenConfig.minevents = 200

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.10
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 ->  13 -13 22 93 93
  CKKW sqr(20/E_CMS)
  Order_EW 5
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1 {1,2,3,4,5,6,7}
  Print_Graphs;
  End process;
}(processes)
(selector){
  Mass 13 -13 40 E_CMS
  PT 22 10 E_CMS
  DeltaR 22 22 0.1 1000
  DeltaR 22 93 0.1 1000
  DeltaR 22 90 0.1 1000
  DeltaR 93 90 0.1 1000
  NJetFinder 2 15. 0. 0.4 -1;
}(selector)
"""
#  Max_Order_EW 6
