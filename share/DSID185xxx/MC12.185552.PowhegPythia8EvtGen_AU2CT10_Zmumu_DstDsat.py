evgenConfig.description = "POWHEG+Pythia8 Z->mumu production with AU2 CT10 tune and forced EvtGen decay"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "mu","EvtGen"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Zmumu"
evgenConfig.auxfiles += ['DstarP2D0PiP_D0satellitePlusAnti.DEC']
evgenConfig.minevents = 200

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/Pythia8_EvtGen.py")
topAlg.EvtInclusiveDecay.userDecayFile = "DstarP2D0PiP_D0satellitePlusAnti.DEC"

include("MC12JobOptions/DstarFilter.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 20000.0



