include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "WWgamma -> tau nu j j gamma  production "
evgenConfig.contact  = [ "zhijun.liang@cern.ch"]
evgenConfig.keywords = [ "EW", "Diboson", "gamma" ]


evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.20
}(run)

(model){
  MODEL         = SM
}(model)


(processes){
  Process 93 93 ->  24[a] -24[b] 22
  Decay 24[a] -> 93 93
  Decay -24[b] ->15 -16
  CKKW sqr(20/E_CMS)
  Max_Order_EW 5
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1
  Print_Graphs
  End process;

  Process 93 93 ->  24[a] -24[b] 22
  Decay -24[b] -> 93 93
  Decay 24[a] ->-15 16
  CKKW sqr(20/E_CMS)
  Max_Order_EW 5
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1
  Print_Graphs
  End process;


  Process 93 93 ->  24[a] -24[b] 
  Decay -24[b] -> 93 93
  Decay 24[a] ->-15 16 22
  CKKW sqr(20/E_CMS)
  Max_Order_EW 5
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1
  Print_Graphs
  End process;


  Process 93 93 ->  24[a] -24[b] 
  Decay 24[a] -> 93 93
  Decay -24[b] ->15 -16 22
  CKKW sqr(20/E_CMS)
  Max_Order_EW 5
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1
  Print_Graphs
  End process;

}(processes)
(selector){
  PT 22 10 E_CMS
  PT 90 8 E_CMS
  DeltaR 22 22 0.1 1000
  DeltaR 22 93 0.1 1000
  DeltaR 22 90 0.1 1000
  DeltaR 93 90 0.1 1000
}(selector)
"""
