## POWHEG+Pythia8 DY->mumu

include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune="AZNLO_CTEQ6L1"
process="Z_mumu"

def powheg_override():
    # needed for AZNLO tune
    PowhegConfig.ptsqmin = 4.0
    PowhegConfig.mass_low = 20.
    PowhegConfig.mass_high = 60.

evgenConfig.description = "POWHEG+Pythia8 DY->mumu production with mass cut 20<M<60GeV with di-lepton filter and AZNLO CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "muon"] 

# compensate filter efficiency
evt_multiplier = 15

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 10000.
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.NLeptons = 2
