###############################################################
#
# Job options file for POWHEG with Pythia8
# C. Johnson, 19.05.2014 <christian.johnson@cern.ch>
#
#==============================================================

#--------------------------------------------------------------
# Configuration for Evgen Job Transforms
#--------------------------------------------------------------
evgenConfig.description    = "POWHEG+Pythia8 W+2jets production with bornsuppfact (pT1 80GeV, pT2 60GeV, Mjj 500GeV), muR*2 muF*2 and AU2 CT10 tune at 7TeV"
evgenConfig.keywords       = [ "QCD","W","2jet","muon","SM","NLO","muRup","muFup" ]
evgenConfig.contact        = [ "christian.johnson@cern.ch" ]
evgenConfig.generators    += [ "Powheg", "Pythia8" ]
evgenConfig.inputfilecheck = "Wminus_munu"
evgenConfig.minevents      = 10000

## Pythia8 Showering
include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Powheg.py")