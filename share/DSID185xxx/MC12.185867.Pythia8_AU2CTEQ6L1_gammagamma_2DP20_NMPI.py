evgenConfig.description = "Pythia8 diphoton sample. gammagamma + gammajet events with at least two hard process or parton shower photons with pT > 20 GeV"
evgenConfig.keywords = ["egamma", "performance", "jets", "photon", "QCD"]
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

## Configure Pythia
topAlg.Pythia8.Commands += ["PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PromptPhoton:ffbar2gammagamma = on",
                            "PromptPhoton:gg2gammagamma = on",
                            "PhaseSpace:pTHatMin = 18",
                            "PartonLevel:MI = off"]

#-------------------------------------------------------------
# Filter
#-------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
topAlg += DirectPhotonFilter()

DirectPhotonFilter = topAlg.DirectPhotonFilter
DirectPhotonFilter.Ptcut = 20000.
DirectPhotonFilter.Etacut =  2.7
DirectPhotonFilter.NPhotons = 2

StreamEVGEN.RequireAlgs +=  [ "DirectPhotonFilter" ]


