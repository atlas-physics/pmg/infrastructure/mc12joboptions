## MC@NLO+HERWIG+JIMMY config -- just include a standard HERWIG+JIMMY tune and enable MC@NLO mode
# also WZ lepton filter is added 
      
include('MC12JobOptions/Jimmy_AUET2_CT10_Common.py')
      
topAlg.Herwig.HerwigCommand += ['iproc mcatnlo']
      
include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')
	
## Instantiate the lepton filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "WZtoLeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import WZtoLeptonFilter
    topAlg += WZtoLeptonFilter()

## Add this filter to the algs required to be successful for streaming
if "WZtoLeptonFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["WZtoLeptonFilter"]

## Cut parameters to keep events with at least one lepton (e/mi from W/Z or from tau)
topAlg.WZtoLeptonFilter.NeedWZleps = 1
topAlg.WZtoLeptonFilter.ElectronMuonNumber = 0
topAlg.WZtoLeptonFilter.BCKGvsSIGNAL = 0
topAlg.WZtoLeptonFilter.IdealReconstructionEfficiency = 0

evgenConfig.generators += [ 'McAtNlo', 'Herwig' ]
evgenConfig.description = 'MC@NLO VV->inclusive using CT10 PDF and fHerwig with AUET2_CT10 configuration'
evgenConfig.keywords = ['EW', 'diboson']
evgenConfig.contact = ['sanya.solodkov@cern.ch']
evgenConfig.inputfilecheck = 'WW_V_V_m0p3_1p0_0p3_8TeV'

#evgenConfig.efficiency = 0.444
evgenConfig.minevents = 5000
