evgenConfig.description = "POWHEG+PYTHIA8 ggH H->gg with AU2,NNPDF21NLO"
evgenConfig.keywords = ["SMhiggs", "ggF", "gamma"]
evgenConfig.inputfilecheck = "ggH_SM_M125"

include("MC12JobOptions/Pythia8_AU2_NNPDF21NLO_Common.py")
include("MC12JobOptions/Pythia8_Powheg.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 22 22'
                            ]
