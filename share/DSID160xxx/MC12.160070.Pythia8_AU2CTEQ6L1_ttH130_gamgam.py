evgenConfig.description = "PYTHIA8 ttH H->gamgam with AU2 CT10"
evgenConfig.keywords = ["SMhiggs", "ttH", "gamma"]

#include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 130',
                            '25:mWidth = 0.00491',
                            '25:doForceWidth = true',
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 22 22',
                            'HiggsSM:gg2Httbar = on',
                            'HiggsSM:qqbar2Httbar = on'
                            ]
