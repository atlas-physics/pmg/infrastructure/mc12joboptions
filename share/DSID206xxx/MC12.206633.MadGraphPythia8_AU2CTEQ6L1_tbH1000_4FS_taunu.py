evgenConfig.description = "MadGraph5+Pythia8 for gg>t(b)ChargedHiggs"
evgenConfig.keywords = ["ChargedHiggs"]
evgenConfig.inputfilecheck = "tHpl_4FS_2HDM_M1000"
evgenConfig.generators = ["MadGraph", "Pythia8"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

topAlg.Pythia8.Commands += [ 'Higgs:useBSM = on',
                             '37:onMode = off',
                             '37:onIfMatch = 15 16'
                             ]

evgenConfig.minevents=5000
