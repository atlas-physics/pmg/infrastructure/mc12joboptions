evgenConfig.description = "bbjetjet production using MadGraph+Pythia8 and AU2 CTEQ6L1"
evgenConfig.keywords = ["jets", "bb"]
evgenConfig.contact  = ["olivier.arnaez@cern.ch", "escalier@lal.in2p3.fr"]

processPythia = ""
fcard = open('proc_card_mg5.dat','w')
processPythia = "Merging:Process = pp>bbjj"
proccard = """
import model sm
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define bjet = b b~
define lightjet = g u d s u~ d~ s~
generate p p > bjet bjet lightjet lightjet @0
add process p p > bjet bjet lightjet lightjet j @1
output -f
"""
fcard.write(proccard)
fcard.close()

include("MC12JobOptions/MadGraphPythia8_AU2CTEQ6L1_BBxx.py")
