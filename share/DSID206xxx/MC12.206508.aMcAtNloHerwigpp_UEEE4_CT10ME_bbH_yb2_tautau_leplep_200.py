evgenConfig.description = "MadGraph5_aMC@NLO+Herwig++ bbH production"
evgenConfig.keywords = ["bbH", "Higgs","tautau","leplep"]
evgenConfig.contact  = ["Nikolaos Rompotis <Nikolaos.rompotis@cern.ch>,Junichi Tanaka <Junichi.Tanaka@cern.ch>"]
evgenConfig.inputfilecheck = "bbH_yb2_tautau_leplep_200"
evgenConfig.generators = ["aMcAtNlo", "Herwigpp"]
evgenConfig.minevents = 2000


include("MC12JobOptions/Herwigpp_UEEE4_MRSTMCal_CT10ME_LHEF_Common.py")
include("MC12JobOptions/Herwigpp_EvtGen.py")

cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost

set /Herwig/Particles/h0/h0->W+,W-;:OnOff 0
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff 0
set /Herwig/Particles/h0/h0->b,bbar;:OnOff 0
set /Herwig/Particles/h0/h0->c,cbar;:OnOff 0
set /Herwig/Particles/h0/h0->g,g;:OnOff 0
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff 0
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff 0
set /Herwig/Particles/h0/h0->t,tbar;:OnOff 0
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff 1

"""

topAlg.Herwigpp.Commands += cmds.splitlines()


# ... Filter H->VV->Children
include("MC12JobOptions/XtoVVDecayFilterExtended.py")
topAlg.XtoVVDecayFilterExtended.PDGGrandParent = 25
topAlg.XtoVVDecayFilterExtended.PDGParent = 15
topAlg.XtoVVDecayFilterExtended.StatusParent = 2
topAlg.XtoVVDecayFilterExtended.PDGChild1 = [11,13]
topAlg.XtoVVDecayFilterExtended.PDGChild2 = [11,13]
