# Setting PDF
include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia_Tauola.py" )

# Loading LHE jobOptions
topAlg.Pythia.PythiaCommand += [ "pyinit user lhef",
                                 "pyinit pylisti -1",
                                 "pyinit pylistf 1",
                                 "pyinit dumpr 1 2"
                                 ]

# Settings for LHE interface
evgenConfig.generators += ["MadGraph"]
evgenConfig.description = "gg>hh at LO with MadGraph5_aMC@NLO, Higgs decays to bbyy using Pythia6, to be showered with Pythia6 using CTEQ6L1 PDFs"
evgenConfig.keywords = ["hh","bbgamgam","lambda=1"]

evgenConfig.inputfilecheck = 'gg_hh125_bbyy_lambda01'
