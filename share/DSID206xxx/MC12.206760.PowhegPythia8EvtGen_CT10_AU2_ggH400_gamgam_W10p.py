#--------------------------------------------------------------
# Powheg ggH_quark_mass_effects setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggH_quark_mass_effects_Common.py')

# Set Powheg variables, overriding defaults
PowhegConfig.mass_H  = 400.
PowhegConfig.width_H = PowhegConfig.mass_H*.100
PowhegConfig.bwshape = 1
PowhegConfig.runningscale = 2
PowhegConfig.use_massive_b = True
PowhegConfig.use_massive_c = True


# Set scaling and masswindow parameters
hfact_scale    = 1.2
masswindow_max = 30.


if PowhegConfig.mass_H <= 1000.:
  PowhegConfig.ew = 1
else:
  PowhegConfig.ew = 0

# Calculate an appropriate masswindow and hfact
masswindow = masswindow_max
if PowhegConfig.ew == 1:
  if PowhegConfig.mass_H <= 700.:
    masswindow = min( (1799.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
  else:
    masswindow = min( (1999.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
  PowhegConfig.masswindow = masswindow
PowhegConfig.hfact = PowhegConfig.mass_H / hfact_scale

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 2.5

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py')
include('MC12JobOptions/Pythia8_Photos.py')
topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 22 22'
                            ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+PYTHIA8 ggH H->gg with AU2,CT10, mass=400, width=.100*mH'
evgenConfig.keywords    = [ 'SMhiggs', 'ggF', 'gamma' ]
evgenConfig.contact     = [ 'Junichi.Tanaka@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
