#
# Job options file for MadGraph/aMC@NLO tWH sample showered with Herwig++
# by Andrey Loginov (Yale)
#
evgenConfig.description = "aMC@NLO showered with Herwig++"
evgenConfig.keywords = ["WtH", "generic", "yukawa", "top", "coupling", "Higgs", "Htop"]
evgenConfig.contact  = ["Andrey Loginov <andrey.loginov@yale.edu>"]
evgenConfig.generators = ["aMcAtNlo", "Herwigpp"]
#
evgenConfig.inputfilecheck = 'twh_bbbar_null_ct10'
#
include("MC12JobOptions/Herwigpp_UEEE4_MRSTMCal_CT10ME_Common.py")

from Herwigpp_i import config as hw
topAlg.Herwigpp.Commands += hw.lhef_cmds(filename="events.lhe", nlo=True).splitlines()

#-- Higgs to b bbar
cmds="""do /Herwig/Particles/h0:SelectDecayModes h0->b,bbar;
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
"""

topAlg.Herwigpp.Commands += cmds.splitlines()
