include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.description = "Photon + bb+jet suitable for hh->bbaa searches"
evgenConfig.process = "Part of suite of samples with diphoton and dijet mass cuts loosely consistent with hh production"
evgenConfig.keywords = ["jets","multijet","3jet","photon","heavyFlavor","bottom"]
evgenConfig.inputfilecheck = 'madgraph5.206456.bbja'
evgenConfig.contact = ['Jahred Adelman']
