#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
PowhegConfig.mass_H  = 2000.
PowhegConfig.width_H = 0.00407

#
PowhegConfig.withdamp = 1

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 5.

# Generate Powheg events
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py')
include('MC12JobOptions/Pythia8_Photos.py')

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 23 23',
                            '23:onMode = off',#decay of Z
                            '23:mMin = 2.0',
                            '23:onIfAny = 1 2 3 4 5 6 12 14 16'
                            ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8 using NWA, VBF H->ZZ->nunuqq with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "VBF", "Z","neutrino","hadronic"]
evgenConfig.contact     = [ 'Junichi.Tanaka@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]

#--------------------------------------------------------------
# X->VV filter
#--------------------------------------------------------------
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 23
topAlg.XtoVVDecayFilter.StatusParent = 22
topAlg.XtoVVDecayFilter.PDGChild1 = [12,14,16]
topAlg.XtoVVDecayFilter.PDGChild2 = [1,2,3,4,5,6]
