include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.description = "Diphoton + dijet suitable for hh->bbaa searches"
evgenConfig.process = "Part of suite of samples with diphoton and dijet mass cuts loosely consistent with hh production"
evgenConfig.keywords = ["jets","dijet","2jet","diphoton"]
evgenConfig.inputfilecheck = 'madgraph5.206459.jjaa'
evgenConfig.contact = ['Jahred Adelman']
