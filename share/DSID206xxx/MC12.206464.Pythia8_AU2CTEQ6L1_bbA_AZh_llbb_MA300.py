evgenConfig.description = "A->Zh->llbb with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["A","Zh","2HDM","bbA"]
evgenConfig.contact  = [ "nikolaos.rompotis@cern.ch"]
evgenConfig.minevents = 10000

#Tune
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

#Pythia8 Commands
topAlg.Pythia8.Commands += ["Higgs:useBSM on", 
                            "HiggsBSM:gg2A3bbbar on",
                            "25:m0 125.0", #h mass
                            "36:m0 300.0", #A mass
                            "36:onMode = off", #turn off all A decays
                            "36:onIfMatch = 23 25", #A->Zh
                            "SLHA:file = narrowWidth.slha", # need to check whether it is needed
                            "25:onMode = off", 
#                            "25:oneChannel = on 1.0 100 5 -5 ",
                            "25:onIfMatch = 5 -5",
                            "23:onMode = off",
                            "23:onIfAny = 11 13 15"
                            ] 

# set width for heavy Higgs
nwf=open('./narrowWidth.slha', 'w')
nwfinput = """#           PDG      WIDTH
DECAY   36  1.00
#          BR         NDA          ID1       ID2       ID3       ID4
1       2       23      25
"""
nwf.write(nwfinput)
nwf.close()
