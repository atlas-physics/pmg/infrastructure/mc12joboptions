evgenConfig.description = "PYTHIA8 ttH H->inv with AU2 CT10"
evgenConfig.keywords = ["SMhiggs", "ttH", "inv"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")


topAlg.Pythia8.Commands += [
    '25:m0 = 125',
    '25:mWidth = 0.00407',
    '25:doForceWidth = true',
    'HiggsSM:gg2Httbar = on',
    'HiggsSM:qqbar2Httbar = on',
    "25:onMode = off",
    "25:addChannel = 1 2.64e-2 100 23  23",
    "23:onMode = off",
    "23:onIfAny =12 12",
    ]
