include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune='AU2_CT10'
process="ZZ_eeee"

evgenConfig.inputconfcheck = "ZZ_4e"
evgenConfig.minevents = 200

evgenConfig.description = "POWHEG+Pythia8 ZZ -> 4e production mll>4GeV with mass filter m4l>500GeV and m4l<50TeV and muon filter pt>3GeV using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["EW"  ,"diboson", "leptonic"]
evgenConfig.contact = ["Antonio Salvucci <antonio.salvucci@cern.ch>"]

# compensate filter efficiency
evt_multiplier = 200

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/FourLeptonInvMassFilter.py")
topAlg.FourLeptonInvMassFilter.MinPt = 3.*GeV
topAlg.FourLeptonInvMassFilter.MaxEta = 5.
topAlg.FourLeptonInvMassFilter.MinMass = 500.*GeV
topAlg.FourLeptonInvMassFilter.MaxMass = 50000.*GeV
