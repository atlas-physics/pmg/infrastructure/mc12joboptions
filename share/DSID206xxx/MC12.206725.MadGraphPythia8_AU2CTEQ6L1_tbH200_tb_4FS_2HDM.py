evgenConfig.description = "MadGraph+Pythia8 tHch Hch->tb with AU2, CTEQ6L1"
evgenConfig.keywords = ["nonSMhiggs", "Hch","top","bottom","semileptonic"]
evgenConfig.inputfilecheck = "tHpl_2HDM_M200"
if runArgs.ecmEnergy == 13000:
    evgenConfig.inputfilecheck = "tHpl_4FS_2HDM_M200"

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [ 'Higgs:useBSM = on',
                             '37:onMode = off',
                             '37:onIfMatch = 5 6'
                             ]

include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.Ptcut = 0
topAlg.TTbarWToLeptonFilter.NumLeptons = 1
