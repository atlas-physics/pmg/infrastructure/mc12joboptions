evgenConfig.description = "MadGraph5_aMC@NLO+Herwig++ bbH production"
evgenConfig.keywords = ["bbH", "Higgs","gamgam"]
evgenConfig.contact  = ["Junichi Tanaka <Junichi.Tanaka@cern.ch>"]
evgenConfig.inputfilecheck = 'MG5aMCAtNloHWpp_bbH125_ybyt'
evgenConfig.generators = ["aMcAtNlo", "Herwigpp"]

include("MC12JobOptions/Herwigpp_UEEE4_MRSTMCal_CT10ME_LHEF_Common.py")

cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost

set /Herwig/Particles/h0/h0->W+,W-;:OnOff 0
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff 0
set /Herwig/Particles/h0/h0->b,bbar;:OnOff 0
set /Herwig/Particles/h0/h0->c,cbar;:OnOff 0
set /Herwig/Particles/h0/h0->g,g;:OnOff 0
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff 1
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff 0
set /Herwig/Particles/h0/h0->t,tbar;:OnOff 0
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff 0

"""

topAlg.Herwigpp.Commands += cmds.splitlines()
