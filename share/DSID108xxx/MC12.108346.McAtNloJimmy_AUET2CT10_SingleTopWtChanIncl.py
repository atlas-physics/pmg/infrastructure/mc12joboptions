evgenConfig.description = "McAtNlo+fHerwig/Jimmy singtop Wt channel production with - CT10 PDF, AUET2 tune"
evgenConfig.generators = ["McAtNlo", "Herwig"]
evgenConfig.keywords = ["top", "singletop","WtChan"]
evgenConfig.contact  = ["cunfeng.feng@cern.ch"]
evgenConfig.inputfilecheck = "SingleTopWtChanIncl"

include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand+=[ "iproc mcatnlo" ]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
