evgenConfig.description = "High-pT inelastic minimum bias events for pile-up, with the A2 CTEQ6L1 tune"
evgenConfig.keywords = ["QCD", "minbias", "pileup"]

include("MC12JobOptions/Pythia8_A2_CTEQ6L1_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += \
    ["SoftQCD:minBias = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]

include("MC12JobOptions/JetFilter_MinbiasHigh.py")
evgenConfig.minevents = 2000
