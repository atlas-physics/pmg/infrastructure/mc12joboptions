###############################################################
#
# Job options file for GGM SUSY in Dilepton Case
#
#==============================================================

#The default tune for Pythia samples (for MC12)
include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )

slha_file = "higgsino_M3_1100_mu_400.slha"
topAlg.Pythia.SusyInputFile = slha_file

topAlg.Pythia.PythiaCommand += ["pysubs msel 0",  # !
                                "pymssm imss 1 11", # Select SLHA
                                "pymssm imss 11 1", ## make gravitino NLSP
                                "pymssm rmss 21 2275.",  ## Gravitino mass
                                "pysubs msub 243 1",  # gluino pair production
                                "pysubs msub 244 1"  # gluino pair production 
                               ]

topAlg.Pythia.PythiaCommand += ["pypars mstp 95 0", # Switch off Pythia color strgins, they don't work correctly for late decaying particles!
                                "pydat1 mstj 22 3", # allow long decay length !
                                "pydat1 parj 72 100000." # max length set to 100m
                                ]

topAlg.Pythia.PythiaCommand += ["pydat3 mdme 2164 1 0", # switch explicetly off neutralino decay to Higgs
                                "pydat3 mdme 174 1 0", # switch explicetly off Z decays to quarks
                                "pydat3 mdme 175 1 0", # switch explicetly off Z decays to quarks
                                "pydat3 mdme 176 1 0", # switch explicetly off Z decays to quarks
                                "pydat3 mdme 177 1 0", # switch explicetly off Z decays to quarks
                                "pydat3 mdme 178 1 0", # switch explicetly off Z decays to quarks
                                "pydat3 mdme 179 1 0", # switch explicetly off Z decays to quarks
                                "pydat3 mdme 180 1 0", # switch explicetly off Z decays to quarks
                                "pydat3 mdme 181 1 0", # switch explicetly off Z decays to quarks            
                                #"pydat3 mdme 182 1 0", # switch explicetly off Z decays to electrons
                                "pydat3 mdme 183 1 0", # switch explicetly off Z decays to neutrinos
                                #"pydat3 mdme 184 1 0", # switch explicetly off Z decays to muons
                                "pydat3 mdme 185 1 0", # switch explicetly off Z decays to neutrinos
                                #"pydat3 mdme 186 1 0", # switch explicetly off Z decays to taus
                                "pydat3 mdme 187 1 0" # switch explicetly off Z decays to neutrinos
                                ]

TestHepMC = Algorithm("TestHepMC")
TestHepMC.MaxTransVtxDisp = 100000.
TestHepMC.MaxVtxDisp = 100000.

from GeneratorFilters.GeneratorFiltersConf import DecayLengthFilter
topAlg += DecayLengthFilter()
topAlg.DecayLengthFilter.Rmin=0.
topAlg.DecayLengthFilter.Rmax=300.
topAlg.DecayLengthFilter.Zmin=0.
topAlg.DecayLengthFilter.Zmax=1000.
topAlg.DecayLengthFilter.nDVreq=2
StreamEVGEN.RequireAlgs += ["DecayLengthFilter"]


evgenConfig.contact = ['nathan.rogers.bernard@cern.ch']
evgenConfig.description = "Non-prompt GGM higgsino signal"
evgenConfig.keywords = ["SUSY", "GGM"]
evgenConfig.generators += ["Pythia"]
evgenConfig.auxfiles += [ slha_file ]
