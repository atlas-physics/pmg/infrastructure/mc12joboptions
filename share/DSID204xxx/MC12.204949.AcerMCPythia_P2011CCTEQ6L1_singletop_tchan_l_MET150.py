evgenConfig.description = "AcerMCPythia6 singletop-tchan-l production with P2011C CTEQ6L1 tune and MET 150 GeV filter"
evgenConfig.generators = ["AcerMC", "Pythia"]
evgenConfig.keywords = ["top", "singletop", "tchan", "leptonic"]
evgenConfig.contact  = ["federico.meloni@cern.ch"]
evgenConfig.inputfilecheck = "singletop_tchan_lept"
evgenConfig.minevents = 4000

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")
topAlg.Pythia.PythiaCommand += ["pyinit user acermc"]
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

## Include METFilter
include ( 'MC12JobOptions/METFilter.py' )
topAlg.METFilter.MissingEtCut = 150*GeV
