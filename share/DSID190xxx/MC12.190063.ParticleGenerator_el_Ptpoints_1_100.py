evgenConfig.description = "Single electrons with constant pt 1 2 5 7 10 15 20 30 50 100 GeV"
evgenConfig.keywords = ["el","singleparticle"]
include("MC12JobOptions/ParticleGenerator_Common.py")

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 3

ParticleGenerator.orders = [
 "PDGcode: sequence -11 11 -11 11 11 -11 11 -11 11 -11 11 -11 11 -11 11 -11 11 -11 11 -11",
 "pt: sequence 1000 2000 5000 7000 10000 15000 20000 30000 50000 100000",
 "eta: flat -4.0 4.0",
 "phi: flat -3.14159 3.14159"
 ]

#==============================================================
#
# End of job options file
#
###############################################################
