#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19675100e+02   # h
        35     1.00710330e+03   # H
        36     1.00000000e+03   # A
        37     1.00966250e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99951050e+03   # b1
   1000006     2.83408420e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     2.12648800e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.33429800e+02   # N1
   1000023     2.91971000e+02   # N2
   1000024     2.86468000e+02   # C1
   1000025    -3.04098300e+02   # N3
   1000035     5.68697600e+02   # N4
   1000037     5.68657800e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00114700e+03   # b2
   2000006     3.16234130e+03   # t2
   2000011     2.12731500e+02   # eR
   2000013     2.12731600e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68791730e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07375822e-01   # O_{11}
  1  2    -7.06837638e-01   # O_{12}
  2  1     7.06837638e-01   # O_{21}
  2  2     7.07375822e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.00851298e-01   # O_{11}
  1  2     7.99360818e-01   # O_{12}
  2  1    -7.99360818e-01   # O_{21}
  2  2     6.00851298e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.72967390e-01   # 
  1  2    -2.79202900e-02   # 
  1  3     1.99230850e-01   # 
  1  4    -1.13409360e-01   # 
  2  1    -2.22216930e-01   # 
  2  2    -2.38125820e-01   # 
  2  3     6.74994290e-01   # 
  2  4    -6.62041430e-01   # 
  3  1     5.84288400e-02   # 
  3  2    -5.52757000e-02   # 
  3  3    -6.98089840e-01   # 
  3  4    -7.11478110e-01   # 
  4  1     2.32346300e-02   # 
  4  2    -9.69258130e-01   # 
  4  3    -1.31759260e-01   # 
  4  4     2.06491040e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -1.85300990e-01   # Umix_{11}
  1  2     9.82681810e-01   # Umix_{12}
  2  1    -9.82681810e-01   # Umix_{21}
  2  2    -1.85300990e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -2.91625500e-01   # Vmix_{11}
  1  2     9.56532600e-01   # Vmix_{12}
  2  1    -9.56532600e-01   # Vmix_{21}
  2  2    -2.91625500e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     3.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.40000000e+02   # M1
     2     5.50000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     2.08400000e+02   # m_eR
    35     2.08400000e+02   # m_muR
    36     2.08500000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     1.80000000e+03   # A_e
  2  2     1.80000000e+03   # A_mu
  3  3     1.80000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.57212634e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.49705800e-02    2      1000022         1                       
      1.35201090e-02    2      1000023         1                       
      1.50781330e-03    2      1000025         1                       
      3.11455970e-01    2      1000035         1                       
      2.38407120e-02    2     -1000024         2                       
      6.34704890e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.57814624e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.94330050e-03    2      1000022         2                       
      2.69178060e-02    2      1000023         2                       
      6.86656680e-04    2      1000025         2                       
      3.05396880e-01    2      1000035         2                       
      5.89289070e-02    2      1000024         1                       
      6.00126500e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.57212634e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.49705840e-02    2      1000022         3                       
      1.35201110e-02    2      1000023         3                       
      1.50781360e-03    2      1000025         3                       
      3.11456050e-01    2      1000035         3                       
      2.38407090e-02    2     -1000024         4                       
      6.34704830e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.57814624e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.94329680e-03    2      1000022         4                       
      2.69177950e-02    2      1000023         4                       
      6.86656390e-04    2      1000025         4                       
      3.05396770e-01    2      1000035         4                       
      5.89289070e-02    2      1000024         3                       
      6.00126560e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.01954308e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.97349760e-02    2      1000022         5                       
      2.09399440e-02    2      1000023         5                       
      3.83443830e-03    2      1000025         5                       
      1.23690260e-01    2      1000035         5                       
      4.26378340e-01    2     -1000024         6                       
      2.88917960e-01    2     -1000037         6                       
      8.65040650e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.96345824e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.26681210e-02    2      1000022         6                       
      2.00041170e-01    2      1000023         6                       
      2.22750830e-01    2      1000025         6                       
      1.48137230e-01    2      1000035         6                       
      1.26614420e-01    2      1000024         5                       
      2.69788300e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.68734619e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.47453620e-01    2      1000022         1                       
      4.86821380e-02    2      1000023         1                       
      3.36020160e-03    2      1000025         1                       
      5.04158900e-04    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.74869271e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.47453680e-01    2      1000022         2                       
      4.86819970e-02    2      1000023         2                       
      3.36019040e-03    2      1000025         2                       
      5.04151740e-04    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.68734619e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.47453620e-01    2      1000022         3                       
      4.86821380e-02    2      1000023         3                       
      3.36020160e-03    2      1000025         3                       
      5.04158900e-04    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.74869271e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.47453680e-01    2      1000022         4                       
      4.86819970e-02    2      1000023         4                       
      3.36019070e-03    2      1000025         4                       
      5.04151740e-04    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  5.15871150e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.29989800e-02    2      1000022         5                       
      7.26852850e-03    2      1000023         5                       
      8.06665330e-03    2      1000025         5                       
      1.43599440e-01    2      1000035         5                       
      4.04849500e-01    2     -1000024         6                       
      3.30154420e-01    2     -1000037         6                       
      9.30625350e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.04081342e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.63832150e-01    2      1000024         5                       
      4.39886860e-02    2      1000037         5                       
      1.69247180e-01    2           23   1000006                       
      2.36745680e-02    2           24   1000005                       
      4.02824280e-02    2           24   2000005                       
      5.20816180e-02    2      1000022         6                       
      1.79360430e-01    2      1000023         6                       
      1.98013720e-01    2      1000025         6                       
      2.95193050e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  3.91064108e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.26322880e-02    2      1000022        11                       
      4.11236990e-02    2      1000023        11                       
      1.69708920e-04    2      1000025        11                       
      2.74600450e-01    2      1000035        11                       
      2.17747580e-02    2     -1000024        12                       
      5.79699100e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  3.91389665e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.01727780e-01    2      1000022        12                       
      4.25802260e-03    2      1000023        12                       
      2.41309520e-03    2      1000025        12                       
      2.89119900e-01    2      1000035        12                       
      5.38707230e-02    2      1000024        11                       
      5.48610390e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  3.91064108e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.26322880e-02    2      1000022        13                       
      4.11236990e-02    2      1000023        13                       
      1.69708920e-04    2      1000025        13                       
      2.74600450e-01    2      1000035        13                       
      2.17747580e-02    2     -1000024        14                       
      5.79699100e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  3.91389665e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.01727780e-01    2      1000022        14                       
      4.25802260e-03    2      1000023        14                       
      2.41309520e-03    2      1000025        14                       
      2.89119900e-01    2      1000035        14                       
      5.38707230e-02    2      1000024        13                       
      5.48610390e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  3.76135779e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  3.94273392e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00979090e-01    2      1000022        16                       
      4.22668460e-03    2      1000023        16                       
      2.39533540e-03    2      1000025        16                       
      2.86992040e-01    2      1000035        16                       
      5.87666150e-02    2      1000024        15                       
      5.44755220e-01    2      1000037        15                       
      1.88491770e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  3.76889601e-01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  3.76889601e-01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  3.93966601e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.22447690e-02    2      1000022        15                       
      4.33181380e-02    2      1000023        15                       
      2.83786930e-03    2      1000025        15                       
      2.72669110e-01    2      1000035        15                       
      2.16142760e-02    2     -1000024        16                       
      5.75427000e-01    2     -1000037        16                       
      9.45300680e-04    2           36   1000015                       
      9.43569990e-04    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  2.04753313e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.66275150e-04    3      1000024         1        -2             
      6.66275150e-04    3     -1000024         2        -1             
      6.66275150e-04    3      1000024         3        -4             
      6.66275150e-04    3     -1000024         4        -3             
      8.81943630e-02    3      1000024         5        -6             
      8.81943630e-02    3     -1000024         6        -5             
      1.68696490e-02    3      1000037         1        -2             
      1.68696490e-02    3     -1000037         2        -1             
      1.68696490e-02    3      1000037         3        -4             
      1.68696490e-02    3     -1000037         4        -3             
      1.14198120e-01    3      1000037         5        -6             
      1.14198120e-01    3     -1000037         6        -5             
      8.76686390e-05    2      1000022        21                       
      3.50968420e-03    3      1000022         1        -1             
      3.50968420e-03    3      1000022         3        -3             
      3.53553380e-03    3      1000022         5        -5             
      2.36300640e-02    3      1000022         6        -6             
      9.57028650e-04    2      1000023        21                       
      9.41644070e-04    3      1000023         1        -1             
      9.41644070e-04    3      1000023         3        -3             
      1.80901250e-03    3      1000023         5        -5             
      1.54709640e-01    3      1000023         6        -6             
      1.12393740e-03    2      1000025        21                       
      8.67892740e-05    3      1000025         1        -1             
      8.67892740e-05    3      1000025         3        -3             
      1.04379250e-03    3      1000025         5        -5             
      1.71550050e-01    3      1000025         6        -6             
      1.84199830e-02    3      1000035         1        -1             
      1.84199830e-02    3      1000035         3        -3             
      1.83229110e-02    3      1000035         5        -5             
      1.02385400e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  1.88439406e-01   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.63691470e-02    2      1000022        23                       
      6.36751000e-01    2      1000022        25                       
      4.28234380e-02    2      2000011       -11                       
      4.28234380e-02    2     -2000011        11                       
      4.28233780e-02    2      2000013       -13                       
      4.28233780e-02    2     -2000013        13                       
      4.77931690e-02    2      1000015       -15                       
      4.77931690e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  1.65082391e-01   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.17156160e-01    2      1000022        23                       
      4.00509390e-02    2      1000022        25                       
      4.17017840e-03    2      2000011       -11                       
      4.17017840e-03    2     -2000011        11                       
      4.17017380e-03    2      2000013       -13                       
      4.17017380e-03    2     -2000013        13                       
      1.30561590e-02    2      1000015       -15                       
      1.30561590e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  4.63325355e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.73542820e-01    2      1000024       -24                       
      2.73542820e-01    2     -1000024        24                       
      9.18660660e-05    2      1000022        23                       
      1.34760710e-02    2      1000023        23                       
      2.28649170e-01    2      1000025        23                       
      8.39770660e-05    2      1000022        25                       
      2.01143430e-01    2      1000023        25                       
      8.60882640e-03    2      1000025        25                       
      1.24655750e-04    2      2000011       -11                       
      1.24655750e-04    2     -2000011        11                       
      1.24655740e-04    2      2000013       -13                       
      1.24655740e-04    2     -2000013        13                       
      1.81342050e-04    2      1000015       -15                       
      1.81342050e-04    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  1.42261223e-01   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.85608220e-01    2      1000022        24                       
      1.43917050e-02    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  4.63064584e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.72761420e-04    2      1000022        24                       
      2.73580190e-01    2      1000023        24                       
      2.50655800e-01    2      1000025        24                       
      1.14546970e-04    2     -1000015        16                       
      2.63532100e-01    2      1000024        23                       
      2.11844740e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56101448e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96585130e-04    2           13       -13                       
      5.62307830e-02    2           15       -15                       
      2.35978230e-03    2            3        -3                       
      7.69229530e-01    2            5        -5                       
      3.34438530e-02    2            4        -4                       
      1.90456110e-03    2           22        22                       
      3.96412310e-02    2           21        21                       
      4.92932930e-03    3           24        11       -12             
      4.92932930e-03    3           24        13       -14             
      4.92932930e-03    3           24        15       -16             
      1.47879890e-02    3           24        -2         1             
      1.47879890e-02    3           24        -4         3             
      4.92932930e-03    3          -24       -11        12             
      4.92932930e-03    3          -24       -13        14             
      4.92932930e-03    3          -24       -15        16             
      1.47879890e-02    3          -24         2        -1             
      1.47879890e-02    3          -24         4        -3             
      5.65286140e-04    3           23        12       -12             
      5.65286140e-04    3           23        14       -14             
      5.65286140e-04    3           23        16       -16             
      2.84502860e-04    3           23        11       -11             
      2.84502860e-04    3           23        13       -13             
      2.84502860e-04    3           23        15       -15             
      9.74682680e-04    3           23         2        -2             
      9.74682680e-04    3           23         4        -4             
      1.25563130e-03    3           23         1        -1             
      1.25563130e-03    3           23         3        -3             
      1.25563130e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  7.90061217e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.28855200e-05    2           13       -13                       
      9.41874180e-03    2           15       -15                       
      3.85513790e-04    2            3        -3                       
      9.50628970e-02    2            5        -5                       
      1.58226730e-01    2            6        -6                       
      8.33247690e-05    2           21        21                       
      5.37384240e-04    2           24       -24                       
      2.72841190e-04    2           23        23                       
      9.58081430e-03    2      1000022   1000022                       
      4.35980040e-02    2      1000022   1000023                       
      8.91451170e-02    2      1000022   1000025                       
      2.59266700e-03    2      1000022   1000035                       
      2.39807690e-03    2      1000023   1000023                       
      8.85538930e-04    2      1000023   1000025                       
      2.05377250e-02    2      1000023   1000035                       
      2.70720220e-03    2      1000025   1000025                       
      1.54478060e-01    2      1000025   1000035                       
      3.81254180e-02    2      1000024  -1000024                       
      1.84809680e-01    2      1000024  -1000037                       
      1.84809680e-01    2     -1000024   1000037                       
      2.13281650e-03    2           25        25                       
      6.08076410e-05    2      2000011  -2000011                       
      6.07950970e-05    2      2000013  -2000013                       
      5.72696920e-05    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  7.67983198e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.36336340e-05    2           13       -13                       
      9.63312760e-03    2           15       -15                       
      3.94306930e-04    2            3        -3                       
      9.73167940e-02    2            5        -5                       
      1.72060270e-01    2            6        -6                       
      1.02396170e-04    2           21        21                       
      1.54849890e-02    2      1000022   1000022                       
      1.01395880e-01    2      1000022   1000023                       
      3.60113790e-02    2      1000022   1000025                       
      4.03742860e-03    2      1000022   1000035                       
      7.18433220e-03    2      1000023   1000023                       
      1.36234480e-01    2      1000023   1000035                       
      2.17133880e-03    2      1000025   1000025                       
      2.03674290e-02    2      1000025   1000035                       
      8.73533260e-02    2      1000024  -1000024                       
      1.54843700e-01    2      1000024  -1000037                       
      1.54843700e-01    2     -1000024   1000037                       
      5.31532340e-04    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  7.61620439e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.42421990e-05    2           14       -13                       
      9.80743020e-03    2           16       -15                       
      3.72159180e-04    2            4        -3                       
      2.42550730e-01    2            6        -5                       
      1.40327860e-01    2      1000024   1000022                       
      1.20323850e-02    2      1000024   1000023                       
      1.75858380e-02    2      1000024   1000025                       
      1.91888120e-01    2      1000024   1000035                       
      7.85879700e-03    2      1000037   1000022                       
      1.99524660e-01    2      1000037   1000023                       
      1.77469460e-01    2      1000037   1000025                       
      5.48327630e-04    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
