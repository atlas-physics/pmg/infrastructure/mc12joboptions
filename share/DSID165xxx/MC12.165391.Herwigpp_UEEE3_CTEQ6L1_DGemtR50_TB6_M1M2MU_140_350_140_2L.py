## SUSY Herwig++ jobOptions for pMSSM DirectGaugino (Direct Neutralino/Chargino/Slepton)  

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.MSUGRA')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_DGemtR50_TB6_M1M2MU_140_350_140.slha' 

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['neutralinos','charginos','sleptons','staus'], slha_file) 

# define metadata
evgenConfig.description = "ElWeak production, slha file: susy_DGemtR50_TB6_M1M2MU_140_350_140.slha"
evgenConfig.keywords = ['SUSY','MSSM','pMSSM','DirectGaugino','DGemt','DGemtR','DGemtR50','chargino','neutralino','slepton']
evgenConfig.contact  = ['borge.gjelsten@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')


# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines() 


# Generator Filter
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()

MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 2
MultiElecMuTauFilter.MinPt = 5000.
MultiElecMuTauFilter.MaxEta = 6.
MultiElecMuTauFilter.MinVisPtHadTau = 5000.  # pt-cut on the visible hadronic tau
MultiElecMuTauFilter.IncludeHadTaus = 1      # include hadronic taus 

StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]


# clean up
del cmds
