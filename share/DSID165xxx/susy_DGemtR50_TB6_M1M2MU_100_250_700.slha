#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19707800e+02   # h
        35     1.00709060e+03   # H
        36     1.00000000e+03   # A
        37     1.00966130e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99848220e+03   # b1
   1000006     2.83566970e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     1.70793700e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     9.86356000e+01   # N1
   1000023     2.43048200e+02   # N2
   1000024     2.42970500e+02   # C1
   1000025    -7.03128100e+02   # N3
   1000035     7.11444200e+02   # N4
   1000037     7.11618700e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00217430e+03   # b2
   2000006     3.16091970e+03   # t2
   2000011     1.70825300e+02   # eR
   2000013     1.70825400e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68784960e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07378013e-01   # O_{11}
  1  2    -7.06835445e-01   # O_{12}
  2  1     7.06835445e-01   # O_{21}
  2  2     7.07378013e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.62283226e-01   # O_{11}
  1  2     7.49253581e-01   # O_{12}
  2  1    -7.49253581e-01   # O_{21}
  2  2     6.62283226e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.97429730e-01   # 
  1  2    -1.63580900e-02   # 
  1  3     6.68096600e-02   # 
  1  4    -2.00774900e-02   # 
  2  1    -2.64083400e-02   # 
  2  2    -9.88689060e-01   # 
  2  3     1.32798420e-01   # 
  2  4    -6.45090700e-02   # 
  3  1    -3.20741700e-02   # 
  3  2     4.91727400e-02   # 
  3  3     7.03680750e-01   # 
  3  4     7.08086850e-01   # 
  4  1    -5.83787200e-02   # 
  4  2     1.40743450e-01   # 
  4  3     6.94791260e-01   # 
  4  4    -7.02885990e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -9.81896160e-01   # Umix_{11}
  1  2     1.89419940e-01   # Umix_{12}
  2  1    -1.89419940e-01   # Umix_{21}
  2  2    -9.81896160e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -9.95727060e-01   # Vmix_{11}
  1  2     9.23449000e-02   # Vmix_{12}
  2  1    -9.23449000e-02   # Vmix_{21}
  2  2    -9.95727060e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     7.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.00000000e+02   # M1
     2     2.50000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     1.65400000e+02   # m_eR
    35     1.65400000e+02   # m_muR
    36     1.65600000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     4.20000000e+03   # A_e
  2  2     4.20000000e+03   # A_mu
  3  3     4.20000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.76997537e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.32508140e-02    2      1000022         1                       
      3.20040670e-01    2      1000023         1                       
      9.06791010e-04    2      1000025         1                       
      6.84355660e-03    2      1000035         1                       
      6.37545820e-01    2     -1000024         2                       
      2.14123020e-02    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.77603121e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.23579650e-03    2      1000022         2                       
      3.25731840e-01    2      1000023         2                       
      5.59807990e-04    2      1000025         2                       
      5.03660830e-03    2      1000035         2                       
      6.54357190e-01    2      1000024         1                       
      5.07879720e-03    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.76997537e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.32508170e-02    2      1000022         3                       
      3.20040730e-01    2      1000023         3                       
      9.06791190e-04    2      1000025         3                       
      6.84355800e-03    2      1000035         3                       
      6.37545760e-01    2     -1000024         4                       
      2.14122980e-02    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.77603121e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.23579280e-03    2      1000022         4                       
      3.25731780e-01    2      1000023         4                       
      5.59807810e-04    2      1000025         4                       
      5.03660650e-03    2      1000035         4                       
      6.54357250e-01    2      1000024         3                       
      5.07879720e-03    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.53282164e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.56291570e-02    2      1000022         5                       
      1.58914800e-01    2      1000023         5                       
      3.69541390e-03    2      1000025         5                       
      1.79457110e-03    2      1000035         5                       
      3.17714180e-01    2     -1000024         6                       
      3.97788820e-01    2     -1000037         6                       
      8.44630150e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.60299753e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.69125470e-02    2      1000022         6                       
      7.03803080e-02    2      1000023         6                       
      2.09260170e-01    2      1000025         6                       
      2.67101500e-01    2      1000035         6                       
      1.39680880e-01    2      1000024         5                       
      2.66664560e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.69098757e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.95345120e-01    2      1000022         1                       
      6.90098850e-04    2      1000023         1                       
      9.21273780e-04    2      1000025         1                       
      3.04358150e-03    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.76318575e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.95345180e-01    2      1000022         2                       
      6.90097400e-04    2      1000023         2                       
      9.21252530e-04    2      1000025         2                       
      3.04350930e-03    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.69098757e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.95345120e-01    2      1000022         3                       
      6.90098850e-04    2      1000023         3                       
      9.21273780e-04    2      1000025         3                       
      3.04358150e-03    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.76318575e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.95345180e-01    2      1000022         4                       
      6.90097400e-04    2      1000023         4                       
      9.21252650e-04    2      1000025         4                       
      3.04350930e-03    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.49038068e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.06702910e-02    2      1000022         5                       
      1.44061880e-01    2      1000023         5                       
      7.63502390e-03    2      1000025         5                       
      1.39428960e-02    2      1000035         5                       
      2.87749500e-01    2     -1000024         6                       
      4.33203010e-01    2     -1000037         6                       
      9.27373690e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  9.95191872e+01   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.61815270e-01    2      1000024         5                       
      1.49359720e-01    2      1000037         5                       
      1.72032240e-01    2           23   1000006                       
      2.97988110e-02    2           24   1000005                       
      3.48466900e-02    2           24   2000005                       
      4.00508310e-02    2      1000022         6                       
      8.02859740e-02    2      1000023         6                       
      1.88344000e-01    2      1000025         6                       
      1.43466560e-01    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.10938378e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.67443310e-02    2      1000022        11                       
      3.05253950e-01    2      1000023        11                       
      2.73133920e-04    2      1000025        11                       
      3.23187910e-03    2      1000035        11                       
      5.84854360e-01    2     -1000024        12                       
      1.96423550e-02    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.11246485e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.76374000e-02    2      1000022        12                       
      2.87525950e-01    2      1000023        12                       
      1.22349070e-03    2      1000025        12                       
      8.16701820e-03    2      1000035        12                       
      6.00783230e-01    2      1000024        11                       
      4.66291560e-03    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.10938378e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.67443310e-02    2      1000022        13                       
      3.05253950e-01    2      1000023        13                       
      2.73133920e-04    2      1000025        13                       
      3.23187910e-03    2      1000035        13                       
      5.84854360e-01    2     -1000024        14                       
      1.96423550e-02    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.11246485e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.76374000e-02    2      1000022        14                       
      2.87525950e-01    2      1000023        14                       
      1.22349070e-03    2      1000025        14                       
      8.16701820e-03    2      1000035        14                       
      6.00783230e-01    2      1000024        13                       
      4.66291560e-03    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  3.83611143e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.17268924e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.62274300e-02    2      1000022        16                       
      2.83373830e-01    2      1000023        16                       
      1.20582250e-03    2      1000025        16                       
      8.04907920e-03    2      1000035        16                       
      5.92291830e-01    2      1000024        15                       
      9.13033630e-03    2      1000037        15                       
      9.72158550e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  3.84103641e-01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  3.84103641e-01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.16978144e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.55130850e-02    2      1000022        15                       
      3.00928950e-01    2      1000023        15                       
      2.60562430e-03    2      1000025        15                       
      5.45866970e-03    2      1000035        15                       
      5.76394140e-01    2     -1000024        16                       
      1.93582330e-02    2     -1000037        16                       
      4.87511140e-03    2           36   1000015                       
      4.86626570e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  1.81067921e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.13705130e-02    3      1000024         1        -2             
      2.13705130e-02    3     -1000024         2        -1             
      2.13705130e-02    3      1000024         3        -4             
      2.13705130e-02    3     -1000024         4        -3             
      7.14234410e-02    3      1000024         5        -6             
      7.14234410e-02    3     -1000024         6        -5             
      6.54675300e-04    3      1000037         1        -2             
      6.54675300e-04    3     -1000037         2        -1             
      6.54675300e-04    3      1000037         3        -4             
      6.54675300e-04    3     -1000037         4        -3             
      1.30080090e-01    3      1000037         5        -6             
      1.30080090e-01    3     -1000037         6        -5             
      3.06359800e-05    2      1000022        21                       
      4.03722520e-03    3      1000022         1        -1             
      4.03722520e-03    3      1000022         3        -3             
      4.00192240e-03    3      1000022         5        -5             
      3.16325310e-02    3      1000022         6        -6             
      1.13886200e-04    2      1000023        21                       
      2.26309630e-02    3      1000023         1        -1             
      2.26309630e-02    3      1000023         3        -3             
      2.24625670e-02    3      1000023         5        -5             
      4.89765520e-02    3      1000023         6        -6             
      1.14440010e-03    2      1000025        21                       
      5.04940000e-05    3      1000025         1        -1             
      5.04940000e-05    3      1000025         3        -3             
      9.63637370e-04    3      1000025         5        -5             
      1.51805530e-01    3      1000025         6        -6             
      9.44083150e-04    2      1000035        21                       
      4.83513400e-04    3      1000035         1        -1             
      4.83513400e-04    3      1000035         3        -3             
      1.37339040e-03    3      1000035         5        -5             
      1.91038670e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  3.33738972e-03   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.92080130e-02    2      1000022        23                       
      7.20479130e-01    2      1000022        25                       
      3.30713990e-02    2      2000011       -11                       
      3.30713990e-02    2     -2000011        11                       
      3.30713500e-02    2      2000013       -13                       
      3.30713500e-02    2     -2000013        13                       
      4.40137270e-02    2      1000015       -15                       
      4.40137270e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  5.50472527e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.04942790e-01    2      1000024       -24                       
      3.04942790e-01    2     -1000024        24                       
      7.24081990e-02    2      1000022        23                       
      2.62654070e-01    2      1000023        23                       
      1.96835990e-02    2      1000022        25                       
      2.94637210e-02    2      1000023        25                       
      2.95897570e-04    2      2000011       -11                       
      2.95897570e-04    2     -2000011        11                       
      2.95897570e-04    2      2000013       -13                       
      2.95897570e-04    2     -2000013        13                       
      2.36062590e-03    2      1000015       -15                       
      2.36062590e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  5.50979407e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.11659310e-01    2      1000024       -24                       
      3.11659310e-01    2     -1000024        24                       
      2.14942560e-02    2      1000022        23                       
      3.40296030e-02    2      1000023        23                       
      6.68982190e-02    2      1000022        25                       
      2.44262460e-01    2      1000023        25                       
      9.93758440e-04    2      2000011       -11                       
      9.93758440e-04    2     -2000011        11                       
      9.93758440e-04    2      2000013       -13                       
      9.93758440e-04    2     -2000013        13                       
      3.01105410e-03    2      1000015       -15                       
      3.01105410e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  3.31837661e-03   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.75315330e-01    2      1000022        24                       
      2.46846940e-02    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  5.47450719e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.16520450e-02    2      1000022        24                       
      3.20314910e-01    2      1000023        24                       
      4.08645110e-03    2     -1000015        16                       
      3.10854520e-01    2      1000024        23                       
      2.83092170e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56354434e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96512770e-04    2           13       -13                       
      5.62101300e-02    2           15       -15                       
      2.35890410e-03    2            3        -3                       
      7.68913390e-01    2            5        -5                       
      3.34327440e-02    2            4        -4                       
      1.88943230e-03    2           22        22                       
      3.96523210e-02    2           21        21                       
      4.94670080e-03    3           24        11       -12             
      4.94670080e-03    3           24        13       -14             
      4.94670080e-03    3           24        15       -16             
      1.48401030e-02    3           24        -2         1             
      1.48401030e-02    3           24        -4         3             
      4.94670080e-03    3          -24       -11        12             
      4.94670080e-03    3          -24       -13        14             
      4.94670080e-03    3          -24       -15        16             
      1.48401030e-02    3          -24         2        -1             
      1.48401030e-02    3          -24         4        -3             
      5.68033720e-04    3           23        12       -12             
      5.68033720e-04    3           23        14       -14             
      5.68033720e-04    3           23        16       -16             
      2.85885680e-04    3           23        11       -11             
      2.85885680e-04    3           23        13       -13             
      2.85885680e-04    3           23        15       -15             
      9.79420030e-04    3           23         2        -2             
      9.79420030e-04    3           23         4        -4             
      1.26173440e-03    3           23         1        -1             
      1.26173440e-03    3           23         3        -3             
      1.26173440e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  4.72607166e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.49735450e-05    2           13       -13                       
      1.57449730e-02    2           15       -15                       
      6.44449550e-04    2            3        -3                       
      1.58913460e-01    2            5        -5                       
      2.64479670e-01    2            6        -6                       
      1.39280600e-04    2           21        21                       
      8.94966940e-04    2           24       -24                       
      4.54393300e-04    2           23        23                       
      2.10535320e-03    2      1000022   1000022                       
      1.22321290e-02    2      1000022   1000023                       
      5.73955740e-02    2      1000022   1000025                       
      1.42727100e-02    2      1000022   1000035                       
      1.64887440e-02    2      1000023   1000023                       
      1.32787970e-01    2      1000023   1000025                       
      7.27655920e-03    2      1000023   1000035                       
      3.51234560e-02    2      1000024  -1000024                       
      1.38579990e-01    2      1000024  -1000037                       
      1.38579990e-01    2     -1000024   1000037                       
      3.52105500e-03    2           25        25                       
      1.05496890e-04    2      2000011  -2000011                       
      1.05475110e-04    2      2000013  -2000013                       
      9.93522990e-05    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  4.64863338e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.55671070e-05    2           13       -13                       
      1.59151700e-02    2           15       -15                       
      6.51445940e-04    2            3        -3                       
      1.60779910e-01    2            5        -5                       
      2.84265760e-01    2            6        -6                       
      1.69171700e-04    2           21        21                       
      2.70998830e-03    2      1000022   1000022                       
      1.79035810e-02    2      1000022   1000023                       
      1.59183140e-02    2      1000022   1000025                       
      5.07677420e-02    2      1000022   1000035                       
      3.01142370e-02    2      1000023   1000023                       
      8.34518480e-03    2      1000023   1000025                       
      1.06376100e-01    2      1000023   1000035                       
      6.39167060e-02    2      1000024  -1000024                       
      1.20618120e-01    2      1000024  -1000037                       
      1.20618120e-01    2     -1000024   1000037                       
      8.74880120e-04    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  4.43889938e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.87527540e-05    2           14       -13                       
      1.68275870e-02    2           16       -15                       
      6.38550610e-04    2            4        -3                       
      4.16168450e-01    2            6        -5                       
      3.06372530e-02    2      1000024   1000022                       
      2.31976300e-05    2      1000024   1000023                       
      1.63182200e-01    2      1000024   1000025                       
      1.49794370e-01    2      1000024   1000035                       
      6.25280510e-02    2      1000037   1000022                       
      1.59204230e-01    2      1000037   1000023                       
      9.37304580e-04    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
