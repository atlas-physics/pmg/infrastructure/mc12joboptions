#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19693000e+02   # h
        35     1.00709770e+03   # H
        36     1.00000000e+03   # A
        37     1.00966200e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99900000e+03   # b1
   1000006     2.83487720e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     3.10724200e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.37429000e+02   # N1
   1000023     4.84059900e+02   # N2
   1000024     4.82307600e+02   # C1
   1000025    -5.02859300e+02   # N3
   1000035     7.21370400e+02   # N4
   1000037     7.21328700e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00165700e+03   # b2
   2000006     3.16163060e+03   # t2
   2000011     3.10749100e+02   # eR
   2000013     3.10749100e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68787850e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07377087e-01   # O_{11}
  1  2    -7.06836372e-01   # O_{12}
  2  1     7.06836372e-01   # O_{21}
  2  2     7.07377087e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.43902393e-01   # O_{11}
  1  2     7.65107645e-01   # O_{12}
  2  1    -7.65107645e-01   # O_{21}
  2  2     6.43902393e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.94110230e-01   # 
  1  2    -8.26621000e-03   # 
  1  3     9.95514700e-02   # 
  1  4    -4.20290700e-02   # 
  2  1    -9.87986900e-02   # 
  2  2    -2.86410480e-01   # 
  2  3     6.81623580e-01   # 
  2  4    -6.66031120e-01   # 
  3  1     4.01252200e-02   # 
  3  2    -3.90170300e-02   # 
  3  3    -7.03174890e-01   # 
  3  4    -7.08810990e-01   # 
  4  1     1.93401700e-02   # 
  4  2    -9.57276520e-01   # 
  4  3    -1.76136450e-01   # 
  4  4     2.28524890e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -2.48242570e-01   # Umix_{11}
  1  2     9.68697910e-01   # Umix_{12}
  2  1    -9.68697910e-01   # Umix_{21}
  2  2    -2.48242570e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -3.22733910e-01   # Vmix_{11}
  1  2     9.46489690e-01   # Vmix_{12}
  2  1    -9.46489690e-01   # Vmix_{21}
  2  2    -3.22733910e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     5.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.40000000e+02   # M1
     2     7.00000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     3.07800000e+02   # m_eR
    35     3.07800000e+02   # m_muR
    36     3.07900000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     3.00000000e+03   # A_e
  2  2     3.00000000e+03   # A_mu
  3  3     3.00000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.42153142e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.33250960e-02    2      1000022         1                       
      2.52090020e-02    2      1000023         1                       
      7.49693950e-04    2      1000025         1                       
      3.02523260e-01    2      1000035         1                       
      4.31750270e-02    2     -1000024         2                       
      6.15018010e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.42741096e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.10887160e-02    2      1000022         2                       
      3.24029510e-02    2      1000023         2                       
      3.48913980e-04    2      1000025         2                       
      2.97443570e-01    2      1000035         2                       
      7.28224590e-02    2      1000024         1                       
      5.85893390e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.42153142e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.33250990e-02    2      1000022         3                       
      2.52090080e-02    2      1000023         3                       
      7.49694130e-04    2      1000025         3                       
      3.02523310e-01    2      1000035         3                       
      4.31750160e-02    2     -1000024         4                       
      6.15017890e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.42741096e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.10887140e-02    2      1000022         4                       
      3.24029400e-02    2      1000023         4                       
      3.48913830e-04    2      1000025         4                       
      2.97443480e-01    2      1000035         4                       
      7.28224590e-02    2      1000024         3                       
      5.85893510e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.30024067e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.95540450e-02    2      1000022         5                       
      2.89015880e-02    2      1000023         5                       
      4.34356000e-03    2      1000025         5                       
      1.19092510e-01    2      1000035         5                       
      4.31619790e-01    2     -1000024         6                       
      2.88326290e-01    2     -1000037         6                       
      8.81622510e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.66410173e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.33053080e-02    2      1000022         6                       
      1.82420100e-01    2      1000023         6                       
      2.24673510e-01    2      1000025         6                       
      1.55315550e-01    2      1000035         6                       
      1.15104370e-01    2      1000024         5                       
      2.79181180e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.68734619e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.88834260e-01    2      1000022         1                       
      9.30400190e-03    2      1000023         1                       
      1.52813550e-03    2      1000025         1                       
      3.33634670e-04    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.74869271e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.88834380e-01    2      1000022         2                       
      9.30390790e-03    2      1000023         2                       
      1.52811900e-03    2      1000025         2                       
      3.33626670e-04    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.68734619e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.88834260e-01    2      1000022         3                       
      9.30400190e-03    2      1000023         3                       
      1.52813550e-03    2      1000025         3                       
      3.33634670e-04    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.74869271e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.88834380e-01    2      1000022         4                       
      9.30390880e-03    2      1000023         4                       
      1.52811890e-03    2      1000025         4                       
      3.33626610e-04    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.58101336e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.80890360e-02    2      1000022         5                       
      7.85394570e-03    2      1000023         5                       
      7.57665750e-03    2      1000025         5                       
      1.40788470e-01    2      1000035         5                       
      3.97595260e-01    2     -1000024         6                       
      3.32701300e-01    2     -1000037         6                       
      9.53953120e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.01342613e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.69575390e-01    2      1000024         5                       
      3.68954090e-02    2      1000037         5                       
      1.71368960e-01    2           23   1000006                       
      2.77896840e-02    2           24   1000005                       
      3.67863400e-02    2           24   2000005                       
      4.23664150e-02    2      1000022         6                       
      1.95067300e-01    2      1000023         6                       
      1.92599950e-01    2      1000025         6                       
      2.75506730e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  3.76006855e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.68514080e-02    2      1000022        11                       
      3.69865260e-02    2      1000023        11                       
      9.12960170e-05    2      1000025        11                       
      2.67197490e-01    2      1000035        11                       
      3.92842140e-02    2     -1000024        12                       
      5.59589030e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  3.76307815e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.02777610e-01    2      1000022        12                       
      1.71452980e-02    2      1000023        12                       
      1.18191660e-03    2      1000025        12                       
      2.78995540e-01    2      1000035        12                       
      6.63207470e-02    2      1000024        11                       
      5.33578930e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  3.76006855e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.68514080e-02    2      1000022        13                       
      3.69865260e-02    2      1000023        13                       
      9.12960170e-05    2      1000025        13                       
      2.67197490e-01    2      1000035        13                       
      3.92842140e-02    2     -1000024        14                       
      5.59589030e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  3.76307815e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.02777610e-01    2      1000022        14                       
      1.71452980e-02    2      1000023        14                       
      1.18191660e-03    2      1000025        14                       
      2.78995540e-01    2      1000035        14                       
      6.63207470e-02    2      1000024        13                       
      5.33578930e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  1.01045457e+00   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  3.80440437e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.01662050e-01    2      1000022        16                       
      1.69592030e-02    2      1000023        16                       
      1.16908790e-03    2      1000025        16                       
      2.75967300e-01    2      1000035        16                       
      7.07488660e-02    2      1000024        15                       
      5.28111460e-01    2      1000037        15                       
      5.38206710e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  1.01071834e+00   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  1.01071834e+00   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  3.80154788e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.58558020e-02    2      1000022        15                       
      3.91349830e-02    2      1000023        15                       
      2.79708600e-03    2      1000025        15                       
      2.64456030e-01    2      1000035        15                       
      3.88571250e-02    2     -1000024        16                       
      5.53505600e-01    2     -1000037        16                       
      2.69929550e-03    2           36   1000015                       
      2.69424610e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  1.86443078e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.22976480e-03    3      1000024         1        -2             
      1.22976480e-03    3     -1000024         2        -1             
      1.22976480e-03    3      1000024         3        -4             
      1.22976480e-03    3     -1000024         4        -3             
      8.43828920e-02    3      1000024         5        -6             
      8.43828920e-02    3     -1000024         6        -5             
      1.65277720e-02    3      1000037         1        -2             
      1.65277720e-02    3     -1000037         2        -1             
      1.65277720e-02    3      1000037         3        -4             
      1.65277720e-02    3     -1000037         4        -3             
      1.17383960e-01    3      1000037         5        -6             
      1.17383960e-01    3     -1000037         6        -5             
      4.01578760e-05    2      1000022        21                       
      3.84149420e-03    3      1000022         1        -1             
      3.84149420e-03    3      1000022         3        -3             
      3.81627770e-03    3      1000022         5        -5             
      3.01721100e-02    3      1000022         6        -6             
      1.05872980e-03    2      1000023        21                       
      1.60686040e-03    3      1000023         1        -1             
      1.60686040e-03    3      1000023         3        -3             
      2.50231920e-03    3      1000023         5        -5             
      1.42490060e-01    3      1000023         6        -6             
      1.16876910e-03    2      1000025        21                       
      4.20531140e-05    3      1000025         1        -1             
      4.20531140e-05    3      1000025         3        -3             
      1.02968190e-03    3      1000025         5        -5             
      1.70361410e-01    3      1000025         6        -6             
      1.86380260e-05    2      1000035        21                       
      1.84374810e-02    3      1000035         1        -1             
      1.84374810e-02    3      1000035         3        -3             
      1.83547080e-02    3      1000035         5        -5             
      1.06567430e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  3.46038589e-01   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.41873080e-01    2      1000022        23                       
      7.70113950e-01    2      1000022        25                       
      1.20013090e-02    2      2000011       -11                       
      1.20013090e-02    2     -2000011        11                       
      1.20013040e-02    2      2000013       -13                       
      1.20013040e-02    2     -2000013        13                       
      2.00039130e-02    2      1000015       -15                       
      2.00039130e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  3.66562709e-01   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.43953430e-01    2      1000022        23                       
      1.23928470e-01    2      1000022        25                       
      2.14609340e-03    2      2000011       -11                       
      2.14609340e-03    2     -2000011        11                       
      2.14609290e-03    2      2000013       -13                       
      2.14609290e-03    2     -2000013        13                       
      1.17669800e-02    2      1000015       -15                       
      1.17669800e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  4.69874358e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.83071040e-01    2      1000024       -24                       
      2.83071040e-01    2     -1000024        24                       
      1.50974330e-03    2      1000022        23                       
      5.36038820e-03    2      1000023        23                       
      2.21247300e-01    2      1000025        23                       
      3.03556160e-03    2      1000022        25                       
      1.99242500e-01    2      1000023        25                       
      2.65190700e-03    2      1000025        25                       
      9.68641910e-05    2      2000011       -11                       
      9.68641910e-05    2     -2000011        11                       
      9.68641620e-05    2      2000013       -13                       
      9.68641620e-05    2     -2000013        13                       
      2.11589610e-04    2      1000015       -15                       
      2.11589610e-04    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  3.24460219e-01   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.82476350e-01    2      1000022        24                       
      1.75236780e-02    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  4.69975009e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.28861100e-03    2      1000022        24                       
      2.84782290e-01    2      1000023        24                       
      2.39095850e-01    2      1000025        24                       
      2.30347180e-04    2     -1000015        16                       
      2.69763170e-01    2      1000024        23                       
      2.01839770e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56227906e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96543430e-04    2           13       -13                       
      5.62188850e-02    2           15       -15                       
      2.35927660e-03    2            3        -3                       
      7.69048330e-01    2            5        -5                       
      3.34374870e-02    2            4        -4                       
      1.90718800e-03    2           22        22                       
      3.96469760e-02    2           21        21                       
      4.93876310e-03    3           24        11       -12             
      4.93876310e-03    3           24        13       -14             
      4.93876310e-03    3           24        15       -16             
      1.48162890e-02    3           24        -2         1             
      1.48162890e-02    3           24        -4         3             
      4.93876310e-03    3          -24       -11        12             
      4.93876310e-03    3          -24       -13        14             
      4.93876310e-03    3          -24       -15        16             
      1.48162890e-02    3          -24         2        -1             
      1.48162890e-02    3          -24         4        -3             
      5.66779930e-04    3           23        12       -12             
      5.66779930e-04    3           23        14       -14             
      5.66779930e-04    3           23        16       -16             
      2.85254670e-04    3           23        11       -11             
      2.85254670e-04    3           23        13       -13             
      2.85254670e-04    3           23        15       -15             
      9.77258310e-04    3           23         2        -2             
      9.77258310e-04    3           23         4        -4             
      1.25894940e-03    3           23         1        -1             
      1.25894940e-03    3           23         3        -3             
      1.25894940e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  2.89841032e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.96405690e-05    2           13       -13                       
      2.56739520e-02    2           15       -15                       
      1.05084760e-03    2            3        -3                       
      2.59126100e-01    2            5        -5                       
      4.31279570e-01    2            6        -6                       
      2.27119690e-04    2           21        21                       
      1.46168840e-03    2           24       -24                       
      7.42129750e-04    2           23        23                       
      6.64616930e-03    2      1000022   1000022                       
      7.36443620e-02    2      1000022   1000023                       
      1.80853230e-01    2      1000022   1000025                       
      2.93201460e-05    2      1000022   1000035                       
      1.03376250e-03    2      1000023   1000023                       
      6.93454780e-03    2      1000023   1000025                       
      5.01312040e-03    2      1000024  -1000024                       
      5.77127240e-03    2           25        25                       
      1.43894820e-04    2      2000011  -2000011                       
      1.43865120e-04    2      2000013  -2000013                       
      1.35517560e-04    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  3.26132197e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.92034770e-05    2           13       -13                       
      2.26849470e-02    2           15       -15                       
      9.28549040e-04    2            3        -3                       
      2.29170260e-01    2            5        -5                       
      4.05182810e-01    2            6        -6                       
      2.41131640e-04    2           21        21                       
      8.43516550e-03    2      1000022   1000022                       
      1.75085110e-01    2      1000022   1000023                       
      5.35703150e-02    2      1000022   1000025                       
      8.46539410e-05    2      1000022   1000035                       
      2.12942810e-02    2      1000023   1000023                       
      4.01752950e-05    2      1000023   1000025                       
      8.19544350e-02    2      1000024  -1000024                       
      1.24902200e-03    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  2.75305337e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.47310250e-05    2           14       -13                       
      2.71322490e-02    2           16       -15                       
      1.02957820e-03    2            4        -3                       
      6.71016450e-01    2            6        -5                       
      2.80441460e-01    2      1000024   1000022                       
      2.05587570e-03    2      1000024   1000023                       
      1.66110580e-02    2      1000024   1000025                       
      1.04903140e-04    2      1000037   1000022                       
      1.51370210e-03    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
