#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19662900e+02   # h
        35     1.00710630e+03   # H
        36     1.00000000e+03   # A
        37     1.00966270e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99985080e+03   # b1
   1000006     2.83352910e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     1.52445100e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.40455600e+02   # N1
   1000023    -1.64389300e+02   # N2
   1000024     1.54099000e+02   # C1
   1000025     2.62637000e+02   # N3
   1000035     6.61296900e+02   # N4
   1000037     6.61278500e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00080660e+03   # b2
   2000006     3.16283890e+03   # t2
   2000011     1.52406800e+02   # eR
   2000013     1.52406800e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68795180e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07374981e-01   # O_{11}
  1  2    -7.06838480e-01   # O_{12}
  2  1     7.06838480e-01   # O_{21}
  2  2     7.07374981e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     5.11105708e-01   # O_{11}
  1  2     8.59517862e-01   # O_{12}
  2  1    -8.59517862e-01   # O_{21}
  2  2     5.11105708e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     3.00184250e-01   # 
  1  2    -1.17417400e-01   # 
  1  3     6.99713050e-01   # 
  1  4    -6.37576940e-01   # 
  2  1    -6.33946600e-02   # 
  2  2     5.86901300e-02   # 
  2  3     6.90994080e-01   # 
  2  4     7.17679440e-01   # 
  3  1    -9.51654970e-01   # 
  3  2    -5.64861600e-02   # 
  3  3     1.73866840e-01   # 
  3  4    -2.46844740e-01   # 
  4  1     1.49412000e-02   # 
  4  2    -9.89736500e-01   # 
  4  3    -5.19583000e-02   # 
  4  4     1.32284640e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -7.29892600e-02   # Umix_{11}
  1  2     9.97332750e-01   # Umix_{12}
  2  1    -9.97332750e-01   # Umix_{21}
  2  2    -7.29892600e-02   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -1.86860430e-01   # Vmix_{11}
  1  2     9.82386470e-01   # Vmix_{12}
  2  1    -9.82386470e-01   # Vmix_{21}
  2  2    -1.86860430e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     1.60000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     2.50000000e+02   # M1
     2     6.50000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     1.46300000e+02   # m_eR
    35     1.46300000e+02   # m_muR
    36     1.46600000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     9.60000000e+02   # A_e
  2  2     9.60000000e+02   # A_mu
  3  3     9.60000000e+02   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.47481787e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.07574740e-02    2      1000022         1                       
      1.78578570e-03    2      1000023         1                       
      4.97226560e-03    2      1000025         1                       
      3.24088690e-01    2      1000035         1                       
      3.85237350e-03    2     -1000024         2                       
      6.54543460e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.48088212e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.40715650e-03    2      1000022         2                       
      7.99028610e-04    2      1000023         2                       
      1.90327910e-02    2      1000025         2                       
      3.19841830e-01    2      1000035         2                       
      2.51970220e-02    2      1000024         1                       
      6.33722190e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.47481787e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.07574770e-02    2      1000022         3                       
      1.78578620e-03    2      1000023         3                       
      4.97226740e-03    2      1000025         3                       
      3.24088810e-01    2      1000035         3                       
      3.85237350e-03    2     -1000024         4                       
      6.54543400e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.48088212e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.40715600e-03    2      1000022         4                       
      7.99028440e-04    2      1000023         4                       
      1.90327860e-02    2      1000025         4                       
      3.19841800e-01    2      1000035         4                       
      2.51970240e-02    2      1000024         3                       
      6.33722190e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  2.25102599e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.92279140e-02    2      1000022         5                       
      5.10662510e-03    2      1000023         5                       
      4.44819670e-02    2      1000025         5                       
      1.26135420e-01    2      1000035         5                       
      4.29951010e-01    2     -1000024         6                       
      2.69379620e-01    2     -1000037         6                       
      8.57173800e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.97933345e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.51911290e-01    2      1000022         6                       
      2.30236470e-01    2      1000023         6                       
      1.07339800e-01    2      1000025         6                       
      1.16835830e-01    2      1000035         6                       
      1.70976390e-01    2      1000024         5                       
      2.22700180e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.67123705e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.10146310e-02    2      1000022         1                       
      4.05260920e-03    2      1000023         1                       
      9.04727820e-01    2      1000025         1                       
      2.04998390e-04    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.68433720e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.10148170e-02    2      1000022         2                       
      4.05261620e-03    2      1000023         2                       
      9.04727520e-01    2      1000025         2                       
      2.04994710e-04    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.67123705e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.10146310e-02    2      1000022         3                       
      4.05260920e-03    2      1000023         3                       
      9.04727820e-01    2      1000025         3                       
      2.04998390e-04    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.68433720e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.10148100e-02    2      1000022         4                       
      4.05261620e-03    2      1000023         4                       
      9.04727580e-01    2      1000025         4                       
      2.04994690e-04    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  5.89151450e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.12107390e-03    2      1000022         5                       
      7.18885470e-03    2      1000023         5                       
      1.21040180e-02    2      1000025         5                       
      1.43000500e-01    2      1000035         5                       
      4.36514560e-01    2     -1000024         6                       
      3.04396180e-01    2     -1000037         6                       
      9.46747660e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.04693887e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.39318860e-01    2      1000024         5                       
      6.67094510e-02    2      1000037         5                       
      1.69932740e-01    2           23   1000006                       
      1.70945930e-02    2           24   1000005                       
      4.72499880e-02    2           24   2000005                       
      1.88803450e-01    2      1000022         6                       
      2.02879830e-01    2      1000023         6                       
      3.14068350e-02    2      1000025         6                       
      3.66043150e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  3.81013025e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.46777570e-04    2      1000022        11                       
      1.87364160e-04    2      1000023        11                       
      1.09624200e-01    2      1000025        11                       
      2.89051260e-01    2      1000035        11                       
      3.51302090e-03    2     -1000024        12                       
      5.96877400e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  3.81322056e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.62886900e-02    2      1000022        12                       
      2.87905010e-03    2      1000023        12                       
      7.09686800e-02    2      1000025        12                       
      2.98444120e-01    2      1000035        12                       
      2.29985230e-02    2      1000024        11                       
      5.78420880e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  3.81013025e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.46777570e-04    2      1000022        13                       
      1.87364160e-04    2      1000023        13                       
      1.09624200e-01    2      1000025        13                       
      2.89051260e-01    2      1000035        13                       
      3.51302090e-03    2     -1000024        14                       
      5.96877400e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  3.81322056e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.62886900e-02    2      1000022        14                       
      2.87905010e-03    2      1000023        14                       
      7.09686800e-02    2      1000025        14                       
      2.98444120e-01    2      1000035        14                       
      2.29985230e-02    2      1000024        13                       
      5.78420880e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  1.57433984e-03   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  3.83722964e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.61241270e-02    2      1000022        16                       
      2.86102760e-03    2      1000023        16                       
      7.05244240e-02    2      1000025        16                       
      2.96575900e-01    2      1000035        16                       
      2.85320840e-02    2      1000024        15                       
      5.74829520e-01    2      1000037        15                       
      5.52820330e-04    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  1.58598588e-03   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  1.58594767e-03   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  3.83432366e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.54323050e-03    2      1000022        15                       
      2.91326640e-03    2      1000023        15                       
      1.09107140e-01    2      1000025        15                       
      2.87254600e-01    2      1000035        15                       
      3.49100050e-03    2     -1000024        16                       
      5.93136670e-01    2     -1000037        16                       
      2.77240060e-04    2           36   1000015                       
      2.76737150e-04    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  2.10368192e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.03232430e-04    3      1000024         1        -2             
      1.03232430e-04    3     -1000024         2        -1             
      1.03232430e-04    3      1000024         3        -4             
      1.03232430e-04    3     -1000024         4        -3             
      1.05743300e-01    3      1000024         5        -6             
      1.05743300e-01    3     -1000024         6        -5             
      1.60969640e-02    3      1000037         1        -2             
      1.60969640e-02    3     -1000037         2        -1             
      1.60969640e-02    3      1000037         3        -4             
      1.60969640e-02    3     -1000037         4        -3             
      9.65992810e-02    3      1000037         5        -6             
      9.65992810e-02    3     -1000037         6        -5             
      9.66149670e-04    2      1000022        21                       
      8.40163440e-04    3      1000022         1        -1             
      8.40163440e-04    3      1000022         3        -3             
      1.75078180e-03    3      1000022         5        -5             
      1.23350930e-01    3      1000022         6        -6             
      1.13174750e-03    2      1000023        21                       
      1.02128360e-04    3      1000023         1        -1             
      1.02128360e-04    3      1000023         3        -3             
      1.04534510e-03    3      1000023         5        -5             
      1.80467620e-01    3      1000023         6        -6             
      6.29423590e-05    2      1000025        21                       
      2.74187560e-03    3      1000025         1        -1             
      2.74187560e-03    3      1000025         3        -3             
      2.79051320e-03    3      1000025         5        -5             
      7.63846410e-02    3      1000025         6        -6             
      1.79470030e-02    3      1000035         1        -1             
      1.79470030e-02    3      1000035         3        -3             
      1.78217330e-02    3      1000035         5        -5             
      8.14794380e-02    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  3.42153142e-04   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.59775070e-05    2      1000022        22                       
      1.31747470e-04    3     -1000024         2        -1             
      4.39158210e-05    3     -1000024        12       -11             
      4.39158210e-05    3     -1000024        14       -13             
      1.31747470e-04    3      1000024         1        -2             
      4.39158210e-05    3      1000024        11       -12             
      4.39158210e-05    3      1000024        13       -14             
      1.31747470e-04    3     -1000024         4        -3             
      4.39158210e-05    3     -1000024        16       -15             
      1.31747470e-04    3      1000024         3        -4             
      4.39158210e-05    3      1000024        15       -16             
      2.14377440e-03    3      1000022         2        -2             
      2.76431530e-03    3      1000022         1        -1             
      2.76431530e-03    3      1000022         3        -3             
      2.14377440e-03    3      1000022         4        -4             
      1.69066400e-03    3      1000022         5        -5             
      6.27098020e-04    3      1000022        11       -11             
      6.27098020e-04    3      1000022        13       -13             
      5.79945630e-04    3      1000022        15       -15             
      1.24777430e-03    3      1000022        12       -12             
      1.24777430e-03    3      1000022        14       -14             
      1.24777430e-03    3      1000022        16       -16             
      9.68872460e-02    2      2000011       -11                       
      9.68872460e-02    2     -2000011        11                       
      9.68833040e-02    2      2000013       -13                       
      9.68833040e-02    2     -2000013        13                       
      2.97269050e-01    2      1000015       -15                       
      2.97269050e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  1.73297175e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.30981050e-02    2      1000024       -24                       
      3.30981050e-02    2     -1000024        24                       
      2.08237510e-03    2      1000022        23                       
      3.78206120e-03    2      1000023        23                       
      6.93559040e-03    2      1000022        25                       
      1.53551520e-01    2      2000011       -11                       
      1.53551520e-01    2     -2000011        11                       
      1.53551410e-01    2      2000013       -13                       
      1.53551410e-01    2     -2000013        13                       
      1.53398900e-01    2      1000015       -15                       
      1.53398900e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  6.02250892e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.58144080e-01    2      1000024       -24                       
      2.58144080e-01    2     -1000024        24                       
      3.25222610e-02    2      1000022        23                       
      2.16459050e-01    2      1000023        23                       
      5.59215620e-03    2      1000025        23                       
      1.51789170e-01    2      1000022        25                       
      3.68902350e-02    2      1000023        25                       
      4.01048210e-02    2      1000025        25                       
      5.58892930e-05    2      2000011       -11                       
      5.58892930e-05    2     -2000011        11                       
      5.58892930e-05    2      2000013       -13                       
      5.58892930e-05    2     -2000013        13                       
      6.53783060e-05    2      1000015       -15                       
      6.53783060e-05    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  3.07570093e-06   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.53188660e-02    3      1000022         2        -1             
      5.53188660e-02    3      1000022         4        -3             
      1.84398720e-02    3      1000022       -11        12             
      1.84398720e-02    3      1000022       -13        14             
      1.84398720e-02    3      1000022       -15        16             
      8.34042610e-01    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  6.02030550e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.01921160e-01    2      1000022        24                       
      2.60054890e-01    2      1000023        24                       
      4.96407640e-02    2      1000025        24                       
      1.92623680e-05    2     -1000015        16                       
      2.55610260e-01    2      1000024        23                       
      2.32753780e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56006651e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96615800e-04    2           13       -13                       
      5.62395450e-02    2           15       -15                       
      2.36015420e-03    2            3        -3                       
      7.69361790e-01    2            5        -5                       
      3.34482490e-02    2            4        -4                       
      1.89226050e-03    2           22        22                       
      3.96370960e-02    2           21        21                       
      4.92294880e-03    3           24        11       -12             
      4.92294880e-03    3           24        13       -14             
      4.92294880e-03    3           24        15       -16             
      1.47688460e-02    3           24        -2         1             
      1.47688460e-02    3           24        -4         3             
      4.92294880e-03    3          -24       -11        12             
      4.92294880e-03    3          -24       -13        14             
      4.92294880e-03    3          -24       -15        16             
      1.47688460e-02    3          -24         2        -1             
      1.47688460e-02    3          -24         4        -3             
      5.64275720e-04    3           23        12       -12             
      5.64275720e-04    3           23        14       -14             
      5.64275720e-04    3           23        16       -16             
      2.83994280e-04    3           23        11       -11             
      2.83994280e-04    3           23        13       -13             
      2.83994280e-04    3           23        15       -15             
      9.72940350e-04    3           23         2        -2             
      9.72940350e-04    3           23         4        -4             
      1.25338680e-03    3           23         1        -1             
      1.25338680e-03    3           23         3        -3             
      1.25338680e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  7.31463371e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.55201450e-05    2           13       -13                       
      1.01733240e-02    2           15       -15                       
      4.16399180e-04    2            3        -3                       
      1.02678810e-01    2            5        -5                       
      1.70910310e-01    2            6        -6                       
      9.00040320e-05    2           21        21                       
      5.81543140e-04    2           24       -24                       
      2.95261590e-04    2           23        23                       
      2.77925540e-02    2      1000022   1000022                       
      1.63114570e-02    2      1000022   1000023                       
      2.44817660e-02    2      1000022   1000025                       
      3.61297130e-02    2      1000022   1000035                       
      5.50431760e-03    2      1000023   1000023                       
      7.71756840e-02    2      1000023   1000025                       
      1.40732910e-01    2      1000023   1000035                       
      2.64549810e-03    2      1000025   1000025                       
      6.87594640e-04    2      1000025   1000035                       
      2.94739550e-02    2      1000024  -1000024                       
      1.75680910e-01    2      1000024  -1000037                       
      1.75680910e-01    2     -1000024   1000037                       
      2.31851220e-03    2           25        25                       
      6.90655800e-05    2      2000011  -2000011                       
      6.90513260e-05    2      2000013  -2000013                       
      6.50401420e-05    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  7.21473200e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.58018590e-05    2           13       -13                       
      1.02541360e-02    2           15       -15                       
      4.19726280e-04    2            3        -3                       
      1.03590420e-01    2            5        -5                       
      1.83152290e-01    2            6        -6                       
      1.08997240e-04    2           21        21                       
      5.64488920e-02    2      1000022   1000022                       
      3.62384760e-03    2      1000022   1000023                       
      5.04994210e-02    2      1000022   1000025                       
      1.29531280e-01    2      1000022   1000035                       
      3.06237050e-03    2      1000023   1000023                       
      3.43298760e-02    2      1000023   1000025                       
      2.94572750e-02    2      1000023   1000035                       
      9.72166190e-03    2      1000025   1000025                       
      1.03933320e-02    2      1000025   1000035                       
      4.24760650e-02    2      1000024  -1000024                       
      1.66163830e-01    2      1000024  -1000037                       
      1.66163830e-01    2     -1000024   1000037                       
      5.66877540e-04    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  7.05662886e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.69575390e-05    2           14       -13                       
      1.05851400e-02    2           16       -15                       
      4.01670670e-04    2            4        -3                       
      2.61784490e-01    2            6        -5                       
      8.57827630e-03    2      1000024   1000022                       
      5.33045320e-03    2      1000024   1000023                       
      1.58667130e-01    2      1000024   1000025                       
      1.82679440e-01    2      1000024   1000035                       
      1.82386830e-01    2      1000037   1000022                       
      1.76103490e-01    2      1000037   1000023                       
      1.28531510e-02    2      1000037   1000025                       
      5.92938100e-04    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
