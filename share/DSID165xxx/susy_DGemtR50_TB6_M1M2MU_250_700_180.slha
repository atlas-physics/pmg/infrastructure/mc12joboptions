#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19663300e+02   # h
        35     1.00710590e+03   # H
        36     1.00000000e+03   # A
        37     1.00966270e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99980440e+03   # b1
   1000006     2.83360840e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     1.71375300e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.58638400e+02   # N1
   1000023    -1.84080000e+02   # N2
   1000024     1.74366500e+02   # C1
   1000025     2.64843200e+02   # N3
   1000035     7.10598300e+02   # N4
   1000037     7.10585700e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00085330e+03   # b2
   2000006     3.16276780e+03   # t2
   2000011     1.71309500e+02   # eR
   2000013     1.71309500e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68794620e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07375066e-01   # O_{11}
  1  2    -7.06838395e-01   # O_{12}
  2  1     7.06838395e-01   # O_{21}
  2  2     7.07375066e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     5.31625697e-01   # O_{11}
  1  2     8.46979409e-01   # O_{12}
  2  1    -8.46979409e-01   # O_{21}
  2  2     5.31625697e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     3.53636800e-01   # 
  1  2    -1.08585160e-01   # 
  1  3     6.85848470e-01   # 
  1  4    -6.26707670e-01   # 
  2  1    -6.02818500e-02   # 
  2  2     5.38520100e-02   # 
  2  3     6.93650420e-01   # 
  2  4     7.15762140e-01   # 
  3  1    -9.33354140e-01   # 
  3  2    -5.79248900e-02   # 
  3  3     2.14388340e-01   # 
  3  4    -2.82014700e-01   # 
  4  1     1.25320200e-02   # 
  4  2    -9.90936040e-01   # 
  4  3    -4.99901100e-02   # 
  4  4     1.24056660e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -7.03118100e-02   # Umix_{11}
  1  2     9.97525040e-01   # Umix_{12}
  2  1    -9.97525040e-01   # Umix_{21}
  2  2    -7.03118100e-02   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -1.75300780e-01   # Vmix_{11}
  1  2     9.84514950e-01   # Vmix_{12}
  2  1    -9.84514950e-01   # Vmix_{21}
  2  2    -1.75300780e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     1.80000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     2.50000000e+02   # M1
     2     7.00000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     1.65900000e+02   # m_eR
    35     1.65900000e+02   # m_muR
    36     1.66200000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     1.08000000e+03   # A_e
  2  2     1.08000000e+03   # A_mu
  3  3     1.08000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.42099792e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.10355450e-02    2      1000022         1                       
      1.54318950e-03    2      1000023         1                       
      4.64702910e-03    2      1000025         1                       
      3.24507740e-01    2      1000035         1                       
      3.62581880e-03    2     -1000024         2                       
      6.54640790e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.42687562e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.02751800e-04    2      1000022         2                       
      6.70084030e-04    2      1000023         2                       
      1.90079120e-02    2      1000025         2                       
      3.20822890e-01    2      1000035         2                       
      2.24913320e-02    2      1000024         1                       
      6.36305030e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.42099792e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.10355460e-02    2      1000022         3                       
      1.54318970e-03    2      1000023         3                       
      4.64702960e-03    2      1000025         3                       
      3.24507770e-01    2      1000035         3                       
      3.62581760e-03    2     -1000024         4                       
      6.54640670e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.42687562e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.02751740e-04    2      1000022         4                       
      6.70083860e-04    2      1000023         4                       
      1.90079080e-02    2      1000025         4                       
      3.20822780e-01    2      1000035         4                       
      2.24913380e-02    2      1000024         3                       
      6.36305210e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  2.39606844e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.98231890e-02    2      1000022         5                       
      5.04093380e-03    2      1000023         5                       
      3.73133120e-02    2      1000025         5                       
      1.26779540e-01    2      1000035         5                       
      4.35379180e-01    2     -1000024         6                       
      2.68791790e-01    2     -1000037         6                       
      8.68721010e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.93659894e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.44554110e-01    2      1000022         6                       
      2.31068860e-01    2      1000023         6                       
      1.19271300e-01    2      1000025         6                       
      1.12693470e-01    2      1000035         6                       
      1.76574130e-01    2      1000024         5                       
      2.15838090e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.67123705e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.26160640e-01    2      1000022         1                       
      3.65879270e-03    2      1000023         1                       
      8.70038690e-01    2      1000025         1                       
      1.41949510e-04    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.68433720e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.26160870e-01    2      1000022         2                       
      3.65879810e-03    2      1000023         2                       
      8.70038390e-01    2      1000025         2                       
      1.41946500e-04    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.67123705e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.26160640e-01    2      1000022         3                       
      3.65879270e-03    2      1000023         3                       
      8.70038690e-01    2      1000025         3                       
      1.41949510e-04    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.68433720e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.26160890e-01    2      1000022         4                       
      3.65879760e-03    2      1000023         4                       
      8.70038330e-01    2      1000025         4                       
      1.41946480e-04    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  5.68541073e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.70106520e-03    2      1000022         5                       
      7.22337370e-03    2      1000023         5                       
      1.34370100e-02    2      1000025         5                       
      1.41873090e-01    2      1000035         5                       
      4.40601470e-01    2     -1000024         6                       
      2.99968480e-01    2     -1000037         6                       
      9.51954870e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.04373474e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.36617830e-01    2      1000024         5                       
      6.87898550e-02    2      1000037         5                       
      1.70213640e-01    2           23   1000006                       
      1.85405480e-02    2           24   1000005                       
      4.58917990e-02    2           24   2000005                       
      1.87223200e-01    2      1000022         6                       
      2.01262980e-01    2      1000023         6                       
      3.41625660e-02    2      1000025         6                       
      3.72976290e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  3.75642050e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.46014930e-03    2      1000022        11                       
      1.43254260e-04    2      1000023        11                       
      1.07884820e-01    2      1000025        11                       
      2.90069430e-01    2      1000035        11                       
      3.30185700e-03    2     -1000024        12                       
      5.96140500e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  3.75942426e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.06496600e-02    2      1000022        12                       
      2.52182990e-03    2      1000023        12                       
      6.84641750e-02    2      1000025        12                       
      2.97882590e-01    2      1000035        12                       
      2.05007720e-02    2      1000024        11                       
      5.79980970e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  3.75642050e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.46014930e-03    2      1000022        13                       
      1.43254260e-04    2      1000023        13                       
      1.07884820e-01    2      1000025        13                       
      2.90069430e-01    2      1000035        13                       
      3.30185700e-03    2     -1000024        14                       
      5.96140500e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  3.75942426e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.06496600e-02    2      1000022        14                       
      2.52182990e-03    2      1000023        14                       
      6.84641750e-02    2      1000025        14                       
      2.97882590e-01    2      1000035        14                       
      2.05007720e-02    2      1000024        13                       
      5.79980970e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  2.19100563e-03   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  3.78384593e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.04508210e-02    2      1000022        16                       
      2.50546960e-03    2      1000023        16                       
      6.80200160e-02    2      1000025        16                       
      2.95950080e-01    2      1000035        16                       
      2.61189640e-02    2      1000024        15                       
      5.76245840e-01    2      1000037        15                       
      7.08843170e-04    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  2.21147062e-03   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  2.21139632e-03   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  3.78102022e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.17022000e-03    2      1000022        15                       
      2.92491660e-03    2      1000023        15                       
      1.07446470e-01    2      1000025        15                       
      2.88199310e-01    2      1000035        15                       
      3.28040660e-03    2     -1000024        16                       
      5.92268410e-01    2     -1000037        16                       
      3.55489230e-04    2           36   1000015                       
      3.54842690e-04    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  2.07542410e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.68175080e-05    3      1000024         1        -2             
      9.68175080e-05    3     -1000024         2        -1             
      9.68175080e-05    3      1000024         3        -4             
      9.68175080e-05    3     -1000024         4        -3             
      1.08346800e-01    3      1000024         5        -6             
      1.08346800e-01    3     -1000024         6        -5             
      1.58500580e-02    3      1000037         1        -2             
      1.58500580e-02    3     -1000037         2        -1             
      1.58500580e-02    3      1000037         3        -4             
      1.58500580e-02    3     -1000037         4        -3             
      9.38712880e-02    3      1000037         5        -6             
      9.38712880e-02    3     -1000037         6        -5             
      9.54040620e-04    2      1000022        21                       
      9.55804720e-04    3      1000022         1        -1             
      9.55804720e-04    3      1000022         3        -3             
      1.83617300e-03    3      1000022         5        -5             
      1.18086080e-01    3      1000022         6        -6             
      1.13595980e-03    2      1000023        21                       
      8.80777300e-05    3      1000023         1        -1             
      8.80777300e-05    3      1000023         3        -3             
      1.04732920e-03    3      1000023         5        -5             
      1.81231950e-01    3      1000023         6        -6             
      9.32465550e-05    2      1000025        21                       
      2.66136090e-03    3      1000025         1        -1             
      2.66136090e-03    3      1000025         3        -3             
      2.74497880e-03    3      1000025         5        -5             
      8.54541210e-02    3      1000025         6        -6             
      1.77958980e-02    3      1000035         1        -1             
      1.77958980e-02    3      1000035         3        -3             
      1.76684880e-02    3      1000035         5        -5             
      7.85218480e-02    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  3.28426725e-04   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.04163290e-05    2      1000022        22                       
      1.04415170e-04    3     -1000024         2        -1             
      3.48050560e-05    3     -1000024        12       -11             
      3.48050560e-05    3     -1000024        14       -13             
      1.04415170e-04    3      1000024         1        -2             
      3.48050560e-05    3      1000024        11       -12             
      3.48050560e-05    3      1000024        13       -14             
      1.04415170e-04    3     -1000024         4        -3             
      3.48050560e-05    3     -1000024        16       -15             
      1.04415170e-04    3      1000024         3        -4             
      3.48050560e-05    3      1000024        15       -16             
      2.97479330e-03    3      1000022         2        -2             
      3.83588930e-03    3      1000022         1        -1             
      3.83588930e-03    3      1000022         3        -3             
      2.97479330e-03    3      1000022         4        -4             
      2.48352830e-03    3      1000022         5        -5             
      8.70189690e-04    3      1000022        11       -11             
      8.70189690e-04    3      1000022        13       -13             
      8.11457400e-04    3      1000022        15       -15             
      1.73146740e-03    3      1000022        12       -12             
      1.73146740e-03    3      1000022        14       -14             
      1.73146740e-03    3      1000022        16       -16             
      9.29178820e-02    2      2000011       -11                       
      9.29178820e-02    2     -2000011        11                       
      9.29144770e-02    2      2000013       -13                       
      9.29144770e-02    2     -2000013        13                       
      3.01898600e-01    2      1000015       -15                       
      3.01898600e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  1.22789344e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.44527790e-02    2      1000024       -24                       
      1.44527790e-02    2     -1000024        24                       
      1.37092650e-03    2      1000022        23                       
      5.54255710e-05    3      1000023         2        -2             
      7.14744460e-05    3      1000023         1        -1             
      7.14744460e-05    3      1000023         3        -3             
      5.54255710e-05    3      1000023         4        -4             
      7.03583280e-05    3      1000023         5        -5             
      1.62129470e-05    3      1000023        11       -11             
      1.62129470e-05    3      1000023        13       -13             
      1.61630870e-05    3      1000023        15       -15             
      3.22562920e-05    3      1000023        12       -12             
      3.22562920e-05    3      1000023        14       -14             
      3.22562920e-05    3      1000023        16       -16             
      1.61635550e-01    2      2000011       -11                       
      1.61635550e-01    2     -2000011        11                       
      1.61635370e-01    2      2000013       -13                       
      1.61635370e-01    2     -2000013        13                       
      1.61356120e-01    2      1000015       -15                       
      1.61356120e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  6.54664810e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.57064010e-01    2      1000024       -24                       
      2.57064010e-01    2     -1000024        24                       
      2.99536920e-02    2      1000022        23                       
      2.16989980e-01    2      1000023        23                       
      7.11409380e-03    2      1000025        23                       
      1.47472190e-01    2      1000022        25                       
      3.55802180e-02    2      1000023        25                       
      4.85140980e-02    2      1000025        25                       
      3.84613490e-05    2      2000011       -11                       
      3.84613490e-05    2     -2000011        11                       
      3.84613410e-05    2      2000013       -13                       
      3.84613410e-05    2     -2000013        13                       
      4.70966730e-05    2      1000015       -15                       
      4.70966730e-05    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  8.36957351e-06   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.97955290e-02    3      1000022         2        -1             
      3.97955290e-02    3      1000022         4        -3             
      1.32653740e-02    3      1000022       -11        12             
      1.32653740e-02    3      1000022       -13        14             
      1.32653750e-02    3      1000022       -15        16             
      8.80612850e-01    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  6.54404454e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.92119910e-01    2      1000022        24                       
      2.58033540e-01    2      1000023        24                       
      5.98323900e-02    2      1000025        24                       
      1.74827360e-05    2     -1000015        16                       
      2.55192040e-01    2      1000024        23                       
      2.34804760e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56006651e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96613790e-04    2           13       -13                       
      5.62389680e-02    2           15       -15                       
      2.36013000e-03    2            3        -3                       
      7.69353510e-01    2            5        -5                       
      3.34481190e-02    2            4        -4                       
      1.89703950e-03    2           22        22                       
      3.96373380e-02    2           21        21                       
      4.92315410e-03    3           24        11       -12             
      4.92315410e-03    3           24        13       -14             
      4.92315410e-03    3           24        15       -16             
      1.47694620e-02    3           24        -2         1             
      1.47694620e-02    3           24        -4         3             
      4.92315410e-03    3          -24       -11        12             
      4.92315410e-03    3          -24       -13        14             
      4.92315410e-03    3          -24       -15        16             
      1.47694620e-02    3          -24         2        -1             
      1.47694620e-02    3          -24         4        -3             
      5.64308200e-04    3           23        12       -12             
      5.64308200e-04    3           23        14       -14             
      5.64308200e-04    3           23        16       -16             
      2.84010600e-04    3           23        11       -11             
      2.84010600e-04    3           23        13       -13             
      2.84010600e-04    3           23        15       -15             
      9.72996410e-04    3           23         2        -2             
      9.72996410e-04    3           23         4        -4             
      1.25345900e-03    3           23         1        -1             
      1.25345900e-03    3           23         3        -3             
      1.25345900e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  6.18376550e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.20153230e-05    2           13       -13                       
      1.20336100e-02    2           15       -15                       
      4.92541530e-04    2            3        -3                       
      1.21454570e-01    2            5        -5                       
      2.02161360e-01    2            6        -6                       
      1.06461350e-04    2           21        21                       
      6.87669150e-04    2           24       -24                       
      3.49144010e-04    2           23        23                       
      3.50508950e-02    2      1000022   1000022                       
      2.42201650e-02    2      1000022   1000023                       
      2.24716910e-02    2      1000022   1000025                       
      2.32413370e-02    2      1000022   1000035                       
      5.40796020e-03    2      1000023   1000023                       
      8.78275410e-02    2      1000023   1000025                       
      1.26272250e-01    2      1000023   1000035                       
      4.69621600e-03    2      1000025   1000025                       
      2.95036270e-04    2      1000025   1000035                       
      2.92524290e-02    2      1000024  -1000024                       
      1.50480150e-01    2      1000024  -1000037                       
      1.50480150e-01    2     -1000024   1000037                       
      2.73979710e-03    2           25        25                       
      8.06021200e-05    2      2000011  -2000011                       
      8.05854870e-05    2      2000013  -2000013                       
      7.59023750e-05    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  6.09839711e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.23560270e-05    2           13       -13                       
      1.21313380e-02    2           15       -15                       
      4.96564670e-04    2            3        -3                       
      1.22554490e-01    2            5        -5                       
      2.16681570e-01    2            6        -6                       
      1.28951130e-04    2           21        21                       
      7.30156450e-02    2      1000022   1000022                       
      6.16700850e-03    2      1000022   1000023                       
      4.70434610e-02    2      1000022   1000025                       
      1.13305690e-01    2      1000022   1000035                       
      3.11703400e-03    2      1000023   1000023                       
      3.85500750e-02    2      1000023   1000025                       
      1.70771780e-02    2      1000023   1000035                       
      1.61194520e-02    2      1000025   1000025                       
      9.10749750e-03    2      1000025   1000035                       
      4.37400190e-02    2      1000024  -1000024                       
      1.40025620e-01    2      1000024  -1000037                       
      1.40025620e-01    2     -1000024   1000037                       
      6.70446080e-04    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  5.92332613e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.40273510e-05    2           14       -13                       
      1.26100300e-02    2           16       -15                       
      4.78508470e-04    2            4        -3                       
      3.11862710e-01    2            6        -5                       
      1.61656220e-02    2      1000024   1000022                       
      5.52493990e-03    2      1000024   1000023                       
      1.78471210e-01    2      1000024   1000025                       
      1.58113080e-01    2      1000024   1000035                       
      1.52961030e-01    2      1000037   1000022                       
      1.50954480e-01    2      1000037   1000023                       
      1.21082430e-02    2      1000037   1000025                       
      7.06144960e-04    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
