#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19509200e+02   # h
        35     5.04374200e+02   # H
        36     5.00000000e+02   # A
        37     5.09428600e+02   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99925680e+03   # b1
   1000006     2.83448070e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     2.18633300e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.91541600e+02   # N1
   1000023     2.45629500e+02   # N2
   1000024     1.93202000e+02   # C1
   1000025    -4.04615000e+02   # N3
   1000035     4.27443800e+02   # N4
   1000037     4.23920600e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00140040e+03   # b2
   2000006     3.16198580e+03   # t2
   2000011     2.18612700e+02   # eR
   2000013     2.18612700e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.80313100e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07376451e-01   # O_{11}
  1  2    -7.06837009e-01   # O_{12}
  2  1     7.06837009e-01   # O_{21}
  2  2     7.07376451e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.27768460e-01   # O_{11}
  1  2     7.78400129e-01   # O_{12}
  2  1    -7.78400129e-01   # O_{21}
  2  2     6.27768460e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     1.61419910e-01   # 
  1  2    -9.30136020e-01   # 
  1  3     2.83103910e-01   # 
  1  4    -1.69242320e-01   # 
  2  1     9.66953580e-01   # 
  2  2     2.15804730e-01   # 
  2  3     1.10782590e-01   # 
  2  4    -7.84622700e-02   # 
  3  1     3.95395600e-02   # 
  3  2    -7.66216400e-02   # 
  3  3    -6.98794480e-01   # 
  3  4    -7.10107390e-01   # 
  4  1     1.93341580e-01   # 
  4  2    -2.87062140e-01   # 
  4  3    -6.47507790e-01   # 
  4  4     6.78931830e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -9.29534200e-01   # Umix_{11}
  1  2     3.68735940e-01   # Umix_{12}
  2  1    -3.68735940e-01   # Umix_{21}
  2  2    -9.29534200e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -9.74666710e-01   # Vmix_{11}
  1  2     2.23662200e-01   # Vmix_{12}
  2  1    -2.23662200e-01   # Vmix_{21}
  2  2    -9.74666710e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     4.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     2.50000000e+05   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     2.50000000e+02   # M1
     2     2.10000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     2.14400000e+02   # m_eR
    35     2.14400000e+02   # m_muR
    36     2.14600000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     2.40000000e+03   # A_e
  2  2     2.40000000e+03   # A_mu
  3  3     2.40000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.78471623e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.04864940e-01    2      1000022         1                       
      4.91903050e-04    2      1000023         1                       
      2.26257300e-03    2      1000025         1                       
      3.33105850e-02    2      1000035         1                       
      5.71908890e-01    2     -1000024         2                       
      8.71611540e-02    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.79081956e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.67914980e-01    2      1000022         2                       
      5.07416760e-02    2      1000023         2                       
      1.54528240e-03    2      1000025         2                       
      2.02422830e-02    2      1000035         2                       
      6.27551500e-01    2      1000024         1                       
      3.20043340e-02    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.78471623e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.04865000e-01    2      1000022         3                       
      4.91903110e-04    2      1000023         3                       
      2.26257350e-03    2      1000025         3                       
      3.33105880e-02    2      1000035         3                       
      5.71908830e-01    2     -1000024         4                       
      8.71611390e-02    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.79081956e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.67914920e-01    2      1000022         4                       
      5.07416650e-02    2      1000023         4                       
      1.54528210e-03    2      1000025         4                       
      2.02422810e-02    2      1000035         4                       
      6.27551560e-01    2      1000024         3                       
      3.20043380e-02    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.32609025e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.59625260e-01    2      1000022         5                       
      3.14174030e-02    2      1000023         5                       
      3.19745460e-03    2      1000025         5                       
      4.07514670e-03    2      1000035         5                       
      3.11028780e-01    2     -1000024         6                       
      4.06223860e-01    2     -1000037         6                       
      8.44321250e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  7.01982658e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.36981080e-02    2      1000022         6                       
      4.99481220e-02    2      1000023         6                       
      2.13201330e-01    2      1000025         6                       
      2.99882980e-01    2      1000035         6                       
      8.44469820e-02    2      1000024         5                       
      3.18822470e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.67127949e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.62183790e-02    2      1000022         1                       
      9.35854970e-01    2      1000023         1                       
      1.52882810e-03    2      1000025         1                       
      3.63979080e-02    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.68433720e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.62184050e-02    2      1000022         2                       
      9.35855030e-01    2      1000023         2                       
      1.52882060e-03    2      1000025         2                       
      3.63976990e-02    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.67127949e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.62183790e-02    2      1000022         3                       
      9.35854970e-01    2      1000023         3                       
      1.52882810e-03    2      1000025         3                       
      3.63979080e-02    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.68433720e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.62184070e-02    2      1000022         4                       
      9.35855090e-01    2      1000023         4                       
      1.52882070e-03    2      1000025         4                       
      3.63977030e-02    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.99848117e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.27042580e-01    2      1000022         5                       
      1.09939420e-02    2      1000023         5                       
      8.95498040e-03    2      1000025         5                       
      3.17139330e-02    2      1000035         5                       
      2.58605240e-01    2     -1000024         6                       
      4.71914200e-01    2     -1000037         6                       
      9.07751170e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.03751576e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.97135850e-01    2      1000024         5                       
      1.13353950e-01    2      1000037         5                       
      1.68584790e-01    2           23   1000006                       
      2.58621070e-02    2           24   1000005                       
      3.77542710e-02    2           24   2000005                       
      9.93493050e-02    2      1000022         6                       
      4.91515770e-02    2      1000023         6                       
      1.97496270e-01    2      1000025         6                       
      1.11312000e-01    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.12018779e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.15247660e-01    2      1000022        11                       
      1.68876560e-01    2      1000023        11                       
      8.90238680e-04    2      1000025        11                       
      9.61697570e-03    2      1000035        11                       
      5.25309740e-01    2     -1000024        12                       
      8.00589030e-02    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.12328510e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.15249350e-01    2      1000022        12                       
      3.00991350e-02    2      1000023        12                       
      2.85499080e-03    2      1000025        12                       
      4.54634910e-02    2      1000035        12                       
      5.76911390e-01    2      1000024        11                       
      2.94216160e-02    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.12018779e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.15247660e-01    2      1000022        13                       
      1.68876560e-01    2      1000023        13                       
      8.90238680e-04    2      1000025        13                       
      9.61697570e-03    2      1000035        13                       
      5.25309740e-01    2     -1000024        14                       
      8.00589030e-02    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.12328510e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.15249350e-01    2      1000022        14                       
      3.00991350e-02    2      1000023        14                       
      2.85499080e-03    2      1000025        14                       
      4.54634910e-02    2      1000035        14                       
      5.76911390e-01    2      1000024        13                       
      2.94216160e-02    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  1.69591095e-03   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.38205600e-01    2      1000022        15                       
      6.17943410e-02    2     -1000024        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.15897890e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.12540410e-01    2      1000022        16                       
      2.98404920e-02    2      1000023        16                       
      2.83045760e-03    2      1000025        16                       
      4.50728240e-02    2      1000035        16                       
      5.72664560e-01    2      1000024        15                       
      3.35671790e-02    2      1000037        15                       
      3.48399090e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  1.56379187e-03   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  1.56379187e-03   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.15582776e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.13815810e-01    2      1000022        15                       
      1.67488530e-01    2      1000023        15                       
      3.37753910e-03    2      1000025        15                       
      1.16691860e-02    2      1000035        15                       
      5.20790760e-01    2     -1000024        16                       
      7.93702300e-02    2     -1000037        16                       
      1.74471400e-03    2           36   1000015                       
      1.74342220e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  2.03878082e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.71726770e-02    3      1000024         1        -2             
      1.71726770e-02    3     -1000024         2        -1             
      1.71726770e-02    3      1000024         3        -4             
      1.71726770e-02    3     -1000024         4        -3             
      5.10031730e-02    3      1000024         5        -6             
      5.10031730e-02    3     -1000024         6        -5             
      2.53905520e-03    3      1000037         1        -2             
      2.53905520e-03    3     -1000037         2        -1             
      2.53905520e-03    3      1000037         3        -4             
      2.53905520e-03    3     -1000037         4        -3             
      1.51562930e-01    3      1000037         5        -6             
      1.51562930e-01    3     -1000037         6        -5             
      2.30339370e-04    2      1000022        21                       
      1.91871450e-02    3      1000022         1        -1             
      1.91871450e-02    3      1000022         3        -3             
      1.91423890e-02    3      1000022         5        -5             
      2.63732340e-02    3      1000022         6        -6             
      3.41154050e-05    2      1000023        21                       
      2.65569870e-03    3      1000023         1        -1             
      2.65569870e-03    3      1000023         3        -3             
      2.64719180e-03    3      1000023         5        -5             
      3.43106430e-02    3      1000023         6        -6             
      1.11395470e-03    2      1000025        21                       
      1.24576710e-04    3      1000025         1        -1             
      1.24576710e-04    3      1000025         3        -3             
      1.05644320e-03    3      1000025         5        -5             
      1.61512320e-01    3      1000025         6        -6             
      7.74691580e-04    2      1000035        21                       
      2.21601940e-03    3      1000035         1        -1             
      2.21601940e-03    3      1000035         3        -3             
      3.02282260e-03    3      1000035         5        -5             
      2.17435970e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  1.51515849e-01   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.51216970e-04    3     -1000024         2        -1             
      5.04056550e-05    3     -1000024        12       -11             
      5.04056550e-05    3     -1000024        14       -13             
      1.51216970e-04    3      1000024         1        -2             
      5.04056550e-05    3      1000024        11       -12             
      5.04056550e-05    3      1000024        13       -14             
      1.51216970e-04    3     -1000024         4        -3             
      5.04056550e-05    3     -1000024        16       -15             
      1.51216970e-04    3      1000024         3        -4             
      5.04056550e-05    3      1000024        15       -16             
      1.66585830e-01    2      2000011       -11                       
      1.66585830e-01    2     -2000011        11                       
      1.66584300e-01    2      2000013       -13                       
      1.66584300e-01    2     -2000013        13                       
      1.66376280e-01    2      1000015       -15                       
      1.66376280e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  2.45670349e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.18142920e-01    2      1000024       -24                       
      3.18142920e-01    2     -1000024        24                       
      3.20756470e-01    2      1000022        23                       
      2.24476640e-02    2      1000023        23                       
      1.51996330e-02    2      1000022        25                       
      3.45817940e-04    2      1000023        25                       
      3.28310040e-04    2      2000011       -11                       
      3.28310040e-04    2     -2000011        11                       
      3.28309920e-04    2      2000013       -13                       
      3.28309920e-04    2     -2000013        13                       
      1.82579310e-03    2      1000015       -15                       
      1.82579310e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  2.63659670e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.31935350e-01    2      1000024       -24                       
      3.31935350e-01    2     -1000024        24                       
      2.70389230e-02    2      1000022        23                       
      1.26486910e-03    2      1000023        23                       
      2.37005460e-01    2      1000022        25                       
      1.78314430e-02    2      1000023        25                       
      8.40371290e-03    2      2000011       -11                       
      8.40371290e-03    2     -2000011        11                       
      8.40371100e-03    2      2000013       -13                       
      8.40371100e-03    2     -2000013        13                       
      9.68692920e-03    2      1000015       -15                       
      9.68692920e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  3.14807729e-11   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.00000380e-01    3      1000022         2        -1             
      1.99999810e-01    3      1000022       -11        12             
      1.99999810e-01    3      1000022       -13        14             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  2.52309579e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.82595130e-01    2      1000022        24                       
      1.44550800e-01    2      1000023        24                       
      2.87174780e-03    2     -1000015        16                       
      3.33874880e-01    2      1000024        23                       
      2.36107380e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  5.07087827e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.01230900e-04    2           13       -13                       
      5.75594460e-02    2           15       -15                       
      2.41559950e-03    2            3        -3                       
      7.87579420e-01    2            5        -5                       
      2.99245640e-02    2            4        -4                       
      1.72803420e-03    2           22        22                       
      3.52065820e-02    2           21        21                       
      4.34190690e-03    3           24        11       -12             
      4.34190690e-03    3           24        13       -14             
      4.34190690e-03    3           24        15       -16             
      1.30257210e-02    3           24        -2         1             
      1.30257210e-02    3           24        -4         3             
      4.34190690e-03    3          -24       -11        12             
      4.34190690e-03    3          -24       -13        14             
      4.34190690e-03    3          -24       -15        16             
      1.30257210e-02    3          -24         2        -1             
      1.30257210e-02    3          -24         4        -3             
      4.94551550e-04    3           23        12       -12             
      4.94551550e-04    3           23        14       -14             
      4.94551550e-04    3           23        16       -16             
      2.48902800e-04    3           23        11       -11             
      2.48902800e-04    3           23        13       -13             
      2.48902800e-04    3           23        15       -15             
      8.52719940e-04    3           23         2        -2             
      8.52719940e-04    3           23         4        -4             
      1.09851340e-03    3           23         1        -1             
      1.09851340e-03    3           23         3        -3             
      1.09851340e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  1.10227254e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.17570140e-04    2           13       -13                       
      3.36713940e-02    2           15       -15                       
      1.38676210e-03    2            3        -3                       
      3.72094600e-01    2            5        -5                       
      1.55360870e-05    2            4        -4                       
      3.71443960e-01    2            6        -6                       
      5.34559540e-04    2           21        21                       
      7.47953450e-03    2           24       -24                       
      3.67688430e-03    2           23        23                       
      6.77810610e-02    2      1000022   1000022                       
      7.27807170e-03    2      1000022   1000023                       
      4.02111620e-05    2      1000023   1000023                       
      1.02308470e-01    2      1000024  -1000024                       
      3.06677070e-02    2           25        25                       
      5.10936200e-04    2      2000011  -2000011                       
      5.10834160e-04    2      2000013  -2000013                       
      4.82028900e-04    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  1.78388487e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.23996490e-05    2           13       -13                       
      2.07358490e-02    2           15       -15                       
      8.54038630e-04    2            3        -3                       
      2.29453280e-01    2            5        -5                       
      3.41753930e-01    2            6        -6                       
      2.85682940e-04    2           21        21                       
      1.48432970e-01    2      1000022   1000022                       
      2.75772720e-02    2      1000022   1000023                       
      6.53325750e-04    2      1000023   1000023                       
      2.26238010e-01    2      1000024  -1000024                       
      3.94323100e-03    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  1.01905897e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.29124350e-04    2           14       -13                       
      3.69822980e-02    2           16       -15                       
      1.40440880e-03    2            4        -3                       
      8.19506820e-01    2            6        -5                       
      4.08501270e-03    2      1000024   1000022                       
      1.30406010e-01    2      1000024   1000023                       
      7.48633690e-03    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
