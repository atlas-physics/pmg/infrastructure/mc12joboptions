#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19478800e+02   # h
        35     5.04387600e+02   # H
        36     5.00000000e+02   # A
        37     5.09429400e+02   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99998000e+03   # b1
   1000006     2.83329100e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     7.66909000e+01   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     3.97510000e+01   # N1
   1000023    -1.13546000e+02   # N2
   1000024     4.49432000e+01   # C1
   1000025     1.61551400e+02   # N3
   1000035     2.62243600e+02   # N4
   1000037     1.75828900e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00067750e+03   # b2
   2000006     3.16305200e+03   # t2
   2000011     7.66100000e+01   # eR
   2000013     7.66101000e+01   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.80341960e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07374387e-01   # O_{11}
  1  2    -7.06839074e-01   # O_{12}
  2  1     7.06839074e-01   # O_{21}
  2  2     7.07374387e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     4.14250215e-01   # O_{11}
  1  2     9.10163040e-01   # O_{12}
  2  1    -9.10163040e-01   # O_{21}
  2  2     4.14250215e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1    -9.86328400e-02   # 
  1  2     6.26242640e-01   # 
  1  3    -6.83571820e-01   # 
  1  4     3.61692760e-01   # 
  2  1    -7.66432600e-02   # 
  2  2     2.37398830e-01   # 
  2  3     6.21494410e-01   # 
  2  4     7.42638950e-01   # 
  3  1    -2.79598270e-01   # 
  3  2    -7.31011870e-01   # 
  3  3    -3.61089350e-01   # 
  3  4     5.07012190e-01   # 
  4  1     9.51956930e-01   # 
  4  2    -1.30706070e-01   # 
  4  3    -1.26843150e-01   # 
  4  4     2.46180000e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -5.36904280e-01   # Umix_{11}
  1  2     8.43643190e-01   # Umix_{12}
  2  1    -8.43643190e-01   # Umix_{21}
  2  2    -5.36904280e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -8.43643010e-01   # Vmix_{11}
  1  2     5.36904450e-01   # Vmix_{12}
  2  1    -5.36904450e-01   # Vmix_{21}
  2  2    -8.43643010e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     1.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     2.50000000e+05   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     2.50000000e+02   # M1
     2     1.00000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     6.36000000e+01   # m_eR
    35     6.36000000e+01   # m_muR
    36     6.43000000e+01   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     6.00000000e+02   # A_e
  2  2     6.00000000e+02   # A_mu
  3  3     6.00000000e+02   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.81322056e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.37454660e-01    2      1000022         1                       
      2.08806400e-02    2      1000023         1                       
      1.52174960e-01    2      1000025         1                       
      3.03650890e-02    2      1000035         1                       
      1.90874190e-01    2     -1000024         2                       
      4.68250420e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.81941624e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.22224320e-01    2      1000022         2                       
      1.64441470e-02    2      1000023         2                       
      2.01098780e-01    2      1000025         2                       
      6.21637500e-04    2      1000035         2                       
      4.70337060e-01    2      1000024         1                       
      1.89274010e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.81322056e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.37454690e-01    2      1000022         3                       
      2.08806430e-02    2      1000023         3                       
      1.52174990e-01    2      1000025         3                       
      3.03650960e-02    2      1000035         3                       
      1.90874160e-01    2     -1000024         4                       
      4.68250360e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.81941624e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.22224290e-01    2      1000022         4                       
      1.64441430e-02    2      1000023         4                       
      2.01098740e-01    2      1000025         4                       
      6.21637390e-04    2      1000035         4                       
      4.70337150e-01    2      1000024         3                       
      1.89274060e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  1.63098424e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.23872680e-01    2      1000022         5                       
      5.45164160e-04    2      1000023         5                       
      4.77207860e-02    2      1000025         5                       
      8.00528450e-02    2      1000035         5                       
      2.76737000e-01    2     -1000024         6                       
      3.92697660e-01    2     -1000037         6                       
      7.83737900e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  7.24291609e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.40721390e-02    2      1000022         6                       
      2.05451290e-01    2      1000023         6                       
      2.55744430e-01    2      1000025         6                       
      1.12062450e-01    2      1000035         6                       
      8.80393760e-03    2      1000024         5                       
      3.93865790e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.67123705e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.86579530e-03    2      1000022         1                       
      5.94215890e-03    2      1000023         1                       
      7.88475350e-02    2      1000025         1                       
      9.05344490e-01    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.68433720e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.86582320e-03    2      1000022         2                       
      5.94217280e-03    2      1000023         2                       
      7.88476770e-02    2      1000025         2                       
      9.05344310e-01    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.67123705e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.86579530e-03    2      1000022         3                       
      5.94215890e-03    2      1000023         3                       
      7.88475350e-02    2      1000025         3                       
      9.05344490e-01    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.68433720e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.86582320e-03    2      1000022         4                       
      5.94217330e-03    2      1000023         4                       
      7.88476770e-02    2      1000025         4                       
      9.05344370e-01    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  6.86826947e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.38626910e-02    2      1000022         5                       
      1.71566570e-02    2      1000023         5                       
      7.69434200e-02    2      1000025         5                       
      2.01083310e-02    2      1000035         5                       
      1.96295960e-01    2     -1000024         6                       
      5.44342700e-01    2     -1000037         6                       
      9.12902650e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.06010823e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.80052780e-01    2      1000024         5                       
      2.91882250e-02    2      1000037         5                       
      1.68532910e-01    2           23   1000006                       
      1.11122940e-02    2           24   1000005                       
      5.27564030e-02    2           24   2000005                       
      1.22637260e-01    2      1000022         6                       
      2.46636850e-01    2      1000023         6                       
      7.09684570e-02    2      1000025         6                       
      1.81149260e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.14875512e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.95766300e-02    2      1000022        11                       
      1.15748070e-02    2      1000023        11                       
      2.36878080e-01    2      1000025        11                       
      4.61858180e-02    2      1000035        11                       
      1.75427720e-01    2     -1000024        12                       
      4.30356950e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.15189554e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.40742540e-01    2      1000022        12                       
      2.36904060e-02    2      1000023        12                       
      1.00767200e-01    2      1000025        12                       
      1.28046320e-01    2      1000035        12                       
      4.32646960e-01    2      1000024        11                       
      1.74106570e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.14875512e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.95766300e-02    2      1000022        13                       
      1.15748070e-02    2      1000023        13                       
      2.36878080e-01    2      1000025        13                       
      4.61858180e-02    2      1000035        13                       
      1.75427720e-01    2     -1000024        14                       
      4.30356950e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.15189554e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.40742540e-01    2      1000022        14                       
      2.36904060e-02    2      1000023        14                       
      1.00767200e-01    2      1000025        14                       
      1.28046320e-01    2      1000035        14                       
      4.32646960e-01    2      1000024        13                       
      1.74106570e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  5.04715896e-03   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.57656430e-01    2      1000022        15                       
      3.42343600e-01    2     -1000024        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.17480655e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.39971000e-01    2      1000022        16                       
      2.35605370e-02    2      1000023        16                       
      1.00214800e-01    2      1000025        16                       
      1.27344380e-01    2      1000035        16                       
      4.34026660e-01    2      1000024        15                       
      1.74664600e-01    2      1000037        15                       
      2.18040080e-04    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  2.02429648e-03   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  2.02429648e-03   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.17163139e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.01496640e-01    2      1000022        15                       
      1.35450150e-02    2      1000023        15                       
      2.36264230e-01    2      1000025        15                       
      4.60155980e-02    2      1000035        15                       
      1.74464990e-01    2     -1000024        16                       
      4.27995230e-01    2     -1000037        16                       
      1.09189020e-04    2           36   1000015                       
      1.09108720e-04    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  2.23489865e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.30892610e-03    3      1000024         1        -2             
      5.30892610e-03    3     -1000024         2        -1             
      5.30892610e-03    3      1000024         3        -4             
      5.30892610e-03    3     -1000024         4        -3             
      2.44386980e-02    3      1000024         5        -6             
      2.44386980e-02    3     -1000024         6        -5             
      1.29408790e-02    3      1000037         1        -2             
      1.29408790e-02    3     -1000037         2        -1             
      1.29408790e-02    3      1000037         3        -4             
      1.29408790e-02    3     -1000037         4        -3             
      1.78514870e-01    3      1000037         5        -6             
      1.78514870e-01    3     -1000037         6        -5             
      4.38442130e-04    2      1000022        21                       
      7.74378470e-03    3      1000022         1        -1             
      7.74378470e-03    3      1000022         3        -3             
      8.45679450e-03    3      1000022         5        -5             
      2.38036100e-02    3      1000022         6        -6             
      1.23842990e-03    2      1000023        21                       
      1.14208150e-03    3      1000023         1        -1             
      1.14208150e-03    3      1000023         3        -3             
      1.88173160e-03    3      1000023         5        -5             
      1.64275270e-01    3      1000023         6        -6             
      3.41559640e-04    2      1000025        21                       
      8.93435350e-03    3      1000025         1        -1             
      8.93435350e-03    3      1000025         3        -3             
      9.14929060e-03    3      1000025         5        -5             
      1.84524670e-01    3      1000025         6        -6             
      4.17111810e-05    2      1000035        21                       
      4.08165580e-03    3      1000035         1        -1             
      4.08165580e-03    3      1000035         3        -3             
      4.09290100e-03    3      1000035         5        -5             
      7.90455790e-02    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  6.79663782e-03   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.17756940e-04    2      1000022        22                       
      5.04162570e-02    3     -1000024         2        -1             
      1.68054200e-02    3     -1000024        12       -11             
      1.68054200e-02    3     -1000024        14       -13             
      5.04162570e-02    3      1000024         1        -2             
      1.68054200e-02    3      1000024        11       -12             
      1.68054200e-02    3      1000024        13       -14             
      5.04162570e-02    3     -1000024         4        -3             
      1.68054200e-02    3     -1000024        16       -15             
      5.04162570e-02    3      1000024         3        -4             
      1.68054200e-02    3      1000024        15       -16             
      1.14891830e-02    3      1000022         2        -2             
      1.48074780e-02    3      1000022         1        -1             
      1.48074780e-02    3      1000022         3        -3             
      1.14891830e-02    3      1000022         4        -4             
      1.42535550e-02    3      1000022         5        -5             
      3.35940790e-03    3      1000022        11       -11             
      3.35940790e-03    3      1000022        13       -13             
      3.33856160e-03    3      1000022        15       -15             
      6.68730210e-03    3      1000022        12       -12             
      6.68730210e-03    3      1000022        14       -14             
      6.68730210e-03    3      1000022        16       -16             
      7.40657150e-02    2      2000011       -11                       
      7.40657150e-02    2     -2000011        11                       
      7.40652610e-02    2      2000013       -13                       
      7.40652610e-02    2     -2000013        13                       
      1.52028320e-01    2      1000015       -15                       
      1.52028320e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  6.22705771e-01   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.01837680e-01    2      1000024       -24                       
      4.01837680e-01    2     -1000024        24                       
      7.42558020e-03    2      1000022        23                       
      1.40116360e-05    3      1000023         2        -2             
      1.80836910e-05    3      1000023         1        -1             
      1.80836910e-05    3      1000023         3        -3             
      1.40116360e-05    3      1000023         4        -4             
      1.60700950e-05    3      1000023         5        -5             
      9.28076330e-04    2      1000022        25                       
      3.09894380e-02    2      2000011       -11                       
      3.09894380e-02    2     -2000011        11                       
      3.09893940e-02    2      2000013       -13                       
      3.09893940e-02    2     -2000013        13                       
      3.19666080e-02    2      1000015       -15                       
      3.19666080e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  3.17832826e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.08345740e-03    2      1000024       -24                       
      1.08345740e-03    2     -1000024        24                       
      1.47431970e-04    2      1000037       -24                       
      1.47431970e-04    2     -1000037        24                       
      1.00448490e-05    2      1000022        23                       
      3.67306100e-02    2      1000023        23                       
      2.91214250e-03    2      1000025        23                       
      2.58453820e-03    2      1000022        25                       
      9.92947030e-04    2      1000023        25                       
      1.59084320e-01    2      2000011       -11                       
      1.59084320e-01    2     -2000011        11                       
      1.59084260e-01    2      2000013       -13                       
      1.59084260e-01    2     -2000013        13                       
      1.58985410e-01    2      1000015       -15                       
      1.58985410e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  8.23656022e-09   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.32958250e-01    3      1000022         2        -1             
      3.33019500e-01    3      1000022         4        -3             
      1.10993580e-01    3      1000022       -11        12             
      1.10993580e-01    3      1000022       -13        14             
      1.12035190e-01    3      1000022       -15        16             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  3.19235619e-01   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.60603560e-01    2      1000022        24                       
      7.19989590e-04    3      1000023         2        -1             
      7.19989590e-04    3      1000023         4        -3             
      2.39982490e-04    3      1000023       -11        12             
      2.39982490e-04    3      1000023       -13        14             
      2.39982820e-04    3      1000023       -15        16             
      3.82242470e-03    2     -1000015        16                       
      4.24289760e-01    2      1000024        23                       
      9.12433860e-03    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  9.80558659e-02   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.04068670e-05    2           13       -13                       
      2.97674510e-03    2           15       -15                       
      1.24925750e-04    2            3        -3                       
      4.07321190e-02    2            5        -5                       
      1.54714000e-03    2            4        -4                       
      1.20561500e-04    2           22        22                       
      1.81913670e-03    2           21        21                       
      2.23681850e-04    3           24        11       -12             
      2.23681850e-04    3           24        13       -14             
      2.23681850e-04    3           24        15       -16             
      6.71045510e-04    3           24        -2         1             
      6.71045510e-04    3           24        -4         3             
      2.23681850e-04    3          -24       -11        12             
      2.23681850e-04    3          -24       -13        14             
      2.23681850e-04    3          -24       -15        16             
      6.71045510e-04    3          -24         2        -1             
      6.71045510e-04    3          -24         4        -3             
      2.54459340e-05    3           23        12       -12             
      2.54459340e-05    3           23        14       -14             
      2.54459340e-05    3           23        16       -16             
      1.28066830e-05    3           23        11       -11             
      1.28066830e-05    3           23        13       -13             
      1.28066830e-05    3           23        15       -15             
      4.38746140e-05    3           23         2        -2             
      4.38746140e-05    3           23         4        -4             
      5.65213050e-05    3           23         1        -1             
      5.65213050e-05    3           23         3        -3             
      5.65213050e-05    3           23         5        -5             
      4.52222940e-01    2      1000022   1000022                       
      4.96047710e-01    2      1000024  -1000024                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  6.59743800e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96431410e-05    2           13       -13                       
      5.62567920e-03    2           15       -15                       
      2.31694500e-04    2            3        -3                       
      6.21678310e-02    2            5        -5                       
      6.20832370e-02    2            6        -6                       
      8.93383110e-05    2           21        21                       
      1.25450190e-03    2           24       -24                       
      6.16705340e-04    2           23        23                       
      1.05945240e-01    2      1000022   1000022                       
      3.36737740e-02    2      1000022   1000023                       
      7.03561120e-03    2      1000022   1000025                       
      1.74039380e-02    2      1000022   1000035                       
      1.98496840e-02    2      1000023   1000023                       
      6.75256100e-02    2      1000023   1000025                       
      4.86095400e-02    2      1000023   1000035                       
      6.78497180e-03    2      1000025   1000025                       
      2.41961050e-03    2      1000025   1000035                       
      2.57869450e-01    2      1000024  -1000024                       
      5.76531100e-03    2      1000037  -1000037                       
      1.44680230e-01    2      1000024  -1000037                       
      1.44680230e-01    2     -1000024   1000037                       
      5.18796030e-03    2           25        25                       
      1.63157940e-04    2      2000011  -2000011                       
      1.63125370e-04    2      2000013  -2000013                       
      1.53955420e-04    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  6.65244944e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.94140300e-05    2           13       -13                       
      5.56033620e-03    2           15       -15                       
      2.29011220e-04    2            3        -3                       
      6.15281020e-02    2            5        -5                       
      9.16416270e-02    2            6        -6                       
      7.66061350e-05    2           21        21                       
      1.55717520e-01    2      1000022   1000022                       
      4.48847190e-03    2      1000022   1000023                       
      4.09747520e-03    2      1000022   1000025                       
      2.44082810e-02    2      1000022   1000035                       
      1.06161770e-02    2      1000023   1000023                       
      3.02691140e-02    2      1000023   1000025                       
      1.16982970e-02    2      1000023   1000035                       
      3.10941410e-02    2      1000025   1000025                       
      2.31458690e-02    2      1000025   1000035                       
      3.49595520e-01    2      1000024  -1000024                       
      7.24555850e-02    2      1000037  -1000037                       
      6.11485020e-02    2      1000024  -1000037                       
      6.11485020e-02    2     -1000024   1000037                       
      1.06151950e-03    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  6.37481840e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.06412410e-05    2           14       -13                       
      5.91182430e-03    2           16       -15                       
      2.24502510e-04    2            4        -3                       
      1.31002750e-01    2            6        -5                       
      5.66172120e-04    2      1000024   1000022                       
      1.24673660e-01    2      1000024   1000023                       
      2.84858350e-01    2      1000024   1000025                       
      9.36301430e-03    2      1000024   1000035                       
      3.23416890e-01    2      1000037   1000022                       
      9.97024850e-02    2      1000037   1000023                       
      1.35274960e-03    2      1000037   1000025                       
      1.77056660e-02    2      1000037   1000035                       
      1.20141230e-03    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
