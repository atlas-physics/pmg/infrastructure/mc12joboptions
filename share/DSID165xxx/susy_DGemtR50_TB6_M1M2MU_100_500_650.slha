#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19704600e+02   # h
        35     1.00709250e+03   # H
        36     1.00000000e+03   # A
        37     1.00966140e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99861180e+03   # b1
   1000006     2.83547170e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     2.88049500e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     9.85178000e+01   # N1
   1000023     4.77519300e+02   # N2
   1000024     4.77359200e+02   # C1
   1000025    -6.52784200e+02   # N3
   1000035     6.76746600e+02   # N4
   1000037     6.76434800e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00204490e+03   # b2
   2000006     3.16109720e+03   # t2
   2000011     2.87984700e+02   # eR
   2000013     2.87984700e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68785650e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07377928e-01   # O_{11}
  1  2    -7.06835530e-01   # O_{12}
  2  1     7.06835530e-01   # O_{21}
  2  2     7.07377928e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.58745998e-01   # O_{11}
  1  2     7.52365410e-01   # O_{12}
  2  1    -7.52365410e-01   # O_{21}
  2  2     6.58745998e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.97206690e-01   # 
  1  2    -6.69738000e-03   # 
  1  3     7.10506300e-02   # 
  1  4    -2.20536100e-02   # 
  2  1    -3.06326900e-02   # 
  2  2    -9.35946880e-01   # 
  2  3     2.73533880e-01   # 
  2  4    -2.19645710e-01   # 
  3  1     3.42235700e-02   # 
  3  2    -4.06616600e-02   # 
  3  3    -7.03991770e-01   # 
  3  4    -7.08216910e-01   # 
  4  1    -5.89049100e-02   # 
  4  2     3.49721550e-01   # 
  4  3     6.51557030e-01   # 
  4  4    -6.70595880e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -9.21339150e-01   # Umix_{11}
  1  2     3.88759790e-01   # Umix_{12}
  2  1    -3.88759790e-01   # Umix_{21}
  2  2    -9.21339150e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -9.49810090e-01   # Vmix_{11}
  1  2     3.12827110e-01   # Vmix_{12}
  2  1    -3.12827110e-01   # Vmix_{21}
  2  2    -9.49810090e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     6.50000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.00000000e+02   # M1
     2     5.00000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     2.84800000e+02   # m_eR
    35     2.84800000e+02   # m_muR
    36     2.85000000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     3.90000000e+03   # A_e
  2  2     3.90000000e+03   # A_mu
  3  3     3.90000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.61509310e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.25046470e-02    2      1000022         1                       
      2.87262410e-01    2      1000023         1                       
      6.98391230e-04    2      1000025         1                       
      4.09051140e-02    2      1000035         1                       
      5.63487770e-01    2     -1000024         2                       
      9.51416190e-02    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.62105958e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.07770520e-02    2      1000022         2                       
      2.93643710e-01    2      1000023         2                       
      3.74269900e-04    2      1000025         2                       
      3.60797460e-02    2      1000035         2                       
      5.97646420e-01    2      1000024         1                       
      6.14788610e-02    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.61509310e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.25046500e-02    2      1000022         3                       
      2.87262500e-01    2      1000023         3                       
      6.98391460e-04    2      1000025         3                       
      4.09051250e-02    2      1000035         3                       
      5.63487710e-01    2     -1000024         4                       
      9.51416050e-02    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.62105958e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.07770500e-02    2      1000022         4                       
      2.93643590e-01    2      1000023         4                       
      3.74269820e-04    2      1000025         4                       
      3.60797420e-02    2      1000035         4                       
      5.97646470e-01    2      1000024         3                       
      6.14788680e-02    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.45711434e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.60898080e-02    2      1000022         5                       
      1.48403670e-01    2      1000023         5                       
      4.06101490e-03    2      1000025         5                       
      8.04551880e-03    2      1000035         5                       
      3.27220890e-01    2     -1000024         6                       
      3.90115950e-01    2     -1000037         6                       
      8.60631910e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.57542458e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.71432920e-02    2      1000022         6                       
      4.00112420e-02    2      1000023         6                       
      2.15955050e-01    2      1000025         6                       
      2.95747670e-01    2      1000035         6                       
      5.70235620e-02    2      1000024         5                       
      3.44119250e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.69098757e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.94906600e-01    2      1000022         1                       
      8.93780260e-04    2      1000023         1                       
      1.06579020e-03    2      1000025         1                       
      3.13394100e-03    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.76318575e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.94906540e-01    2      1000022         2                       
      8.93771180e-04    2      1000023         2                       
      1.06576920e-03    2      1000025         2                       
      3.13387420e-03    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.69098757e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.94906600e-01    2      1000022         3                       
      8.93780260e-04    2      1000023         3                       
      1.06579020e-03    2      1000025         3                       
      3.13394100e-03    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.76318575e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.94906540e-01    2      1000022         4                       
      8.93771240e-04    2      1000023         4                       
      1.06576920e-03    2      1000025         4                       
      3.13387390e-03    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.46843177e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.99537350e-02    2      1000022         5                       
      1.19168890e-01    2      1000023         5                       
      7.42475690e-03    2      1000025         5                       
      3.54950880e-02    2      1000035         5                       
      2.69551130e-01    2     -1000024         6                       
      4.54284910e-01    2     -1000037         6                       
      9.41214930e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  9.98710265e+01   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.23923520e-01    2      1000024         5                       
      8.54412840e-02    2      1000037         5                       
      1.72038500e-01    2           23   1000006                       
      2.94109300e-02    2           24   1000005                       
      3.52812220e-02    2           24   2000005                       
      4.03497000e-02    2      1000022         6                       
      1.16051520e-01    2      1000023         6                       
      1.88978340e-01    2      1000025         6                       
      1.08525020e-01    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  3.95457823e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.34048220e-02    2      1000022        11                       
      2.75417600e-01    2      1000023        11                       
      1.38400120e-04    2      1000025        11                       
      2.89735330e-02    2      1000035        11                       
      5.15095350e-01    2     -1000024        12                       
      8.69702100e-02    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  3.95743146e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.79753430e-02    2      1000022        12                       
      2.56001920e-01    2      1000023        12                       
      1.02394840e-03    2      1000025        12                       
      4.19536980e-02    2      1000035        12                       
      5.46797340e-01    2      1000024        11                       
      5.62477000e-02    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  3.95457823e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.34048220e-02    2      1000022        13                       
      2.75417600e-01    2      1000023        13                       
      1.38400120e-04    2      1000025        13                       
      2.89735330e-02    2      1000035        13                       
      5.15095350e-01    2     -1000024        14                       
      8.69702100e-02    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  3.95743146e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.79753430e-02    2      1000022        14                       
      2.56001920e-01    2      1000023        14                       
      1.02394840e-03    2      1000025        14                       
      4.19536980e-02    2      1000035        14                       
      5.46797340e-01    2      1000024        13                       
      5.62477000e-02    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  1.13586553e+00   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.01219141e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.66399760e-02    2      1000022        16                       
      2.52512690e-01    2      1000023        16                       
      1.00999230e-03    2      1000025        16                       
      4.13818840e-02    2      1000035        16                       
      5.40124540e-01    2      1000024        15                       
      5.96874360e-02    2      1000037        15                       
      8.64338780e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  1.13559117e+00   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  1.13559117e+00   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.00925870e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.21572600e-02    2      1000022        15                       
      2.72043730e-01    2      1000023        15                       
      2.60798420e-03    2      1000025        15                       
      3.06839010e-02    2      1000035        15                       
      5.08062780e-01    2     -1000024        16                       
      8.57828330e-02    2     -1000037        16                       
      4.33477060e-03    2           36   1000015                       
      4.32671420e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  1.80863926e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.74986010e-02    3      1000024         1        -2             
      1.74986010e-02    3     -1000024         2        -1             
      1.74986010e-02    3      1000024         3        -4             
      1.74986010e-02    3     -1000024         4        -3             
      4.59267350e-02    3      1000024         5        -6             
      4.59267350e-02    3     -1000024         6        -5             
      2.81993700e-03    3      1000037         1        -2             
      2.81993700e-03    3     -1000037         2        -1             
      2.81993700e-03    3      1000037         3        -4             
      2.81993700e-03    3     -1000037         4        -3             
      1.55647610e-01    3      1000037         5        -6             
      1.55647610e-01    3     -1000037         6        -5             
      3.07489350e-05    2      1000022        21                       
      3.95285060e-03    3      1000022         1        -1             
      3.95285060e-03    3      1000022         3        -3             
      3.91899890e-03    3      1000022         5        -5             
      3.20249130e-02    3      1000022         6        -6             
      2.83392470e-04    2      1000023        21                       
      1.95937210e-02    3      1000023         1        -1             
      1.95937210e-02    3      1000023         3        -3             
      1.95535200e-02    3      1000023         5        -5             
      3.16969340e-02    3      1000023         6        -6             
      1.15832840e-03    2      1000025        21                       
      3.90939390e-05    3      1000025         1        -1             
      3.90939390e-05    3      1000025         3        -3             
      9.82476050e-04    3      1000025         5        -5             
      1.58641320e-01    3      1000025         6        -6             
      7.92161740e-04    2      1000035        21                       
      2.74472990e-03    3      1000035         1        -1             
      2.74472990e-03    3      1000035         3        -3             
      3.54075730e-03    3      1000035         5        -5             
      2.10292860e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  4.95632530e-02   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.41088170e-01    2      1000022        23                       
      7.82120760e-01    2      1000022        25                       
      9.30816870e-03    2      2000011       -11                       
      9.30816870e-03    2     -2000011        11                       
      9.30816590e-03    2      2000013       -13                       
      9.30816590e-03    2     -2000013        13                       
      1.97792260e-02    2      1000015       -15                       
      1.97792260e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  2.81233977e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.89211060e-01    2      1000024       -24                       
      2.89211060e-01    2     -1000024        24                       
      1.28572820e-01    2      1000022        23                       
      2.49754430e-01    2      1000023        23                       
      3.31430730e-02    2      1000022        25                       
      1.90920170e-03    2      1000023        25                       
      4.48438190e-04    2      2000011       -11                       
      4.48438190e-04    2     -2000011        11                       
      4.48438190e-04    2      2000013       -13                       
      4.48438190e-04    2     -2000013        13                       
      3.20226370e-03    2      1000015       -15                       
      3.20226370e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  3.21308274e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.36375060e-01    2      1000024       -24                       
      3.36375060e-01    2     -1000024        24                       
      3.00495080e-02    2      1000022        23                       
      4.81331580e-03    2      1000023        23                       
      9.23545210e-02    2      1000022        25                       
      1.88194950e-01    2      1000023        25                       
      1.24636150e-03    2      2000011       -11                       
      1.24636150e-03    2     -2000011        11                       
      1.24636150e-03    2      2000013       -13                       
      1.24636150e-03    2     -2000013        13                       
      3.42601610e-03    2      1000015       -15                       
      3.42601610e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  4.84897598e-02   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.77911230e-01    2      1000022        24                       
      2.20888570e-02    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  3.12609831e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.24142480e-01    2      1000022        24                       
      3.51265520e-01    2      1000023        24                       
      4.51993010e-03    2     -1000015        16                       
      3.19143210e-01    2      1000024        23                       
      2.00928880e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56322795e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96516890e-04    2           13       -13                       
      5.62113080e-02    2           15       -15                       
      2.35895490e-03    2            3        -3                       
      7.68932880e-01    2            5        -5                       
      3.34333180e-02    2            4        -4                       
      1.90634690e-03    2           22        22                       
      3.96506000e-02    2           21        21                       
      4.94488840e-03    3           24        11       -12             
      4.94488840e-03    3           24        13       -14             
      4.94488840e-03    3           24        15       -16             
      1.48346670e-02    3           24        -2         1             
      1.48346670e-02    3           24        -4         3             
      4.94488840e-03    3          -24       -11        12             
      4.94488840e-03    3          -24       -13        14             
      4.94488840e-03    3          -24       -15        16             
      1.48346670e-02    3          -24         2        -1             
      1.48346670e-02    3          -24         4        -3             
      5.67750420e-04    3           23        12       -12             
      5.67750420e-04    3           23        14       -14             
      5.67750420e-04    3           23        16       -16             
      2.85743120e-04    3           23        11       -11             
      2.85743120e-04    3           23        13       -13             
      2.85743120e-04    3           23        15       -15             
      9.78931550e-04    3           23         2        -2             
      9.78931550e-04    3           23         4        -4             
      1.26110520e-03    3           23         1        -1             
      1.26110520e-03    3           23         3        -3             
      1.26110520e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  2.62597247e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.89390250e-05    2           13       -13                       
      2.83371280e-02    2           15       -15                       
      1.15985270e-03    2            3        -3                       
      2.86005590e-01    2            5        -5                       
      1.04332160e-05    2            4        -4                       
      4.76003320e-01    2            6        -6                       
      2.50673040e-04    2           21        21                       
      1.61133720e-03    2           24       -24                       
      8.18109370e-04    2           23        23                       
      4.12413250e-03    2      1000022   1000022                       
      2.98283750e-02    2      1000022   1000023                       
      1.25056160e-01    2      1000022   1000025                       
      2.47757380e-02    2      1000022   1000035                       
      4.72156380e-03    2      1000023   1000023                       
      1.03679270e-02    2      1000024  -1000024                       
      6.34391790e-03    2           25        25                       
      1.65570860e-04    2      2000011  -2000011                       
      1.65536690e-04    2      2000013  -2000013                       
      1.55906590e-04    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  3.26002972e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.92342540e-05    2           13       -13                       
      2.26937610e-02    2           15       -15                       
      9.28909870e-04    2            3        -3                       
      2.29259300e-01    2            5        -5                       
      4.05340250e-01    2            6        -6                       
      2.41225340e-04    2           21        21                       
      4.23367600e-03    2      1000022   1000022                       
      4.71661580e-02    2      1000022   1000023                       
      3.09003410e-02    2      1000022   1000025                       
      6.72144520e-02    2      1000022   1000035                       
      6.05307300e-02    2      1000023   1000023                       
      1.30164040e-01    2      1000024  -1000024                       
      1.24798250e-03    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  2.43877135e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.06937310e-04    2           14       -13                       
      3.06282960e-02    2           16       -15                       
      1.16224140e-03    2            4        -3                       
      7.57478240e-01    2            6        -5                       
      9.44105310e-02    2      1000024   1000022                       
      6.32163500e-05    2      1000024   1000023                       
      1.14443790e-01    2      1000037   1000022                       
      1.70665860e-03    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
