# SUSY Herwig++ jobOptions for pMSSM grid
# Use SUSYHIT and bottom-up schema for SLHA files 

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.Stop')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_pMSSM_88927_120FT_grav.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['squarks','sbottoms','stops','gauginos','gluino','sleptons','staus','sneutrinos'], slha_file)

# define metadata
evgenConfig.description = 'pMSSM grid generation' 
evgenConfig.keywords = ['SUSY','pMSSM']
evgenConfig.contact  = ['Takashi.Yamanaka@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds

#==============================================================
#
# End of job options file
#
###############################################################

