#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19688300e+02   # h
        35     1.00709920e+03   # H
        36     1.00000000e+03   # A
        37     1.00966220e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99912870e+03   # b1
   1000006     2.83467900e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     2.85773500e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.36932600e+02   # N1
   1000023     4.34641400e+02   # N2
   1000024     4.32472300e+02   # C1
   1000025    -4.53114900e+02   # N3
   1000035     6.71540500e+02   # N4
   1000037     6.71493400e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00152860e+03   # b2
   2000006     3.16180830e+03   # t2
   2000011     2.85809200e+02   # eR
   2000013     2.85809200e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68788580e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07376875e-01   # O_{11}
  1  2    -7.06836584e-01   # O_{12}
  2  1     7.06836584e-01   # O_{21}
  2  2     7.07376875e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.36731352e-01   # O_{11}
  1  2     7.71085719e-01   # O_{12}
  2  1    -7.71085719e-01   # O_{21}
  2  2     6.36731352e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.92173550e-01   # 
  1  2    -1.07925100e-02   # 
  1  3     1.13517930e-01   # 
  1  4    -5.08820700e-02   # 
  2  1    -1.15076640e-01   # 
  2  2    -2.86454680e-01   # 
  2  3     6.80858250e-01   # 
  2  4    -6.64178730e-01   # 
  3  1     4.35283900e-02   # 
  3  2    -4.25821400e-02   # 
  3  3    -7.02378630e-01   # 
  3  4    -7.09194300e-01   # 
  4  1     2.13174700e-02   # 
  4  2    -9.57086380e-01   # 
  4  3    -1.73810150e-01   # 
  4  4     2.30914760e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -2.44833050e-01   # Umix_{11}
  1  2     9.69565270e-01   # Umix_{12}
  2  1    -9.69565270e-01   # Umix_{21}
  2  2    -2.44833050e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -3.26061760e-01   # Vmix_{11}
  1  2     9.45348440e-01   # Vmix_{12}
  2  1    -9.45348440e-01   # Vmix_{21}
  2  2    -3.26061760e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     4.50000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.40000000e+02   # M1
     2     6.50000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     2.82600000e+02   # m_eR
    35     2.82600000e+02   # m_muR
    36     2.82700000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     2.70000000e+03   # A_e
  2  2     2.70000000e+03   # A_mu
  3  3     2.70000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.47536829e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.34200820e-02    2      1000022         1                       
      2.45300680e-02    2      1000023         1                       
      8.87016070e-04    2      1000025         1                       
      3.02840290e-01    2      1000035         1                       
      4.17777000e-02    2     -1000024         2                       
      6.16544900e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.48125033e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.05585660e-02    2      1000022         2                       
      3.28766590e-02    2      1000023         2                       
      4.14773940e-04    2      1000025         2                       
      2.97309580e-01    2      1000035         2                       
      7.39446210e-02    2      1000024         1                       
      5.84895790e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.47536829e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.34200840e-02    2      1000022         3                       
      2.45300740e-02    2      1000023         3                       
      8.87016240e-04    2      1000025         3                       
      3.02840350e-01    2      1000035         3                       
      4.17776960e-02    2     -1000024         4                       
      6.16544840e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.48125033e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.05585630e-02    2      1000022         4                       
      3.28766480e-02    2      1000023         4                       
      4.14773820e-04    2      1000025         4                       
      2.97309490e-01    2      1000035         4                       
      7.39446360e-02    2      1000024         3                       
      5.84895910e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.27332405e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.08558470e-02    2      1000022         5                       
      2.85709520e-02    2      1000023         5                       
      4.21399720e-03    2      1000025         5                       
      1.19323750e-01    2      1000035         5                       
      4.29636690e-01    2     -1000024         6                       
      2.89808210e-01    2     -1000037         6                       
      8.75905530e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.75770021e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.13705000e-02    2      1000022         6                       
      1.82534380e-01    2      1000023         6                       
      2.24026500e-01    2      1000025         6                       
      1.56926350e-01    2      1000035         6                       
      1.13548870e-01    2      1000024         5                       
      2.81593350e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.68734619e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.85016520e-01    2      1000022         1                       
      1.27534900e-02    2      1000023         1                       
      1.81795030e-03    2      1000025         1                       
      4.12005350e-04    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.74869271e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.85016640e-01    2      1000022         2                       
      1.27533940e-02    2      1000023         2                       
      1.81793480e-03    2      1000025         2                       
      4.11997000e-04    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.68734619e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.85016520e-01    2      1000022         3                       
      1.27534900e-02    2      1000023         3                       
      1.81795030e-03    2      1000025         3                       
      4.12005350e-04    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.74869271e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.85016640e-01    2      1000022         4                       
      1.27533930e-02    2      1000023         4                       
      1.81793460e-03    2      1000025         4                       
      4.11996880e-04    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.70445286e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.70831870e-02    2      1000022         5                       
      8.03176030e-03    2      1000023         5                       
      7.69791520e-03    2      1000025         5                       
      1.41306490e-01    2      1000035         5                       
      3.96490340e-01    2     -1000024         6                       
      3.34890430e-01    2     -1000037         6                       
      9.44998410e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.02174824e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.70619630e-01    2      1000024         5                       
      3.64524460e-02    2      1000037         5                       
      1.70578060e-01    2           23   1000006                       
      2.69843560e-02    2           24   1000005                       
      3.73387970e-02    2           24   2000005                       
      4.32184340e-02    2      1000022         6                       
      1.93693000e-01    2      1000023         6                       
      1.93624260e-01    2      1000025         6                       
      2.74910650e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  3.81388342e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.42170470e-02    2      1000022        11                       
      3.88131630e-02    2      1000023        11                       
      1.10076050e-04    2      1000025        11                       
      2.67032770e-01    2      1000035        11                       
      3.80658100e-02    2     -1000024        12                       
      5.61761080e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  3.81697982e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.01864700e-01    2      1000022        12                       
      1.57939050e-02    2      1000023        12                       
      1.39695830e-03    2      1000025        12                       
      2.80098860e-01    2      1000035        12                       
      6.74361070e-02    2      1000024        11                       
      5.33409540e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  3.81388342e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.42170470e-02    2      1000022        13                       
      3.88131630e-02    2      1000023        13                       
      1.10076050e-04    2      1000025        13                       
      2.67032770e-01    2      1000035        13                       
      3.80658100e-02    2     -1000024        14                       
      5.61761080e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  3.81697982e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.01864700e-01    2      1000022        14                       
      1.57939050e-02    2      1000023        14                       
      1.39695830e-03    2      1000025        14                       
      2.80098860e-01    2      1000035        14                       
      6.74361070e-02    2      1000024        13                       
      5.33409540e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  8.49093114e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  3.85453268e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00869220e-01    2      1000022        16                       
      1.56395580e-02    2      1000023        16                       
      1.38330630e-03    2      1000025        16                       
      2.77361540e-01    2      1000035        16                       
      7.19208420e-02    2      1000024        15                       
      5.28512540e-01    2      1000037        15                       
      4.31292360e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  8.49454733e-01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  8.49454733e-01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  3.85160044e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.33684560e-02    2      1000022        15                       
      4.09713200e-02    2      1000023        15                       
      2.80350540e-03    2      1000025        15                       
      2.64578550e-01    2      1000035        15                       
      3.76932290e-02    2     -1000024        16                       
      5.56262970e-01    2     -1000037        16                       
      2.16304790e-03    2           36   1000015                       
      2.15902620e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  1.91626878e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.18714470e-03    3      1000024         1        -2             
      1.18714470e-03    3     -1000024         2        -1             
      1.18714470e-03    3      1000024         3        -4             
      1.18714470e-03    3     -1000024         4        -3             
      8.36081800e-02    3      1000024         5        -6             
      8.36081800e-02    3     -1000024         6        -5             
      1.66025160e-02    3      1000037         1        -2             
      1.66025160e-02    3     -1000037         2        -1             
      1.66025160e-02    3      1000037         3        -4             
      1.66025160e-02    3     -1000037         4        -3             
      1.18370980e-01    3      1000037         5        -6             
      1.18370980e-01    3     -1000037         6        -5             
      4.45362970e-05    2      1000022        21                       
      3.74422040e-03    3      1000022         1        -1             
      3.74422040e-03    3      1000022         3        -3             
      3.72444680e-03    3      1000022         5        -5             
      2.88685800e-02    3      1000022         6        -6             
      1.03701380e-03    2      1000023        21                       
      1.55880310e-03    3      1000023         1        -1             
      1.55880310e-03    3      1000023         3        -3             
      2.44550450e-03    3      1000023         5        -5             
      1.42550010e-01    3      1000023         6        -6             
      1.15311970e-03    2      1000025        21                       
      5.00635620e-05    3      1000025         1        -1             
      5.00635620e-05    3      1000025         3        -3             
      1.03025280e-03    3      1000025         5        -5             
      1.70376180e-01    3      1000025         6        -6             
      1.87741130e-05    2      1000035        21                       
      1.83864780e-02    3      1000035         1        -1             
      1.83864780e-02    3      1000035         3        -3             
      1.83069090e-02    3      1000035         5        -5             
      1.07848490e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  3.13145250e-01   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.29992010e-01    2      1000022        23                       
      7.65035090e-01    2      1000022        25                       
      1.50588810e-02    2      2000011       -11                       
      1.50588810e-02    2     -2000011        11                       
      1.50588730e-02    2      2000013       -13                       
      1.50588730e-02    2     -2000013        13                       
      2.23686970e-02    2      1000015       -15                       
      2.23686970e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  3.26520488e-01   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.59170790e-01    2      1000022        23                       
      1.07803320e-01    2      1000022        25                       
      2.42443800e-03    2      2000011       -11                       
      2.42443800e-03    2     -2000011        11                       
      2.42443690e-03    2      2000013       -13                       
      2.42443690e-03    2     -2000013        13                       
      1.16641570e-02    2      1000015       -15                       
      1.16641570e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  4.51440329e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.83356880e-01    2      1000024       -24                       
      2.83356880e-01    2     -1000024        24                       
      1.26858990e-03    2      1000022        23                       
      6.30557720e-03    2      1000023        23                       
      2.21331670e-01    2      1000025        23                       
      2.35305840e-03    2      1000022        25                       
      1.97988350e-01    2      1000023        25                       
      3.12936910e-03    2      1000025        25                       
      1.15269520e-04    2      2000011       -11                       
      1.15269520e-04    2     -2000011        11                       
      1.15269520e-04    2      2000013       -13                       
      1.15269520e-04    2     -2000013        13                       
      2.24466740e-04    2      1000015       -15                       
      2.24466740e-04    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  2.87473795e-01   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.83521160e-01    2      1000022        24                       
      1.64788070e-02    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  4.51471294e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.31112070e-03    2      1000022        24                       
      2.85283090e-01    2      1000023        24                       
      2.40272910e-01    2      1000025        24                       
      2.19502280e-04    2     -1000015        16                       
      2.69731820e-01    2      1000024        23                       
      2.01181590e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56196285e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96554200e-04    2           13       -13                       
      5.62219580e-02    2           15       -15                       
      2.35940730e-03    2            3        -3                       
      7.69095180e-01    2            5        -5                       
      3.34392450e-02    2            4        -4                       
      1.90688960e-03    2           22        22                       
      3.96455940e-02    2           21        21                       
      4.93629600e-03    3           24        11       -12             
      4.93629600e-03    3           24        13       -14             
      4.93629600e-03    3           24        15       -16             
      1.48088880e-02    3           24        -2         1             
      1.48088880e-02    3           24        -4         3             
      4.93629600e-03    3          -24       -11        12             
      4.93629600e-03    3          -24       -13        14             
      4.93629600e-03    3          -24       -15        16             
      1.48088880e-02    3          -24         2        -1             
      1.48088880e-02    3          -24         4        -3             
      5.66388600e-04    3           23        12       -12             
      5.66388600e-04    3           23        14       -14             
      5.66388600e-04    3           23        16       -16             
      2.85057730e-04    3           23        11       -11             
      2.85057730e-04    3           23        13       -13             
      2.85057730e-04    3           23        15       -15             
      9.76583570e-04    3           23         2        -2             
      9.76583570e-04    3           23         4        -4             
      1.25808010e-03    3           23         1        -1             
      1.25808010e-03    3           23         3        -3             
      1.25808010e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  3.10808896e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.35960160e-05    2           13       -13                       
      2.39427310e-02    2           15       -15                       
      9.79987790e-04    2            3        -3                       
      2.41652910e-01    2            5        -5                       
      4.02201620e-01    2            6        -6                       
      2.11806440e-04    2           21        21                       
      1.36367730e-03    2           24       -24                       
      6.92367380e-04    2           23        23                       
      8.03130400e-03    2      1000022   1000022                       
      8.12625960e-02    2      1000022   1000023                       
      1.86852660e-01    2      1000022   1000025                       
      1.71066170e-04    2      1000022   1000035                       
      5.47930230e-03    2      1000023   1000023                       
      1.33335560e-02    2      1000023   1000025                       
      6.59588900e-04    2      1000025   1000025                       
      2.72773060e-02    2      1000024  -1000024                       
      5.39088340e-03    2           25        25                       
      1.40406720e-04    2      2000011  -2000011                       
      1.40377730e-04    2      2000013  -2000013                       
      1.32233920e-04    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  3.68244377e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.01447570e-05    2           13       -13                       
      2.00904090e-02    2           15       -15                       
      8.22348400e-04    2            3        -3                       
      2.02959430e-01    2            5        -5                       
      3.58840940e-01    2            6        -6                       
      2.13552750e-04    2           21        21                       
      9.84708590e-03    2      1000022   1000022                       
      1.73477950e-01    2      1000022   1000023                       
      5.57144580e-02    2      1000022   1000025                       
      3.43228650e-02    2      1000023   1000023                       
      6.55623390e-04    2      1000023   1000025                       
      1.41952630e-03    2      1000025   1000025                       
      1.40459180e-01    2      1000024  -1000024                       
      1.10661560e-03    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  2.91988289e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.93186440e-05    2           14       -13                       
      2.55820750e-02    2           16       -15                       
      9.70754130e-04    2            4        -3                       
      6.32678510e-01    2            6        -5                       
      2.97170490e-01    2      1000024   1000022                       
      5.11976840e-03    2      1000024   1000023                       
      3.67151540e-02    2      1000024   1000025                       
      2.46170470e-04    2      1000037   1000022                       
      1.42779620e-03    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
