from GeneratorFilters.GeneratorFiltersConf import ForwardProtonFilter

topAlg += ForwardProtonFilter()
topAlg.ForwardProtonFilter.xi_min = 0.010
topAlg.ForwardProtonFilter.xi_max = 0.25
topAlg.ForwardProtonFilter.pt_min = 0.
topAlg.ForwardProtonFilter.pt_max = 1.5*GeV
topAlg.ForwardProtonFilter.beam_energy = runArgs.ecmEnergy/2.*GeV
