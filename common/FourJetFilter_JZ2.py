## Truth four jet filter config for JZ2
include("MC12JobOptions/FourJetFilter_JZX_Fragment.py")
topAlg.QCDTruthMultiJetFilter.MinLeadJetPt = 80.*GeV
topAlg.QCDTruthMultiJetFilter.MaxLeadJetPt = 150.*GeV
