## POWHEG+Pythia8 config with the AU2 CT10 UE tune
## DEPRECATED: Prefer to explicitly use the two lines below in JOs
include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Powheg.py")
