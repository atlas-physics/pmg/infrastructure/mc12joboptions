## PYTHIA-B using CTEQ6L with the UE tuning by Arthur Moraes
# TODO: This tune is massively defunct and should be removed.
# TODO: Use standard atlasTune names via standard Pythia tune config includes... requires alg name change
topAlg.PythiaB.PythiaCommand += [
    "pysubs ckin 9 -4.5",
    "pysubs ckin 10 4.5",
    "pysubs ckin 11 -4.5",
    "pysubs ckin 12 4.5",
    "pydat1 mstj 26 0",
    "pydat1 mstj 22 2",
    "pydat1 parj 13 0.65",
    "pydat1 parj 14 0.12",
    "pydat1 parj 15 0.04",
    "pydat1 parj 16 0.12",
    "pydat1 parj 17 0.2",
    "pydat1 parj 55 -0.006",
    ]
