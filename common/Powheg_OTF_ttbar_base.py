##--------------------------------------------------------------
##
## Base Job options file for POWHEG OTF setup
## Author: Thomas Neep 12.2.2014 <tneep@cern.ch>
##
##--------------------------------------------------------------

include('PowhegControl/PowhegControl_hvq_Common.py')
PowhegConfig.energy = 4000
PowhegConfig.quarkMass = 172.5
PowhegConfig.pdf = 10800
PowhegConfig.withnegweights = 0

PowhegConfig.topdecaymode = 22222
PowhegConfig.tdec_wmass = 80.399
PowhegConfig.tdec_wwidth = 2.085
PowhegConfig.tdec_bmass = 4.95
PowhegConfig.tdec_twidth = 1.3200
PowhegConfig.tdec_elbranching = 0.108
PowhegConfig.tdec_emass = 0.00051
PowhegConfig.tdec_mumass = 0.1057
PowhegConfig.tdec_taumass = 1.777
PowhegConfig.tdec_dmass = 0.320
PowhegConfig.tdec_umass = 0.320
PowhegConfig.tdec_smass = 0.5
PowhegConfig.tdec_cmass = 1.55
PowhegConfig.tdec_sin2cabibbo = 0.051

PowhegConfig.foldcsi = 1
PowhegConfig.foldy = 1
PowhegConfig.foldphi = 1

PowhegConfig.ncall1 = 10000
PowhegConfig.ncall2 = 100000
PowhegConfig.nubound = 100000

PowhegConfig.nEvents = 15000 #Using a 1 lepton filter approx. 60% efficient (make sure there are enough events for the shower
PowhegConfig.outputEventsName = "events.lhe" #Needed to set this... not sure if it recommended
