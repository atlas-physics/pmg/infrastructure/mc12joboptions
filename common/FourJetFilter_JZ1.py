## Truth four jet filter config for JZ1
include("MC12JobOptions/FourJetFilter_JZX_Fragment.py")
topAlg.QCDTruthMultiJetFilter.MinLeadJetPt = 10.*GeV
topAlg.QCDTruthMultiJetFilter.MaxLeadJetPt = 80.*GeV
