
## Instantiate the lepton filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
# TruthJet filter
try:
    from JetRec.JetGetters import *
#    c4=make_StandardJetGetter('Cone',0.4,'Truth')
    c4=make_StandardJetGetter('AntiKt',0.4,'Truth')
    c4alg = c4.jetAlgorithmHandle()
    c4alg.JetFinalEtCut.MinimumSignal = 10.*GeV
except Exception, e:
    pass


if not hasattr(topAlg, "VBFForwardJetsFilter"):
    from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
    topAlg += VBFForwardJetsFilter()


## Add this filter to the algs required to be successful for streaming
if "VBFForwardJetsFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["VBFForwardJetsFilter"]
