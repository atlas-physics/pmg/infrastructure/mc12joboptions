## Configuration for MEtop+PYTHIA 6 using the AUET2B_CTEQ6L1 tune
include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
evgenConfig.generators = [ "MEtop", "Pythia"]

## Read external Les Houches event file
topAlg.Pythia.PythiaCommand += ["pyinit user lhef"]

