runconf=runArgs.jobConfig[0].split("_")

mHconf=runconf[2].replace("H","")
maconf=runconf[3].replace("aa","").replace("p",".")

evgenConfig.description = "ggF H "+mHconf+" GeV, with the AU2 CTEQ6L1 tune, to aa "+maconf+" GeV, to 2mu2tau"
evgenConfig.keywords = ["NMSSM", "Haa", "2mu2tau"]

#
if (float(maconf) > 10.1 and float(maconf) <= 20.0):
    evgenConfig.minevents = 500
elif (float(maconf) >= 30.0 and float(maconf) <= 50.0):
    evgenConfig.minevents = 200
elif float(maconf) >= 60.0:
    evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["Higgs:useBSM on", 
#                          "HiggsBSM:allH2 on", #all
                           "HiggsBSM:gg2H2 on", #gg fusion
#                          "HiggsBSM:ff2H2ff(t:ZZ) on", "HiggsBSM:ff2H2ff(t:WW) on", #VBF
#                          "HiggsBSM:ffbar2H2W on", #WH
#                          "HiggsBSM:ffbar2H2Z on", #ZH
                            "35:m0 "+mHconf+".0", #H0 mass
                            "35:mWidth 0.01", #narrow width
                            "35:doForceWidth = on", #force narrow width
                            "35:onMode = off", #turn off all H0 decays
                            "35:onIfMatch = 36 36", #H0->aa
                            "36:onMode = off", #turn off all a decays
                            "36:onIfAny = 13", #a->mumu
                            "36:onIfAny = 15", #a->tautau
                            "36:m0 "+maconf+".0", #a mass
                            "36:tau0 0", #a lifetime
#                            "36:7:bRatio = 0.05", #a->mumu BR
#                            "36:7:meMode = 100", #a->mumu BR
                            ]

from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()
MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 2
MultiElecMuTauFilter.MinPt = 15000.
MultiElecMuTauFilter.MaxEta = 2.5
MultiElecMuTauFilter.IncludeHadTaus = 0

from GeneratorFilters.GeneratorFiltersConf import DiLeptonMassFilter
topAlg += DiLeptonMassFilter()
DiLeptonMassFilter = topAlg.DiLeptonMassFilter
DiLeptonMassFilter.MinPt = 15000
DiLeptonMassFilter.MaxEta = 2.5
DiLeptonMassFilter.MinMass = 3000
DiLeptonMassFilter.MinDilepPt = 30000
DiLeptonMassFilter.AllowElecMu = False
DiLeptonMassFilter.AllowSameCharge = False
if float(maconf) > 10.1:
    DiLeptonMassFilter.MaxMass = 61000
else:
    DiLeptonMassFilter.MaxMass = 11000

StreamEVGEN.RequireAlgs += [ "MultiElecMuTauFilter","DiLeptonMassFilter" ]
