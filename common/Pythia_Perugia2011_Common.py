## PYTHIA 6 config with Perugia2011 tune (CTEQ5L PDF)
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_350"
evgenConfig.tune = "Perugia2011"
