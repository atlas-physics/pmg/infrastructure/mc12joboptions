## HT filter setup for anti-kT R=0.4 truth jets

from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth',
                              #useInteractingOnly=False, # to include neutrinos
                              #includeMuons=False, # to include muons
                              disable=False).jetAlgorithmHandle()
# Would be nice to have a handle to exclude electrons as well!

from GeneratorFilters.GeneratorFiltersConf import HTFilter
if "HTFilter" not in topAlg:
    topAlg += HTFilter()
if "HTFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["HTFilter"]

topAlg.HTFilter.MinJetPt = 20.*GeV # Min pT to consider jet in HT
topAlg.HTFilter.MaxJetEta = 999. # Max eta to consider jet in HT
topAlg.HTFilter.MinHT = 200.*GeV # Min HT to keep event
topAlg.HTFilter.MaxHT = 1000.*GeV # Max HT to keep event
topAlg.HTFilter.TruthJetContainer = "AntiKt4TruthJets" # Which jets to use for HT
topAlg.HTFilter.UseNeutrinos = False # Include neutrinos from the MC event in the HT
topAlg.HTFilter.UseLeptons = True # Include e/mu from the MC event in the HT

