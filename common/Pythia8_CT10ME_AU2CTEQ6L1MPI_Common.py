## Config for Py8 with the NLO CT10 PDF for the ME, and tune AU2 CTEQ6L1 for the MPI and shower
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
topAlg.Pythia8.Commands += [
    "PDF:useHard = on",
    "PDF:useHardLHAPDF = on",
    "PDF:hardLHAPDFset = CT10.LHgrid"]
