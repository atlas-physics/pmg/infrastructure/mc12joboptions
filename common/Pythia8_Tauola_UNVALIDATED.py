## Tauola++ tau decay config for Pythia8

## Disable native tau decays
assert hasattr(topAlg, "Pythia8")
topAlg.Pythia8.Commands += ["15:onMode = off"]

## Enable Tauola++
include("MC12JobOptions/Tauolapp_Fragment.py")
