###############################################################
#
# Job options file for Evgen Single Monopole Generation
# W. Taylor, 2014-01-23
#==============================================================

PDG = 4110000

loE = (float(mass) + 10.)*1000.
hiE  = (float(mass) + 3000.)*1000.

#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include ( "MC12JobOptions/ParticleGenerator_Common.py" )

# For VERBOSE output from ParticleGenerator.
topAlg.ParticleGenerator.OutputLevel = 1

topAlg.ParticleGenerator.orders = [
 "PDGcode: sequence -"+str(PDG)+" "+str(PDG),
 "energy: flat %s %s" % (loE,hiE),
 "eta: flat -3.0 3.0",
 "phi: flat -3.14159 3.14159"
 ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.description = "Single magnetic monopole generation for Mass=%s, Gcharge=%s in MC12" % (mass,gcharge)
evgenConfig.keywords = ["exotics", "Monopole", "Single"]
evgenConfig.contact = ["wtaylor@cern.ch"]

evgenConfig.specialConfig = 'MASS=%s;GCHARGE=%s;preInclude=SimulationJobOptions/preInclude.Monopole.py' % (mass,gcharge)

#--------------------------------------------------------------
# Edit PDGTABLE.MeV with monopole mass
#--------------------------------------------------------------
ALINE1="M 4110000                          %s.E+03       +0.0E+00 -0.0E+00 Monopole        0" % (mass)
ALINE2="W 4110000                          0.E+00         +0.0E+00 -0.0E+00 Monopole        0"

import os
import sys

pdgmod = os.path.isfile('PDGTABLE.MeV')
if pdgmod is True:
    os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
f=open('PDGTABLE.MeV','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2

