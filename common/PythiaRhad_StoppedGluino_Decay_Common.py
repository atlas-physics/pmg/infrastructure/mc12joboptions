## Stopped gluino evgen job options fragment
# TODO: Reduce production/decay/R-hadron fragment duplication

include("MC12JobOptions/PythiaRhad_Common.py")

if CASE == 'gluino':
    topAlg.PythiaRhad.RunGluinoHadrons = True
    topAlg.PythiaRhad.RunStopHadrons = False
    topAlg.PythiaRhad.RunSbottomHadrons = False
    topAlg.PythiaRhad.randomtshift = 50 # +-X ns, overrides tshift if non-zero
    topAlg.PythiaRhad.rh_decay = True
    topAlg.PythiaRhad.strip_out_rh = True
    topAlg.PythiaRhad.boost_rh_to_rest_frame = True
    topAlg.PythiaRhad.rotate_rh = True
    topAlg.PythiaRhad.translate_rh_to_stopping_position = True
    topAlg.PythiaRhad.StoppingInput = [ [ 0,0,0,0,0,0 ] ]
    try:
        include("StoppingInput.txt")
    except:
        pass
    topAlg.PythiaRhad.PythiaCommand += [
        "pymssm imss 1 1", "pymssm imss 3 1", "pymssm imss 5 1",
        "pymssm rmss 3 %d.0"+str(MASS)+".0", "pymssm rmss 1 "+str(MASSX)+".0", # gluino and neutralino masses
        "pymssm rmss 2 3000.0", "pymssm rmss 4 10000.0",
        "pymssm rmss 7 4000.0", "pymssm rmss 8 4000.0",
        "pymssm rmss 9 4000.0", "pymssm rmss 10 4000.0",
        "pymssm rmss 11 4000.0", "pymssm rmss 12 5000.0",
        ## Decays into gluons
        "pymssm rmss 21 "+str(MASSX)+".0e9", "pymssm imss 11 1", #ok to use the Gravitino
        "pymssm rmss 29 2.0e6", # planck mass, controls BR(gluino->g+Gravitino)
        "pydat3 mdcy 1000022 1 0", # kill neutralino decays
        #"pydat1 mstj 45 6", # allow CMshower->ttbar in gluino decays
        #"pydat1 mstj 43 1", # z definition in CM shower
        "pysubs msel 0", "pysubs msub 243 1", "pysubs msub 244 1", "pypars mstp 111 0",
        "pyinit pylisti 12", # dumps the full decay table, etc.
        #"pyinit pylistf 1", # dumps pythia event
        "pyinit dumpr 0 100" #,"pystat 2"
        ]
    if 'GBALLPROB' in globals():
        topAlg.PythiaRhad.GluinoBallProbability = GBALLPROB
    elif CASE == 'stop':
        topAlg.PythiaRhad.RunGluinoHadrons = False
        topAlg.PythiaRhad.RunStopHadrons = True
        topAlg.PythiaRhad.RunSbottomHadrons = False
    elif CASE == 'sbottom':
        topAlg.PythiaRhad.RunGluinoHadrons = False
        topAlg.PythiaRhad.RunStopHadrons = False
        topAlg.PythiaRhad.RunSbottomHadrons = True
