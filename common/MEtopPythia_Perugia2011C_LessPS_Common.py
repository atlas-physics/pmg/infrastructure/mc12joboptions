## Configuration for MEtop+PYTHIA using the Perugia2011C LessPS tune
include("MC12JobOptions/Pythia_CTEQ6L1_Perugia2011C_LessPS_Common.py")
evgenConfig.generators = [ "MEtop", "Pythia"]

## Read external Les Houches event file
topAlg.Pythia.PythiaCommand += ["pyinit user lhef"]

