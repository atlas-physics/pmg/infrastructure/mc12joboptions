## Set up PythiaTopMdiff

# TODO: Can we do something to make PythiaTopMdiff available as topAlg.Pythia?
from PythiaExo_i.PythiaTopMdiff_iConf import PythiaTopMdiff
topAlg += PythiaTopMdiff()

# Use the AUET2B LO** tune
topAlg.Pythia.Tune_Name="ATLAS_20110002"
topAlg.Pythia.PythiaCommand = [
    # Initializations
    "pyinit pylisti 12",
    "pyinit pylistf 1",
    "pystat 1 3 4 5",
    "pyinit dumpr 1 5",
    # Mass
    "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
    "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
    # The settings below have no effect (widths calculated perturbatively in Pythia)
    #"pydat2 pmas 24 2 2.085",   # PDG2010 W width
    #"pydat2 pmas 23 2 2.4952",  # PDG2010 Z0 width
    ]

## Alias currently needed for further config
# TODO: Eliminate this: see TODO above for suggested route
Pythia = topAlg.PythiaTopMdiff
