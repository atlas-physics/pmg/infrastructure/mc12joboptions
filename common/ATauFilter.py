## Instantiate the ATau filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "ATauFilter"):
    from GeneratorFilters.GeneratorFiltersConf import ATauFilter
    topAlg += ATauFilter()



## Add this filter to the algs required to be successful for streaming
if "ATauFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["ATauFilter"]
