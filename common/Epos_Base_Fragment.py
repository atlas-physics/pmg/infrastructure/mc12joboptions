## Base config for Epos
from Epos_i.Epos_iConf import Epos
topAlg += Epos("Epos")
evgenConfig.generators += ["Epos"]

topAlg.Epos.BeamMomentum     = -runArgs.ecmEnergy/2.0
topAlg.Epos.TargetMomentum   = runArgs.ecmEnergy/2.0
topAlg.Epos.PrimaryParticle  = 1
topAlg.Epos.TargetParticle   = 1
topAlg.Epos.Model            = 0
topAlg.Epos.ParamFile        = "epos_crmc.param"

## Get files from the InstallArea
import os
os.system("get_files %s" % topAlg.Epos.ParamFile)
inputFiles = "qgsjet.dat \
              qgsjet.ncs \
              sectnu-II-03 \
              epos.initl \
              epos.iniev \
              epos.inirj \
              epos.inics \
              epos.inirj.lhc \
              epos.inics.lhc"
if not os.path.exists("tabs"):
    os.mkdir("tabs")
os.system("get_files %s" % inputFiles)
os.system("mv %s tabs/" % inputFiles)
