## Functions to construct Herwig++ configuration for UED signal generation

kkparticles = {}

## KKGluon
kkparticles['KKgluon']	  = ['KK1_g']

## KKquarks
kkparticles['KKupL']      = ['KK1_u_L','KK1_u_Lbar']
kkparticles['KKdownL']    = ['KK1_d_L','KK1_d_Lbar']
kkparticles['KKcharmL']   = ['KK1_c_L','KK1_c_Lbar']
kkparticles['KKstrangeL'] = ['KK1_s_L','KK1_s_Lbar']
kkparticles['KKbottom1']  = ['KK1_b_1','KK1_b_1bar']
kkparticles['KKtop1']     = ['KK1_t_1','KK1_t_1bar']
#
kkparticles['KKupR']      = ['KK1_u_R','KK1_u_Rbar']
kkparticles['KKdownR']    = ['KK1_d_R','KK1_d_Rbar']
kkparticles['KKcharmR']   = ['KK1_c_R','KK1_c_Rbar']
kkparticles['KKstrangeR'] = ['KK1_s_R','KK1_s_Rbar']
kkparticles['KKbottom2']  = ['KK1_b_2','KK1_b_2bar']
kkparticles['KKtop2']     = ['KK1_t_2','KK1_t_2bar']
## Collections of squarks
kkparticles['KKquarksG1_L'] = kkparticles['KKupL']+kkparticles['KKdownL']
kkparticles['KKquarksG1_R'] = kkparticles['KKupR']+kkparticles['KKdownR']
kkparticles['KKquarksG2_L'] = kkparticles['KKcharmL']+kkparticles['KKstrangeL']
kkparticles['KKquarksG2_R'] = kkparticles['KKcharmR']+kkparticles['KKstrangeR']
#
kkparticles['KKbottoms']    = kkparticles['KKbottom1']+kkparticles['KKbottom2']
kkparticles['KKtops']       = kkparticles['KKtop1']+kkparticles['KKtop2']
#
kkparticles['KKquarks_L']   = kkparticles['KKquarksG1_L']+kkparticles['KKquarksG2_L']
kkparticles['KKquarks_R']   = kkparticles['KKquarksG1_R']+kkparticles['KKquarksG2_R']
kkparticles['KKquarks']     = kkparticles['KKquarks_L']+kkparticles['KKquarks_R']

## Gauginos
kkparticles['KKbosons'] = ['KK1_Z0','KK1_W+','KK1_W-','KK1_gamma']

## Sleptons
kkparticles['KKelectronL'] = ['KK1_e_L-','KK1_e_L+']
kkparticles['KKmuonL']     = ['KK1_mu_L-','KK1_mu_L+']
kkparticles['KKtau1']      = ['KK1_tau_1-','KK1_tau_1+']
#
kkparticles['KKelectronR'] = ['KK1_e_R-','KK1_e_R+']    
kkparticles['KKmuonR']     = ['KK1_mu_R-','KK1_mu_R+']  
kkparticles['KKtau2']      = ['KK1_tau_2-','KK1_tau_2+']
## Collections of sleptons
kkparticles['KKleptons_L'] = kkparticles['KKelectronL']+kkparticles['KKmuonL']
kkparticles['KKleptons_R'] = kkparticles['KKelectronR']+kkparticles['KKmuonR']
kkparticles['KKtaus']      = kkparticles['KKtau1']+kkparticles['KKtau2']
kkparticles['KKleptons']   = kkparticles['KKleptons_L']+kkparticles['KKleptons_R']

## Sneutrinos
kkparticles['KK_nu_eL']   = ['KK1_nu_eL',  'KK1_nu_eLbar']
kkparticles['KK_nu_muL']  = ['KK1_nu_muL', 'KK1_nu_muLbar']
kkparticles['KK_nu_tauL'] = ['KK1_nu_tauL','KK1_nu_tauLbar']
## Collections of sneutrinos
kkparticles['KKneutrinos'] = kkparticles['KK_nu_eL']+kkparticles['KK_nu_muL']+kkparticles['KK_nu_tauL']

def addOutgoingKKparticle(index, kkparticle):
    cmdline = """insert HPConstructor:Outgoing %(index)d /Herwig/Particles/%(kkparticle)s""" % \
        {'index':index,'kkparticle':kkparticle}
    return cmdline


def buildHerwigppCommands(kkparticle_list, InverseRadius, LambdaR):
    header = """
## Generate the process in MSSM equivalent to 2-parton -> 2-kkparticle processes in Fortran Herwig
## Read the MSSM model details
read MUED.model
cd /Herwig/NewPhysics
"""
    process_mode = """
set MUED/Model:InverseRadius %d*GeV
set MUED/Model:LambdaR %d
set /Herwig/Particles/h0:NominalMass 125*GeV
""" % (InverseRadius, LambdaR)

    incoming_partons = """
#incoming parton
insert HPConstructor:Incoming 0 /Herwig/Particles/g
insert HPConstructor:Incoming 1 /Herwig/Particles/u
insert HPConstructor:Incoming 2 /Herwig/Particles/ubar
insert HPConstructor:Incoming 3 /Herwig/Particles/d
insert HPConstructor:Incoming 4 /Herwig/Particles/dbar
insert HPConstructor:Incoming 5 /Herwig/Particles/s
insert HPConstructor:Incoming 6 /Herwig/Particles/sbar
insert HPConstructor:Incoming 7 /Herwig/Particles/c
insert HPConstructor:Incoming 8 /Herwig/Particles/cbar
insert HPConstructor:Incoming 9 /Herwig/Particles/b
insert HPConstructor:Incoming 10 /Herwig/Particles/bbar
"""
    kkparticle_flatlist = []
    for kkparty in kkparticle_list:
        kkparticle_flatlist += kkparticles[kkparty]
    ## A slightly clunky way of removing duplicates
    helperdict = {}
    for kkparticle in kkparticle_flatlist:
        helperdict[kkparticle] = 1
    kkparticle_flatlist = helperdict.keys()

    outgoing_kkparticles = """
#outgoing kkparticles
"""
    for index,kkparticle in enumerate(kkparticle_flatlist):
        outgoing_kkparticles += addOutgoingKKparticle(index,kkparticle)
        outgoing_kkparticles += '\n'

    ## get model file via the auxfiles mechanism
    evgenConfig.auxfiles += [ 'MUED.model' ]

    cmd = header + process_mode + incoming_partons + outgoing_kkparticles
    return cmd
