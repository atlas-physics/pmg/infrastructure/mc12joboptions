## Instantiate the lepton filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "FourLeptonMassFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import FourLeptonMassFilter
    topAlg += FourLeptonMassFilter()

## Add this filter to the algs required to be successful for streaming
if "FourLeptonMassFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["FourLeptonMassFilter"]

## Default cut params
