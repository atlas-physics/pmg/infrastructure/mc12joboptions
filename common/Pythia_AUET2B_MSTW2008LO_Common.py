## PYTHIA 6 config with AUET2B-MSTW2008LO tune
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "ATLAS_20110004"
evgenConfig.tune = "AUET2B MSTW2008LO"
