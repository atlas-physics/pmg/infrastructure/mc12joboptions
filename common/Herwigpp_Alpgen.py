# Common job options for Alpgen+Herwig++
# Contact: m.k.bugge@fys.uio.no

#Set up Herwig++ to process Alpgen inputs
topAlg.Herwigpp.doAlpgen = True

#Put Alpgen in the generators list
evgenConfig.generators = ['Alpgen', 'Herwigpp']
