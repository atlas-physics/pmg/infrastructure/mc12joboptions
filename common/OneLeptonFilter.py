
## Instantiate the lepton filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "WZtoLeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import WZtoLeptonFilter
    topAlg += WZtoLeptonFilter()
    
## Add this filter to the algs required to be successful for streaming
if "WZtoLeptonFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["WZtoLeptonFilter"]

## Cut parameters to keep events with at least one lepton
topAlg.WZtoLeptonFilter.NeedWZleps = 1
topAlg.WZtoLeptonFilter.ElectronMuonNumber = 0
topAlg.WZtoLeptonFilter.BCKGvsSIGNAL = 0
topAlg.WZtoLeptonFilter.IdealReconstructionEfficiency = 0
topAlg.WZtoLeptonFilter.Etacut_electron = 2.6
topAlg.WZtoLeptonFilter.Etacut_muon = 2.6
