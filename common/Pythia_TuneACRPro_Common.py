## PYTHIA 6 config with Tune A-Pro [117] (Tune ACR, with LEP tune from Professor)
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_117"
evgenConfig.tune = "ACR-Pro"
