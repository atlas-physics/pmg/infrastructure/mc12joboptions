## Base config for HERWIG 6.5 + Jimmy 4.31 using the AUET2 CTEQ66 tune
from Herwig_i.Herwig_iConf import Herwig
topAlg += Herwig()
evgenConfig.generators += ["Herwig"]

include("MC12JobOptions/Jimmy_Base_Fragment.py")
include("MC12JobOptions/Jimmy_AUET2B_CTEQ66_Fragment.py")
