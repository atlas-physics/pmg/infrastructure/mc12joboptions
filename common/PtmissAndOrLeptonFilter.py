if not hasattr(topAlg, "PtmissAndOrLeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import PtmissAndOrLeptonFilter
    topAlg += PtmissAndOrLeptonFilter()

if "PtmissAndOrLeptonFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["PtmissAndOrLeptonFilter"]

topAlg.PtmissAndOrLeptonFilter.PtmissANDLepton = False
topAlg.PtmissAndOrLeptonFilter.PtminElectron = 5*GeV
topAlg.PtmissAndOrLeptonFilter.MaxEtaElectron = 2.8
topAlg.PtmissAndOrLeptonFilter.PtminMuon = 5*GeV
topAlg.PtmissAndOrLeptonFilter.MaxEtaMuon = 2.8
topAlg.PtmissAndOrLeptonFilter.PtminLostTrack = 5*GeV
topAlg.PtmissAndOrLeptonFilter.MinEtaLost = 100.
topAlg.PtmissAndOrLeptonFilter.PtminMissing = 80*GeV
