include("MC12JobOptions/Boosted2DijetFilter_Fragment.py")

#pT cuts are in MeV
topAlg.Boosted2DijetFilter.DijetPtMin = 160000.
topAlg.Boosted2DijetFilter.LargeRJetPtMin = 160000.
topAlg.Boosted2DijetFilter.JetPtMin = 25000.
topAlg.Boosted2DijetFilter.JetEtaMax = 2.8
topAlg.Boosted2DijetFilter.DijetDRMax = 1.8

try:
    StreamEVGEN.RequireAlgs +=  [ "Boosted2DijetFilter" ]
except Exception, e:
    pass
