if not hasattr(topAlg, "ParentsTracksFilter"):
    from GeneratorFilters.GeneratorFiltersConf import ParentsTracksFilter
    topAlg += ParentsTracksFilter()

## Add this filter to the algs required to be successful for streaming
if "ParentsTracksFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["ParentsTracksFilter"]

## Default cut params
topAlg.ParentsTracksFilter.PDGParent  = [511,521,531,541,5122,5132,5232,5332]
topAlg.ParentsTracksFilter.PtMinParent =  0.
topAlg.ParentsTracksFilter.PtMaxParent =  1e9
topAlg.ParentsTracksFilter.MassMinParent = -1e9
topAlg.ParentsTracksFilter.MassMaxParent =  1e9
topAlg.ParentsTracksFilter.EtaRangeParent = 10.0
topAlg.ParentsTracksFilter.PtMinLeptons = 0.
topAlg.ParentsTracksFilter.EtaRangeLeptons = 1000.
topAlg.ParentsTracksFilter.PtMinHadrons = 0.
topAlg.ParentsTracksFilter.EtaRangeHadrons = 1000.
topAlg.ParentsTracksFilter.NumMinTracks = 0
topAlg.ParentsTracksFilter.NumMaxTracks = 999999
topAlg.ParentsTracksFilter.NumMinLeptons = 0
topAlg.ParentsTracksFilter.NumMaxLeptons = 999999
topAlg.ParentsTracksFilter.NumMinOthers = 0
topAlg.ParentsTracksFilter.NumMaxOthers = 999999
