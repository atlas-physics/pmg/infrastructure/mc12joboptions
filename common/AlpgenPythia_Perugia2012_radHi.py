#-----------------------------------------------------------------------------
#
# Generic generation with Alpgen2.1.4_p3 + Pythia6.427 in MC12
# copy of Generators/AlpGenControl/trunk/share/MC12.AlpGenPythia.py
#-----------------------------------------------------------------------------

from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_371"
evgenConfig.tune = "Perugia2012radHi"

evgenConfig.generators += ["Alpgen"]

topAlg.Pythia.PythiaCommand += ["pyinit user alpgen",
                                "pydat1 parj 90 20000.",
                                "pydat3 mdcy 15 1 0",
                                "pypars mstp 143 1"
                                ]

include ( "MC12JobOptions/Tauola_Fragment.py" )
include ( "MC12JobOptions/Photos_Fragment.py" )
