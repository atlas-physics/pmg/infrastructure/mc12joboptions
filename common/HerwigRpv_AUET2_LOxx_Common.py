## Base config for HerwigRpv
## Per-energy UE tuning parameters are specified in energy-specific common fragments

from Herwig_i.HerwigRpv_iConf import HerwigRpv
topAlg += HerwigRpv("Herwig")
evgenConfig.generators += ["HerwigRpv"]

include("MC12JobOptions/Jimmy_Base_Fragment.py")
include("MC12JobOptions/Jimmy_AUET2_LOxx_Fragment.py")
