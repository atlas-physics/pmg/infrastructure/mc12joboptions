## Run EvtGen afterburner on top of Pythia 8
assert hasattr(topAlg, "Pythia8")
include("MC12JobOptions/EvtGen_Fragment.py")
evgenConfig.auxfiles += ['inclusiveP8.pdt']
topAlg.EvtInclusiveDecay.pdtFile = "inclusiveP8.pdt"

# FHerwig has problems with omega b* (5334), so not present in the base EvtGen fragment.  Add it here.
topAlg.EvtInclusiveDecay.whiteList+=[-5334, 5334]


