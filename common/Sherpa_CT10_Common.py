## Sherpa config with CT10 PDF
include("MC12JobOptions/Sherpa_Base_Fragment.py")

# CT10 is Sherpa 1.4.0's default PDF/tune

evgenConfig.tune = "CT10"
