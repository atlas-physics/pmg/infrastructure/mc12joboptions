## Instantiate the lepton filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "MultiElecMuTauFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
    topAlg += MultiElecMuTauFilter()

## Add this filter to the algs required to be successful for streaming
if "MultiElecMuTauFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["MultiElecMuTauFilter"]

## Default cut params
