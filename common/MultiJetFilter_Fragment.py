## Default truth multi-jet filter setup
## The specified truth jet container must exist

from GeneratorFilters.GeneratorFiltersConf import QCDTruthMultiJetFilter
if "QCDTruthMultiJetFilter" not in topAlg:
    topAlg += QCDTruthMultiJetFilter()
if "QCDTruthMultiJetFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["QCDTruthMultiJetFilter"]


topAlg.QCDTruthMultiJetFilter.Njet = -1
topAlg.QCDTruthMultiJetFilter.NjetMinPt = -1.*GeV
topAlg.QCDTruthMultiJetFilter.MinLeadJetPt = -1.*GeV
topAlg.QCDTruthMultiJetFilter.MaxLeadJetPt = float(runArgs.ecmEnergy)*GeV
topAlg.QCDTruthMultiJetFilter.MaxEta = 999.
topAlg.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt6TruthJets"
topAlg.QCDTruthMultiJetFilter.DoShape = False
