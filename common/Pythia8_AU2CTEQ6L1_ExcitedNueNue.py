evgenConfig.description = "Excited neutrino, with the CTEQ6L1 tune"
evgenConfig.keywords = ["excitedlepton",'excitedneutrino','neutrino']
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact = ["Olya Igonkina"]

runDefaultPythia=False
#Excited Lepton ID
#leptID = 4000011
#

# Excited Neutrino Mass (in GeV)
#M_ExNu = 1000.0

# Mass Scale parameter (Lambda, in GeV)
#M_Lam = 5000.0

# Coupling constant
f = 1.0
fPrime = 1.0


include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

if not runDefaultPythia :
    if leptID == 4000011 :
        topAlg.Pythia8.UserProcess = "qqbar2eStareStarBar"
    elif leptID == 4000013 :
        topAlg.Pythia8.UserProcess = "qqbar2muStarmuStarBar"
    elif leptID == 4000015 :
        topAlg.Pythia8.UserProcess = "qqbar2tauStartauStarBar"
    elif leptID == 4000012 :
        topAlg.Pythia8.UserProcess = "qqbar2nueStarnueStarBar"
    elif leptID == 4000014 :
        topAlg.Pythia8.UserProcess = "qqbar2numuStarnumuStarBar"
    elif leptID == 4000016 :
        topAlg.Pythia8.UserProcess = "qqbar2nutauStarnutauStarBar"
    else :
        from AthenaCommon.Logging import logging
        logging.error("Can not generate this process for particle "+str(leptID))



    if leptID > 0 :
        SMleptID=leptID-4000000
        if SMleptID% 2 == 1 :
            WleptID=-24
            SMPairleptID = SMleptID+1
        else                :
            SMPairleptID = SMleptID-1
            WleptID=24

    else :
        SMleptID=leptID+4000000
        if SMleptID% 2 == 1 :
            WleptID=24
            SMPairleptID = SMleptID-1
        else                :
            WleptID=-24
            SMPairleptID = SMleptID+1
        
    # exact BR values are not important, as they are recalculated by jobOpythia
    topAlg.Pythia8.UserResonances = "ExcitedCI:"+str(leptID)
    topAlg.Pythia8.Commands  += [
##        str(leptID)+':oneChannel = 1 0.1 0 13 -13 '+str(SMleptID),
         str(leptID)+':oneChannel = 1 0.1 0 22 '+str(SMleptID),
         str(leptID)+':addChannel = 1 0.1 0 '+str(WleptID)+' '+str(SMPairleptID),
         str(leptID)+':addChannel = 1 0.1 0 23 '+str(SMleptID),
         str(leptID)+':addChannel = 1 0.1 0 11 -11 '+str(SMleptID),
         str(leptID)+':addChannel = 1 0.1 0 12 -12 '+str(SMleptID),
         str(leptID)+':addChannel = 1 0.1 0 13 -13 '+str(SMleptID),
         str(leptID)+':addChannel = 1 0.1 0 14 -14 '+str(SMleptID),
         str(leptID)+':addChannel = 1 0.1 0 15 -15 '+str(SMleptID),
         str(leptID)+':addChannel = 1 0.1 0 16 -16 '+str(SMleptID),
         str(leptID)+':addChannel = 1 0.1 0 1 -1 '+str(SMleptID),
         str(leptID)+':addChannel = 1 0.1 0 2 -2 '+str(SMleptID),
         str(leptID)+':addChannel = 1 0.1 0 3 -3 '+str(SMleptID),
         str(leptID)+':addChannel = 1 0.1 0 4 -4 '+str(SMleptID),
         str(leptID)+':addChannel = 1 0.1 0 5 -5 '+str(SMleptID),
         str(leptID)+':addChannel = 1 0.1 0 6 -6 '+str(SMleptID)
        ]
        
else :
        from AthenaCommon.Logging import logging
        logging.error("Default Pythia8 does not have double production of "+str(leptID))
    

topAlg.Pythia8.Commands += [
     str(leptID)+":m0 = "+str(M_ExNu),          #"4000012:m0 = "+str(M_ExNu), # 
    "ExcitedFermion:Lambda = "+str(M_Lam),
    "ExcitedFermion:coupF = "+str(f),           # SU(2) coupling
    "ExcitedFermion:coupFprime = "+str(fPrime), # U(1) coupling
    "ExcitedFermion:coupFcol = "+str(f),        # SU(3) coupling
    ] 

