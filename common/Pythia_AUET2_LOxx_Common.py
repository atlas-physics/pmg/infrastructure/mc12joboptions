## PYTHIA 6 config with AUET2-LO** tune
## NB. This is not AUET2B!
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "ATLAS_20110001"
evgenConfig.tune = "AUET2 LO**"
