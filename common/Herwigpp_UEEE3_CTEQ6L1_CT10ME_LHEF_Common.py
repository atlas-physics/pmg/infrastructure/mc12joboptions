## Herwig++ config for the CTEQ6L1 UE-EE-3 tune series with NLO events read from an LHEF file
include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_CT10ME_Common.py")

from Herwigpp_i import config as hw
topAlg.Herwigpp.Commands += hw.lhef_cmds(filename="events.lhe", nlo=True).splitlines()
