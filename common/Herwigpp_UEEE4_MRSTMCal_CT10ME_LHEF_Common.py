## Herwig++ config for the MRSTMCal UE-EE-4 tune series with an NLO ME PDF with NLO events read from an LHEF file
include("MC12JobOptions/Herwigpp_UEEE4_MRSTMCal_CT10ME_Common.py")

from Herwigpp_i import config as hw
topAlg.Herwigpp.Commands += hw.lhef_cmds(filename="events.lhe", nlo=True).splitlines()
