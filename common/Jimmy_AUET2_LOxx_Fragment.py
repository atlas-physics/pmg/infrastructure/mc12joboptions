## Energy-specific UE tune handling for JIMMY with the AUET2 LO** tune

## Calculate the energy-specific pT cutoff using the standard ansatz with tuned coeff and exponent
__ptjim = 3.696 * (runArgs.ecmEnergy/1800.)**0.219

## Set params
topAlg.Herwig.HerwigCommand += [
    # PDF
    "modpdf 20651",     # LO** PDF
    "autpdf DEFAULT",  # External PDF library
    # AUET2 (LO**) tune settings
    "ispac 2",          # ISR-shower scheme
    "qspac 2.5",        # ISR shower cut-off (default value)
    "ptrms 1.2",        # Primordial kT
    "ptjim %f" % __ptjim, # Min pT of secondary scatters, calculated above
    "jmrad 73 2.339",   # Inverse proton radius squared
    "prsof 0",          # Soft underlying event off (HERWIG parameter)
    ]

del __ptjim

evgenConfig.tune = "AUET2 LO**"
