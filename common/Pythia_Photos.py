## PHOTOS config for PYTHIA

## Disable native QED FSR
assert hasattr(topAlg, "Pythia")
topAlg.Pythia.PythiaCommand += ["pydat1 parj 90 20000"]

## Enable PHOTOS
include("MC12JobOptions/Photos_Fragment.py")
