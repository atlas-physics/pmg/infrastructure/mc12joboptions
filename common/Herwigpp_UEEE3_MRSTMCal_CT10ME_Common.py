## Herwig++ config for the MRSTMCal UE-EE-3 tune series with an NLO ME PDF
include("MC12JobOptions/Herwigpp_Base_Fragment.py")

## Construct command set
from Herwigpp_i import config as hw
cmds = hw.energy_cmds(runArgs.ecmEnergy) + hw.base_cmds() \
     + hw.nlo_pdf_cmds("CT10.LHgrid", "MRSTMCal.LHgrid") \
     + hw.ue_tune_cmds("LO**-UE-EE-%d-3" % int(runArgs.ecmEnergy))
topAlg.Herwigpp.Commands += cmds.splitlines()
del cmds
