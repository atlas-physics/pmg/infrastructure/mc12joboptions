## Config for Py8 tune AU2 with NNPDF21NLO
include("MC12JobOptions/Pythia8_Base_Fragment.py")

topAlg.Pythia8.Commands += [
    "Tune:pp = 5",
    "PDF:useLHAPDF = on",
    "PDF:LHAPDFset = NNPDF21_100.LHgrid",
    "MultipartonInteractions:bProfile = 4",
    "MultipartonInteractions:a1 = 0.08",
    "MultipartonInteractions:pT0Ref = 1.74",
    "MultipartonInteractions:ecmPow = 0.17",
    "BeamRemnants:reconnectRange = 8.36",
    "SpaceShower:rapidityOrder=0"]
evgenConfig.tune = "AU2 NNPDF21NLO"
