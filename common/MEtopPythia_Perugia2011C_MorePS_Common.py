## Configuration for MEtop+PYTHIA using the Perugia2011C MorePS tune
include("MC12JobOptions/Pythia_CTEQ6L1_Perugia2011C_MorePS_Common.py")
evgenConfig.generators = [ "MEtop", "Pythia"]

## Read external Les Houches event file
topAlg.Pythia.PythiaCommand += ["pyinit user lhef"]

