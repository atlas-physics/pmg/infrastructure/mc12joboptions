## PYTHIA 6 config with Perugia SOFT tune
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_322"
evgenConfig.tune = "PerugiaSOFT"
