##############################################################
# Pythia8B_exclusiveB_Common.py
#
# Common job options for exclusive b production using
# Pythia8B.
##############################################################

# Hard process
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.VetoDoubleBEvents = True

# Close B decays and open antiB decays
include ("Pythia8B_i/CloseBDecays.py")

# List of B-species
include("Pythia8B_i/BPDGCodes.py")
