########################################################
# OpenBPsi2SDecays.py
# Opens B->psi(2S) decays
########################################################

# B0
topAlg.Pythia8B.Commands += ['511:onIfAny = 100443']
# B+/-
topAlg.Pythia8B.Commands += ['521:onIfAny = 100443']
# Bs
topAlg.Pythia8B.Commands += ['531:onIfAny = 100443']
# Bc
topAlg.Pythia8B.Commands += ['541:onIfAny = 100443']
# LambdaB
topAlg.Pythia8B.Commands += ['5122:onIfAny = 100443']
# Xb+/-
topAlg.Pythia8B.Commands += ['5132:onIfAny = 100443']
# Xb
topAlg.Pythia8B.Commands += ['5232:onIfAny = 100443']
# Omega_b+/-
topAlg.Pythia8B.Commands += ['5332:onIfAny = 100443']

