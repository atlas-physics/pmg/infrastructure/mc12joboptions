## Tauola config for leptonic decays (dmode is changed to 1)
from Tauola_i.Tauola_iConf import Tauola
topAlg += Tauola()
topAlg.Tauola.TauolaCommand = \
    ["tauola polar 1",
     "tauola radcor 1",
     "tauola phox 0.01",
     "tauola dmode 1",
     "tauola jak1 0",
     "tauola jak2 0"]
evgenConfig.generators += [ "Tauola" ]
